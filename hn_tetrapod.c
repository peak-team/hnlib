///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_tetrapod.c
// Design Name:
// Module Name: HN library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    HN functions for TETRAPOD. Implementation file
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "hn.h"
#include "hn_private_common.h"
#include "hn_tetrapod.h"



const char *hn_tetrapod_to_str_unit_model(uint32_t model) {
  switch(model) {
    case HN_TETRAPOD_MODEL_DE4WI4  : return "TTDE4WI4";
    default                        : return NULL;
  }
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_tetrapod_get_utilization(uint32_t tile, uint32_t *utilization)
{
  HN_INITIALIZE_CHECK;
  // TODO
  *utilization = 15;
  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

