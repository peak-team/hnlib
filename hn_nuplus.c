
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_nuplus.c
// Design Name:
// Module Name:
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    HN functions for NU+. Implementation file
//
//
// Dependencies: NONE
//
// Revision:
//   TODO add here revisions and author
//
// Additional Comments: NONE
//
//   TODO describe here additions
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "hn.h"
#include "hn_private_common.h"
#include "hn_nuplus.h"



const char *hn_nuplus_to_str_unit_model(uint32_t model) {
  switch(model) {
    case HN_NUPLUS_MODEL_BASE      : return "NU+B";
    default                        : return NULL;
  }
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_nuplus_get_utilization(uint32_t tile, uint32_t *utilization)
{
  HN_INITIALIZE_CHECK;
  // TODO
  *utilization = 3;
  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

// --------------------------------------------------------------------------------------------------
// hn_nuplus_set_thread_mask
//   short: Sets NUPLUS core active thread mask
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_mask: NUPLUS thread mask (which threads are active)
//
uint32_t hn_nuplus_set_thread_mask (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_mask)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_nuplus_set_thread_mask: tile %d, nuplus_core_id %d, nuplus_thread_mask %x\n", tile, nuplus_core_id, nuplus_thread_mask);

  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, COMMAND_NUPLUS_SET_THREAD_MASK);
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_thread_mask);

  return HN_SUCCEEDED;
}

// --------------------------------------------------------------------------------------------------
// hn_nuplus_stop
//   short: Stops NUPLUS core
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_num: Total number of cores
//
uint32_t hn_nuplus_stop (uint32_t tile, uint32_t nuplus_core_num)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_nuplus_stop: tile %d, nuplus_core_num %d\n", tile, nuplus_core_num);

  // Stop all NUPLUS cores
  for (int nuplus_core_id = 0; nuplus_core_id < nuplus_core_num; nuplus_core_id++){
    hn_nuplus_set_thread_mask (tile, nuplus_core_id, 0);
  }

  return HN_SUCCEEDED;
}

// --------------------------------------------------------------------------------------------------
// hn_nuplus_set_pc
//   short: Sets a PC address to a given thread in the NUPLUS core
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_id: NUPLUS thread id
//   argument nuplus_pc: New PC address to be written
//
uint32_t hn_nuplus_set_pc(uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_id, uint32_t nuplus_pc)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_nuplus_set_pc: tile %d, nuplus_core_id %d, nuplus_thread_id %d, nuplus_pc %x\n", tile, nuplus_core_id, nuplus_thread_id, nuplus_pc);

  // We send the items 
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, COMMAND_NUPLUS_SET_PC);
  //hn_send_item_to_unit(tile, nuplus_core_id); //TODO: Mirko will change it
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_thread_id);
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_pc);

  return HN_SUCCEEDED;
}

// --------------------------------------------------------------------------------------------------
// hn_nuplus_get_control_register
//   short: Reads NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_id: NUPLUS thread id (some control registers have different instance for each thread)
//   argument nuplus_cr: NUPLUS control register id
//   argument item_rec: where to store the value from the selected control register
//
uint32_t hn_nuplus_get_control_register (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_id, uint32_t nuplus_cr, uint32_t * item_rec)
{
  HN_INITIALIZE_CHECK;

  uint32_t nuplus_data = ((nuplus_thread_id & 0x0000FFFF) << 16) | (nuplus_cr & 0x0000FFFF);

  log_debug("hn_nuplus_get_control_register: tile %d, nuplus_core_id %d, nuplus_thread_id %d, nuplus_cr %x\n", tile, nuplus_core_id, nuplus_thread_id, nuplus_cr);

  // We send the items 
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, COMMAND_NUPLUS_READ_STATUS);
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_data);
  hn_read_socket(item_rec, sizeof(uint32_t));
  //hn_read_register(tile, 6, &item_rec)

  return 0;
}

// --------------------------------------------------------------------------------------------------
// hn_nuplus_set_control_register
//   short: Write a NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_id: NUPLUS thread id (some control registers have different instance for each thread)
//   argument nuplus_cr: NUPLUS control register id
//   argument nuplus_data: data to write into the selected NUPLUS control register
//
uint32_t hn_nuplus_set_control_register (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_id, uint32_t nuplus_cr, uint32_t nuplus_data)
{
  HN_INITIALIZE_CHECK;

  uint32_t nuplus_tmp = ((nuplus_thread_id & 0x0000FFFF) << 16) | (nuplus_cr & 0x0000FFFF);

  log_debug("hn_nuplus_set_control_register: tile %d, nuplus_core_id %d, nuplus_thread_id %d, nuplus_cr %x nuplus_data %x\n", tile, nuplus_core_id, nuplus_thread_id, nuplus_cr, nuplus_data);

  // We send the items 
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, COMMAND_NUPLUS_WRITE_STATUS);
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_tmp);
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_data);

  return HN_SUCCEEDED;
}

// ---------------------------------------------------------------------------------------------
// hn_boot_nuplus - XXX: Never called
//   short: Boots NUPLUS core
//   argument tile: Tile where the unit is located
//   argument tile_memory: Tile where the memory image (if needed) is located)
//   argument addr: Starting address of the memory image (if needed)
//
uint32_t hn_nuplus_boot(uint32_t tile, uint32_t tile_memory, uint32_t addr)
{ 

  HN_INITIALIZE_CHECK;

  int nuplus_core_num = 1;
  int nuplus_thread_num = 8;

  log_debug("hn_boot_nuplus: tile %d, tile_memory %d, addr %x\n", tile, tile_memory, addr);

  // The process to boot NUPLUS is divided in the following steps:
  //   1) Set threads mask to 0x0000 for each core
  //   2) Set PCs initial values for each core and threads
  
  for (int nuplus_core_id = 0; nuplus_core_id < nuplus_core_num; nuplus_core_id++){
    hn_nuplus_set_thread_mask (tile, nuplus_core_id, 0);
    for (int nuplus_thread_id = 0; nuplus_thread_id < nuplus_thread_num; nuplus_thread_id++){
      hn_nuplus_set_pc(tile, nuplus_core_id, nuplus_thread_id, NUPLUS_DEFAULT_PC);
    }
  }

  return HN_SUCCEEDED;
}

// --------------------------------------------------------------------------------------------------
// hn_nuplus_run_kernel
//   uint32_t: HN_SUCCEEDED
//   argument tile: Tile where the NuPlus unit is located
//   argument addr: Virtual address where the image will be found
//   argument args: string including all arguments
//
uint32_t hn_nuplus_run_kernel(uint32_t tile, uint32_t addr, char *args)
{
  HN_INITIALIZE_CHECK;
  uint32_t tile_mem = 0;
  uint32_t nuplus_thread_mask = 0;
  uint32_t * nuplus_args;
  uint32_t bchar_idx = 0;
  uint32_t lidx = 0;
  uint32_t pidx = 0;

  log_debug("hn_nuplus_run_kernel: tile %d, addr %x, args %x", tile, addr, args);

  // The process to boot NUPLUS is divided in the following steps:
  //   1) Prepares NUPLUS parameters and writes them into the main memory
  //   2) Sets PCs.
  //   3) Passes kernel argc and args.
  //   4) Enables NUPLUS threads.
   
  // Calculating arguments number
  char *tmp = args;
  int argc = 1;
  uint32_t * conv_res = malloc(16 * sizeof(uint32_t));

  // Counting argc
  while(*tmp){
     if(*tmp == ' '){
        argc++;
    }
    tmp++;
  }
  
  if (argc > NUPLUS_MAX_ARGS){
     log_error("hn_nuplus_run_kernel: too many arguments! ARGC: %i> Max: %i", argc, NUPLUS_MAX_ARGS);
     return HN_WRONG_ARGUMENTS;
  }
   
  // Converting arguments
  tmp = args;
  nuplus_args = malloc(argc * sizeof(uint32_t));
  argc = 0;
  int i = 1;
  while(*tmp){  
    if(*(tmp+1) == ' ' || *(tmp+1) == NULL){
      char buffer[16] = {0};
      strncpy(buffer, tmp-i+1, i);
      nuplus_args[argc] = strtol(buffer, NULL, 16); 
      argc++;
      i = 0;
    }
    else{
      i++;
    }
    tmp++;
  }
  log_debug("hn_nuplus_run_kernel: argc: %d", argc);

  // Preparing arguments for NUPLUS
  hn_nuplus_endian_row(nuplus_args, argc);

  pidx = (argc%16==0) ? argc/16 : argc/16+1;     
  // Writing parameters into main memory from address NUPLUS_DEFAULT_ARGS_ADDRESS
  log_debug("hn_nuplus_run_kernel: Writing arguments into main memory: addr %x\n", addr);
  for (int i = 0; i < pidx; i++) {
    hn_write_memory_block(tile_mem, (addr + NUPLUS_DEFAULT_ARGS_ADDRESS + i * 0x40), (char *) (nuplus_args + i * 16));
  }

  // Setting NUPLUS PCs to default values
  for (int nuplus_core_id = 0; nuplus_core_id < NUPLUS_DEFAULT_CORE_NUMB; nuplus_core_id++){
    for (int nuplus_thread_id = 0; nuplus_thread_id < NUPLUS_MAX_THREAD_NUMB; nuplus_thread_id++){
      hn_nuplus_set_pc(tile, nuplus_core_id, nuplus_thread_id, NUPLUS_DEFAULT_PC);
    }
    // Passing argc value and the default args address
    hn_nuplus_set_control_register (tile, nuplus_core_id, 0x0000, NUPLUS_KERNEL_ARGC, argc);
    hn_nuplus_set_control_register (tile, nuplus_core_id, 0x0000, NUPLUS_KERNEL_ARGV, NUPLUS_DEFAULT_ARGS_ADDRESS); 
  }

  // Calculating threads mask
  nuplus_thread_mask = hn_nuplus_thread_mask_convert((uint32_t)(nuplus_args[NUPLUS_DEFAULT_ARGS_THREAD_ID] >> 24));
  
  // Enabling NUPLUS threads
  for (int nuplus_core_id = 0; nuplus_core_id < NUPLUS_DEFAULT_CORE_NUMB; nuplus_core_id++){
    hn_nuplus_set_thread_mask (tile, nuplus_core_id, nuplus_thread_mask);
  }
  
  hn_read_memory_block(tile_mem, (addr + NUPLUS_DEFAULT_ARGS_ADDRESS), (char *) conv_res);
  hn_nuplus_endian_row(conv_res, 16);
  log_debug("Reading from memory NUPLUS args: %x\t%x\t%x\t%x\t%x ... %x", conv_res[0], conv_res[1], conv_res[2], conv_res[3], conv_res[4], conv_res[11]);
  
  return HN_SUCCEEDED;
}

// ---------------------------------------------------------------------------------------------
// hn_nuplus_thread_mask_convert
//   short: private function which converts the number of active threads into
//      the right activation thread mask
//   argument thread_numb: number of active threads
//
uint32_t hn_nuplus_thread_mask_convert(uint32_t thread_numb){
  uint32_t thread_mask = 0x0000u;

  for (int i = 0; i < thread_numb; i++){
    thread_mask = thread_mask << 1u;
    thread_mask = thread_mask | 0x01u;
  }

  return thread_mask;
}

// --------------------------------------------------------------------------------------------------
// hn_nuplus_log_event
//   short: Reads NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_log_cmd: NUPLUS Logger command
//   argument event_rec: where to store the event value from the logger
//
uint32_t hn_nuplus_log_event (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_log_cmd, uint32_t * event_rec)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_nuplus_log_event: tile %d, nuplus_core_id %d, nuplus_log_cmd %d\n", tile, nuplus_core_id, nuplus_log_cmd);

  // We send the items 
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, COMMAND_NUPLUS_LOG_REQUEST);
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_log_cmd);
  hn_read_socket(event_rec, sizeof(uint32_t));

  return HN_SUCCEEDED;
}

// --------------------------------------------------------------------------------------------------
// hn_nuplus_log_data
//   short: Reads NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_log_cmd: NUPLUS Logger command
//   argument event_rec: where to store the values from the logger
//
uint32_t hn_nuplus_log_data (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_log_cmd, uint32_t nuplus_log_addr, nup_logger_entry_t * event_rec)
{
  HN_INITIALIZE_CHECK;

  uint32_t nuplus_logger_data = ((nuplus_log_addr & 0x0000FFFF) << 16) | ((nuplus_log_cmd & 0x0000FFFF));
  uint32_t rec_item = 0;

  log_debug("hn_nuplus_log_data: tile %d, nuplus_core_id %d, nuplus_log_cmd %d\n", tile, nuplus_core_id, nuplus_log_cmd);

  // We send the items 
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, COMMAND_NUPLUS_LOG_REQUEST);
  hn_write_register(tile, HN_MANGO_TILEREG_TO_UNIT, nuplus_logger_data);

  hn_read_socket(&rec_item, sizeof(uint32_t));
  event_rec->wr = rec_item;
  hn_read_socket(&rec_item, sizeof(uint32_t));
  event_rec->id = rec_item;

  for (int i = 0; i < 16; i++){
     hn_read_socket(&rec_item, sizeof(uint32_t));
    event_rec->data[i] = rec_item;
  }

  hn_read_socket(&rec_item, sizeof(uint32_t));
  event_rec->addr = rec_item;

  return HN_SUCCEEDED;
}

// ---------------------------------------------------------------------------------------------
// hn_nuplus_endian_row
//   short: private function which swaps endianess of a give memory row
//   argument data: memory row
//
void hn_nuplus_endian_row (uint32_t * data, int size) {
  uint8_t * buffer;

  for(int i = 0; i < size; i++){
      buffer = (uint8_t *)&data[i];
      data[i] = ((buffer[0] << 24) & 0xFF000000) | ((buffer[1] << 16) & 0x00FF0000) | ((buffer[2] << 8) & 0x0000FF00) | (buffer[3] & 0x000000FF);
  }
}
