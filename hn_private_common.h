#ifndef __HN_PRIVATE_COMMON_H__
#define __HN_PRIVATE_COMMON_H__ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: february 22, 2018
// File Name: hn_private_common.h
// Design Name:
// Module Name: Heterogeneous node library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    Common file to include for HN unit specific source code
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif


/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include "hn_daemon.h"
#include "hn_utils.h" 
#include "hn_logging.h"

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

/*
 *  Checking at the begin of most of the hnlib functions.
 *
 *  RTG: I decided to put it an macro because of if we want decide to remove
 *       the checking it will be enough to declare this macro to nothing
 */
#define HN_INITIALIZE_CHECK if (!hn_initialized) return HN_NOT_INITIALIZED

/*
 *  Checking at the begin of most of the hnlib functions. Check tile id is valid
 *
 *  RTG: I decided to put it an macro because of if we want decide to remove
 *       the checking it will be enough to declare this macro to nothing
 *  WARNING: tile=999 is used when we want the operation to be valid for ANY_TILE, 999 should be replaced by a TAG
 */
#define HN_TILE_CHECK if ((tile >= hn_num_tiles_in_arch) && (tile != 999)) {printf("err out of range tile id %u\n", tile); fflush(stdout);return HN_TILE_ID_OUT_OF_RANGE;}

/*
 * Reads from socket
 *
 * \param [in] data pointer where data will be stored
 * \param [in] num_bytes number of bytes we want to read
 * \return HN result code
 */
uint32_t hn_read_socket(void *data, uint32_t num_bytes);

/*
 * Reads from socket using a best effort approach, 
 * i.e., it could read less than requested
 *
 * \param [in] data pointer where data will be stored
 * \param [in] num_bytes number of bytes we want to read
 * \param [out] read_bytes number of bytes read actually
 * \return HN result code
 */
uint32_t hn_read_socket_best_effort(void *data, uint32_t num_bytes, uint32_t *read_bytes);

/*
 * Writes to socket
 *
 * \param [in] data pointer to the data to write
 * \param [in] num_bytes number of bytes to write
 * \return HN result code
 */
uint32_t hn_write_socket(void *data, uint32_t num_bytes);



#ifdef __cplusplus
}
#endif

#endif

//*********************************************************************************************************************
// end of file
//*********************************************************************************************************************
