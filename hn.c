///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    hn library
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <locale.h>
#include <signal.h>
#include <sys/select.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdint.h>
#include <netinet/in.h>

#define __HN_ENTRY_POINT
#include "hn.h"
#include "hn_item_commands.h"
#include "hn_private_common.h"
#include "hn_socket_unix.h"
#include "hn_list.h"
#include "hn_shared_memory.h"

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

#define BLOCK_MEMORY_SIZE_BYTES 64
#define FILE_LINE_MAX_LENGTH    512


#define HN_STRATEGIC_OPERATION_SHM_BUFFER_SIZE (16 * 1024 * 1024)
#define HN_STRATEGIC_OPERATION_MODE_1_SIZE_THRESHOLD (2048*64)
//#define HN_STRATEGIC_WRITE_MODE_1
//#define HN_STRATEGIC_WRITE_MODE_2
//#define HN_STRATEGIC_WRITE_DUMMYDATA_MODE_2
#define HN_STRATEGIC_WRITE_MODE_3
//#define HN_STRATEGIC_WRITE_DUMMYDATA_MODE_3

#define HN_STRATEGIC_READ_MODE_1
//#define HN_STRATEGIC_READ_MODE_2
//#define HN_STRATEGIC_READ_MODE_3


// This is to indicate a no valid architecture
#define HN_ARCHITECTURE_UNDEFINED  99999


uint64_t as_nanoseconds(struct timespec* ts) {
    return ts->tv_sec * (uint64_t)1000000000L + ts->tv_nsec;
}
/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
// global variables of the library -------------------------------------------------------------------
uint32_t            hn_partition_strategy;                        // partition strategy to be used
uint32_t            hn_read_register_access_enabled;              // When set it enables reading register
static int          fd_socket = -1;
int                 char_ptr;                                     // char pointer to a dummy string to get dummy chars through a non-connected console
char                register_free[HN_MAX_TILES][HN_MAX_REGS_PER_TILE];  // Register usage for synch events (and other uses)

static uint32_t     hn_shm_buffer_info_list;
static uint32_t     hn_num_tiles_in_arch;                         // number of tiles in current architecture

static struct sigaction    act_old_sigint;
static struct sigaction    act_old_sigterm;

static uint32_t     hn_stats_deprecated_access = 0;

void               *hn_shm_buffer_strategic_write = NULL;
void               *hn_shm_buffer_strategic_read  = NULL;

/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/

// hn_connect_to_daemon
//   short: Establishes connection with the HN daemon and requests a socket to get info from HN system
//   argument filter: Filter to define the type of socket (console, debug, raw frames, ...)
//

static uint32_t hn_connect_to_daemon(hn_filter_t filter);

// ---------------------------------------------------------------------------------------------------
// hn_disconnect_from_daemon
//   short: Disconnects from hn daemon
//
static uint32_t hn_disconnect_from_daemon(void); 

// ---------------------------------------------------------------------------------------------------
// hn_release_shm_strategic_buffers
//   notify daemon to release shm buffers with the application
static uint32_t hn_release_shm_strategic_buffers(void);

/**********************************************************************************************************************
 **
 ** Functions implementation
 **
 **********************************************************************************************************************
 **/

void hn_signal_handler(int sig) {
  hn_end();
  raise(sig);
}
/**************************************************************************************************
***************************************************************************************************
 **/

// ---------------------------------------------------------------------------------------------------- //
// Functions to connect and disconnect from HN daemon server                                            //
// These functions are private and cannot be used by the client application                             //
//    hn_connect_to_daemon                                                                              //
//    hn_disconnect_from_daemon                                                                         //
//                                                                                                      //
//    hn_                                                                                               //
//                                                                                                      //
uint32_t hn_connect_to_daemon(hn_filter_t filter)
{
  struct sockaddr_un server;

  fd_socket = socket(AF_UNIX, SOCK_STREAM, 0);
  if (fd_socket < 0)
  {
    log_error("hn_connect_to_daemon: opening socket");
    fd_socket = -1;
    return HN_SOCKET_ERROR;
  }

  server.sun_family = AF_UNIX;
  strcpy(server.sun_path, HN_DAEMON_SOCKET_UNIX_PORT);

  if (connect(fd_socket, (struct sockaddr *)&server, sizeof(struct sockaddr_un)) < 0)
    {
    close(fd_socket);
    fd_socket = -1;
    log_error("hn_connect_to_daemon: connecting to the socket");
    return HN_SOCKET_ERROR;
  }

  // we send the filter to the daemon
  hn_command_set_filter_t cmd;
  cmd.common.command = HN_COMMAND_SET_FILTER;
  cmd.tile           = filter.tile;
  cmd.core           = filter.core;
  cmd.target         = filter.target;
  cmd.mode           = filter.mode;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_disconnect_from_daemon(void)
{
  if (fd_socket != -1)
    close(fd_socket);

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/
uint32_t hn_release_shm_strategic_buffers(void)
{
  uint32_t rv_1 = 0;
  uint32_t rv_2 = 0;
  uint32_t rv;

  if(hn_shm_buffer_strategic_write != NULL) 
  {
    rv_1 = hn_shm_free(hn_shm_buffer_strategic_write);
    if (rv_1 != HN_SUCCEEDED) {
      log_debug("Error deleting buffer in shared memory, returned: %u", rv_1);
    }
  }

  if(hn_shm_buffer_strategic_read != NULL)
  {
    rv_2 = hn_shm_free(hn_shm_buffer_strategic_read);
    if (rv_2 != HN_SUCCEEDED) {
      log_debug("Error deleting buffer in shared memory, returned: %u", rv_2);
    }
  }

  rv = rv_1 + rv_2;
  if (rv != HN_SUCCEEDED) log_warn("hn-lib shm buffers with daemon were not properly released");

  return rv;
}


/**************************************************************************************************
***************************************************************************************************
**/

// ---------------------------------------------------------------------------------------------------- //
// Functions to read & write the socket                                                                 //
// These functions are private and cannot be used by the client application                             //
//    hn_write_socket                                                                                   //
//    hn_read_socket                                                                                    //
//                                                                                                      //
uint32_t hn_write_socket(void *data, uint32_t num_bytes) {
  fd_set wfds;                    // to set the file descriptors we want to be notified when an event ocurrs 
  struct timeval tv;             // timeout
  int select_rs = 0;             // to know if there is something in the socket for reading
  int num_bytes_written = 0;     // actual number of bytes written the socket in a call to write
  int buf_offset = 0;            // what is the offset to put the data read from the socket
  uint32_t ret_code = HN_SUCCEEDED;

  if ((num_bytes > 0) && (data != NULL))
  { // we want to write something
    while ((hn_initialized == 1) && (buf_offset < num_bytes) && (ret_code == HN_SUCCEEDED))
    {
      // let's initialize file descriptors in which we want to be notified when write is possible
      FD_ZERO(&wfds);
      FD_SET(fd_socket, &wfds);

      /* wait upto 1s in select before wake up */
      tv.tv_sec  = 1;
      tv.tv_usec = 0;

      select_rs = select(fd_socket+1, NULL, &wfds, NULL, &tv);
      if (select_rs > 0)
      { // the socket is available for writing
        num_bytes_written = write(fd_socket, (unsigned char *)data + buf_offset, num_bytes - buf_offset);
        if ((num_bytes_written < 0) && (errno == EPIPE))
        {
          log_notice("hn_write_socket: the other end of the socket has disconnected");
          ret_code = HN_SOCKET_ERROR;
        }
        else if (num_bytes_written > 0)
        {
          log_verbose("hn_write_socket: written %d / %d bytes", num_bytes_written, num_bytes);
          buf_offset += num_bytes_written;     
        }
      }
    }
  }

  // the function hn_end() could be called meanwhile we are waiting to write the desired number of bytes
  if (hn_initialized == 0)
    ret_code = HN_NOT_INITIALIZED;  

  return ret_code;
}
/**************************************************************************************************
***************************************************************************************************
 **/

uint32_t hn_read_socket(void *data, uint32_t num_bytes) {
  fd_set rfds;                   // to set the file descriptors we want to be notified when an event ocurrs 
  struct timeval tv;             // timeout
  int select_rs = 0;             // to know if there is something in the socket for reading
  int num_bytes_read = 0;        // actual number of bytes read from the socket
  int buf_offset = 0;            // what is the offset to put the data read from the socket
  uint32_t ret_code = HN_SUCCEEDED;

  if ((num_bytes > 0) && (data != NULL))
  { // we want to read something. So, let's iterate until we read what we want or some error happens
    while ((hn_initialized == 1) && (buf_offset < num_bytes) && (ret_code == HN_SUCCEEDED))
{
      // let's initialize file descriptors in which we want to be notified when read/write occurs
      FD_ZERO(&rfds);
      FD_SET(fd_socket, &rfds);

      /* wait upto 1s in select before wake up */
      tv.tv_sec  = 1;
      tv.tv_usec = 0;

      select_rs = select(fd_socket+1, &rfds, NULL, NULL, &tv);
      if (select_rs > 0)
      { // there is something in the socket to read
        num_bytes_read = read(fd_socket, (unsigned char *)data + buf_offset, num_bytes - buf_offset);
        if (num_bytes_read <= 0)
  {
          log_notice("hn_read_socket: the other end of the socket has disconnected");
          ret_code = HN_SOCKET_ERROR;
  }
        else if (num_bytes_read > 0)
  {
          buf_offset += num_bytes_read;     
          log_verbose("hn_read_socket: read %u / %u in_this_read=%u (all in bytes)", buf_offset, num_bytes, num_bytes_read);
  }
}
}
  }

  // the function hn_end() could be called meanwhile we are waiting to read the desired number of bytes
  if (hn_initialized == 0)
    ret_code = HN_NOT_INITIALIZED;  

  return ret_code;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_read_socket_best_effort(void *data, uint32_t num_bytes, uint32_t *read_bytes) {
  fd_set rfds;                   // to set the file descriptors we want to be notified when an event ocurrs 
  struct timeval tv;             // timeout
  int select_rs = 0;             // to know if there is something in the socket for reading
  int num_bytes_read = 0;        // actual number of bytes read from the socket
  uint32_t ret_code = HN_SUCCEEDED;

  *read_bytes = 0;
  if ((num_bytes > 0) && (data != NULL))
  { // we want to read something. 
    // let's initialize file descriptors in which we want to be notified when read/write occurs
    FD_ZERO(&rfds);
    FD_SET(fd_socket, &rfds);

    /* wait upto 1s in select before wake up */
    tv.tv_sec  = 15;
    tv.tv_usec = 0;

    select_rs = select(fd_socket+1, &rfds, NULL, NULL, &tv);
    if (select_rs > 0)
    { // there is something in the socket to read
      num_bytes_read = read(fd_socket, (unsigned char *)data, num_bytes);
      if (num_bytes_read <= 0)
      {
        log_notice("hn_read_socket: the other end of the socket has disconnected");
        ret_code = HN_SOCKET_ERROR;
      } else {
        *read_bytes = (uint32_t)num_bytes_read;
      }
    }
  }

  // the function hn_end() could be called meanwhile we are waiting to read the desired number of bytes
  if (hn_initialized == 0)
    ret_code = HN_NOT_INITIALIZED;  

  return ret_code;
}
/**************************************************************************************************
***************************************************************************************************
**/

/**********************************************************************************************************************
 **
 ** Public functions
 **
 **********************************************************************************************************************
 **/
uint32_t initialize_shm_resources (void)
{
  int rv;
  int lst;

  // let's create shared_memory_area buffers list
  lst = hn_list_create_list();
  if (lst == -1)
  {
    log_error("shared memory area buffers list could not be created");
    rv = -1;
  } 
  else
  {
    hn_shm_buffer_info_list = (unsigned int)lst;
    log_debug("hn_shm_buffer_list created with id %u", hn_shm_buffer_info_list);
  }

  return rv;
}

/**************************************************************************************************
***************************************************************************************************
**/
void destroy_shm_resources (void)
{
  // let's destroy the shared_memory_area buffers list
  hn_list_destroy_list(hn_shm_buffer_info_list);
}

/**************************************************************************************************
***************************************************************************************************
**/
uint32_t hn_initialize(hn_filter_t filter, uint32_t partition_strategy, uint32_t socket_connection, uint32_t reset, uint32_t capture_signals_enable) 
{
  uint32_t rv;
  uint32_t i;
  uint32_t j;

  rv = HN_SUCCEEDED;

  // first thing is to set log for errors, warnings and infos to std out
  hn_log_initialize(HN_LOG_TO_STDOUT, NULL);

  log_debug("hn_initialize: filter.target : %d", filter.target);

#ifndef HDL_SIM
  if (capture_signals_enable != 0) {
    // To block all possible signals when running the handler
    sigset_t set;
    struct sigaction act;
    sigfillset(&set);

    // fill the action for the signal to use to terminate the program
    act.sa_handler = hn_signal_handler;
    act.sa_mask = set;
    act.sa_flags = SA_ONESHOT;
    sigaction(SIGINT, &act, &act_old_sigint);
    sigaction(SIGTERM, &act, &act_old_sigterm);
 
    // block all the signals, except for the elected for terminating the program
    // FIXME this could too hard
    sigdelset(&set, SIGINT);
    sigdelset(&set, SIGTERM);
    pthread_sigmask(SIG_BLOCK, &set, NULL);
    log_notice("hn_initialize: registering SIGINT & SIGTERM to terminate the program or stoping access to the HN library");
  }
#endif

  // The HN library has been correctly initialized 
  hn_initialized = 1;


  rv = initialize_shm_resources();

  log_debug("hn_initialize: connecting to daemon with filter");
  rv = hn_connect_to_daemon(filter);
  if (rv != HN_SUCCEEDED) {
    hn_end();
    return rv;
  }
  log_debug("hn_connect_to_daemon: connection to the daemon stablished with filter");

  // now we get the arch id, notice that only MANGO TARGET can get access to the complete
  // system, so for other accesses different we do not get grant to registers for reading
  if ((filter.target == HN_FILTER_TARGET_MANGO) && (filter.mode == HN_FILTER_APPL_MODE_SYNC_READS)) {
    hn_read_register_access_enabled = 1;
  } else {
    hn_read_register_access_enabled = 0;
  }

  // We remember the partition strategy
  hn_partition_strategy = partition_strategy;
  
  char_ptr = 0;
  
  // We initialize the bitmap for free synch registers
  // WARNING: regs from 
  for (i=0;i<HN_MAX_TILES;i++) {
    for (j=0;j < HN_REGULAR_REG_TYPE_LOW_ID; j++ ){
      register_free[i][j] = 0;
    }
    for (j=HN_REGULAR_REG_TYPE_LOW_ID;j<HN_MAX_REGS_PER_TILE;j++) {
      register_free[i][j] = 1;
    }
  }

  // We initialize the number of tiles in this architecture
  uint32_t tmp1;
  uint32_t tmp2;
  log_debug("hn_initialize: getting number of tiles in architecture");
  rv = hn_get_num_tiles (&hn_num_tiles_in_arch, &tmp1, &tmp2);
  if (rv != HN_SUCCEEDED) {
    hn_end();
    return rv;
  }
  log_debug("hn_connect_to_daemon: connection to the daemon stablished with filter");

  // let's create the buffers for the strategic read/write operations
  rv = hn_shm_malloc(HN_STRATEGIC_OPERATION_SHM_BUFFER_SIZE, &hn_shm_buffer_strategic_write);
  if (rv != HN_SUCCEEDED) {
    log_error("creating shm_buffer_strategic_write, errno %s", strerror(errno));
    hn_end();
    return rv;
  }
  rv = hn_shm_malloc(HN_STRATEGIC_OPERATION_SHM_BUFFER_SIZE, &hn_shm_buffer_strategic_read);
  if (rv != HN_SUCCEEDED) {
    log_error("creating shm_buffer_strategic_read, errno %s", strerror(errno));
    hn_end();
    return rv;
  }
  log_debug("hn_initialized, shm buffers for strategic read/write item operations created");

  log_info("HN low-level runtime initialized");

  // initialization completed
  return HN_SUCCEEDED;
}

// -------------------------------------------------------------------------------------------------
// hn_end function. This functions deletes all the internal structures and concludes the library
//   short: Ends the library. As a single action it disconnects from the daemon server
//
uint32_t hn_end() 
{
  uint32_t rv = HN_SUCCEEDED;

  if (hn_initialized == 1) {
    log_info("HN low-level runtime access stopped");
    hn_release_shm_strategic_buffers();
    hn_disconnect_from_daemon();
    hn_log_close();
    hn_initialized = 0;
  }

  sigaction(SIGINT, &act_old_sigint, NULL);
  sigaction(SIGTERM, &act_old_sigterm, NULL);


  return rv;
                              }
/**************************************************************************************************
***************************************************************************************************
**/

 
// --------------------------------------------------------------------------------------------------- //
// Functions to read and write MANGO registers                                                         //
// These functions are public and can be used by the client application directly                       //
//    hn_read_register                                                                                 //
//    hn_write_register                                                                                //
//                                                                                                     //

uint32_t hn_read_register(uint32_t tile, uint32_t reg, uint32_t *data)
{
  HN_INITIALIZE_CHECK;

  int wc, rc;

  log_debug("hn_read_register (%2d) in tile %d, register %d",HN_COMMAND_READ_REGISTER, tile, reg);

  HN_TILE_CHECK;

  if (hn_read_register_access_enabled == 0) return HN_READ_REGISTER_ACCESS_NOT_ALLOWED;

  hn_command_read_register_t cmd;
  cmd.common.command = HN_COMMAND_READ_REGISTER;
  cmd.tile           = tile;
  cmd.reg            = reg;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  if (data != NULL) {
    // we read the register value through the socket
    rc = hn_read_socket(data, sizeof(uint32_t));
    if (rc != HN_SUCCEEDED) {
      return rc;
    }
    log_debug("data read: %08x", *data);
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_write_register(uint32_t tile, uint32_t reg, uint32_t data)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_write_register: tile %d, register %d, value %d", tile, reg, data);
  HN_TILE_CHECK;

  hn_command_write_register_t cmd;
  cmd.common.command = HN_COMMAND_WRITE_REGISTER;
  cmd.tile           = tile;
  cmd.reg            = reg;
  cmd.data           = data;
 
  return hn_write_socket(&cmd, sizeof(cmd));
}
 
// ---------------------------------------------------------------------------------------------------
// hn_load_architecture
//   short: Loads an architecture on the FPGA system
//   argument arch_id: Architecture ID to be loaded
//
uint32_t hn_load_architecture(uint32_t arch_id) {
  char cmd[200];

  // We load a bitfile depending on the architecture id
  sprintf(cmd, "source /opt/prodesign/profpga/proFPGA-2015D/bin/settings64_2015D.sh && /opt/prodesign/profpga/proFPGA-2014C/bin/linux_x86_64/profpga_run /home/jflich/profpga/arch%03d.cfg -u", arch_id);
  system(cmd);

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

// ------------------------------------------------------------------------------------------------
// Functions to retrieve configuration information and statistics
// These functions are accessible by the client application
//   hn_get_arch_id
//   hn_get_num_tiles
//   hn_get_num_vns
//   hn_get_memory_size
//   hn_get_tile_info
//   hn_get_tile_stats
//   hn_get_tile_temperature
//   hn_get_router_port_bandwidth
//

uint32_t hn_get_arch_id(uint32_t *arch_id)
{
  HN_INITIALIZE_CHECK;

  uint32_t v = 0;

  // FIXME May it be better to ask for the arch id to the resource manager
  uint32_t ret_code = hn_read_register(HN_TILE_0, HN_MANGO_TILEREG_CONF, &v); 
  if (ret_code == HN_SUCCEEDED)
  {
    // The ARCH id is located in bits 7..0
    *arch_id = v & 0x000000FF;    
    log_debug("hn_get_arch_id: got arch id %d", *arch_id);
  }
  else
  {
    // the arch id could not be got
    *arch_id = HN_ARCHITECTURE_UNDEFINED;
    if (ret_code != HN_NOT_INITIALIZED){
      log_error("hn_get_arch_id: could not get the architecture id", *arch_id);
    }
  }

  return ret_code;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_num_tiles(uint32_t *num_tiles, uint32_t *num_tiles_x, uint32_t *num_tiles_y) 
{
  HN_INITIALIZE_CHECK;

   // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_NUM_TILES_RSC;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait from server
  uint32_t rc;
  hn_data_rsc_t data;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
 
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  *num_tiles   = data.num_tiles;  
  *num_tiles_x = data.num_tiles_x;
  *num_tiles_y = data.num_tiles_y;
  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_num_vns(uint32_t *num_vns) 
{
  HN_INITIALIZE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_NUMBER_OF_NETWORKS_RSC;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait from server
  uint32_t rc;
  hn_data_rsc_t data;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
 
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  *num_vns = data.num_vns;

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_memory_size(uint32_t tile, uint32_t *size)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_MEMORY_SIZE_RSC;
  cmd.tile_mem       = tile;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait from server
  uint32_t rc;
  hn_data_rsc_t data;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
 
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_MEMORY_NOT_PRESENT_IN_TILE;
  }

  *size = data.mem_size;

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_tile_info(uint32_t tile, hn_tile_info_t *tile_info) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_TILE_INFO_RSC;
  cmd.tile           = tile;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait from server
  uint32_t rc;
  hn_data_rsc_t data;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
 
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_TILE_DOES_NOT_EXIST;
  }

  tile_info->unit_family     = data.type;
  tile_info->unit_model      = data.subtype;
  tile_info->memory_attached = data.mem_size;

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

const char *hn_to_str_unit_family(uint32_t family) {
  switch(family) {
    case HN_TILE_FAMILY_PEAK     : return "PEAK";
    case HN_TILE_FAMILY_NUPLUS   : return "NUPLUS";
    case HN_TILE_FAMILY_DCT      : return "DCT";
    case HN_TILE_FAMILY_TETRAPOD : return "TETRAPOD";
    case HN_TILE_FAMILY_GN       : return "GN";
    default                      : return "UNKNOWN";
  }
}
/**************************************************************************************************
***************************************************************************************************
**/

const char *hn_to_str_unit_model(uint32_t model) {
  const char *str_model;
  if ((str_model = hn_peak_to_str_unit_model(model)) != NULL)
    return str_model;
  else if ((str_model = hn_nuplus_to_str_unit_model(model)) != NULL)
    return str_model;
  else if ((str_model = hn_dct_to_str_unit_model(model)) != NULL)
    return str_model;
  else if ((str_model = hn_tetrapod_to_str_unit_model(model)) != NULL)
    return str_model;

  return "UNKNOWN";
}
/**************************************************************************************************
***************************************************************************************************
**/


uint32_t hn_get_tile_temperature(uint32_t tile, float *temp)
{
  hn_command_temperature_request_t   cmd_request;
  hn_command_temperature_response_t  cmd_response;
  uint32_t                   rv;
  
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // if returned val != 0.... error, but currently the function has no error control
  //hn_rscmgt_get_tile_location(tile, mb_desc, fm_desc);

  // get motherboard and fpga module where tile is located
  cmd_request.info.command = HN_COMMAND_GET_TEMPERATURE_OF_FPGA;
  cmd_request.tile         = tile;

  rv = hn_write_socket(&cmd_request, sizeof(hn_command_temperature_request_t));

  if(rv != HN_SUCCEEDED) {
    if (rv != HN_NOT_INITIALIZED) {
      log_error("writing temperature request in daemon socket");
    }
    return rv;
  }

  rv = hn_read_socket(&cmd_response, sizeof(hn_command_temperature_response_t));
  if(rv != HN_SUCCEEDED)
  {
    if (rv != HN_NOT_INITIALIZED) {
      log_error("getting temperature response from daemon socket");
    }
    return rv;
  }


  if (cmd_response.status != HN_TEMPERATURE_OPERATION_OK)
  {
    rv = HN_TEMPERATURE_ERROR;
  }

  *temp = (float)(cmd_response.temperature);
  
  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_tile_stats(uint32_t tile, hn_tile_stats_t *data)
{
  HN_INITIALIZE_CHECK;
  uint32_t rv = 0;

  if (!hn_stats_deprecated_access){
    log_error("DEPRECATED FUNCTION: hn_get_tile_stats, stats monitorization under development\n\n");
    hn_stats_deprecated_access = 1;
  }
/*
  HN_TILE_CHECK;

  
  uint32_t rv = HN_TILE_TYPE_NOT_RECOGNIZED;

  // Network stats
  uint32_t num_vns = 0;
  if (hn_get_num_vns(&num_vns) != HN_SUCCEEDED)
    return HN_RSC_ERROR;

  uint32_t vn;
  for (vn = 0; vn < num_vns; vn++) {
    hn_read_register(tile, HN_MANGO_TILEREG_NETSTATS, &data->flits_ejected_north[vn]);
    hn_read_register(tile, HN_MANGO_TILEREG_NETSTATS, &data->flits_ejected_east[vn]);
    hn_read_register(tile, HN_MANGO_TILEREG_NETSTATS, &data->flits_ejected_west[vn]);
    hn_read_register(tile, HN_MANGO_TILEREG_NETSTATS, &data->flits_ejected_south[vn]);
    hn_read_register(tile, HN_MANGO_TILEREG_NETSTATS, &data->flits_ejected_local[vn]);
  }

  // temperature
  uint32_t temp;
  hn_read_register(tile, HN_MANGO_TILEREG_TEMPERATURE, &temp);
  data->temperature = 20.3; // for the moment

  // TODO get utlization correctly
  hn_tile_info_t tile_info;
  uint32_t rc = hn_get_tile_info(tile, &tile_info);
  if (rc == HN_SUCCEEDED) {
    switch (tile_info.unit_family) {
      case HN_TILE_FAMILY_PEAK:     
        rv = hn_peak_get_utilization(tile, &data->unit_utilization);
                         break;

      case HN_TILE_FAMILY_NUPLUS:   
        data->unit_utilization = 11; 
        rv = HN_SUCCEEDED;
                           break;

      case HN_TILE_FAMILY_DCT: 
        data->unit_utilization = 9;
        rv = HN_SUCCEEDED;
        break;

//    case HN_TILE_TYPE_TETRAPOD:
//      rv = hn_tetrapod_get_utilization(tile, &data->unit_utilization);
//      break;
//
      default:               
        return HN_TILE_TYPE_NOT_RECOGNIZED;
    }
  }
*/
  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_router_port_bandwidth(uint32_t tile, uint32_t port, uint32_t *bandwidth)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // TODO 
  *bandwidth = 0;

  return HN_SUCCEEDED;
}


// -----------------------------------------------------------------------------------------------
// Functions to set router port bandwidths
// These functions are accessible by the client application
//   hn_set_router_port_vn_weights
//   hn_set_vn_weights_along_path
//

uint32_t hn_set_router_port_vn_weights(uint32_t tile, uint32_t port, uint32_t *weights)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we prepare the weight value
  uint32_t value = 0;  // TOMAS TODO

  // We write the value on the specific TILEREG register
  return hn_write_register(tile, HN_MANGO_TILEREG_WEIGHTS, value);
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_set_vn_weights_along_path(uint32_t src_tile, uint32_t dst_tile, uint32_t *weights)
{
  HN_INITIALIZE_CHECK;

  // TODO: Tomas..... 

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t pack_weights(uint32_t vns, uint32_t *weights, uint32_t num_weights)
{
  uint32_t vn_w, vn_mask = 0, ret_weights = 0, i;

  vn_w = (uint32_t) ceil(log2(vns));

  for (i = 0; i < vn_w; i++) {
    vn_mask |= (1 << i);
  }

  for (i = 0; i < num_weights; i++) {
    uint32_t weight_compact = weights[i] & vn_mask;
    ret_weights |= weight_compact << (vn_w * i);
    log_debug("weight %d = %d ret_weights = %08X", i, weight_compact, ret_weights);
  }

  return ret_weights;
}

uint32_t hn_set_vn_weights(uint32_t *weights, uint32_t num_weights)
{
  HN_INITIALIZE_CHECK;

  uint32_t num_tiles, num_tiles_x, num_tiles_y, i;
  uint32_t vns;
  uint32_t weights_vector_compact = 0;

  if (hn_get_num_vns(&vns) != HN_SUCCEEDED)
    return HN_RSC_ERROR;

  weights_vector_compact = pack_weights(vns, weights, num_weights);

  if (hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y) != HN_SUCCEEDED)
    return HN_RSC_ERROR;

  for (i=0;i<num_tiles;i++) {
    if (hn_write_register(i, HN_MANGO_TILEREG_WEIGHTS, weights_vector_compact) != HN_SUCCEEDED)
      return HN_RSC_ERROR;
  }

  hn_command_set_backend_weights_t cmd;

  memcpy(cmd.weights, weights, num_weights * sizeof(uint32_t));
  for (i=0;i<num_weights;i++) {
    if (cmd.weights[i] < 3) { // TODO: generalize this?
      cmd.weights[i] = 0;
    } else {
      cmd.weights[i] -= 3;
    }
  }

  cmd.common.command = HN_COMMAND_SET_BACKEND_WEIGHTS;
  cmd.num_weights    = num_weights;

  if (hn_write_socket(&cmd, sizeof(cmd)) != HN_SUCCEEDED)
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

// --------------------------------------------------------------------------------------------
// Functions to manage TLBs
// These functions are accesible from the client application
//   hn_set_tlb
//

uint32_t hn_set_tlb(uint32_t tile, uint32_t entry, uint32_t vaddr_ini, uint32_t vaddr_end, uint32_t paddr, uint32_t mem_rsc, uint32_t tilereg_rsc, uint32_t reg_rsc, uint32_t tile_rsc)
{
  HN_INITIALIZE_CHECK;

  // We prepare the words to be written in the specific register for TLB configuration
  uint32_t word_0 = entry;
  uint32_t word_1 = vaddr_ini;
  uint32_t word_2 = vaddr_end;
  uint32_t word_3 = paddr;
  uint32_t word_4 = (mem_rsc & 0x00000001) | ((tilereg_rsc & 0x00000001) << 1) | ((reg_rsc & 0x0000003F) << 2) | ((tile_rsc & 0x000001FF) << 8);

  log_debug("HN: set_tlb: tile %u, entry %u, vaddr_ini %08x, vaddr_end %08x, paddr %08x, mem %u, tilereg %u, register %u, tile_rsc %u",
             tile, entry, vaddr_ini, vaddr_end, paddr, mem_rsc, tilereg_rsc, reg_rsc, tile_rsc);

  HN_TILE_CHECK; 

  //FIXME the TLB programming requires exactly 5 register writings. If something
  //      fails here we could have a serious problem
  uint32_t rv;
  rv = hn_write_register(tile, HN_MANGO_TILEREG_TLB, word_0);
  if (rv != HN_SUCCEEDED)
    return rv;

  rv = hn_write_register(tile, HN_MANGO_TILEREG_TLB, word_1);
  if (rv != HN_SUCCEEDED)
    return rv;

  rv = hn_write_register(tile, HN_MANGO_TILEREG_TLB, word_2);
  if (rv != HN_SUCCEEDED)
    return rv;

  rv = hn_write_register(tile, HN_MANGO_TILEREG_TLB, word_3);
  if (rv != HN_SUCCEEDED)
    return rv;

  rv = hn_write_register(tile, HN_MANGO_TILEREG_TLB, word_4);
  if (rv != HN_SUCCEEDED)
    return rv;

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

// ---------------------------------------------------------------------------------------------
// Functions to boot the system and units
// Not all These functions can be accessed from the client application
//   hn_boot_unit    <- This function can be accessed from the client application
// -------------------------------------------------------------------------------------------
// FIXME change parameters after addr to varadic parameters
uint32_t hn_boot_unit(uint32_t tile, uint32_t tile_memory, uint32_t addr,
          const char *protocol_img_path, const char *kernel_img_path)                               
{
  HN_INITIALIZE_CHECK;

  uint32_t rv;

  log_debug("hn_boot_unit: tile %d, tile_memory %d, addr 0x%08x", tile, tile_memory, addr);
 
  HN_TILE_CHECK;


  rv = HN_TILE_TYPE_NOT_RECOGNIZED;

  hn_tile_info_t tile_info;
  uint32_t rc = hn_get_tile_info(tile, &tile_info);
  if (rc == HN_SUCCEEDED) {
    switch (tile_info.unit_family) {
      case HN_TILE_FAMILY_PEAK :
        rv = hn_peak_boot(tile, tile_memory, addr, protocol_img_path, kernel_img_path);
        break;

      case HN_TILE_FAMILY_NUPLUS :
        //rv = hn_nuplus_boot(tile, tile_memory, addr);
        break;

      case HN_TILE_FAMILY_DCT :
         //rv = hn_dct_boot(tile, tile_memory, addr, vargs);
         break;

      case HN_TILE_FAMILY_TETRAPOD : 
         // rv = hn_tetrapod_boot(tile, tile_memory, addr, vargs);
         break;

      default : return HN_TILE_TYPE_NOT_RECOGNIZED;
    }  
  }

  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_boot_peak(uint32_t tile, uint32_t tile_memory, uint32_t addr, const char *protocol_img_path, const char *kernel_img_path)
{ 
  HN_INITIALIZE_CHECK;

  uint32_t  rv; //returned value from load image function
  log_debug("hn_boot_peak: tile %d, tile_memory %d, addr 0x%08x, protocol image file path: %s  kernel image file path: %s",
        tile, tile_memory, addr, protocol_img_path, kernel_img_path);
  HN_TILE_CHECK;

  // The process to boot PEAK is divided in the following steps:
  //   1) Write the PEAKos image into memory (provided by addr parameter)
  //   2) Set the TLB of the UNIT to correctly match virtual addresses with physical addresses
  //   3) Write the coherence protocol into the unit
  //   4) Set the PEAK unit (core 0) to point to the starting address
  //   5) Unfreeze core 0 in PEAK unit
  
  // Writting PEAKos image, first check that file exists and is readable
  // char peakosfilePath[1024];
  // sprintf(peakosfilePath, "%s/mango-apps/hn-library/peakos.img", getenv("HOME"));
  // rv = hn_write_image_into_memory(peakosfilePath, tile_memory, addr);
  rv = hn_write_image_into_memory(kernel_img_path, tile_memory, addr);
  if (rv != HN_SUCCEEDED)
  {
    if (rv != HN_NOT_INITIALIZED) {
      log_error("peakos image file not found, errno value is: %d", errno);
    }
    return rv;
  }

  // Set TLB. PEAKos runs on Segment 0 virtual address space (0x0XXXXXXX)
  hn_set_tlb(tile, 0, 0x00000000, 0x0FFFFFFF, addr, 1, 0, 0, tile_memory);

  // Writting the coherence protocol into the unit, first check that file exists and is readable
  // char peak_protocolfilePath[1024];
  // sprintf(peak_protocolfilePath, "%s/mango-apps/hn-library/peak_protocol.img", getenv("HOME"));
  // rv = hn_peak_write_protocol(peak_protocolfilePath, tile);
  rv = hn_peak_write_protocol(protocol_img_path, tile);
  if(rv != HN_SUCCEEDED)
  {
    if (rv != HN_NOT_INITIALIZED) {
      log_error("memory coherence protocol image file not found, errno value is: %d", errno);
    }
    return rv;
  }

  // Set PEAK unit (core 0) to point to the starting address (virtual address)
  hn_peak_set_new_pc(tile, 0, 0x00032000);

  // Unfreeze core 0 in PEAK
  hn_peak_unfreeze(tile, 0);

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

// ----------------------------------------------------------------------------------------------
// Functions to access memory
// ----------------------------------------------------------------------------------------------
// Not all functions are accessible from the client side
//   hn_write_memory_byte
//   hn_write_memory_halfword
//   hn_write_memory_word
//   hn_write_memory_block
//   hn_write_memory  (accesible)
//   hn_write_image_into_memory (accesible)
//   hn_load_kernel (accessible)
//   hn_read_memory (accessible)
//   hn_read_memory_word            TODO JM10
//   hn_read_memory_block
//

uint32_t hn_write_memory_byte(uint32_t tile, uint32_t addr, uint32_t data)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_write_memory_byte: tile %3u, addr 0x%08x, data 0x%08x     wmb_%d_0x%08x_0x%08x", tile, addr, data,tile, addr, data);
  HN_TILE_CHECK;

  hn_command_write_memory_byte_t cmd;
  cmd.common.command = HN_COMMAND_WRITE_MEMORY_BYTE;
  cmd.tile           = tile;
  cmd.addr         = addr;
  cmd.data         = data;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_write_memory_halfword(uint32_t tile, uint32_t addr, uint32_t data)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_write_memory_halfword: tile %3u, addr 0x%08x, data 0x%08x", tile, addr, data);
  HN_TILE_CHECK;

  hn_command_write_memory_half_t cmd;
  cmd.common.command = HN_COMMAND_WRITE_MEMORY_HALF;
  cmd.tile           = tile;
  cmd.addr         = addr;
  cmd.data         = data;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_write_memory_word(uint32_t tile, uint32_t addr, uint32_t data)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_write_memory_word: tile %3u, addr 0x%08x, data 0x%08x", tile, addr, data);
  HN_TILE_CHECK;
  
  hn_command_write_memory_word_t cmd;
  cmd.common.command = HN_COMMAND_WRITE_MEMORY_WORD;
  cmd.tile           = tile;
  cmd.addr         = addr;
  cmd.data         = data;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_write_memory_block(uint32_t tile, uint32_t addr, const char *data)
{
  HN_INITIALIZE_CHECK;

  int i;
  
  log_debug("hn_write_memory_block: tile %3u, addr 0x%08x, data <not shown>", tile, addr);
  HN_TILE_CHECK;

  hn_command_write_memory_block_t cmd;
  cmd.common.command = HN_COMMAND_WRITE_MEMORY_BLOCK;
  cmd.tile           = tile;
  cmd.addr         = addr;
  for (i=0;i<64;i++) cmd.data[i] = data[i];

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/
// Send data to fpga per items
uint32_t hn_write_memory_strategy_1(uint32_t tile, uint32_t addr, uint32_t size, const char *data)
{
  log_verbose("hn_write_memory_strategy_1: tile %d, addr %08x, size %d(bytes), data <not shown>", tile, addr, size);

  // We write in blocks of 64 bytes and block-aligned
  uint32_t rv = HN_SUCCEEDED;
  uint32_t curr_addr = addr;
  uint32_t remaining_bytes = size;
  uint32_t ptr = 0;
  char buffer[64];
  char padding[64] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

  while (remaining_bytes != 0) {
    if (remaining_bytes >= 64) {
      memcpy(buffer, &data[ptr], 64);
    } else {
      memcpy(buffer, &data[ptr], remaining_bytes);
      memcpy(&buffer[remaining_bytes], &padding[0], 64-remaining_bytes);
    }

    // We write the block in memory
    rv = hn_write_memory_block(tile, curr_addr, buffer);
    if (rv != HN_SUCCEEDED)
      return rv;

    ptr = ptr + 64;
    if (remaining_bytes >= 64) remaining_bytes -= 64; else remaining_bytes = 0;
    curr_addr = curr_addr + 64;
  }

return HN_SUCCEEDED;
}

/******************************************************************************
**/
// Send data to fpga through shm buffers, requesting a new buffer every time we send data and destroying it at the end of the transfer
uint32_t hn_write_memory_strategy_2(uint32_t tile, uint32_t addr, uint32_t size, const char *data)
{

  log_verbose("hn_write_memory_strategy_2: tile %d, addr %08x, size %d(bytes), data <not shown>", tile, addr, size);

  // changes on the function, I am going to request for an shm buffer, copy data,
  //  program the shm_write function and then release the buffer
  // this is a first approximation 

  uint32_t rv = HN_SUCCEEDED;
  char padding[64] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

  uint32_t   shm_buffer_size;
  void      *shm_buffer_base_addr_ptr;

  void      *transfer_wr_src_addr;
  uint32_t   transfer_wr_dst_tile;
  uint32_t   transfer_wr_dst_addr;
  uint32_t   transfer_wr_is_blocking;
  uint32_t   transfer_wr_size;

  int        needs_padding;
  uint32_t   remaining_bytes;

  // we check the size of the transfer to allocate buffer for the padding
  remaining_bytes = 0;

  if((size %64) == 0) {
    shm_buffer_size = size;
    needs_padding   = 0;
  } else {
    shm_buffer_size = ((size / 64)*64) + 64;
    needs_padding   = 1;
    remaining_bytes = shm_buffer_size - size;
  }

  rv = hn_shm_malloc(shm_buffer_size, &shm_buffer_base_addr_ptr);
  if (rv != 0) {
    log_error("@hn_write_memory allocating memory for shm buffer");
    return rv;
  }

  memcpy(shm_buffer_base_addr_ptr, &data[0], size);
  
  if (needs_padding) {
    memcpy(shm_buffer_base_addr_ptr+size, &padding[0], remaining_bytes);
  }

  transfer_wr_src_addr    = (void *)shm_buffer_base_addr_ptr;
  transfer_wr_size        = shm_buffer_size;
  transfer_wr_dst_addr    = addr; 
  transfer_wr_dst_tile    = tile;
  transfer_wr_is_blocking = 1;
  
  rv = hn_shm_write_memory(transfer_wr_src_addr , transfer_wr_size, transfer_wr_dst_addr, transfer_wr_dst_tile, transfer_wr_is_blocking);
  if (rv != 0) {
    log_error("@hn_write_memory sending data from shm buffer");
    return rv;
  }

  rv = hn_shm_free(shm_buffer_base_addr_ptr);
  if (rv != 0) {
    log_error("@hn_write_memory releasing shm buffer");
    return rv;
  }

 return HN_SUCCEEDED;
}


/******************************************************************************
**/
// Send DUMMY data to fpga through shm buffers, requesting a new buffer every time we send data and destroying it at the end of the transfer
uint32_t hn_write_memory_strategy_2_dummy(uint32_t tile, uint32_t addr, uint32_t size, const char *data)
{
  log_verbose("hn_write_memory_strategy_2: tile %d, addr %08x, size %d(bytes), data <not shown>", tile, addr, size);

  // changes on the function, I am going to request for an shm buffer, 
  //  program the shm_write function and then release the buffer
  //  we will send dummy data !!!! 

  uint32_t   rv = HN_SUCCEEDED;
  uint32_t   shm_buffer_size;
  void      *shm_buffer_base_addr_ptr;
  void      *transfer_wr_src_addr;
  uint32_t   transfer_wr_dst_tile;
  uint32_t   transfer_wr_dst_addr;
  uint32_t   transfer_wr_is_blocking;
  uint32_t   transfer_wr_size;


  // we check the size of the transfer to allocate buffer for the padding

  if((size %64) == 0) {
    shm_buffer_size = size;
  } else {
    shm_buffer_size = ((size / 64)*64) + 64;
  }

  rv = hn_shm_malloc(shm_buffer_size, &shm_buffer_base_addr_ptr);
  if (rv != 0) {
    log_error("@hn_write_memory allocating memory for shm buffer");
    return rv;
  }


  transfer_wr_src_addr    = (void *)shm_buffer_base_addr_ptr;
  transfer_wr_size        = shm_buffer_size;
  transfer_wr_dst_addr    = addr; 
  transfer_wr_dst_tile    = tile;
  transfer_wr_is_blocking = 1;
  
  rv = hn_shm_write_memory(transfer_wr_src_addr , transfer_wr_size, transfer_wr_dst_addr, transfer_wr_dst_tile, transfer_wr_is_blocking);
  if (rv != 0) {
    log_error("@hn_write_memory sending data from shm buffer");
    return rv;
  }

  rv = hn_shm_free(shm_buffer_base_addr_ptr);
  if (rv != 0) {
    log_error("@hn_write_memory releasing shm buffer");
    return rv;
  }

 return HN_SUCCEEDED;
}


/******************************************************************************
**/
// Send data to fpga through shm buffers, using an existing buffer in shm area, if data does not fit in this buffer, we will call strategy_2, to create a new buffer for this specific operation
uint32_t hn_write_memory_strategy_3(uint32_t tile, uint32_t addr, uint32_t size, const char *data)
{
  log_verbose("hn_write_memory_strategy_3: tile %d, addr %08x, size %d(bytes), data <not shown>", tile, addr, size);

  // changes on the function, I am going to request for an shm buffer, copy data,
  //  program the shm_write function and then release the buffer
  // this is a first approximation 

  uint32_t rv = HN_SUCCEEDED;
  char padding[64] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

  uint32_t   transfer_size;
  uint32_t   transfer_is_blocking = 1;
  int        needs_padding;
  uint32_t   remaining_bytes;

  // let's check if the data fits into the already created shm_buffer
  if(size < HN_STRATEGIC_OPERATION_MODE_1_SIZE_THRESHOLD) {
    rv = hn_write_memory_strategy_1(tile, addr, size, data);
    return rv;
  }else if (size > HN_STRATEGIC_OPERATION_SHM_BUFFER_SIZE) {
    rv = hn_write_memory_strategy_2(tile, addr, size, data);
    return rv;
  }
  // else, the data will fit (even if we had to add padding) in the local buffer

  // we check the size of the transfer to allocate buffer for the padding

  if((size %64) == 0) {
    needs_padding   = 0;
    transfer_size   = size;
    remaining_bytes = 0;
  } else
  {
    needs_padding   = 1;
    transfer_size = ((size / 64)*64) + 64;
    remaining_bytes = transfer_size - size;
  }

  memcpy(hn_shm_buffer_strategic_write, &data[0], size);
  
  if (needs_padding) {
    memcpy(hn_shm_buffer_strategic_write+size, &padding[0], remaining_bytes);
  }

  rv = hn_shm_write_memory(hn_shm_buffer_strategic_write , transfer_size, addr, tile, transfer_is_blocking);
  if (rv != 0) {
    log_error("@hn_write_memory sending data from shm buffer");
    return rv;
  }

 return HN_SUCCEEDED;
}

/******************************************************************************
**/
// Send data to fpga through shm buffers, using an existing buffer in shm area, if data does not fit in this buffer, we will call strategy_2, to create a new buffer for this specific operation
uint32_t hn_write_memory_strategy_3_dummy(uint32_t tile, uint32_t addr, uint32_t size, const char *data)
{
  log_verbose("hn_write_memory_strategy_3: tile %d, addr %08x, size %d(bytes), data <not shown>", tile, addr, size);

  // changes on the function, I am going to request for an shm buffer, copy data,
  //  program the shm_write function and then release the buffer
  // this is a first approximation 

  uint32_t rv = HN_SUCCEEDED;

  uint32_t   transfer_size;
  uint32_t   transfer_is_blocking = 1;

  // let's check if the data fits into the already created shm_buffer
  if (size > HN_STRATEGIC_OPERATION_SHM_BUFFER_SIZE) {
    rv = hn_write_memory_strategy_2_dummy(tile, addr, size, data);
    return rv;
  }
  // else, the data will fit (even if we had to add padding) in the local buffer

  // we check the size of the transfer to allocate buffer for the padding
  if((size %64) == 0) {
    transfer_size   = size;
  } else {
    transfer_size = ((size / 64)*64) + 64;
  }

  rv = hn_shm_write_memory(hn_shm_buffer_strategic_write , transfer_size, addr, tile, transfer_is_blocking);
  if (rv != 0) {
    log_error("@hn_write_memory sending data from shm buffer");
    return rv;
  }

 return HN_SUCCEEDED;
}

/******************************************************************************
**/

uint32_t hn_write_memory(uint32_t tile, uint32_t addr, uint32_t size, const char *data)
{
  uint32_t rv;
  
  HN_INITIALIZE_CHECK;
  log_debug("hn_write_memory: tile %d, addr %08x, size %d(bytes), data <not shown>", tile, addr, size);
  HN_TILE_CHECK;

  rv = 1;

  // there are three stategies currently available: 1-2-3
#if  defined(HN_STRATEGIC_WRITE_MODE_1)
  rv = hn_write_memory_strategy_1(tile, addr, size, data);

#elif  defined(HN_STRATEGIC_WRITE_MODE_2)
  rv = hn_write_memory_strategy_2(tile, addr, size, data);

#elif  defined(HN_STRATEGIC_WRITE_DUMMYDATA_MODE_2)
  rv = hn_write_memory_strategy_2_dummy(tile, addr, size, data);

#elif  defined(HN_STRATEGIC_WRITE_MODE_3)
  rv = hn_write_memory_strategy_3(tile, addr, size, data);

#elif  defined (HN_STRATEGIC_WRITE_DUMMYDATA_MODE_3)
  rv = hn_write_memory_strategy_3_dummy(tile, addr, size, data);

#else 
  rv = hn_write_memory_strategy_1(tile, addr, size, data);

#endif
  
  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_write_image_into_memory(const char *file_name, uint32_t tile, uint32_t addr_offset)
{
  HN_INITIALIZE_CHECK;
  setlocale(LC_ALL, ""); // to allow digit grouping in printf, using user selected locale
  
  struct timespec    t_start;
  struct timespec    t_end;
  struct timespec    t_ps;
  struct timespec    t_pe;
  uint64_t           total_p_elapsed_ns  = 0;// time elapsed in nanoseconds
  //uint64_t           total_elapsed_ns    = 0;  // time elapsed in nanoseconds


  log_debug("hn_write_image_into_memory: file_name %s, tile %d, addr %d", file_name, tile, addr_offset);
  HN_TILE_CHECK;

  // We open the file 
  FILE *fd = fopen(file_name, "r");
  if (fd == NULL) return HN_IMAGE_FILE_NOT_FOUND;

  // We read the file in a loop and write on the memory. Each iteration writes a block of 64 bytes
  unsigned int   fline_index;
  uint32_t       curr_addr = addr_offset;
  char           buffer[BLOCK_MEMORY_SIZE_BYTES];
  char           flinebuff[FILE_LINE_MAX_LENGTH];
  size_t         flen;
  ssize_t        read_bytes;
  char          *fline = NULL;
  unsigned long int address_in_line; // address contained in current line of processed file
  uint32_t       rv = HN_SUCCEEDED;

  clock_gettime(CLOCK_MONOTONIC, &t_start);

  fline_index = 0;
  while ((read_bytes = getline(&fline, &flen, fd)) != -1) {
    ssize_t ss, ss_new, read_bytes;
    read_bytes = strlen(fline);
    // clean line, remove unexpected chars, blanks, ...
    ss = 0;
    ss_new = 0;
    for(; (ss < read_bytes) && (ss < (FILE_LINE_MAX_LENGTH - 1)); ss++) {
      if(   ((fline[ss] >= '0') && (fline[ss] <= '9'))
          ||((fline[ss] >= 'a') && (fline[ss] <= 'f'))
          ||((fline[ss] >= 'A') && (fline[ss] <= 'F'))
          ||( fline[ss] == 'x') ||( fline[ss] == 'X')
          ||( fline[ss] == ',')
        ) {
        flinebuff[ss_new] = fline[ss];
        ss_new++;
      } else if ((fline[ss] == 10) || (fline[ss] == 32)) {
      } else {
        log_warn("unexpected char (\"%c\") in line %u,%zu", fline[ss], (unsigned int)fline_index, ss);
      }
    }

    if(ss_new > 0) {
      flinebuff[ss_new ] = 0;
      log_verbose("processing file line l.%u (%ld bytes): %s",fline_index, strlen(flinebuff), flinebuff);

      if(    (ss_new == 141) && (flinebuff[10] == ',')
          && ( flinebuff[0] == '0') && (( flinebuff[1] == 'x')  || ( flinebuff[1] == 'X'))
          && (flinebuff[11] == '0') && ((flinebuff[12] == 'x')  || (flinebuff[12] == 'X'))) {
        uint8_t   addr_in_line[8];

        memcpy(addr_in_line, &flinebuff[2], 8);
        address_in_line = hn_utils_hex_addr_to_ul(addr_in_line);
        curr_addr       = (uint32_t)address_in_line + addr_offset;

        int i;
        for(i = 0; i < BLOCK_MEMORY_SIZE_BYTES; i++) {
          uint8_t dh, dl, vh, vl, value;
          dh = flinebuff[13 + 2*i] & 0xFF;
          dl = flinebuff[13 + 2*i+1] & 0xFF;
          vh = hn_utils_hex_to_val(dh) & 0xFF;
          vl = hn_utils_hex_to_val(dl) & 0xFF;
          value = ((vh * 16) & 0xFF) + (vl & 0xFF);
          buffer[i] = (char)value & 0xFF;
        }
        clock_gettime(CLOCK_MONOTONIC, &t_ps);
        rv = hn_write_memory(tile, curr_addr, BLOCK_MEMORY_SIZE_BYTES, buffer);
        clock_gettime(CLOCK_MONOTONIC, &t_pe);
        total_p_elapsed_ns = total_p_elapsed_ns + (as_nanoseconds(&t_pe) - as_nanoseconds(&t_ps));
        if (rv != HN_SUCCEEDED)
          return rv;
      } else {
        log_error("line not processed (%zu), unexpected line format:\n%s", ss_new, fline);
      }
    } else {
      log_error("hn_write_image_into_memory:unexpected line #%u length %ld bytes\n\n",fline_index, read_bytes);
    }
     fline_index++;
  }

  clock_gettime(CLOCK_MONOTONIC, &t_end);
  //total_elapsed_ns = as_nanoseconds(&t_end) - as_nanoseconds(&t_start);
  fclose(fd);

  log_debug("hn_write_image_into_memory: file_name %s, took   total: %'lu ns    write_block operations: %'lu", file_name, (as_nanoseconds(&t_end) - as_nanoseconds(&t_start)), total_p_elapsed_ns);

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_load_kernel(uint32_t tile, uint32_t addr, uint32_t kernel_size, char *kernel_image)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_load_kernel: tile %d, addr %x, kernel_size %d", tile, addr, kernel_size);
  HN_TILE_CHECK;

  return hn_write_memory(tile, addr, kernel_size, kernel_image);
}
/**************************************************************************************************
***************************************************************************************************
**/
//Get data from fpga per blocks by items
uint32_t hn_read_memory_strategy_1 (uint32_t tile, uint32_t addr, uint32_t size, char *data)
{
  uint32_t i;
  uint32_t rv;

  //log_verbose("hn_read_memory_strategy_1: tile %d, addr %x, size %d", tile, addr, size);

  uint32_t num_blocks = size / 64;
  uint32_t pading =size%64;
  uint32_t current_addr = addr;
  uint32_t pointer = 0;
  char     buffer[64] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

  if (pading != 0) {
    num_blocks++;
  }
  for (i=0;i<num_blocks;i++) {
    if ((i==num_blocks-1) && (pading!=0)) {
      hn_read_memory_block(tile, current_addr, buffer);
      memcpy(&data[pointer], buffer, pading);
    } else {

      rv = hn_read_memory_block(tile, current_addr, &data[pointer]);
      if (rv != HN_SUCCEEDED)
        return rv;
    }
    pointer += 64;
    current_addr += 64;
  }

 return HN_SUCCEEDED;

}
/******************************************************************************
**/
// get data from fpga through shm buffers, requesting a new buffer every time we send data and destroying it at the end of the transfer
uint32_t hn_read_memory_strategy_2 (uint32_t tile, uint32_t addr, uint32_t size, char *data)
{
  uint32_t rv;
  uint32_t   shm_buffer_size;

  void      *transfer_rd_dst_addr; // address of shared memory buffer to store data read from DDR memory in HN system
  uint32_t   transfer_rd_src_tile; // tile where DDR is located
  uint32_t   transfer_rd_src_addr; // address to read from DDR memory
  uint32_t   transfer_rd_is_blocking; 
  uint32_t   transfer_rd_size;     // number of bytes to read from DDR memory

  uint32_t num_blocks = size / 64;
  uint32_t padding    =size % 64;
  
  if (padding != 0) {
    num_blocks++;
  }
  
  shm_buffer_size = num_blocks * 64;

  rv = hn_shm_malloc(shm_buffer_size, &transfer_rd_dst_addr);
  if (rv != 0) {
    log_error("@hn_read_memory allocating memory for shm buffer");
    return rv;
  }

  //recovering data from fpga
  transfer_rd_size        = shm_buffer_size;
  transfer_rd_src_addr    = addr;
  transfer_rd_src_tile    = tile;
  transfer_rd_is_blocking = 1;

  rv = hn_shm_read_memory(transfer_rd_dst_addr, transfer_rd_size, transfer_rd_src_addr, transfer_rd_src_tile, transfer_rd_is_blocking);

  if (rv != 0) {
    log_error("@hn_read_memory getting data from shm buffer");
    return rv;
  }

  memcpy(&data[0], transfer_rd_dst_addr, size);
  
  rv = hn_shm_free(transfer_rd_dst_addr);
  if (rv != 0) {
    log_error("@hn_read_memory releasing shm buffer");
    return rv;
  }

  return HN_SUCCEEDED;
}
/******************************************************************************
**/
// get data from fpga through shm buffers, using an existing buffer in shm area, if data does not fit in this buffer, we will call strategy_2, to create a new buffer for this specific operation
uint32_t hn_read_memory_strategy_3(uint32_t tile, uint32_t addr, uint32_t size, char *data)
{
  uint32_t rv;
  uint32_t   num_blocks;
  uint32_t   transfer_size;
  uint32_t   padding;
  uint32_t   transfer_rd_is_blocking; 

  //log_verbose("hn_read_memory_strategy_3: tile %d, addr %08x, size %d(bytes), data <not shown>", tile, addr, size);
  // let's check if the data fits into the already created shm_buffer
  if (size > HN_STRATEGIC_OPERATION_SHM_BUFFER_SIZE) {
    rv = hn_read_memory_strategy_2(tile, addr, size, data);
    return rv;
  }
  // else, the data will fit in the local buffer

  num_blocks    = size / 64;
  padding       = size % 64;

  if (padding != 0){
    num_blocks++;
  }
  
  transfer_size = num_blocks*64;

  //recovering data from fpga
  transfer_rd_is_blocking = 1;

  rv = hn_shm_read_memory(hn_shm_buffer_strategic_read, transfer_size, addr, tile, transfer_rd_is_blocking);

  if (rv != 0) {
    log_error("@hn_read_memory getting data from shm buffer");
    return rv;
  }

  memcpy(&data[0], hn_shm_buffer_strategic_read, size);
  
  return HN_SUCCEEDED;
}

/******************************************************************************
**/
// 
uint32_t hn_read_memory(uint32_t tile, uint32_t addr, uint32_t size, char *data)
{
  uint32_t rv;

  log_debug("hn_read_memory: tile %d, addr %x, size %d", tile, addr, size);

  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;
  // We perform block-aligned accesses to memory
  if (addr % 64) return HN_UNALIGNED_READ;
  rv = 1;
 
  // there are three stategies currently available: 1-2-3

#if   defined(HN_STRATEGIC_READ_MODE_1)
  rv = hn_read_memory_strategy_1(tile, addr, size, data);

#elif defined(HN_STRATEGIC_READ_MODE_2)
  rv = hn_read_memory_strategy_2(tile, addr, size, data);

#elif defined(HN_STRATEGIC_READ_MODE_3)
  rv = hn_read_memory_strategy_3(tile, addr, size, data);

#else 
  rv = hn_read_memory_strategy_1(tile, addr, size, data);

#endif
  return rv;

}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_read_memory_block(uint32_t tile, uint32_t addr, char *data)
{
  HN_INITIALIZE_CHECK;

  int wc, rc;

  log_debug("hn_read_memory_block: tile %d, addr %x", tile, addr);
  HN_TILE_CHECK;

  hn_command_read_memory_block_t cmd;
  cmd.common.command = HN_COMMAND_READ_MEMORY_BLOCK;
  cmd.tile           = tile;
  cmd.addr           = addr;

  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  if (data != NULL) {
    rc = hn_read_socket(data, 64);
    if (rc != HN_SUCCEEDED) {
      return rc;
    }
  }
  
  log_verbose("hn_read_memory: tile %d, addr %x, size 64 bytes, content %08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x", tile, addr,  
      htonl(*((uint32_t *)data)),    htonl(*((uint32_t *)data+1)),  htonl(*((uint32_t *)data+2)),  htonl(*((uint32_t *)data+3)),  htonl(*((uint32_t *)data+4)), htonl(*((uint32_t *)data+5)), 
      htonl(*((uint32_t *)data+6)),  htonl(*((uint32_t *)data+7)),  htonl(*((uint32_t *)data+8)),  htonl(*((uint32_t *)data+9)),  htonl(*((uint32_t *)data+10)), 
      htonl(*((uint32_t *)data+11)), htonl(*((uint32_t *)data+12)), htonl(*((uint32_t *)data+13)), htonl(*((uint32_t *)data+14)), htonl(*((uint32_t *)data+15)));

  return HN_SUCCEEDED;



/*
  uint32_t   size = 64;
  uint32_t   rv;


  uint32_t   shm_buffer_size;
  void      *shm_buffer_base_addr_ptr;

  void      *transfer_rd_dst_addr; // address of shared memory buffer to store data read from DDR memory in HN system
  uint32_t   transfer_rd_src_tile; // tile where DDR is located
  uint32_t   transfer_rd_src_addr; // address to read from DDR memory
  uint32_t   transfer_rd_is_blocking; 
  uint32_t   transfer_rd_size;     // number of bytes to read from DDR memory

  uint32_t num_blocks = 1;//size / 64;
  
  shm_buffer_size = num_blocks * 64;

  //log_warn("JM10, hn_read_block: request shm_buffer of %ubytes \n",shm_buffer_size);


  rv = hn_shm_malloc(shm_buffer_size, &shm_buffer_base_addr_ptr);
  if (rv != 0) {
    log_error("@hn_read_block allocating memory for shm buffer");
    return rv;
  }

  //recovering data from fpga
  transfer_rd_dst_addr    = (void *)shm_buffer_base_addr_ptr;
  transfer_rd_size        = shm_buffer_size;
  transfer_rd_src_addr    = addr;
  transfer_rd_src_tile    = tile;
  transfer_rd_is_blocking = 1;

  rv = hn_shm_read_memory(transfer_rd_dst_addr, transfer_rd_size, transfer_rd_src_addr, transfer_rd_src_tile, transfer_rd_is_blocking);

  if (rv != 0) {
    log_error("@hn_read_block getting data from shm buffer");
    return rv;
  }

  //log_warn("JM10, hn_read_block: data read to shm-buffer. Copying data to array\n");
  //fflush(stdout);

  memcpy(&data[0], shm_buffer_base_addr_ptr, size);
  

  rv = hn_shm_free(shm_buffer_base_addr_ptr);
  if (rv != 0) {
    log_error("@hn_read_memory releasing shm buffer");
    return rv;
  }

  //printf("JM10,@hn_read_block , all done returning\n");
  //fflush(stdout);

  return rv;
*/

}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_read_memory_word(uint32_t tile, uint32_t addr, char *data)
{
  HN_INITIALIZE_CHECK;

  int wc, rc;

  log_debug("hn_read_memory_word: tile %d, addr %x", tile, addr);
  HN_TILE_CHECK;

  hn_command_read_memory_block_t cmd;
  cmd.common.command = HN_COMMAND_READ_MEMORY_WORD;
  cmd.tile           = tile;
  cmd.addr         = addr;

  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  if (data != NULL) {
    rc = hn_read_socket(data, 4);
    if (rc != HN_SUCCEEDED) {
      return rc;
    }
  }
  
  log_verbose("hn_read_memory_word: tile %d, addr %x, size 4 bytes, content %08x", tile, addr,  htonl(*((uint32_t *)data)));

  return HN_SUCCEEDED;
}

uint32_t hn_run_kernel(uint32_t tile, uint32_t addr, char *args)
{
  HN_INITIALIZE_CHECK;

  uint32_t rv = HN_TILE_TYPE_NOT_RECOGNIZED;

  log_debug("hn_run_kernel: tile %d", tile);

  hn_tile_info_t tile_info;
  uint32_t rc = hn_get_tile_info(tile, &tile_info);
  if (rc == HN_SUCCEEDED) {
    switch (tile_info.unit_family) {
      case HN_TILE_FAMILY_PEAK :
        rv = hn_peak_run_kernel(tile, addr, args);
                       break;

      case HN_TILE_FAMILY_NUPLUS :
        rv = hn_nuplus_run_kernel(tile, addr, args); 
        break;

      case HN_TILE_FAMILY_DCT :
        rv = hn_dct_run_kernel(tile);
        break;

      case HN_TILE_FAMILY_TETRAPOD : 
        break;

      default : 
        return HN_TILE_TYPE_NOT_RECOGNIZED;
  }
  }  

  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/


// --------------------------------------------------------------------------------------------------------------------
// Functions to reset and receive raw items
// --------------------------------------------------------------------------------------------------------------------

uint32_t hn_reset(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_reset_t cmd;
  cmd.common.command = HN_COMMAND_RESET;
  cmd.tile           = tile;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  log_info("   reset will take 6 seconds");
  sleep(6);

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_receive_item(uint32_t *item)
{
  return hn_read_socket(item, sizeof(*item));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_receive_items(uint32_t *item, uint32_t num_items, uint32_t *read_items)
{
  uint32_t read_bytes = 0;
  uint32_t ret_code = hn_read_socket_best_effort(item, sizeof(*item)*num_items, &read_bytes);
  *read_items = read_bytes / sizeof(*item);

  return ret_code;
}
/**************************************************************************************************
***************************************************************************************************
**/

// --------------------------------------------------------------------------------------------------------------------
// Functions to enable debuging
// --------------------------------------------------------------------------------------------------------------------

uint32_t hn_debug_memory_controller_enable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_MC_DEBUG_ENABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_memory_controller_disable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_MC_DEBUG_DISABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_inject_enable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_INJECT_DEBUG_ENABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_inject_disable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_INJECT_DEBUG_DISABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_eject_enable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_EJECT_DEBUG_ENABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_eject_disable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_EJECT_DEBUG_DISABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_unit2mango_enable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_U2M_DEBUG_ENABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_unit2mango_disable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_U2M_DEBUG_DISABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_mango2unit_enable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_M2U_DEBUG_ENABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_mango2unit_disable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_M2U_DEBUG_DISABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_tilereg_enable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_TILEREG_DEBUG_ENABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_debug_tilereg_disable(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_configure_debug_and_stats_t cmd;
  cmd.common.command  = HN_COMMAND_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile            = tile;
  cmd.config_function = HN_MANGO_ITEM_COMMAND_FUNCTION_TILEREG_DEBUG_DISABLE;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_stop_clk(uint32_t tile)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_clock_t cmd;
  cmd.common.command  = HN_COMMAND_CLOCK;
  cmd.tile            = tile;
  cmd.clock_function  = HN_MANGO_ITEM_COMMAND_FUNCTION_PAUSE_CLOCK;
  cmd.num_cycles      = 0;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_resume_clk(uint32_t tile) 
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_clock_t cmd;
  cmd.common.command  = HN_COMMAND_CLOCK;
  cmd.tile            = tile;
  cmd.clock_function  = HN_MANGO_ITEM_COMMAND_FUNCTION_RESUME_CLOCK;
  cmd.num_cycles      = 0;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_ticks(uint32_t tile, uint32_t value)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t wc;
  hn_command_clock_t cmd;
  cmd.common.command  = HN_COMMAND_CLOCK;
  cmd.tile            = tile;
  cmd.clock_function  = HN_MANGO_ITEM_COMMAND_FUNCTION_RUNFOR_CLOCK;
  cmd.num_cycles      = value; 
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

void hn_print_error(uint32_t error_code)
{
  switch (error_code) {
    case HN_SUCCEEDED                    : printf("HN: No error\n"); break;
    case HN_DEVICE_NOT_FOUND             : printf("HN: Device not found\n"); break;
    case HN_NOT_INITIALIZED              : printf("HN: HN not initialized\n"); break;
    case HN_TILE_DOES_NOT_EXIST          : printf("HN: Tile does not exist\n"); break;
    case HN_PARTITION_NOT_FOUND          : printf("HN: Partition not found\n"); break;
    case HN_PARTITION_NOT_DEFINED        : printf("HN: Partition not defined\n"); break;
    case HN_ALTERNATIVE_PARTITION_NOT_FOUND: printf("HN: Alternative partition not found\n"); break;
    case HN_MEMORY_NOT_PRESENT_IN_TILE   : printf("HN: Memory not present in tile\n"); break;
    case HN_REGISTER_DOES_NOT_EXIST      : printf("HN: Register does not exist\n"); break;
    case HN_PEAK_NOT_FOUND_IN_TILE       : printf("HN: PEAK unit not found in tile\n"); break;
    case HN_NUPLUS_NOT_FOUND_IN_TILE     : printf("HN: NUPLUS unit not found in tile\n"); break;
    case HN_ACCELERATOR_NOT_FOUND_IN_TILE: printf("HN: Accelerator unit not found in tile\n"); break;
    case HN_SYNCH_RESOURCE_DOES_NOT_EXIST: printf("HN: SYNCH resource not found in tile\n"); break;
    case HN_WRITE_ONLY_REGISTER          : printf("HN: Write only register\n"); break;
    case HN_READ_ONLY_REGISTER           : printf("HN: Read only register\n"); break;
    case HN_WRONG_BANDWIDTH_SETTING      : printf("HN: Wrong bandwidth setting\n"); break;
    case HN_ARCHITECTURE_NOT_FOUND       : printf("HN: Architecture not found\n"); break;
    case HN_TILE_TYPE_NOT_RECOGNIZED     : printf("HN: Tile type not recognized\n"); break;
    case HN_UNALIGNED_READ               : printf("HN: Unaligned read\n"); break;
    case HN_IMAGE_FILE_NOT_FOUND         : printf("HN: Image file not found\n"); break;
    case HN_PROTOCOL_FILE_NOT_FOUND      : printf("HN: Protocol file not found\n"); break;
    case HN_WRONG_REGISTER_READ          : printf("HN: Wrong register read\n"); break;
    case HN_SOCKET_ERROR                 : printf("HN: Socket error\n"); break;
    case HN_FIND_MEMORY_ERROR            : printf("HN: Find memory error\n"); break;
    case HN_RSC_ERROR                    : printf("HN: Resource manager error\n"); break;
    case HN_DMA_ERROR                    : printf("HN: DMA operation error\n"); break;
    case HN_TEMPERATURE_ERROR            : printf("HN: temperature operation error\n"); break;
    case HN_MEMORY_ALLOCATION_ERROR      : printf("HN: memory allocation error\n"); break;
    case HN_TILE_ID_OUT_OF_RANGE         : printf("HN:Tile ID out of range error\n"); break;
                                           
  }
}

// Pending (to discuss):
//   uint32_t hn_wait_for_synch(uint32_t sync_rsc, uint32_t value);                                                 // waits for a synchronization
//   uint32_t hn_write_on_synch(uint32_t sync_rsc, uint32_t value);                                                 // writes on a synchronization resource
//   uint32_t hn_set_routing_bits(uint32_t tile, uint32_t bits);                                                    // sets routing bits for a given tile
//   uint32_t hn_set_connectivity_bits(uint32_t tile, uint32_t bits);                                               // sets connectivity bits for a given tile

/**************************************************************************************************
***************************************************************************************************
**/



// -----------------------------------------------------------------------------------------------
// Functions for semaphores implementations
// FIXME move this set of functions to the resource manager module
//
//   hn_get_synch_id
//   hn_get_synch_id_array
//   hn_release_synch_id
//   hn_release_synch_id_array
//   hn_read_synch_register
//   hn_write_synch_register

uint32_t hn_get_synch_id(uint32_t *id, uint32_t tile, uint32_t type)
{
  HN_INITIALIZE_CHECK;

  return hn_get_synch_id_array(id, 1, 3 /*tile*/, type);
}

/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_get_synch_id_array(uint32_t *id, uint32_t n, uint32_t tile, uint32_t type)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t r_ini;
  uint32_t r_end;
  uint32_t i;
  uint32_t t;
  uint32_t r;
  uint32_t pending_regs;
  uint32_t first_reg;

// MANGO TILEREG has 128 addressable regs
//    2  for regular rd/wr
//   56  for regular wr - reset read
//   56  for inc     wr - reset read
  if (n==0) return HN_WRONG_ARGUMENTS;

  // We split in types of registers
  if (type == HN_READRESET_REG_TYPE) {
    r_ini = HN_READRESET_REG_TYPE_LOW_ID;
    r_end = HN_READRESET_REG_TYPE_HIGH_ID;
  } else if (type == HN_READRESET_INCRWRITE_REG_TYPE) {
    r_ini = HN_READRESET_INCRWRITE_REG_TYPE_LOW_ID;
    r_end = HN_READRESET_INCRWRITE_REG_TYPE_HIGH_ID;
  } else if (type == HN_REGULAR_REG_TYPE) {
    r_ini = HN_REGULAR_REG_TYPE_LOW_ID;
    r_end = HN_REGULAR_REG_TYPE_HIGH_ID;
  } else {
    log_error("@hn_get_synch_id_array, wrong register type");
    return HN_WRONG_REGISTER_TYPE;
  }

  // We get the number of tiles from the resource manager
  uint32_t num_tiles = 0;
  uint32_t num_tiles_x = 0;
  uint32_t num_tiles_y = 0;
  if (hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y) != HN_SUCCEEDED)
    return HN_RSC_ERROR;

  // We search from the requested tile and search in cyclic way, all requested regs must be consecutive
  for (i=0; i<num_tiles; i++)
  {
    t = (tile + i) % num_tiles;
    pending_regs = n;
    first_reg = r_ini;
    for (r=r_ini; r<=r_end; r++)
    {
      if (register_free[t][r])
      {
        pending_regs--;
      }
      else
      {
        first_reg = r+1;
        pending_regs = n;
      }
      if (pending_regs == 0) break;
    }
    if (pending_regs==0)
    {
      for (r=first_reg;r<first_reg+n;r++)
      {
        register_free[t][r] = 0;
      }
      // create the id, move the tile id as many bits as necessary to leave room to add the first_req id , and then move
      // *id = tile_id   first_req_id  ??
      //        X bits,  7 bits        2 bits
      *id = ((t << 7) + first_reg) << 2;
      
      return HN_SUCCEEDED;
    }
  }
  
  return HN_NO_FREE_REGISTERS_AVAILABLE;
}
    
      

/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_release_synch_id(uint32_t id)
{
  HN_INITIALIZE_CHECK;

  return hn_release_synch_id_array(id, 1);
}

/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_release_synch_id_array(uint32_t id, uint32_t n)
{
  HN_INITIALIZE_CHECK;

  uint32_t r;
  uint32_t tile = id >> 9;
  uint32_t reg  = (id >> 2) & 0x0000007f;

  // at this point we should check tile id is in range !!!!

  for (r=0;r<n;r++) register_free[tile][reg+r] = 1;
  
  return HN_SUCCEEDED;
}

/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_read_synch_register(uint32_t id, uint32_t *v)
{
  HN_INITIALIZE_CHECK;

  uint32_t rv;
  uint32_t tile   = id >> 9;
  uint32_t reg_id = (id >> 2) & 0x0000007f;

  rv = hn_read_register(tile, reg_id, v);

  //log_warn("JM10, hn_read_synch_register id 0x%08x   tile %2u   reg_id (%3u) 0x%04X   value %04u   rv = %u\n", id, tile, reg_id, reg_id, *v, rv);
  
  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_write_synch_register(uint32_t id, uint32_t v)
{
  HN_INITIALIZE_CHECK;

  uint32_t rv;
  uint32_t tile   =  id >> 9;
  uint32_t reg_id =  (id >> 2)  & 0x0000007f;

  //log_warn("JM10,hn_write_synch_register id 0x%08x   tile %2u   reg_id (%3u) 0x%04X   value %04u\n",id, tile, reg_id, reg_id, v);

  rv = hn_write_register(tile, reg_id, v);

  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/

// ------------------------------------------------------------------------------------------------
// Data burst transfers between FPGA DDR <-> Host Application
//
// hn_shm_malloc
// hn_shm_free
// hn_shm_write_memory
// hn_shm_read_memory

// Compose message for the hn_daemon, the hn_daemon will create the buffer in SHM region, 
//   on success, the hn_daemon will notify this function the name for the created shm buffer 
uint32_t hn_shm_malloc(uint32_t size, void **ptr)
{
  HN_INITIALIZE_CHECK;
  uint32_t               rv;
  hn_shm_buffer_info_t  *buffer_info;
  int                    rb;
  int                    rd;  
  void                  *shared_memory_address;
  int                    shared_memory_fd;
  hn_command_shm_buffer_create_t          cmd_create;
  hn_command_shm_buffer_destroy_t         cmd_destroy;
  hn_command_shm_buffer_manage_status_t   cmd_status;

  
  log_debug("hn_shm_malloc function request for new shm buffer allocation of %u bytes", size);

  // allocate memory for local information, to add to list of shared memory buffers
  //  the first, to abort the operation in case the malloc fails
  buffer_info = (hn_shm_buffer_info_t *)malloc(sizeof(hn_shm_buffer_info_t));
  if (buffer_info == NULL)
  {
    // error, return error
    log_error("allocating memory for new shm_buffer information");
    return 1;
  }
  
  // send request to hn_daemon to create new buffer in shared memory region
  cmd_create.info.command = HN_COMMAND_CREATE_SHARED_MEMORY_BUFFER;
  cmd_create.size         = size;
  rv = hn_write_socket(&cmd_create, sizeof(hn_command_shm_buffer_create_t));
  if (rv != 0)
  {
    *ptr = NULL;
    free(buffer_info);
    return rv; 
  }
  // wait for answer of creation process from hn_daemon
  rv = hn_read_socket(&cmd_status, sizeof(hn_command_shm_buffer_manage_status_t));
  if (rv != 0)
  {
    if(rv != HN_NOT_INITIALIZED) {
      log_error("hn_shm_malloc, reading from socket with daemon, returned %u", rv);
    }
    *ptr = NULL;
    free(buffer_info);
    return rv; 
  }
  // read from socket succeeded, check buffer creation operation status
  if(cmd_status.status != 0)
  {
    log_error("hn_shm_malloc, shm malloc failed on daemon side, returned status %d",cmd_status.status );
    *ptr = NULL;
    free(buffer_info);
    return cmd_status.status; 
  }

  // shm buffer succesfully created by daemon. open shm buffer at this side
  rv = hn_shared_memory_open_buffer(cmd_status.name, size, &shared_memory_address, &shared_memory_fd);
  if (rv != 0)
  {
    *ptr = NULL;
    if(rv != HN_NOT_INITIALIZED) {
      log_error("error opening shm_buffer on hn side after daemon succesfully created it, notify daemon to destroy buffer %s", cmd_status.name);
    }
    hn_shm_free(shared_memory_address);
    free(buffer_info);
    // 
    // nofity daemon to destroy buffer...
    cmd_destroy.info.command = HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER;
    cmd_destroy.size = size;
    strcpy(cmd_destroy.name, cmd_status.name);
    rd = hn_write_socket(&cmd_destroy, sizeof(cmd_destroy));
    if (rd != 0)
    {
      // Error writing socket to release shm buffer !!
      if (rd != HN_NOT_INITIALIZED) {
        log_error("writing on socket to destroy shared memory buffer on daemon side");
      }
    }
    return rv; 
  }
 
  // buffer succesfully openend, add buffer information to list of shm_buffer list
  strcpy(buffer_info->name, cmd_status.name);
  buffer_info->addr  = shared_memory_address;
  buffer_info->size  = size;
  buffer_info->fd    = shared_memory_fd;

  log_debug("shm_malloc, buffer succesfully opened on application side  %s    fd: %d    ptr: 0x%08X    size: %u bytes",
      buffer_info->name, buffer_info->fd, buffer_info->addr , buffer_info->size);
  rb = hn_list_add_to_end( hn_shm_buffer_info_list, buffer_info);

  // check whether buffer info was succesfully added to list
  if (rb != 0)
  {
    // failed adding buffer to list, close buffer and remove local info. Notify the daemon to destroy the buffer and release resources
    // error
    *ptr = NULL;
    
    // destroy local resources information
    hn_shared_memory_close_buffer(buffer_info->size, buffer_info->addr, buffer_info->fd);

    if(rv != HN_NOT_INITIALIZED) {
    log_error("adding shm_buffer info to list in hn");
    }
    // 
    // nofity daemon to destroy buffer...
     cmd_destroy.info.command = HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER;
     cmd_destroy.size = size;
     strcpy(cmd_destroy.name, cmd_status.name);
     rd = hn_write_socket(&cmd_destroy, sizeof(cmd_destroy));
     if (rd != 0)
     {
       // Error writing socket to release shm buffer !!
       if(rd != HN_NOT_INITIALIZED) {
         log_error("writing on socket to destroy shared memory buffer on daemon side");
       }
     }

     free(buffer_info);

     return rb;
  }

  // update pointer to shm buffer, return status OK
  *ptr = shared_memory_address;
  
  return rv;
}

/**************************************************************************************************
***************************************************************************************************
**/
uint32_t hn_shm_free(void *ptr)
{
  HN_INITIALIZE_CHECK;
  int                   iter;
  hn_shm_buffer_info_t *shm_buffer;
  int                   rd;
  uint32_t              rdu;
  int                   buffer_found; // set to 1 if the buffer is found
  int                   buffer_iter;  // id value of the requested shm_buffer
  hn_command_shm_buffer_destroy_t         cmd_destroy;
  hn_command_shm_buffer_manage_status_t   cmd_destroy_status;

  // search buffer in list of shared memory buffers
  // close buffer
  // remove entry from list
  // notify hn_daemon to destroy buffer (close buffer, release memory, remove info from its list) 

  log_debug("hn_shm_free, request to close shm buffer associated to addr: 0x%08X", ptr);
  // search buffer in list of shared memory buffers
  buffer_found   = 0;
  //buffer_iter    = -1;
  iter           = hn_list_iter_front_begin(hn_shm_buffer_info_list);
  while((!hn_list_iter_end(hn_shm_buffer_info_list, iter)))
  {
    shm_buffer = (hn_shm_buffer_info_t *)hn_list_get_data_at_position(hn_shm_buffer_info_list , iter);
    log_debug("iter: %d    iter->addr: 0x%08X", iter, shm_buffer->addr);

    if ((shm_buffer != NULL) )
    {
      // replace by address range instead of base address
      if ((shm_buffer->addr == ptr))
      {
        log_debug("  match found");
        // we found it
        // close buffer on this side
        rdu = hn_shared_memory_close_buffer(shm_buffer->size, shm_buffer->addr, shm_buffer->fd);
        if (rdu != 0)
        {
          log_error("closing shared memory buffer on hn_shm_free request");
          log_error("Continue releasing operations, will let buffer useless");
        }
        buffer_found = 1;
        buffer_iter  = iter;

        // set iterator to end of the list to relesase semaphores
        hn_list_iter_end(hn_shm_buffer_info_list, HN_LIST_ITER_END);

        break;
      }
    }
    
    iter = hn_list_iter_next(hn_shm_buffer_info_list, iter) ;
  };

  if (buffer_found != 0)
  {
    log_verbose("shm_buffer to destroy found");
    //remove entry from list
    hn_list_remove_element_at(hn_shm_buffer_info_list, buffer_iter);
  }
  else
  {
    log_error("hn_shm_free, no buffer info found for address");
    return 1;
  }
  // all done at this side (application -- hn_lib)

  // nofity daemon to destroy buffer...
  // TO-DO call destroy buffer function, instead of this simplified version,
  cmd_destroy.info.command = HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER;
  cmd_destroy.size         = shm_buffer->size;
  strcpy(cmd_destroy.name, shm_buffer->name);

  // do not longer any the shm_buffer information, we can now release memory of buffer info
  free (shm_buffer);
  
  log_debug("notifying daemon to destroy buffer  %s    size: %u", cmd_destroy.name, cmd_destroy.size);

  rd = hn_write_socket(&cmd_destroy, sizeof(cmd_destroy));
  if (rd != 0)
  {
    // Error writing socket to release shm buffer !!
    if(rd != HN_NOT_INITIALIZED) {
      log_error("writing on socket to destroy shared memory buffer on daemon side");
    }
    // ?? return rb;
    return rd;
  }
  
  //wait ack from daemon side....
  rd = hn_read_socket(&cmd_destroy_status, sizeof(cmd_destroy_status));
  if (rd != 0)
  {
    if(rd != HN_NOT_INITIALIZED) {
    // Error writing socket to release shm buffer !!
      log_error("reading status from socket to destroy shared memory buffer on daemon side");
    }
    return rd;
  }

  if(cmd_destroy_status.status != 0)
  {
    log_error("destroying shared memory buffer at daemon side");
    return cmd_destroy_status.status;
  }

  log_debug("shm_buffer destroyed %s and resources succesfully released", cmd_destroy_status.name);

  // all done at the daemon side  
  return ((uint32_t)rd + rdu);
}

/**************************************************************************************************
***************************************************************************************************
**/
uint32_t hn_shm_transfer(void *ptr, uint32_t size, uint32_t address_dst, uint32_t tile_dst, uint32_t blocking_transfer, hn_command_type_t shm_operation_type)
{
  HN_INITIALIZE_CHECK;
  uint32_t tile = tile_dst;
  HN_TILE_CHECK;

  int                   rd;
  int                   iter;
  hn_shm_buffer_info_t *shm_buffer;
  int                   buffer_found; // set to 1 if the buffer is found
  //int                   buffer_iter;  // id value of the requested shm_buffer
  // we use char *, to ensure compatibility of arithmetic operations over address pointers, pointers arithmetic is not supported by standard c (gcc does)
  //  so we use char* since  sizeof(char *) = 1byte, and the size of the shm buffer is also stored in bytes
  char                 *shm_addr_base;
  char                 *shm_addr_end;
  char                 *shm_addr_req;
  uint32_t              shm_transfer_offset; // address offset in bytes!!!!!
  hn_command_shm_buffer_transfer_t      cmd_transfer;
  hn_command_shm_buffer_manage_status_t cmd_transfer_status;

  // search buffer associated to received ptr addr
  // search buffer in list of shared memory buffers
  // set parameters in command
  // notify hn_daemon to send data from buffer  

  log_debug("hn_shm_transfer, requests to operate on shm_buffer addr: 0x%08X", ptr);
  // search buffer in list of shared memory buffers
  buffer_found   = 0;
  iter           = hn_list_iter_front_begin(hn_shm_buffer_info_list);
  shm_addr_req   = (char *)ptr;
  while((!hn_list_iter_end(hn_shm_buffer_info_list, iter)))
  {
    shm_buffer = (hn_shm_buffer_info_t *)hn_list_get_data_at_position(hn_shm_buffer_info_list , iter);
    log_debug("iter: %d    iter->addr: 0x%08X", iter, shm_buffer->addr);

    if ((shm_buffer != NULL) )
    {

      shm_addr_base = (char *) (shm_buffer->addr);
      shm_addr_end  = (char *) (shm_addr_base + (shm_buffer->size));
      // replace by address range instead of base address
      if ((shm_addr_base <= shm_addr_req) && (shm_addr_end >= shm_addr_req) )
      {
        log_debug("  shm buffer match found for shm region %s", shm_buffer->name);
        // we found it
        // check requested buffer transfer is inside buffer memory space
        if((shm_addr_req + size) > shm_addr_end)
        {
          log_error("  request shm memory operation out of assigned bounds");
          log_error("  request transfer base addr %p", shm_addr_req);
          log_error("          transfer size %u bytes", size);
          log_error("  buffer is [%p, %p]", shm_addr_base, shm_addr_end);
          return 1;
        }
        buffer_found        = 1;
        //buffer_iter         = iter;
        shm_transfer_offset   = shm_addr_req - shm_addr_base;
        
        // set iterator to end of the list to relesase semaphores
        hn_list_iter_end(hn_shm_buffer_info_list, HN_LIST_ITER_END);

        break;
      }
    }
    
    iter = hn_list_iter_next(hn_shm_buffer_info_list, iter);
  };

  if (buffer_found == 0)
  {
    log_error("hn_smh_transfer, no buffer info found for requested ptr");
    return 1;
  }
  
  // all done at this side (application -- hn_lib)
  log_debug("shm_buffer for transfer found, address offset %u (char * address),  compose message for daemon....delete-me", shm_transfer_offset);

  cmd_transfer.info.command = shm_operation_type;
  strcpy((cmd_transfer.name), shm_buffer->name);
  cmd_transfer.offset   = shm_transfer_offset;
  cmd_transfer.tile     = tile_dst;
  cmd_transfer.addr     = address_dst;
  cmd_transfer.size     = size;
  cmd_transfer.blocking = blocking_transfer;

  rd = hn_write_socket(&cmd_transfer, sizeof(cmd_transfer));
 
  log_debug("transfer cmd sent to daemon");
  if (rd != 0)
  {
    if(rd != HN_NOT_INITIALIZED) {
      log_error("writing on socket to start shm transfer with HN system");
    }
    return rd;
  }

  //wait ack from daemon side....
  // For NON-blocking transfers, it will respond inmediately after processing the command
  // For     blocking tranfsers, it will wait until transfer is finished and later it will send the response to the application
  log_debug("Waiting transfer ack from daemon, transfer blocking: %s", (blocking_transfer == 0)?"NO":"YES");
  rd = hn_read_socket(&cmd_transfer_status, sizeof(cmd_transfer_status));
  if (rd != 0)
  {
    // Error writing socket to release shm buffer !!
    log_error("reading status from socket to send content of shared memory buffer on daemon side");
    return rd;
  }

  if(cmd_transfer_status.status != HN_SHM_OPERATION_OK)
  {
    log_error("shared memory transfer failed at daemon side");
    return cmd_transfer_status.status;
  }

  log_debug("shm_buffer %s command processed, return", cmd_transfer_status.name);

  return 0;
}
/**************************************************************************************************
***************************************************************************************************
**/
uint32_t hn_shm_write_memory(void *ptr, uint32_t size, uint32_t address_dst, uint32_t tile_dst, uint32_t blocking_transfer)
{
  HN_INITIALIZE_CHECK;
  uint32_t tile = tile_dst;
  HN_TILE_CHECK;

  uint32_t rv;

  log_debug("hn_shmwrite_memory, request to send data from shm_buffer addr: 0x%08X", ptr);
  rv = hn_shm_transfer(ptr, size, address_dst, tile_dst, blocking_transfer, HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER);
  return rv;
 }

/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_shm_read_memory(void *ptr, uint32_t size, uint32_t address_src, uint32_t tile_src, uint32_t blocking_transfer)
{
  HN_INITIALIZE_CHECK;
  uint32_t tile = tile_src;
  HN_TILE_CHECK;

  uint32_t rv;

  log_debug("hn_shm_read_memory, request to send to read data and put on shm_buffer addr: 0x%08X", ptr);
  rv = hn_shm_transfer(ptr, size, address_src, tile_src, blocking_transfer, HN_COMMAND_READ_SHARED_MEMORY_BUFFER);

  return rv;
}
/**************************************************************************************************
***************************************************************************************************
**/

// ------------------------------------------------------------------------------------------------
// DMA transfer functions (FPGA DDR -> UNIT)
//
// hn_register_dma_operation
// hn_dma_to_unit
// hn_dma_to_mem
// hn_release_dma_operation
// hn_get_dma_status
//

// hn_register_dma_operation. Returns an id that identifies a DMA operation
uint32_t hn_register_dma_operation(uint32_t *id) {
  HN_INITIALIZE_CHECK;

  // we send a register dma request to the daemon, the daemon will send back an id to be used
  uint32_t wc;
  hn_command_dma_t cmd;
  cmd.common.command = HN_COMMAND_REGISTER_DMA;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return HN_RSC_ERROR;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  *id = data.id;
  return HN_SUCCEEDED;
}

// hn_dma_to_unit. Programs a DMA transfer from a DDR memory in HN to a UNIT (accelerator). If blocking_transfer is set
// the function returns once the DMA is finished from the DMA device located in tile_src. If not, the DMA command is sent
// and the function returns
uint32_t hn_dma_to_unit(uint32_t id, uint32_t addr_src, uint32_t tile_src, uint32_t size, uint32_t tile_dst, uint32_t blocking_transfer) {
  HN_INITIALIZE_CHECK;
  uint32_t tile;
  tile = tile_src;
  HN_TILE_CHECK;
  tile = tile_dst;
  HN_TILE_CHECK;

  // we send a trigger dma request to the daemon
  uint32_t wc;
  hn_command_dma_t cmd;
  cmd.common.command = HN_COMMAND_TRIGGER_DMA;
  cmd.id           = id;
  cmd.tile_src     = tile_src;
  cmd.addr_src     = addr_src;
  cmd.size         = size;
  cmd.tile_dst     = tile_dst;
  cmd.to_unit      = 1;
  cmd.to_mem       = 0;
  cmd.to_ext       = 0;
  cmd.notify       = blocking_transfer;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // We wait now even if we didn't put the blocking_transfer mode
  // If not set, the daemon will reply with error or ok and in blocking
  // mode the daemon will reply once operation is completed
  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return HN_RSC_ERROR;
  }

  if (blocking_transfer) {
    if (data.result == HN_RSC_DMA_FINISHED) return HN_SUCCEEDED;
    else return HN_DMA_ERROR;
  }
  if (data.result == HN_RSC_ERROR) return HN_DMA_ERROR;

  return HN_SUCCEEDED;
}

// hn_dma_to_mem. Programs a DMA transfer from a DDR memory in HN to another (or the same) DDR memory in HN. If blocking_transfer is set, the function
// returns once the DMA is finished from the DMA device located in tile_src. If not, the DMA command is sent and the function returns
uint32_t hn_dma_to_mem(uint32_t id, uint32_t addr_src, uint32_t tile_src, uint32_t size, uint32_t addr_dst, uint32_t tile_dst, uint32_t blocking_transfer) {
  HN_INITIALIZE_CHECK;
  uint32_t tile;
  tile = tile_src;
  HN_TILE_CHECK;
  tile = tile_dst;
  HN_TILE_CHECK;

  // we send a trigger dma request to the daemon
  uint32_t wc;
  hn_command_dma_t cmd;
  cmd.common.command = HN_COMMAND_TRIGGER_DMA;
  cmd.id           = id;
  cmd.tile_src     = tile_src;
  cmd.addr_src     = addr_src;
  cmd.size         = size;
  cmd.tile_dst     = tile_dst;
  cmd.addr_dst     = addr_dst;
  cmd.to_unit      = 0;
  cmd.to_mem       = 1;
  cmd.to_ext       = 0;
  cmd.notify       = blocking_transfer;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // We wait now even if we didn't put the blocking_transfer mode
  // If not set, the daemon will reply with error or ok and in blocking
  // mode the daemon will reply once operation is completed
  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return HN_RSC_ERROR;
  }

  if (blocking_transfer) {
    if (data.result == HN_RSC_DMA_FINISHED) return HN_SUCCEEDED;
    else return HN_DMA_ERROR;
  }
  if (data.result == HN_RSC_ERROR) return HN_DMA_ERROR;

  return HN_SUCCEEDED;
}

// hn_dma_to_mem. Programs a DMA transfer from a DDR memory in HN to an EXTernal IO device. If blocking_transfer is set, the function
// returns once the DMA is finished from the DMA device located in tile_src. If not, the DMA command is sent and the function returns
uint32_t hn_dma_to_ext(uint32_t id, uint32_t addr_src, uint32_t tile_src, uint32_t size, uint32_t tile_dst, uint32_t blocking_transfer) {
  HN_INITIALIZE_CHECK;
  uint32_t tile;
  tile = tile_src;
  HN_TILE_CHECK;
  tile = tile_dst;
  HN_TILE_CHECK;

  // we send a trigger dma request to the daemon
  uint32_t wc;
  hn_command_dma_t cmd;
  cmd.common.command = HN_COMMAND_TRIGGER_DMA;
  cmd.id           = id;
  cmd.tile_src     = tile_src;
  cmd.addr_src     = addr_src;
  cmd.size         = size;
  cmd.tile_dst     = tile_dst;
  cmd.addr_dst     = 0;
  cmd.to_unit      = 0;
  cmd.to_mem       = 0;
  cmd.to_ext       = 1;
  cmd.notify       = blocking_transfer;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // We wait now even if we didn't put the blocking_transfer mode
  // If not set, the daemon will reply with error or ok and in blocking
  // mode the daemon will reply once operation is completed
  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return HN_RSC_ERROR;
  }

  if (blocking_transfer) {
    if (data.result == HN_RSC_DMA_FINISHED) return HN_SUCCEEDED;
    else return HN_DMA_ERROR;
  }
  if (data.result == HN_RSC_ERROR) return HN_DMA_ERROR;

  return HN_SUCCEEDED;
}

// hn_release_dma_operation. Releases a DMA operation
uint32_t hn_release_dma_operation(uint32_t id) {
  HN_INITIALIZE_CHECK;

  // we send a release dma request to the daemon
  uint32_t wc;
  hn_command_dma_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_DMA;
  cmd.id           = id;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return HN_RSC_ERROR;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }
  return HN_SUCCEEDED;
} 

// hn_get_dma_status. Retrieves the status (e.g. NO-INITIATED, RUNNING, FINISHED) of a DMA operation
uint32_t hn_get_dma_status(uint32_t id, uint32_t *status) {
  HN_INITIALIZE_CHECK;

  // we send a get status dma request to the daemon
  uint32_t wc;
  hn_command_dma_t cmd;
  cmd.common.command = HN_COMMAND_GET_STATUS_DMA;
  cmd.id           = id;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // we wait till notification from daemon
  uint32_t rc;
  rc = hn_read_socket(&status, sizeof(uint32_t));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  return 0;
}

// -------------------------------------------------------------------------------------------------
// Interrupt support functions
//
// hn_register_int
// hn_interrupt
// hn_wait_int
// hn_release_int
//

// hn_register_int. Registrs an interrupt service for a given tile (the interrupt will be used for the accelerator located in that tile)
// The vector mask indicates which interrupt bits (out of 32) will be monitored
uint32_t hn_register_int(uint32_t tile, uint16_t vector_mask, uint32_t *id) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a register_int request to the daemon, the daemon will send back an id to be used
  uint32_t wc;
  hn_command_int_t cmd;
  cmd.common.command = HN_COMMAND_REGISTER_INTS;
  cmd.tile           = tile;
  cmd.vector_mask  = vector_mask;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return HN_RSC_ERROR;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  *id = data.id;
  return HN_SUCCEEDED;
}

// hn_interrupts. Triggers an interrupt to the unit located with the associated tile with the id
// The vector argument indicates which interrupt bits are set
uint32_t hn_interrupt(uint32_t id, uint16_t vector) {
  HN_INITIALIZE_CHECK;

  // we send a int request to the daemon
  uint32_t wc;
  hn_command_int_t cmd;
  cmd.common.command = HN_COMMAND_TRIGGER_INTS;
  cmd.id           = id;
  cmd.vector_mask  = vector;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
 
  return HN_SUCCEEDED;
}

// hn_wait_int. Returns when an interrupt is received by the UNIT located in the tile associated with the id
// and with the corresponding vector bits in vector_mask
uint32_t hn_wait_int(uint32_t id, uint16_t vector_mask) {
  HN_INITIALIZE_CHECK;

  // we send a wait_int request to the daemon, the daemon will send back a confirmation once the int is received
  uint32_t wc;
  hn_command_int_t cmd;
  cmd.common.command = HN_COMMAND_WAIT_INTS;
  cmd.id           = id;
  cmd.vector_mask  = vector_mask;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // we wait till we get anything from the daemon
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
 
  if (data.result != HN_RSC_INT_RECEIVED)
    return HN_RSC_ERROR;
 
  return HN_SUCCEEDED;
}

// hn_release_int. Releases an interrupt service
uint32_t hn_release_int(uint32_t id) {
  HN_INITIALIZE_CHECK;

  // we send a release_int request to the daemon
  uint32_t wc;
  hn_command_int_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_INTS;
  cmd.id           = id;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return HN_RSC_ERROR;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  return HN_SUCCEEDED;
}


// --------------------------------------------------------------------------------------------------
// Resource allocation (memories, units and bandwidth) functions
//
// hn_lock_resources_access
// hn_unlock_resources_access
// hn_find_memory
// hn_find_memories
// hn_allocate_memory
// hn_release_memory
// hn_get_available_network_bandwidth
// hn_get_available_read_memory_bandwidth
// hn_get_available_write_memory_bandwidth
// hn_get_available_read_cluster_bandwidth
// hn_get_available_write_cluster_bandwidth
// hn_reserve_network_bandwidth
// hn_reserve_read_memory_bandwidth
// hn_reserve_write_memory_bandwidth
// hn_reserve_read_cluster_bandwidth
// hn_reserve_write_cluster_bandwidth
// hn_release_network_bandwidth
// hn_release_read_memory_bandwidth
// hn_release_write_memory_bandwidth
// hn_release_read_cluster_bandwidth
// hn_release_write_cluster_bandwidth
// hn_find_uints_set
// hn_find_units_sets
// hn_reserve_unit_set
// hn_release_unit_set
//

// hn_lock_resources_access. Locks the access to resources
uint32_t hn_lock_resources_access() {
  HN_INITIALIZE_CHECK;

  // we send the request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_LOCK_RESOURCES_ACCESS_RSC;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) return wc;

  // we wait the daemon reply
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if (data.result != HN_RSC_OK) return HN_RSC_ERROR;
  return HN_SUCCEEDED;
}

// hn_unlock_resources_access. Unlocks the access to resources
uint32_t hn_unlock_resources_access() {
  HN_INITIALIZE_CHECK;

  // we send the request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_UNLOCK_RESOURCES_ACCESS_RSC;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) return wc;

  // we wait the daemon reply
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if (data.result != HN_RSC_OK) return HN_RSC_ERROR;
  return HN_SUCCEEDED;
}

// hn_find_memory. Returns the closest memory location and address available to a tile
uint32_t hn_find_memory(uint32_t tile, unsigned long long size, uint32_t *tile_mem, uint32_t *starting_addr) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_FIND_MEMORY_RSC;
  cmd.tile_mem     = tile;
  cmd.size         = size;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with requested info
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if (data.result != HN_RSC_FOUND) {
    return HN_FIND_MEMORY_ERROR;
  } 

  *tile_mem      = data.tile_mem;
  *starting_addr = data.starting_addr;

  return HN_SUCCEEDED;
}

// hn_find_memories. Returns all the tiles with memories (and addresses) where the request fits
uint32_t hn_find_memories(uint32_t size, uint32_t *tiles_mem, uint32_t *starting_addr, uint32_t *num_memories) {
  HN_INITIALIZE_CHECK;

  int i;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_FIND_MEMORIES_RSC;
  cmd.size         = size;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with requested info
  // TODO: Daemon has to send to the application a clear struct with all needed info (the same way we do to send info to daemon)
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if (data.result != HN_RSC_FOUND) {
    *num_memories = 0;
    return HN_FIND_MEMORY_ERROR;
  }

  *num_memories = data.num_memories;
  tiles_mem = malloc(data.num_memories*sizeof(uint32_t));
  starting_addr = malloc(data.num_memories*sizeof(uint32_t));
  for (i=0;i < data.num_memories; i++) {
    tiles_mem[i]     = 0;
    starting_addr[i] = 0;
    rc = hn_read_socket(&data, sizeof(data));
    if (rc != HN_SUCCEEDED) return rc;
    tiles_mem[i]     = data.tile_mem;
    starting_addr[i] = data.starting_addr;
  }
  return HN_SUCCEEDED;
}

// hn_allocate_memory. Allocates memory segment
uint32_t hn_allocate_memory(uint32_t tile, uint32_t addr, uint32_t size) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_ALLOCATE_MEMORY_RSC;
  cmd.tile_mem     = tile;
  cmd.addr         = addr;
  cmd.size         = size;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  return HN_SUCCEEDED;
}

// hn_release_memory. Frees memory segment
uint32_t hn_release_memory(uint32_t tile, uint32_t addr, uint32_t size) {
  HN_INITIALIZE_CHECK;

  //log_warn("JM10, @hn.c, hn_release_memory for tile %u   addr %u   size %u\n\n", tile, addr, size);
  
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_MEMORY_RSC;
  cmd.tile_mem     = tile;
  cmd.addr         = addr;
  cmd.size         = size;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with ok or failed notification
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  return HN_SUCCEEDED;
}

// hn_get_available_network_bandwidth. Returns the available bandwidth for paths
uint32_t hn_get_available_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long *bw) {
  HN_INITIALIZE_CHECK;
  uint32_t tile;
  tile = tile_src;
  HN_TILE_CHECK;
  tile = tile_dst;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_NETWORK_BANDWIDTH_RSC;
  cmd.tile_src     = tile_src;
  cmd.tile_dst     = tile_dst;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies 
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  *bw = data.bw;
  return HN_SUCCEEDED;
}

// hn_get_available_read_memory_bandwidth. Returns the available read memory bandwidth
uint32_t hn_get_available_read_memory_bandwidth(uint32_t tile, unsigned long long *bw) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_READ_MEMORY_BANDWIDTH_RSC;
  cmd.tile_mem     = tile;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  *bw = data.bw;
  return HN_SUCCEEDED;
}

// hn_get_available_write_memory_bandwidth. Returns the available write memory bandwidth
uint32_t hn_get_available_write_memory_bandwidth(uint32_t tile, unsigned long long *bw) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_WRITE_MEMORY_BANDWIDTH_RSC;
  cmd.tile_mem     = tile;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  *bw = data.bw;
  return HN_SUCCEEDED;
}

// hn_get_available_read_cluster_bandwidth. Returns the available read cluster bandwidth
uint32_t hn_get_available_read_cluster_bandwidth(unsigned long long *bw) {
  HN_INITIALIZE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_READ_CLUSTER_BANDWIDTH_RSC;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  *bw = data.bw;
  return HN_SUCCEEDED;
}

// hn_get_available_write_cluster_bandwdith. Returns the available write cluster bandwidth
uint32_t hn_get_available_write_cluster_bandwidth(unsigned long long *bw) {
  HN_INITIALIZE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_GET_WRITE_CLUSTER_BANDWIDTH_RSC;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  *bw = data.bw;
  return HN_SUCCEEDED;
}

// hn_reserve_network_bandwidth. Reserves (decrements) bandwidth for a path
uint32_t hn_reserve_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw) {
  HN_INITIALIZE_CHECK;
  uint32_t tile;
  tile = tile_src;
  HN_TILE_CHECK;
  tile = tile_dst;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RESERVE_NETWORK_BANDWIDTH_RSC;
  cmd.tile_src    = tile_src;
  cmd.tile_dst    = tile_dst;
  cmd.bw          = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_reserve_read_memory_bandwidth. Reserves (decrements) read memory bandwidth
uint32_t hn_reserve_read_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RESERVE_READ_MEMORY_BANDWIDTH_RSC;
  cmd.tile_mem     = tile;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_reserve_write_memory_bandwidth. Reserves (decrements) write memory bandwidth
uint32_t hn_reserve_write_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RESERVE_WRITE_MEMORY_BANDWIDTH_RSC;
  cmd.tile_mem     = tile;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_reserve_read_cluster_bandwidth. Reserves (decrements) read cluster bandwidth
uint32_t hn_reserve_read_cluster_bandwidth(unsigned long long bw) {
  HN_INITIALIZE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RESERVE_READ_CLUSTER_BANDWIDTH_RSC;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_reserve_write_cluster_bandwidth. Reserves (decrements) write cluster bandwidth
uint32_t hn_reserve_write_cluster_bandwidth(unsigned long long bw) {
  HN_INITIALIZE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RESERVE_WRITE_CLUSTER_BANDWIDTH_RSC;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_release_network_bandwidth. Releases (increments) bandwidth for a path
uint32_t hn_release_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw) {
  HN_INITIALIZE_CHECK;
  uint32_t tile;
  tile = tile_src;
  HN_TILE_CHECK;
  tile = tile_dst;
  HN_TILE_CHECK;
  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_NETWORK_BANDWIDTH_RSC;
  cmd.tile_src     = tile_src;
  cmd.tile_dst     = tile_dst;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_release_read_memory_bandwidth. Releases (increments) read memory bandwidth
uint32_t hn_release_read_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_READ_MEMORY_BANDWIDTH_RSC;
  cmd.tile_mem     = tile;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_release_write_memory_bandwidth. Releases (increments) write memory bandwidth
uint32_t hn_release_write_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_WRITE_MEMORY_BANDWIDTH_RSC;
  cmd.tile_mem     = tile;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_release_read_cluster_bandwidth. Releases (increments) read cluster bandwidth
uint32_t hn_release_read_cluster_bandwidth(unsigned long long bw) {
  HN_INITIALIZE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_READ_CLUSTER_BANDWIDTH_RSC;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_release_write_cluster_bandwidth. Releases (increments) write clustr bandwidth
uint32_t hn_release_write_cluster_bandwidth(unsigned long long bw) {
  HN_INITIALIZE_CHECK;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_RELEASE_WRITE_CLUSTER_BANDWIDTH_RSC;
  cmd.bw           = bw;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait till daemon replies with succeed or failed
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  return HN_SUCCEEDED;
}

// hn_find_units_set. Returns a set of tiles according to the requested types and number of tiles
uint32_t hn_find_units_set(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t *tiles_dst, uint32_t *types_dst) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  if (num_tiles == 0) return HN_RSC_ERROR;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_FIND_UNITS_SET_RSC;
  cmd.tile           = tile;
  cmd.num_tiles    = num_tiles;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // we send also the types
  int i;
  for (i=0;i<num_tiles;i++) {
    wc = hn_write_socket(&types[i], sizeof(uint32_t));
    if (wc != HN_SUCCEEDED) return wc;
  }

  // we wait till daemon replies with data
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES))
    return HN_RSC_ERROR;

  // we copy the data into the parameters
  for (i=0;i<num_tiles;i++) {
    rc = hn_read_socket(&data, sizeof(data));
    if (rc != HN_SUCCEEDED) return rc;
    tiles_dst[i] = data.tile;
    types_dst[i] = data.type;
  }

  return HN_SUCCEEDED;
}

// hn_find_units_sets. Returns all possible (disjoint) set of tiles according to the request types nd number of tiles
uint32_t hn_find_units_sets(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t ***tiles_dst, uint32_t ***types_dst, uint32_t *num) {
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  if (num_tiles == 0) {
    *num = 0;
    return HN_RSC_ERROR;
  }

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command = HN_COMMAND_FIND_UNITS_SETS_RSC;
  cmd.tile           = tile;
  cmd.num_tiles    = num_tiles;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // after the command we inject into the socket the list of tile types
  int i;
  for (i=0;i<num_tiles;i++) {
    wc = hn_write_socket(&types[i], sizeof(uint32_t));
    if (wc != HN_SUCCEEDED) {
      return wc;
    }
  }

  // we wait till daemon replies with data
  hn_data_rsc_t data;
  uint32_t rc;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
  uint32_t num_sets = data.num_sets;
  
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    *num = 0;
    return HN_RSC_ERROR;
  }

  // we copy the data into the parameters, creating memory
  if (num_sets > 0) {
    *tiles_dst = malloc(num_sets*sizeof(uint32_t *));
    *types_dst = malloc(num_sets*sizeof(uint32_t *));
    uint32_t i;
    uint32_t j;
    for (i=0;i<num_sets;i++) {
      (*tiles_dst)[i] = malloc(num_tiles*sizeof(uint32_t));
      (*types_dst)[i] = malloc(num_tiles*sizeof(uint32_t));
      for (j=0;j<num_tiles;j++) {
      rc = hn_read_socket(&data, sizeof(data));
      if (rc != HN_SUCCEEDED) return rc;
        (*tiles_dst)[i][j] = data.tile;
        (*types_dst)[i][j] = data.type;
      }
    }
  }
  *num = num_sets;

  return HN_SUCCEEDED;
}

// hn_reserve_unit_set. Reserves a set of tiles
uint32_t hn_reserve_units_set(uint32_t num_tiles, uint32_t *tiles) {
  HN_INITIALIZE_CHECK;

  if (num_tiles == 0) return HN_SUCCEEDED;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command   = HN_COMMAND_RESERVE_UNITS_SET_RSC;
  cmd.num_tiles    = num_tiles;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
  // we send also the tiles
  int i;
  for (i=0;i<num_tiles;i++) {
    wc = hn_write_socket(&tiles[i], sizeof(uint32_t));
    if (wc != HN_SUCCEEDED) return wc;
  }

  // we wait ok or error from server
  uint32_t rc;
  hn_data_rsc_t data;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  // We return SUCCEEDED or ERROR depending on the reply
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  return HN_SUCCEEDED;
}

// hn_release_unit_set. Releases a set of tiles
uint32_t hn_release_units_set(uint32_t num_tiles, uint32_t *tiles) {
  HN_INITIALIZE_CHECK;

  if (num_tiles == 0) return HN_SUCCEEDED;

  // we send a resource request to the daemon
  uint32_t wc;
  hn_command_rsc_t cmd;
  cmd.common.command   = HN_COMMAND_RELEASE_UNITS_SET_RSC;
  cmd.num_tiles    = num_tiles;
  wc = hn_write_socket(&cmd, sizeof(cmd));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we send also the tiles
  int i;
  for (i=0;i<num_tiles;i++) {
    wc = hn_write_socket(&tiles[i], sizeof(uint32_t));
    if (wc != HN_SUCCEEDED) return wc;
  }

  // we wait ok or error from server
  uint32_t rc;
  hn_data_rsc_t data;
  rc = hn_read_socket(&data, sizeof(data));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  // We return SUCCEEDED or ERROR depending on the reply
  if ((data.result != HN_RSC_FOUND) && (data.result != HN_RSC_OPERATION_SUCCES)) {
    return HN_RSC_ERROR;
  }

  return HN_SUCCEEDED;
}

// --------------------------------------------------------------------------------------------------------------------
// Units Statistics functions
//
// hn_stats_monitor_configure_tile
// hn_stats_monitor_set_polling_period
// hn_stats_monitor_read
// enable/disable statistics monitor for a specific tile.

//---------------------------------------------------------------------------------------------------------------------
uint32_t hn_stats_monitor_configure_tile(uint32_t tile, uint32_t stats_enable)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t  wc;
  uint32_t  rc;
  hn_command_stats_monitor_t          cmd;
  hn_command_stats_monitor_response_t response;

  // prepare command, send tile to enable/disable for stats automatic monitorization
  // tile ID exists in architecture will be checked in hn_daemon
  cmd.common.command = (stats_enable == 0) ? HN_COMMAND_CONFIGURE_STATS_MONITOR_UNSET_TILE : HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_TILE;
  cmd.tile           = tile;

  wc = hn_write_socket(&cmd, sizeof(hn_command_stats_monitor_t));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }
 
  // we wait response from daemon 
  rc = hn_read_socket(&response, sizeof(hn_command_stats_monitor_response_t));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
  // Command succesfully triggered, return response of command, if any error ocurred during the process it will be returned  
  return response.status;
}

//---------------------------------------------------------------------------------------------------------------------
// stats for all tiles will be automatically updated for the period specified when calling hn_stats_monitor_configure_period
uint32_t hn_stats_monitor_set_polling_period(uint32_t polling_period)
{
  HN_INITIALIZE_CHECK;

  uint32_t  wc;
  uint32_t  rc;
  hn_command_stats_monitor_t          cmd;
  hn_command_stats_monitor_response_t response;

  // prepare command, send polling period 
  // tile ID exists in architecture will be checked in hn_daemon
  cmd.common.command = HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_PERIOD;
  cmd.polling_period = polling_period;
  wc = hn_write_socket(&cmd, sizeof(hn_command_stats_monitor_t));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait response from daemon 
  rc = hn_read_socket(&response, sizeof(hn_command_stats_monitor_response_t));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }
 
  if ((response.status != HN_STATS_MONITOR_OPERATION_OK)) {
    // error setting tile for automatic monitorization
    return response.status;
  }
  
  // Command succesfully triggered, polling period has succesfully been updated 
  return HN_SUCCEEDED;
}
//---------------------------------------------------------------------------------------------------------------------
uint32_t hn_stats_monitor_read(uint32_t tile, uint32_t *cores_in_tile,  hn_stats_monitor_t **stats)
{
  HN_INITIALIZE_CHECK;
  HN_TILE_CHECK;

  uint32_t  wc;
  uint32_t  rc;
  uint32_t  num_cores; // number of cores in current tile
  uint32_t  curr_core_index;
  hn_command_stats_monitor_t           cmd;
  hn_command_stats_monitor_response_t  response;
  hn_stats_monitor_t                  *tile_statistics;
  uint32_t  ret_code = HN_SUCCEEDED;

  *stats = NULL;
  // prepare command, send polling period 
  // tile ID exists in architecture will be checked in hn_daemon
  cmd.common.command = HN_COMMAND_READ_STATS_MONITOR;
  cmd.tile           = tile;
  //cmd.core           = core;
  log_debug("hn_stats_monitor_read, hn_lib send read stats command to daemon\n");
  wc = hn_write_socket(&cmd, sizeof(hn_command_stats_monitor_t));
  if (wc != HN_SUCCEEDED) {
    return wc;
  }

  // we wait response from daemon 
  rc = hn_read_socket(&response, sizeof(hn_command_stats_monitor_response_t));
  if (rc != HN_SUCCEEDED) {
    return rc;
  }

  if ((response.status != HN_STATS_MONITOR_OPERATION_OK)) {
    // error setting tile for automatic monitorization
    return response.status;
  }

  // read the rest of responses, one per core in the tile
  num_cores = response.num_cores;

  *cores_in_tile = num_cores;
  
  if(num_cores == 0) {
    log_debug("hn_stats_monitor_read, tile has %u cores, early return\n", num_cores);
    return HN_SUCCEEDED;
  }
  
  // let's allocate memory for the required stats
  tile_statistics=(hn_stats_monitor_t *)malloc(num_cores * sizeof(hn_stats_monitor_t));
  if(tile_statistics == NULL){
    // it will return an error, but now we need to read all the stats to remove data from the socket
    log_error("allocating memory for the stats, discarding incoming data from the socket\n");
    for(curr_core_index = 0; curr_core_index < num_cores; curr_core_index++)
    {
      rc = hn_read_socket(&response, sizeof(hn_command_stats_monitor_response_t));
      if (rc != HN_SUCCEEDED) {
        log_error("Multiple error reading data for core %u\n", curr_core_index);
      }
    }
    log_info("all data discarded, continue");
    return HN_MEMORY_ALLOCATION_ERROR;
  }

  for(curr_core_index = 0; curr_core_index < num_cores; curr_core_index++)
  {
    rc = hn_read_socket(&response, sizeof(hn_command_stats_monitor_response_t));
    if (rc != HN_SUCCEEDED) {
      log_error("hn_stats_monitor_read, reading a response...reading the rest\n");
      ret_code = HN_SOCKET_ERROR;
    }

    //tile_statistics[curr_core_index].tics_kernel       = response.core_stats.tics_kernel;
    //tile_statistics[curr_core_index].tics_user         = response.core_stats.tics_user;
    tile_statistics[curr_core_index].tics_sleep        = response.core_stats.tics_sleep;
    //tile_statistics[curr_core_index].num_threads       = response.core_stats.num_threads;
    tile_statistics[curr_core_index].l1i_hits          = response.core_stats.l1i_hits;
    tile_statistics[curr_core_index].l1i_misses        = response.core_stats.l1i_misses;
    tile_statistics[curr_core_index].l1d_hits          = response.core_stats.l1d_hits;
    tile_statistics[curr_core_index].l1d_misses        = response.core_stats.l1d_misses;
    tile_statistics[curr_core_index].l2_hits           = response.core_stats.l2_hits;
    tile_statistics[curr_core_index].l2_misses         = response.core_stats.l2_misses;
    tile_statistics[curr_core_index].btb_predict       = response.core_stats.btb_predict;
    tile_statistics[curr_core_index].btb_misspredict   = response.core_stats.btb_misspredict;
    tile_statistics[curr_core_index].core_cycles       = response.core_stats.core_cycles;
    tile_statistics[curr_core_index].core_instr        = response.core_stats.core_instr;
    tile_statistics[curr_core_index].core_cpi          = response.core_stats.core_cpi;
    tile_statistics[curr_core_index].mc_accesses       = response.core_stats.mc_accesses;
    tile_statistics[curr_core_index].timestamp         = response.core_stats.timestamp;
    memcpy((void *)&tile_statistics[curr_core_index].nstats, (void *)&response.core_stats.nstats, HN_MAX_VNS * sizeof (hn_unit_net_stats_t) );

  }
  
  // Command succesfully triggered, polling period has succesfully been updated 
  *stats = tile_statistics;
  
  return ret_code;

}
//*********************************************************************************************************************
// end of file hn.c
//*********************************************************************************************************************
