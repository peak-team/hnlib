///////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
// 
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and 
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is 
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
// 
// contact: jflich@disca.upv.es
//-----------------------------------------------------------------------------
//
// Company:  GAP (UPV)  
// Engineer: R. Tornero (ratorga@disca.upv.es)
// 
// Create Date: 
// Design Name: 
// Module Name: 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
//
//  
//
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////

#define COMMAND_PEAK_CONSOLE                         (u_int8_t)0
#define COMMAND_PEAK_DEBUG_L1D_FIRST_ITEM            (u_int8_t)6
#define COMMAND_PEAK_DEBUG_L1D_REMAINING_ITEMS       (u_int8_t)7
#define COMMAND_PEAK_DEBUG_L2_FIRST_ITEM             (u_int8_t)8
#define COMMAND_PEAK_DEBUG_L2_REMAINING_ITEMS        (u_int8_t)9
#define COMMAND_PEAK_TILE_STATS_FIRST_ITEM           (u_int8_t)1
#define COMMAND_PEAK_TILE_STATS_REMAINING_ITEMS      (u_int8_t)2
#define COMMAND_PEAK_DEBUG_MC_FIRST_ITEM             (u_int8_t)10
#define COMMAND_PEAK_DEBUG_MC_REMAINING_ITEM         (u_int8_t)11
#define COMMAND_PEAK_DEBUG_BLOCK_L1D_FIRST_ITEM      24//(u_int8_t)10
#define COMMAND_PEAK_DEBUG_BLOCK_L1D_REMAINING_ITEMS 25//(u_int8_t)11
#define COMMAND_PEAK_DEBUG_BLOCK_L2_FIRST_ITEM       26//(u_int8_t)10
#define COMMAND_PEAK_DEBUG_BLOCK_L2_REMAINING_ITEMS  27//(u_int8_t)11
#define COMMAND_PEAK_ECHO                            (u_int8_t)5
#define COMMAND_PEAK_DEBUG_CORE_FIRST_ITEM           (u_int8_t)16
#define COMMAND_PEAK_DEBUG_CORE_REMAINING_ITEMS      (u_int8_t)17
#define COMMAND_PEAK_DEBUG_TR_FIRST_ITEM             (u_int8_t)18
#define COMMAND_PEAK_DEBUG_TR_REMAINING_ITEMS        (u_int8_t)19
#define COMMAND_PEAK_TILEREG_FIRST_ITEM              (u_int8_t)3
#define COMMAND_PEAK_TILEREG_REMAINING_ITEMS         (u_int8_t)4
#define COMMAND_PEAK_DEBUG_INJECT_FIRST_ITEM         (u_int8_t)12
#define COMMAND_PEAK_DEBUG_INJECT_REMAINING_ITEMS    (u_int8_t)13
#define COMMAND_PEAK_DEBUG_EJECT_FIRST_ITEM          (u_int8_t)14
#define COMMAND_PEAK_DEBUG_EJECT_REMAINING_ITEMS     (u_int8_t)15



#define COMMAND_PEAK_CONFIGURE_TILE                  (u_int8_t)34
#define COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS       (u_int8_t)33
#define COMMAND_PEAK_RESET_PEAK                      (u_int8_t)43
#define COMMAND_PEAK_READ_BLOCK                      (u_int8_t)40
#define COMMAND_PEAK_WRITE_MEMORY_BLOCK              (u_int8_t)41
#define COMMAND_PEAK_WRITE_PROTOCOL                  (u_int8_t)42
#define COMMAND_PEAK_TASK_REQUEST                    (u_int8_t)37
#define COMMAND_PEAK_SET_PROCESSOR_ADDRESS           (u_int8_t)38
#define COMMAND_PEAK_CLOCK                           (u_int8_t)44
#define COMMAND_PEAK_SET_NEW_CLOCK_MASK              (u_int8_t)39           // functionality not implemented yet
#define COMMAND_PEAK_KEY_INTERRUPT                   (u_int8_t)45
#define COMMAND_PEAK_WRITE_TILEREG                   (u_int8_t)36
#define COMMAND_PEAK_READ_TILEREG                    (u_int8_t)35
#define COMMAND_PEAK_SET_ETH_BW_BOTTOM_BIT           (u_int8_t)46
#define COMMAND_PEAK_SET_ETH_BW_TOP_BIT              (u_int8_t)47

// Subfunctions for command RESET
#define COMMAND_PEAK_RESET_ALL_FUNC                     (u_int8_t)0x01
#define COMMAND_PEAK_RESET_ETH_FUNC                     (u_int8_t)0x02
#define COMMAND_PEAK_RESET_ITENET_FUNC                  (u_int8_t)0x03


// Subfunctions for command CLOCK
#define COMMAND_PEAK_PAUSE_CLOCK_FUNC                        (u_int8_t)0x01
#define COMMAND_PEAK_RESUME_CLOCK_FUNC                       (u_int8_t)0x02
#define COMMAND_PEAK_RUNFOR_CLOCK_FUNC                       (u_int8_t)0x80

// Subcommands for DEBUG_AND_STATS
#define COMMAND_PEAK_STATS_DISABLE                   (u_int8_t)1
#define COMMAND_PEAK_STATS_ENABLE                    (u_int8_t)2         
#define COMMAND_PEAK_DEBUG_DISABLE                   (u_int8_t)5
#define COMMAND_PEAK_DEBUG_ENABLE                    (u_int8_t)6
#define COMMAND_PEAK_FLIT_DEBUG_DISABLE              (u_int8_t)13
#define COMMAND_PEAK_FLIT_DEBUG_ENABLE               (u_int8_t)14
#define COMMAND_PEAK_MC_DEBUG_DISABLE                (u_int8_t)9
#define COMMAND_PEAK_MC_DEBUG_ENABLE                 (u_int8_t)10
#define COMMAND_PEAK_BLOCK_DEBUG_DISABLE             (u_int8_t)11
#define COMMAND_PEAK_BLOCK_DEBUG_ENABLE              (u_int8_t)12
#define COMMAND_PEAK_ECHO_DISABLE                    (u_int8_t)3
#define COMMAND_PEAK_ECHO_ENABLE                     (u_int8_t)4
#define COMMAND_PEAK_CORE_DEBUG_DISABLE              (u_int8_t)21
#define COMMAND_PEAK_CORE_DEBUG_ENABLE               (u_int8_t)22
#define COMMAND_PEAK_POST_MORTEN_CORE_DEBUG_DISABLE  (u_int8_t)23
#define COMMAND_PEAK_POST_MORTEN_CORE_DEBUG_ENABLE   (u_int8_t)24
#define COMMAND_PEAK_POST_MORTEN_MC_DEBUG_DISABLE    (u_int8_t)27
#define COMMAND_PEAK_POST_MORTEN_MC_DEBUG_ENABLE     (u_int8_t)28
#define COMMAND_PEAK_VN2_FLIT_DEBUG_DISABLE              (u_int8_t)13
#define COMMAND_PEAK_VN2_FLIT_DEBUG_ENABLE               (u_int8_t)14
#define COMMAND_PEAK_VN2_POST_MORTEN_FLIT_DEBUG_DISABLE  (u_int8_t)15
#define COMMAND_PEAK_VN2_POST_MORTEN_FLIT_DEBUG_ENABLE   (u_int8_t)16
#define COMMAND_PEAK_TR_DEBUG_DISABLE                    (u_int8_t)25
#define COMMAND_PEAK_TR_DEBUG_ENABLE                     (u_int8_t)26
#define COMMAND_PEAK_SILENT_DEBUG_DISABLE                (u_int8_t)29
#define COMMAND_PEAK_SILENT_DEBUG_ENABLE                 (u_int8_t)30

