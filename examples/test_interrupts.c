#include <stdio.h>
#include <stdlib.h>

#include "../hn.h"

#define VERBOSE

#define MAX_VECTOR_IDS 256

int main(int argc, char **argv)
{
  uint32_t __attribute__((unused)) i;
  uint32_t __attribute__((unused)) x;
  uint32_t __attribute__((unused)) id;
  uint32_t __attribute__((unused)) tile;
  uint16_t __attribute__((unused)) vector_mask;
  uint32_t __attribute__((unused)) num_iterations;
  uint32_t __attribute__((unused)) num_attempts;
  uint32_t __attribute__((unused)) num_succeeds;
  uint32_t __attribute__((unused)) used_id[MAX_VECTOR_IDS];
  uint16_t __attribute__((unused)) vector_id[MAX_VECTOR_IDS];

  hn_filter_t filter;
  hn_initialize(filter, 1, 1, 0, 0);
  
  // Information
  printf("\n");
  printf("Collecting information:\n");
  uint32_t num_tiles;
  uint32_t num_tiles_x;
  uint32_t num_tiles_y;
  hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
  printf("  - num tiles : %d\n", num_tiles);
  printf("  - geometry  : %dx%x\n", num_tiles_x, num_tiles_y);
  printf("\n");

  // Test 1: We register interrupts
 /* printf("Test1: Registering and releasing interrupts\n");
  num_iterations = 10000;
  num_attempts = 0;
  num_succeeds = 0;
  for (i=0;i<MAX_VECTOR_IDS;i++) used_id[i] = 0;
  
  for (i=0;i<num_iterations;i++) {
    x = random() % MAX_VECTOR_IDS;
    if (used_id[x]) {
      #ifdef VERBOSE 
        printf("releasing id %d\n", vector_id[x]); 
      #endif
      hn_release_int(vector_id[x]);
      used_id[x] = 0;
    } else {
      tile = random() % num_tiles;
      vector_mask = 0x1008001F;
      #ifdef VERBOSE 
        printf("registering interrupt for tile %d, vector mask %08x\n", tile, vector_mask); 
      #endif
      if ( hn_register_int(tile, vector_mask, &id) == HN_SUCCEEDED ) {
        #ifdef VERBOSE 
          printf("successfully registered with id %d\n", id); 
        #endif
        num_succeeds++;
        used_id[x] = 1;
        vector_id[x] = id;
      }
      num_attempts++;
    }
  }
  printf("Number of attempts: %d, succeeded %d\n", num_attempts, num_succeeds);
  
  for (i=0;i<MAX_VECTOR_IDS;i++) {
    if (used_id[i]) {
      hn_release_int(vector_id[i]); 
      used_id[i] = 0;
    }
  }*/

  printf("Test2: Registering one interrupt and triggering interrupt\n");
  if ( hn_register_int(0, 0x00ff, &id) == HN_SUCCEEDED ) {
    for (i=0;i<10;i++) {
      printf("triggering interrupt\n");
      hn_interrupt(id, 0x0010);
    }
    hn_release_int(id);
  } else {
    printf("error, interrupt could not be registered\n");
  }
      
  

  return 1;
} 
