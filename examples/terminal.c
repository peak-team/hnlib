///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// This application implements a terminal with PEAK units running in a MANGO tile.
// The application receives the following parameters:
//   - <tile>: Tile where the PEAK unit is located
//   - <core>: Core of PEAK to get the terminal
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <stdlib.h>

#include "hn.h"

pthread_t  th_writer;

uint32_t tile;
uint32_t core;

void *thread_writer(void *argv) {

  char ch;
  struct timespec ts;

  ts.tv_sec = 0;
  ts.tv_nsec = 500000;

  while(1) {
    ch = getchar();
    
    // keystrokes always go to core 0
    hn_peak_send_char(tile, 0, ch);
    nanosleep(&ts, NULL);
  }
}

int main(int argc, char **argv)
{
  unsigned int rv;
  uint32_t     init_lib;
  uint32_t     rcx_core;
  
  if (argc != 3) {
    printf("Error in number of arguments\nSyntax: %s <tile> <core>\n", argv[0]);
    exit(1);
  }

  tile = atoi(argv[1]);
  core = atoi(argv[2]);

  // We prepare the filter to get console access
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_PEAK;
  filter.mode   = HN_FILTER_APPL_MODE_CONSOLE;
  filter.tile   = tile;
  filter.core   = core;
  
  init_lib = hn_initialize(filter, NO_PARTITION_STRATEGY, 1, 0, 1);
  // We init the hn library
  if (init_lib != HN_SUCCEEDED) {
    printf("Error  %u, not able to initialize the HN library\n", init_lib);
    exit(1);
  }

  // We spawn the writer thread
  rv = pthread_create(&th_writer, NULL, thread_writer, (void *)NULL);
  if (rv) {
    printf("Error, thread could not be run\n");
    exit(1);
  }
  // now we get characters from the HN library and print them out
  char c;
  while (1) {
    rv = hn_peak_get_console_char(&rcx_core, &c);
    if (rv != HN_SUCCEEDED) {
      hn_print_error(rv);
      exit(1);
    }
    printf("%c", c);
    fflush(stdout);
  }
  
  printf("leaving terminal\n");
  return 0;
}
