#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <pthread.h>

#include "hn.h"

typedef struct {
  uint32_t num_transfers;
  uint32_t id;
  uint32_t shm_buffer_size;
  uint32_t mc_addr;
  uint32_t tile;
} thread_args;

pthread_mutex_t ready_mutex;
uint32_t ready_counter;
pthread_mutex_t turn_mutex;
uint32_t turn_counter;

uint32_t check_phase;

float get_elapsed_seconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;

  if ((end.tv_nsec - start.tv_nsec) < 0)
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  }
  else
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }

  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;

  return (float)(elapsed_nanoseconds) / 1e9;
}

void *thread_func(void *arg)
{
  uint32_t  rv;
  uint64_t *shm_buffer = NULL;
  uint64_t *local_buff = NULL;
  uint64_t  it;
  uint32_t  magic;

  thread_args *args = arg;

  rv = hn_shm_malloc(args->shm_buffer_size, (void **) &shm_buffer);
  if (rv != HN_SUCCEEDED) {
    printf("THREAD %u: Error requesting buffer in shared memory, returned: %u\n", args->id, rv);
    return NULL;
  }

  printf("THREAD %u: I have been granted a shm on addr 0x%016lX\n", args->id, shm_buffer);

  local_buff = malloc(args->shm_buffer_size);

  magic = rand()/*1*/;
  printf("THREAD %u: Filling a local buffer with known data, magic = %u\n", args->id, magic);
  for (it = 0; it < args->shm_buffer_size / sizeof(uint64_t); it++)
  {
    *(local_buff + it) = it + magic;
  }

  printf("THREAD %u: Waiting for sync\n", args->id);

  pthread_mutex_lock(&ready_mutex);
  ready_counter++;
  pthread_mutex_unlock(&ready_mutex);

  while (ready_counter < args->num_transfers);

  while (turn_counter != args->id);

  float elapsed_in_sec;

  printf("THREAD %u: Requesting shm write\n", args->id);

  memcpy(shm_buffer, local_buff, args->shm_buffer_size);

  hn_shm_write_memory(shm_buffer, args->shm_buffer_size, args->mc_addr, args->tile, 0);

  pthread_mutex_lock(&turn_mutex);
  turn_counter++;
  pthread_mutex_unlock(&turn_mutex);

  printf("THREAD %u: Writing done\n", args->id);

  printf("THREAD %u: Waiting for sync\n", args->id);

  pthread_mutex_lock(&ready_mutex);
  ready_counter++;
  pthread_mutex_unlock(&ready_mutex);

  while (ready_counter < 2*(args->num_transfers));

  while (turn_counter != args->id);

  printf("THREAD %u: Cleaning buffer\n", args->id);

  bzero(shm_buffer, args->shm_buffer_size);

  printf("THREAD %u: Requesting shm read\n", args->id);

  hn_shm_read_memory(shm_buffer, args->shm_buffer_size, args->mc_addr, args->tile, 0);

  pthread_mutex_lock(&turn_mutex);
  turn_counter++;
  pthread_mutex_unlock(&turn_mutex);

  printf("THREAD %u: Reading done\n", args->id);

  printf("THREAD %u: Waiting for sync\n", args->id);

  while (check_phase == 0);

  int errors = 0;

  for (it = 0; it < args->shm_buffer_size / sizeof(uint64_t) && errors < 20; it++)
  {
    uint64_t *shmptr   = shm_buffer + it;
    uint64_t *localptr = local_buff + it;
    if ((*shmptr) != (*localptr)) {
      printf("THREAD %u: found data mismatch at byte 0x%X : wrote %016lX but read %016lX\n", args->id, it * sizeof(uint64_t), *localptr, *shmptr);
      errors++;
    }
  }

  if (errors == 0) {
    printf("THREAD %u: Data is clean\n", args->id);
  }

  hn_shm_free(shm_buffer);
  free(local_buff);

  return NULL;
}

int main(int argc, char **argv)
{
  uint32_t   rv;
  uint32_t   tile;
  uint32_t   transfers;
  uint32_t   num_weights;
  uint32_t   weights[100];

  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO;
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;

  if (argc != 4) {
    printf("USAGE: %s <tile> <transfers> <num_weights>\n", argv[0]);
    exit(0);
  }

  tile = atoi(argv[1]);
  transfers = atoi(argv[2]);
  num_weights = atoi(argv[3]);

  pthread_t threads[transfers];
  thread_args args[transfers];

  srand(time(NULL));

  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");

  uint32_t i;
  printf("Reading %d weights\n", num_weights);

  for (i = 0; i < num_weights; i++) {
    scanf("%d", &weights[i]);
  }

  printf("Setting weights\n");
  hn_set_vn_weights(weights, num_weights);

  uint32_t addr_counter = 0;

  pthread_mutex_init(&ready_mutex, NULL);
  pthread_mutex_init(&turn_mutex, NULL);
  ready_counter = turn_counter = check_phase = 0;

  for (i = 0; i < transfers; i++) {
    uint32_t size;

    printf("Reading transfer %d\n", i);
    printf("size: ");
    scanf("%u", &size);

    args[i].num_transfers = transfers;
    args[i].id = i;
    args[i].shm_buffer_size = size;
    args[i].mc_addr = addr_counter;
    args[i].tile = tile;

    addr_counter += size;

    pthread_create(&threads[i], NULL, thread_func, &args[i]);
  }

  while (turn_counter < transfers);

  printf("MAIN THREAD: Press enter to continue with the read phase\n");

  getchar();
  getchar();

  pthread_mutex_lock(&turn_mutex);
  turn_counter = 0;
  pthread_mutex_unlock(&turn_mutex);

  while (turn_counter < transfers);

  printf("MAIN THREAD: Press enter to continue with the check phase\n");

  getchar();

  check_phase = 1;

  for (i = 0; i < transfers; i++) {
    pthread_join(threads[i], NULL);
  }

  pthread_mutex_destroy(&ready_mutex);
  pthread_mutex_destroy(&turn_mutex);

  return 0;
}
