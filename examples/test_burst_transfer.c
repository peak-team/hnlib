///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 15, 2018
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Initialize hn library, request tow buffers in shared memrory,
//      fill one buffer with data
//      send data to FPGA through shm_write function
//      read data from FPGA throug shm_read function
//      check data send and received mathces
//      destroy buffers in shared memory
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdlib.h>
#include <locale.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>

#include "hn.h"


#define DEBUG

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
struct timespec get_elapsed_time(struct timespec start, struct timespec end)
{
  struct timespec temp;
  if ((end.tv_nsec - start.tv_nsec) < 0) 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  } 
  else 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }
  return temp;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
unsigned long int get_elapsed_time_nanoseconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;


  temp = get_elapsed_time(start, end);
  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;
  
  return elapsed_nanoseconds;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * \brief main test application
 *
 * accepts one parameter ./test_burst_transfer_donw <num_iterations>
 */

int main(int argc, char **argv)
{
  int        test_num_of_passes; // number of times the test will run, if no parameter is passed, the test will run for 100 times

  uint32_t   rv;
  uint32_t   shm_buffer_size;
  uint32_t   number_of_blocks;
  uint32_t   block_size_in_bytes;


//  int        random_number;
  uint64_t   value;
  uint64_t   value_increment;
  uint32_t   it;

  //Write to DDR memory  
  void      *shm_write_buffer_base_addr_ptr;
  uint64_t  *shm_write_buffer_ptr_64bit;
  uint32_t   number_of_blocks_to_send;
                                   // function call variables
  void      *transfer_wr_src_addr; // pointer to address of shm buffer
  uint32_t   transfer_wr_dst_tile;
  uint32_t   transfer_wr_dst_addr;
  uint32_t   transfer_wr_is_blocking;
  uint32_t   transfer_wr_size;
#ifdef DEBUG
  uint64_t   transfer_last_64bit;
#endif
  // Read from DDR memory
  void      *shm_read_buffer_base_addr_ptr;
  uint64_t  *shm_read_buffer_ptr_64bit;
  void      *transfer_rd_local_dst_addr;
  uint32_t   transfer_rd_src_tile;
  uint32_t   transfer_rd_src_addr;
  uint32_t   transfer_rd_is_blocking;
  uint32_t   transfer_rd_size;

  double   ts_megabytes;

  struct timespec    t1;
  struct timespec    t2;
  struct timespec    t3;
  struct timespec    t4;
  unsigned long int  wr_elapsed_ns;  // time elapsed in nanoseconds
  float              wr_elapsed_in_sec;
  unsigned long int  rd_elapsed_ns;  // time elapsed in nanoseconds
  float              rd_elapsed_in_sec;

#ifdef DEBUG
  uint32_t  transfer_last_mc_wr_addr;
#endif
  char      mem_content_compare_match;


  int       cmd_opt;
  uint32_t  pass_it;
  uint32_t  pass_failed;
  uint32_t  mc_on_tile;
  uint32_t  reset_system;
  uint32_t  addr_on_mc;

  setlocale(LC_ALL, ""); // to allow digit grouping in printf, using user selected locale

  pass_failed  = 0;
  // set default values for application parameters
  pass_it      = 0;
  mc_on_tile          = 0;
  test_num_of_passes  = 1;
  reset_system        = 0;
  number_of_blocks    = 30;
  addr_on_mc          = 0x00000000;;


  printf("\n\n");
  printf("---------------------------------------------------\n");
  printf("---------------------------------------------------\n");
  printf("     DEMO Application\n");
  printf("       Initialize hn library\n");
  printf("       Request buffer in shared memory area\n");
  printf("       fill buffer in host side\n");
  printf("       transfer buffer to Memory on FPGA\n");
//  printf("       read Memory and save to local buffer\n")
  printf("       check transfer\n");
  printf("       Close buffer in shared memory area\n");
  printf("---------------------------------------------------\n");

  printf("\n\n");
  printf("***************************************************\n");
  printf("    RE-ACTIVATE debug, info and verbose messages\n");
  printf("      search for pattern:\n");
  printf("          JM10_msg_disabled\n");
  printf("***************************************************\n");

  // process args searching application configuration parameters
  printf("\n");
  printf("---------------------------------\n");
  while ((cmd_opt = getopt (argc, argv, "ra:t:b:i:h")) != -1)
  {
    switch (cmd_opt) 
    {
      case 'r':
        printf("option -> reset, will reset system before running test\n");
        reset_system = 1;
        break;
      case 'a':
        addr_on_mc=(uint32_t)strtol(optarg, NULL, 16);
        addr_on_mc = addr_on_mc - (addr_on_mc%512);
        printf("option-> set first block  write/read address to 0x%08X\n", addr_on_mc);
        break;
      case 't':
        mc_on_tile = atoi(optarg);
        printf("option -> set MC on tile : %d\n", mc_on_tile);
        break;
      case 'b':
        if(strcmp(optarg,"max")==0){
          number_of_blocks = 30000000;
        } else {
          number_of_blocks = atoi(optarg);
          if (number_of_blocks > 30000000) number_of_blocks = 30000000;
          else if (number_of_blocks < 1) number_of_blocks = 1;
        }
        printf("option -> set number of blocks for transfer : %d\n",number_of_blocks);
        break;
      case 'i':
        test_num_of_passes = atoi(optarg);
        if (test_num_of_passes < 1) test_num_of_passes = 1;
        printf("option -> set test number of iterations : %d\n", test_num_of_passes);
        break;
      case 'h':
        printf("HELP\n");
        printf("supported options\n");
        printf("  -t <mc_on_tile> -b <number_of_blocks_to_transfer> -a <base_addr_on_mc> -r  -i <number_of_test_runs>\n");
        printf("  -t target Memory Controller attached on tile <mc_on_tile>\n");
        printf("  -a first block will be written at <base_addr_on_mc>");
        printf("  -b number of blocks to write/read on MC, starting at <base_addr_on_mc>\n");
        printf("     address in HEX format without 0x, default value is 0\n");
        printf("  -r reset hn_system before running the test\n");
        printf("  -i <number_of_test_runs>\n");
        printf("  \n");
        return -1;
      case '?':
        if ((optopt == 't') || (optopt == 'b') || (optopt == 'r'))
          printf ("Option -%c requires an argument\n", optopt);
        else if (isprint (optopt))
          printf ("Unknown option `-%c'\n", optopt);
        else
          printf ("Unknown option character `\\x%x'\n", optopt);
        return 1;
      default:
        return -1;
    }
  }

  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;
   
  printf("\n");
  printf(" Test configuration\n");
  printf("   number_of_runs   %d\n", test_num_of_passes);
  printf("   transfer %d blocks \n", number_of_blocks);
  printf("   target MC on tile  %d\n", mc_on_tile); 
  printf("\n");
  fflush(stdout);
  
  // We initialize the hn library with the filter and the UPV's partition strategy
  //hn_initialize(hn_filter_t filter, uint32_t partition_strategy, uint32_t socket_connection, uint32_t reset, uint32_t capture_signals_enable)
  //printf("reset system before test\n");
  //hn_reset(0);
  
  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");
  sleep(1);

  if(reset_system) {
    printf("reset system before run the test\n");
    fflush(stdout);
    hn_reset(0);
  }
  printf("   ...done\n");


  block_size_in_bytes      = 64; // 64byte values, to mimic block width in DDR memory  
  shm_buffer_size          = number_of_blocks * block_size_in_bytes; // MULTIPLE OF 512 bits -> 64 BYTES 
  ts_megabytes             = (double)shm_buffer_size /(1024.0 * 1024.0);
  number_of_blocks_to_send = number_of_blocks;  // send half of the buffer

  // REQUEST SHM BUFFERS
  printf("Request WRITE shm buffer for %u blocks. It is %u bytes -> %4.4f MB\n", number_of_blocks, shm_buffer_size, ts_megabytes);
  rv = hn_shm_malloc(shm_buffer_size, &shm_write_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error requesting write buffer in shared memory, returned: %u\n", rv);
    hn_end();
    exit(1);
  }
  printf("  shmn buffer granted on addr 0x%08lx    of %u bytes\n", (unsigned long)shm_write_buffer_base_addr_ptr, shm_buffer_size);
  printf("  each run will write %lu values of 64 bits on the buffer\n", shm_buffer_size/sizeof(uint64_t));
  printf("\n");
    // set pointers
  shm_write_buffer_ptr_64bit =  (uint64_t *)shm_write_buffer_base_addr_ptr;

  printf("Request READ shm buffer for %u blocks. It is %u bytes -> %4.4f MB\n", number_of_blocks, shm_buffer_size, ts_megabytes);
  rv = hn_shm_malloc(shm_buffer_size, &shm_read_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error requesting read buffer in shared memory, returned: %u\n", rv);
    hn_shm_free(shm_write_buffer_base_addr_ptr);
    hn_end();
    exit(1);
  }
  printf("  shmn buffer granted on addr 0x%08lx    of %u bytes\n", (unsigned long)shm_read_buffer_base_addr_ptr, shm_buffer_size);
  printf("\n");
    // set pointers
  shm_read_buffer_ptr_64bit =  (uint64_t *)shm_read_buffer_base_addr_ptr;


  
  // set initial seed for the test
  srand(time(NULL));
  for (pass_it = 0; pass_it < test_num_of_passes; pass_it++)
  {
    printf("\n");
    printf("***********");
    printf("      ITERATION #%d\n", pass_it);

    value                  = 0x0000000000000000; // initial value for the data of the shm buffer
    //value                  = rand() + RAND_MAX/2;
    value_increment        = 0x0000000000000001;

    for (it = 0; it < shm_buffer_size/sizeof(uint64_t); it++)
    {
      *shm_write_buffer_ptr_64bit = value;
      // increase ptr to next uint64 position 
      value = value + value_increment;
      shm_write_buffer_ptr_64bit++;
    }

    // write to DDR-memory from shared memory buffer tesa
    transfer_wr_dst_tile    = mc_on_tile;
    transfer_wr_dst_addr    = addr_on_mc;
    transfer_wr_is_blocking = 1;
    transfer_wr_size        = number_of_blocks_to_send * block_size_in_bytes;
    shm_write_buffer_ptr_64bit    = (uint64_t *)shm_write_buffer_base_addr_ptr;
    transfer_wr_src_addr    = (void *)shm_write_buffer_ptr_64bit;


#ifdef DEBUG
    transfer_last_mc_wr_addr = transfer_wr_dst_addr + (number_of_blocks_to_send - 1) * block_size_in_bytes;
    transfer_last_64bit      = *(shm_write_buffer_ptr_64bit + (transfer_wr_size/8) -1);

    printf("Send notification to hn_daemon to send data from shm buffer to HN system\n");
    printf("Test will send   %d blocks, as %u bytes\n", number_of_blocks_to_send, transfer_wr_size);
    printf("  transfer configuration: \n");
    printf("    base addr:     %p\n",        (void *)shm_write_buffer_base_addr_ptr);
    printf("    src addr:      %p\n",        (void *)transfer_wr_src_addr);
    printf("    size:          %u bytes\n",  transfer_wr_size);
    printf("    dst  mc tile:  %u\n",        transfer_wr_dst_tile);
    printf("    dst  mc addr:  0x%08X\n",    transfer_wr_dst_addr);
    printf("    last mc addr   0x%08X\n",    transfer_last_mc_wr_addr);
    printf("    first val:     0x%016lx\n", *shm_write_buffer_ptr_64bit);
    printf("    last  val:     0x%016lx\n",  transfer_last_64bit);
    printf("    %sblocking transfer\n",      transfer_wr_is_blocking?"":"non-");
#endif
    clock_gettime(CLOCK_MONOTONIC, &t1);

    hn_shm_write_memory(transfer_wr_src_addr , transfer_wr_size, transfer_wr_dst_addr, transfer_wr_dst_tile, transfer_wr_is_blocking);
#ifdef DEBUG    
    printf("...write completed, ack received\n\n");
#endif
    clock_gettime(CLOCK_MONOTONIC, &t2);
    wr_elapsed_ns     = (float)get_elapsed_time_nanoseconds(t1, t2);
    wr_elapsed_in_sec = wr_elapsed_ns / (float)1000000000.0;


    transfer_rd_src_tile         = transfer_wr_dst_tile;
    transfer_rd_src_addr         = transfer_wr_dst_addr;;
    transfer_rd_is_blocking      = 1;
    transfer_rd_size             = transfer_wr_size;
    shm_read_buffer_ptr_64bit    = (uint64_t *)shm_read_buffer_base_addr_ptr;
    transfer_rd_local_dst_addr   = (void *)shm_read_buffer_ptr_64bit;

#ifdef DEBUG
    printf("Programming read operation;");
    printf("Send notification to hn_daemon to read from HN system Memory to shm buffer\n");
#endif
    clock_gettime(CLOCK_MONOTONIC, &t3);
    // check the last value written in the memory to validate all data has been sent
    hn_shm_read_memory(transfer_rd_local_dst_addr, transfer_rd_size, transfer_rd_src_addr, transfer_rd_src_tile, transfer_rd_is_blocking);
    clock_gettime(CLOCK_MONOTONIC, &t4);
    rd_elapsed_ns     = (float)get_elapsed_time_nanoseconds(t3, t4);
    rd_elapsed_in_sec = rd_elapsed_ns / (float)1000000000.0;


#ifdef DEBUG
    printf("...read operation finished\n");
    printf("\n");
    
#endif
    
    /*
    printf("  checking registers\n");

    stats_active_wr_to_data_burst_dnstr_fifo_expected    = stats_active_wr_to_data_burst_dnstr_fifo_expected    + (1 * 8) + (transfer_wr_size / 8);
    stats_active_rd_from_data_burst_dnstr_fifo_expected  = stats_active_rd_from_data_burst_dnstr_fifo_expected  + (1 * 8) + (transfer_wr_size / 8);
    stats_active_wr_to_simple_fifo_expected              = stats_active_wr_to_simple_fifo_expected              + (1 * 8) + (transfer_wr_size / 8);
    stats_active_rd_from_simple_fifo_expected            = stats_active_rd_from_simple_fifo_expected            + (1 * 8) + (transfer_wr_size / 8);

    hn_read_register(0, 1011, &rd_tilereg_value_msb);
    hn_read_register(0, 1012, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_wr_to_data_burst_dnstr_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);

    hn_read_register(0, 1013, &rd_tilereg_value_msb);
    hn_read_register(0, 1014, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_rd_from_data_burst_dnstr_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);

    hn_read_register(0, 1015, &rd_tilereg_value_msb);
    hn_read_register(0, 1016, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_wr_to_simple_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);

    hn_read_register(0, 1017, &rd_tilereg_value_msb);
    hn_read_register(0, 1018, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_rd_from_simple_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);
    */
/*
    hn_read_memory_block(0, transfer_last_mc_wr_addr,&block_data[0]);

    w0 = *((uint32_t *)&block_data[60]);
    w1 = *((uint32_t *)&block_data[56]);
    w64_0 = w0;
    w64_1 = w1;
    readblock_last_64bit = w64_0 << 32 | w64_1;
*/
    // void pointers size is one byte,
    uint32_t loop_index;
    mem_content_compare_match = 1;
    unsigned char *rd_p;
    unsigned char *wr_p;
    rd_p = (unsigned char *)shm_read_buffer_base_addr_ptr;
    wr_p = (unsigned char *)shm_write_buffer_base_addr_ptr;
    for(loop_index = 0; loop_index < shm_buffer_size; loop_index++){
      //printf("addr rd %p val %u  addr wr %p  val %u\n", rd_p, (0xff&(*rd_p)), wr_p, (0xff&(*wr_p)));      
      if(*rd_p != *wr_p) {
        mem_content_compare_match = 0;
        break;
      }
      rd_p++;
      wr_p++;
    }

    /*
    if (
           ( stats_active_wr_to_data_burst_dnstr_fifo_expected   ==  stats_active_wr_to_data_burst_dnstr_fifo_read   )
        && ( stats_active_rd_from_data_burst_dnstr_fifo_expected ==  stats_active_rd_from_data_burst_dnstr_fifo_read )
        && ( stats_active_wr_to_simple_fifo_expected             ==  stats_active_wr_to_simple_fifo_read             )
        && ( stats_active_rd_from_simple_fifo_expected           ==  stats_active_rd_from_simple_fifo_read           )
        && ( mem_content_compare_match                           !=  0)
       )*/
    if (mem_content_compare_match !=  0)
    {
      printf("#%u passed. Write BW = %'6.4f kB/s  Read BW =  %'6.4f kB/s (size %6.2f kB  the test took: WR %3.3f s  RD %3.3f s))\n",
            pass_it,
            (float) ((float)(transfer_wr_size) / (float)(1000.0)) / wr_elapsed_in_sec, 
            (float) ((float)(transfer_rd_size) / (float)(1000.0)) / rd_elapsed_in_sec,
            (float)transfer_wr_size / (float)1000.0,
            wr_elapsed_in_sec,
            rd_elapsed_in_sec
            );
      fflush(stdout);
    }
    else
    {
      printf("#%u FAILED. Write BW = %'6.4f kB/s  Read BW =  %'6.4f kB/s (size %6.2f kB  the test took: WR %3.3f s  RD %3.3f s))\n",
            pass_it,
            (float) ((float)(transfer_wr_size) / (float)(1000.0)) / wr_elapsed_in_sec, 
            (float) ((float)(transfer_rd_size) / (float)(1000.0)) / rd_elapsed_in_sec,
            (float)transfer_wr_size / (float)1000.0,
            wr_elapsed_in_sec,
            rd_elapsed_in_sec
            ); 
      //printf("  %8s stats_active_wr_to_data_burst_dnstr_fifo     expected     %16lu   read    %10lu \n", (stats_active_wr_to_data_burst_dnstr_fifo_expected == stats_active_wr_to_data_burst_dnstr_fifo_read)?"OK":"FAILED", stats_active_wr_to_data_burst_dnstr_fifo_expected   , stats_active_wr_to_data_burst_dnstr_fifo_read   );
      //printf("  %8s stats_active_rd_from_data_burst_dnstr_fifo   expected     %16lu   read    %10lu \n", (stats_active_rd_from_data_burst_dnstr_fifo_expected == stats_active_rd_from_data_burst_dnstr_fifo_read)?"OK":"FAILED", stats_active_rd_from_data_burst_dnstr_fifo_expected , stats_active_rd_from_data_burst_dnstr_fifo_read );
      //printf("  %8s stats_active_wr_to_simple_fifo               expected     %16lu   read    %10lu \n", (stats_active_wr_to_simple_fifo_expected == stats_active_wr_to_simple_fifo_read)?"OK":"FAILED", stats_active_wr_to_simple_fifo_expected, stats_active_wr_to_simple_fifo_read             );
      //printf("  %8s stats_active_rd_from_simple_fifo             expected     %16lu   read    %10lu \n", (stats_active_rd_from_simple_fifo_expected == stats_active_rd_from_simple_fifo_read)?"OK":"FAILED", stats_active_rd_from_simple_fifo_expected , stats_active_rd_from_simple_fifo_read           );
      printf("  Memory Content %s\n", (mem_content_compare_match != 0) ? "matches":" does NOT match" );
      printf("\n");

      pass_failed++;
      // setting values on expected stats variables to avoid false error detections
      //printf("reset system before continue\n");
      //fflush(stdout);
      //hn_reset(0);
      //stats_active_wr_to_data_burst_dnstr_fifo_expected    = 0;
      //stats_active_rd_from_data_burst_dnstr_fifo_expected  = 0;
      //stats_active_wr_to_simple_fifo_expected              = 0;
      //stats_active_rd_from_simple_fifo_expected            = 0;
      
    }
    //end of pass iteration, loop until all runs have completed
  }
  
  // Test finished, release all data structures
  printf("\n");
  printf("all done\n\n");

  if (pass_failed != 0)
  {
    printf(" TEST KO !!\n");
    printf(" %u of %u runs failed\n", pass_failed, pass_it);
  }
  else
  {
    printf(" TEST OK \n");
    printf(" total runs: %u\n", pass_it);
  }

  
  rv = hn_shm_free(shm_write_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error deleting write buffer in shared memory, returned: %u", rv);
    hn_end();
  }

  rv = hn_shm_free(shm_read_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error deleting read buffer in shared memory, returned: %u", rv);
    hn_end();;
  }

  printf("Release communication sockets and resources\n");
  hn_end();

  printf("bye bye\n\n");

  return 0;
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

