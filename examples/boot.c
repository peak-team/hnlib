///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node background communication process
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "hn.h"

int main(int argc, char **argv)
{
  int            i, x, y;
  uint32_t       rv;
  uint32_t       num_tiles, num_tiles_x, num_tiles_y, num_vns;
  hn_tile_info_t tile_info[HN_MAX_TILES];
  uint32_t       tile;
  const char     *prot_file_path;
  const char     *kern_file_path;
  int            default_paths;

  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;
   
  prot_file_path = "../img/peak_protocol.img";
  kern_file_path = "../img/peakos.img";
  
  default_paths = 0;
  if(argc == 1) {
    printf("No parameters detected, setting default peak_protocol.img and peakos.img files path\n");
    default_paths = 1;
    printf("  peak protocol image file path set to: %s\n", prot_file_path);
    printf("  peak kernel   image file path set to: %s\n", kern_file_path);
  }else if(argc != 3) {
    printf("Unexpected number of parameters");
    printf("USAGE: %s <path_to_protocol.img> <path_to_kernel.img>\n", argv[0]);
    printf("     : %s \n", argv[0]);
    return (-1);
  } else if(access(argv[1], 0) != 0 ) {
    printf("ERROR: protocol image file not found: %s\n", argv[1]);
    return (-1);
  }else if(access(argv[2], 0) != 0 ) {
    printf("ERROR: kernel image file not found: %s\n", argv[2]);
    return (-1);
  }

  if(default_paths == 0) {
    prot_file_path = argv[1];
    kern_file_path = argv[2];
  } 

  printf("\n");
  printf("---------------------------------\n");
  printf("     DEMO Application\n");
  printf("       Initialize hn library\n");
  printf("       Initialize PEAK cores\n");
  printf("       Load application kernel\n");
  printf("       Run application\n");
  printf("---------------------------------\n");
  printf("\n");

  // We initialize the hn library with the filter and the UPV's partition strategy
  // with reset
  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 1, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");

  hn_reset(0);

  if (hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y) != HN_SUCCEEDED) {
    printf("Error getting mesh architecture info\n");
    return 1;
  }

  if (hn_get_num_vns(&num_vns) != HN_SUCCEEDED) {
    printf("Error getting number of networks\n");
    return 1;
  }

  for (i=0;i<num_tiles;i++) {
    hn_get_tile_info(i, &tile_info[i]);
  }

  printf("Number of tiles: %d (%d x %d)\n", num_tiles, num_tiles_x, num_tiles_y);
  printf("Number of Virtual Networks: %d\n", num_vns);
  printf("\nTiles configuration:\n");
  tile = 0;
  for (x = 0; x < num_tiles_x; x++)
  {
    for (y = 0; y < num_tiles_y; y++)
    {
      switch(tile_info[tile].unit_family) {
        case HN_TILE_FAMILY_PEAK:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "PEAK0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "PEAK1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "PEAK2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "PEAK3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "PEAK4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "PEAK5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "PEAK6" :
                                                                                     "UNKWN");
          break;

        case HN_TILE_FAMILY_NUPLUS:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "NUPLUS0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "NUPLUS1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "NUPLUS2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "NUPLUS3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "NUPLUS4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "NUPLUS5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "NUPLUS6" :
                                                                                     "UNKWN");
          break;

        case HN_TILE_FAMILY_DCT:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "DCT0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "DCT1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "DCT2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "DCT3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "DCT4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "DCT5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "DCT6" :
                                                                                     "UNKWN");
          break;

        case HN_TILE_FAMILY_TETRAPOD:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "TETRAPOD0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "TETRAPOD1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "TETRAPOD2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "TETRAPOD3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "TETRAPOD4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "TETRAPOD5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "TETRAPOD6" :
                                                                                     "UNKWN");
          break;
          
        default: 
          printf("T%03d: %-10s   ", tile, "UNKWN");
          break;
      }
      tile++;
    }
    printf("\n");
  }

  printf("\nMemory:\n");
  for (i=0;i<num_tiles;i++) {
    if (tile_info[i].memory_attached) printf("Memory at tile %d (%u KB)\n", i, tile_info[i].memory_attached);
  }

  uint32_t tile_mem[HN_MAX_TILES];
  uint32_t paddr[HN_MAX_TILES];
  uint32_t family_req[HN_MAX_TILES];
  uint32_t tile_peak[HN_MAX_TILES];
  uint32_t family_got[HN_MAX_TILES];
  uint32_t num_rsc_requested = 3;
  uint32_t num_mems = 0;

  for (i= 0; i < HN_MAX_TILES; i++) {
    family_req[i] = HN_TILE_FAMILY_PEAK;
  }

  if (hn_find_units_set(0, num_rsc_requested, family_req, tile_peak, family_got) != HN_SUCCEEDED) {
    printf("We could not get any set of units of type %d \n", family_req[0]);
    hn_end();
    return 0;
  }

  printf("Got resources requested...\n");

   // We boot now the system
  printf("\nbooting the system...\n");
  for (i=0;i<num_rsc_requested;i++) {
    if (family_got[i] == HN_TILE_FAMILY_PEAK) {
      printf("booting tile %u   with protocol image: %s  with peakos image: %s\n", tile_peak[i], prot_file_path, kern_file_path);

      // FIXME we are assuming we are going to get the resources granted
      if (hn_find_memory(tile_peak[i], 1<<28, &tile_mem[i], &paddr[i]) == HN_SUCCEEDED) {
        hn_allocate_memory(tile_mem[i], paddr[i], 1<<28);
        hn_reserve_units_set(1, &(tile_peak[i])); 
        hn_set_tlb(tile_peak[i], 0, 0, 0x0fffffff, paddr[i], 1, 0, 0, tile_mem[i]);
        hn_boot_unit(tile_peak[i], tile_mem[i], i << 28, prot_file_path, kern_file_path);
        num_mems++;

        printf("tile %u booted in mem %u and paddr 0x%08X...\n", tile_peak[i], tile_mem[i], paddr[i]);
      } else {
        printf("tile %u could not be booted in mem %u and paddr 0x%08X...\n", tile_peak[i], tile_mem[i], paddr[i]);
      }
    }
  }


  getchar();

  printf("Release communication sockets and resources\n");

  for (i=0;i<num_mems;i++) {
    hn_release_memory(tile_mem[i], paddr[i], 1 << 28);
  }
  
  hn_release_units_set(num_rsc_requested, tile_peak);

  hn_end();

  printf("bye bye\n\n");

  return 0;
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

