///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 15, 2018
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Initialize hn library
//    request two buffers in memrory of the highest size we are going to send to the fpga
//    loop, sending each time a bigger transfer
//      fill one buffer with data
//      send data   to FPGA through write_memory function
//      read data from FPGA through read_memory  function
//      check data send and received matches
//    end loop
//    destroy buffers in local memory
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdlib.h>
#include <locale.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>

#include "hn.h"


//#define VERBOSE_MODE
#define MAX_NUM_BLOCKS 33554432

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
// Function to check if x is power of 2
// All power of two numbers have only one bit set. 
//  So count the number of set bits and if you get 1 then number is a power of 2
int is_pow2 (uint32_t x)
{
  // First x in the below expression is for the case when x is 0
  return (((x & ~(x-1))==x)? x : 0);
}
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
struct timespec get_elapsed_time(struct timespec start, struct timespec end)
{
  struct timespec temp;
  if ((end.tv_nsec - start.tv_nsec) < 0) 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  } 
  else 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }
  return temp;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
unsigned long int get_elapsed_time_nanoseconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;


  temp = get_elapsed_time(start, end);
  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;
  
  return elapsed_nanoseconds;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * \brief main test application
 *
 * accepts one parameter ./test_burst_transfer_donw <num_iterations>
 */

int main(int argc, char **argv)
{
  int        test_num_of_passes; // number of times the test will run, if no parameter is passed, the test will run for 100 times

  uint32_t   rv;
  uint32_t   local_buffer_size;
  uint32_t   number_of_blocks;
  uint32_t   block_size_in_bytes;
  uint32_t   check_data_enable;


//  int        random_number;
  uint64_t   value;
  uint64_t   value_increment;
  uint32_t   it;

  //Write to DDR memory  
  void      *write_buffer = NULL;
  uint64_t  *write_buffer_ptr_64bit = NULL;
  uint32_t   number_of_blocks_to_send;
                                   // function call variables
  void      *transfer_wr_src_addr = NULL; // pointer to address of buffer
  uint32_t   transfer_wr_dst_tile;
  uint32_t   transfer_wr_dst_addr;
  uint32_t   transfer_wr_size;
#ifdef VERBOSE_MODE
  uint64_t   transfer_last_64bit;
#endif
  // Read from DDR memory
  void      *read_buffer = NULL;
  uint64_t  *read_buffer_ptr_64bit = NULL;
  void      *transfer_rd_local_dst_addr = NULL;
  uint32_t   transfer_rd_src_tile;
  uint32_t   transfer_rd_src_addr;
  uint32_t   transfer_rd_size;

  double   ts_megabytes;

  struct timespec    t1;
  struct timespec    t2;
  struct timespec    t3;
  struct timespec    t4;
  unsigned long int  wr_elapsed_ns;  // time elapsed in nanoseconds
  float              wr_elapsed_in_sec;
  unsigned long int  rd_elapsed_ns;  // time elapsed in nanoseconds
  float              rd_elapsed_in_sec;

#ifdef VERBOSE_MODE
  uint32_t  transfer_last_mc_wr_addr;
#endif
  char      mem_content_compare_match;


  int       cmd_opt;
  uint32_t  pass_it;
  uint32_t  pass_failed;
  uint32_t  mc_on_tile;
  uint32_t  reset_system;
  uint32_t  addr_on_mc;

  setlocale(LC_ALL, ""); // to allow digit grouping in printf, using user selected locale

  pass_failed       = 0;
  check_data_enable = 0;

  // set default values for application parameters
  pass_it      = 0;
  mc_on_tile          = 0;
  reset_system        = 0;
  number_of_blocks    = 32768;
  test_num_of_passes  = 16;
  addr_on_mc          = 0x00000000;



  printf("\n\n");
  printf("---------------------------------------------------\n");
  printf("---------------------------------------------------\n");
  printf("     DEMO Application\n");
  printf("       Initialize hn library\n");
  printf("       Request local buffers \n");
  printf("       fill tx buffer in host side\n");
  printf("       transfer buffer to Memory on FPGA\n");
//  printf("       read Memory and save to local buffer\n")
  printf("       check transfer\n");
  printf("       Close buffers\n");
  printf("---------------------------------------------------\n");

  printf("\n\n");
  printf("***************************************************\n");
  printf("    RE-ACTIVATE debug, info and verbose messages\n");
  printf("      search for pattern:\n");
  printf("          JM10_msg_disabled\n");
  printf("***************************************************\n");

  // process args searching application configuration parameters
  printf("\n");
  printf("---------------------------------\n");
  while ((cmd_opt = getopt (argc, argv, "b:chrt:")) != -1)
  {
    switch (cmd_opt) 
    {
      case 'b': 
        if(strcmp(optarg,"max")==0){
          number_of_blocks =MAX_NUM_BLOCKS;
        }
        else
        {
          //double d_exp;
          //int    i_exp;
          number_of_blocks = atoi(optarg);
          if((number_of_blocks < 1)||(number_of_blocks > MAX_NUM_BLOCKS)) {
            printf("warning -b <%d> wrong value, out of bounds\n", number_of_blocks);
          }
          /*
          if (is_pow2(number_of_blocks) == 0)
          {
            printf("varning value -b <%d>, not power of 2, number will be rounded\n", number_of_blocks);

            d_exp = log2((double)number_of_blocks);
            i_exp = (int)d_exp + 1;
            number_of_blocks   = pow(2, i_exp);
            test_num_of_passes = i_exp;
            printf("new values   d_exp = %lf  i_exp = %d   number_fo_blocks = %u   passes = %u\n\n", d_exp, i_exp, number_of_blocks, test_num_of_passes);
          }
*/
          if (number_of_blocks > MAX_NUM_BLOCKS) number_of_blocks = MAX_NUM_BLOCKS;
          else if (number_of_blocks < 1) number_of_blocks = 1;
        }
        printf("option -> set number of blocks for transfer : %d\n",number_of_blocks);
        break;
      case 'c':
        printf("option -> enable check data\n");
        printf("          will read data from memory after writing\n");
        check_data_enable = 1;
        break;
      case 'h':
        printf("HELP\n");
        printf("supported options\n");
        printf("  -b <number_of_blocks_to_transfer> -c -r -t <mc_on_tile>  \n");
        printf("  -b max size of transfer in test, size is incremental starting from 1 block to specified value, in pow2\n");
        printf("  -c check data. Will read data and compare it with sent data\n");
        printf("  -r reset hn_system before running the test\n");
        printf("  -t target Memory Controller attached on tile <mc_on_tile>\n");
        printf("  \n");
        return -1;
        break;
      case 'r':
        printf("option -> reset, will reset system before running test\n");
        reset_system = 1;
        break;
      case 't':
        mc_on_tile = atoi(optarg);
        printf("option -> set MC on tile : %d\n", mc_on_tile);
        break;
      case '?':
        if ((optopt == 't') || (optopt == 'b') || (optopt == 'r'))
          printf ("Option -%c requires an argument\n", optopt);
        else if (isprint (optopt))
          printf ("Unknown option `-%c'\n", optopt);
        else
          printf ("Unknown option character `\\x%x'\n", optopt);
        return 1;
      default:
        return -1;
    }
  }

//  printf("current number_of_blocks is %u \n", number_of_blocks);
  // we check again if number of blocks is power of 2
  int extra_iter = 0; 
  if (is_pow2(number_of_blocks) == 0){
    extra_iter = 1;
  }
  printf(" checking number of blocks (%u)is power of 2, otherwise it will be rounded up\n", number_of_blocks);
  double d_exp;
  int    i_exp;
  d_exp = log2((double)number_of_blocks);
  i_exp = (int)d_exp + extra_iter;
  number_of_blocks   = pow(2, i_exp);
  test_num_of_passes = i_exp + 1; // if we have to send one block we have to run 1 time, not 0 (log 2 (1) = 0), so we need and extra iter of the pow2 exp

#ifdef VERBOSE_MODE
  printf("new values   d_exp = %lf  i_exp = %d   number_fo_blocks = %u   passes = %u\n\n", d_exp, i_exp, number_of_blocks, test_num_of_passes);
#endif


  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;
   
  printf("\n");
  printf(" Test configuration\n");
  printf("   number_of_runs   %d\n", test_num_of_passes);
  printf("   maxt transfer %d blocks \n", number_of_blocks);
  printf("   target MC on tile  %d\n", mc_on_tile); 
  printf("   will %scheck memory content after write\n", check_data_enable?"":"NOT");
  printf("\n");
  fflush(stdout);
 
  // We initialize the hn library with the filter and the UPV's partition strategy
  //hn_initialize(hn_filter_t filter, uint32_t partition_strategy, uint32_t socket_connection, uint32_t reset, uint32_t capture_signals_enable)
  //printf("reset system before test\n");
  //hn_reset(0);
  
  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");
  sleep(1);

  if(reset_system) {
    printf("reset system before run the test\n");
    fflush(stdout);
    hn_reset(0);
  }
  printf("   ...done\n");

  block_size_in_bytes      = 64; // 64byte values, to mimic block width in DDR memory  
  local_buffer_size        = number_of_blocks * block_size_in_bytes; // MULTIPLE OF 512 bits -> 64 BYTES 
  ts_megabytes             = (double)local_buffer_size /(1024.0 * 1024.0);


  // REQUEST BUFFERS
  printf("Request WRITE buffer for %u blocks. It is %u bytes -> %4.4f MB\n", number_of_blocks, local_buffer_size, ts_megabytes);
  write_buffer = malloc(local_buffer_size);
  if (write_buffer == NULL) {
    printf("Error requesting write buffer memory, returned: %d  %s\n", errno, strerror(errno));
    hn_end();
    exit(1);
  }
  printf("  write buffer granted on addr 0x%08lx    of %u bytes\n", (unsigned long)write_buffer, local_buffer_size);
  printf("\n");
  // set pointers
  write_buffer_ptr_64bit =  (uint64_t *)write_buffer;

  if (check_data_enable) {
    printf("Request READ buffer for %u blocks. It is %u bytes -> %4.4f MB\n", number_of_blocks, local_buffer_size, ts_megabytes);
    read_buffer= malloc(local_buffer_size);
    if (read_buffer == NULL) {
      printf("Error requesting read buffer, returned: %d  %s\n", errno, strerror(errno));
      free(write_buffer);
      hn_end();
      exit(1);
    }
    printf("  read buffer granted on addr 0x%08lx    of %u bytes\n", (unsigned long)read_buffer, local_buffer_size);
    printf("\n");
    // set pointers
    read_buffer_ptr_64bit =  (uint64_t *)read_buffer;
  }

#ifndef VERBOSE_MODE
  printf("%8s  %8s  %18s  %20s  %20s  %20s\n", "#it", "passed", "#blocks", "kBytes", "WR kBps", "RD kBps");
#endif
  
  // set initial seed for the test
  srand(time(NULL));
  number_of_blocks_to_send = 1;
  for (pass_it = 0; pass_it < test_num_of_passes; pass_it++)
  {
    uint32_t  current_it_transfer_size;

    if(pass_it != 0) number_of_blocks_to_send = number_of_blocks_to_send << 1;
#ifdef VERBOSE_MODE
    printf("\n");
    printf("***********");
    printf("      ITERATION #%d\n", pass_it);
    printf("      bloks to send on current pass: %d\n", number_of_blocks_to_send);
#endif
    //value                  = 0x0000000000000000; // initial value for the data of the buffer
    value                  = rand() + RAND_MAX/2;
    value_increment        = 0x0000000000000001;

    current_it_transfer_size = number_of_blocks_to_send * block_size_in_bytes;

    for (it = 0; it < current_it_transfer_size/sizeof(uint64_t); it++)
    {
      *write_buffer_ptr_64bit = value;
      // increase ptr to next uint64 position 
      value = value + value_increment;
      write_buffer_ptr_64bit++;
    }

    // write to DDR-memory from shared memory buffer tesa
    transfer_wr_dst_tile    = mc_on_tile;
    transfer_wr_dst_addr    = addr_on_mc;
    transfer_wr_size        = current_it_transfer_size;
    write_buffer_ptr_64bit    = (uint64_t *)write_buffer;
    transfer_wr_src_addr    = (void *)write_buffer_ptr_64bit;

#ifdef VERBOSE_MODE
    transfer_last_mc_wr_addr = transfer_wr_dst_addr + (number_of_blocks_to_send - 1) * block_size_in_bytes;
    transfer_last_64bit      = *(write_buffer_ptr_64bit + (transfer_wr_size/8) -1);

    printf("Send notification to hn_daemon to send data from buffer to HN system\n");
    printf("Test will send   %d blocks, as %u bytes\n", number_of_blocks_to_send, transfer_wr_size);
    printf("  transfer configuration: \n");
    printf("    base addr:     %p\n",        (void *)write_buffer);
    printf("    src addr:      %p\n",        (void *)transfer_wr_src_addr);
    printf("    size:          %u bytes\n",  transfer_wr_size);
    printf("    dst  mc tile:  %u\n",        transfer_wr_dst_tile);
    printf("    dst  mc addr:  0x%08X\n",    transfer_wr_dst_addr);
    printf("    last mc addr   0x%08X\n",    transfer_last_mc_wr_addr);
    printf("    first val:     0x%016lx\n", *write_buffer_ptr_64bit);
    printf("    last  val:     0x%016lx\n",  transfer_last_64bit);
#endif
    clock_gettime(CLOCK_MONOTONIC, &t1);

    rv = hn_write_memory(transfer_wr_dst_tile, transfer_wr_dst_addr, transfer_wr_size, (char *)transfer_wr_src_addr);
    if (rv != HN_SUCCEEDED) {
      printf("Error writing data to fpga memory\n");
      hn_print_error(rv);
      printf("Will abort current pass execution\n");
      continue;
    }
    clock_gettime(CLOCK_MONOTONIC, &t2);
    wr_elapsed_ns     = (float)get_elapsed_time_nanoseconds(t1, t2);
    wr_elapsed_in_sec = wr_elapsed_ns / (float)1000000000.0;

    if (check_data_enable) {

      transfer_rd_src_tile         = transfer_wr_dst_tile;
      transfer_rd_src_addr         = transfer_wr_dst_addr;;
      transfer_rd_size             = transfer_wr_size;
      read_buffer_ptr_64bit    = (uint64_t *)read_buffer;
      transfer_rd_local_dst_addr   = (void *)read_buffer_ptr_64bit;

#ifdef VERBOSE_MODE
      printf("Programming read operation;");
      printf("Send notification to hn_daemon to read from HN system Memory to buffer\n");
#endif

      clock_gettime(CLOCK_MONOTONIC, &t3);
      // check the last value written in the memory to validate all data has been sent
      hn_read_memory(transfer_rd_src_tile, transfer_rd_src_addr, transfer_rd_size, (char *)transfer_rd_local_dst_addr);
      clock_gettime(CLOCK_MONOTONIC, &t4);
      rd_elapsed_ns     = (float)get_elapsed_time_nanoseconds(t3, t4);
      rd_elapsed_in_sec = rd_elapsed_ns / (float)1000000000.0;

#ifdef VERBOSE_MODE
      printf("...read operation finished\n");
      printf("\n");

#endif
    }

    // void pointers size is one byte,
    uint32_t loop_index;
    
    unsigned char *rd_p;
    unsigned char *wr_p;

    mem_content_compare_match = 1;
    
    if (check_data_enable) {
      //printf("\n");
      //printf("check data enabled !!!\n");
      //printf("  transfer_wr_size %u   transfer rd_size %u\n\n", transfer_wr_size, transfer_rd_size);

      rd_p = (unsigned char *)read_buffer;
      wr_p = (unsigned char *)write_buffer;
      for(loop_index = 0; loop_index < transfer_rd_size; loop_index++){
        //printf("addr rd %p val %u  addr wr %p  val %u\n", rd_p, (0xff&(*rd_p)), wr_p, (0xff&(*wr_p)));      
        if(*rd_p != *wr_p) {
          mem_content_compare_match = 0;
          break;
        }
        rd_p++;
        wr_p++;
      }

     if (mem_content_compare_match ==  0) pass_failed++;

#ifdef VERBOSE_MODE
      if (mem_content_compare_match !=  0)
      {
        printf("#%u passed. Write BW = %'6.04f kB/s  Read BW =  %'6.04f kB/s (size %'6.02f kB  the test took: WR %'3.03f s  RD %'3.03f s))\n",
            pass_it,
            (float) ((float)(transfer_wr_size) / (float)(1000.0)) / wr_elapsed_in_sec, 
            (float) ((float)(transfer_rd_size) / (float)(1000.0)) / rd_elapsed_in_sec,
            (float)transfer_wr_size / (float)1000.0,
            wr_elapsed_in_sec,
            rd_elapsed_in_sec
            );
        fflush(stdout);
      }
      else
      {
        printf("#%u FAILED. Write BW = %'6.04f kB/s  Read BW =  %'6.04f kB/s (size %'6.2f kB  the test took: WR %'3.03f s  RD %'3.03f s))\n",
            pass_it,
            (float) ((float)(transfer_wr_size) / (float)(1000.0)) / wr_elapsed_in_sec, 
            (float) ((float)(transfer_rd_size) / (float)(1000.0)) / rd_elapsed_in_sec,
            (float)transfer_wr_size / (float)1000.0,
            wr_elapsed_in_sec,
            rd_elapsed_in_sec
            ); 
        printf("  Memory Content %s\n", (mem_content_compare_match != 0) ? "matches":" does NOT match" );
        printf("\n");
        fflush(stdout);
      }
#else
        printf("%8u  %8s  %'18u  %'20u  %'20.04f  %'20.04f\n",
            pass_it,
            mem_content_compare_match?"y":"n",
            number_of_blocks_to_send,
            current_it_transfer_size/1024 ,
            (float) ((float)(transfer_wr_size) / (float)(1000.0)) / wr_elapsed_in_sec,
            (float) ((float)(transfer_rd_size) / (float)(1000.0)) / rd_elapsed_in_sec
            );
#endif
    } else {
#ifdef VERBOSE_MODE
      // we do not compare any data, only display bandwidth
      printf("#%u passed. Write BW = %'6.04f kB/s  (size %'6.02f kB  the test took: WR %'3.03f s))\n",
          pass_it,
          (float) ((float)(transfer_wr_size) / (float)(1000.0)) / wr_elapsed_in_sec, 
          (float)transfer_wr_size / (float)1000.0,
          wr_elapsed_in_sec
          );
      fflush(stdout);
#else
        printf("%8u  %8s  %'18u  %'20u  %'20.04f  %20s\n",
            pass_it,
            mem_content_compare_match?"y":"n",
            number_of_blocks_to_send,
            current_it_transfer_size/1024 ,
            (float) ((float)(transfer_wr_size) / (float)(1000.0)) / wr_elapsed_in_sec,
            "---"
            );
#endif

    }
    //end of pass iteration, loop until all runs have completed
  }
  
  // Test finished, release all data structures
  printf("\n");
  printf("all done\n\n");

  if (pass_failed != 0)
  {
    printf(" TEST KO !!\n");
    printf(" %u of %u runs failed\n", pass_failed, pass_it);
  }
  else
  {
    printf(" TEST OK \n");
    printf(" total runs: %u\n", pass_it);
  }

  printf("\n");
  printf("Release buffers memory\n");
  free(write_buffer);
  free(read_buffer);

  printf("Release communication sockets and resources\n");
  hn_end();

  printf("bye bye\n\n");

  return 0;
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

