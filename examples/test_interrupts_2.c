#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hn.h"

int main(int argc, char **argv)
{
  uint32_t send;
  uint32_t wait;
  uint32_t tile;
  uint16_t vector_mask;
  uint32_t id;
  uint32_t rv;
  uint16_t interrupt;

  if (argc != 5) {
    printf("Use: %s <send/wait> tile vector interrupt\n", argv[0]);
    exit(1);
  }
  send = 0;
  wait = 0;
  if (!strncmp(argv[1], "send", 4)) {
    send = 1;
  } else {
    wait = 1;
  }
  tile = atoi(argv[2]);
  vector_mask = atoi(argv[3]);
  interrupt = atoi(argv[4]);

  hn_filter_t filter;
  if (hn_initialize(filter, 1, 1, 0, 0) != HN_SUCCEEDED) {
    printf("HN initialization error\n");
    exit(1);
  }

  printf("registering interrupt...\n");
  if ( hn_register_int(tile, vector_mask, &id) != HN_SUCCEEDED ) {
    printf("error while registering interrupt\n");
    hn_end();
    exit(1);
  }
  printf("interrupt registered with id %d, tile %d, vector %08x\n", id, tile, vector_mask);

  if (send) {
    printf("Sending interrupt %08x to tile %d\n", interrupt, tile);
    hn_interrupt(id, interrupt);
  }

  if (wait) {
    printf("Waiting for interrupt %08x from tile %d\n", interrupt, tile);
    rv = hn_wait_int(id, interrupt);
    if (rv == HN_SUCCEEDED) {
      printf("interrupt received\n");
    } else {
      printf("Error!\n");
    }
  }

  printf("releasing interrupt...\n");
  hn_release_int(id);

  hn_end();

  return 1;
} 
