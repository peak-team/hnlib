///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// This application implements a terminal with PEAK units running in a MANGO tile.
// The application receives the following parameters:
//   - <tile>: Tile where the PEAK unit is located
//   - <core>: Core of PEAK to get the terminal
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include "../hn.h"
#include<stdio.h>
#include<unistd.h>
#include <pthread.h>
#include <time.h>



#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#ifndef _WIN32
#include <unistd.h>
#endif

#include "profpga.h"
#include "profpga_error.h"

#include "mmi64.h"
#include "mmi64_defines.h"
#include "mmi64_module_regif.h"
#include "mmi64_module_axi_master.h"
#include "mmi64_module_upstreamif.h"

#include "profpga_logging.h"
#include "libconfig.h"

// here we use the mmi64 cross-platform definitions to implement
// a OS (semi-)independent threading
#include "mmi64_crossplatform.h"


#ifdef HDL_SIM
  #include "hn_daemon.h"
#endif

//#define _BLOCK_ACCESS_
//#define _SINGLE_ACCESS_
//
#define CHECK(status)  if (status!=E_PROFPGA_OK) { \
  printf(NOW("ERROR: %s\n"), profpga_strerror(status)); \
  return status;  }

//------------------------------------------------------------------------------
pthread_t  th_writer;

uint32_t tile;
uint32_t core;
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
void *thread_writer(void *argv) {

  char ch;
  struct timespec ts;

  ts.tv_sec = 0;
  ts.tv_nsec = 500000;

  while(1) {
    ch = getchar();
    
    // keystrokes always go to core 0
    hn_peak_send_char(tile, 0, ch);
    nanosleep(&ts, NULL);
  }
}

//-------------------------------------------------------------------------------

  pthread_t  th_boot_daemon;

  void *thread_boot_daemon(void *argv) {
  int status_boot_daemon;
  
  printf("boot daemon\n");
  fflush(stdout);

  status_boot_daemon = hdl_sim_boot_daemon("mmi64");
  
  printf("boot daemon error...should never display this message\n");
  fflush(stdout);
  
  return 0;
}

//-------------------------------------------------------------------------------
#ifdef HDL_SIM 
mmi64_error_t mmi64_main(int argc, char* argv[])
{
  unsigned int rv;
  uint32_t     init_lib;
  uint32_t     rcx_core;
 
  printf("\n\n");
  printf("-------------------------\n");
  printf("-------------------------\n");
  
  printf("Create thread to boot daemon\n");
  fflush(stdout);
  
  rv = pthread_create(&th_boot_daemon, NULL, thread_boot_daemon, (void *)NULL);
  if (rv) {
    printf("Error, thread to boot daemon could not be run\n");
    printf("exit now\n");
    fflush(stdout);
    exit(1);
  }
/*
  int status_boot_daemon; 
  printf("boot daemon\n");
  fflush(stdout);
  status_boot_daemon = hdl_sim_boot_daemon("mmi64");
  printf("\n");
  printf("daemon booted, check status\n");
  fflush(stdout);  if (status_boot_daemon != 0) {
    printf("Error booting daemon....\n");
    printf("quit now\n");
    exit(1);
  }
*/
  
  tile = 0;
  core = 0;
  
  printf("Launching hn test application to boot peakos\n");
  fflush(stdout);
  
  // We prepare the filter to get console access
  hn_daemon_socket_filter filter;
  filter.target = HN_FILTER_TARGET_PEAK;
  filter.mode   = HN_FILTER_APPL_MODE_CONSOLE;
  filter.tile   = tile;
  filter.core   = core;
  

  printf("vaig a iniziar la llib, espere q inicie el daemon...test and fail, sleeping 200\n");
  fflush(stdout);
  sleep(200);
  printf("\n\n");
  printf("espere que el daemon ja estiga en marxa....... inicie hn_lib\n");
  fflush(stdout);
  //test purposes... for reset after initialization activate following line
  //init_lib = hn_initialize(filter, NO_PARTITION_STRATEGY, 1, 1, 1);
  init_lib = hn_initialize(filter, NO_PARTITION_STRATEGY, 1, 0, 1);



  // We init the hn library
  if (init_lib != HN_SUCCEEDED) {
    printf("Error  %u, not able to initialize the HN library\n", init_lib);
    fflush(stdout);
    sleep(5);
    return(1);
  }
/*
  // We spawn the writer thread
  rv = pthread_create(&th_writer, NULL, thread_writer, (void *)NULL);
  if (rv) {
    printf("Error, thread could not be run\n");
    exit(1);
  }

  // now we get characters from the HN library and print them out
  char c;
  while (true) {
    rv = hn_peak_get_console_char(&rcx_core, &c);
    if (rv != HN_SUCCEEDED) {
      hn_print_error(rv);
      exit(1);
    }
    printf("%c", c);
    fflush(stdout);
  }
*/  
  printf("sleeping 5secs\n");
  fflush(stdout);
  sleep(50);
  printf("leaving hn test app\n");
  return 0;
}
#endif
