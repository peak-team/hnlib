///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 15, 2018
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Initialize hn library, request a new buffer in shared memrory, destroy buffer in shared memory
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "hn.h"

int main(int argc, char **argv)
{
  uint32_t   rv;
  void      *shm_buffer_base_addr_ptr;
  uint32_t   shm_buffer_size;
  uint32_t   number_of_entries;
  uint32_t   entry_width;


  int        random_number;
  uint64_t   value;
  uint32_t   it;

  //Write to DDR memory  
  uint64_t  *shm_buffer_ptr_64bit;
  uint32_t   number_of_entries_to_send;
                                   // function call variables
  void      *transfer_wr_src_addr;
  uint32_t   transfer_wr_dst_tile;
  uint32_t   transfer_wr_dst_addr;
  uint32_t   transfer_wr_is_blocking;
  uint32_t   transfer_wr_size;

  // Read from DDR memory
  uint64_t  *shm_buffer_rd_ptr_64bit;
  uint32_t   number_of_entries_to_read;
                                   // function call variables
  void      *transfer_rd_dst_addr; // address of shared memory buffer to store data read from DDR memory in HN system
  uint32_t   transfer_rd_src_tile; // tile where DDR is located
  uint32_t   transfer_rd_src_addr; // address to read from DDR memory
  uint32_t   transfer_rd_is_blocking; 
  uint32_t   transfer_rd_size;     // number of bytes to read from DDR memory

  //
  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;
   
  printf("\n");
  printf("---------------------------------\n");
  printf("     DEMO Application\n");
  printf("       Initialize hn library\n");
  printf("       Request buffer in shared memory area\n");
  printf("       Close buffer in shared memory area\n");
  printf("       quit\n");
  printf("---------------------------------\n");
  printf("\n");

  // We initialize the hn library with the filter and the UPV's partition strategy
  // with reset
  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");

  shm_buffer_size = 4096; // bytes

  printf("press any key to request a shm buffer of %u bytes\n", shm_buffer_size);
  getchar();
  rv = hn_shm_malloc(shm_buffer_size, &shm_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error requesting buffer in shared memory, returned: %u\n", rv);
    hn_end();
    exit(1);
  }

  printf("I have been granted a buffer on addr 0x%08lx    of %u bytes\n", (unsigned long)shm_buffer_base_addr_ptr, shm_buffer_size);
  printf("\n");

  printf("press any to test write operation from application\n");
  getchar();
  srand(time(NULL));
  random_number             = rand();// + RAND_MAX/2;
  entry_width               = 64; // 64byte values, to mimic block width in DDR memory
  number_of_entries         = shm_buffer_size/entry_width;
  number_of_entries_to_send = 4; // so we can send partial buffer
  value                     = (uint64_t)random_number;
  // set pointers
  shm_buffer_ptr_64bit =  (uint64_t *)shm_buffer_base_addr_ptr;
  printf("Test will write %d values of %d bytes to fill shm_buffer\n", number_of_entries, entry_width);
  printf("initial value: %016lX\n", value);
  printf("            +: %016lX\n", 0x0001000000000000);
  for (it = 0; it < number_of_entries; it++)
  {
    value = random_number + (0x0001000000000000 * it) + 2*it;
    *shm_buffer_ptr_64bit = value;
     // increase ptr to next uint64 position 
    shm_buffer_ptr_64bit++;
  }

  shm_buffer_ptr_64bit =  (uint64_t *)shm_buffer_base_addr_ptr;
  for (it = 0; it < number_of_entries; it++)
  {
   printf("  0x%016lx    0X%016lx\n", (unsigned long)shm_buffer_ptr_64bit, *shm_buffer_ptr_64bit);
   shm_buffer_ptr_64bit++;
  }

  // write to DDR-memory from shared memory buffer test
  printf("press any key to send notification to hn_daemon to send partial buffer to HN system\n");
  printf("Test will send %d values of %d bytes\n", number_of_entries_to_send, entry_width);
  getchar();
  transfer_wr_dst_tile    = 0;
  transfer_wr_dst_addr    = 0x0000F000;;
  transfer_wr_is_blocking = 0;
  transfer_wr_size        = number_of_entries_to_send *entry_width; // sending 4 values
  shm_buffer_ptr_64bit    = (uint64_t *)shm_buffer_base_addr_ptr;
  shm_buffer_ptr_64bit    = shm_buffer_ptr_64bit + 2;
  transfer_wr_src_addr    = (void *)shm_buffer_ptr_64bit; // firt sent value is the third (base + 2)
  printf("  transfer configuration: \n");
  printf("    base addr: %p\n",       (void *)shm_buffer_base_addr_ptr);
  printf("    src addr:  %p\n",       (void *)transfer_wr_src_addr);
  printf("    size:      %u bytes\n", transfer_wr_size);
  printf("    dst tile:  %u\n",       transfer_wr_dst_tile);
  printf("    dst addr:  0x%08X\n",   transfer_wr_dst_addr);
  printf("    %sblocking transfer\n", transfer_wr_is_blocking?"":"non-");

  hn_shm_write_memory(transfer_wr_src_addr , transfer_wr_size, transfer_wr_dst_addr, transfer_wr_dst_tile, transfer_wr_is_blocking);
  printf("...done\n\n");

  // Read from DDR-memory to shared memory buffer test
  shm_buffer_rd_ptr_64bit   = (uint64_t *)shm_buffer_base_addr_ptr;
  shm_buffer_rd_ptr_64bit   = shm_buffer_rd_ptr_64bit + 8; // store received data at eighth position (data is received in words of 64 bits)
  transfer_rd_dst_addr      = (void *)shm_buffer_rd_ptr_64bit;
  transfer_rd_src_tile      = 0;
  transfer_rd_src_addr      = 0x000A0000;
  transfer_rd_is_blocking   = 1;
  number_of_entries_to_read = 8;
  transfer_rd_size          = number_of_entries_to_read * entry_width; // will read 512 bytes -> 
  printf("press any key to send notification to hn_daemon to read memory from DDR in HN system and store data into shm buffer\n");
  getchar();
  printf("  transfer parameters\n"); 
  printf("    transfer size:      %u bytes\n", transfer_rd_size);
  printf("    src tile:           %u\n",       transfer_rd_src_tile);
  printf("    src addr:           0x%08X\n",   transfer_rd_src_addr);
  printf("    shm_buff addr:      %p\n",       (void *)transfer_rd_dst_addr);
  printf("    base addr:          %p\n",       (void *)shm_buffer_base_addr_ptr);
  printf("    %sblocking transfer\n", transfer_rd_is_blocking?"":"non-");

  hn_shm_read_memory(transfer_rd_dst_addr , transfer_rd_size, transfer_rd_src_addr, transfer_rd_src_tile, transfer_rd_is_blocking);
  printf("...done\n\n");


  shm_buffer_rd_ptr_64bit = (uint64_t *)transfer_rd_dst_addr;
  printf("Test read %u values of %u bytes from address 0x%08lx \n", number_of_entries_to_read, entry_width, (unsigned long)shm_buffer_rd_ptr_64bit);
  for (it = 0; it < number_of_entries_to_read; it++)
  {
   printf("  0x%016lX    0X%016lX\n", (unsigned long)shm_buffer_rd_ptr_64bit, *shm_buffer_rd_ptr_64bit);
   shm_buffer_rd_ptr_64bit++;
  }

  printf("\nall done\n\n");


  printf("press any key to delete the buffer\n");
  getchar();
  rv = hn_shm_free(shm_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error deleting buffer in shared memory, returned: %u", rv);
    hn_end();
    exit(1);
  }

  printf("press any key to finish test");
  getchar();


  printf("Release communication sockets and resources\n");
  hn_end();

  printf("bye bye\n\n");
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

