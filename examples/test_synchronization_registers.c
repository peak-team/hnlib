///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Flich (jflich@disca.upv.es)
//
// Create Date: December 19, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Test of synchronization registers
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <sys/time.h>

#include "hn.h"

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

// 
#define HN_REG_ID_ARRAY_MAX_SIZE                HN_MAX_REGS_PER_TILE * HN_MAX_TILES 

#define HN_TEST_MAX_CONSECUTIVE_OPERATIONS      8
/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/
void cancel_test(uint32_t test_num, uint32_t regs_expected, uint32_t regs_granted, const char *str)
{
  printf("ERROR detected\n");
  printf("       test %u canceled\n", test_num);
  printf("       regs obtained %u  expected %u\n", regs_granted, regs_expected);
  if (str != NULL){
    printf("       %s\n", str);
  }
  printf("       HN_DAEMON restart required\n\n");
  fflush(stdout);
  hn_end();
  exit(1);
}


/*****************************************************************************
*
******************************************************************************/
uint32_t single_register_access_test(uint32_t test_id, uint32_t num_test_iterations, uint32_t num_regs_expected_for_current_test, uint32_t reg_type)
{
  uint32_t id[HN_REG_ID_ARRAY_MAX_SIZE]; // array to store registers ids granted by the hn_lib 
  uint32_t it;
  uint32_t i;
  uint32_t rv = 0;
  uint32_t granted;
  uint32_t num_regs_to_request_out_of_range = num_regs_expected_for_current_test;
  
  for (it = 0; it < num_test_iterations; it++) {
    if (it != 0) printf("\r\033[A");
    printf("  iteration: %4u / %4u\n", it+1, num_test_iterations);

    granted = 0;
    for (i = 0; i < num_regs_expected_for_current_test; i++) {
      rv = hn_get_synch_id(&id[i], 0, reg_type);
      if (rv == HN_SUCCEEDED) {
        granted++;
      }
    }
    if (i != num_regs_expected_for_current_test) {
      cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"not all requested registers were granted");
    }
    
    // attempting to get more registers, the hn_lib has to return error
    granted = 0;
    for (i = 0; i < num_regs_to_request_out_of_range; i++) {
      uint32_t tmp_id;
      
      rv = hn_get_synch_id(&tmp_id, 0, reg_type);
      if(rv == HN_SUCCEEDED) {
        granted++;
      }
    }
    if(granted != 0) {
      cancel_test(test_id, 0, granted, (const char*)"unexpected granted registers");
    }
    
    // now we release all them
    for (i = 0; i < num_regs_expected_for_current_test; i++) {
      hn_release_synch_id(id[i]);
    }
  }
  
  return 0;
}

/*****************************************************************************
*
* test_id             number that identifies current test to be displayed in messages
* num_test_iterations number of times that the complete test will be run
* num_regs_expected_for_current_test number or registes fo reg_type
* reg_type            type of register 
* num_writes          number of consecutive times a single register will be written before reading it
* num_reads           number of consecutive times a single register will be read after writting it
******************************************************************************/
uint32_t single_register_read_write_test(uint32_t test_id, uint32_t num_test_iterations, uint32_t num_regs_expected_for_current_test, uint32_t reg_type, uint32_t num_writes, uint32_t num_reads)
{
  uint32_t id[HN_REG_ID_ARRAY_MAX_SIZE]; // array to store registers ids granted by the hn_lib
  uint32_t wr_value[HN_REG_ID_ARRAY_MAX_SIZE][HN_TEST_MAX_CONSECUTIVE_OPERATIONS]; // array of values to write for each single reg
  uint32_t rd_value[HN_REG_ID_ARRAY_MAX_SIZE][HN_TEST_MAX_CONSECUTIVE_OPERATIONS]; // array containing values read from tilereg
  uint32_t exp_rd_value[HN_REG_ID_ARRAY_MAX_SIZE][HN_TEST_MAX_CONSECUTIVE_OPERATIONS]; // array containing values expected to be read from tilereg
  uint32_t it;
  uint32_t i;    // first  level loop var
  uint32_t j;    // second level loop var
  uint32_t z;    // third  level loop var
  uint32_t rv = 0;
  uint32_t granted;
  struct timeval time; 
  
  gettimeofday(&time,NULL);
  srand((time.tv_sec * 1000) + (time.tv_usec / 1000));

  if (num_writes < 1) {
    printf("WARNING: test %2u , num_writes set to %u, requested value %u was out of range\n", test_id, 1, num_writes);
    num_writes = 1;
  } else if (num_writes > HN_TEST_MAX_CONSECUTIVE_OPERATIONS) {
    printf("WARNING: test %2u , num_writes set to %u, requested value %u was out of range\n", test_id, HN_TEST_MAX_CONSECUTIVE_OPERATIONS, num_writes);
    num_writes = HN_TEST_MAX_CONSECUTIVE_OPERATIONS;
  }
  
  if (num_reads < 1) {
    printf("WARNING: test %2u , num_reads set to %u, requested value %u was out of range\n", test_id, 1, num_reads);
    num_reads = 1;
  } else   if (num_reads > HN_TEST_MAX_CONSECUTIVE_OPERATIONS) {
    printf("WARNING: test %2u , num_reads set to %u, requested value %u was out of range\n", test_id, HN_TEST_MAX_CONSECUTIVE_OPERATIONS, num_reads);
    num_reads = HN_TEST_MAX_CONSECUTIVE_OPERATIONS;
  }

//  printf("Test %2u   iterations %2u   num_writes set to %2u   num_reads set to %2u\n", test_id, num_test_iterations, num_writes, num_reads);
//  fflush(stdout);

  for (it = 0; it < num_test_iterations; it++) {
    
    if (it != 0) printf("\r\033[A");
    printf("  iteration: %4u / %4u\n", it+1, num_test_iterations);
    fflush(stdout);

    granted = 0;
    for (i = 0; i < num_regs_expected_for_current_test; i++) {
      rv = hn_get_synch_id(&id[i], 0, reg_type);
      if (rv == HN_SUCCEEDED) {
        granted++;
      }
    }
    if (i != num_regs_expected_for_current_test) {
      cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"not all requested registers were granted");
    }

    //printf("all regs granted \n");
    //printf("%6s %8s\n", "#","reg_id" );
    //for (i = 0; i < num_regs_expected_for_current_test; i++)
    //  printf("%6u %8u\n", i, id[i]);
    //printf("\n");

    // at this point all the requested regisgters have been granted
    // Set to zero all the regs, and make sure their value is set to zero
    

    // we perform a read of all regs and discard read values, so we reset readreset regs
    if ((reg_type == HN_READRESET_REG_TYPE) || (reg_type == HN_READRESET_INCRWRITE_REG_TYPE)) {
      //printf("Requested reg_type is %s, reading all registers to reset its value\n",
      //    (reg_type == HN_READRESET_REG_TYPE) ? "HN_READRESET_REG_TYPE" :
      //    (reg_type == HN_READRESET_INCRWRITE_REG_TYPE)? "HN_READRESET_INCRWRITE_REG_TYPE":
      //    "UNKNOWN"
      //    );
      //fflush(stdout);
      for(i = 0; i < num_regs_expected_for_current_test; i++) {
        uint32_t val_rd;
        rv = hn_read_synch_register(id[i], &val_rd);
        if (rv != 0) {
          // error reading value
          printf("   reg %4u\n", id[i]);
          fflush(stdout);
          cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"error in initialization (reset), pre-read register value to reset its value");
        }
        //printf("    reset reg %3u   id %4u   rd_val %d\n", i, id[i], val_rd);
      }

    } else if (reg_type == HN_REGULAR_REG_TYPE){
      //printf("Requested reg_type is regular read, writting '0' all registers to reset its value\n");
      //fflush(stdout);
      for(i = 0; i < num_regs_expected_for_current_test; i++) {
        rv = hn_write_synch_register(id[i], 0);
        if (rv != 0) {
          // error writing value
          printf("   reg %4u\n", id[i]);
          cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"error writing initial value");
        }
      }
    } else {
        printf("UNKNOWN reg_type %u\n", reg_type);
        fflush(stdout);
        cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"error in initialization of test");
    }

    // registers are reset, checking that they are really set to 0

    for(i = 0; i < num_regs_expected_for_current_test; i++) {
      uint32_t val_rd;

      rv = hn_read_synch_register(id[i], &val_rd);
      if (rv != 0) {
        // error reading value
        printf("   reg %4u\n", id[i]);
        fflush(stdout);
        cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"error in initialization (reset), getting register value");
      }
      if (val_rd != 0) {
        // wrong value read
        printf("   reg %4u  expected %u, read %u\n", id[i], 0,val_rd);
        cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"error in initialization (reset), read unexpected value from register");
      }
    }

    //printf("ok all registers are properly set to 0\n\n");

    // set values that will be written for each single register
    for(i = 0; i < num_regs_expected_for_current_test; i++) {
      for( j = 0; j < num_writes; j++) {
        uint32_t rand_val  = rand() % 128;
        wr_value[i][j] = rand_val;
      }
    }
    // prepare the array of expected values to be read from registers 

    if (reg_type == HN_READRESET_REG_TYPE) {
      //regular write - read reset register
      // in this case, we will read a value different from 0 the first time,
      // the first value will be the last written
      // the rest of readings on the same reg will be 0
      for(i = 0; i < num_regs_expected_for_current_test; i++) {
        exp_rd_value[i][0] = wr_value[i][num_writes-1];
        for(j = 1; j < num_reads; j++) {
          exp_rd_value[i][j] = 0;
        }
      }
    }else if (reg_type == HN_READRESET_INCRWRITE_REG_TYPE) {
      //regular write - read reset register
      // in this case, we will read a value different from 0 the first time,
      // the first value will be the sum of all the written last written
      // the rest of readings on the same reg will be 0
      for(i = 0; i < num_regs_expected_for_current_test; i++) {
        uint32_t value = 0;
        for(j = 0; j < num_writes; j++) {
          value = value + wr_value[i][j];
        }
        exp_rd_value[i][0] = value;
        for(j = 1; j < num_reads; j++) {
          exp_rd_value[i][j] = 0;
        }
      }
    } else {
      //regular read/write register
      // in this case, the value will be the same for all the readings,
      // the value will be the last written
      for(i = 0; i < num_regs_expected_for_current_test; i++) {
        for(j = 0; j < num_reads; j++) {
          exp_rd_value[i][j] = wr_value[i][num_writes-1];  
        }
      }
    }


    // We are now ready, let's perform the test
    // perform the read/write test
    for(i = 0; i < num_regs_expected_for_current_test; i++) {
      // let's first write the register
      for (j = 0; j < num_writes; j++ ) {
        rv = hn_write_synch_register(id[i], wr_value[i][j]);
        if (rv != 0) {
          // error writing value
          printf("   single reg %4u\n", id[i]);
          fflush(stdout);
          cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"function returned error writing synch reg");
        }
        //printf("   wr reg #%4u   wr loop #%u  val %u\n", i, j, wr_value[i][j]);
      }

      // let's read all the registers, checking that requested reg 
      for(j = 0; j < num_regs_expected_for_current_test; j++) {
        for (z = 0; z < num_reads; z++) {
          uint32_t val_rd;

          if ( (i != j) && (z > 0)) continue;

          rv = hn_read_synch_register(id[j], &val_rd);
          //printf("   wr reg #%4u   rd reg #%4u   rd loop #%u   val %u\n",i, j, z, val_rd);
          fflush(stdout);

          if (rv != 0) {
            // error reading value
            printf("   reg %4u\n", id[j]);
            cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"function returned error reading synch reg");
          }

          if (i == j) {
            // this is the register than can be read multiple times
            // read value must match with written value
            //printf("   checking written reg\n");
            if (val_rd != exp_rd_value[i][z]) {
              // wrong value read
              printf("   reg %4u  expected %u, read %u\n", id[i], exp_rd_value[i][z], val_rd);
              cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"read unexpected value from written register");
            }
          }  else {
            // value has to be zero
            //printf("   checking other reg\n");
            if (val_rd != 0) {
              // wrong value read
              printf("   reg %4u  expected %u, read %u\n", id[j], 0, val_rd);
              cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"read unexpected value from un-written register");
            }
          }
        }
      }
      // let's reset the written value for next iteration
      rv = hn_write_synch_register(id[i], 0);
      if (rv != 0) {
        // error writing value
        printf("   reg %4u\n", id[i]);
        cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"function returned error writing reset value on synch reg");
      }
    }

    // Second part of the test
    // Now let's write and then read all the registers 
    for(i = 0; i < num_regs_expected_for_current_test; i++) {
      for (j = 0; j < num_writes; j++ ) {
        rv = hn_write_synch_register(id[i], wr_value[i][j]);
        if (rv != 0) {
          // error writing value
          printf("   all write reg %4u\n", id[i]);
          cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"function returned error writing synch reg");
        }
      }
    }
    for(i = 0; i < num_regs_expected_for_current_test; i++) {
      for (j = 0; j < num_reads; j++ ) {
        rv = hn_read_synch_register(id[i], &rd_value[i][j]);
        if (rv != 0) {
          // error reading value
          printf("   all read reg %4u\n", id[i]);
          fflush(stdout);
          cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"function returned error reading synch reg");
        }
      }
    }
    for(i = 0; i < num_regs_expected_for_current_test; i++) {
      for (j = 0; j < num_reads; j++ ) {
        if (exp_rd_value[i][j] != rd_value[i][j]) {
          // wrong value read
          printf("   error checking data for entire wr/rd operation on reg \n");
          printf("   reg %4u  expected %u, read %u\n", id[i], exp_rd_value[i][j], rd_value[i][j]);
          cancel_test(test_id, num_regs_expected_for_current_test, granted, (const char*)"read unexpected value from register");
        }
      }
    }

    // End of current iteration
    //   let's release all the synch ids, to then re-acquire then in next iteration
    for (i = 0; i < num_regs_expected_for_current_test; i++) {
      hn_release_synch_id(id[i]);
    }
  }

  return 0;
}

/*****************************************************************************
*
******************************************************************************/
uint32_t group_register_access_test(uint32_t test_id, uint32_t num_test_iterations, uint32_t num_groups_expected_for_current_test, uint32_t group_size,  uint32_t reg_type)
{
  uint32_t id[HN_REG_ID_ARRAY_MAX_SIZE]; // array to store registers ids granted by the hn_lib 
  uint32_t it;
  uint32_t i;
  uint32_t rv = 0;
  uint32_t granted;
  uint32_t num_groups_to_request_out_of_range = num_groups_expected_for_current_test;

  //printf("attemtping to run %u iterations to get %u grops of %u registers of type %u\n", num_test_iterations, num_groups_expected_for_current_test,group_size, reg_type);

  for (it = 0; it < num_test_iterations; it++) {
    if (it != 0) printf("\r\033[A");
    printf("  iteration: %4u / %4u\n", it+1, num_test_iterations);

    granted = 0;
    for (i = 0; i < num_groups_expected_for_current_test; i++) {
      //printf("  getting group %2u of %2u\n", i+1, num_groups_expected_for_current_test);
      rv = hn_get_synch_id_array(&id[i], group_size, 0, reg_type);
      if (rv == HN_SUCCEEDED) {
        granted++;
      } else {
        printf("\r  FAILED");
      }
    }
    if (granted != num_groups_expected_for_current_test) {
      cancel_test(test_id, num_groups_expected_for_current_test, granted, (const char*)"not all requested groups of registers were granted");
    }
    
    // attempting to get more registers, the hn_lib has to return error
    granted = 0;
    for (i = 0; i < num_groups_to_request_out_of_range; i++) {
      uint32_t tmp_id;
      
      rv = hn_get_synch_id_array(&tmp_id, group_size, 0, reg_type);
      if(rv == HN_SUCCEEDED) {
        granted++;
      }
    }
    if(granted != 0) {
      cancel_test(test_id, 0, granted, (const char*)"unexpected group of registers granted");
    }
    
    // now we release all them
    for (i = 0; i < num_groups_expected_for_current_test; i++) {
      hn_release_synch_id_array(id[i], group_size);
    }
  }
  
  return 0;
}

/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/
int main(int argc, char **argv)
{
  uint32_t rv;

  uint32_t test_id;
  uint32_t group_size;
  uint32_t num_groups;
  uint32_t test_num_writes;
  uint32_t test_num_reads;
  uint32_t  num_regs_expected_for_current_test;
  uint32_t  num_test_iterations;
  
  // FOLLOWING NUMBERS ARE PER TILE !!!
  uint32_t  num_regs_reg_type                      = HN_REGULAR_REG_TYPE_HIGH_ID             - HN_REGULAR_REG_TYPE_LOW_ID             + 1;
  uint32_t  num_regs_readreset_reg_type            = HN_READRESET_REG_TYPE_HIGH_ID           - HN_READRESET_REG_TYPE_LOW_ID           + 1;
  uint32_t  num_regs_readreset_incwrite_reg_type   = HN_READRESET_INCRWRITE_REG_TYPE_HIGH_ID - HN_READRESET_INCRWRITE_REG_TYPE_LOW_ID + 1;
 
  uint32_t num_tiles, num_tiles_x, num_tiles_y;

  int       cmd_opt;
  //uint32_t  pass_it;
  //uint32_t  pass_failed;
  uint32_t  reset_system;

  //pass_failed         = 0;
  // set default values for application parameters
  //pass_it             = 0;
  reset_system        = 0;
  num_test_iterations = 2;

  
  //---------------------------------------------------------------------------


  test_id = 1;
  // Display Welcome Message
  printf("\n\n");
  printf("-------------------------------------------------------------------------------\n");
  printf("-------------------------------------------------------------------------------\n");
  printf("  \n");
  printf("  TEST Application\n");
  printf("    Initialize hn library\n");
  printf("    Reset system\n");
  printf("    Run different register access tests \n");
  printf("       Test  %2u - get access to single registers    of type: regular type\n", test_id++);
  printf("       Test  %2u - get access to single registers    of type: regular write     - read reset\n", test_id++);
  printf("       Test  %2u - get access to single registers    of type: incremental write - read reset\n", test_id++);

  printf("       Test  %2u - get access to groups of registers of type: regular type\n", test_id++);
  printf("       Test  %2u - get access to groups of registers of type: regular write     - read reset\n", test_id++);
  printf("       Test  %2u - get access to groups of registers of type: incremental write - read reset\n", test_id++);

  printf("       Test  %2u - write once  / read once  (check content) all single registers   of type: regular type\n", test_id++);
  printf("       Test  %2u - write twice / read once  (check content) all single registers   of type: regular type\n", test_id++);
  printf("       Test  %2u - write once  / read twice (check content) all single registers   of type: regular type\n", test_id++);
  printf("       Test  %2u - write twice / read twice (check content) all single registers   of type: regular type\n", test_id++);

  printf("       Test  %2u - write once  / read once  (check content) all single registers   of type: regular write     - read reset\n", test_id++);
  printf("       Test  %2u - write twice / read once  (check content) all single registers   of type: regular write     - read reset\n", test_id++);
  printf("       Test  %2u - write once  / read twice (check content) all single registers   of type: regular write     - read reset\n", test_id++);
  printf("       Test  %2u - write twice / read twice (check content) all single registers   of type: regular write     - read reset\n", test_id++);

  printf("       Test  %2u - write once  / read once  (check content) all single registers   of type: incremental write - read reset\n", test_id++);
  printf("       Test  %2u - write twice / read once  (check content) all single registers   of type: incremental write - read reset\n", test_id++);
  printf("       Test  %2u - write once  / read twice (check content) all single registers   of type: incremental write - read reset\n", test_id++);
  printf("       Test  %2u - write twice / read twice (check content) all single registers   of type: incremental write - read reset\n", test_id++);


  printf("  \n");
  printf("  \n");
//  printf("  press ctrl+c to stop the test\n");
//  printf("    this will trigger the routine to stop the hn_lib in a soft and controlled manner\n");
  printf("  NON CANCELABLE TEST, please wait until the end of the test\n");
  printf("  in case of cancelation, please, then restart the hn_daemon\n");
  printf("  \n");
  printf("--------------------------------------------------------------------------------\n");



  while ((cmd_opt = getopt (argc, argv, "ri:h")) != -1)
  {
    switch (cmd_opt) 
    {
      case 'r':
        printf("option -> reset, will reset system before running test\n");
        reset_system = 1;
        break;
      case 'i':
        num_test_iterations = atoi(optarg);
        if (num_test_iterations < 1) num_test_iterations = 1;
        printf("option -> set test number of iterations : %d\n", num_test_iterations);
        break;
      case 'h':
        printf("HELP\n");
        printf("supported options\n");
        printf("  -i <number_of_test_runs>\n");
        printf("  -r reset hn_system before running the test\n");
        printf("  \n");
        return -1;
      case '?':
        if ((optopt == 'i') )
          printf ("Option -%c requires an argument\n", optopt);
        else if (isprint (optopt))
          printf ("Unknown option `-%c'\n", optopt);
        else
          printf ("Unknown option character `\\x%x'\n", optopt);
        return 1;
      default:
        return -1;
    }
  }


  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile = 999;
  filter.core = 999;

  // We initialize the hn library with the filter and the UPV's partition strategy
  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");
  sleep(1);

  if(reset_system) {
    printf("reset system before run the test\n");
    fflush(stdout);
    hn_reset(0);
  }
  printf("   ...done\n");

  // Now we collect data from the system and print it
  hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
  printf("\n");
  printf("System info\n");
  printf("----------------");
  printf("Number of tiles: %d (%d x %d)\n", num_tiles, num_tiles_x, num_tiles_y);
  printf("register                       regular type: %u\n", num_regs_reg_type);
  printf("register     regular write - readreset type: %u\n", num_regs_readreset_reg_type);
  printf("register incremental write - readreset type: %u\n", num_regs_readreset_incwrite_reg_type);
  printf("\n");


  //------------------------------------
  // Let's start the test
  test_id = 1;

  // test 1
  // REQUEST REGISTER --> REGULAR WRITE - REGULAR READ
  num_regs_expected_for_current_test = num_tiles * num_regs_reg_type;
  printf("TEST %2u attempting to get %u regular registers %u times...\n", test_id, num_regs_expected_for_current_test, num_test_iterations);
  rv = single_register_access_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_REGULAR_REG_TYPE);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  // test 2
  // REQUEST REGISTER --> REGULAR WRITE - READ RESET
  num_regs_expected_for_current_test = num_tiles * num_regs_readreset_reg_type;
  printf("TEST %2u: attempting to get %u READRESET registers %u times...\n", test_id, num_regs_expected_for_current_test, num_test_iterations);
  rv = single_register_access_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_REG_TYPE);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  // test 3
  // REQUEST REGISTER -->  INCREMENTAL WRITE - READ RESET
  num_regs_expected_for_current_test = num_tiles * num_regs_readreset_incwrite_reg_type;
  printf("TEST %2u: attempting to get %u READRESET_INCRWRITE registers %u times...\n", test_id, num_regs_expected_for_current_test, num_test_iterations);
  rv = single_register_access_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_INCRWRITE_REG_TYPE);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  // test 4
  // REQUEST GROUP OF REGISTERS --> REGULAR WRITE - REGULAR RESET
  group_size = 4;
  num_groups = num_tiles * (num_regs_reg_type / group_size);
  printf("TEST %2u: attempting to get %u groups of %u registers of REGULAR type %u times...\n", test_id, num_groups, group_size, num_test_iterations);
  rv = group_register_access_test(test_id, num_test_iterations, num_groups, group_size, HN_REGULAR_REG_TYPE);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;
  
  // test 5
  // REQUEST GROUP OF REGISTERS --> REGULAR WRITE - READ RESET
  group_size = 4;
  num_groups = num_tiles * (num_regs_readreset_reg_type / group_size);
  printf("TEST %2u: attempting to get %u groups of %u registers of type READRESET %u times...\n", test_id, num_groups, group_size, num_test_iterations);
  rv = group_register_access_test(test_id, num_test_iterations, num_groups, group_size, HN_READRESET_REG_TYPE);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  // test 6
  // REQUEST GROUP OF REGISTERS --> REGULAR WRITE - READ RESET
  group_size = 4;
  num_groups = num_tiles * (num_regs_readreset_reg_type / group_size);
  printf("TEST %2u: attempting to get %u groups of %u registers of type READRESET_INCRWRITE %u times...\n", test_id, num_groups, group_size, num_test_iterations);
  rv = group_register_access_test(test_id, num_test_iterations, num_groups, group_size, HN_READRESET_INCRWRITE_REG_TYPE);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;


  // get all regs
  // set to 0 all registers,
  // write a value in one reg
  // read value of that reg, and check it contains the expected value
  // read the rest of registers checking they contaib 0
  // write a 0 in this reg
  //    loop until this operation is performed on all the regs
  //test_id = 5;
  
  // WRITE & READ REGISTER --> REGULAR WRITE - REGULAR READ
  test_num_writes = 1;
  test_num_reads = 1;
  num_regs_expected_for_current_test = num_tiles * num_regs_reg_type;
  printf("TEST %2u attempting to get %u regular registers and perform %2u write / %2u read operations to it %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_REGULAR_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 2;
  test_num_reads = 1;
  num_regs_expected_for_current_test = num_tiles * num_regs_reg_type;
  printf("TEST %2u attempting to get %u regular registers and perform %2u write / %2u read operations to it %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_REGULAR_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 1;
  test_num_reads = 2;
  num_regs_expected_for_current_test = num_tiles * num_regs_reg_type;
  printf("TEST %2u attempting to get %u regular registers and perform %2u write / %2u read operations to it %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_REGULAR_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 2;
  test_num_reads = 2;
  num_regs_expected_for_current_test = num_tiles * num_regs_reg_type;
  printf("TEST %2u attempting to get %u regular registers and perform %2u write / %2u read operations to it %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_REGULAR_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;
  

  // WRITE & READ REGISTER --> REGULAR WRITE - READ RESET
  test_num_writes = 1;
  test_num_reads = 1;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_reg_type;
  printf("TEST %2u attempting to get %u READRESET registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 2;
  test_num_reads = 1;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_reg_type;
  printf("TEST %2u attempting to get %u READRESET registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 1;
  test_num_reads = 2;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_reg_type;
  printf("TEST %2u attempting to get %u READRESET registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 2;
  test_num_reads = 2;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_reg_type;
  printf("TEST %2u attempting to get %u READRESET registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;


  // WRITE & READ REGISTER --> INCREMENTAL WRITE - READ RESET
  test_num_writes = 1;
  test_num_reads = 1;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_incwrite_reg_type;
  printf("TEST %2u attempting to get %u READRESET_INCRWRITE registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_INCRWRITE_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 2;
  test_num_reads = 1;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_incwrite_reg_type;
  printf("TEST %2u attempting to get %u READRESET_INCRWRITE registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_INCRWRITE_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 1;
  test_num_reads = 2;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_incwrite_reg_type;
  printf("TEST %2u attempting to get %u READRESET_INCRWRITE registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_INCRWRITE_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;

  test_num_writes = 2;
  test_num_reads = 2;
  num_regs_expected_for_current_test = num_tiles *  num_regs_readreset_incwrite_reg_type;
  printf("TEST %2u attempting to get %u READRESET_INCRWRITE registers and perform %2u write / %2u read operations to it for %u times...\n", test_id, num_regs_expected_for_current_test, test_num_writes, test_num_reads, num_test_iterations);
  fflush(stdout);
  rv = single_register_read_write_test(test_id, num_test_iterations, num_regs_expected_for_current_test, HN_READRESET_INCRWRITE_REG_TYPE, test_num_writes, test_num_reads);
  if (rv == 0) printf("TEST %2u PASSED\n\n", test_id);
  else printf("TEST %2u FAILED\n\n", test_id);
  test_id++;


  printf("end of all single tests.\n");
  printf("All tests sucessfully PASSED\n");
  printf("TEST OK\n");

  // We end
  hn_end();
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

