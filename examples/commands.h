///////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
// 
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and 
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is 
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
// 
// contact: jflich@disca.upv.es
//-----------------------------------------------------------------------------
//
// Company:  GAP (UPV)  
// Engineer: R. Tornero (ratorga@disca.upv.es)
// 
// Create Date: 
// Design Name: 
// Module Name: 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
//
//  
//
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////

// Output from the FPGA commands
#define COMMAND_CONSOLE                         (u_int8_t)0
#define COMMAND_TILE_STATS_FIRST_ITEM           (u_int8_t)1
#define COMMAND_TILE_STATS_REMAINING_ITEMS      (u_int8_t)2
#define COMMAND_TILEREG_FIRST_ITEM              (u_int8_t)3
#define COMMAND_TILEREG_REMAINING_ITEMS         (u_int8_t)4
#define COMMAND_ECHO                            (u_int8_t)5
#define COMMAND_DEBUG_MC_FIRST_ITEM             (u_int8_t)10
#define COMMAND_DEBUG_MC_REMAINING_ITEM         (u_int8_t)11
#define COMMAND_UNIT_DATA                       (u_int8_t)16
#define COMMAND_UNIT_DATA_FIRST                 (u_int8_t)16
#define COMMAND_UNIT_DATA_REMAINING             (u_int8_t)17
#define COMMAND_DEBUG_U2M_FIRST_ITEM            (u_int8_t)18
#define COMMAND_DEBUG_U2M_REMAINING_ITEMS       (u_int8_t)19
#define COMMAND_DEBUG_M2U_FIRST_ITEM            (u_int8_t)20
#define COMMAND_DEBUG_M2U_REMAINING_ITEMS       (u_int8_t)21
#define COMMAND_DEBUG_INJECT_FIRST_ITEM         (u_int8_t)12
#define COMMAND_DEBUG_INJECT_REMAINING_ITEMS    (u_int8_t)13
#define COMMAND_DEBUG_EJECT_FIRST_ITEM          (u_int8_t)14
#define COMMAND_DEBUG_EJECT_REMAINING_ITEMS     (u_int8_t)15
#define COMMAND_MC_DATA_FIRST_ITEM              (u_int8_t)22
#define COMMAND_MC_DATA_REMAINING_ITEMS         (u_int8_t)23
#define COMMAND_DEBUG_TILEREG_FIRST_ITEM        (u_int8_t)30
#define COMMAND_DEBUG_TILEREG_REMAINING_ITEMS   (u_int8_t)31

// input to the FPGA commands
#define COMMAND_CONFIGURE_DEBUG_AND_STATS       (u_int8_t)33
#define COMMAND_CONFIGURE_TILE                  (u_int8_t)34
#define COMMAND_READ_TILEREG                    (u_int8_t)35
#define COMMAND_WRITE_TILEREG                   (u_int8_t)36
#define COMMAND_READ_MEMORY_BLOCK               (u_int8_t)40
#define COMMAND_WRITE_MEMORY_BLOCK              (u_int8_t)41
#define COMMAND_PROTOCOL_WRITE                  (u_int8_t)42
#define COMMAND_RESET_SYSTEM                    (u_int8_t)43
#define COMMAND_CLOCK                           (u_int8_t)44
#define COMMAND_READ_MEMORY_WORD                (u_int8_t)45
#define COMMAND_WRITE_MEMORY_WORD               (u_int8_t)46
#define COMMAND_WRITE_MEMORY_HALF               (u_int8_t)47
#define COMMAND_WRITE_MEMORY_BYTE               (u_int8_t)48


// Subfunctions for command CLOCK
#define PAUSE_CLOCK_FUNC                        (u_int8_t)0x01
#define RESUME_CLOCK_FUNC                       (u_int8_t)0x02
#define RUNFOR_CLOCK_FUNC                       (u_int8_t)0x80

// Subcommands for DEBUG_AND_STATS
#define COMMAND_STATS_DISABLE                   (u_int8_t)0x01
#define COMMAND_STATS_ENABLE                    (u_int8_t)0x02         
#define COMMAND_ECHO_DISABLE                    (u_int8_t)0x03
#define COMMAND_ECHO_ENABLE                     (u_int8_t)0x04
#define COMMAND_MC_DEBUG_DISABLE                (u_int8_t)0x09
#define COMMAND_MC_DEBUG_ENABLE                 (u_int8_t)0x0a
#define COMMAND_U2M_DEBUG_ENABLE                (u_int8_t)32
#define COMMAND_U2M_DEBUG_DISABLE               (u_int8_t)31
#define COMMAND_M2U_DEBUG_ENABLE                (u_int8_t)34
#define COMMAND_M2U_DEBUG_DISABLE               (u_int8_t)33
#define COMMAND_INJECT_DEBUG_ENABLE             (u_int8_t)14
#define COMMAND_INJECT_DEBUG_DISABLE            (u_int8_t)13
#define COMMAND_EJECT_DEBUG_ENABLE              (u_int8_t)18
#define COMMAND_EJECT_DEBUG_DISABLE             (u_int8_t)17

