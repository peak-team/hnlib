///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node background communication process
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include "../hn.h"

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/
int main(int argc, char **argv)
{
  int i, x, y;
  uint32 rv;

  // We prepare the filter to enable access to registers and statistics
  hn_daemon_socket_filter filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode = HN_FILTER_APPL_MODE_TILEREG_AND_STATS;
  filter.tile = 999;
  filter.core = 999;

  // We initialize the hn library with the filter and the UPV's partition strategy
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }

  // We reset the system
  printf("reseting the system (waiting 5 secs...)\n");
  hn_reset(0);


  // Now we collect data from the system and print it
  uint32 num_tiles, num_tiles_x, num_tiles_y, num_vns;
  hn_st_tile_info tile_info[HN_MAX_TILES];

  hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
  hn_get_num_vns(&num_vns);
  for (i=0;i<num_tiles;i++) {
    hn_get_tile_info(i, &tile_info[i]);
  }

  printf("Number of tiles: %d (%d x %d)\n", num_tiles, num_tiles_x, num_tiles_y);
  printf("Number of Virtual Networks: %d\n", num_vns);
  printf("\nTiles configuration:\n");
  uint32 tile = 0;
  for (x=0;x<num_tiles_x;x++) {
    for (y=0;y<num_tiles_y;y++) {
      printf("T%03d: %s  ", tile, tile_info[tile].unit_model==HN_PEAK_TYPE_0?"PEAK0":
                                     tile_info[tile].unit_model==HN_PEAK_TYPE_1?"PEAK1":
                                     tile_info[tile].unit_model==HN_PEAK_TYPE_2?"PEAK2":
                                     tile_info[tile].unit_model==HN_NUPLUS_TYPE_0?"NUPL0":
                                     tile_info[tile].unit_model==HN_NUPLUS_TYPE_1?"NUPL1":
                                                                             "UNKWN");
      tile++;
    }
    printf("\n");
  }

  printf("\nMemory:\n");
  for (i=0;i<num_tiles;i++) {
    if (tile_info[i].memory_attached) printf("Memory at tile %d (%d KB)\n", i, tile_info[i].memory_attached);
  }

  //float temp;
  //printf("\nTemperature:\n");
  //for (i=0;i<num_tiles;i++) { 
  //  hn_get_tile_temperature(i, &temp);
  //  printf("T%03d %6.2fºC\n", i, temp);
  //}

  //hn_st_tile_stats stats;
  //printf("\nUnits utilization:\n");
  //for (i=0;i<num_tiles;i++) {
  //  hn_get_tile_stats(i, &stats);
  //  printf("Tile %03d: U: %03d\n", i, stats.unit_utilization);
  //} 

  //int vn;
  //printf("\nRouter's port stats (VN%01d) (N-E-W-S-L):\n", vn);
  //for (i=0;i<num_tiles;i++) {
  //  hn_get_tile_stats(i, &stats);
  //  printf("Tile %03d:", i);
  //  for (vn=0;vn < num_vns;vn++) {
  //    printf(" |VN%01d: %4d-%4d-%4d-%4d-%4d", vn, stats.flits_ejected_north[vn], stats.flits_ejected_east[vn], stats.flits_ejected_west[vn], stats.flits_ejected_south[vn], stats.flits_ejected_local[vn]); 
  //  }
  //  printf("\n");
  //}

  // We boot now the system
  printf("\nbooting the system...\n");
  for (i=0;i<num_tiles;i++) {
    printf("booting tile %d\n", i);
    hn_boot_unit(i, 0, i << 28);
  }

  // We load a kernel image in memory in tile 0
  //hn_write_image_into_memory("../peakos.img", 0, 0x00000000);
  //hn_peak_write_protocol("../peak_protocol.img", 0);

  //hn_set_tlb(0, 0, 0x00000000, 0x0FFFFFFF, 0x00000000, 1, 0, 0, 0);
  //printf("ahora\n");
  //hn_peak_set_new_pc(0, 0, 0x00032000);
  //hn_peak_unfreeze(0, 0);
  
  // We set the tlb entry of the unit where the kernel will be launched
  //hn_set_tlb(0, 1, 0x10000000, 0x1fffffff, 0x20000000, 1, 0, 0, 0);

  // Now we launch the kernel in tile0
  //hn_run_kernel(0);

  // We end
  hn_end();
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

