///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// This application implements a terminal with PEAK units running in a MANGO tile.
// The application receives the following parameters:
//   - <tile>: Tile where the PEAK unit is located
//   - <core>: Core of PEAK to get the terminal
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include "../hn.h"
#include<stdio.h>
#include<unistd.h>
#include <pthread.h>
#include <time.h>


#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#ifndef _WIN32
#include <unistd.h>
#endif

#include "profpga.h"
#include "profpga_error.h"

#include "mmi64.h"
#include "mmi64_defines.h"
#include "mmi64_module_regif.h"
#include "mmi64_module_axi_master.h"
#include "mmi64_module_upstreamif.h"

#include "profpga_logging.h"
#include "libconfig.h"

// here we use the mmi64 cross-platform definitions to implement
// a OS (semi-)independent threading
#include "mmi64_crossplatform.h"


#ifdef HDL_SIM
  #include "hn_daemon.h"
#endif

//#define _BLOCK_ACCESS_
//#define _SINGLE_ACCESS_
//
#define CHECK(status)  if (status!=E_PROFPGA_OK) { \
  printf(NOW("ERROR: %s\n"), profpga_strerror(status)); \
  return status;  }

//------------------------------------------------------------------------------
pthread_t  th_writer;

uint32_t tile;
uint32_t core;
//-------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
void *thread_writer(void *argv) {

  char ch;
  struct timespec ts;

  ts.tv_sec = 0;
  ts.tv_nsec = 500000;

  while(1) {
    ch = getchar();
    
    // keystrokes always go to core 0
    hn_peak_send_char(tile, 0, ch);
    nanosleep(&ts, NULL);
  }
}

//-------------------------------------------------------------------------------

  pthread_t  th_boot_daemon;

  void *thread_boot_daemon(void *argv) {
  int status_boot_daemon;
  
  printf("boot daemon\n");
  fflush(stdout);

  status_boot_daemon = hdl_sim_boot_daemon("mmi64");
  
  printf("boot daemon error...should never display this message\n");
  fflush(stdout);
  
  return 0;
}

//-------------------------------------------------------------------------------
void init_buf(char *buf, int size)
{
  int i;
  for (i=0;i<size;i++) buf[i] = ((random() % 256))& 0x000000FF;
  //for (i=0;i<size;i++) buf[i] = (i+1)& 0x000000FF;
}
//-------------------------------------------------------------------------------
#ifdef HDL_SIM 
mmi64_error_t mmi64_main(int argc, char* argv[])
{
  uint32_t     init_lib;


  char    *buf;
  char    *buf2;
  int      buf_size;
  int      buf_size_rounded_up;
  unsigned int base_address;
  uint32_t rv;
  int      num_blocks;
  int      padding;
  int      err;
  int      i;
  int      block;
  uint32_t num_tiles;
  uint32_t x_dim;
  uint32_t y_dim;
  uint32_t req_tile_memory_size; // size of the memory this test will run on.
  int      num_blocks_rounded_up;
 
  printf("\n\n");
  printf("-------------------------\n");
  printf("-------------------------\n");
  
  printf("Create thread to boot daemon\n");
  fflush(stdout);
  
  rv = pthread_create(&th_boot_daemon, NULL, thread_boot_daemon, (void *)NULL);
  if (rv) {
    printf("Error, thread to boot daemon could not be run\n");
    printf("exit now\n");
    fflush(stdout);
    exit(1);
  }
/*
  int status_boot_daemon; 
  printf("boot daemon\n");
  fflush(stdout);
  status_boot_daemon = hdl_sim_boot_daemon("mmi64");
  printf("\n");
  printf("daemon booted, check status\n");
  fflush(stdout);  if (status_boot_daemon != 0) {
    printf("Error booting daemon....\n");
    printf("quit now\n");
    exit(1);
  }
*/
  
  tile         = 0;
  core         = 0;
  buf_size     = 256;
  base_address = 0x00000000;
  
  printf("Launching hn test application to test memory in rd/wr access\n");
  fflush(stdout);
  
  // We prepare the filter to get console access
  hn_daemon_socket_filter filter;
  filter.target = HN_FILTER_TARGET_MANGO;
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = tile;
  filter.core   = core;
  

  printf("vaig a iniziar la llib, espere q inicie el daemon...test and fail, sleeping 200\n");
  fflush(stdout);
  sleep(200);
  printf("\n\n");
  printf("espere que el daemon ja estiga en marxa....... inicie hn_lib\n");
  fflush(stdout);
  init_lib = hn_initialize(filter, NO_PARTITION_STRATEGY, 1, 1, 1);

  //test purposes...reset after initialization!!!
  //init_lib = hn_initialize(filter, NO_PARTITION_STRATEGY, 1, 1, 1);

  // We init the hn library
  if (init_lib != HN_SUCCEEDED) {
    printf("Error  %u, not able to initialize the HN library\n", init_lib);
    fflush(stdout);
    sleep(5);
    return(1);
  }

  
  
  
  
  
  rv = hn_get_num_tiles(&num_tiles, &x_dim, &y_dim);
  if (rv != HN_SUCCEEDED){
    printf ("ERROR: getting tiles information\n");
    printf ("       abort test\n");
    fflush(stdout);
    hn_print_error(rv);
    hn_end();
    exit(1);
  }
  if (tile >= num_tiles){
    printf ("ERROR: requested tile %d out of bounds [0:%d]\n", tile, num_tiles-1);
    printf ("       abort test\n");
    fflush(stdout);
    hn_end();
    exit(1);
  }

  rv = hn_get_memory_size((uint32_t)tile, &req_tile_memory_size);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    hn_end();
    exit(1);
  }
  if(req_tile_memory_size <= 0) {
    printf ("ERROR: requested tile has no memory attached\n");
    printf ("       abort test\n");
    hn_end();
    exit(1);
  }


  //check address is block aligned for the test.
  //  HN subsystem and hn-lib support non aligned acces (byte and word) 
  //  but the test only supports block access
  if((base_address % 64) != 0) {
    printf("Warning: requested base address for test is not aligned\n");
    printf("         Non aligned access is not allowed in this test\n");
    printf("         Setting new base address for test\n");
    printf("         requested base address: 0x%08X\n", base_address);
    base_address = base_address - (base_address%64);
    printf("               new base address: 0x%08X\n", base_address);
    printf("\n");
  }

  num_blocks = buf_size / 64;
  padding    = buf_size % 64;
  num_blocks_rounded_up = num_blocks;
  if(padding != 0) {
    num_blocks_rounded_up =  num_blocks_rounded_up + 1;
  }
  buf_size_rounded_up = num_blocks_rounded_up * 64;

  printf("Test for memory located in tile %d: size: %d, addr: %08X, num_blocks: %d, padding : %d\n", tile, buf_size, base_address, num_blocks, padding);

  buf  = malloc(buf_size_rounded_up);
  buf2 = malloc(buf_size_rounded_up);
  init_buf(buf, buf_size_rounded_up);

  printf("Write buffer to memory\n");
  rv = hn_write_memory(tile, base_address, buf_size, buf);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    free(buf);
    free(buf2);
    hn_end();
    exit(1);
  }
  printf("   ...done\n");

  printf("Read memory contents\n");
  rv = hn_read_memory(tile, base_address, buf_size, buf2);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    free(buf);
    free(buf2);
    hn_end();
    exit(1);
  }
  printf("   ...done\n");


  // check
  
  printf("Checking data\n");
  if (padding != 0) num_blocks++;
  
  err = 0;
  for (block = 0; block < num_blocks && !err; block++) {
    if ((block == (num_blocks-1)) && (padding != 0)) {
      for (i = 0; i < padding && !err; i++) {
        //if (buf[block*64+(padding-1)-i] != buf2[block*64+i]) {
        if (buf[block*64+i] != buf2[block*64+i]) {
          err = 1;
          printf("Error block %4d  pos %2d    %02X != %02X \n", block, i, buf[block*64+i]& 0x00FF,buf2[block*64+i]& 0x00FF );
          fflush(stdout);
        }
      }
    } else {
      for (i = 0; i < 64 && !err; i++) {
        //if (buf[block*64+63-i] != buf2[block*64+i]) {
        if (buf[block*64+i] != buf2[block*64+i]) {
          err = 1;
          printf("Error block %4d  pos %2d    %02X != %02X \n", block, i, buf[block*64+i]& 0x00FF, buf2[block*64+i]& 0x00FF);
          fflush(stdout);
        }
      }
    }
  }


  if (err) {
    printf("The content (%d B) written and read in tile %d is different\n", buf_size, tile);
  } else {
    printf("The content (%d B) written and read in tile %d is equal\n", buf_size, tile);
  }

  free(buf);
  free(buf2);

  
  //printf("sleeping 5secs\n");
  //fflush(stdout);
  //sleep(50);

  printf("\n");
  printf("all is done :)\n");
  printf("you can now stop the app\n");
  fflush(stdout);

  int loops = 10;
  do {
    //nothing
    printf("ZZzzz\n");
    fflush(stdout);
    sleep(2);
    loops--;
  }while (loops > 0);
  
  printf("leaving hn test app\n");

  // We end
  hn_end();

  return 0;
}
#endif
