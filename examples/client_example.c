///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node background communication process
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdio.h>
#include <stdlib.h>

#include "hn.h"

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/
int main(int argc, char **argv)
{
  int i, x, y;
  uint32_t rv;

  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile = 999;
  filter.core = 999;

  // We initialize the hn library with the filter and the UPV's partition strategy
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 0);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }

  // We reset the system
  printf("reseting the system (waiting 5 secs...)\n");
  hn_reset(0);


  // Now we collect data from the system and print it
  hn_tile_info_t tile_info[256];
  uint32_t num_tiles, num_tiles_x, num_tiles_y, num_vns;
  hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
  hn_get_num_vns(&num_vns);
  for (i=0;i<num_tiles;i++) {
    hn_get_tile_info(i, &tile_info[i]);
  }

  printf("Number of tiles: %d (%d x %d)\n", num_tiles, num_tiles_x, num_tiles_y);
  printf("Number of Virtual Networks: %d\n", num_vns);
  printf("\nTiles configuration:\n");
  uint32_t tile = 0;
  for (x = 0; x < num_tiles_x; x++)
  {
    for (y = 0; y < num_tiles_y; y++)
    {
      switch(tile_info[tile].unit_family) {
        case HN_TILE_FAMILY_PEAK:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "PEAK0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "PEAK1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "PEAK2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "PEAK3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "PEAK4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "PEAK5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "PEAK6" :
                                                                                     "UNKWN");
          break;

        case HN_TILE_FAMILY_NUPLUS:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "NUPLUS0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "NUPLUS1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "NUPLUS2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "NUPLUS3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "NUPLUS4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "NUPLUS5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "NUPLUS6" :
                                                                                     "UNKWN");
          break;

        case HN_TILE_FAMILY_DCT:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "DCT0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "DCT1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "DCT2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "DCT3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "DCT4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "DCT5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "DCT6" :
                                                                                     "UNKWN");
          break;

        case HN_TILE_FAMILY_TETRAPOD:
          printf("T%03d: %-10s   ", tile, 
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_0   ? "TETRAPOD0" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_1   ? "TETRAPOD1" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_2   ? "TETRAPOD2" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_3   ? "TETRAPOD3" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_4   ? "TETRAPOD4" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_5   ? "TETRAPOD5" :
                                tile_info[tile].unit_model == HN_TILE_UNIT_CONF_6   ? "TETRAPOD6" :
                                                                                     "UNKWN");
          break;
          
        default: 
          printf("T%03d: %-10s   ", tile, "UNKWN");
          break;
      }
      tile++;
    }
    printf("\n");
  }

  printf("\nMemory:\n");
  for (i=0;i<num_tiles;i++) {
    if (tile_info[i].memory_attached) printf("Memory at tile %d (%d KB)\n", i, tile_info[i].memory_attached);
  }

  hn_end();
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

