///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: may 30, 2018
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Initialize hn library
//    Get statistics for tiles in arch
//
//    The hn_lib mananges ctrl+c signal to softly stop the hn_lib and release the resources,
//      for any other operations that the application should do, please install the appropiate signal handler
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>

#include "hn.h"


#define DEBUG
#define ALL_TILES ((int)999)


volatile sig_atomic_t run_test = 1;
volatile sig_atomic_t cancel_test = 0;
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * \brief Signal handler
 *
 * Detect ctl+c signal and toggle run_test signal to terminate thread in a soft manner
 */

void signal_handler_function(int signal)
{
  if (signal == SIGINT) {
    run_test = 0;

    printf("\n");
    printf(" cancelling test\n");
    cancel_test = 1;
    fflush(stdout);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * \brief main test application
 *
 * 
 */

int main(int argc, char **argv)
{
  int         cmd_opt;              // temporal variable to store commands received as parameter
  uint32_t    reset_system;         // indicates whether we will send reset signal to the system before running the test
  uint32_t    display_stats_period; // time the system will sleep between two consecutive reads of the temperature sensor, like a "siesta". Time in SECONDS
  uint32_t    polling_period;       // stats polling interval in monitoring thread. time in miliseconds
  uint32_t    update_polling_period;
  uint32_t    silent_mode_enable;   // indicates wheter the test will display the temperatures or not (set to 0). This mode is intended to minimize output when running the automated testbench 
  int         number_of_iterations; 
  int         endless_run;
  int         stats_of_tile;

  uint32_t    error_detected;
  int         val;
  time_t      rawtime;
  struct tm  *timeinfo;
  uint32_t    num_tiles_in_arch;
  uint32_t    num_tiles_dim_x;
  uint32_t    num_tiles_dim_y;
  uint32_t    tmp_val;
  uint32_t    arch_id;                   // architecture id returned by hn_lib, architecture running on HN_system
  uint64_t    number_of_readings_errors; // number of times that the hn_lib reported an error on a read temperature function call 
  uint64_t    number_of_readings;        // total of times the function has been called, number of temperature readings
  uint32_t    tile_index;
  uint32_t    num_vns;

  uint32_t    low_tile_index;
  uint32_t    high_tile_index;


  if (signal(SIGINT, signal_handler_function) == SIG_ERR)
  {
    printf("Error setting signal handler to detect end of test\n");
    printf("  %s\n", strerror(errno));
    printf("Abort test execution\n");
    fflush(stdout);
    return (-1);
  }

  // set default values for application parameters
  reset_system         = 0;
  display_stats_period = 2;   //  s
  polling_period       = 500; // ms
  update_polling_period= 0;
  error_detected       = 0;
  silent_mode_enable   = 0;
  number_of_iterations = 1000;
  endless_run          = 0;
  stats_of_tile        = ALL_TILES;  // -1 stands for all tiles


  // Display Welcome Message
  printf("\n\n");
  printf("-------------------------------------------------------------------------------\n");
  printf("-------------------------------------------------------------------------------\n");
  printf("  \n");
  printf("  DEMO Application\n");
  printf("    Initialize hn library\n");
  printf("    Read statistics from tiles\n");
  printf("  \n");
  printf("  run example test with -h option for help\n");
  printf("  press ctrl+c to stop the test\n");
  printf("    this will trigger the routine to stop the hn_lib in a soft and controlled manner\n");
  printf("  \n");
  printf("--------------------------------------------------------------------------------\n");


  // process args searching application configuration parameters
  printf("\n");
  printf("---------------------------------\n");
  while ((cmd_opt = getopt (argc, argv, "d:ei:rst:p:h")) != -1)
  {
    switch (cmd_opt) 
    {
      case 'd':
        val = atoi(optarg);
        if(val >= 0) 
          display_stats_period = val;
        else
          printf("Wrong value, %s out of range, set to default value", optarg);

        printf("option -> display stats interval to: %u s\n", display_stats_period);
        break;
      case 'e':
        endless_run = 1;
        printf("option -> endless run set, the test will run in an infinite loop\n");
        break;
      case 'i':
        number_of_iterations = atoi(optarg);
        if  (number_of_iterations < 1) number_of_iterations = 1;
        printf("option -> iterations, the test will request stats %d times\n", number_of_iterations);
        break;
      case 'r':
        printf("option -> reset, will reset system before running test\n");
        reset_system = 1;
        break;
      case 's':
        printf("option -> silent mode enabled, will not display statistics, The test will only read stats and look for erroneous readings\n");
        silent_mode_enable = 1;
        break;
      case 't':
        val = atoi(optarg);
        if((val == ALL_TILES) || (val >= 0) )
          stats_of_tile = val;
        else
          printf("Wrong value, %s out of range, set to default value", optarg);

        printf("option -> set tile for stats to: %u %s s\n", stats_of_tile, (stats_of_tile == ALL_TILES)?"ALL tiles":"");
        break;
      case 'p':
        val = atoi(optarg);
        if(val >= 0)
        {
          polling_period = val;
          update_polling_period = 1;
          printf("option -> set polling period to: %u ms\n", polling_period);
        }
        else
        {
          printf("DISCARD  %c %s\n", cmd_opt, optarg);
          printf("  Wrong value, %s out of range", optarg);
        }
 
        break;
      case 'h':
        printf("HELP\n");
        printf("supported options\n");
        printf("  -d <display_stats_period> -r -s -t <tile> -p <polling_period>\n");
        printf("    -d <display_stats_period> set time between stats update on screen. Time in seconds\n");
        printf("    -e enless loop until ctrl+c is pressed\n");
        printf("    -i iterations, number of times the test will request stats from tiles\n");
        printf("    -r reset before run test\n");
        printf("    -s silent, will not display data on terminal\n");
        printf("    -t <tile> display stats for specifc tile\n");
        printf("    -p <polling_period> set polling period value in miliseconds\n");
        printf("\n");
        return -1;
      case '?':
        if ((optopt == 'd') || (optopt == 'i') ||(optopt == 't') || (optopt == 'p'))
          printf ("Option -%c requires an argument\n", optopt);
        else if (isprint (optopt))
          printf ("Unknown option `-%c'\n", optopt);
        else
          printf ("Unknown option character `\\x%x'\n", optopt);
        return 1;
      default:
        return -1;
    }
  }

  // Prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;


  // Initialize the hn library with the filter and the UPV's partition strategy
  printf("Initialize hn library....\n");
  tmp_val = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (tmp_val != HN_SUCCEEDED)
  {
    printf("FATAL initializing hn_lib\n");
    hn_print_error(tmp_val);
    exit(1);
  }
  printf("   ...done\n");

  if(reset_system) {
    printf("reset system before running the test\n");
    fflush(stdout);
    hn_reset(0);
    printf("   ...done\n");
  }

  tmp_val = hn_get_arch_id(&arch_id);
  if (tmp_val != 0) {
    printf("ERROR getting architecture ID\n");
    printf("  ABORT test\n");
    error_detected = 1;
    hn_end();
    exit(1);
  }

  tmp_val = hn_get_num_vns(&num_vns);
  if (tmp_val != 0) {
    printf("ERROR getting number of VNs in system\n");
    printf("  ABORT test\n");
    error_detected = 1;
    hn_end();
    exit(1);
  }

  if(run_test)
  {
    // let's get the number of tiles in this architecture
    tmp_val =  hn_get_num_tiles(&num_tiles_in_arch, &num_tiles_dim_x, &num_tiles_dim_y);

    if (tmp_val != 0)
    {
      printf("ERROR getting number of tiles in architecture\n");
      printf("  ABORT test\n");
      error_detected = 1;
      hn_end();
      exit(1);
    }
    else 
    {
      if ( stats_of_tile == ALL_TILES)
      {
        low_tile_index = 0;
        high_tile_index = num_tiles_in_arch;
      }
      else
      {
        if (stats_of_tile < num_tiles_in_arch)
        {
          low_tile_index  = stats_of_tile;
          high_tile_index = low_tile_index + 1;
        }
        else
        {
          printf("ERROR request stats of tile %d, but higher tile_id available in architecture is %u\n", stats_of_tile, num_tiles_in_arch);
          printf("  ABORT test\n");
          error_detected = 1;
          hn_end();
          exit(1);

        }
      }
    }
  }


  if((error_detected == 0) && (run_test))
  {
    // let's display system information
    printf("\n");
    printf("System Information\n");
    printf("  Architecture ID:   %u\n", arch_id);
    printf("  Number of Tiles:   %u\n", num_tiles_in_arch);
    printf("  Number of VNs  :   %u\n", num_vns);
    printf("\n");
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    printf("Test started on %s", asctime (timeinfo));
    if(stats_of_tile == ALL_TILES)
    {
      printf("     displaying stats of all tiles\n");
    }
    else
    {
      printf("     displaying stats of tile %u\n", stats_of_tile);
    }
    
    if(endless_run)
    {
      printf("ENDLESS run mode enabled, test will run in an infinite loop\n");
    }
    else
    {
      printf("     reading stats for %d times\n", number_of_iterations);
    }

    if(silent_mode_enable)
    {
      printf("SILENT_MODE enabled, the test will not display statistics\n");
    }
    printf(" Test will get statistics for all tiles and sleep for %u seconds\n", display_stats_period);
    printf("\n");
    fflush(stdout);

    printf("\n");
    number_of_readings_errors = 0;
    number_of_readings        = 0;

    if(update_polling_period)
    {
      printf("Update polling period to: %u ms\n", polling_period);
      if(hn_stats_monitor_set_polling_period(polling_period))
      {
        printf("Error updating polling period, will use HN_DAEMON default value\n");
      }
    }

    printf("Test will enable stats for %d tiles\n\n", (high_tile_index - low_tile_index));
    fflush(stdout);
    // set tiles to monitor stats
    for (tile_index = low_tile_index; (tile_index < high_tile_index) && (run_test); tile_index++)
    {
      printf("Adding tile %u\n", tile_index);
      fflush(stdout);
      if(hn_stats_monitor_configure_tile(tile_index, 1))
      {
        printf("Error adding tile %u to monitor stats\n", tile_index);
      }
      printf("  done\n");
      fflush(stdout);
    }

    if( stats_of_tile == ALL_TILES)
    {
      // try to add an extra tile, should display an error msg
      printf("\n");
      printf("Adding non-existing tiles (out of range) to trigger tile_id control\n");
      fflush(stdout);
      for(tile_index = num_tiles_in_arch; tile_index < (num_tiles_in_arch + 10); tile_index++)
      {
        uint32_t rv_add;
        rv_add =  hn_stats_monitor_configure_tile(tile_index, 1);
        if(rv_add == 0)
        {
          printf("Error checking tile index range, adding tile %u to monitor stats did not return error\n", tile_index);
          error_detected = 1;
        }
        else
        {
          printf("  hn_lib returned error code %u, tile id range control worked as expected when attempting to add tile id %u\n", rv_add, tile_index);
        }
      }
    }


    printf("\n");
    printf("Stats monitor configured and autmatically started\n");
    sleep(1);

    printf("\n");
    printf("Request stats\n");
    printf("\n");
    fflush(stdout);

    // stat the test loop until cancellation requested
    do
    {
      uint32_t  total_cores = 0;

      for (tile_index = low_tile_index; (tile_index < high_tile_index) && (run_test); tile_index++)
      {
        hn_stats_monitor_t *stats;
        uint32_t            rv;
        uint32_t            num_cores;
        uint32_t            core_index;
        uint32_t            vn;

        rv = hn_stats_monitor_read(tile_index, &num_cores, &stats);
        if(rv != 0)
        {
          printf("Error gettin stats, returned: %u\n", rv);
          number_of_readings_errors++;
        }
        else if (stats == NULL)
        {
          printf("ERROR!!!! stats points to NULL");
        }
        else
        {
          if(!silent_mode_enable)
          {
            printf("STATS for Tile %2u,  %2u cores and %2u VNs\n", tile_index, num_cores, num_vns);
            printf("---------------------------------------------\n");


            for(core_index = 0; core_index < num_cores; core_index++)
            {
              printf("core %u\n", core_index);
              printf("          timestamp       : %'20lu\n",  stats[core_index].timestamp);
              //printf("          tics_kernel     : %'20lu\n", stats[core_index].tics_kernel);
              //printf("          tics_user       : %'20lu\n", stats[core_index].tics_user);
              printf("          cycles_sleep    : %'20lu\n",  stats[core_index].tics_sleep);
              //printf("          num_threads     : %'20u\n", stats[core_index].num_threads);
              printf("          l1i_hits        : %'20u\n",   stats[core_index].l1i_hits);
              printf("          l1i_misses      : %'20u\n",   stats[core_index].l1i_misses);
              printf("          l1d_hits        : %'20u\n",   stats[core_index].l1d_hits);
              printf("          l1d_misses      : %'20u\n",   stats[core_index].l1d_misses);
              printf("          l2_hits         : %'20u\n",   stats[core_index].l2_hits);
              printf("          l2_misses       : %'20u\n",   stats[core_index].l2_misses);
              printf("          btb_predict     : %'20u\n",   stats[core_index].btb_predict);
              printf("          btb_misspredict : %'20u\n",   stats[core_index].btb_misspredict);
              printf("          core_cycles     : %'20u\n",   stats[core_index].core_cycles);
              printf("          core_instr      : %'20u\n",   stats[core_index].core_instr);
              printf("          core_cpi        : %'20.3f\n", stats[core_index].core_cpi);
              printf("          mc_accesses     : %'20u\n",   stats[core_index].mc_accesses);
              printf("\n");
              printf("          Network stats\n");
              printf ("         %4s   %10s   %10s   %10s   %10s   %10s\n", "VN", "LOCAL", "SOUTH", "WEST", "EAST", "NORTH");
              for(vn = 0; vn < num_vns; vn++)
              {
                printf ("         %4u   %10u   %10u   %10u   %10u   %10u\n",
                //printf ("         %4u     %08x     %08x     %08x     %08x     %08x\n",
                    vn,
                    stats[core_index].nstats[vn].flits_ejected_local,
                    stats[core_index].nstats[vn].flits_ejected_south,
                    stats[core_index].nstats[vn].flits_ejected_west,
                    stats[core_index].nstats[vn].flits_ejected_east,
                    stats[core_index].nstats[vn].flits_ejected_north
                    );
              }
              total_cores++;
            }
          }
          number_of_readings++;

          if (stats != NULL){
            free(stats);
          }

          if(!silent_mode_enable)
          {
            printf("\n");
            fflush(stdout);
          }
        }
      }

      sleep(display_stats_period);

      if(!endless_run) {
        number_of_iterations--;
      }

      printf("\033[A\33[2K\r"); // go up one line and erase it 
      fflush(stdout);
      printf("remaining iterations: %u\n", number_of_iterations);

      if (!endless_run && number_of_iterations <=0) 
      {
        run_test = 0;
      }
      else if((!silent_mode_enable) && (run_test))
      {
        // WARNING: this comman will only work fine when output is redirected to a file. otherwise in can not go backwards properly
        // move terminal write pointer backwards to overprint last stats
        uint32_t lines_to_rewind;
        uint32_t ind;
        lines_to_rewind = 2*(high_tile_index - low_tile_index) + total_cores * ( 18 + num_vns) + 1*(high_tile_index - low_tile_index) ;
        // let's put the cursor at the begining of the 
        for(ind = 0; ind < lines_to_rewind; ind++)
        {
          printf("\033[A\33[2K\r"); // erases current line 
          fflush(stdout);
        }

      }
    }  while (run_test);

    printf("\n");
    printf("Finished\n");
  }

  // let's make sure all stats are off
  printf("\n");
  printf("End of test: disable stats for all tiles\n");


  // if the test has been cancelled, then the hn_lib has automatically closed the socket with the daemon, we need to open it again so we can stop the monitorization of stats
  if (cancel_test) {
    // we need to connect again to the hn....
    printf("Initialize hn library....\n");
    tmp_val = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
    if (tmp_val != HN_SUCCEEDED)
    {
      printf("FATAL reconnecting to hn_lib\n");
      hn_print_error(tmp_val);
      exit(1);
    }
    printf("   ...done\n");

  }

  for (tile_index = low_tile_index; (tile_index < high_tile_index); tile_index++)
  {
    printf("Removing tile %u\n", tile_index);
    hn_stats_monitor_configure_tile(tile_index, 0);
    printf("  done\n");
  }
  
  if (stats_of_tile == ALL_TILES)
  {
    printf("\n");
    printf("Removing non-existing tiles (out of range) to trigger tile_id control\n");
    for(tile_index = num_tiles_in_arch; tile_index < (num_tiles_in_arch + 10); tile_index++)
    {
      uint32_t rv_add;
      rv_add =  hn_stats_monitor_configure_tile(tile_index, 0);
      if(rv_add == 0)
      {
        printf("Error checking tile index range, removing tile %u from monitor stats did not return error\n", tile_index);
        error_detected = 1;
      }
      else
      {
        printf("  hn_lib returned error code %u, tile id range control worked as expected when attempting to remove tile id %u\n", rv_add, tile_index);
      }
    }
  }


  if (error_detected && (run_test != 0))
  {
    // we are finishing because of an error, we need to call hn_end because we did not call the hn_lib signal handler
    printf("Error detected whille running the test calling hn_end function\n");
    fflush(stdout);
    hn_end();

  }
  
  // close hn
  printf("Release communication sockets and resources\n");
  hn_end();
  
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  printf("Test finished on %s", asctime (timeinfo));
  printf("total readings: %lu\n", number_of_readings);
  printf("error readings: %lu\n", number_of_readings_errors);


  if ((number_of_readings_errors != 0) ||  (error_detected))
  {
    printf("TEST KO \n");
    printf("   %lu read operations failed\n", number_of_readings_errors);
  }
  else
  {
    printf("TEST OK \n");
  }

  printf("bye bye\n\n");

  return 0;
}
//*********************************************************************************************************************
// end of file test_acquire_temp.c
//*********************************************************************************************************************


