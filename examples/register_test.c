///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//   performs a register access test
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "hn.h"


int main(int argc, char **argv)
{
  int     tile;
  int     reg_number;
  int     expected_value;
  uint32_t  read_value;
  uint32_t  rv;
  int    cont_loop;
  int     i;
  uint64_t cont;

  if (argc != 4) {
    printf("USAGE: %s <tile> <reg_num> <integer_value>\n", argv[0]);
    exit(0);
  }

  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile = 999;
  filter.core = 999;

  tile            = atoi(argv[1]);
  reg_number      = atoi(argv[2]);
  expected_value  = atoi(argv[3]);

  // We initialize the hn library with the filter and the UPV's partition strategy
  printf("initializing hn_lib...\n");
  fflush(stdout);
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("...done\n");
  fflush(stdout);

  // Now we collect data from the system and print it
  uint32_t num_tiles, num_tiles_x, num_tiles_y;
  hn_tile_info_t tile_info[HN_MAX_TILES];

  hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
  for (i=0; i < num_tiles; i++) {
    hn_get_tile_info(i, &tile_info[i]);
  }

  printf("Number of tiles: %d (%d x %d)\n", num_tiles, num_tiles_x, num_tiles_y);
  printf("Test will run over tile  : %d\n", tile);
  printf("                   reg   : %d\n", reg_number);
  printf("                   value : %d\n", expected_value);

  // set register with value and then read.
  // both register number, and value to set are passed as parameters

  printf("writing value on register....\n");
  hn_write_register(tile, reg_number, (uint32_t)expected_value);
  //read reg content and compare, notify when read value differs from expected value
  cont_loop = 1;
  cont = 0;
  printf("done, entering infinite loop\n");
  fflush(stdout);
  while(cont_loop == 1){
    //sleep(1);
    //printf("reading register\n");
    fflush(stdout);
    hn_read_register(tile, reg_number, &read_value);
    //printf("%d\n", i++);
    //fflush(stdout);
    if((cont%1000) == 0)
    {
      printf("%lu\n",cont);
    }
    cont++;
    
    if((int)read_value != (int)expected_value)
    {
      printf("Error reading value, expected %d --> read %d\n", (int)expected_value, (int)read_value);
      fflush(stdout);
      cont_loop = 0;
    }
  }

  // We end
  hn_end();

  return 0;
}
