///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: March 15, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Memory Readings after writings through the HN communication proccess
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hn.h"

void init_buf(char *buf, int size)
{
  int i;
  //printf("Filling buffer to send with random data (size: %d)\n", size);
  //for (i=0;i<size;i++) buf[i] = ((random() % 256))& 0x000000FF;
  printf("Filling buffer to send with known data (size: %d)\n", size);
  for (i=0;i<size;i++) buf[i] = (i+1)& 0x000000FF;
}

int main(int argc, char **argv)
{
  int      tile;
  char    *buf;
  char    *buf2;
  int      buf_size;
  int      buf_size_rounded_up;
  unsigned int base_address;
  uint32_t rv;
  int      num_blocks;
  int      padding;
  int      err;
  int      i;
  int      block;
  uint32_t num_tiles;
  uint32_t x_dim;
  uint32_t y_dim;
  uint32_t req_tile_memory_size; // size of the memory this test will run on.
  int      num_blocks_rounded_up;

  if (argc != 4) {
    printf("USAGE: %s <tile> <buf_size> <base_address in hex, e.g. 00032A0>\n", argv[0]);
    exit(0);
  }

  printf("Launching hn test application to test memory in rd/wr access\n");
  fflush(stdout);

  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;

  tile         = atoi(argv[1]);
  buf_size     = atoi(argv[2]);
  base_address = (unsigned int)strtoul(argv[3], NULL, 16);

  printf("vaig a iniziar la llib, espere q inicie el daemon...test and fail\n");
  fflush(stdout);
  printf("\n\n");
  printf("espere que el daemon ja estiga en marxa....... inicie hn_lib\n");
  fflush(stdout);

  // We initialize the hn library with the filter and the UPV's partition strategy
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    printf("Error intializing hn\n");
    fflush(stdout);
    hn_print_error(rv);
    exit(1);
  }

  //check memory availability in selected tile, call get_memory_size function
  // this function returns the size of the memory,
  // if returned value is 0, it means that no memory is available in requested tile
  rv = hn_get_num_tiles(&num_tiles, &x_dim, &y_dim);
  if (rv != HN_SUCCEEDED){
    printf ("ERROR: getting tiles information\n");
    printf ("       abort test\n");
    fflush(stdout);
    hn_print_error(rv);
    hn_end();
    exit(1);
  }
  if (tile >= num_tiles){
    printf ("ERROR: requested tile %d out of bounds [0:%d]\n", tile, num_tiles-1);
    printf ("       abort test\n");
    fflush(stdout);
    hn_end();
    exit(1);
  }

  rv = hn_get_memory_size((uint32_t)tile, &req_tile_memory_size);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    hn_end();
    exit(1);
  }
  if(req_tile_memory_size <= 0) {
    printf ("ERROR: requested tile has no memory attached\n");
    printf ("       abort test\n");
    hn_end();
    exit(1);
  }


  //check address is block aligned for the test.
  //  HN subsystem and hn-lib support non aligned acces (byte and word) 
  //  but the test only supports block access
  if((base_address % 64) != 0) {
    printf("Warning: requested base address for test is not aligned\n");
    printf("         Non aligned access is not allowed in this test\n");
    printf("         Setting new base address for test\n");
    printf("         requested base address: 0x%08X\n", base_address);
    base_address = base_address - (base_address%64);
    printf("               new base address: 0x%08X\n", base_address);
    printf("\n");
  }

  num_blocks            = buf_size / 64;
  padding               = buf_size % 64;
  num_blocks_rounded_up = num_blocks;
     

  if(padding != 0) {
    num_blocks_rounded_up =  num_blocks_rounded_up + 1;
  }
  buf_size_rounded_up = num_blocks_rounded_up * 64;
 
  printf("Test for memory located in tile %d: size: %d, addr: %08X, num_blocks: %d, padding : %d\n", tile, buf_size, base_address, num_blocks, padding);


  if(padding != -1)
  buf  = malloc(buf_size_rounded_up);
  buf2 = malloc(buf_size_rounded_up);
  init_buf(buf, buf_size_rounded_up);

  printf("Write buffer to memory\n");
  rv = hn_write_memory(tile, base_address, buf_size, buf);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    free(buf);
    free(buf2);
    hn_end();
    exit(1);
  }
  printf("   ...done\n");

  printf("Read memory contents\n");
  rv = hn_read_memory(tile, base_address, buf_size, buf2);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    free(buf);
    free(buf2);
    hn_end();
    exit(1);
  }
  printf("   ...done\n");


  // check
  
  printf("Checking data\n");
  if (padding != 0) num_blocks++;
  
  err = 0;
  for (block = 0; block < num_blocks && !err; block++) {
    printf("checking block %d\n", block);
    fflush(stdout);
    if ((block == (num_blocks-1)) && (padding != 0)) {
      for (i = 0; i < padding && !err; i++) {
        //if (buf[block*64+(padding-1)-i] != buf2[block*64+i]) {
        if (buf[block*64+i] != buf2[block*64+i]) {
          err = 1;
          printf("Error block %4d  pos %2d    %02X != %02X \n", block, i, buf[block*64+i]& 0x00FF,buf2[block*64+i]& 0x00FF );
          fflush(stdout);
        }
      }
    } else {
      for (i = 0; i < 64 && !err; i++) {
        //if (buf[block*64+63-i] != buf2[block*64+i]) {
        if (buf[block*64+i] != buf2[block*64+i]) {
          err = 1;
          printf("Error block %4d  pos %2d    %02X != %02X \n", block, i, buf[block*64+i]& 0x00FF, buf2[block*64+i]& 0x00FF);
          fflush(stdout);
        }
      }
    }
  }


  if (err) {
    printf("The content (%d B) written and read in tile %d is different\n", buf_size, tile);
  } else {
    printf("The content (%d B) written and read in tile %d is equal\n", buf_size, tile);
  }

  free(buf);
  free(buf2);

  // We end
  hn_end();

  return (0);
}
