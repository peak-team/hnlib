///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node background communication process
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hn.h"

//////////////
void init_buf(char *buf, int size)
{
  int i;
  for (i=0;i<size;i++) buf[i] = random() % 256;
}

//////////////
struct timespec get_elapsed_time(struct timespec start, struct timespec end)
{
  struct timespec temp;
  if ((end.tv_nsec - start.tv_nsec) < 0) 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  } 
  else 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }
  return temp;
}

//////////////
unsigned long int get_elapsed_time_nanoseconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;


  temp = get_elapsed_time(start, end);
  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;
  
  return elapsed_nanoseconds;
}

//////////////
int main(int argc, char **argv)
{
  int       tile;
  char     *buf;
  char     *buf2;
  int       buf_size;
  int       buf2_size;
  int       it;
  struct timespec  t1;
  struct timespec  t2;
  unsigned long int  elapsed_ns;  // time elapsed in nanoseconds 
  float     elapsed_in_sec;
  uint32_t  num_tiles;
  uint32_t  num_tiles_x;
  uint32_t  num_tiles_y;
  uint32_t  rv;
  hn_tile_info_t tile_info[HN_MAX_TILES];

  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile = 999;
  filter.core = 999;

  // We initialize the hn library with the filter and the UPV's partition strategy
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 1, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }

  // Now we collect data from the system and print it

  hn_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
  for (tile=0;tile<num_tiles;tile++) {
    hn_get_tile_info(tile, &tile_info[tile]);
  }

  printf("Number of tiles: %d (%d x %d)\n", num_tiles, num_tiles_x, num_tiles_y);

  // For each memory in the system we perform the test
  for ( tile = 0; tile < num_tiles; tile++ ) {
    if ( tile_info[tile].memory_attached ) {
      printf("Test for memory located in tile %d (with %u KB)\n", tile, tile_info[tile].memory_attached/1024);
      for ( it = 1; it < 4; it++ ) {
        buf_size  = (tile_info[tile].memory_attached /    1000) * it;
        buf2_size = (tile_info[tile].memory_attached / 1000000) * it;
        
        buf       = malloc(buf_size);
        buf2      = malloc(buf2_size);
        
        init_buf(buf, buf_size);


        printf("  Test %d: PC->MEM transfer of %6.2f kB\n", it, (float)buf_size/(float)1024.0);
        //printf("type something to continue\n");
        //getchar();
        //write access
        clock_gettime(CLOCK_MONOTONIC, &t1);
        hn_write_memory(tile, 0x00000000, buf_size, buf);
        clock_gettime(CLOCK_MONOTONIC, &t2);
        elapsed_ns     = (float)get_elapsed_time_nanoseconds(t1, t2);
        elapsed_in_sec = elapsed_ns / (float)1000000000.0;
        printf("    Write BW = %6.4f kB/s (size %6.2f kB  the test took: %12ld ns  --  %3.3f s))\n",
            (float) ((float)buf_size / (float)(1024.0/**1024.0*/)) / elapsed_in_sec,
            (float)buf_size / (float)1024.0,
            elapsed_ns,
            elapsed_in_sec
            );
        fflush(stdout);
        
//        
//        // read access
//        
//        printf("  Test %d: MEM-PC transfer of %6.2f kB\n", it, (float)buf2_size/(float)1024.0);
//        clock_gettime(CLOCK_MONOTONIC, &t1);
//        hn_read_memory(tile, 0x00000000, buf2_size, buf2);
//        clock_gettime(CLOCK_MONOTONIC, &t2);
//        elapsed_ns = (float)get_elapsed_time_nanoseconds(t1, t2);
//        elapsed_in_sec = elapsed_ns / (float)1000000000.0;
//        printf("    Read  BW = %6.4f kB/s (size %6.2f kB  the test took: %12ld ns  --  %3.3f s)\n\n",
//            (float)( (float)buf2_size / (float)(1000.0/**1000.0*/) ) / elapsed_in_sec,
//            (float)buf2_size / (float)1024.0,
//            elapsed_ns,
//            elapsed_in_sec
//            );
//        fflush(stdout);
//
        // End of iteration, release current buffers, next iteration the buffers will be of different size
        free(buf);
        free(buf2);
      }
      printf("   test for current memory finished\n");
    } else {
         printf("\n");
         printf("tile  %d has no memory attached, skipping test for current tile\n\n", tile);
    }
  }
  printf("All memories tested\n");
  printf("test finished\n");
  // We end
  hn_end();

  return (0);
}
