DEPDIR  := .deps

CXX        ?= g++
CC         ?= gcc
build_type ?= "Release"  # pass Debug on command line to enable compilation with debug

sources := boot.c \
           memory_test.c \
           terminal.c \
           memory_rdwr.c \
           register_test.c \
           tx_term.c \
           client_example.c \
           test_burst_transfer_down.c \
           test_burst_transfer.c \
           test_dma.c \
           test_interrupts.c \
           test_interrupts_2.c \
           test_resource_manager.c \
           test_shm_buffer.c \
           test_shm_rw.c \
           test_stats_monitor.c \
           test_synchronization_registers.c \
           test_temp_monitor.c \
           test_weights.c \
           test_write_memory_incremental.c

libhn-path    := ../
hndaemon-path := ../hn_daemon

obj-files := $(sources:%.c=%.o)
bin-files := $(sources:%.c=%)
ccflags-y := -Wall -g -DMANGO
ldflags-y := -Wl,-Bstatic -lhn -Wl,-Bdynamic -pthread -lrt -lm 
dflags-y  = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
incl-dir  := -I$(libhn-path) -I$(hndaemon-path)
lib-dir   := -L$(libhn-path)

ifeq ($(build_type),Debug)
	ccflags-y += -g -DDEBUG 
endif

.PHONY: all clean libhn.a

all: clean $(bin-files) rx_term

libhn.a: 
	@echo "Building libhn.a..."
	@make -C $(libhn-path) libhn.a

%.o : %.c
%.o : %.c $(DEPDIR)/%.d
	@if [ ! -d $(DEPDIR) ]; then mkdir -p $(DEPDIR); fi
	$(CC) $(ccflags-y) $(dflags-y) $(incl-dir) -c $<
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@


.SECONDEXPANSION:
$(bin-files) : $$@.o libhn.a 
	$(CXX) $(lib-dir) $< -o $@ $(ldflags-y)

rx_term : rx_term.o libhn.a
	$(CXX) $(lib-dir) $< -o $@ $(ldflags-y) -lpcap

clean:
	@echo "Cleaning obj and program files..."
	@rm -rf $(DEPDIR)
	@rm -f *.o $(bin-files) rx_term

$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

include $(wildcard $(patsubst %,$(DEPDIR)/%.d,$(basename $(sources))))

