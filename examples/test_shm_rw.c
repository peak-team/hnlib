#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>

#include "hn.h"

float get_elapsed_seconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;

  if ((end.tv_nsec - start.tv_nsec) < 0)
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  }
  else
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }

  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;

  return (float)(elapsed_nanoseconds) / 1e9;
}

int main(int argc, char **argv)
{
  uint32_t   rv;
  uint64_t  *shm_buffer = NULL;
  uint32_t   shm_buffer_size;
  uint32_t   mc_addr;
  uint32_t   tile;
  uint32_t   blocking;
  uint64_t   it;
  uint64_t  *local_buff = NULL;
  uint32_t   write_en;
  uint32_t   read_en;
  uint32_t   magic;

  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO;
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;

  if (argc != 8) {
    printf("USAGE: %s <tile> <buf_size> <base_address_in_hex> <magic> <write_en> <read_en> <blocking>\n", argv[0]);
    exit(0);
  }

  tile = atoi(argv[1]);
  shm_buffer_size = atoi(argv[2]);
  mc_addr = (unsigned int)strtoul(argv[3], NULL, 16);
  magic = atoi(argv[4]);
  write_en = atoi(argv[5]);
  read_en = atoi(argv[6]);
  blocking = atoi(argv[7]);

  printf("\n");
  printf("---------------------------------\n");
  printf("     DEMO Application\n");
  printf("---------------------------------\n");
  printf("\n");

  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");

  if (magic == 0) {
    if (write_en == 1) {
      srand(time(NULL));
      magic = rand();
      printf("Magic number = %d\n", magic);
    } else {
      printf("No magic provided and no write requested - no data check will be performed\n");
    }
  }

  rv = hn_shm_malloc(shm_buffer_size, (void **) &shm_buffer);
  if (rv != HN_SUCCEEDED) {
    printf("Error requesting buffer in shared memory, returned: %u\n", rv);
    hn_end();
    exit(1);
  }

  printf("I have been granted a shm on addr 0x%016lX\n", shm_buffer);

  if (magic != 0 || write_en == 1) {
    local_buff = malloc(shm_buffer_size);

    printf("Filling a local buffer with known data\n");
    for (it = 0; it < shm_buffer_size / sizeof(uint64_t); it++)
    {
      *(local_buff + it) = it + magic;
    }
  }

  if (write_en == 1) {
    float elapsed_in_sec, bw;

    printf("Requesting shm write of %lu bytes to tile %d at addr 0x%08lX\n", shm_buffer_size, tile, mc_addr);

    memcpy(shm_buffer, local_buff, shm_buffer_size);

    {
      struct timespec t1, t2;

      clock_gettime(CLOCK_MONOTONIC, &t1);

      hn_shm_write_memory(shm_buffer, shm_buffer_size, mc_addr, tile, blocking);
      //hn_write_memory(tile, mc_addr, shm_buffer_size, shm_buffer);

      clock_gettime(CLOCK_MONOTONIC, &t2);

      elapsed_in_sec = (float) get_elapsed_seconds(t1, t2);
    }

    printf("Done. BW = %6.4f KB/s. Press enter to continue.\n", ((float)(shm_buffer_size) / 1024.) / elapsed_in_sec);
    getchar();
  }

  bzero(shm_buffer, shm_buffer_size);

  if (read_en == 1) {
    float elapsed_in_sec, bw;

    printf("Requesting shm read of %lu bytes from tile %d at addr 0x%08lX\n", shm_buffer_size, tile, mc_addr);

    {
      struct timespec t1, t2;

      clock_gettime(CLOCK_MONOTONIC, &t1);

      hn_shm_read_memory(shm_buffer, shm_buffer_size, mc_addr, tile, blocking);

      clock_gettime(CLOCK_MONOTONIC, &t2);

      elapsed_in_sec = (float) get_elapsed_seconds(t1, t2);
    }

    printf("Done. BW = %6.4f KB/s.\n", ((float)(shm_buffer_size) / 1024.) / elapsed_in_sec);

    if (magic != 0) {
      do {
        printf("Press enter to continue.\n");
        getchar();

        int errors = 0;

        for (it = 0; it < shm_buffer_size / sizeof(uint64_t) && errors < 20; it++)
        {
          uint64_t *shmptr   = shm_buffer + it;
          uint64_t *localptr = local_buff + it;
          if ((*shmptr) != (*localptr)) {
            printf("Found data mismatch at byte 0x%X : wrote %016lX but read %016lX\n", it * sizeof(uint64_t), *localptr, *shmptr);
            errors++;
          }
        }

        if (errors == 0) {
          printf("Data is clean\n");
          break;
        }
      } while (blocking == 0);
    }
  }

  if (local_buff) {
    free(local_buff);
  }

  hn_shm_free(shm_buffer);

  return 0;
}
