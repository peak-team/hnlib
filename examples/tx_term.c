//////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//-----------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Flich    (jflich@disca.upv.es)
//            R. Tornero  (ratorga@disca.upv.es)
//            J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: May 01, 2016
// Design Name:
// Module Name: tx_term
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//   TX Terminal
//   Application to send commands to MANGO arqchitecture platform
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////



/******************************************************************************
 **
 ** Included files
 **
 ******************************************************************************
 **/
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <sys/socket.h>
#include <features.h>    /* para el numero de version de glibc */
#if __GLIBC__ >= 2 && __GLIBC_MINOR >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>     /* los protocolos de nivel 2 */
#else
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>   /* los protocolos de nivel 2 */
#endif
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <errno.h>

#include <getopt.h>

#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>

#include <errno.h>

#include "commands.h"
#include "peak_commands.h"

#include "../hn.h"

/******************************************************************************
 **
 ** Macro definitions
 **
 ******************************************************************************
 **/

#define ETH_ALEN 6
//Protocol sizes

#define DATA_SIZE 1500
#define PADDING_SIZE 26
#define PROTOCOL_SIZE (2+2+PADDING_SIZE+DATA_SIZE)

//Payload size
#define PAYLOAD_SIZE (6+1+1+PROTOCOL_SIZE)
//Frame to send size
#define FRAME_SIZE (ETH_ALEN+ETH_ALEN+2+PAYLOAD_SIZE)

//ZCT payload size, depends on ZCT module
//#define ZCT_DATA_VECTOR_SIZE 256

#define DIF16(t1,t0) ( (t1>=t0)?(t1-t0):(0xffff-(t0-t1)) )

#define PACKETS_PER_FILE 5000

#define NUMBER_OF_TILES 512
#define L1D_DEBUG_SIZE 6
#define L2_DEBUG_SIZE 6
#define LINE_MAX_LENGTH 2048
#define NUMBER_OF_SWITCHES 64




#define MY_DEST_MAC0    0xFF
#define MY_DEST_MAC1    0xFF
#define MY_DEST_MAC2    0xFF
#define MY_DEST_MAC3    0xFF
#define MY_DEST_MAC4    0xFF
#define MY_DEST_MAC5    0xFF

#define MY_SRC_MAC0    0xDA
#define MY_SRC_MAC1    0x01
#define MY_SRC_MAC2    0x02
#define MY_SRC_MAC3    0x03
#define MY_SRC_MAC4    0x04
#define MY_SRC_MAC5    0x05

#define DEFAULT_IF    "eth1"
#define BUF_SIZ        2048

// Write down the data frame length here. For 3bytes/item should be that to keep
// the same behaviour as for 2bytes/item case. In that way frame has capacity for 72 items always
// Change to 0x90 for the 2bytes/item case
#define FRAME_LENGTH  (u_int8_t)0xD8

#define SECOND_ITEM_BYTE(tile, CMD) (u_int8_t)((tile << 6) | CMD)
#define THIRD_ITEM_BYTE(tile)       (u_int8_t)((tile & 0xFFC) >> 2)      // added, item is 3 bytes now (to address 512 tiles)

// FIELD REG MASKs
#define TYPE_RDWR_MS_MASK         0x0000000F
#define SIZE_RDWR_MS_MASK         0x0000FFFF
#define RATE_RDWR_MS_MASK         0x0000007F
#define SET_RDWR_MS_MASK          0x00000001
#define VN_RDWR_MS_MASK           0x00000003
#define DEST_RDWR_MS_MASK         0x000001FF 
#define RST_RDWR_MS_MASK          0x00000000
#define ONE_WEIGHT_MASK           0x00000003
//#define SET_WEIGHTS_MASK          0x00000001
//#define WEIGHTS_MASK              0x00000001


// Field offset (from register lsb)
#define TYPE_RDWR_MS_OFFSET     25
#define SIZE_RDWR_MS_OFFSET     9
#define SET_RDWR_MS_OFFSET      29     
#define VN_RDWR_MS_OFFSET       7
#define DEST_RDWR_MS_OFFSET     0
//#define SET_WEIGHTS_OFFSET      29
//#define WEIGHTS_OFFSET          8
//#define WEIGHTS_VECTOR_OFFSET   9

// registers
#define MANGO_TIMESTAMP_REG           "0x00000001"
#define MANGO_MS_INJECT_DST_REG       "0x00000007"
#define MANGO_MS_INJECT_OTH_REG       "0x00000008"
#define MANGO_WEIGHTS_VECTOR_REG      "0x00000009"
#define MANGO_NET_STATS_REG           "0x0000000A"
#define MANGO_TLB_REG                 11


#define MANGO_WRITE_REGISTER_TO_PEAK_TILE (u_int8_t)5

// macros
#define MAX_MACROS 50


/*
 * Function prototypes
 */
uint32_t hn_peak_write_protocol(const char *file_name, uint32_t tile);                                             
uint32_t hn_peak_set_new_pc(uint32_t tile, uint32_t core, uint32_t addr);  
uint32_t hn_peak_unfreeze(uint32_t tile, uint32_t core);  
uint32_t hn_read_memory_block(uint32_t tile, uint32_t addr, char *data);


/******************************************************************************
 **
 ** Global variables
 **
 ******************************************************************************
 **/

extern int errno;

// macros
char macro_name[MAX_MACROS][BUF_SIZ];
char macro_string[MAX_MACROS][BUF_SIZ];
char macro_description[MAX_MACROS][BUF_SIZ];
int num_macros;
char macros_fname[512];

int use_hnlib;    // when set the program uses hn library
int payload_0_index_first_byte;
int tx_len_0;


/******************************************************************************
 * @brief
 *
 * @param 
 *
 * @return ERROR
 ******************************************************************************
 **/
int send_frame(int sockfd, char *sendbuf_0, struct sockaddr *socket_address, int sockaddr_ll)
{
  if (use_hnlib == 0) {
    return sendto(sockfd, sendbuf_0, tx_len_0, 0, socket_address, sockaddr_ll);
  }
 
  return 1;
}

/******************************************************************************
 * @brief Reads macro function file containing sets of operations
 *
 * @param NONE
 * 
 * @return NONE
 ******************************************************************************
 **/
void read_macros()
{
  char *fline = NULL;
  size_t flen = 0;
  FILE *fd;
  ssize_t fread;

  num_macros = 0;

  char macro_processed[MAX_MACROS]; 
  memset(macro_processed, 0, MAX_MACROS);    

  if( (fd = fopen("macros.txt", "r")) != NULL) {
    while ((fread = getline(&fline, &flen, fd)) != -1) {
      char *p1 = strstr(fline, ",");
      if (p1) {
        strncpy(macro_name[num_macros], fline, p1-fline);
        macro_name[num_macros][p1-fline] = '\0';
        char *p2 = strstr(p1+1, ",");
        if (p2) {
          strncpy(macro_string[num_macros], p1+1, p2-p1-1);
          macro_string[num_macros][p2-p1-1] = '\0';
          strcpy(macro_description[num_macros], p2+1);
        } else {
          strcpy(macro_string[num_macros], p1+1);
        }
        // description
        num_macros++;
      }
    }
    fclose(fd);
  }

  if ((fd = fopen(macros_fname, "r")) != NULL) {
    while ((fread = getline(&fline, &flen, fd)) != -1) {
      char *p1 = strstr(fline, ",");
      if (p1) {
        strncpy(macro_name[num_macros], fline, p1-fline);
        macro_name[num_macros][p1-fline] = '\0';
        char *p2 = strstr(p1+1, ",");
        if (p2) {
          strncpy(macro_string[num_macros], p1+1, p2-p1-1);
          macro_string[num_macros][p2-p1-1] = '\0';
          strcpy(macro_description[num_macros], p2+1);
        } else {
          strcpy(macro_string[num_macros], p1+1);
        }
        // description
        num_macros++;
      }
    }
    fclose(fd);
  }

  // preprocessing for expanding macros (macros inside macros)
  int i, j, offset1, offset2, len1, found;
  char *p1;
  char line1[BUF_SIZ], line2[BUF_SIZ];
  i = 0;
  while (i < num_macros) {
    found = 0;
    macro_processed[i] = 1;
    strcpy(line1, macro_string[i]);  
    //printf("macro %s to expand: %s\n", macro_name[i], line1);
    p1 = strtok(line1, " ");
    do {  
      //printf("expanding token: |%s|\n", p1);
      for (j = 0; j < num_macros; j++) {
        if (!strcmp(macro_name[j], p1) && !macro_processed[j]) {
          macro_processed[j] = 1;

          //printf("token %s is a macro: |%s|\n", p1, macro_string[j]);

          len1 = p1-line1; 
          strncpy(line2, macro_string[i], len1);

          //line2[len1] = 0;
          //printf("1-phase expanded: |%s| [0,%d]\n", line2, len1);

          offset2 = len1;
          len1 = strlen(macro_string[j]);
          strncpy(line2+offset2, macro_string[j], len1);

          //line2[offset2+len1] = 0;
          //printf("2-phase expanded: |%s| [%d,%d]\n", line2, offset2, len1);

          offset2 += len1;
          offset1 = (size_t)(p1 - line1) + strlen(p1);
          len1 = strlen(macro_string[i]) - offset1;
          strncpy(line2+offset2, macro_string[i]+offset1, len1);
          offset2 += len1;
          line2[offset2] = 0;

          //printf("3-phase expanded: |%s| [%d,%d]\n", line2, offset2, len1);
          //printf("expanded token %s with macro %s: |%s|\n", p1, macro_string[j], line2); 

          strcpy(macro_string[i], line2);
          found = 1;
          break;
        } else if (!strcmp(macro_name[j], p1)) {
          printf("WARNING!!!! token %s is a macro already expanded. Possible infinite loop.\n", macro_name[j]);

          len1 = p1-line1; 
          strncpy(line2, macro_string[i], len1);

          //line2[len1] = 0;
          //printf("1-phase expanded: |%s| [0,%d]\n", line2, len1);

          if (len1 == 0) {
            offset2 = 0;
            offset1 = (size_t)(p1 - line1) + strlen(p1) + 1;
          } else {
            offset2 = len1-1;
            offset1 = (size_t)(p1 - line1) + strlen(p1);
          }

          len1 = strlen(macro_string[i]) - offset1;
          strncpy(line2+offset2, macro_string[i]+offset1, len1);
          offset2 += len1;
          line2[offset2] = 0;

          //printf("3-phase expanded: |%s| [%d,%d]\n", line2, offset2, len1);
          //printf("expanded token %s with macro '': |%s|\n", p1, line2); 

          strcpy(macro_string[i], line2);
          found = 1;
          break;
        }
      }
    } while ((p1 = strtok(NULL, " ")) != NULL && !found);
    if (!found) {
      memset(macro_processed, 0, MAX_MACROS);                      
      i++;
    }
  }

}

/******************************************************************************
 * @brief Converts binary value passed as parameter to integer
 *
 * @param sz_bin binary value to convert
 * 
 * @return decimal value
 ******************************************************************************
 **/
int bin2dec(char *sz_bin)
{
  int max = strlen(sz_bin);
  int k;
  int weight;
  int value;

  value = 0;
  weight = 1;
  for (k=max-1;k>=0;k--) {
    if (sz_bin[k] == '1') value = value + weight;
    weight = weight * 2;
  }

  return value;
}

/******************************************************************************
 * @brief gets the decimal value represented by a character representing an HEX
 *
 * @param digit ASCII char to process
 * 
 * @return value of character passed as parameter
 ******************************************************************************
 **/
unsigned char weight_hex(char digit)
{
  if ((digit >= '0') && (digit <= '9')) return digit - '0';
  if ((digit >= 'a') && (digit <= 'f')) return 10 + (digit - 'a');
  if ((digit >= 'A') && (digit <= 'F')) return 10 + (digit - 'A');

  return 0;
}

/******************************************************************************
 * @brief Convert 32 bit hex value as char string to its value 
 *
 * @param hex_value string containing hex value
 * 
 * @return converted value
 ******************************************************************************
 **/
unsigned long int hex_value_to_ul( char hex_value[8]) {
  unsigned long int ul_value;
  ul_value =   ((unsigned long int)weight_hex(hex_value[0]) * (unsigned long int)268435456 ) + ( (unsigned long int)weight_hex(hex_value[1]) * (unsigned long int)16777216 )
             + ((unsigned long int)weight_hex(hex_value[2]) *   (unsigned long int)1048576 ) + ( (unsigned long int)weight_hex(hex_value[3]) *    (unsigned long int)65536 )
             + ((unsigned long int)weight_hex(hex_value[4]) *     (unsigned long int) 4096 ) + ( (unsigned long int)weight_hex(hex_value[5]) *      (unsigned long int)256 )
             + ((unsigned long int)weight_hex(hex_value[6]) *        (unsigned long int)16 ) + ( (unsigned long int)weight_hex(hex_value[7])            )  ;
  
  return ul_value;
}

/******************************************************************************
 * @brief Convert unsigned long value to 32 bit hex value as char string.
 *        only returns values that fit in 8 chars (32bit)!!
 *
 * @param addr_with_offset pointer to char array to store the converted value
 * @param value to convert to char array
 * 
 * @return NONE
 ******************************************************************************
 **/
void ul_value_to_32bit_hex_as_char_array(char *addr_with_offset, unsigned long int value) {
  char hex_str[9];
  snprintf(hex_str, 9,"%08lX", value);
  //printf(   "la cadena convertida es:  %s\n", hex_str);
  strncpy(addr_with_offset, &hex_str[0], 8);
}

/******************************************************************************
 * @brief Compares 2 MAC addresses.
 *
 * @param mac1 First MAC Addresss
 * @param mac2 Second MAC Addresses
 * 
 * @return 1 if both addresses are equal. 0 otherwise
 ******************************************************************************
 **/
int cmpMAC(unsigned char* mac1, unsigned char* mac2){

  int i;
  int ret = 0;

  for (i=0; i <ETH_ALEN; i++) {
    if (mac1[i]==mac2[i]) {
      ret = 1;
    } else  {
      ret = 0;
      break;
    }
  }

  return ret;
}

/******************************************************************************
 * @brief Display error message and exit 
 *
 * @param err message to display
 * 
 * @return NONE
 ******************************************************************************
 **/
void program_error ( char * err )
{
  perror ( err );
  exit (-1);
}

/******************************************************************************
 * @brief Swap MSB <>LSB bytes
 *
 * @param original_value 16 bit data to swap bytes
 * 
 * @return data with swapped bytes
 ******************************************************************************
 **/
u_int16_t swap_bytes(u_int16_t original_value)
{
  u_int16_t new_value = 0; // Start with a known value.
  u_int16_t byte;          // Temporary variable.

  // Copy the lowest order byte from the original to
  // the new value:
  byte = original_value & 0xFF;  // Keep only the lowest byte from original value.
  new_value = new_value * 0x100; // Shift one byte left to make room for a new byte.
  new_value |= byte;             // Put the byte, from original, into new value.

  // For the next byte, shift the original value by one byte
  // and repeat the process:
  original_value = original_value >> 8; // 8 bits per byte.
  byte = original_value & 0xFF;  // Keep only the lowest byte from original value.
  new_value = new_value * 0x100; // Shift one byte left to make room for a new byte.
  new_value |= byte;             // Put the byte, from original, into new value.

  return new_value;
}

/******************************************************************************
 * @brief Get integer value of HEX character 0 to F converted to 0 to 16
 *
 * @param c char representing hex value
 * 
 * @return integer value of hex digit
 ******************************************************************************
 **/
int char_to_val(char c){
  int ret = 0;

  if  ((c >= '0') && (c <= '9')) {
    ret = c - '0';
  } else if ((c >= 'a') && (c <= 'f')) {
    ret = c - 'a' + 10;
  } else if ((c >= 'A') && (c <= 'F')) {
    ret = c - 'A' + 10;
  }

  return ret;
}

/******************************************************************************
 * @brief 
 *
 * @param str
 * @param new_value
 * 
 * @return 
 ******************************************************************************
 **/
int strToChar(char *str, char *new_value) {
  char   *endptr;
  int64_t value;
  int    ret;

  value = 0;
  ret = 0;


  if(strlen(str) == 0) {
    printf("   ERR: empty string, returning value '0'\n");
  } else {
    errno = 0;
    value = strtol(str, &endptr, 10);
    if ((errno != 0 ) || (str == endptr) || ((str + strlen(str)) != endptr)) {
      printf("   ERR: Failed conversion of (%s) to int64_t \n", str);
      printf("   errno(%d)   string(%s)   converted as int64_t value(%ld)\n",
          errno,
          str,
          value);
    } else {
      if((value >= 0) && (value <64)) {
        ret = 1;
        *new_value = (char)value;
      }
    }
  }

  return ret;
}

/******************************************************************************
 * @brief Process command typed in app and performs required operations
 *
 * @param new_command     command text 
 * @param sendbuf_0
 * @param sockfd          transmission eth socket file descriptor
 * @param socket_address
 * 
 * @return 
 ******************************************************************************
 **/
char ProcessCommand(char *new_command, char *sendbuf_0, int sockfd, struct sockaddr_ll *socket_address){
  char cont;
  struct timespec ts;
  int i;

  cont = 1;
  ts.tv_sec = 0;
  ts.tv_nsec = 1000000;

  //printf("entering function with command %s\n", new_command);
  //printf("New command: ");

  if(strcmp(new_command, "h") == 0) {
    printf("\n");
    printf("\n");
    printf("----------------\n");
    printf("   help menu    \n");
    printf("----------------\n");
    printf("\n");
    printf("   'r_<tile>'                                               software reset\n");
    printf("   'f_<tile>'                                               freeze a tile\n");
    printf("   'wa_<tile>_<addr>_<value>'                               write value   to address of TileRegister, address and value in hex format \"0x12345678\n");
    printf("   'ra_<tile>_<addr>'                                       read  value from address of TileRegister, address in hex format \"0x12345678\n");
    printf("   'rm_<tile>_<addr>'                                       read memory address 0x12345678\n");
    printf("   'rmw_<tile>_<addr>'                                      read memory address 0x12345678\n");
    printf("   'wmw_<tile>_<addr>_<32bit-value>'                        write 32-bit word value in memory. Address and value in hex format 0x12345678\n");
    printf("   'wmh_<tile>_<addr>_<32bit-value>'                        write half word value in memory. Address and value in hex format 0x12345678\n");
    printf("   'wmb_<tile>_<addr>_<32bit-value>'                        write byte  value in memory. Address and value in hex format 0x12345678\n");
    printf("   'wm_<tile>_<filename>'                                   write memory contents\n");
    printf("   'wmao_<tile>_<offset>_<filename>'                        write memory contents with address offset, address offset in hex format \"0x12345678\"\n");
    printf("   'tlb_<tile>_<entry>_<va_ini>_<va_end>_<pa>_<mem|tr>_<tile_rsc>_<reg>'  Sets a TLB entry in a given tile. Parameters:\n");
    printf("                                                                        - <tile>      : Tile where the TLB is located\n");
    printf("                                                                        - <entry>     : TLB entry (0 to 3)\n");
    printf("                                                                        - <va_ini>    : Initial virtual address\n");
    printf("                                                                        - <va_end>    : Final virtual address\n");
    printf("                                                                        - <pa>        : Initial physical address\n");
    printf("                                                                        - <mem|tr>    : Whether the physical address belongs to memory or to a register in TILEREG structure\n");
    printf("                                                                        - <tile_rsc>  : Tile where the physical resource (memory or TILEREG) is located\n");
    printf("                                                                        - <reg>       : Register of TILEREG that implements the physical address (if tr is set)\n");
    // following commands are for peak
    printf("   'wcp_<mango_tile>_<filename>'                            PEAK: write coherence protocol in PEAK tile 0 of mango <tile> passed as argument\n");
    printf("   'npc_<mango_tile>_<unit>_<address>'                      PEAK: sets new pc address of a PEAK tile\n");
    printf("   'u_<mango_tile>_<PEAKunit>'                              PEAK: un-freeze PEAK tile in noc\n");
    printf("   'cmd_<mango_tile>'                                       PEAK: put term in PEAK command mode. Next command will be sent to master PEAK processor in <mango_tile>. Type \'help\' after entering cmd mode to display help menu\n");
    printf("   'cde_<mango_tile>_<unit>'                                PEAK: CORE debug  enable for a PEAK unit in a MANGO tile\n");  
    printf("   'cdd_<mango_tile>_<unit>'                                PEAK: CORE debug disable for a PEAK unit in a MANGO tile\n");
    printf("   'cdpme_<mango_tile>_<unit>'                              PEAK: CORE FORENSE debug  enable for a PEAK unit in a MANGO tile\n");  
    printf("   'cdpmd_<mango_tile>_<unit>'                              PEAK: CORE FORENSE debug disable for a PEAK unit in a MANGO tile\n");
    printf("   'ccde_<mango_tile>_<unit>'                               PEAK: CACHE CONTROLLER debug enable for a PEAK unit in a MANGO tile\n");  
    printf("   'ccdd_<mango_tile>_<unit>'                               PEAK: CACHE CONTROLLER debug disable for a PEAK unit in a MANGO tile\n");
    printf("   'trde_<mango_tile>_<unit>'                               PEAK: TILEREG debug enable for a PEAK unit in a MANGO tile\n");  
    printf("   'trdd_<mango_tile>_<unit>'                               PEAK: TILEREG debug disable for a PEAK unit in a MANGO tile\n");
    printf("   'vnde_<mango_tile>_<unit>'                               PEAK: VN debug enable for a PEAK unit in a MANGO tile\n");  
    printf("   'vndd_<mango_tile>_<unit>'                               PEAK: VN debug disable for a PEAK unit in a MANGO tile\n");
    printf("   'echoe_<mango_tile>_<unit>'                              PEAK: NOTIFICATION/ECHO enable for a PEAK unit in a MANGO tile\n");  
    printf("   'echod_<mango_tile>_<unit>'                              PEAK: NOTIFICATION/ECHO disable for a PEAK unit in a MANGO tile\n");
    printf("   'rap_<mango_tile>_<unit>_<tilereg_address>'              PEAK: reads a tile register of a PEAK tile\n");
    printf("   'wap_<mango_tile>_<unit>_<tilereg_address>_<data>        PEAK: writes a tile register of a PEAK tile with data (in 0x format)\n");
     // resume with mango commands
    printf("   'se_<tile>'                                              stats enable of a tile\n");
    printf("   'sd_<tile>'                                              stats disable of a tile\n");
    printf("   'reset_nstats'                                           (resets all statistics) in all tiles\n");
    printf("   'nshow'                                                  (shows network statistics)\n");
    printf("   'weights_<vn2%%bw>_<vn1%%bw>_<vn0%%bw>'                  (switches guaranteed bandwidth percentage for each virtual networks)\n");
    printf("   'swinject_<tile>_<mode>_<msize>_<irate>_<vn>_<dsttile>'  switches on and off a tile's message generator\n");
    printf("                                                            mode 0: <vn> and <dsttile> will be rnd\n");
    printf("                                                            mode 1: <dsttile> will be rnd\n");
    printf("                                                            mode 2: <vn> will be rnd\n");
    printf("                                                            mode 3: <vn> and <dsttile> must be selected\n");
    printf("   'tre_<tile>'                                             TR DEBUG enable for a tile\n");
    printf("   'trd_<tile>'                                             TR DEBUG disable for a tile\n");
    printf("   'u2me_<tile>'                                            U2M debug enable for a tile\n");
    printf("   'u2md_<tile>'                                            U2M debug disable for a tile\n");
    printf("   'm2ue_<tile>'                                            M2U debug enable for a tile\n");
    printf("   'm2ud_<tile>'                                            M2U debug disable for a tile\n");
    printf("   'injde_<tile>'                                           NET INJECT debug enable for a tile\n");
    printf("   'injdd_<tile>'                                           NET INJECT debug disable for a tile\n");
    printf("   'ejede_<tile>'                                           NET EJECT debug enable for a tile\n");
    printf("   'ejedd_<tile>'                                           NET EJECT debug disable for a tile\n");
    printf("   'mde_<tile>'                                             MC debug enable for a tile\n");
    printf("   'mdd_<tile>'                                             MC debug disable for a tile\n");
    printf("   'echoe_<tile>'                                           echo enable for a tile\n");
    printf("   'echod_<tile>'                                           echo disable for a tile\n");
    printf("   'stopclk_<tile>'                                         stops system clock\n");
    printf("   'resumeclk_<tile>'                                       resumes system clock\n");
    printf("   'ticks_<tile>_<value>                                    runs the system for 2^<value> clock ticks; value ranges [15-0]\n");
    printf("   'z_<value>                                               sleeps during <value> nanoseconds\n");
    printf("   'reload_macros'                                          reloads macros in macros.txt file\n"); 
    printf("   'q'                                                      quits the application\n");
    printf("   'h'                                                      help. Shows this menu.\n");
    printf("   ------------------------------------------------------------------------------------------------------------------------------------------------\n");

    if (num_macros) {
      printf("   macros:\n");
      for (i=0;i<num_macros;i++) printf("   %s\t\t\t%s", macro_name[i], macro_description[i]);
    } else printf("   (no macros found)\n");
    printf("\n");
    printf("\n");
  } else if(strncmp(new_command, "r_", 2) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- r command (RESET)
    // ---------------------------------------------------------------------------------------------------------
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);
    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH; //this frame will be min-payload size

    sendbuf_0[index++] = 0x00;                                            // Now, item is 3 bytes (to address 512 tiles). Input LSB -> MSB
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_RESET_SYSTEM); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
      printf("Sending RESET frame\n");
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      sleep(5);
    } else {
      hn_reset(0);
    }
  } else if (strncmp(new_command, "f_", 2) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- f command (FREEZE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = 0x01;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_TILE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
      printf("Sending FREEZE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
//      hn_peak_freeze(tile, 0);
    }
  }
  else if (strncmp(new_command, "u_", 2) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- u command (UNFREEZE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending un-freeze command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_PEAK_CONFIGURE_TILE);
      peak_item_b0    = 0x02; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);

      if (use_hnlib == 0) { 
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending UN-FREEZE frame to PEAK tile %d\n", tile);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_unfreeze(tile, unit);
      }
    } else
    {
      printf("u_ command wrong syntax\n");
    }

  }
  else if (strncmp(new_command, "ccde_", 5) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- ccde command (COMMAND_PEAK_CACHE_CONTROLLER_DEBUG_ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending CC debug enable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_DEBUG_ENABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);

      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending CC_DEBUG_ENALBE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_debug_protocol_enable(tile, unit);
      }

    } else
    {
      printf("ccde_ command wrong syntax\n");
    }

  }

  else if (strncmp(new_command, "ccdd_", 5) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- cdd command (COMMAND_PEAK_CACHE_CONTROLLER_DEBUG_DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending CC debug disable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_DEBUG_DISABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);


      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending CC_DEBUG_DISABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_debug_protocol_disable(tile, unit);
      }
    } else
    {
      printf("ccdd_ command wrong syntax\n");
    }

  } 
  else if (strncmp(new_command, "trde_", 5) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- trde command (COMMAND_PEAK_TR_DEBUG_ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending TR debug enable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_TR_DEBUG_ENABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);

      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending TR_DEBUG_ENALBE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
     } else {
//       hn_peak_debug_tilereg_enable(tile, unit);
     }
    } else
    {
      printf("trde_ command wrong syntax\n");
    }

  }

  else if (strncmp(new_command, "trdd_", 5) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- trdd command (COMMAND_PEAK_TR_DEBUG_DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending TR debug disable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_TR_DEBUG_DISABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);

      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending TR_DEBUG_DISABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
//        hn_peak_debug_tilereg_disable(tile, unit);
      }
    } else
    {
      printf("trdd_ command wrong syntax\n");
    }

  } 
  else if (strncmp(new_command, "vnde_", 5) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- vnde command (COMMAND_PEAK_FLIT_DEBUG_ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending VN debug enable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_FLIT_DEBUG_ENABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);

      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending VN_DEBUG_ENABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
//        hn_peak_debug_virtual_network_enable(tile, unit);
      }
    } else
    {
      printf("trde_ command wrong syntax\n");
    }

  }

  else if (strncmp(new_command, "vndd_", 5) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- vndd command (COMMAND_PEAK_FLIT_DEBUG_DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending VN debug disable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_FLIT_DEBUG_DISABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);

      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending VN_DEBUG_DISABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
//        hn_peak_debug_virtual_network_disable(tile, unit);
      }
    } else
    {
      printf("vndd_ command wrong syntax\n");
    }




  } else if (strncmp(new_command, "cde_", 4) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- cde command (COMMAND_PEAK_CORE_DEBUG_ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending core debug enable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_CORE_DEBUG_ENABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);

      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending CORE_DEBUG_ENALBE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_debug_core_enable(tile, unit);
      }
    } else
    {
      printf("cde_ command wrong syntax\n");
    }

  }

  else if (strncmp(new_command, "cdd_", 4) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- cdd command (COMMAND_PEAK_CORE_DEBUG_DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending core debug disable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_CORE_DEBUG_DISABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);


      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending CORE_DEBUG_DISABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_debug_core_disable(tile, unit);
      }
    } else
    {
      printf("cdd_ command wrong syntax\n");
    }

  } else if (strncmp(new_command, "cdpme_", 6) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- cdpme command (COMMAND_PEAK_CORE_FORENSE_DEBUG_ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending core forense debug enable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_POST_MORTEN_CORE_DEBUG_ENABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);


      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending CORE_FORENSE_DEBUG_ENABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_debug_core_forense_enable(tile, unit);
      }
    } else
    {
      printf("cdpme_ command wrong syntax\n");
    }

  }

  else if (strncmp(new_command, "cdpmd_", 6) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- cdpmd command (COMMAND_PEAK_CORE_DEBUG_FORENSE_DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending core debug forense disable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_PEAK_POST_MORTEN_CORE_DEBUG_DISABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);


      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending CORE_DEBUG_FORENSE_DISABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_debug_core_forense_disable(tile, unit);
      }
    } else
    {
      printf("cdpmd_ command wrong syntax\n");
    }

  } else if (strncmp(new_command, "echoe_", 6) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- cde command (COMMAND_PEAK_NOTIFICATION/ECHO_ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending NOTIFICATION/ECHO enable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_ECHO_ENABLE;
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);


      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending ECHO_ENALBE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_echo_enable(tile, unit);
      }
    } else
    {
      printf("echoe_ command wrong syntax\n");
    }

  }
  else if (strncmp(new_command, "echod_", 6) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- cdd command (COMMAND_PEAK_ECHO_DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    //char tilesz[10];
    //char *p1 = strstr(new_command, "_")+1;
    //strcpy(tilesz, p1);
    //int tile = atoi(tilesz);
    char  tilesz[12];
    char  unitsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    int   tile;
    int   unit;   
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");

    if(p1 && p2 ) 
    {
      p1++;
      p2++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strcpy(unitsz, p2);

      tile = atoi (tilesz);
      unit = atoi (unitsz);

      printf("Sending NOTIFICATION/ECHO disable command frame to  MANGO tile: %d  PEAK tile: %d\n", tile, unit);


      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_CONFIGURE_DEBUG_AND_STATS);
      peak_item_b0    = COMMAND_ECHO_DISABLE; // from verilog ITEM_parameters.h --> ITEM_CODE_CLOCK_RESUME
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      // 
      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //sendbuf_0[index++] = 0x00;

      //printf("el valor de index es: %d\n", index);


      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
        printf("Sending ECHO_DISABLE frame to MANGO tile %d - PEAK tile %d\n", tile, unit);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_echo_disable(tile, unit);
      }

    } else
    {
      printf("echod_ command wrong syntax\n");
    }

  }
  else if (strncmp(new_command, "rap_", 4) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- rap command (COMMAND_READ_TILE_REG)
    // ---------------------------------------------------------------------------------------------------------
    //
    char  tilesz[12];
    char  unitsz[12];
    char  addrsz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    char *p3 = NULL;
    int   tile;
    int   unit;   
    int   addr;
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");
    p3   = strstr(p2 + 1, "_");

    if(p1 && p2 && p3) 
    {
      p1++;
      p2++;
      p3++;
      
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strncpy(unitsz, p2, p3-p2-1);
      unitsz[p3-p2-1] = 0;
      strcpy(addrsz, p3);

      tile = atoi (tilesz);
      unit = atoi (unitsz);
      addr = atoi (addrsz);

      printf("Sending READ_TILEREG_ADDRESS command frame to  MANGO tile: %d  PEAK tile: %d  ADDRESS: %d\n", tile, unit, addr);

      // tilereg address should be given as 2 bytes so two MANGO TR writings are required to send
      // the complete tilereg address to read in a peak tile inside a mango tile

      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
      u_int8_t  wr_reg_for_peak;

      index           = payload_0_index_first_byte;
      peak_item_b3    = 0x00;
      peak_item_b2    = THIRD_ITEM_BYTE(unit);
      peak_item_b1    = SECOND_ITEM_BYTE(unit, COMMAND_READ_TILEREG);
      peak_item_b0    = (u_int8_t)addr;
      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      // Compose MANGO items
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //bytes of PEAK item
      sendbuf_0[index++] = peak_item_b0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);

      sendbuf_0[index++] = wr_reg_for_peak;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      //bytes of PEAK item
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      sendbuf_0[index++] = peak_item_b1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      sendbuf_0[index++] = peak_item_b2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
      sendbuf_0[index++] = peak_item_b3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);

      if (use_hnlib == 0) {
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
          printf("Sending READ_TILEREG_ADDRESS frame to MANGO tile %d - PEAK tile %d - ADDRESS %d\n", tile, unit, addr);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_read_register(tile, unit, addr, NULL);
      }
    } else
    {
      printf("rap_ command wrong syntax\n");
    }
  } else if (strncmp(new_command, "wap_", 4) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- wap command (COMMAND_WRITE_TILE_REG)
    // ---------------------------------------------------------------------------------------------------------
    //
    char  tilesz[12];
    char  unitsz[12];
    char  addrsz[12];
    char  datasz[12];
    char *p1 = NULL;;
    char *p2 = NULL;
    char *p3 = NULL;
    char *p4 = NULL;
    int   tile;
    int   unit;   
    int   addr;
    int   index;
    unsigned int   data;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");
    p3   = strstr(p2 + 1, "_");
    p4   = strstr(p3 + 1, "_");

    if(p1 && p2 && p3 && p4) 
    {
      p1++; p2++; p3++; p4++;
      
      strncpy(tilesz, p1, p2-p1-1); tilesz[p2-p1-1] = '\0';
      strncpy(unitsz, p2, p3-p2-1); unitsz[p3-p2-1] = 0;
      strncpy(addrsz, p3, p4-p2-1); addrsz[p4-p3-1] = 0;
      strcpy(datasz, p4+2);

      tile = atoi (tilesz); unit = atoi (unitsz); addr = atoi(addrsz); data = hex_value_to_ul(datasz);

      printf("Sending WRITE_TILEREG_ADDRESS command frame to  MANGO tile: %d  PEAK tile: %d  ADDRESS: %08x DATA: %08x\n", tile, unit, addr, data);

      // we need to send 5 PEAK items, each one takes 6 MANGO items, so, in total 30 items
      u_int32_t  peak_items[5];
      peak_items[0] = (0x00 << 24) | (THIRD_ITEM_BYTE(unit) << 16) | (SECOND_ITEM_BYTE(unit, COMMAND_WRITE_TILEREG) << 8) | (u_int8_t)addr;
      peak_items[1] = (0x00 << 24) | (THIRD_ITEM_BYTE(unit) << 16) | (SECOND_ITEM_BYTE(unit, COMMAND_WRITE_TILEREG) << 8) | (data & 0x000000FF);
      peak_items[2] = (0x00 << 24) | (THIRD_ITEM_BYTE(unit) << 16) | (SECOND_ITEM_BYTE(unit, COMMAND_WRITE_TILEREG) << 8) | ((data & 0x0000FF00) >> 8);
      peak_items[3] = (0x00 << 24) | (THIRD_ITEM_BYTE(unit) << 16) | (SECOND_ITEM_BYTE(unit, COMMAND_WRITE_TILEREG) << 8) | ((data & 0x00FF0000) >> 16);
      peak_items[4] = (0x00 << 24) | (THIRD_ITEM_BYTE(unit) << 16) | (SECOND_ITEM_BYTE(unit, COMMAND_WRITE_TILEREG) << 8) | ((data & 0xFF000000) >> 24);

      u_int8_t wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

      index = payload_0_index_first_byte;
      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;
      int i;
      for (i=0; i < 5; i++) {   // ??????? 5 to 0 ???
        // two items for MANGO TILEREG address ({00,wr_reg_for_peak})
        sendbuf_0[index++] = wr_reg_for_peak;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG);
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
        sendbuf_0[index++] = 0x00;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG);
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
        // four items for data to be written (for each peak item)
        sendbuf_0[index++] = peak_items[i] & 0x000000FF;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG);
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
        sendbuf_0[index++] = (peak_items[i] & 0x0000FF00) >> 8;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG);
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
        sendbuf_0[index++] = (peak_items[i] & 0x00FF0000) >> 16;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG);
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
        sendbuf_0[index++] = (peak_items[i] & 0xFF000000) >> 24;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG);
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
     }

       if (use_hnlib == 0) {
         for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
         printf("Sending WRITE_TILEREG_ADDRESS frame to MANGO tile %d - PEAK tile %d - ADDRESS %08x DATA %08x\n", tile, unit, addr, data);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_peak_write_register(tile, unit, addr, data);
      }
    } else
    {
      printf("wap_ command wrong syntax\n");
    }
  }
  else if (strncmp(new_command, "wa_", 3) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- wa command (write address of BankRegister)
    //  Address is 32bit  in hex format  -> 0xAAAABBBB
    //  Data    is 32 bit in hex format  -> 0xDATADATA
    // ---------------------------------------------------------------------------------------------------------
    // lets get the tile, address, and numcores
    printf("Write address of RegBank\n");
    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;         // address
    char *p3 = strstr(p2, "_")+1;         // value
    char tilesz[12];      
    char addresssz[12];
    char valuesz[12];
    // do not copy initial 0x
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = '\0';
    printf("    tilesz: %s\n", tilesz);
    strncpy(addresssz, p2, p3-p2-1);
    addresssz[p3-p2-1] = '\0';
    printf("  address: %s\n", addresssz);
    // do not copy initial 0x
    strcpy(valuesz, p3);
    printf("     value: %s \n", valuesz);

    int tile = atoi(tilesz);
    int addr = strtol(addresssz, NULL, 16);
    int value = strtol(valuesz, NULL, 16);

    unsigned char address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
    unsigned char address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
    unsigned char address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
    unsigned char address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);

    unsigned char value_byte_3 = weight_hex(valuesz[2]) * 16 + weight_hex(valuesz[3]);
    unsigned char value_byte_2 = weight_hex(valuesz[4]) * 16 + weight_hex(valuesz[5]);
    unsigned char value_byte_1 = weight_hex(valuesz[6]) * 16 + weight_hex(valuesz[7]);
    unsigned char value_byte_0 = weight_hex(valuesz[8]) * 16 + weight_hex(valuesz[9]);

    printf("Write address of bank register\n");
    printf("            tile : %d\n", tile);
    printf("  address_byte_3 : 0x%02x\n", address_byte_3);
    printf("  address_byte_2 : 0x%02x\n", address_byte_2);
    printf("  address_byte_1 : 0x%02x\n", address_byte_1);
    printf("  address_byte_0 : 0x%02x\n", address_byte_0);
    printf("    value_byte_3 : 0x%02x\n", value_byte_3);
    printf("    value_byte_2 : 0x%02x\n", value_byte_2);
    printf("    value_byte_1 : 0x%02x\n", value_byte_1);
    printf("    value_byte_0 : 0x%02x\n", value_byte_0); 


    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

    sendbuf_0[index++] = value_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {
      printf("Sending block address to tile: %s, address: %s\n", tilesz, addresssz);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_write_register(tile, addr, value);
    }
  } else if (strncmp(new_command, "ra_", 3) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- ra command (read address of BankRegister)
    //  Address is 32bit  in hex format  -> 0xAAAABBBB
    // ---------------------------------------------------------------------------------------------------------

    printf("Read address of RegBank\n");
    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;         // address
    char tilesz[12];      
    char addresssz[12];
    // do not copy initial 0x
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = '\0';
    printf("    tilesz: %s\n", tilesz);
    strcpy(addresssz, p2);
    printf("  address: %s\n", addresssz);

    int tile = atoi(tilesz);
    int addr = strtol(addresssz, NULL, 16);

    unsigned char address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
    unsigned char address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
    unsigned char address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
    unsigned char address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);

    printf(" Read address of bank register\n");
    printf("            tile : %d\n", tile);
    printf("  address_byte_3 : 0x%02x\n", address_byte_3);
    printf("  address_byte_2 : 0x%02x\n", address_byte_2);
    printf("  address_byte_1 : 0x%02x\n", address_byte_1);
    printf("  address_byte_0 : 0x%02x\n", address_byte_0);


    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  



    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      printf("Sending block address to tile: %s, address: %s\n", tilesz, addresssz);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_read_register(tile, addr, NULL);
    }
  } else if (strncmp(new_command, "rm_", 3) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- rm command (read memory address)
    //  Address is 32bit  in hex format  -> 0xAAAABBBB
    // ---------------------------------------------------------------------------------------------------------

    printf("Read memory address\n");
    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;         // address
    char tilesz[12];      
    char addresssz[12];
    char dst_buffer[64];
    // do not copy initial 0x
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = '\0';
    printf("    tilesz: %s\n", tilesz);
    strcpy(addresssz, p2);
    printf("  address: %s\n", addresssz);

    int tile = atoi(tilesz);
    int addr = strtol(addresssz, NULL, 16);

    unsigned char address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
    unsigned char address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
    unsigned char address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
    unsigned char address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);

    printf(" Read memory address\n");
    printf("            tile : %d\n", tile);
    printf("  address_byte_3 : 0x%02x\n", address_byte_3);
    printf("  address_byte_2 : 0x%02x\n", address_byte_2);
    printf("  address_byte_1 : 0x%02x\n", address_byte_1);
    printf("  address_byte_0 : 0x%02x\n", address_byte_0);


    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_BLOCK); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_BLOCK); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_BLOCK); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_BLOCK); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  



    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      printf("Sending block address to tile: %s, address: %s\n", tilesz, addresssz);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_read_memory_block(tile, addr, dst_buffer);
    }
  } else if (strncmp(new_command, "rmw_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- rmw command (read word memory address)
    //  Address is 32bit  in hex format  -> 0xAAAABBBB
    // ---------------------------------------------------------------------------------------------------------

    printf("Read word memory address\n");
    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;         // address
    char tilesz[12];      
    char addresssz[12];
    // do not copy initial 0x
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = '\0';
    printf("    tilesz: %s\n", tilesz);
    strcpy(addresssz, p2);
    printf("  address: %s\n", addresssz);

    int tile = atoi(tilesz);
    int __attribute__((unused)) addr = strtol(addresssz, NULL, 16);

    unsigned char address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
    unsigned char address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
    unsigned char address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
    unsigned char address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);

    printf(" Read word memory address\n");
    printf("            tile : %d\n", tile);
    printf("  address_byte_3 : 0x%02x\n", address_byte_3);
    printf("  address_byte_2 : 0x%02x\n", address_byte_2);
    printf("  address_byte_1 : 0x%02x\n", address_byte_1);
    printf("  address_byte_0 : 0x%02x\n", address_byte_0);


    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  



    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      printf("Sending Read word memory address to tile: %s, address: %s\n", tilesz, addresssz);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
        uint32_t unused;
        hn_read_memory_word(tile, addr, (char *) &unused);
    }
  } else if (strncmp(new_command, "wmw_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- wmw command (write word memory address)
    //  Address & value are 32bit  in hex format  -> 0xAAAABBBB
    // ---------------------------------------------------------------------------------------------------------

    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;         // address
    char *p3 = strstr(p2, "_")+1;         // value
    char tilesz[12];      
    char addresssz[12];
    char valuesz[12];
    // do not copy initial 0x
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = '\0';
    printf("    tilesz: %s\n", tilesz);
    strncpy(addresssz, p2, p3-p2-1);
    addresssz[p3-p2-1] = '\0';
    printf("  address: %s\n", addresssz);
    // do not copy initial 0x
    strcpy(valuesz, p3);
    printf("     value: %s \n", valuesz);

    int tile = atoi(tilesz);
    int __attribute__((unused)) addr = strtol(addresssz, NULL, 16);
    int __attribute__((unused)) value = strtol(valuesz, NULL, 16);

    unsigned char address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
    unsigned char address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
    unsigned char address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
    unsigned char address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);

    unsigned char value_byte_3 = weight_hex(valuesz[2]) * 16 + weight_hex(valuesz[3]);
    unsigned char value_byte_2 = weight_hex(valuesz[4]) * 16 + weight_hex(valuesz[5]);
    unsigned char value_byte_1 = weight_hex(valuesz[6]) * 16 + weight_hex(valuesz[7]);
    unsigned char value_byte_0 = weight_hex(valuesz[8]) * 16 + weight_hex(valuesz[9]);

    printf("Write word memory address\n");
    printf("            tile : %d\n", tile);
    printf("  address_byte_3 : 0x%02x\n", address_byte_3);
    printf("  address_byte_2 : 0x%02x\n", address_byte_2);
    printf("  address_byte_1 : 0x%02x\n", address_byte_1);
    printf("  address_byte_0 : 0x%02x\n", address_byte_0);
    printf("    value_byte_3 : 0x%02x\n", value_byte_3);
    printf("    value_byte_2 : 0x%02x\n", value_byte_2);
    printf("    value_byte_1 : 0x%02x\n", value_byte_1);
    printf("    value_byte_0 : 0x%02x\n", value_byte_0); 

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_WORD); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  



    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      printf("Sending WRITE_MEMORY_WORD address to tile: %s, address: %s, value: %s\n", tilesz, addresssz, valuesz);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
        hn_write_memory_word(tile, addr, value);
    }
  } else if (strncmp(new_command, "wmh_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- wmh command (write half memory address)
    //  Address & value are 32bit  in hex format  -> 0xAAAABBBB
    // ---------------------------------------------------------------------------------------------------------

    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;         // address
    char *p3 = strstr(p2, "_")+1;         // value
    char tilesz[12];      
    char addresssz[12];
    char valuesz[12];
    // do not copy initial 0x
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = '\0';
    printf("    tilesz: %s\n", tilesz);
    strncpy(addresssz, p2, p3-p2-1);
    addresssz[p3-p2-1] = '\0';
    printf("  address: %s\n", addresssz);
    // do not copy initial 0x
    strcpy(valuesz, p3);
    printf("     value: %s \n", valuesz);

    int tile = atoi(tilesz);
    int __attribute__((unused)) addr = strtol(addresssz, NULL, 16);
    int __attribute__((unused)) value = strtol(valuesz, NULL, 16);

    unsigned char address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
    unsigned char address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
    unsigned char address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
    unsigned char address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);

    unsigned char value_byte_3 = weight_hex(valuesz[2]) * 16 + weight_hex(valuesz[3]);
    unsigned char value_byte_2 = weight_hex(valuesz[4]) * 16 + weight_hex(valuesz[5]);
    unsigned char value_byte_1 = weight_hex(valuesz[6]) * 16 + weight_hex(valuesz[7]);
    unsigned char value_byte_0 = weight_hex(valuesz[8]) * 16 + weight_hex(valuesz[9]);

    printf("Write half memory address\n");
    printf("            tile : %d\n", tile);
    printf("  address_byte_3 : 0x%02x\n", address_byte_3);
    printf("  address_byte_2 : 0x%02x\n", address_byte_2);
    printf("  address_byte_1 : 0x%02x\n", address_byte_1);
    printf("  address_byte_0 : 0x%02x\n", address_byte_0);
    printf("    value_byte_3 : 0x%02x\n", value_byte_3);
    printf("    value_byte_2 : 0x%02x\n", value_byte_2);
    printf("    value_byte_1 : 0x%02x\n", value_byte_1);
    printf("    value_byte_0 : 0x%02x\n", value_byte_0); 

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_HALF); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  



    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      printf("Sending WRITE_MEMORY_HALF address to tile: %s, address: %s, value %s\n", tilesz, addresssz, valuesz);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
        hn_write_memory_halfword(tile, addr, value);
    }
  } else if (strncmp(new_command, "wmb_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- wmb command (write byte memory address)
    //  Address & value are 32bit  in hex format  -> 0xAAAABBBB
    // ---------------------------------------------------------------------------------------------------------

    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;         // address
    char *p3 = strstr(p2, "_")+1;         // value
    char tilesz[12];      
    char addresssz[12];
    char valuesz[12];
    // do not copy initial 0x
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = '\0';
    printf("    tilesz: %s\n", tilesz);
    strncpy(addresssz, p2, p3-p2-1);
    addresssz[p3-p2-1] = '\0';
    printf("  address: %s\n", addresssz);
    // do not copy initial 0x
    strcpy(valuesz, p3);
    printf("     value: %s \n", valuesz);

    int tile = atoi(tilesz);
    int __attribute__((unused)) addr = strtol(addresssz, NULL, 16);
    int __attribute__((unused)) value = strtol(valuesz, NULL, 16);

    unsigned char address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
    unsigned char address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
    unsigned char address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
    unsigned char address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);

    unsigned char value_byte_3 = weight_hex(valuesz[2]) * 16 + weight_hex(valuesz[3]);
    unsigned char value_byte_2 = weight_hex(valuesz[4]) * 16 + weight_hex(valuesz[5]);
    unsigned char value_byte_1 = weight_hex(valuesz[6]) * 16 + weight_hex(valuesz[7]);
    unsigned char value_byte_0 = weight_hex(valuesz[8]) * 16 + weight_hex(valuesz[9]);

    printf("Write byte memory address\n");
    printf("            tile : %d\n", tile);
    printf("  address_byte_3 : 0x%02x\n", address_byte_3);
    printf("  address_byte_2 : 0x%02x\n", address_byte_2);
    printf("  address_byte_1 : 0x%02x\n", address_byte_1);
    printf("  address_byte_0 : 0x%02x\n", address_byte_0);
    printf("    value_byte_3 : 0x%02x\n", value_byte_3);
    printf("    value_byte_2 : 0x%02x\n", value_byte_2);
    printf("    value_byte_1 : 0x%02x\n", value_byte_1);
    printf("    value_byte_0 : 0x%02x\n", value_byte_0); 

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = value_byte_3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BYTE); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  



    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      printf("Sending WRITE_MEMORY_BYTE address to tile: %s, address: %s, value %s\n", tilesz, addresssz, valuesz);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
        hn_write_memory_byte(tile, addr, value);
    }
  } else if (strncmp(new_command, "wm_", 3) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- wm command (WRITE MEMORY)
    // ---------------------------------------------------------------------------------------------------------
    //
    // Let's get the file with memory contents
    char fname[LINE_MAX_LENGTH];
    FILE *fd;
    //char fc;
    //int  findex = 0;
    char *fline = NULL;
    size_t flen = 0;
    ssize_t fread;
    char flinebuff[LINE_MAX_LENGTH];

    // lets get the tile and the file
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;  // begin of tileid in command
    char *p2 = strstr(p1, "_")+1; // begin of file in command     

    // tile
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = 0;
    int tile = atoi(tilesz);

    // Let's get the file with memory contents
    strcpy(fname, p2);

    if (use_hnlib == 0) {
    if( (fd = fopen(fname, "r")) == NULL) {
      printf("ERR: unable to open data file: \"%s\"\n", fname);
      //cont = 0;
    } else {
      //read file line by line, compose frame and send it to fpga
      //unsigned long int fpga_line_index;
      unsigned int fline_index;
      //char *pEnd;

      //fpga_line_index = 0;
      fline_index = 0;

      printf("sending write command(s) from file %s to tile %d...\n", fname, tile);

      while ((fread = getline(&fline, &flen, fd)) != -1) {
        ssize_t ss;
        ssize_t ss_new;

        //printf("Retrieved line of length %zu : \n", fread);
        //printf("    \"%s\"", fline);
        ss = 0;
        ss_new = 0;
        for(; (ss < fread) && (ss < (LINE_MAX_LENGTH - 1)); ss++) {

          if(   ((fline[ss] >= '0') && (fline[ss] <= '9')) 
              ||((fline[ss] >= 'a') && (fline[ss] <= 'f'))
              ||((fline[ss] >= 'A') && (fline[ss] <= 'F'))
              //||( fline[ss] == '@')
              ||( fline[ss] == 'x') ||( fline[ss] == 'X')
              ||( fline[ss] == ',')
            ) {
            flinebuff[ss_new] = fline[ss];
            ss_new++;
          } else if ((fline[ss] == 10) || (fline[ss] == 32)) {
          } else {
            printf("WAR: unexpected char (\"%c\") in line %u,%zu\n", fline[ss], fline_index, ss);
          }

        }


        if(ss_new > 0) {
          flinebuff[ss_new ] = 0;
          //printf("clean copy of line l.%lu: \"%s\"\n\n", fpga_line_index, flinebuff);
//          printf("linea l.%u (%ld bytes): %s\n",fline_index, strlen(flinebuff), flinebuff);
          
          // line format is 0x0000,0x...   "0x(4Bytes_Address),0x(64Bytes_Data)" -> total 73bytes
          if(    (ss_new == 141) && (flinebuff[10] == ',') 
              && ( flinebuff[0] == '0') && (( flinebuff[1] == 'x')  || ( flinebuff[1] == 'X'))
              && (flinebuff[11] == '0') && ((flinebuff[12] == 'x')  || (flinebuff[12] == 'X'))
            ) {
            //printf("ok. line is ok.\n%s", fline);
            int index = payload_0_index_first_byte;
            //int index_payload_size = index;

            sendbuf_0[index++] = 0x00;
            sendbuf_0[index++] = FRAME_LENGTH;
            
            sendbuf_0[index++] = (char_to_val(flinebuff[8]) * 16) + char_to_val(flinebuff[9]);
            sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
            sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 
//            printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);

            sendbuf_0[index++] = (char_to_val(flinebuff[6]) * 16) + char_to_val(flinebuff[7]);
            sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
            sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
//            printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);

            sendbuf_0[index++] = (char_to_val(flinebuff[4]) * 16) + char_to_val(flinebuff[5]);
            sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
            sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
//            printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);

            sendbuf_0[index++] = (char_to_val(flinebuff[2]) * 16) + char_to_val(flinebuff[3]);
            sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
            sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
//            printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);


              int ind; 
              //for(ind = 127; ind >= 0; ind = ind - 2)
              for (ind = 0; ind < 128; ind = ind + 2) {
                //sendbuf_0[index++] = (char_to_val(flinebuff[13 + ind - 1]) * 16) + char_to_val(flinebuff[13 + ind]);
                sendbuf_0[index++] = (char_to_val(flinebuff[13 + ind]) * 16) + char_to_val(flinebuff[13 + ind + 1]);
                sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
                sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
              }

              for(; index < tx_len_0; index++) {
                sendbuf_0[index] = 0xff;
              }

              if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
                printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");

            //ts.tv_sec = 1;
            //ts.tv_nsec = 250000000;
            nanosleep(&ts, NULL);
            //ts.tv_nsec = 1000000;

          } else {
            printf("ERROR: line not processed (%zu), unexpected line format:\n%s", ss_new, fline);
          }
        } else {
          //printf("processed line from(\"%s\") is empty.\n", fline);
        }
        fline_index++;
      }

      if (fline) { 
        free (fline);
      }

      fclose (fd);
    }
    } else {
      hn_write_image_into_memory(fname, tile, 0);
    }
  }else if (strncmp(new_command, "wmao_", 5) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- wmao command (WRITE MEMORY file content, adding address offset)
    // ---------------------------------------------------------------------------------------------------------
    //
    // Let's get the file with memory contents
    char     fname[LINE_MAX_LENGTH];
    FILE    *fd;
    char    *fline = NULL;
    size_t   flen = 0;
    ssize_t  read_size;
    char     flinebuff[LINE_MAX_LENGTH];
    int      tile = 0;

    char    *p0 = NULL; // mango tile
    char    *p1 = NULL; // address
    char    *p2 = NULL; // fname
    char     addresssz[12]; // 0xAABBCCDD
    char     tilesz[10];
    char     addr_off[8];
    char     addr_in_line[8];
    char     addr_with_offset[8];

    unsigned long int address_offset;  // offset value for address, set in command
    unsigned long int address_in_line; // address contained in current line of processed file
    unsigned long int address_new;     // new address to send to system


    p0 = strstr(new_command, "_");
    if (p0)
    {
      p0++;
      p1 = strstr(p0, "_");
      if (p1)
      {
        p1++;
        p2 = strstr(p1, "_");
        if(p2)
        {
          p2++;
        }
      }
    }

    if(p0 && p1 && p2) 
    {
      strncpy(tilesz, p0, p1-p0-1);
      tilesz[p1-p0-1] = 0;
      tile = atoi(tilesz);

      // Let's get the address offset
      strncpy(addresssz, p1, p2-p1-1);
      addresssz[11] = 0;
      // p1 + 2, to remove the 0x of the hex representation
      strncpy(addr_off, p1+2, 8);

      // Let's get the file with memory contents
      strcpy(fname, p2); //wmao_0xAABBCCDD_fname

// printf only for debugging
      char address_offset_byte_3;
      char address_offset_byte_2;
      char address_offset_byte_1;
      char address_offset_byte_0;
      address_offset_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
      address_offset_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
      address_offset_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
      address_offset_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);
      printf("Write memory file \"%s\" content with offset 0x%02x%02x%02x%02x in tile %d\n",
          fname,
          address_offset_byte_3,
          address_offset_byte_2,
          address_offset_byte_1,
          address_offset_byte_0,
          tile
          );
      //printf("  address_offset_byte_3 : 0x%02x\n", address_offset_byte_3);
      //printf("  address_offset_byte_2 : 0x%02x\n", address_offset_byte_2);
      //printf("  address_offset_byte_1 : 0x%02x\n", address_offset_byte_1);
      //printf("  address_offset_byte_0 : 0x%02x\n", address_offset_byte_0);
      //printf("   data filename is: %s\n", fname);
// end of printf for debugging

      
      address_offset = hex_value_to_ul(addr_off);
      //printf("   address offset: %lu \n", address_offset);

      if (use_hnlib == 0) {
	      if((addresssz[0] != '0')|| (addresssz[1] != 'x'))
	      {
		      printf("ERR: unexpected address offset: \"%s\",  exptected format is 0xAABBCCDD\n", addresssz);
	      }
	      else if( (fd = fopen(fname, "r")) == NULL)
	      {
		      printf("ERR: unable to open data file: \"%s\"\n", fname);
		      //cont = 0;
	      } else
	      {
		      //read file line by line, compose frame and send it to fpga
		      //unsigned long int fpga_line_index;
		      unsigned int fline_index;

		      //fpga_line_index = 0;
		      fline_index = 0;

		      printf("   file succesfully opened, sending content...\n");

		      while ((read_size = getline(&fline, &flen, fd)) != -1)
		      {
			      ssize_t ss;
			      ssize_t ss_new;

			      //printf("Retrieved line of length %zu : \n", read_size);
			      //printf("    \"%s\"", fline);
			      ss = 0;
			      ss_new = 0;
			      for(; (ss < read_size) && (ss < (LINE_MAX_LENGTH - 1)); ss++) 
			      {
				      if(   ((fline[ss] >= '0') && (fline[ss] <= '9')) 
						      ||((fline[ss] >= 'a') && (fline[ss] <= 'f'))
						      ||((fline[ss] >= 'A') && (fline[ss] <= 'F'))
						      //||( fline[ss] == '@')
						      ||( fline[ss] == 'x') ||( fline[ss] == 'X')
						      ||( fline[ss] == ',')
					) {
					      flinebuff[ss_new] = fline[ss];
					      ss_new++;
				      }
				      else if ((fline[ss] == 10) || (fline[ss] == 32))
				      {
				      }
				      else
				      {
					      printf("WAR: unexpected char (\"%c\") in line %u,%zu\n", fline[ss], fline_index, ss);
				      }
			      }


			      if(ss_new > 0) {
				      flinebuff[ss_new ] = 0;
				      //printf("clean copy of line l.%lu: \"%s\"\n\n", fpga_line_index, flinebuff);

				      // line format is 0x0000,0x...   "0x(4Bytes_Address),0x(64Bytes_Data)" -> total 73bytes
				      if(    (ss_new == 141) && (flinebuff[10] == ',') 
						      && ( flinebuff[0] == '0') && (( flinebuff[1] == 'x')  || ( flinebuff[1] == 'X'))
						      && (flinebuff[11] == '0') && ((flinebuff[12] == 'x')  || (flinebuff[12] == 'X'))
					) {
					      //printf("ok. line is ok.\n%s", fline);
					      int index = payload_0_index_first_byte;

					      strncpy(addr_in_line, &flinebuff[2], 8);
					      address_in_line = hex_value_to_ul(addr_in_line);
					      address_new = address_offset + address_in_line;
					      //printf("   address to convert to char array in hex is: %lu\n", address_new);

					      ul_value_to_32bit_hex_as_char_array(&addr_with_offset[0], address_new);

					      //char str_test[9];
					      //strncpy(str_test, &addr_with_offset[0], 8);
					      //str_test[8] = 0;
					      //printf("   address from file with offset is: %s\n", str_test);
					      //printf("\n\n");
					      //printf("under construction...\n");
					      //exit(-1);
					      //wmao_0x00112233_mem.data 

					      sendbuf_0[index++] = 0x00;
					      sendbuf_0[index++] = FRAME_LENGTH;

					      //sendbuf_0[index++] = (char_to_val(flinebuff[8]) * 16) + char_to_val(flinebuff[9]);
					      sendbuf_0[index++] = (char_to_val(addr_with_offset[6]) * 16) + char_to_val(addr_with_offset[7]);
					      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
					      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

					      //sendbuf_0[index++] = (char_to_val(flinebuff[6]) * 16) + char_to_val(flinebuff[7]);
					      sendbuf_0[index++] = (char_to_val(addr_with_offset[4]) * 16) + char_to_val(addr_with_offset[5]);
					      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
					      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

					      //sendbuf_0[index++] = (char_to_val(flinebuff[4]) * 16) + char_to_val(flinebuff[5]);
					      sendbuf_0[index++] = (char_to_val(addr_with_offset[2]) * 16) + char_to_val(addr_with_offset[3]);
					      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
					      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

					      //sendbuf_0[index++] = (char_to_val(flinebuff[2]) * 16) + char_to_val(flinebuff[3]);
					      sendbuf_0[index++] = (char_to_val(addr_with_offset[0]) * 16) + char_to_val(addr_with_offset[1]);
					      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
					      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

					      int ind; 
					      //for(ind = 127; ind >= 0; ind = ind - 2)
					      for (ind = 0; ind < 128; ind = ind + 2) {
						      //sendbuf_0[index++] = (char_to_val(flinebuff[13 + ind - 1]) * 16) + char_to_val(flinebuff[13 + ind]);
						      sendbuf_0[index++] = (char_to_val(flinebuff[13 + ind]) * 16) + char_to_val(flinebuff[13 + ind + 1]);
						      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_MEMORY_BLOCK); 
						      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
					      }

					      for(; index < tx_len_0; index++) {
						      sendbuf_0[index] = 0xff;
					      }

					      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
						      printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
					      //ts.tv_sec = 1;
					      //ts.tv_nsec = 250000000;
					      nanosleep(&ts, NULL);
					      //ts.tv_nsec = 1000000;

				      } else {
					      printf("ERROR: line not processed (%zu), unexpected line format:\n%s", ss_new, fline);
				      }
			      } else {
				      //printf("processed line from(\"%s\") is empty.\n", fline);
			      }

			      fline_index++;
		      }

		      if (fline) { 
			      free (fline);
		      }

		      fclose (fd);
	      }
      } else {
        hn_write_image_into_memory(fname, tile, address_offset);
      }
    }
    else
    {
      printf("wmao_ command wrong syntax\n");
    }

  }
  else if (strncmp(new_command, "tlb_", 4) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- tlb command (writes a tlb entry)
    // format: tlb_<tile>_<entry>_<va_ini>_<va_end>_<pa>_<mem|tr>_<tile_rsc>_<reg>
    //  Address is 32bit  in hex format  -> 0xAAAABBBB
    // ---------------------------------------------------------------------------------------------------------
    // lets get the parameters
    char *p1 = strstr(new_command, "_")+1; // tile
    char *p2 = strstr(p1, "_")+1;          // entry
    char *p3 = strstr(p2, "_")+1;          // va_ini
    char *p4 = strstr(p3, "_")+1;          // va_end
    char *p5 = strstr(p4, "_")+1;          // pa
    char *p6 = strstr(p5, "_")+1;          // mem|tr
    char *p7 = strstr(p6, "_")+1;          // tile_rsc
    char *p8 = strstr(p7, "_")+1;          // reg
    char sztile[12], szentry[12], szva_ini[12], szva_end[12], szpa[12], szmem_tr[12], sztile_rsc[12], szreg[12];

    strncpy(sztile, p1, p2-p1-1); sztile[p2-p1-1] = '\0';
    strncpy(szentry, p2, p3-p2-1); szentry[p3-p2-1] = '\0';
    strncpy(szva_ini, p3, p4-p3-1); szva_ini[p4-p3-1] = '\0';
    strncpy(szva_end, p4, p5-p4-1); szva_end[p5-p4-1] = '\0';
    strncpy(szpa, p5, p6-p5-1); szpa[p6-p5-1] = '\0';
    strncpy(szmem_tr, p6, p7-p6-1); szmem_tr[p7-p6-1] = '\0';
    strncpy(sztile_rsc, p7, p8-p7-1); sztile_rsc[p8-p7-1] = '\0';
    strcpy(szreg, p8);

    int tile = atoi(sztile);
    int entry = atoi(szentry);
    unsigned int va_ini =  weight_hex(szva_ini[9]) + 
                          (weight_hex(szva_ini[8]) << 4) + 
                          (weight_hex(szva_ini[7]) << 8) + 
                          (weight_hex(szva_ini[6]) << 12) + 
                          (weight_hex(szva_ini[5]) << 16) +
                          (weight_hex(szva_ini[4]) << 20) +
                          (weight_hex(szva_ini[3]) << 24) +
                          (weight_hex(szva_ini[2]) << 28); 
    unsigned int va_end =  weight_hex(szva_end[9]) + 
                          (weight_hex(szva_end[8]) << 4) + 
                          (weight_hex(szva_end[7]) << 8) + 
                          (weight_hex(szva_end[6]) << 12) + 
                          (weight_hex(szva_end[5]) << 16) +
                          (weight_hex(szva_end[4]) << 20) +
                          (weight_hex(szva_end[3]) << 24) +
                          (weight_hex(szva_end[2]) << 28); 
    unsigned int pa     =  weight_hex(szpa[9]) + 
                          (weight_hex(szpa[8]) << 4) + 
                          (weight_hex(szpa[7]) << 8) + 
                          (weight_hex(szpa[6]) << 12) + 
                          (weight_hex(szpa[5]) << 16) +
                          (weight_hex(szpa[4]) << 20) +
                          (weight_hex(szpa[3]) << 24) +
                          (weight_hex(szpa[2]) << 28); 
    int to_mem;
    int to_tr;
    int tile_rsc = atoi(sztile_rsc);
    int reg = atoi(szreg);
    if (strcmp(szmem_tr, "mem") == 0) {
      to_mem = 1;
      to_tr  = 0;
    } else {
      to_mem = 0;
      to_tr  = 1; 
    }

    printf("   INFO: setting tlb: tile: %d, entry: %d, va_ini: %08x, va_end: %08x, pa: %08x, mem %s, tile_rsc: %d, reg: %d\n", tile, entry, va_ini, va_end, pa, to_mem?" yes ":" no (using tr) ", tile_rsc, reg);

    // First word: entry
    unsigned char w0_b0 = entry;
    unsigned char w0_b1 = 0;
    unsigned char w0_b2 = 0;
    unsigned char w0_b3 = 0;

    // Second word: va_ini
    unsigned char w1_b0 = va_ini & 0x000000FF;
    unsigned char w1_b1 = (va_ini & 0x0000FF00) >> 8;
    unsigned char w1_b2 = (va_ini & 0x00FF0000) >> 16;
    unsigned char w1_b3 = (va_ini & 0xFF000000) >> 24;

    // third word: va_end
    unsigned char w2_b0 = va_end & 0x000000FF;
    unsigned char w2_b1 = (va_end & 0x0000FF00) >> 8;
    unsigned char w2_b2 = (va_end & 0x00FF0000) >> 16;
    unsigned char w2_b3 = (va_end & 0xFF000000) >> 24;

    // Fourth word: pa
    unsigned char w3_b0 = pa & 0x000000FF;
    unsigned char w3_b1 = (pa & 0x0000FF00) >> 8;
    unsigned char w3_b2 = (pa & 0x00FF0000) >> 16;
    unsigned char w3_b3 = (pa & 0xFF000000) >> 24;

    // Fitth word: mem_tr (tomem: bit 0, totilereg: bit 1), reg (bits 7..2), tile_rsc (16..8)
    unsigned w4 = (to_mem & 0x1) | ((to_tr & 0x1) << 1) | ((reg & 0x3F) << 2) | ((tile_rsc & 0x1FF) << 8);
    unsigned char w4_b0 = w4 & 0x000000FF;
    unsigned char w4_b1 = (w4 & 0x0000FF00) >> 8;
    unsigned char w4_b2 = (w4 & 0x00FF0000) >> 16;
    unsigned char w4_b3 = (w4 & 0xFF000000) >> 24;

    int index = payload_0_index_first_byte;

    //printf("   tlb w0 [lsB:msB]: 0x %02x %02x %02x %02x\n", w0_b0,w0_b1,w0_b2,w0_b3);
    //printf("   tlb w1 [lsB:msB]: 0x %02x %02x %02x %02x\n", w1_b0,w1_b1,w1_b2,w1_b3);
    //printf("   tlb w2 [lsB:msB]: 0x %02x %02x %02x %02x\n", w2_b0,w2_b1,w2_b2,w2_b3);
    //printf("   tlb w3 [lsB:msB]: 0x %02x %02x %02x %02x\n", w3_b0,w3_b1,w3_b2,w3_b3);
    //printf("   tlb w4 [lsB:msB]: 0x %02x %02x %02x %02x\n", w4_b0,w4_b1,w4_b2,w4_b3);

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = (MANGO_TLB_REG & 0x00FF);
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0xFF00) >> 8;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w0_b0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

    sendbuf_0[index++] = w0_b1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w0_b2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w0_b3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0x00FF);
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0xFF00) >> 8;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w1_b0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

    sendbuf_0[index++] = w1_b1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w1_b2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w1_b3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0x00FF);
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0xFF00) >> 8;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w2_b0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

    sendbuf_0[index++] = w2_b1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w2_b2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w2_b3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0x00FF);
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0xFF00) >> 8;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w3_b0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

    sendbuf_0[index++] = w3_b1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w3_b2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w3_b3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0x00FF);
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = (MANGO_TLB_REG & 0xFF00) >> 8;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w4_b0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

    sendbuf_0[index++] = w4_b1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w4_b2;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = w4_b3;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    //printf("  payload_index_first_byte: %d,  FRAME_LENGTH: %d,  index: %d  frame necessary length 12 + 2 + 90 = 104\n",  payload_0_index_first_byte, FRAME_LENGTH, index);

    //debug, print items content. avoid print frame length bytes
    //printf("   print item bytes in same order they will be sent to fpga\n");
    //for (int c_ind = payload_0_index_first_byte+2; c_ind < index; c_ind = c_ind + 3)
    //{
    //  if( ((c_ind - (payload_0_index_first_byte+2)) % 18) == 0) printf("\n");
    //  printf("   %02x %02x %02x\n", sendbuf_0[c_ind], sendbuf_0[c_ind+1], sendbuf_0[c_ind+2]);
    //}
    //printf("\n");
    //printf("   print items [MSB:LSB]\n");
    //for (int c_ind = payload_0_index_first_byte+2; c_ind < index; c_ind = c_ind + 3)
    //{
    //  //if( ((c_ind - (payload_0_index_first_byte+2)) % 18) == 0) printf("\n");
    //  printf("   %02x %02x %02x\n", sendbuf_0[c_ind+2], sendbuf_0[c_ind+1], sendbuf_0[c_ind]);
    //}
    
    if (use_hnlib == 0) {
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_set_tlb(tile, entry, va_ini, va_end, pa, to_mem, to_tr, reg, tile_rsc);
    }
  } else if (strncmp(new_command, "wcp_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- wcp command (WRITE COHERENCE PROTOCOL)
    // ---------------------------------------------------------------------------------------------------------
    //
    // Let's get the file with protocol contents
    char    fname[LINE_MAX_LENGTH];
    FILE   *fd;
    char   *fline = NULL;
    char    fline2[LINE_MAX_LENGTH];
    size_t  flen = 0;
    ssize_t read_size;
    char    tilesz[10];
    int     tile = 0; 
    int     unit = 0; // currently it goes to unit 0 (processor 0 of PEAK architecture inside the MANGO tile)
    char   *p1;
    char   *p2;

    // lets get the pointers to tile and filename fields
    p1 = strstr(new_command, "_");  // begin of tileid in command
    p2 = strstr(p1 + 1, "_");           // begin of file in command

    if(p1 && p2) {
      p1++;
      p2++;

      // get tile number from command
      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = 0;
      tile = atoi(tilesz);
      // Let's get the file with memory contents
      strcpy(fname, p2);

      //printf("DBG: Send ");
      if (use_hnlib == 0) {
        if( (fd = fopen(fname, "r")) == NULL) {
          printf("ERR: unable to open protocol data file: \"%s\"\n", fname);
          //cont = 0;
        } else {
          //read file line by line, compose frame and send it to fpga
          //unsigned int fline_index;

          //fline_index = 0;

          printf("INFO: sending coherence protocol file \"%s\" to MANGO tile #%d  PEAK tile #%d \n", fname, tile, unit);

          int items_sent = 0;
          int fline_index = 0;
          
          while ((read_size = getline(&fline, &flen, fd)) != -1) {
            // The data is in binary format, we read groups of 8 bits and build an item
            while ((fline[0]=='0') || (fline[0]=='1')) {
              char item_sz[10];
              int item_dec;
              strncpy(item_sz, fline, 8);
              item_sz[8] = 0;
              item_dec = bin2dec(item_sz); 

//              printf("linea l.%u (%ld bytes): %s\n",fline_index, strlen(fline), fline);
              fline_index++;

              //printf("fline %s, item_sz %s, item_dec %d\n", fline, item_sz, item_dec);
              //printf("%02x", item_dec);
              //struct timespec ts2;
              //  ts2.tv_sec = 0;
              //  ts2.tv_nsec = 10000000;
              nanosleep(&ts, NULL);

              //sendbuf_0[index++] = 0x00;
              //sendbuf_0[index++] = FRAME_LENGTH;
              //sendbuf_0[index++] = (u_int8_t)item_dec;
              //sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_PROTOCOL); 
              //sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
              //sendbuf_0[index++] = 0x00;

              //a PEAK item is split in 6 MANGO items, compose item by item 
              // command is sent to Tile 0, 
              int       index;
              u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
              u_int8_t  wr_reg_for_peak;

              index        = payload_0_index_first_byte;
              peak_item_b3 = 0x00;
              peak_item_b2 = THIRD_ITEM_BYTE(unit);
              peak_item_b1 = SECOND_ITEM_BYTE(unit, COMMAND_PEAK_WRITE_PROTOCOL);
              peak_item_b0 = (u_int8_t)item_dec;
              wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

              //printf("sending protocol byte: %2x with PEAKitem: %2x %2x %2x %2x\n", item_dec, peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0);

              // Compose MANGO items
              sendbuf_0[index++] = 0x00;
              sendbuf_0[index++] = FRAME_LENGTH;

              // 
              sendbuf_0[index++] = wr_reg_for_peak;
              sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
              sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
//              printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);
              //sendbuf_0[index++] = 0x00;
              sendbuf_0[index++] = 0x00;
              sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
              sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
//              printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);
              //sendbuf_0[index++] = 0x00;
              //bytes of PEAK item
              sendbuf_0[index++] = peak_item_b0;
              sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
              sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
//              printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);
              //sendbuf_0[index++] = 0x00;
              sendbuf_0[index++] = peak_item_b1;
              sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
              sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
//              printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);
              //sendbuf_0[index++] = 0x00;
              sendbuf_0[index++] = peak_item_b2;
              sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
              sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
//              printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);
              //sendbuf_0[index++] = 0x00;
              sendbuf_0[index++] = peak_item_b3;
              sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
              sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
//              printf("ITEM 00%02x%02x%02x\n", (unsigned char)sendbuf_0[index-1], (unsigned char)sendbuf_0[index-2], (unsigned char)sendbuf_0[index-3]);
              //sendbuf_0[index++] = 0x00;

              for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

              if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
                printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");

              items_sent++;
              strcpy(fline2, &fline[8]);
              strcpy(fline, fline2);

              //struct timespec ts_wcp;
              //ts_wcp.tv_sec = 0;
              //ts_wcp.tv_nsec = 300000000;
              //nanosleep(&ts_wcp, NULL);

            }
            //printf("\n");
          }
          printf("Sent %d items\n", items_sent);
          if (fline) free (fline);
          fclose (fd);
    
         }      
      } else {
        hn_peak_write_protocol(fname, tile);
      }
    } 
    else
    {
      printf("wcp_ command wrong syntax\n");
    }
  }
  else if (strncmp(new_command, "npc_", 4) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- npc command (NEW PC ADDRESS)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the address and tile
    char  tilesz[12];
    char  unitsz[12];
    char  addresssz[12];
    char *p1 = NULL;
    char *p2 = NULL;
    char *p3 = NULL;
    int   tile;
    int   unit;
    char  address_byte[4];
    int   index;

    p1   = strstr(new_command, "_");
    p2   = strstr(p1 + 1, "_");
    p3   = strstr(p2 + 1, "_");

    if(p1 && p2 && p3) 
    {
      //char tilesz[12];      
      ///har addresssz[12];
      //char valuesz[12];
      
      p1++;
      p2++;
      p3++;  // increase pointers to avoid "_"

      strncpy(tilesz, p1, p2-p1-1);
      tilesz[p2-p1-1] = '\0';
      //printf("      tile: %s\n", tilesz);
      strncpy(unitsz, p2, p3-p2-1);
      unitsz[p3-p2-1] = '\0';
      //printf(" peak_unit: %s\n", unitsz);
      
      if(( *p3 == '0') && (*(p3+1) == 'x' ))
      {
        // do not copy initial 0x
        strcpy(addresssz, p3+2);
        //printf("   address: %s \n", addresssz);

        tile = atoi (tilesz);
        unit = atoi (unitsz);
        int addr = strtol(addresssz, NULL, 16);

        address_byte[3] = weight_hex(addresssz[0]) * 16 + weight_hex(addresssz[1]);
        address_byte[2] = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
        address_byte[1] = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
        address_byte[0] = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);

        printf("Setting new pc address B3:%02x B2:%02x B1:%02x B0:%02x\n", address_byte[3],address_byte[2],address_byte[1],address_byte[0]);
        /*
           index = payload_0_index_first_byte;
        // set new pc address in a peak tile consists of sending 4 PEAK items,
        // each PEAK item needs of four MANGO ITEMS,
        // 6*4 = 24 items of 4 bytes -->> total of 96 bytes

        sendbuf_0[index++] = 0x00;
        sendbuf_0[index++] = FRAME_LENGTH;

        sendbuf_0[index++] = address_byte_0;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_SET_PROCESSOR_ADDRESS); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = address_byte_1;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_SET_PROCESSOR_ADDRESS); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = address_byte_2;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_SET_PROCESSOR_ADDRESS); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = address_byte_3;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_SET_PROCESSOR_ADDRESS); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
         */

        printf("Sending NEW PROCESSOR ADDRESS frame to  MANGO tile: %d  PEAK tile: %d  with address: %s\n", tile, unit, addresssz);

        int addr_index;
        for (addr_index = 0; addr_index < 4; addr_index++)
        {
          u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
          u_int8_t  wr_reg_for_peak; 

          //printf("Sending Byte address_byte[%2d]: %02x\n", addr_index, address_byte[addr_index]);
          index        = payload_0_index_first_byte;
          peak_item_b3 = 0x00;
          peak_item_b2 = THIRD_ITEM_BYTE(unit);
          peak_item_b1 = SECOND_ITEM_BYTE(unit, COMMAND_PEAK_SET_PROCESSOR_ADDRESS);
          peak_item_b0 = (u_int8_t)address_byte[addr_index];
          wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

          sendbuf_0[index++] = 0x00;
          sendbuf_0[index++] = FRAME_LENGTH;

          // 
          sendbuf_0[index++] = wr_reg_for_peak;
          //sendbuf_0[index++] = 0xaa;
          sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
          sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
          //sendbuf_0[index++] = 0x00;
          sendbuf_0[index++] = 0x00;
          sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
          sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
          //sendbuf_0[index++] = 0x00;
          //bytes of PEAK item
          sendbuf_0[index++] = peak_item_b0;
          sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
          sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
          //sendbuf_0[index++] = 0x00;
          sendbuf_0[index++] = peak_item_b1;
          sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
          sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
          //sendbuf_0[index++] = 0x00;
          sendbuf_0[index++] = peak_item_b2;
          sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
          sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
          //sendbuf_0[index++] = 0x00;
          sendbuf_0[index++] = peak_item_b3;
          sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
          sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
          //sendbuf_0[index++] = 0x00;


          //for (int j = payload_0_index_first_byte + 2; j < index; j = j+3)
          //{
          //     printf("sent item [MSB:LSB]: %02x %02x %02x\n", (uint8_t)sendbuf_0[j+2], (uint8_t)sendbuf_0[j+1], (uint8_t)sendbuf_0[j+0]);
          //}

          if (use_hnlib == 0) {
            for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

            if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
              printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
          } 
        }

        if (use_hnlib == 1) {
          hn_peak_set_new_pc(tile, unit, addr);
        }
      }
      else
      {
        printf("npc_ command wrong syntax, unexpected address format, expected 0xAABBCCDD\n");
      }
    } else {
      printf("npc_ command wrong syntax\n");
    }
  }
  else if (strncmp(new_command, "se_", 3) == 0)
  {
    // ---------------------------------------------------------------------------------------------------------
    // -- se command (STATS ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_STATS_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
    printf("Sending STATS ENABLE frame to tile %d\n", tile);
    if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
      printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
  } else if (strncmp(new_command, "sd_", 3) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- sd command (STATS DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_STATS_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    printf("Sending STATS DISABLE frame to tile %d\n", tile);
    if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
      printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
  } else if (strncmp(new_command, "reset_nstats", 12) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- reset_nstats command (RESET NET STATS)
    // ---------------------------------------------------------------------------------------------------------
    
    int tile = 0;
    char address_stats[12] = MANGO_NET_STATS_REG;
    char valuesz[12] = "00000000";

    //unsigned char address_byte_3;
    //unsigned char address_byte_2;
    unsigned char address_byte_1;
    unsigned char address_byte_0;
    unsigned char value_byte_3;
    unsigned char value_byte_2;
    unsigned char value_byte_1;
    unsigned char value_byte_0;

    //address_byte_3 = weight_hex(address_stats[2]) * 16 + weight_hex(address_stats[3]);
    //address_byte_2 = weight_hex(address_stats[4]) * 16 + weight_hex(address_stats[5]);
    address_byte_1 = weight_hex(address_stats[6]) * 16 + weight_hex(address_stats[7]);
    address_byte_0 = weight_hex(address_stats[8]) * 16 + weight_hex(address_stats[9]);
  
    value_byte_3 = weight_hex(valuesz[2]) * 16 + weight_hex(valuesz[3]);
    value_byte_2 = weight_hex(valuesz[4]) * 16 + weight_hex(valuesz[5]);
    value_byte_1 = weight_hex(valuesz[6]) * 16 + weight_hex(valuesz[7]);
    value_byte_0 = weight_hex(valuesz[8]) * 16 + weight_hex(valuesz[9]);

    for(; tile < NUMBER_OF_SWITCHES; tile++){
      int index = payload_0_index_first_byte;

      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      sendbuf_0[index++] = address_byte_0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = address_byte_1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = value_byte_0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

      sendbuf_0[index++] = value_byte_1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = value_byte_2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = value_byte_3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;      
      //printf("Sending RESET NET STATS to tile: %d, address: %s\n", tile, address_stats);
      
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
  
      //ts.tv_sec = 1;
      ts.tv_nsec = 1000;
      nanosleep(&ts, NULL);
   }

    int index = payload_0_index_first_byte;
    tile = 0;

    char addresssz1[12] = MANGO_TIMESTAMP_REG;

    //address_byte_3 = weight_hex(addresssz1[2]) * 16 + weight_hex(addresssz1[3]);
    //address_byte_2 = weight_hex(addresssz1[4]) * 16 + weight_hex(addresssz1[5]);
    address_byte_1 = weight_hex(addresssz1[6]) * 16 + weight_hex(addresssz1[7]);
    address_byte_0 = weight_hex(addresssz1[8]) * 16 + weight_hex(addresssz1[9]);

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    printf("Sending READING TIMESTAMP to tile: %d, address: %s\n", tile, addresssz1);
    if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
  } else if (strncmp(new_command, "weights_", 8) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- weights command (VN WEIGHTS VECTOR)
    //  Data    is 32 bit in hex format  -> 0xDATADATA
    // ---------------------------------------------------------------------------------------------------------
    
    int tile = 0;
    char addresssz[12] = MANGO_WEIGHTS_VECTOR_REG;

    char *p1 = strstr(new_command, "_")+1; // arg1
    char *p2 = strstr(p1, "_")+1;         // arg2
    char *p3 = strstr(p2, "_")+1;         // arg3
    char arg1z[12];      
    char arg2z[12];
    char arg3z[12];

    strncpy(arg1z, p1, p2-p1-1);
    arg1z[p2-p1-1] = '\0';
    int arg1 = atoi(arg1z);

    strncpy(arg2z, p2, p3-p2-1);
    arg2z[p3-p2-1] = '\0';
    int arg2 = atoi(arg2z);

    strcpy(arg3z, p3);
    int arg3 = atoi(arg3z);

    if ((arg1 + arg2 + arg3) != 100) {
      printf("ERROR: Total percentage must be 100%%\n");
    } else{
      printf("Setting new weights vector VN2=%d%% VN1=%d%% VN0=%d%%\n",arg1, arg2, arg3);
      int vn2_weight = arg1/10;
      int vn1_weight = arg2/10;
      int vn0_weight = arg3/10;

      unsigned int i, max, offset, value;
      value = 0;
      //First bucle to fill weights vector even positions (2 bits each).
      for (i=0; i<5; i++) {
          max = (vn2_weight < vn1_weight) ? (vn1_weight < vn0_weight ? 0 : 1) : (vn2_weight < vn0_weight ? 0 : 2);
          offset = i*4;
          value = (value | ((max & ONE_WEIGHT_MASK) << offset));
          if (max == 0) vn0_weight--;
          else if (max == 1) vn1_weight--;
          else vn2_weight--;
        }
      // ---------------------------------------------------------------------------------------------------------
      //Second bucle to fill weights vector odd positions (2 bits each).
      for (i=0; i<5; i++) {
          max = (vn2_weight < vn1_weight) ? (vn1_weight < vn0_weight ? 0 : 1) : (vn2_weight < vn0_weight ? 0 : 2);
          value = (value | ((max & ONE_WEIGHT_MASK) << ((i*4) + 2)));
          if (max == 0) vn0_weight--;
          else if (max == 1) vn1_weight--;
          else vn2_weight--;
        }
      // ---------------------------------------------------------------------------------------------------------   
      //unsigned char address_byte_3;
      //unsigned char address_byte_2;
      unsigned char address_byte_1;
      unsigned char address_byte_0;
      unsigned char value_byte_3;
      unsigned char value_byte_2;
      unsigned char value_byte_1;
      unsigned char value_byte_0;

      //address_byte_3 = weight_hex(addresssz[2]) * 16 + weight_hex(addresssz[3]);
      //address_byte_2 = weight_hex(addresssz[4]) * 16 + weight_hex(addresssz[5]);
      address_byte_1 = weight_hex(addresssz[6]) * 16 + weight_hex(addresssz[7]);
      address_byte_0 = weight_hex(addresssz[8]) * 16 + weight_hex(addresssz[9]);
    
      //to convert int value to hex
      value_byte_3 = (value >> 24) & 0x000000FF;
      value_byte_2 = (value >> 16) & 0x000000FF;
      value_byte_1 = (value >> 8)  & 0x000000FF;
      value_byte_0 = value         & 0x000000FF;
      // ---------------------------------------

      for(; tile < NUMBER_OF_SWITCHES; tile++){
        int index = payload_0_index_first_byte;

        sendbuf_0[index++] = 0x00;
        sendbuf_0[index++] = FRAME_LENGTH;

        sendbuf_0[index++] = address_byte_0;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = address_byte_1;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = value_byte_0;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

        sendbuf_0[index++] = value_byte_1;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = value_byte_2;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = value_byte_3;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;      
        //printf("Sending NEW WEIGHTS VECTOR to tile: %d, address: %s\n", tile, addresssz);
        
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
        //ts.tv_sec = 1;
        ts.tv_nsec = 1000;
        nanosleep(&ts, NULL); 
      }
    }//end else
  } else if (strncmp(new_command, "swinject_", 9) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- swinject command (SWITCH INJECT (MS "MESSAGE SYSTEM"))
    //  Data    is 32 bit in hex format  -> 0xDATADATA
    // ---------------------------------------------------------------------------------------------------------
    
    // ******************************************************************
    
    char *p1 = NULL;
    char *p2 = NULL;
    char *p3 = NULL;
    char *p4 = NULL;
    char *p5 = NULL;
    char *p6 = NULL;
    char arg1z[12];      
    char arg2z[12];
    char arg3z[12];
    char arg4z[12];      
    char arg5z[12];
    char arg6z[12];
    int tile;
    int mode;
    int message_size;
    int inject_rate;
    int vn;
    int dest_tile;

    if ( (p1 = strstr(new_command, "_")) )
    if ( (p2 = strstr(p1+1, "_")) )
    if ( (p3 = strstr(p2+1, "_")) )
    if ( (p4 = strstr(p3+1, "_")) )
    if ( (p5 = strstr(p4+1, "_")) )
    if ( (p6 = strstr(p5+1, "_")) ) ;

    if ( p1 && p2 && p3 && p4 && p5 && p6 ) {

      p1++; p2++; p3++; p4++; p5++; p6++; 

      strncpy(arg1z, p1, p2-p1);
      strncpy(arg2z, p2, p3-p2-1);
      strncpy(arg3z, p3, p4-p3-1);
      strncpy(arg4z, p4, p5-p4-1);
      strncpy(arg5z, p5, p6-p5-1);

    } else {
      printf("wrong syntax\n"); 
      return cont;
    }

    arg1z[p2-p1+1] = '\0';
    tile = atoi(arg1z);
    arg2z[p3-p2-1] = '\0';
    mode = atoi(arg2z);
    arg3z[p4-p3-1] = '\0';
    message_size = atoi(arg3z);
    arg4z[p5-p4-1] = '\0';
    inject_rate = atoi(arg4z);
    arg5z[p6-p5-1] = '\0';
    vn = atoi(arg5z);
    strcpy(arg6z, p6);
    dest_tile = atoi(arg6z);


    //-------------------------------------------------------------------
    int value = 0;
    //unsigned char address_byte_3;
    //unsigned char address_byte_2;
    unsigned char address_byte_1;
    unsigned char address_byte_0;
    unsigned char value_byte_3;
    unsigned char value_byte_2;
    unsigned char value_byte_1;
    unsigned char value_byte_0;
    int index = payload_0_index_first_byte;      

    if ((mode < 0) || (mode > 3)) {
      printf("ERROR: arg2 must be [0..3] (0 to inject at rnd destiny and vn, 1 to inject at rnd destiny, 2 to inject at random vn, 3 to select both destiny and vn.\nUsage: switch_inject <tile> <mode> <message_size> <inject_rate> <vn> <dest_tile>)\n\n");
    } else{

      //Prepare to write in destiny register
      if ((mode == 2) || (mode == 3)) {

        char addresss_dst[12] = MANGO_MS_INJECT_DST_REG;

       //Building final hex value in int variable
        value = (dest_tile & DEST_RDWR_MS_MASK);
        // -------------------------------------------------     
        //to convert addresss_dst to hex
        //address_byte_3 = weight_hex(addresss_dst[2]) * 16 + weight_hex(addresss_dst[3]);
        //address_byte_2 = weight_hex(addresss_dst[4]) * 16 + weight_hex(addresss_dst[5]);
        address_byte_1 = weight_hex(addresss_dst[6]) * 16 + weight_hex(addresss_dst[7]);
        address_byte_0 = weight_hex(addresss_dst[8]) * 16 + weight_hex(addresss_dst[9]);
        // ----------------------------------------------------------------------------
        //to convert int value to hex
        value_byte_3 = (value >> 24) & 0x000000FF;
        value_byte_2 = (value >> 16) & 0x000000FF;
        value_byte_1 = (value >> 8)  & 0x000000FF;
        value_byte_0 = value         & 0x000000FF;
        // ---------------------------------------

        sendbuf_0[index++] = 0x00;
        sendbuf_0[index++] = FRAME_LENGTH;

        sendbuf_0[index++] = address_byte_0;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = address_byte_1;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = value_byte_0;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

        sendbuf_0[index++] = value_byte_1;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = value_byte_2;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = value_byte_3;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        //Overwrite the remaining slots of the buffer
        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;  

        //Send the buffer to write at destiny register
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");

      }//end destiny write for modes 2 and 3

      //Prepare to write in inject_oth register
      char addresss_oth[12] = MANGO_MS_INJECT_OTH_REG;

      //Building final hex value in int variable
      value = 0;
      value = (SET_RDWR_MS_MASK << SET_RDWR_MS_OFFSET) |
      ((message_size   & SIZE_RDWR_MS_MASK) << SIZE_RDWR_MS_OFFSET) |
      ((mode   & TYPE_RDWR_MS_MASK) << TYPE_RDWR_MS_OFFSET) |
      ((vn & VN_RDWR_MS_MASK) << VN_RDWR_MS_OFFSET) |
      (inject_rate  & RATE_RDWR_MS_MASK);
      // -----------------------------------------------------------

      //to convert addresss_oth to hex
      //address_byte_3 = weight_hex(addresss_oth[2]) * 16 + weight_hex(addresss_oth[3]);
      //address_byte_2 = weight_hex(addresss_oth[4]) * 16 + weight_hex(addresss_oth[5]);
      address_byte_1 = weight_hex(addresss_oth[6]) * 16 + weight_hex(addresss_oth[7]);
      address_byte_0 = weight_hex(addresss_oth[8]) * 16 + weight_hex(addresss_oth[9]);
      // ----------------------------------------------------------------------------
      //to convert int value to hex
      value_byte_3 = (value >> 24) & 0x000000FF;
      value_byte_2 = (value >> 16) & 0x000000FF;
      value_byte_1 = (value >> 8)  & 0x000000FF;
      value_byte_0 = value         & 0x000000FF;
      // ---------------------------------------

      index = payload_0_index_first_byte;

      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;

      sendbuf_0[index++] = address_byte_0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = address_byte_1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = value_byte_0;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile); 

      sendbuf_0[index++] = value_byte_1;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = value_byte_2;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      sendbuf_0[index++] = value_byte_3;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

      //Overwrite the remaining slots of the buffer
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;   

      //Print the function that will be carried out
      if (mode == 0) {    
        printf("Switching (on/off) on tile=%d, mode=%d, message_size=%d, rate=%d, vn=RND, dest_tile=RND\n", tile,mode,message_size,inject_rate);
      }else if (mode == 1) {    
        printf("Switching (on/off) on tile=%d, mode=%d, message_size=%d, rate=%d, vn=%d, dest_tile=RND\n", tile,mode,message_size,inject_rate,vn);
      }else if (mode == 2) {
        printf("Switching (on/off) on tile=%d, mode=%d, message_size=%d, rate=%d, vn=RND, dest_tile=%d\n", tile,mode,message_size,inject_rate,dest_tile);
      } else { //mode == 3
        printf("Switching (on/off) on tile=%d, mode=%d, message_size=%d, rate=%d, vn=%d, dest_tile=%d\n", tile,mode,message_size,inject_rate,vn,dest_tile);
      }  

      //Send the buffer to write at inject_oth register
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
      printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");

    }//end else
  } else if (strncmp(new_command, "nshow", 5) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- nshow  command (SHOW NET STATS)
    // ---------------------------------------------------------------------------------------------------------

    int index = payload_0_index_first_byte;
    int tile = 0;

    char addresss_time[12] = MANGO_TIMESTAMP_REG;

    //unsigned char address_byte_3 = weight_hex(addresss_time[2]) * 16 + weight_hex(addresss_time[3]);
    //unsigned char address_byte_2 = weight_hex(addresss_time[4]) * 16 + weight_hex(addresss_time[5]);
    unsigned char address_byte_1 = weight_hex(addresss_time[6]) * 16 + weight_hex(addresss_time[7]);
    unsigned char address_byte_0 = weight_hex(addresss_time[8]) * 16 + weight_hex(addresss_time[9]);

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;

    sendbuf_0[index++] = address_byte_0;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    sendbuf_0[index++] = address_byte_1;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    printf("Sending READING TIMESTAMP to tile: %d, address: %s\n", tile, addresss_time);
    if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    //------------------------------------------------------------------------------------------------------
    tile = 0;
    char address_stats[12] = MANGO_NET_STATS_REG;

    //address_byte_3 = weight_hex(address_stats[2]) * 16 + weight_hex(address_stats[3]);
    //address_byte_2 = weight_hex(address_stats[4]) * 16 + weight_hex(address_stats[5]);
    address_byte_1 = weight_hex(address_stats[6]) * 16 + weight_hex(address_stats[7]);
    address_byte_0 = weight_hex(address_stats[8]) * 16 + weight_hex(address_stats[9]);

    for(; tile < NUMBER_OF_SWITCHES; tile++){
      int i;
      for(i=0; i<15;i++){
        int index = payload_0_index_first_byte;

        sendbuf_0[index++] = 0x00;
        sendbuf_0[index++] = FRAME_LENGTH;

        sendbuf_0[index++] = address_byte_0;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        sendbuf_0[index++] = address_byte_1;
        sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_READ_TILEREG); 
        sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  

        for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

        printf("Sending SHOW NET STATS to tile: %d, address: %s\n", tile, address_stats);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");


            //ts.tv_sec = 1;
            ts.tv_nsec = 1000;
            nanosleep(&ts, NULL);        

      }// end for 15 times
    }// end for switches

//    for(; index < 0x016B/*tx_len_0*/; index++) sendbuf_0[index] = 0xff;

//    printf("Sending SHOW NET STATS to tile: %d, address: %s\n", tile, addresssz);
    //if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
     // printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");

    
  } else if (strncmp(new_command, "mde_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mde command (MC DEBUG ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_MC_DEBUG_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending MC DEBUG ENABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_memory_controller_enable(tile);
    }
  } else if (strncmp(new_command, "mdd_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mdd command (MC DEBUG DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_MC_DEBUG_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);

    if (use_hnlib == 0) {  
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      printf("Sending MC DEBUG DISABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_memory_controller_disable(tile);
    }
  } else if (strncmp(new_command, "tre_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mde command (TILEREG DEBUG ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_U2M_DEBUG_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending TR DEBUG ENABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      printf("Sending TR DEBUG ENABLE frame to tile %d\n", tile);
      hn_debug_tilereg_enable(tile);
    }
  } else if (strncmp(new_command, "trd_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mdd command (TILEREG DEBUG DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_U2M_DEBUG_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending TR DEBUG DISABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      printf("Sending TR DEBUG DISABLE frame to tile %d\n", tile);
      hn_debug_tilereg_disable(tile);
    }
   } else if (strncmp(new_command, "u2me_", 5) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mde command (U2M DEBUG ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_U2M_DEBUG_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending U2M DEBUG ENABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_unit2mango_enable(tile);
    }
  } else if (strncmp(new_command, "u2md_", 5) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mdd command (U2M DEBUG DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_U2M_DEBUG_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending U2M DEBUG DISABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_unit2mango_disable(tile);
    }
  } else if (strncmp(new_command, "m2ue_", 5) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mde command (M2U DEBUG ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_M2U_DEBUG_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    
    if (use_hnlib == 0) {  
      printf("Sending M2U DEBUG ENABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_mango2unit_enable(tile);
    }
  } else if (strncmp(new_command, "m2ud_", 5) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mdd command (M2U DEBUG DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_M2U_DEBUG_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending M2U DEBUG DISABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_mango2unit_disable(tile);
    }

  } else if (strncmp(new_command, "injde_", 6) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mde command (NET INJECT DEBUG ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_INJECT_DEBUG_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending NET INJECT DEBUG ENABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n"); 
    } else {
      hn_debug_inject_enable(tile);
    }
  } else if (strncmp(new_command, "injdd_", 6) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mdd command (NET INJECT DEBUG DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_INJECT_DEBUG_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending NET INJECT DEBUG DISABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_inject_disable(tile);
    }
  } else if (strncmp(new_command, "ejede_", 6) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mde command (NET EJECT DEBUG ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_EJECT_DEBUG_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending NET EJECT DEBUG ENABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_eject_enable(tile);
    }
  } else if (strncmp(new_command, "ejedd_", 6) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- mdd command (NET EJECT DEBUG DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    // lets get the tile
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);

    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_EJECT_DEBUG_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending NET EJECT DEBUG DISABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_debug_eject_disable(tile);
    }
  } else if (strncmp(new_command, "echoe_", 6) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- ddecho_enable command (DD ECHO ENABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);
    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_ECHO_ENABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending ECHO ENABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
//      hn_echo_enable(tile);
    }
  } else if (strncmp(new_command, "echod_", 6) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- ddecho_disable command (DD ECHO DISABLE)
    // ---------------------------------------------------------------------------------------------------------
    //
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);
    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = COMMAND_ECHO_DISABLE;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CONFIGURE_DEBUG_AND_STATS); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending ECHO DISABLE frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
//      hn_echo_disable(tile);
    }
  } else if (strncmp(new_command, "ticks_", 6) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- run clock ticks command (TICKS)
    // ---------------------------------------------------------------------------------------------------------
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    char *p2 = strstr(p1, "_")+1;
    strncpy(tilesz, p1, p2-p1-1);
    tilesz[p2-p1-1] = 0;
    int tile = atoi(tilesz);
    int index = payload_0_index_first_byte;
    char newmask[LINE_MAX_LENGTH];
    //char fc;
    //int  nindex = 0;
    char newmask_value = 0;
    int conversion_ok = 0;

    // lets get the divider
    strcpy(newmask, p2);

    //printf("bit position is: %s\n", newmask);

    conversion_ok = strToChar(newmask, &newmask_value);


    if(conversion_ok){
      printf("value %x\n", newmask_value);

      sendbuf_0[index++] = 0x00;
      sendbuf_0[index++] = FRAME_LENGTH;
      sendbuf_0[index++] = RUNFOR_CLOCK_FUNC | newmask_value;
      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CLOCK); 
      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

      if (use_hnlib == 0) {  
        printf("Sending RUN_CLOCK_TICKS with clock tic value: %d to tile %d\n", (int)newmask_value, tile);
        if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
          printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
      } else {
        hn_ticks(tile, (uint32_t)newmask_value);
      }
    } else {
      printf("   ERR: unsupported mask string \"%s\". Valid range \[0,63]\n\n", newmask);
    }
  } else if(strncmp(new_command, "stopclk_", 8) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- stop clock command (STOP_CLOCK)
    // ---------------------------------------------------------------------------------------------------------
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);
    int index = payload_0_index_first_byte;
    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = PAUSE_CLOCK_FUNC;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CLOCK); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending STOP CLOCK frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_stop_clk(tile);
    }
  } else if(strncmp(new_command, "resumeclk_", 10) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- resume clock command (RESUME_CLOCK)
    // ---------------------------------------------------------------------------------------------------------
    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);
    int index = payload_0_index_first_byte;

    sendbuf_0[index++] = 0x00;
    sendbuf_0[index++] = FRAME_LENGTH;
    sendbuf_0[index++] = RESUME_CLOCK_FUNC;
    sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_CLOCK); 
    sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
    for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;

    if (use_hnlib == 0) {  
      printf("Sending RESUME CLOCK frame to tile %d\n", tile);
      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
        printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");
    } else {
      hn_resume_clk(tile);
    }
  } else if (strncmp(new_command, "z_", 2) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- sleep command (SLEEP)
    // ---------------------------------------------------------------------------------------------------------
    // lets get the time (nanoseconds)
    char timesz[20];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(timesz, p1);
    struct timespec ts2;
    ts2.tv_sec = atol(timesz)/1000000000;
    ts2.tv_nsec = atol(timesz)%1000000000;
    printf("sleeping %ld s, %ld ns\n",ts2.tv_sec,ts2.tv_nsec);
    nanosleep(&ts2, NULL);
  } else if (strncmp(new_command, "reload_macros", 13) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- reload_macros command (RELOAD_MACROS)
    // ---------------------------------------------------------------------------------------------------------
    read_macros();
  } else if (strncmp(new_command, "cmd_", 4) == 0) {
    // ---------------------------------------------------------------------------------------------------------
    // -- cmd mode (CMD)
    // ---------------------------------------------------------------------------------------------------------

    char tilesz[10];
    char *p1 = strstr(new_command, "_")+1;
    strcpy(tilesz, p1);
    int tile = atoi(tilesz);
    char *ch_ptr;

    int unit = 0;

    //int tile = 0;
    unsigned int i;
    char sz_command[100];
    
    printf("command to mango tile %d> \n", tile);
    ch_ptr = fgets(sz_command, 100, stdin);
    if(ch_ptr == NULL) printf ("ERR: cmd_ NULL ptr detected in command, unexpected behaviour may appear\n");
    sz_command[strlen(sz_command)-1] = '\n';
    sz_command[strlen(sz_command)] = '\0';

    for (i=0;i<strlen(sz_command);i++) {
      /*
         int index = payload_0_index_first_byte;
         sendbuf_0[index++] = 0x00;
         sendbuf_0[index++] = FRAME_LENGTH;

         sendbuf_0[index++] = sz_command[i];
         sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_KEY_INTERRUPT); 
         sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
       */

      if (use_hnlib == 0) {  
	      int       index;
	      u_int8_t  peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0;
	      u_int8_t  wr_reg_for_peak;

	      index        = payload_0_index_first_byte;
	      peak_item_b3 = 0x00;
	      peak_item_b2 = THIRD_ITEM_BYTE(unit);
	      peak_item_b1 = SECOND_ITEM_BYTE(unit, COMMAND_PEAK_KEY_INTERRUPT);
	      peak_item_b0 = (u_int8_t)sz_command[i];
	      wr_reg_for_peak = MANGO_WRITE_REGISTER_TO_PEAK_TILE;

	      printf("sending os byte: %2x %2x %2x %2x\n", peak_item_b3, peak_item_b2, peak_item_b1, peak_item_b0);

	      // Compose MANGO items
	      sendbuf_0[index++] = 0x00;
	      sendbuf_0[index++] = FRAME_LENGTH;

	      // 
	      sendbuf_0[index++] = wr_reg_for_peak;
	      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
	      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
	      //sendbuf_0[index++] = 0x00;
	      sendbuf_0[index++] = 0x00;
	      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
	      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
	      //sendbuf_0[index++] = 0x00;
	      //bytes of PEAK item
	      sendbuf_0[index++] = peak_item_b0;
	      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
	      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);  
	      //sendbuf_0[index++] = 0x00;
	      sendbuf_0[index++] = peak_item_b1;
	      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
	      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
	      //sendbuf_0[index++] = 0x00;
	      sendbuf_0[index++] = peak_item_b2;
	      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
	      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
	      //sendbuf_0[index++] = 0x00;
	      sendbuf_0[index++] = peak_item_b3;
	      sendbuf_0[index++] = SECOND_ITEM_BYTE(tile, COMMAND_WRITE_TILEREG); 
	      sendbuf_0[index++] = THIRD_ITEM_BYTE(tile);
	      //sendbuf_0[index++] = 0x00;

	      for(; index < tx_len_0; index++) sendbuf_0[index] = 0xff;
	      if (send_frame(sockfd, sendbuf_0, (struct sockaddr*)socket_address, sizeof(struct sockaddr_ll)) < 0)
		      printf("Send failed, are you sudo or have sudo permissions? it is necessary to open eth\n");    
              usleep(100000);
      } else {
        hn_peak_send_char(tile, unit, sz_command[i]);
      }
      usleep(100000);
    }
  }
  else if (strcmp(new_command, "q") == 0){
    // ---------------------------------------------------------------------------------------------------------
    // -- quit command (QUIT)
    // ---------------------------------------------------------------------------------------------------------
    printf("quit command detected\n");
    cont = 0;
  } else {
    printf("\nWARNING! -> Unknown command: %s\n", new_command);
    //printf("no action will be done\n");
  }

  struct timespec ts3;
  ts3.tv_sec = 0;
  ts3.tv_nsec = 300000000;
  nanosleep(&ts3, NULL);


  return cont;
}


/******************************************************************************
 * @brief Main application, open eth interface to send frames and  
 *           read commands typed in terminal
 *
 * @param argc  size of argv vector
 * @param argv  list of arguments passed to main
 * 
 * @return 
 ******************************************************************************
 **/
int main ( int argc, char ** argv )
{
  //char iface_name[32] = "eth1";
  //long long dstmac = 0xFFFFFFFFFFFFULL;
  //long long srcmac = 0x0013960385E5ULL;
  //int  packet_size  = 560;
  //int  packet_type  = 0xFFFF;

  //char c = 'a';

  int sockfd;
  struct ifreq if_idx;
  struct ifreq if_mac;

  struct sockaddr_ll socket_address;
  char ifName[IFNAMSIZ];


  char sendbuf_0[BUF_SIZ];
  struct ether_header *eh_0 = (struct ether_header *) sendbuf_0;
  //struct iphdr *iph_0 = (struct iphdr *) (sendbuf_0 + sizeof(struct ether_header));

  if (argc < 2) {
    printf("USAGE: %s [-hnlib || <device>] [macros_file]\n", argv[0]);
    printf("   macros_file: an additional macros file to macros.txt\n");
    exit(1);
  }


  /* Get interface name */
  if (argc > 1) {
    if (strcmp(argv[1], "-hnlib") == 0) {
      use_hnlib = 1;
    } else {
      use_hnlib = 0;
      strcpy(ifName, argv[1]);
    }
  } else {
    use_hnlib = 0;
    strcpy(ifName, DEFAULT_IF);
  }

  macros_fname[0] = 0;
  if (argc > 2) {
    strcpy(macros_fname, argv[2]);
  }  
  
  // read macros
  read_macros();    

  if (use_hnlib == 0) {
    /* Open RAW socket to send on */
    if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
      perror("socket");
    }

    /* Get the index of the interface to send on */
    memset(&if_idx, 0, sizeof(struct ifreq));
    strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
    if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
      perror("SIOCGIFINDEX");

    /* Get the MAC address of the interface to send on */
    memset(&if_mac, 0, sizeof(struct ifreq));
    strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
    if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
      perror("SIOCGIFHWADDR");

    /* Construct the Ethernet header   UNFreeze fpga*/
    memset(sendbuf_0, 0, BUF_SIZ);

    /* Ethernet header */
    eh_0->ether_shost[0] = MY_SRC_MAC0;
    eh_0->ether_shost[1] = MY_SRC_MAC1;
    eh_0->ether_shost[2] = MY_SRC_MAC2;
    eh_0->ether_shost[3] = MY_SRC_MAC3;
    eh_0->ether_shost[4] = MY_SRC_MAC4;
    eh_0->ether_shost[5] = MY_SRC_MAC5;
    eh_0->ether_dhost[0] = MY_DEST_MAC0;
    eh_0->ether_dhost[1] = MY_DEST_MAC1;
    eh_0->ether_dhost[2] = MY_DEST_MAC2;
    eh_0->ether_dhost[3] = MY_DEST_MAC3;
    eh_0->ether_dhost[4] = MY_DEST_MAC4;
    eh_0->ether_dhost[5] = MY_DEST_MAC5;
    /* Ethertype field */
    eh_0->ether_type = htons(ETH_P_IP);


    tx_len_0 += sizeof(struct ether_header);
    /* Packet data -> unfreeze tiles */
    // 90 was the length for 2bytes/item (72 items fit in a frame). To keep same behaviour 
    // length has to be D8 for 3bytes/item (72 items fit in a frame)
    u_int8_t frame_length = FRAME_LENGTH;
    sendbuf_0[tx_len_0++] = 0x00;
    sendbuf_0[tx_len_0++] = frame_length;  
    u_int8_t ii;
    for (ii = 0; ii < frame_length ; ii++) {
      sendbuf_0[tx_len_0++] = 0xff;
    }

    payload_0_index_first_byte = sizeof(struct ether_header);

    /* Index of the network device */
    socket_address.sll_ifindex = if_idx.ifr_ifindex;
    /* Address length*/
    socket_address.sll_halen = ETH_ALEN;
    /* Destination MAC */
    socket_address.sll_addr[0] = MY_DEST_MAC0;
    socket_address.sll_addr[1] = MY_DEST_MAC1;
    socket_address.sll_addr[2] = MY_DEST_MAC2;
    socket_address.sll_addr[3] = MY_DEST_MAC3;
    socket_address.sll_addr[4] = MY_DEST_MAC4;
    socket_address.sll_addr[5] = MY_DEST_MAC5;
  } else {
    payload_0_index_first_byte = 0;
    tx_len_0 = 1025; // prueba (debe ser multiplo de 3 + 2)
    // hn library initialization
    hn_filter_t filter;
    filter.target = HN_FILTER_TARGET_MANGO;
    filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS; //HN_FILTER_APPL_MODE_NONE;  // The application will not receive items
    filter.tile   = 0;
    filter.core   = 0;
    uint32_t init_lib;
    init_lib = hn_initialize(filter, NO_PARTITION_STRATEGY, 1, 0, 1);
    if (init_lib != HN_SUCCEEDED) {
      printf("Error, hn library\n");
      exit(1);
    }
  }

  printf("\n");
  printf("    -----------------------------------------\n");   
  printf("                The MANGO Project            \n"); 
  printf("        Starting TX terminal application\n");
  printf("\n");
  printf("                             Type 'q' to quit\n");
  printf("    -----------------------------------------\n\n"); 

  char line[LINE_MAX_LENGTH];
  char cont = 1;
  char *ptr_stdin;

//  ProcessCommand("wm_0_mem4.data",  sendbuf_0, sockfd, &socket_address);
//  ProcessCommand("wcp_0_conf0.txt",  sendbuf_0, sockfd, &socket_address);


  int i;
  do {
    printf("Enter command: ");
    //gets(line);
    //if (true){

    ptr_stdin = fgets(line, LINE_MAX_LENGTH, stdin);
    line[strlen(line)-1] = '\0';
    if(ptr_stdin != NULL){

      // Let's see if this is a macro
      for ( i = 0; i < num_macros; i++ ){ 
        if (!(strcmp(macro_name[i], line)))
        {
          printf("macro found, copying macro: \"%s\"\n", macro_name[i]);
          strcpy(line, macro_string[i]);
          printf("   line:\"%s\"\n", line);

        }
      }


      if(strlen(line) > (size_t)0){
        // not empty text line
        char *cmd;
        char *next;
        cmd = line;
        char sub_cmd[LINE_MAX_LENGTH];

        while( (next = strchr(cmd, ' ')) != NULL ){
          printf("command: ");
          size_t j;
          for(j = 0; j < (size_t)(next-cmd); j++){
            sub_cmd[j] = cmd[j];
            sub_cmd[j +1] = 0;
            printf("%c",cmd[j]);
          }
          printf("\n");

          cont = cont & ProcessCommand(sub_cmd, sendbuf_0, sockfd, &socket_address);

          cmd = next + 1;
        }

        if(strlen(cmd) > 0) {
          printf("command: ");
          size_t j;
          for(j = 0; j < (strlen(cmd)); j++) {
            sub_cmd[j] = cmd[j];
            sub_cmd[j +1] = 0;
            printf("%c",cmd[j]);
          }
          printf("\n");

          cont = cont & ProcessCommand(sub_cmd, sendbuf_0, sockfd, &socket_address);
        }
      }


      //index = 0;
      int i;
      for(i = 0; i < LINE_MAX_LENGTH; i++) line[i] = 0;

    }
  } while (cont);

  if (use_hnlib) hn_end();

  printf("\n\n     quit application.\n     Bye.\n\n");
  return (0);

}


/*
 ******************************************************************************
 ** end of tx_term.c
 ******************************************************************************
 */

