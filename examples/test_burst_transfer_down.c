///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 15, 2018
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Initialize hn library, request a new buffer in shared memrory, destroy buffer in shared memory
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "hn.h"


//#define DEBUG

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
struct timespec get_elapsed_time(struct timespec start, struct timespec end)
{
  struct timespec temp;
  if ((end.tv_nsec - start.tv_nsec) < 0) 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  } 
  else 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }
  return temp;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
unsigned long int get_elapsed_time_nanoseconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;


  temp = get_elapsed_time(start, end);
  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;
  
  return elapsed_nanoseconds;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * \brief main test application
 *
 * accepts one parameter ./test_burst_transfer_donw <num_iterations>
 */

int main(int argc, char **argv)
{
  int        test_num_of_passes; // number of times the test will run, if no parameter is passed, the test will run for 100 times

  uint32_t   rv;
  void      *shm_buffer_base_addr_ptr;
  uint32_t   shm_buffer_size;
  uint32_t   number_of_blocks;
  uint32_t   block_size_in_bytes;


//  int        random_number;
  uint64_t   value;
  uint64_t   value_increment;
  uint32_t   it;

  //Write to DDR memory  
  uint64_t  *shm_buffer_ptr_64bit;
  uint32_t   number_of_blocks_to_send;
                                   // function call variables
  void      *transfer_wr_src_addr;
  uint32_t   transfer_wr_dst_tile;
  uint32_t   transfer_wr_dst_addr;
  uint32_t   transfer_wr_is_blocking;
  uint32_t   transfer_wr_size;
  uint64_t   transfer_last_64bit;

  // Read from DDR memory
//  uint64_t  *shm_buffer_rd_ptr_64bit;
//  uint32_t   number_of_entries_to_read;
//  void      *transfer_rd_dst_addr; // address of shared memory buffer to store data read from DDR memory in HN system
//  uint32_t   transfer_rd_src_tile; // tile where DDR is located
//  uint32_t   transfer_rd_src_addr; // address to read from DDR memory
//  uint32_t   transfer_rd_is_blocking; 
//  uint32_t   transfer_rd_size;     // number of bytes to read from DDR memory

  double   ts_megabytes;

  struct timespec    t1;
  struct timespec    t2;
  unsigned long int  elapsed_ns;  // time elapsed in nanoseconds
  float              elapsed_in_sec;

  uint64_t stats_active_wr_to_data_burst_dnstr_fifo_expected;
  uint64_t stats_active_rd_from_data_burst_dnstr_fifo_expected;
  uint64_t stats_active_wr_to_simple_fifo_expected;
  uint64_t stats_active_rd_from_simple_fifo_expected;

  uint64_t stats_active_wr_to_data_burst_dnstr_fifo_read;
  uint64_t stats_active_rd_from_data_burst_dnstr_fifo_read;
  uint64_t stats_active_wr_to_simple_fifo_read;
  uint64_t stats_active_rd_from_simple_fifo_read;
   
  uint32_t  rd_tilereg_value_msb; // value read from tilereg
  uint32_t  rd_tilereg_value_lsb; // value read from tilereg
  uint64_t  rd64_tilereg_value_msb; // value read from tilereg
  uint64_t  rd64_tilereg_value_lsb; // value read from tilereg

  uint32_t  transfer_last_mc_wr_addr;
  char      block_data [64];
  //uint64_t  msb_of_block;
  uint64_t  readblock_last_64bit;


  uint32_t           pass_it;
  uint32_t           pass_failed;

  pass_failed = 0;
  pass_it     = 0;
  stats_active_wr_to_data_burst_dnstr_fifo_expected    = 0;
  stats_active_rd_from_data_burst_dnstr_fifo_expected  = 0;
  stats_active_wr_to_simple_fifo_expected              = 0;
  stats_active_rd_from_simple_fifo_expected            = 0;

  test_num_of_passes  = 2;
  //number_of_blocks         = 30000000;
  //number_of_blocks         = 5000000;
  //number_of_blocks         = 3000000;
  //number_of_blocks           = 70000;
  //number_of_blocks         = 30000;
  number_of_blocks         = 30;


  // set new number of passes the test will perform in case that the user specified any value
  if (argc >= 2)
  {
    int val;
    val = atoi(argv[1]);
    if (val > 0)
    {
      test_num_of_passes = val;
    }
  }

  //
  // We prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;
   
  printf("\n");
  printf("---------------------------------\n");
  printf("     DEMO Application\n");
  printf("       Initialize hn library\n");
  printf("       Request buffer in shared memory area\n");
  printf("       fill buffer in host side\n");
  printf("       transfer buffer to Memory on FPGA\n");
//  printf("       read Memory and save to local buffer\n")
  printf("       check transfer\n");
  printf("       Close buffer in shared memory area\n");
  printf("       \n");
  printf("       Test will run %d times\n", test_num_of_passes);
  printf("       quit\n");
  printf("---------------------------------\n");
  printf("\n");
  fflush(stdout);
  
  // We initialize the hn library with the filter and the UPV's partition strategy
  //hn_initialize(hn_filter_t filter, uint32_t partition_strategy, uint32_t socket_connection, uint32_t reset, uint32_t capture_signals_enable)
  //printf("reset system before test\n");
  //hn_reset(0);
  
  printf("Initialize hn\n");
  rv = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (rv != HN_SUCCEEDED) {
    hn_print_error(rv);
    exit(1);
  }
  printf("   ...done\n");

  printf("reset system before run the test\n");
  fflush(stdout);
  hn_reset(0);


  block_size_in_bytes      = 64; // 64byte values, to mimic block width in DDR memory  
  shm_buffer_size          = number_of_blocks * block_size_in_bytes; // MULTIPLE OF 512 bits -> 64 BYTES 
  ts_megabytes             = (double)shm_buffer_size /(1024.0 * 1024.0);
  number_of_blocks_to_send = number_of_blocks;  // send half of the buffer

  printf("Request a shm buffer for %u blocks. It is %u bytes -> %4.4f MB\n", number_of_blocks, shm_buffer_size, ts_megabytes);
  rv = hn_shm_malloc(shm_buffer_size, &shm_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error requesting buffer in shared memory, returned: %u\n", rv);
    hn_end();
    exit(1);
  }

  printf("  shmn buffer granted on addr 0x%08lx    of %u bytes\n", (unsigned long)shm_buffer_base_addr_ptr, shm_buffer_size);
  printf("  each run will write %lu values of 64 bits on the buffer\n", shm_buffer_size/sizeof(uint64_t));
  printf("\n");
  // set pointers
  shm_buffer_ptr_64bit =  (uint64_t *)shm_buffer_base_addr_ptr;
  
  // set initial seed for the test
  srand(time(NULL));
  for (pass_it = 0; pass_it < test_num_of_passes; pass_it++)
  {
    uint32_t w0;
    uint32_t w1;
    uint64_t w64_0;
    uint64_t w64_1;


    // temporal 
    float temp_0;
    float temp_1;
    hn_get_tile_temperature(0, &temp_0);
    hn_get_tile_temperature(1, &temp_1);
    printf("temperature of tiles \n");
    printf(" tile 0  %fºC\n", temp_0);
    printf(" tile 1  %fºC\n", temp_1);
    printf("\n");
    //////////////////////////////// end of temporal


    //value                  = 0x0000000000000000; // initial value for the data of the shm buffer
    value                  = rand() + RAND_MAX/2;
    value_increment        = 0x0000000000000001;

    for (it = 0; it < shm_buffer_size/sizeof(uint64_t); it++)
    {
      *shm_buffer_ptr_64bit = value;
      // increase ptr to next uint64 position 
      value = value + value_increment;
      shm_buffer_ptr_64bit++;
    }
    //printf("   last written addr: 0x%016lx   with value: 0X%016lx  (0X%016lx)\n", (unsigned long)shm_buffer_ptr_64bit-1, *(shm_buffer_ptr_64bit - 1), value - value_increment);
    //  shm_buffer_ptr_64bit =  (uint64_t *)shm_buffer_base_addr_ptr;
    //  for (it = 0; it < number_of_entries; it++)
    //  {
    //   printf("  0x%016lx    0X%016lx\n", (unsigned long)shm_buffer_ptr_64bit, *shm_buffer_ptr_64bit);
    //   shm_buffer_ptr_64bit++;
    //  }

    // write to DDR-memory from shared memory buffer tesa
    transfer_wr_dst_tile    = 0;
    transfer_wr_dst_addr    = 0x00000000;;
    transfer_wr_is_blocking = 1;
    transfer_wr_size        = number_of_blocks_to_send * block_size_in_bytes;
    shm_buffer_ptr_64bit    = (uint64_t *)shm_buffer_base_addr_ptr;
    //shm_buffer_ptr_64bit    = shm_buffer_ptr_64bit + 0xa;
    shm_buffer_ptr_64bit    = shm_buffer_ptr_64bit;
    transfer_wr_src_addr    = (void *)shm_buffer_ptr_64bit;

    transfer_last_mc_wr_addr = (number_of_blocks_to_send - 1) * block_size_in_bytes;
    transfer_last_64bit      = *(shm_buffer_ptr_64bit + (transfer_wr_size/8) -1);

#ifdef DEBUG
    printf("Send notification to hn_daemon to send partial buffer to HN system\n");
    printf("Test will send   %d blocks, as %u bytes\n", number_of_blocks_to_send, transfer_wr_size);
    printf("  transfer configuration: \n");
    printf("    base addr:     %p\n",        (void *)shm_buffer_base_addr_ptr);
    printf("    src addr:      %p\n",        (void *)transfer_wr_src_addr);
    printf("    size:          %u bytes\n",  transfer_wr_size);
    printf("    dst  mc tile:  %u\n",        transfer_wr_dst_tile);
    printf("    dst  mc addr:  0x%08X\n",    transfer_wr_dst_addr);
    printf("    last mc addr   0x%08X\n",    transfer_last_mc_wr_addr);
    printf("    first val:     0x%016lx\n", *shm_buffer_ptr_64bit);
    printf("    last  val:     0x%016lx\n",  transfer_last_64bit);
    printf("    %sblocking transfer\n",      transfer_wr_is_blocking?"":"non-");
#endif
    clock_gettime(CLOCK_MONOTONIC, &t1);

    hn_shm_write_memory(transfer_wr_src_addr , transfer_wr_size, transfer_wr_dst_addr, transfer_wr_dst_tile, transfer_wr_is_blocking);
#ifdef DEBUG    
    printf("...done\n\n");
#endif
    clock_gettime(CLOCK_MONOTONIC, &t2);
    elapsed_ns     = (float)get_elapsed_time_nanoseconds(t1, t2);
    elapsed_in_sec = elapsed_ns / (float)1000000000.0;

    //  // check the last value written in the memory to validate all data has been sent
    //  //  then also check the values of the counters of the fifos, they will contain 8words per header and (transfered_bytes / 8)
    //  // hn_read_register
    //  //   short: Reads a registers from a tile
    //  //   argument tile: Tile where the register is located
    //  //   argument reg: Register to read
    //  //   argument data: the value of the register is returned on this argument
    //  uint32_t hn_read_register(uint32_t tile, uint32_t reg, uint32_t *data);
    //  // hn_read_memory_block
    //  //   short: reads a block-aligned memory block
    //  //   argument tile: Tile where the memory attached will be read
    //  //   argument addr: Address from where the memory will be accessed
    //  //   argument data: Pointer to the variable to put the data read
    //  uint32_t hn_read_memory_block(uint32_t tile, uint32_t addr, char *data);
#ifdef DEBUG
    printf("  checking registers\n");
#endif
    stats_active_wr_to_data_burst_dnstr_fifo_expected    = stats_active_wr_to_data_burst_dnstr_fifo_expected    + (1 * 8) + (transfer_wr_size / 8);
    stats_active_rd_from_data_burst_dnstr_fifo_expected  = stats_active_rd_from_data_burst_dnstr_fifo_expected  + (1 * 8) + (transfer_wr_size / 8);
    stats_active_wr_to_simple_fifo_expected              = stats_active_wr_to_simple_fifo_expected              + (1 * 8) + (transfer_wr_size / 8);
    stats_active_rd_from_simple_fifo_expected            = stats_active_rd_from_simple_fifo_expected            + (1 * 8) + (transfer_wr_size / 8);

    hn_read_register(0, 1011, &rd_tilereg_value_msb);
    hn_read_register(0, 1012, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_wr_to_data_burst_dnstr_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);

    hn_read_register(0, 1013, &rd_tilereg_value_msb);
    hn_read_register(0, 1014, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_rd_from_data_burst_dnstr_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);

    hn_read_register(0, 1015, &rd_tilereg_value_msb);
    hn_read_register(0, 1016, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_wr_to_simple_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);

    hn_read_register(0, 1017, &rd_tilereg_value_msb);
    hn_read_register(0, 1018, &rd_tilereg_value_lsb);
    rd64_tilereg_value_msb = rd_tilereg_value_msb;
    rd64_tilereg_value_lsb = rd_tilereg_value_lsb;
    stats_active_rd_from_simple_fifo_read = ((rd64_tilereg_value_msb << 32) & 0xFFFFFFFF00000000) + (rd64_tilereg_value_lsb & 0x00000000FFFFFFFF);

    hn_read_memory_block(0, transfer_last_mc_wr_addr,&block_data[0]);

    w0 = *((uint32_t *)&block_data[60]);
    w1 = *((uint32_t *)&block_data[56]);
    w64_0 = w0;
    w64_1 = w1;
    readblock_last_64bit = w64_0 << 32 | w64_1;


    if (
        ( stats_active_wr_to_data_burst_dnstr_fifo_expected   ==  stats_active_wr_to_data_burst_dnstr_fifo_read   )
        && ( stats_active_rd_from_data_burst_dnstr_fifo_expected ==  stats_active_rd_from_data_burst_dnstr_fifo_read )
        && ( stats_active_wr_to_simple_fifo_expected             ==  stats_active_wr_to_simple_fifo_read             )
        && ( stats_active_rd_from_simple_fifo_expected           ==  stats_active_rd_from_simple_fifo_read           )
        && ( transfer_last_64bit                                 ==  readblock_last_64bit                            )
       )
    {
      printf("#%u passed. Write BW = %6.4f kB/s (size %6.2f kB  the test took: %12ld ns  --  %3.3f s))\n", pass_it, (float) ((float)(transfer_wr_size) / (float)(1024.0)) / elapsed_in_sec, (float)transfer_wr_size / (float)1024.0, elapsed_ns, elapsed_in_sec );
      fflush(stdout);
    }
    else
    {
      printf("#%u FAILED Write BW = %6.4f kB/s (size %6.2f kB  the test took: %12ld ns  --  %3.3f s))\n", pass_it, (float) ((float)(transfer_wr_size) / (float)(1024.0)) / elapsed_in_sec, (float)transfer_wr_size / (float)1024.0, elapsed_ns, elapsed_in_sec );
      
      printf("  %8s stats_active_wr_to_data_burst_dnstr_fifo     expected     %16lu   read    %10lu \n", (stats_active_wr_to_data_burst_dnstr_fifo_expected == stats_active_wr_to_data_burst_dnstr_fifo_read)?"OK":"FAILED", stats_active_wr_to_data_burst_dnstr_fifo_expected   , stats_active_wr_to_data_burst_dnstr_fifo_read   );
      printf("  %8s stats_active_rd_from_data_burst_dnstr_fifo   expected     %16lu   read    %10lu \n", (stats_active_rd_from_data_burst_dnstr_fifo_expected == stats_active_rd_from_data_burst_dnstr_fifo_read)?"OK":"FAILED", stats_active_rd_from_data_burst_dnstr_fifo_expected , stats_active_rd_from_data_burst_dnstr_fifo_read );
      printf("  %8s stats_active_wr_to_simple_fifo               expected     %16lu   read    %10lu \n", (stats_active_wr_to_simple_fifo_expected == stats_active_wr_to_simple_fifo_read)?"OK":"FAILED", stats_active_wr_to_simple_fifo_expected, stats_active_wr_to_simple_fifo_read             );
      printf("  %8s stats_active_rd_from_simple_fifo             expected     %16lu   read    %10lu \n", (stats_active_rd_from_simple_fifo_expected == stats_active_rd_from_simple_fifo_read)?"OK":"FAILED", stats_active_rd_from_simple_fifo_expected , stats_active_rd_from_simple_fifo_read           );
      printf("  %8s last 64bit word                              expected  0x%016lx   read 0x%016lx \n", (transfer_last_64bit == readblock_last_64bit)? "OK":"FAILED", transfer_last_64bit, readblock_last_64bit);
      printf("\n");

      pass_failed++;
      // setting values on expected stats variables to avoid false error detections
      printf("reset system before continue\n");
      fflush(stdout);
      hn_reset(0);
      stats_active_wr_to_data_burst_dnstr_fifo_expected    = 0;
      stats_active_rd_from_data_burst_dnstr_fifo_expected  = 0;
      stats_active_wr_to_simple_fifo_expected              = 0;
      stats_active_rd_from_simple_fifo_expected            = 0;
      
    }
    //end of pass iteration, loop until all runs have completed
  }
  
  // Test finished, release all data structures
  printf("\n");
  printf("all done\n\n");

  if (pass_failed != 0)
  {
    printf(" TEST KO !!\n");
    printf(" %u of %u runs failed\n", pass_failed, pass_it);
  }
  else
  {
    printf(" TEST OK \n");
    printf(" total runs: %u\n", pass_it);
  }

  
  rv = hn_shm_free(shm_buffer_base_addr_ptr);
  if (rv != HN_SUCCEEDED) {
    printf("Error deleting buffer in shared memory, returned: %u", rv);
    hn_end();
    exit(1);
  }
  printf("Release communication sockets and resources\n");
  hn_end();

  printf("bye bye\n\n");

  return 0;
}
//*********************************************************************************************************************
// end of file hn_client.cpp
//*********************************************************************************************************************

