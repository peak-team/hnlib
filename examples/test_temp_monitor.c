///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: may 15, 2018
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Initialize hn library
//
//    The hn_lib mananges ctrl+c signal to softly stop the hn_lib and release the resources,
//      for any other operations that the application should do, please install the appropiate signal handler
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>
#include <locale.h>

#include "hn.h"


#define DEBUG

#define TIME_TO_READ_TEMP_MS ((float)(0.500))

volatile sig_atomic_t run_test = 1;
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * \brief Signal handler
 *
 * Detect ctl+c signal and toggle run_test signal to terminate thread in a soft manner
 */

void signal_handler_function(int signal)
{
  if (signal == SIGINT) {
    run_test = 0;

    printf("\n");
    printf(" stopping test\n");
    fflush(stdout);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
struct timespec get_elapsed_time(struct timespec start, struct timespec end)
{
  struct timespec temp;
  if ((end.tv_nsec - start.tv_nsec) < 0) 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  } 
  else 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }
  return temp;
}

unsigned long int get_elapsed_time_nanoseconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;


  temp = get_elapsed_time(start, end);
  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;
  
  return elapsed_nanoseconds;
}

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/*
 * \brief main test application
 *
 * 
 */

int main(int argc, char **argv)
{
  int         cmd_opt;              // temporal variable to store commands received as parameter
  uint32_t    reset_system;         // indicates whether we will send reset signal to the system before running the test
  uint32_t    manual_mode_enable;   // indicates whether the time between samples will be automatic or manual
                                    //    for auto mode, it requests temperature every 2 * num_times * TIME_TO_READ_TEMP_MS
  uint32_t    time_between_samples; // for manual mode: time in seconds that the system will sleep between two consecutive reads of the temperature sensor, like a "siesta"
  uint32_t    time_between_samples_automatic;
  uint32_t    silent_mode_enable;   // indicates wheter the test will display the temperatures or not (set to 0). This mode is intended to minimize output when running the automated testbench 

  uint32_t    error_detected;
  int         val;
  time_t      rawtime;
  struct tm  *timeinfo;
  uint32_t    num_tiles_in_arch;
  uint32_t    num_tiles_dim_x;
  uint32_t    num_tiles_dim_y;
  uint32_t    tmp_val;
  uint32_t    arch_id;                   // architecture id returned by hn_lib, architecture running on HN_system
  uint64_t    number_of_readings_errors; // number of times that the hn_lib reported an error on a read temperature function call 
  uint64_t    number_of_readings;        // total of times the function has been called, number of temperature readings
  uint32_t    tile_index;
  struct timespec    t_start;
  struct timespec    t_end;
  //struct timespec    t_elapsed;
  unsigned long      t_elapsed_ns;

  setlocale(LC_ALL, ""); /* use user selected locale */


  if (signal(SIGINT, signal_handler_function) == SIG_ERR)
  {
    printf("Error setting signal handler to detect end of test\n");
    printf("  %s\n", strerror(errno));
    printf("cancel test\n");
    fflush(stdout);
    return (-1);
  }

  // set default values for application parameters
  reset_system         = 0;
  time_between_samples = 2;
  error_detected       = 0;
  silent_mode_enable   = 0;
  manual_mode_enable   = 0;


  // Display Welcome Message
  printf("\n\n");
  printf("-------------------------------------------------------------------------------\n");
  printf("-------------------------------------------------------------------------------\n");
  printf("  \n");
  printf("  DEMO Application\n");
  printf("    Initialize hn library\n");
  printf("    Read temperature sensors\n");
  printf("      Tile -> provides temperature of physical FPGA where the tile is located \n");
  printf("      Motherboard-> provides the temperature of the motherboard FPGA \n");
  printf("  \n");
  printf("  run example test with -h option for help\n");
  printf("  press ctrl+c to stop the test\n");
  printf("    this will trigger the routine to stop the hn_lib in a soft and controlled manner\n");
  printf("  \n");
  printf("--------------------------------------------------------------------------------\n");


  // process args searching application configuration parameters
  printf("\n");
  printf("---------------------------------\n");
  while ((cmd_opt = getopt (argc, argv, "rst:h")) != -1)
  {
    switch (cmd_opt) 
    {
      case 'r':
        printf("option -> reset, will reset system before running test\n");
        reset_system = 1;
        break;
      case 's':
        printf("option -> silent mode enabled, will not display temperature values, The test will only acquire temperature and look for erroneous readings\n");
        silent_mode_enable = 1;
        break;
      case 't':
        val = atoi(optarg);
        printf("option -> manual mode enabled\n");
        if(val >= 0) {
          time_between_samples = val;
          printf("          manual mode enabled set time between samples to : %u seconds\n", time_between_samples);
        } else {
          printf("          manual mode enable  failed. Value will be set automaticall\n");
          printf("          Wrong value, %s out of range\n", optarg);
        }
        break;
      case 'h':
        printf("HELP\n");
        printf("supported options\n");
        printf("  -t <time_between_samples> -r -s\n");
        printf("  -t <time> set time in seconds between 2 consecutive temperature reads\n");
        printf("  -r reset system before running the test\n");
        printf("  -s enable silent mode, temperature will not be displayed on sreen, automated testbench usage\n");
        return -1;
      case '?':
        if ((optopt == 't'))
          printf ("Option -%c requires an argument\n", optopt);
        else if (isprint (optopt))
          printf ("Unknown option `-%c'\n", optopt);
        else
          printf ("Unknown option character `\\x%x'\n", optopt);
        return 1;
      default:
        return -1;
    }
  }

  // Prepare the filter to enable access to registers and statistics
  hn_filter_t filter;
  filter.target = HN_FILTER_TARGET_MANGO; 
  filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  filter.tile   = 999;
  filter.core   = 999;


  // Initialize the hn library with the filter and the UPV's partition strategy
  printf("Initialize hn library....\n");
  tmp_val = hn_initialize(filter, UPV_PARTITION_STRATEGY, 1, 0, 1);
  if (tmp_val != HN_SUCCEEDED)
  {
    printf("FATAL initializing hn_lib\n");
    hn_print_error(tmp_val);
    exit(1);
  }
  printf("   ...done\n");

  if(reset_system) {
    printf("reset system before running the test\n");
    fflush(stdout);
    hn_reset(0);
    printf("   ...done\n");
  }

  tmp_val = hn_get_arch_id(&arch_id);
  if (tmp_val != 0) {
    printf("ERROR getting architecture ID\n");
    printf("  cancel test\n");
    error_detected = 1;
  }

  if(run_test) {
    // let's get the number of tiles in this architecture
    tmp_val =  hn_get_num_tiles(&num_tiles_in_arch, &num_tiles_dim_x, &num_tiles_dim_y);

    if (tmp_val != 0) {
      printf("ERROR getting number of tiles in architecture\n");
      printf("  cancel test\n");
      error_detected = 1;
    }

    if (num_tiles_in_arch <= 0) {
      printf("ERROR unexpected number of tiles in architecture: %u tiles\n", num_tiles_in_arch);
      printf("  cancel test\n");
      error_detected = 1;
    }
  }

  time_between_samples_automatic = ceil(2.0 * (float)num_tiles_in_arch * TIME_TO_READ_TEMP_MS);
  
  if(!manual_mode_enable) {
    time_between_samples = time_between_samples_automatic;
  } else if (time_between_samples < time_between_samples_automatic) {
    printf("\n");
    printf("WARNING: detected time_between_samples time is minor that the interval allowed by the system\n");
    printf("         replacing user specifiec value (%u seconds) per automatically calculated value (%u seconds)\n", time_between_samples, time_between_samples_automatic);
    manual_mode_enable = 0;
    time_between_samples = time_between_samples_automatic;
  }

  // let's display system information
  printf("\n");
  printf("System Information\n");
  printf("  Architecture ID:   %u\n", arch_id);
  printf("  Number of Tiles:   %u\n", num_tiles_in_arch);
  printf("\n");
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  printf("Test started on %s", asctime (timeinfo));
  if(silent_mode_enable)
  {
    printf("SILENT_MODE enabled, the test will not display temperatures\n");
  }
  printf(" Test will get temperature for all tiles and sleep for %u seconds %s\n",
      time_between_samples,
      manual_mode_enable?"(set by user)":"(set automatically)"
      );
  printf("\n");
  fflush(stdout);

  printf("\n");
  number_of_readings_errors = 0;
  number_of_readings        = 0;
  do
  {
    float    temp;

    if(silent_mode_enable == 0)
    {
      time ( &rawtime );
      timeinfo = localtime ( &rawtime );
      printf("%s", asctime (timeinfo));
    }
    clock_gettime(CLOCK_MONOTONIC, &t_start);
    for (tile_index = 0; (tile_index < num_tiles_in_arch) && (run_test); tile_index++)
    {

      tmp_val = hn_get_tile_temperature(tile_index, &temp);
      number_of_readings = number_of_readings + (uint64_t)1;
      
      if(tmp_val != 0)
      {
        // error getting temperature, we will only display -1, but will not notify the error
        temp = -999.0;
      }
      
      if((tmp_val != HN_SUCCEEDED) && run_test)
      {
        number_of_readings_errors = number_of_readings_errors + (uint64_t)1;
      }

      if(run_test && (silent_mode_enable == 0))
      {
        printf("  Tile %3u      Temp  %3.1f\n", tile_index, temp);
        fflush(stdout);
      }
    }
    clock_gettime(CLOCK_MONOTONIC, &t_end);
    t_elapsed_ns = get_elapsed_time_nanoseconds(t_start, t_end);
    

    if(silent_mode_enable == 0)
    {
      printf("\n");
      printf("Reading temperature of %u tiles took: %'lu us\n", num_tiles_in_arch, t_elapsed_ns/(unsigned long)1000);
      printf("Errors in read operation: %lu\n", number_of_readings_errors);
      fflush(stdout);
    }


    if(time_between_samples > 0)
    {
      sleep(time_between_samples);
    }

    if((silent_mode_enable == 0) && run_test)
    {
      // Let's overwrite the display parameters 
      printf("\r");
      printf("\033[A"); // time
      printf("\033[A"); // errors in read
      fflush(stdout);
      for (tile_index = 0; (tile_index < num_tiles_in_arch) && run_test; tile_index++)
      {
        printf("\033[A"); // one per each tile temperature
        fflush(stdout);
      }
     // two extra lines for the time_to_read temperature display line
     printf("\033[A"); // time
     printf("\033[A"); // errors in read
    }



  }  while (run_test);
  
  // lets make sure that no output message gets overwritten
  printf("\n\n");
  //for (tile_index = 0; (tile_index < num_tiles_in_arch); tile_index++) printf("\n");

  if (error_detected && (run_test != 0))
  {
    // we are finishing because of an error, we need to call hn_end because we did not call the hn_lib signal handler
    printf("Error detected whille running the test calling hn_end function\n");
    fflush(stdout);
    hn_end();
  }
    
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  printf("Test finished on %s", asctime (timeinfo));
  printf("total readings: %lu\n", number_of_readings);
  printf("error readings: %lu\n", number_of_readings_errors);


  if (number_of_readings_errors != 0)
  {
    printf("TEST KO \n");
    printf("   %lu read operations failed\n", number_of_readings_errors);
  }
  else
  {
    printf("TEST OK \n");
  }
 
  hn_end();

  return 0;
}
//*********************************************************************************************************************
// end of file test_acquire_temp.c
//*********************************************************************************************************************


