///////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
// 
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and 
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is 
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
// 
// contact: jflich@disca.upv.es
//-----------------------------------------------------------------------------
//
// Company:  GAP (UPV)  
// Engineer: R. Tornero (ratorga@disca.upv.es)
// 
// Create Date: 
// Design Name: 
// Module Name: 
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
//
//  
//
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <pcap.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <features.h>    /* para el nÃºmero de versiÃ³n de glibc */
#if __GLIBC__ >= 2 && __GLIBC_MINOR >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>     /* los protocolos de nivel 2 */
#else
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>   /* los protocolos de nivel 2 */
#endif
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <sched.h>
#include <pthread.h>

#include "commands.h"
#include "peak_commands.h"

#include "../hn.h"
#include "../hn_utils.h"

// CONSTANTS
#define PCAP_FILE_HEADER_SIZE      40
#define ROTATE_FILE_DEFAULT_SIZE     0x080000000UL   // 2GB
#define ROTATE_FILE_MAX_SIZE  0x140000000UL   // 5GB
#define NUM_ROTATE_FILES      3
#define LIVE_CAPTURE_FILE_BUFFER_SIZE      (1<<20)
#define PCAP_BUFFER_MEM_SIZE       (1<<29) //(4096*1204) 

#define ETHER_ADDR_LEN 6
#define BUFSIZE        1518  // max frame size 1500 + dst + src + size + fcs

#define HN_MAX_ITEMS_TO_READ_FROM_HN_SOCKET 1<<25
#define HN_MAX_ITEMS_TO_READ_FROM_HN_FILE   8192
#define MAX_LINE_M13   50000000 // maximum number of lines for mode 13

#define NOT_APPLICABLE 9999

#define L1D_TYPE  0
#define L2_TYPE   1
#define MC_TYPE   2
#define L1I_TYPE  3
#define TR_TYPE   4
#define UNIT_TYPE 5
#define DMA_TYPE  6
#define MS_TYPE   7


#define NUMBER_OF_TILES    512 // max num of tiles this program can support
#define NUMBER_OF_SUBTILES 64  // max num of subtiles this program can support 
#define L1D_DEBUG_SIZE     28  // num items that composes an L1D debug message
#define L2_DEBUG_SIZE      28  // num items that composes an L2 debug message
#define BLOCK_DEBUG_SIZE   76  // num items that composes a memory block debug message (Block 512 + Address + timestamp)
#define INJECT_DEBUG_SIZE  17  // num items that composes a Flit debug message (VN +flit + timestamp)
#define EJECT_DEBUG_SIZE   17  // num items that composes a Flit debug message (VN + flit + timestamp)
#define MC_DEBUG_SIZE      76  // num items that composes a memory block debug message (block + address + timestamp)
#define STATS_SIZE         76  // num items that composes a stat message
#define CORE_DEBUG_SIZE    33  // num items that composes a core debug message (debug (197 bits) + timestamp (64 bits) + 3 bits for padding)      
#define TR_DEBUG_SIZE      20  // num items that composes a TR debug message ((95 bits) + timestamp (64 bits) + 1 bit for padding)
#define TR_SIZE            6   // num items that composes a TR reading message
#define ITEM_ITEM_SIZE     4   // number of items that composes a item
#define U2M_DEBUG_SIZE     21 //90 //90 
#define M2U_DEBUG_SIZE     17 //79
#define MC_READ_SIZE       68
#define MANGO_TR_DEBUG_SIZE 43
#define U_INT32_T_MAX      0xFFFFFFFFUL

#define CORErow 1
#define COREcol 5
#define L1Irow  1
#define L1Icol  17
#define L1Drow  1
#define L1Dcol  30
#define L2row   4
#define L2col   17
#define R0row   7
#define R0col   5
#define R1row   7
#define R1col   17
#define R2row   7
#define R2col   30

struct eth_addresses_s {
  u_char ether_dhost[ETHER_ADDR_LEN]; // Destination host address
	u_char ether_shost[ETHER_ADDR_LEN]; // Source host address
};

struct fpga_pkt_header_s {
  u_int8_t  ether_dhost[ETH_ALEN];  // destination eth addr
  u_int8_t  ether_shost[ETH_ALEN];  // source ether addr
  u_int16_t ip_frame_type;
  u_int32_t ipv4_header[5];
  u_int32_t udp_header[2];
  u_int16_t payload_len;            // frame len 
} __attribute__ ((__packed__));


// This structure contains all program params. The ones supplied by the user and the ones that are inferred.
struct params {
  int mode; // console mode
  unsigned int tile; // filtering tile
  unsigned int subtile; // Only for the MANGO project when an item comes from a UNIT
  char devname[32]; // etherne device
  char m13fname[256]; // mode 13 debug filename 
  unsigned char use_pcap;  // use pcap library for live capture or offline analysis
  char livecapfname[NUM_ROTATE_FILES][256];                 // there are some pcap files when running live pcap capture with rotate flag. Goal: limit the growing size, since it cannot be done with fseek
  unsigned char verbose; 
  unsigned char live_capture; // live capture (from ethernet) using pcap
  int offline; // offline capture (from file) 
  FILE *fd_m13; // descriptor of the mode 13 debug file
  int line_m13;  // num lines in mode 13 debug file
  int current_livecap_file_index; // current index in the rotation process
  int rotate;  // rotate or not
  long rotate_file_size;  // size of the rotate files. When this value is reached, rotation is done 
  int pcap_buffer_size;  // buffer size in the kernel for the received frames
  int live_capture_file_block_size;  // size of the buffer for writing in file by blocks
  int offline_first_file_index;  // Used in offline analysis, when rotate is enabled. All rotate files are explored starting at the one with this index
  unsigned char use_hnlib;       // used to get items from HN library
  char hnfile[256];
};

// this structure contains the required structures for using libpcap
struct pcap_info_s {
  pcap_t        *handler;
  struct         bpf_program fp;
  pcap_dumper_t *dumper;
  struct         pcap_stat stats;

  char  buf_file[LIVE_CAPTURE_FILE_BUFFER_SIZE];

  pthread_t       show_stats_tid;
  pthread_t       flush_pcapf_tid;
  pthread_t       rotate_tid;
  pthread_attr_t  sched_attr;
  pthread_mutex_t pcap_file_mutex;
};


typedef struct hnlib_stat_st {
  unsigned long total_items;
}hnlib_stat_t;


// this structure contains the required structures for using libpcap
struct hnlib_info_s {
  FILE            *handler;

  hnlib_stat_t    stats;

  char            buf_file[LIVE_CAPTURE_FILE_BUFFER_SIZE];

  uint32_t        items[HN_MAX_ITEMS_TO_READ_FROM_HN_SOCKET];

  pthread_t       show_stats_tid;
  pthread_t       flush_tid;
  pthread_t       rotate_tid;

  pthread_attr_t  sched_attr;
  pthread_mutex_t file_mutex;
};

// for native socket access, when pcap is not used
struct socket_s {
  int fd;
};

// this structure represents an item. A set of items composes a complete debug/console/notification message
struct item_net_st {  // this is to proccess items in network packets, which format is in the network domain (not the host)
  u_char padding;
  u_char tile_cmd[2];
  u_char payload;
}__attribute__((packed));

struct item_host_st { // this is to process an item in the host domain. The ntohl function has been applied previosly
  u_int32_t payload:8;
  u_int32_t cmd:6;
  u_int32_t tile:18;
}__attribute__((packed));

struct unit_item_st {
  u_int32_t payload:8;
  u_int32_t cmd:6;
  u_int32_t tile:18;
}__attribute__((packed));


// The next Data structures contain the structure of the debug info received for some of the different debug types
struct u2m_debug {
  u_int8_t  write:1;
  u_int8_t  read:1;
  u_int8_t  injected:1;
  u_int8_t  injected_id:4;
  u_int8_t  inject:1;
  u_int16_t dst:9;
  u_int16_t byte_access_i:1;
  u_int16_t half_access_i:1;
  u_int16_t word_access_i:1;
  u_int16_t id_to_inject:4;
  u_int16_t sender_type:3;
  u_int16_t sender:9;
  u_int16_t byte_access:1;
  u_int16_t half_access:1;
  u_int16_t word_access:1;
  u_int16_t to_memory:1;
  u_int32_t v_addr;
  u_int32_t phy_addr;
  u_int64_t timestamp;
}__attribute__((packed));

struct m2u_debug {
  u_int8_t  unit_req;
  u_int8_t  table_retrieve;
  u_int8_t  table_id_to_retrieve:7;
  u_int8_t  word_access:1;
  u_int16_t dst_type:3;
  u_int16_t dst:13;
  u_int32_t address;
  u_int64_t timestamp;
}__attribute__((packed));

struct net_debug {
  u_int64_t flit;
  u_int8_t  vn;
  u_int64_t timestamp;
}__attribute__((packed));

struct MANGO_flit {
  u_int32_t blockaddr:26;
  u_int32_t cmd:6;
  u_int8_t  reg_or_id:6;
  u_int8_t  flit_type:2;
  u_int32_t dst_type:3;
  u_int32_t dst:9;
  u_int32_t src_type:3;
  u_int32_t src:9;
}__attribute__((packed));

struct CORE_debug_st {
  u_int8_t exception:1;
  u_int8_t interrupt:1;
  u_int32_t address:32;
  u_int32_t IRcode:32;
  u_int8_t writemem:1;
  u_int8_t writereg:1;
  u_int32_t data_to_mem:32;
  u_int32_t data_from_alu:32;
  u_int32_t data_from_mem:32;
  u_int8_t ku_bit:1;
  u_int32_t epc:32;
  u_int8_t dummy:3;
  u_int64_t timestamp;
}__attribute__((packed));

struct L1D_debug_st {
  u_int16_t message_type:6;
  u_int16_t line_state:5;
  u_int32_t address:32;
  u_int8_t sender_type:3;
  u_int16_t sender_id:9;
  u_int16_t sc_succ:1;
  u_int16_t num_acks_msg:6;
  u_int16_t num_acks_mshr:6;
  u_int64_t index2:7;
  u_int64_t index1:6;
  u_int64_t signature:64;
  u_int8_t  flags:7;
  u_int8_t error:1;
  u_int64_t timestamp;
}__attribute__((packed));

struct L2_debug_st {
  u_int16_t message_type:6;
  u_int16_t line_state:5;
  u_int32_t address:32;
  u_int8_t  sender_type:3;
  u_int16_t sender_id:9;
  u_int16_t  index2:7;
  u_int16_t  index1:6;
  u_int64_t signature_low:64;
  u_int64_t signature_high:8;
  u_int8_t flags:3;
  u_int16_t sharers:9;
  u_int8_t  error:1;
  u_int64_t timestamp;
}__attribute__((packed));

struct PEAK_flit {
  u_int32_t blockaddr:26;
  u_int32_t cmd:6;
  u_int32_t ack_or_reg:6;
  u_int32_t dst_type:3;
  u_int32_t dst:9;
  u_int32_t src_type:3;
  u_int32_t src:9;
  u_int32_t type:2;  
}__attribute__((packed));

struct MC_debug_st {
  u_int32_t address;
  u_int32_t word[16];
  u_int64_t timestamp;
}__attribute__((packed));

struct MC_info_st {
  u_int32_t word[16];
  u_int32_t address;
}__attribute__((packed));

struct TR_info_st {
  u_int32_t value;
  u_int16_t reg;
}__attribute__((packed));

struct MANGO_TR_debug_st {
  u_int32_t item_wr_data:32;
  u_int32_t item_wr_trdata:32;
  u_int8_t  item_wr_reg:5;
  u_int8_t  item_wr_req:1;
  u_int32_t item_rd_data:32;
  u_int8_t item_rd_reg:5;
  u_int8_t  item_rd_req:1;
  u_int32_t ni_wr_trdata:32;
  u_int32_t ni_wr_data:32;
  u_int8_t ni_wr_reg:5;
  u_int8_t  ni_wr_avail:1;
  u_int8_t  ni_wr_pending:1;
  u_int32_t ni_rd_data:32;
  u_int16_t ni_rd_dst:9;
  u_int8_t ni_rd_reg:5;
  u_int8_t  ni_rd_avail:1;
  u_int8_t  ni_rd_pending:1;
  u_int32_t ni_data:32;
  u_int16_t ni_dst:9;
  u_int8_t ni_reg:5;
  u_int8_t  ni_req:1;
  u_int64_t timestamp;
}__attribute__((packed));

// this structure is for item handler functions
typedef void (*handle_item_t)(unsigned int tile, unsigned int subtile, unsigned int cmd, unsigned char value); 
struct handler_item_s {
  handle_item_t handle; 
};

// this structure holds all peak stats
struct peak_stats_s {
  unsigned int flits_north_r0;
  unsigned int flits_north_r1;
  unsigned int flits_north_r2;

  unsigned int flits_south_r0;
  unsigned int flits_south_r1;
  unsigned int flits_south_r2;

  unsigned int flits_east_r0;
  unsigned int flits_east_r1;
  unsigned int flits_east_r2;

  unsigned int flits_west_r0;
  unsigned int flits_west_r1;
  unsigned int flits_west_r2;

  unsigned int flits_local_r0;
  unsigned int flits_local_r1;
  unsigned int flits_local_r2;

  unsigned int L1Ihits;
  unsigned int L1Dhits;
  unsigned int L2hits;

  unsigned int L1Imisses;
  unsigned int L1Dmisses;
  unsigned int L2misses;

  unsigned int Cycles;
  
  unsigned int InstrExec;
};

// this structure stores the different types of items received
struct data_s {
  unsigned char CONSOLE[10000];
  int CONSOLE_index;

  unsigned char STATS_info[STATS_SIZE];
  int STATS_info_index;
  unsigned int errors_stats;

  unsigned char TR_reading[TR_SIZE];
  unsigned int TR_reading_reg_val; 
  int TR_reading_index;

  unsigned char TR_debug[TR_DEBUG_SIZE];
  int TR_debug_index;

  unsigned char L1D_debug[L1D_DEBUG_SIZE];
  int L1D_debug_index;
  int L1D_entries_after_unmanaged_transaction;

  unsigned char L2_debug[L2_DEBUG_SIZE];
  int L2_debug_index;
  int L2_entries_after_unmanaged_transaction;

  unsigned char CORE_debug[CORE_DEBUG_SIZE];
  int CORE_debug_index;

  unsigned char L1D_BLOCK_debug[BLOCK_DEBUG_SIZE];
  int L1D_BLOCK_debug_index;

  unsigned char L2_BLOCK_debug[BLOCK_DEBUG_SIZE];
  int L2_BLOCK_debug_index;

  unsigned char MC_BLOCK_debug[BLOCK_DEBUG_SIZE];
  int MC_BLOCK_debug_index;

  // this is used for external tool memory readings
  unsigned char MC_BLOCK_info[BLOCK_DEBUG_SIZE];  
  int MC_BLOCK_info_index;

  unsigned char INJECT_debug[INJECT_DEBUG_SIZE];
  int INJECT_debug_index;

  unsigned char EJECT_debug[EJECT_DEBUG_SIZE];
  int EJECT_debug_index;

  struct peak_stats_s stats;
};

struct mango_data_s {
  unsigned char CONSOLE[10000];
  int CONSOLE_index;

  unsigned char UNIT_item[ITEM_ITEM_SIZE];
  int           UNIT_item_index;

  unsigned char MC_BLOCK_debug[BLOCK_DEBUG_SIZE];
  int MC_BLOCK_debug_index;

  // this is used for external tool memory readings
  unsigned char MC_BLOCK_info[BLOCK_DEBUG_SIZE];  
  int MC_BLOCK_info_index;

  unsigned char INJECT_debug[INJECT_DEBUG_SIZE];
  int INJECT_debug_index;

  unsigned char EJECT_debug[EJECT_DEBUG_SIZE];
  int EJECT_debug_index;

  unsigned char U2M_debug[U2M_DEBUG_SIZE];
  int U2M_debug_index;

  unsigned char M2U_debug[M2U_DEBUG_SIZE];
  int M2U_debug_index;

  unsigned char TR_debug[MANGO_TR_DEBUG_SIZE];
  int TR_debug_index;

  unsigned char TR_reading[TR_SIZE];
  unsigned int TR_reading_reg_val; 
  int TR_reading_index;
};

// typedef for pcap callback functions
typedef void (*callback_t)(u_char* args, const struct pcap_pkthdr *header, const u_char *packet);

// typedef for pthread functions
typedef void* (*pthread_func_t)(void *args);



// PROTOYPES
void end_program(int);
void show_help(char *prog);
void show_params();
void show_stats();
void show_live_capture_stats();
int  parse_arguments(int argc, char **argv);
int  check_supported_mode(int mode);

int init_capture_system();
int init_pcap_capture_system();
int init_hnlib_capture_system();
int init_sock_capture_system();
void init_data_structure();

void pcap_main();
void hnlib_main();


int socket_loop(int sock, callback_t func, void *args);
int hnlib_loop(FILE *f, callback_t cb, void *args);
int hnlib_loop_offline(FILE *f, callback_t cb, void *args);
int hnlib_loop_offline_infinity(FILE *handler, callback_t callback, void *args);
int pcap_loop_offline(pcap_t *handler, callback_t callback, void *args);

void free_hnlib_structures();
void free_pcap_structures();
void free_sock_structures();

void print_raw_packets(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void check_for_lost_packets(u_char *args, const struct pcap_pkthdr *header, const u_char *packet); 
void proccess_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void pcap_dump_with_flush(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void pcap_dump_rotate(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void hnlib_dump_rotate(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void hnlib_dump(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void hnlib_process_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
void* rotate_pcap_file(void *args);
void* show_stats_thread(void *args);
void* flush_pcap_file_thread(void *args);
void* rotate_hnlib_file(void *args);
void* flush_hnlib_file_thread(void *args);

void proccess_item(unsigned int tile, unsigned int cmd, unsigned char value); 
inline void print_raw_packet(int len, const unsigned char *packet);
inline void update_seq_number(const unsigned char payload[4]);

void print_L1D_debug(register unsigned int tile, register unsigned int subtile); 
void print_L2_debug(register unsigned int tile, register unsigned int subtile);
void print_CORE_debug(register unsigned int tile, register unsigned int subtile);
void print_BLOCK_debug(register unsigned int tile, register unsigned int subtile, register unsigned int type);
void print_INJECT_debug(register unsigned int tile, register unsigned int subtile);
void print_EJECT_debug(register unsigned int tile, register unsigned int subtile);
void print_TR_debug(register unsigned int tile, register unsigned int subtile);
void print_CONSOLE_info(register unsigned int tile, register unsigned int subtile);
void print_TR_reading(register unsigned int tile, register unsigned int subtile);
void print_PEAK_FLIT_debug (register char *inj_eje, register uint64_t timestamp, register unsigned int tile, register unsigned int subtile, register unsigned int vn, register struct PEAK_flit *st_flit, register FILE *fd);
void print_U2M_debug(register unsigned tile);
void print_M2U_debug(register unsigned tile);
void print_BLOCK_info(register unsigned int tile, register unsigned int subtile);
void print_MANGO_TR_debug(register unsigned int tile);

void handle_memory_reading_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value); 
void handle_debug_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value); 
void handle_console_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value);
void handle_raw_console_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value);
void handle_tr_reading_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value);
void handle_echo_and_notification_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value);
void handle_stats_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value);
void handle_mode_not_supported(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value);
void handle_raw_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value);

// some functions for thread scheduling configuration
int get_policy(char p, int *policy);
void display_sched_attr(int policy, struct sched_param *param);
void display_thread_sched_attr(pthread_t tid, FILE *fd, char *msg);
void display_thread_affinity(pthread_t tid, FILE *fd, char *msg); 


// GLOBAL VARIABLES
// program params
struct params pparams;

// encapsulates pcap structs
struct pcap_info_s pcap_info;

struct hnlib_info_s hnlib_info;

struct socket_s sock;

// keeps current number of processed packets
unsigned long captured_packets = 0;

// keeps the last frame sequence number received. Used for detecting lost frames
unsigned int seq_num_ant = 0;

// keeps the current number of lost frames
unsigned int num_lost_frames = 0;

unsigned int num_mshr_l1 = 0;

// all data info
#ifdef MANGO
struct data_s unit_data[NUMBER_OF_TILES][NUMBER_OF_SUBTILES];
struct mango_data_s mango_data[NUMBER_OF_TILES];
#else
struct data_s unit_data[1][NUMBER_OF_TILES];
#endif

// address headers for filtering packets when socket interface (no pcap) is used
// older one is {0xDA, 0x01, 0x02, 0x03, 0x04, 0x05}
struct eth_addresses_s eaddr = { {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, {0x5A, 0x01, 0x02, 0x03, 0x04, 0x05} };

int glbl_unmanaged_transaction = 0;

struct handler_item_s item_handlers[] = {
  {handle_console_item}, // mode 0 = console
  {handle_stats_item},   // mode 1 = stats
  {handle_debug_item},   // mode 2 = debug
  {handle_echo_and_notification_item}, // mode 3 = echo & notification
  {handle_raw_item},     // mode 4 = raw items
  {handle_stats_item},   // mode 5 = stats
  {handle_debug_item},   // mode 6 = debug (filtering hits)
  {NULL}, // mode 7 = pipe mem file, but not implemented
  {handle_stats_item},   // mode 8 = stats
  {NULL}, // mode 9  = stats, but not implemented. Timestamp was removed from stats
  {handle_memory_reading_item},// mode 10 = Memory readings from external tools
  {handle_raw_console_item},   // mode 11 = raw console
  {handle_tr_reading_item},    // mode 12 = TR readings
  {handle_debug_item}          // mode 13 = debug (writing to cyclic file)
};

// supported program modes, finish the vector with -1
int supported_modes[] = { 0, 1, 2, 3, 4, 5, 6, 8, 10, 11, 12, 13, -1}; 

char COMPONENT_TYPES[16][4] = {"L1D", "L2D", "MC", "L1I", "TR", "UNI", "NCA", "MS", 
                               "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A"};  // those last 8 positions can be used for new component types

// MANGO events
char mango_events[64][80] = { 
  "GET_TO_MC (0)      ",
  "not impl. (1)      ",
  "not impl. (2)      ",
  "not impl. (3)      ",
  "not impl. (4)      ",
  "not implemented (5)",
  "not implemented (6)",
  "not impl. (7)      ",
  "not impl. (8)      ",
  "not impl. (9)      ",
  "not impl. (10)     ",
  "not impl. (11)     ",
  "not impl. (12)     ",
  "not impl. (13)     ",
  "not impl. (14)     ",
  "not impl. (15)     ",
  "not impl. (16)     ",
  "not impl. (17)     ",
  "not impl. (18)     ",
  "not impl. (19)     ",
  "not impl. (20)     ",
  "not impl. (21)     ",
  "not impl. (22)     ",
  "not impl. (23)     ",
  "not impl. (24)     ",
  "not impl. (25)     ",
  "not impl. (26)     ",
  "not impl. (27)     ",
  "not impl. (28)     ",
  "not impl. (29)     ",
  "not impl. (30)     ",
  "not impl. (31)     ",
  "DATA_FROM_MC (32)  ",
  "not impl. (33)     ",
  "not impl. (34)     ",
  "not impl. (35)     ",
  "not impl. (36)     ",
  "not impl. (37)     ",
  "not impl. (38)     ",
  "not impl. (39)     ",
  "not impl. (40)     ",
  "not impl. (41)     ",
  "not impl. (42)     ",
  "not impl. (43)     ",
  "not impl. (44)     ",
  "not impl. (45)     ",
  "not impl. (46)     ",
  "not impl. (47)     ",
  "WB_DATA_TO_MC (48) ",
  "WORD_RD_FROM_MC(49)",
  "WORD_WR_TO_MC (50) ",
  "HALF_WR_TO_MC (51) ",
  "BYTE_WR_TO_MC (52) ",
  "not impl. (53)     ",
  "not impl. (54)     ",
  "not impl. (55)     ",
  "not impl. (56)     ",
  "not impl. (57)     ",
  "not impl. (58)     ",
  "not impl. (59)     ",
  "not impl. (60)     ",
  "not impl. (61)     ",
  "not impl. (60)     ",
  "not impl. (63)     "
};

// Debug info Metadata
char events[64][80] = { 
  "GET_TO_MC (0)      ",
  "FETCH_TO_MC (1)    ",
  "not impl. (2)      ",
  "not impl. (3)      ",
  "not impl. (4)      ",
  "not implemented (5)",
  "not implemented (6)",
  "not impl. (7)      ",
  "ACK (8)            ",
  "WBACK (9)          ",
  "HOME_SEARCH_ACK(10)",
  "RHM_ACK (11)       ",
  "HOME_UNBLOCK (12)  ",
  "not impl. (13)     ",
  "not impl. (14)     ",
  "not impl. (15)     ",
  "GETS (16)          ",
  "GETX (17)          ",
  "FWD_GETS (18)      ",
  "FWD_GETX (19)      ",
  "INV_TO_SHARERS (20)",
  "INV_REPL (21)      ",
  "INV_TO_OWNER (22)  ",
  "PUTS (23)          ",
  "HOME_SEARCH_S (24) ",
  "HOME_SEARCH_X (25) ",
  "not impl. (26)     ",
  "not impl. (27)     ",
  "not impl. (28)     ",
  "not impl. (29)     ",
  "not impl. (30)     ",
  "not impl. (31)     ",
  "DATA_FROM_MC (32)  ",
  "FETCH_FROM_MC (33) ",
  "REQ_WR_TILEREG (34)",
  "REQ_RD_TILEREG (35)",
  "RSP_WR_TILEREG (36)",
  "RSP_RD_TILEREG (37)",
  "HOME_FROM_MC (38)  ",
  "not impl. (39)     ",
  "DATA_SHARED (40)   ",
  "DATA_EXCLUSIVE (41)",
  "PUTX (42)          ",
  "ACCEPTS (43)       ",
  "not impl. (44)     ",
  "not impl. (45)     ",
  "not impl. (46)     ",
  "not impl. (47)     ",
  "WB_DATA_TO_MC (48) ",
  "STALL (49)     	",
  "UNSTALL (50)     	",
  "SC_WRITE_Failed(51)",
  "READ_WORD (52)     ",
  "WRITE_WORD (53)    ",
  "WRITE_HALF (54)    ",
  "WRITE_BYTE (55)    ",
  "not impl. (56)     ",
  "not impl. (57)     ",
  "not impl. (58)     ",
  "not impl. (59)     ",
  "not impl. (60)     ",
  "req_load (61)      ",
  "req_store (62)     ",
  "req_replacement(63)"
};


char states_L1[32][80] = 	{
  "I (0)         ",
  "S (1)         ",
  "E (2)         ",
  "M (3)         ",
  "IE (4)        ",
  "IM (5)        ",
  "IS (6)        ",
  "MI (7)        ",
  "EI (8)        ",
  "ISI (9)       ",
  "IMI (10)      ",
  "IMWA (11)     ",
  "ISWA (12)     ",
  "SWA (13)      ",
  "EWA (14)      ",
  "MWA (15)      ",
  "SWAI (16)     ",
  "MWAI (17)     ",
  "SM (18)       ",
  "SMWA (19)     ",
  "ES (20)       ",
  "IEWA (21)     ",
  "IMST (22)	",
  "SSST (23)	",
  "IMIMST (24)	",
  "SI (25)	",
  "not impl. (26)",
  "not impl. (27)",
  "not impl. (28)",
  "not impl. (29)",
  "not impl. (30)",
  "not impl. (31)"
};

char states_L2[32][80] = 	{
  "I (0)         ",
  "S (1)         ",
  "P (2)         ",
  "C (3)         ",
  "IP (4)        ",
  "PS (5)        ",
  "PI (6)        ",
  "IR (7)        ",
  "IPW (8)       ",
  "IPR (9)       ",
  "PW (10)       ",
  "SP (11)       ",
  "BS (12)       ",
  "BP (13)       ",
  "BPS (14)      ",
  "BPS1 (15)     ",
  "PS2 (16)	 ",
  "SSST (17)	 ",
  "IPread (18)	 ",
  "PSP (19)	 ",
  "PP (20)       ",
  "not impl. (21)",
  "not impl. (22)",
  "not impl. (23)",
  "not impl. (24)",
  "not impl. (25)",
  "not impl. (26)",
  "not impl. (27)",
  "not impl. (28)",
  "not impl. (29)",
  "not impl. (30)",
  "not impl. (31)"
};


volatile sig_atomic_t end_flag = 0;

int main(int argc, char **argv) {
  int status;
 
  status = parse_arguments(argc, argv);
  if (status == 0) {
    return 1;
  } else if (status < 0) {
    return 2;
  }

  if (pparams.verbose == 1) {
    show_params();
  }

  if (check_supported_mode(pparams.mode) == 0) {
    fprintf(stderr, "Mode %d not supported.\n", pparams.mode);
    show_help(argv[0]);
    return 2;
  }

  if ((pparams.use_pcap == 1) && (pparams.use_hnlib == 1)) {
    fprintf(stderr, "pcap and hnlib working modes enabled, but only permited one at a time\n");
    return 2;
  }

  if (pparams.mode == 13) {
    if ((pparams.fd_m13 = fopen(pparams.m13fname, "w")) == NULL) {
      fprintf(stderr, "Couldn't open %s debug file for mode 13", pparams.m13fname);
      return 2;
    }
  } 

 
  status = init_capture_system();
  if (status == -1) {
    // capture system pcap failed
    free_pcap_structures();
    return 2;
  } else if (status == -2) {
    // capture system socket ethernet failed
    free_sock_structures();
    return 2;
  } else if (status == -3) {
    // hnlib initialize failed
    free_hnlib_structures();
    return 2;
  }

  if (pparams.verbose == 1) {
    char msg[256];
    if (pparams.live_capture == 1) {
      strcpy(msg, "live capture thread");
    } else {
      strcpy(msg, "file capture thread");
    }

    display_thread_sched_attr(pthread_self(), stderr, msg);
    display_thread_affinity(pthread_self(), stderr, msg);

    if ((pparams.rotate == 1) && (pparams.live_capture == 1) && (pparams.use_pcap == 1)) {
      display_thread_sched_attr(pcap_info.rotate_tid, stderr, "rotate pcap file thread");
      display_thread_affinity(pcap_info.rotate_tid, stderr, "rotate pcap file thread");
    }

    if ((pparams.rotate == 1) && (pparams.live_capture == 1) && (pparams.use_hnlib == 1)) {
      display_thread_sched_attr(hnlib_info.rotate_tid, stderr, "rotate hnlib file thread");
      display_thread_affinity(hnlib_info.rotate_tid, stderr, "rotate hnlib file thread");
    }
  }

  init_data_structure();

  signal(SIGINT, end_program);

  if (pparams.use_pcap == 1) {
    // using pcap library
    pcap_main();
  } else if (pparams.use_hnlib == 1) {
    // using hn library
    hnlib_main();
  } else {
    // original socket capture
    socket_loop(sock.fd, proccess_packet, NULL);
  }

  // waits for the rotating pcacp file thread
  if (pparams.live_capture == 1) {
    if (pparams.use_pcap == 1) {
      pthread_kill(pcap_info.show_stats_tid, SIGINT);
      pthread_kill(pcap_info.flush_pcapf_tid, SIGINT);
      pthread_join(pcap_info.show_stats_tid, NULL);
      pthread_join(pcap_info.flush_pcapf_tid, NULL);
    
      if (pparams.rotate == 1) {
        pthread_join(pcap_info.rotate_tid, NULL);
      }
    }

    if (pparams.use_hnlib == 1) {
      pthread_kill(hnlib_info.show_stats_tid, SIGINT);
      pthread_kill(hnlib_info.flush_tid, SIGINT);
      pthread_join(hnlib_info.show_stats_tid, NULL);
      pthread_join(hnlib_info.flush_tid, NULL);
    
      if (pparams.rotate == 1) {
        pthread_join(hnlib_info.rotate_tid, NULL);
      }
    }
  }

  signal(SIGINT, SIG_DFL);

  show_stats();

  if (pparams.use_pcap == 1) {
    free_pcap_structures();
  } else if (pparams.use_hnlib == 1) {
    free_hnlib_structures();
  } else {
    free_sock_structures();
  }

  return 0;
}


void pcap_main() {
  int status;

  if (pparams.live_capture == 1) {
    if (pparams.rotate == 1) {
      // save captured frames to file with file rotating scheme
      status = pcap_loop(pcap_info.handler, -1, pcap_dump_rotate, (u_char *)pcap_info.dumper);
    } else {
      // save captured frames to file without rotating it. The most efficient, but disk consuming
      status = pcap_loop(pcap_info.handler, -1, pcap_dump, (u_char *)pcap_info.dumper);
    }
    if (status == -1) {
      fprintf(stderr, "pcap_loop error in live capture: %s\n", pcap_geterr(pcap_info.handler));
    }
  } else if (pparams.offline == 1) {
    // proccess the given pcap file offline and ends
    if (pparams.rotate == 1) {
      do {
        if (pcap_info.handler != NULL) {
          //fprintf(stderr, "processing file %s\n", pparams.livecapfname[pparams.current_livecap_file_index]);
          pcap_loop(pcap_info.handler, -1, proccess_packet, NULL);
        }

        pparams.current_livecap_file_index = (pparams.current_livecap_file_index + 1) % NUM_ROTATE_FILES;
        char errbuf[PCAP_ERRBUF_SIZE];
        pcap_info.handler = pcap_open_offline(pparams.livecapfname[pparams.current_livecap_file_index], errbuf);
        if (pcap_info.handler == NULL) {
          fprintf(stderr, "Couldn't open file %s: %s. Trying next\n", pparams.livecapfname[pparams.current_livecap_file_index], errbuf);
        }
      } while (pparams.current_livecap_file_index != pparams.offline_first_file_index);
    } else {
      pcap_loop(pcap_info.handler, -1, proccess_packet, NULL);
    }
  } else {
    // proccess the given pcap file offline and still waiting for more frames
    pcap_loop_offline(pcap_info.handler, proccess_packet, NULL);
  }
}



int pcap_loop_offline(pcap_t *handler, callback_t callback, void *args) {
  struct pcap_pkthdr pkt_hdr;
  struct fpga_pkt_header_s *fpga_pkt_hdr; 
  u_char pkt[BUFSIZE];
  fd_set rdfs;
  int fd_pcap = pcap_get_selectable_fd(pcap_info.handler);

  if (fd_pcap == -1) {
    fprintf(stderr, "error getting select fd id. %s\n", pcap_geterr(pcap_info.handler));
    return -1;
  }

  lseek (fd_pcap, 0, SEEK_SET);



  int status;
  int offset = 0;
  int reading_glbl_header = 1;
  int reading_header = 0;
  int payload_len = 0;
  int nread_payload = 0;
  int packet_completed = 0;
  int glbl_header_len = PCAP_FILE_HEADER_SIZE;  // first it is 40 and then, we are considering the last 16 bytes of the packet (we have to discard them) as other global header
  do {
    FD_ZERO(&rdfs);
    FD_SET(fd_pcap, &rdfs);
    status = select(fd_pcap+1, &rdfs, NULL, NULL, NULL);

    if (end_flag == 1) {
      // return due to end program signal
      return -2;
    }

    if (status > 0) {
      // data available in file
      if (reading_glbl_header) {
        int to_read = glbl_header_len - offset;
        size_t nread = read(fd_pcap, &(pkt[offset]), to_read * sizeof(u_char));
        offset += nread;
        //print_raw_packet(offset, pkt);
        if ((to_read - nread) == 0) {
          // header read
          reading_header = 1;
          reading_glbl_header = 0;
          offset = 0;
        }
      } else if (reading_header) {
        int to_read = sizeof(struct fpga_pkt_header_s) - offset;
        size_t nread = read(fd_pcap, &(pkt[offset]), to_read * sizeof(u_char));
        offset += nread;
        //print_raw_packet(offset, pkt);

        if ((to_read - nread) == 0) {
          // header read
          reading_header = 0;
          nread_payload = 0;
          fpga_pkt_hdr = (struct fpga_pkt_header_s *)pkt;
          payload_len = ntohs(fpga_pkt_hdr->payload_len);
          //fprintf(stdout, "payload_len = %d\n", payload_len);
        }
      } else {
        // we are reading payload
        int to_read = payload_len-nread_payload; 
        size_t nread = read(fd_pcap, &(pkt[offset]), sizeof(u_char) * to_read);
        nread_payload += nread;
        offset += nread;
        //print_raw_packet(offset, pkt);
        if ((to_read-nread) == 0) {
          //packet read complete
          reading_glbl_header = 1;
          packet_completed = 1;
          offset = 0;
          glbl_header_len = 16;
        }
      }

      if (packet_completed) {
        packet_completed = 0;
        pkt_hdr.len    = payload_len + sizeof(struct fpga_pkt_header_s);
        pkt_hdr.caplen = payload_len + sizeof(struct fpga_pkt_header_s);
        callback(args, &pkt_hdr, pkt);
      }
    } 
  } while (end_flag == 0);

  return 0;
}



int socket_loop(int s, callback_t callback, void *args) {
  int status;
  int pkt_size;
  unsigned char packet[BUFSIZE];
  struct pcap_pkthdr hdr;
  fd_set rdfs;

 
  do {
    FD_ZERO(&rdfs);
    FD_SET(s, &rdfs);
    status = select(s+1, &rdfs, NULL, NULL, NULL);

    if (end_flag == 1) {
      // return due to end program signal
      return -2;
    }

    if (status > 0) {
      pkt_size = recv(s, packet, BUFSIZE, 0);
      //fprintf(stdout, "pkt_size: %d\n", pkt_size);
      //print_raw_packet(pkt_size, packet);
      if (pkt_size > 0) {
        // something received
        hdr.len    = pkt_size;
        hdr.caplen = pkt_size;
        if (memcmp(packet, &eaddr, sizeof(struct eth_addresses_s)) == 0) {
          callback(args, &hdr, packet);
        }
      }     
    } else if (status < 0) {
      // error found
      if (errno != EINTR) {
        fprintf(stderr, "socket error: %s\n", strerror(errno));
        return -1;
      }
    } 
  } while (end_flag == 0);

  return 0;
}


void hnlib_main() {
  int status = 0;

  if (pparams.live_capture == 1) {
    if (strcmp(pparams.devname, "hn") == 0) { 
      // capture anything from hnlib socket and dump it to file
      if (pparams.rotate == 1) {
        status = hnlib_loop(hnlib_info.handler, hnlib_dump_rotate, NULL);
      } else {
        status = hnlib_loop(hnlib_info.handler, hnlib_dump, NULL);
      }
    } else {
      // capturing anything from hnlib socket and processing it directly
      status = hnlib_loop(NULL, hnlib_process_packet, NULL);
    }
    
    if (status == -1) {
      fprintf(stderr, "hnlib_loop err in live capture\n");
    }
  } else if (pparams.offline == 1) {
    // process the hnlib file and ends
    if (pparams.rotate == 1) {
      do {
        if (hnlib_info.handler != NULL) {
          hnlib_loop_offline(hnlib_info.handler, hnlib_process_packet, NULL);
        }
        pparams.current_livecap_file_index = (pparams.current_livecap_file_index + 1) % NUM_ROTATE_FILES;
        hnlib_info.handler = fopen(pparams.livecapfname[pparams.current_livecap_file_index], "rb");
        if (hnlib_info.handler == NULL) {
          fprintf(stderr, "Couldn't open file %s: %s. Trying next\n", pparams.livecapfname[pparams.current_livecap_file_index], strerror(errno));
        }
      } while (pparams.current_livecap_file_index != pparams.offline_first_file_index);
    } else {
      hnlib_loop_offline(hnlib_info.handler, hnlib_process_packet, NULL);
    }
  } else {
    // dump it with live capture and processing it offline, but
    // w/o finishing with eof
    hnlib_loop_offline_infinity(hnlib_info.handler, hnlib_process_packet, NULL);
  }
}


int hnlib_loop(FILE *handler, callback_t callback, void *args) {
  int      status;
  struct   pcap_pkthdr hdr;
  uint32_t read_items = 0;

  do {
    status = hn_receive_items(hnlib_info.items, HN_MAX_ITEMS_TO_READ_FROM_HN_SOCKET, &read_items);
    hnlib_info.stats.total_items += read_items;
    
    if (end_flag == 1) {
      return -2;
    }
    if (status != HN_SUCCEEDED) {
      fprintf(stderr, "hnlib error (%d)\n", status);
      return -1;
    }

    if (read_items > 0) {
      hdr.len = read_items * sizeof(*hnlib_info.items);
      hdr.caplen = hdr.len;
      callback(args, &hdr, (unsigned char *)&hnlib_info.items);
    }
  } while (end_flag == 0);

  return 0;
}

int hnlib_loop_offline(FILE *handler, callback_t callback, void *args) {
  size_t   ir;
  uint32_t items[HN_MAX_ITEMS_TO_READ_FROM_HN_FILE];
  struct   pcap_pkthdr hdr;

  ir = fread(items, sizeof(*items), HN_MAX_ITEMS_TO_READ_FROM_HN_FILE, handler);
  hnlib_info.stats.total_items += ir;
  while ((end_flag == 0) && (ir > 0)) {
    hdr.len    = ir*sizeof(*items);
    hdr.caplen = hdr.len;

    callback(args, &hdr, (unsigned char *)items);

    ir = fread(items, sizeof(*items), HN_MAX_ITEMS_TO_READ_FROM_HN_FILE, handler);
    hnlib_info.stats.total_items += ir;
  } 

  return 0;
}

int hnlib_loop_offline_infinity(FILE *handler, callback_t callback, void *args) {
  int      status;
  size_t   ir;
  uint32_t items[HN_MAX_ITEMS_TO_READ_FROM_HN_FILE];
  struct   pcap_pkthdr hdr;
  int fno;
  fd_set rdfs;
  
  fno = fileno(handler);
  lseek (fno, 0, SEEK_SET);
  do {
    FD_ZERO(&rdfs);
    FD_SET(fno, &rdfs);
    status = select(fno+1, &rdfs, NULL, NULL, NULL);

    if ((status > 0)) {
      ir = fread(items, sizeof(*items), HN_MAX_ITEMS_TO_READ_FROM_HN_FILE, handler);

      if (ir > 0) {
        hnlib_info.stats.total_items += ir;

        hdr.len    = ir*sizeof(*items);
        hdr.caplen = hdr.len;
        callback(args, &hdr, (unsigned char *)items);
      } 
    }
  } while (end_flag == 0);

  return 0;
}

void* rotate_hnlib_file(void *args) {
  sigset_t set;


  // Do not care of SIGINT, the main thread will do it
  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGUSR1); 
  sigaddset(&set, SIGUSR2); 
  pthread_sigmask(SIG_BLOCK, &set, NULL);

  register pthread_mutex_t *file_mutex = &(hnlib_info.file_mutex);
  do {
    sleep(3);

    register long current_file_size = ftell(hnlib_info.handler);
    if (current_file_size > pparams.rotate_file_size) {
      pparams.current_livecap_file_index = (pparams.current_livecap_file_index + 1) % NUM_ROTATE_FILES;

      register FILE *df_new;
      register FILE *df_curr = hnlib_info.handler;
      if ((df_new = fopen(pparams.livecapfname[pparams.current_livecap_file_index], "wb")) == NULL) {
        fprintf(stderr, "Couldn't open %s: %s", pparams.livecapfname[pparams.current_livecap_file_index], strerror(errno));
        end_program(SIGINT);
      }

      pthread_mutex_lock(file_mutex);
      fflush(df_curr);
      hnlib_info.handler = df_new;
      pthread_mutex_unlock(file_mutex);

      if (df_curr != NULL) {
        fclose(df_curr);
      }
     }
  } while (end_flag == 0);

  return NULL;
}

void* flush_hnlib_file_thread(void *args) {
  int sig, status;
  sigset_t set, blocked_set;

  // Do not care of SIGINT, the main thread will do it
  sigemptyset(&blocked_set);
  sigaddset(&blocked_set, SIGUSR1); 
  pthread_sigmask(SIG_BLOCK, &blocked_set, NULL);

  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGUSR2); 
  do {
    status = sigwait(&set, &sig);
    if ((status == 0) && (sig == SIGUSR2)) {
      fprintf(stdout, "\nflushing to %s\n", pparams.livecapfname[pparams.current_livecap_file_index]);
      pthread_mutex_lock(&hnlib_info.file_mutex);
      fflush(hnlib_info.handler);
      pthread_mutex_unlock(&hnlib_info.file_mutex);
    }
  } while((end_flag == 0) && (sig != SIGINT));

  return NULL;
}

void hnlib_dump_rotate(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  register pthread_mutex_t *file_mutex = &(hnlib_info.file_mutex);

  pthread_mutex_lock(file_mutex);
  register size_t l = header->len;
  register size_t s = fwrite(packet, 1, header->len, hnlib_info.handler);
  if (s < l) {
    fprintf(stderr, "hnlib_loop callback function: req_to_write=%zu written=%zu\n", l, s);
    raise(SIGINT);
  }
  pthread_mutex_unlock(file_mutex);
}

void hnlib_dump(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  register size_t l = header->len;
  register size_t s = fwrite(packet, 1, header->len, hnlib_info.handler);
  if (s < l) {
    fprintf(stderr, "hnlib_loop callback function: req_to_write=%lu written=%zu\n", l, s);
    raise(SIGINT);
  }
}

void hnlib_process_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  //print_raw_packet(header->len, packet);


  // getting items
  size_t offset = 0;
  while (offset < header->len) {
    struct item_host_st *item = (struct item_host_st *)&(packet[offset]);

    proccess_item((unsigned int)item->tile, (unsigned int)item->cmd, (unsigned char)item->payload);

    offset += sizeof(struct item_host_st);
  } 
}


void print_raw_packet(int len, const unsigned char *packet) {
   int i;
   for (i = 0; i < len; i++) {
    printf("%02X", packet[i]);
  }
  printf("\n\n");
}





void proccess_packet(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  ++captured_packets;

  //print_raw_packet(header->len, packet);

  if (header->len > 47) { //17) {
    const u_char seq_number[4] = { packet[44], packet[45], packet[46], packet[47] };
    update_seq_number(seq_number);
  }

  // getting items
  unsigned int tile, cmd;
  unsigned char value;
  bpf_u_int32 offset = 48; //18;
  while (offset < header->len) {
    struct item_net_st *item = (struct item_net_st *)&(packet[offset]);
    tile = (((unsigned int)item->tile_cmd[0]) << 2) + (unsigned int)((item->tile_cmd[1] & 0xC0) >> 6);
    cmd  = (unsigned int)(item->tile_cmd[1] & 0x3F);
    value = (unsigned char)item->payload;

//    fprintf(stdout, "[INFO] raw_item: %x item: %x offset: %u, tile: %08X, cmd: %02X, payload: %02X\n", packet[offset], item, offset, tile, cmd, value);
    proccess_item(tile, cmd, value);

    offset += sizeof(struct item_net_st);
  } 
}



void print_raw_packets(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  print_raw_packet(header->len, packet);
}



void check_for_lost_packets(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  if (header->len > 47) {
    const u_char seq_number[4] = { packet[14], packet[15], packet[16], packet[17] };
    update_seq_number(seq_number);
  }
}




void pcap_dump_with_flush(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  ++captured_packets;

  pcap_dump(args, header, packet);
  pcap_dump_flush((pcap_dumper_t *)args);
}


void pcap_dump_rotate(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
  register pthread_mutex_t *pcap_file_mutex = &(pcap_info.pcap_file_mutex);
  ++captured_packets;

  pthread_mutex_lock(pcap_file_mutex);
  pcap_dump((u_char *)pcap_info.dumper, header, packet);
  pthread_mutex_unlock(pcap_file_mutex);
}


void* rotate_pcap_file(void *args) {
  sigset_t set;


  // Do not care of SIGINT, the main thread will do it
  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGUSR1); 
  sigaddset(&set, SIGUSR2); 
  pthread_sigmask(SIG_BLOCK, &set, NULL);

  register pthread_mutex_t *pcap_file_mutex = &(pcap_info.pcap_file_mutex);
  do {
    sleep(3);

    register long pcap_current_file_size = pcap_dump_ftell(pcap_info.dumper);
    if (pcap_current_file_size > pparams.rotate_file_size) {
      pparams.current_livecap_file_index = (pparams.current_livecap_file_index + 1) % NUM_ROTATE_FILES;

      register pcap_dumper_t *df_new;
      register pcap_dumper_t *df_curr = pcap_info.dumper;
      if ((df_new = pcap_dump_open(pcap_info.handler, pparams.livecapfname[pparams.current_livecap_file_index])) == NULL) {
        fprintf(stderr, "Couldn't open %s: %s", pparams.livecapfname[pparams.current_livecap_file_index], pcap_geterr(pcap_info.handler));
        end_program(SIGINT);
      } else {
        pcap_dump_flush(df_new);
      }

      FILE *fd = pcap_dump_file(df_new);
      setbuffer(fd, pcap_info.buf_file, pparams.live_capture_file_block_size);

      pthread_mutex_lock(pcap_file_mutex);
      pcap_info.dumper = df_new;

      pthread_mutex_unlock(pcap_file_mutex);

      if (df_curr != NULL) {
        pcap_dump_close(df_curr);
      }
     }
  } while (end_flag == 0);

  return NULL;
}


void* show_stats_thread(void *args) {
  int sig, status;
  sigset_t set, blocked_set;

  // Do not care of SIGINT, the main thread will do it
  sigemptyset(&blocked_set);
  sigaddset(&blocked_set, SIGUSR2); 
  pthread_sigmask(SIG_BLOCK, &blocked_set, NULL);

  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGUSR1); 
  do {
    status = sigwait(&set, &sig);
    if ((status == 0) && (sig == SIGUSR1)) {
      show_stats();
    }
  } while((end_flag == 0) && (sig != SIGINT));

  return NULL;
}


void* flush_pcap_file_thread(void *args) {
  int sig, status;
  sigset_t set, blocked_set;

  // Do not care of SIGINT, the main thread will do it
  sigemptyset(&blocked_set);
  sigaddset(&blocked_set, SIGUSR1); 
  pthread_sigmask(SIG_BLOCK, &blocked_set, NULL);

  sigemptyset(&set);
  sigaddset(&set, SIGINT);
  sigaddset(&set, SIGUSR2); 
  do {
    status = sigwait(&set, &sig);
    if ((status == 0) && (sig == SIGUSR2)) {
      fprintf(stdout, "\nflushing to %s\n", pparams.livecapfname[pparams.current_livecap_file_index]);
      pcap_dump_flush(pcap_info.dumper);
    }
  } while((end_flag == 0) && (sig != SIGINT));

  return NULL;
}



void proccess_item(unsigned int tile, unsigned int cmd, unsigned char value) {
  unsigned int subtile = NOT_APPLICABLE;

  if ((tile >= 0x3FF) && (cmd == 0x3F) && (value == 0xFF)) {
    //it is a padding item
    //fprintf(stderr, "item received: tile %04X (%04d), command %02X (%02d), value %02X\n", tile, tile, cmd, cmd, value);
    return;
  }

  if (tile > 511) {
    fprintf(stderr, "ERROR: incorrect tile tile=%04X (%u) cmd=%02X value=%02X\n", tile, tile, cmd, value);
    //fprintf(stderr, "item received: tile %04X (%04d), command %02X (%02d), value %02X\n", tile, tile, cmd, cmd, value);
    //raise(SIGINT);
    return;
  }

  if ((pparams.tile != 999) && (pparams.tile != tile)) {
    // not for me
    return;
  }


#ifdef MANGO
  if (cmd == COMMAND_UNIT_DATA_FIRST) {
    register int *index = &(mango_data[tile].UNIT_item_index);
 
    if (*index != 0) {
      fprintf(stderr, "Detected incomplete UNIT ITEM for tile %d\n", tile);
    }

    mango_data[tile].UNIT_item[0] = value;
    *index = 1;
    return; 

  } else if (cmd == COMMAND_UNIT_DATA_REMAINING) {
    register int *index = &(mango_data[tile].UNIT_item_index);
    mango_data[tile].UNIT_item[*index] = value;
    
    if (*index < (ITEM_ITEM_SIZE -1)) {
      *index += 1;
      return;
    } else {
      *index = 0;

      register struct unit_item_st *item = (struct unit_item_st *) &(mango_data[tile].UNIT_item[0]);
      subtile = (unsigned int)item->tile;
      cmd     = (unsigned int)item->cmd;
      value   = (unsigned char)item->payload;

      if ((subtile == 0x3FFFF) && (cmd == 0x3F) && (value == 0xFF)) {
        //it is a padding item
        //fprintf(stderr, "item received: tile %04X (%04d), command %02X (%02d), value %02X\n", tile, tile, cmd, cmd, value);
        return;
      }

      if (subtile > 511) {
        fprintf(stderr, "ERROR: incorrect UNIT tile %u for MANGO Tile %u\n", subtile, tile);
        //fprintf(stderr, "item received: tile %04X (%04d), command %02X (%02d), value %02X\n", tile, tile, cmd, cmd, value);
        //raise(SIGINT);
        return;
      }

      if ((pparams.subtile != 999) && (pparams.subtile != subtile)) {
        // not for me
        return;
      }
    }
  }
#else
  subtile = tile;
  tile    = 0;
#endif  

  handle_item_t handle = item_handlers[pparams.mode].handle;
  if (handle != NULL) {
    handle(tile, subtile, cmd, value);
  }
}






void print_L1D_debug(register unsigned int tile, register unsigned int subtile) {
  register int mode = pparams.mode;
//  register unsigned char *data = &(unit_data[tile][subtile].L1D_debug[0]);

  register struct L1D_debug_st *l1d = (struct L1D_debug_st *) &(unit_data[tile][subtile].L1D_debug[0]);


//  unsigned char event = data[0] & 0x3F;

   if ((mode == 6) &&
          (((l1d->line_state==2) && (l1d->message_type==61)) ||  // Load on E
           ((l1d->line_state==2) && (l1d->message_type==62)) ||  // Store on E
           ((l1d->line_state==3) && (l1d->message_type==61)) ||  // Load on M
           ((l1d->line_state==3) && (l1d->message_type==62)) ||  // Store on M
           ((l1d->line_state==1) && (l1d->message_type==61)))) {  // Load on S
     // filtering hits 
    return;
  }


  if (glbl_unmanaged_transaction == 0) {
    // we only print info if the controller is not blocked
    FILE *fd = stdout;
    if (mode == 13) { 
      fd = pparams.fd_m13;
      pparams.line_m13++;
    }

    if (l1d->signature & 0x80000000000000) num_mshr_l1++;
    if (l1d->signature & 0x40000000000000) num_mshr_l1--;

    fprintf(fd, "Cycle: %016lu [L1D  M_%2u  T_%2u]  %sAddr: %08x BlockAddr: %08x St:%s  Ev:%s Sender:%4d (%4s) Input Filter: %03x ACKs_MSHR:%d ACKs_MSG:%d SCsucc:%d index1:%d index2:%d, Output: %016lx %d\n",
          l1d->timestamp,
          tile,
          subtile,
          l1d->error ? "UNMANAGED_TRANSACTION\t" : "",  
          l1d->address,
          l1d->address & 0xFFFFFFC0,
          states_L1[l1d->line_state],
          events[l1d->message_type],
          l1d->sender_id,
          l1d->sender_type==0?"L1D":l1d->sender_type==1?"L2":l1d->sender_type==2?"MC":
          l1d->sender_type==3?"L1I":l1d->sender_type==4?"TR":"CORE",
          l1d->flags, l1d->num_acks_mshr, l1d->num_acks_msg, l1d->sc_succ, l1d->index1, l1d->index2,
          l1d->signature,
          num_mshr_l1);
  }

  if ((l1d->error == 1) && (glbl_unmanaged_transaction == 0)) {
    fprintf(stderr, "Detected UNMANAGED TRANSACTION in L1D  M_%2u  T_%2u\n", tile, subtile);

    //glbl_unmanaged_transaction = 1;

    FILE *fd = fopen("tr.lock", "w");
    fclose(fd);
  }

}

void print_L2_debug(register unsigned int tile, register unsigned int subtile) {
  register int mode = pparams.mode;
  //register unsigned char *data = &(unit_data[tile][subtile].L2_debug[0]);

  register struct L2_debug_st *l2d = (struct L2_debug_st *) &(unit_data[tile][subtile].L2_debug[0]);

  if (glbl_unmanaged_transaction == 0) {
    // we only print info when the controller is not blocked
   FILE *fd = stdout;
   if (mode == 13) { 
     fd = pparams.fd_m13;
     pparams.line_m13++;
   }
   
   fprintf(fd, "Cycle: %016lu [L2D  M_%2u  T_%2u]  %sAddr: %08x BlockAddr: %08x St:%s  Ev:%s Sender:%4d (%4s) Input Filter: %02X, NSharers: %03d,  index1: %d index2: %d, Output: %02x%016lx\n",
          l2d->timestamp, 
          tile, 
          subtile, 
          l2d->error? "UNMANAGED_TRANSACTION\t" : "",
          l2d->address,
          l2d->address & 0xFFFFFFC0,
          states_L2[l2d->line_state],
          events[l2d->message_type],
          l2d->sender_id,
          l2d->sender_type==0?"L1D":l2d->sender_type==1?"L2":l2d->sender_type==2?"MC":
          l2d->sender_type==3?"L1I":l2d->sender_type==4?"TR":"CORE",
          l2d->flags,
          l2d->sharers,
          l2d->index1,
          l2d->index2,
          l2d->signature_high,
          l2d->signature_low);
  }

  if ((l2d->error == 1) && (glbl_unmanaged_transaction == 0)) {
    fprintf(stderr, "Detected UNMANAGED TRANSACTION in L2   M_%2u  T_%2u\n", tile, subtile);

    //glbl_unmanaged_transaction = 1;

    FILE *fd = fopen("tr.lock", "w");
    fclose(fd);
  }
}


void print_CORE_debug(register unsigned int tile, register unsigned int subtile) {
  register int mode = pparams.mode;
  //register unsigned char *data = &(unit_data[tile][subtile].CORE_debug[0]);
  
  register struct CORE_debug_st *cd = (struct CORE_debug_st *) &(unit_data[tile][subtile].CORE_debug[0]);


  FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }

  fprintf(fd, "Cycle: %016lu [CR   M_%2u  T_%2u]  Addr: %08x IR: %08x int: %01x excep: %01x write_mem: %01x write_reg: %01x data: %08x dat_fmem: %08x dat_tmem: %08x ku_bit: %1x ALU: %08x\n",
        cd->timestamp, tile, subtile, cd->address, cd->IRcode, cd->interrupt, cd->exception, cd->writemem, cd->writereg, cd->data_from_alu, cd->data_from_mem, cd->data_to_mem, cd->ku_bit, cd->epc);
}



void print_BLOCK_debug(register unsigned int tile, register unsigned int subtile, register unsigned int type) {
  register int mode = pparams.mode;
  register unsigned char *data;

#ifdef MANGO
  data = &(mango_data[tile].MC_BLOCK_debug[0]);
#else
  if (type == L1D_TYPE) {
   data = &(unit_data[tile][subtile].L1D_BLOCK_debug[0]);
  } else if (type == L2_TYPE) {
   data = &(unit_data[tile][subtile].L2_BLOCK_debug[0]);
  } else if (type == MC_TYPE) {
   data = &(unit_data[tile][subtile].MC_BLOCK_debug[0]);
  } else {
    // non supported type
    fprintf(stderr, "Unsupported BLOCK debug type %d\n", type);
    return;
  }

#endif

  register struct MC_debug_st *mc_block = (struct MC_debug_st *)data;

  FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }

#ifdef MANGO  
  fprintf(fd, "Cycle: %016lu [%-3s  M_%2u  T_  ]  Addr: %08x Block content: %08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x\n",
#else
  fprintf(fd, "Cycle: %016lu [%-3s  M_%2u  T_%2u]  Addr: %08x Block content: %08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x\n",
#endif      
        mc_block->timestamp,
        COMPONENT_TYPES[type],
        tile, 
#ifndef MANGO        
        subtile,
#endif        
       mc_block->address,
       mc_block->word[15], mc_block->word[14], mc_block->word[13], mc_block->word[12], mc_block->word[11], mc_block->word[10], mc_block->word[9], mc_block->word[8],
       mc_block->word[7], mc_block->word[6], mc_block->word[5], mc_block->word[4], mc_block->word[3], mc_block->word[2], mc_block->word[1], mc_block->word[0]);
} 


void print_PEAK_FLIT_debug (register char *inj_eje, register uint64_t timestamp, register unsigned int tile, register unsigned int subtile, register unsigned int vn, register struct PEAK_flit *st_flit, register FILE *fd) {
//                        struct PEAK_flit {
//                          u_int32_t blockaddr:26;
//                          u_int32_t cmd:6;
//                          u_int32_t ack_or_reg:6
//                            u_int32_t dst_type:3;
//                          u_int32_t dst:9;
//                          u_int32_t src_type:3;
//                          u_int32_t src:9;
//                          u_int32_t type:2;  
//                        }__attribute__((packed, aligned(8)));
//

  fprintf(fd, "Cycle: %016lu [%3s  M_%2u  T_%2u]  VN: %02u, Flit(MSG_TYPE: %02u, DST: %03u (%3s), SRC: %03u (%3s), BA: %08x, CMD: %s, ACK: %02u   |  REG: %02u, DATA: %08x)\n", 
      timestamp, 
      inj_eje,
      tile,
      subtile,
      vn,
      st_flit->type,
      st_flit->dst,
      COMPONENT_TYPES[st_flit->dst_type],
      st_flit->src,
      COMPONENT_TYPES[st_flit->src_type],
      st_flit->blockaddr << 6,
      events[st_flit->cmd],
      st_flit->ack_or_reg & 0x3F,
      (st_flit->ack_or_reg & 0x3E) >> 1,
      st_flit->cmd << 26 | st_flit->blockaddr);

}

void print_INJECT_debug(register unsigned int tile, register unsigned int subtile) {
  register int mode = pparams.mode;
  register unsigned char *data;

#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    data = &(mango_data[tile].INJECT_debug[0]);
  } else {
    data = &(unit_data[tile][subtile].INJECT_debug[0]);
  }
#else
  data = &(unit_data[tile][subtile].INJECT_debug[0]);
#endif  

  struct net_debug *st_net   = (struct net_debug *)data;

  FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }


#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    register struct MANGO_flit *st_flit = (struct MANGO_flit *) &(st_net->flit);

    fprintf(fd, "Cycle: %016lu [INJ  M_%2d  T_  ]  VN: %02u, Flit(FT: %2s, DST: %03u (%3s), SRC: %03u (%3s), BA: %08x, CMD: %s, Id: %02u   |  REG: %02u, DATA: %08x)\n", 
      st_net->timestamp, 
      tile,
      st_net->vn,
      (st_flit->flit_type == 0) ? "B" : (st_flit->flit_type == 1) ? "T" : (st_flit->flit_type == 2) ? "HT" : "H",
      st_flit->dst,
      COMPONENT_TYPES[st_flit->dst_type],
      st_flit->src,
      COMPONENT_TYPES[st_flit->src_type],
      st_flit->blockaddr << 6,
      mango_events[st_flit->cmd],
      st_flit->reg_or_id & 0x0F,
      st_flit->reg_or_id & 0x1F,
      st_flit->cmd << 26 | st_flit->blockaddr);
  } else {
    register struct PEAK_flit *st_flit = (struct PEAK_flit *) &(st_net->flit);
    print_PEAK_FLIT_debug("INJ", st_net->timestamp, tile, subtile, st_net->vn, st_flit, fd);
  }
#else
  register struct PEAK_flit *st_flit = (struct PEAK_flit *) &(st_net->flit);
  print_PEAK_FLIT_debug("INJ", st_net->timestamp, tile, subtile, st_net->vn, st_flit, fd);
#endif  

}

void print_EJECT_debug(register unsigned int tile, register unsigned int subtile) {
  register int mode = pparams.mode;
  register unsigned char *data;

#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    data = &(mango_data[tile].EJECT_debug[0]);
  } else {
    data = &(unit_data[tile][subtile].EJECT_debug[0]);
  }
#else
  data = &(unit_data[tile][subtile].EJECT_debug[0]);
#endif  

  struct net_debug *st_net   = (struct net_debug *)data;

  FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }


#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    register struct MANGO_flit *st_flit = (struct MANGO_flit *) &(st_net->flit);

    fprintf(fd, "Cycle: %016lu [EJE  M_%2d  T_  ]  VN: %02u, Flit(FT: %2s, DST: %03u (%3s), SRC: %03u (%3s), BA: %08x, CMD: %s, Id: %02u   |  REG: %02u, DATA: %08x)\n", 
      st_net->timestamp, 
      tile,
      st_net->vn,
      (st_flit->flit_type == 0) ? "B" : (st_flit->flit_type == 1) ? "T" : (st_flit->flit_type == 2) ? "HT" : "H",
      st_flit->dst,
      COMPONENT_TYPES[st_flit->dst_type],
      st_flit->src,
      COMPONENT_TYPES[st_flit->src_type],
      st_flit->blockaddr << 6,
      mango_events[st_flit->cmd],
      st_flit->reg_or_id & 0x0F,
      st_flit->reg_or_id & 0x1F,
      st_flit->cmd << 26 | st_flit->blockaddr);
  } else {
    register struct PEAK_flit *st_flit = (struct PEAK_flit *) &(st_net->flit);
    print_PEAK_FLIT_debug("EJE", st_net->timestamp, tile, subtile, st_net->vn, st_flit, fd);
  }
#else
  register struct PEAK_flit *st_flit = (struct PEAK_flit *) &(st_net->flit);
  print_PEAK_FLIT_debug("EJE", st_net->timestamp, tile, subtile, st_net->vn, st_flit, fd);
#endif  


}

void print_TR_debug(register unsigned int tile, register unsigned int subtile) {
  register int mode = pparams.mode;
  register unsigned char *data = &(unit_data[tile][subtile].TR_debug[0]);

  u_int32_t dataout = (u_int32_t)(
      (((u_int32_t)data[3]) << 24) + 
      (((u_int32_t)data[2]) << 16) + 
      (((u_int32_t)data[1]) << 8) + 
      ((u_int32_t)data[0])
      );  

  u_int32_t datatowrite = (u_int32_t)(
      (((u_int32_t)data[7]) << 24) + 
      (((u_int32_t)data[6]) << 16) + 
      (((u_int32_t)data[5]) << 8) + 
      ((u_int32_t)data[4])
      );  

  u_int16_t regwrite = (u_int16_t)(data[8] & 0x1F);    

  u_int16_t regread  = (u_int16_t)(
      (((u_int16_t)(data[9] & 0x03)) << 3) +
      (((u_int16_t)(data[8] & 0xE0)) >> 5)
      );    

  u_int16_t tiledst = (u_int16_t)(
      (((u_int16_t)(data[10] & 0x07)) << 6) +
      (((u_int16_t)(data[9] & 0xFC)) >> 2)
      );

  u_int16_t tilesrc = (u_int16_t)(
      (((u_int16_t)(data[11] & 0x0F)) << 5) +
      (((u_int16_t)(data[10] & 0xF8)) >> 3)
      );

  u_int8_t read_completed_op = (data[11] & 0x10) >> 4;

  u_int8_t write_op = (data[11] & 0x20) >> 5;

  u_int8_t read_op = (data[11] & 0x40) >> 6;

  u_int8_t intwrite_op = (data[11] & 0x80) >> 7;

  unsigned long timestamp = (unsigned long)((((u_int64_t)data[19]) << 56) +
      (((u_int64_t)data[18]) << 48) +
      (((u_int64_t)data[17]) << 40) +
      (((u_int64_t)data[16]) << 32) +
      (((u_int64_t)data[15]) << 24) +
      (((u_int64_t)data[14]) << 16) +
      (((u_int64_t)data[13]) << 8) +
      (((u_int64_t)data[12])));	

  FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }

  if ((data[11] & 0x70) == 0x70) {
    //post morten debug info
      fprintf(fd, "Cycle: %016lu [TR   M_%2u  T_%2u] %02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X-%02X\n", 
          timestamp, tile, subtile,
          data[11], data[10], data[9], data[8], data[7], data[6], 
          data[5], data[4],data[3], data[2], data[1], data[0]);
  } else {
    fprintf(fd, "Cycle: %016lu [TR   M_%2u  T_%2u] int_write: %1u, Read: %1u, Write: %1u, ReadCompleted: %1u,"\
                " RegRead: %04u, RegWrite: %04u, Sender: %04u, Dest: %04u, DataWrite: %08x, DataRead: %08x\n",
        timestamp, tile, subtile, intwrite_op, read_op, write_op, read_completed_op, regread, regwrite, tilesrc, tiledst,
        datatowrite, dataout);
  }
}

#ifdef MANGO
void print_U2M_debug(register unsigned tile) {
  register int mode = pparams.mode;

  register struct u2m_debug *st_u2m = (struct u2m_debug *)&(mango_data[tile].U2M_debug[0]);

  register FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }

  fprintf(fd, "Cycle: %016lu [U2M  M_%2d  T_  ]  Write: %u, Read: %u, Addr: %08x, Sender: %02u (%3s), Word: %u, Half: %u, Byte: %u   |   to_inject: %u, to_mem: %u, id_to_inject: %u   |   Injected: %u, InjId: %u, Word: %u, Half: %u, Byte: %u, Dst: %03u, PhyAddr: %08x\n", 
      st_u2m->timestamp, 
      tile,
      st_u2m->write,
      st_u2m->read,
      st_u2m->v_addr,
      st_u2m->sender,
      COMPONENT_TYPES[st_u2m->sender_type],
      st_u2m->word_access_i,
      st_u2m->half_access_i,
      st_u2m->byte_access_i,
      st_u2m->inject,
      st_u2m->to_memory,
      st_u2m->id_to_inject,
      st_u2m->injected,
      st_u2m->injected_id,
      st_u2m->word_access,
      st_u2m->half_access,
      st_u2m->byte_access,
      st_u2m->dst,
      st_u2m->phy_addr);
}

void print_M2U_debug(register unsigned int tile) {
  register int mode = pparams.mode;
  register struct m2u_debug *st_m2u = (struct m2u_debug *)&(mango_data[tile].M2U_debug[0]);

  register FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }

  fprintf(fd, "Cycle: %016lu [M2U  M_%2d  T_  ]  Retrive: %u, TableId: %2u    |   UnitReq: %u,  Word: %u, Dst: %02u (%3s), Addr: %08x\n", 
      st_m2u->timestamp, 
      tile,
      st_m2u->table_retrieve,
      st_m2u->table_id_to_retrieve,
      st_m2u->unit_req,
      st_m2u->word_access,
      st_m2u->dst,
      COMPONENT_TYPES[st_m2u->dst_type],
      st_m2u->address);
}

void print_MANGO_TR_debug(register unsigned int tile) {
  register int mode = pparams.mode;
  register struct MANGO_TR_debug_st *st_tr = (struct MANGO_TR_debug_st *)&(mango_data[tile].TR_debug[0]);

  register FILE *fd = stdout;
  if (mode == 13) { 
    fd = pparams.fd_m13;
    pparams.line_m13++;
  }

  fprintf(fd, "Cycle: %016lu [TR  M_%2d  T_  ] EXT_WR_REQ: %u, EXT_REG: %2u, RB_DATA: %08X, EXT_DATA: %08X  |  EXT_RD_REQ: %u, EXT_REG: %2u, RB_DATA: %08X  |  NI_WR_REQ: %u, NI_REG: %2u, RB_DATA: %08X, NI_DATA: %08X  |"\
      "  NI_RD_REQ: %u, NI_REG: %2u, NI_DST: %3u, RB_DATA: %08X  |  NI_RD: %u, NI_REG: %2u, NI_DST: %3u, NI_DATA: %08X  |  NI_RD_AVAIL: %u, NI_WR_AVAIL: %u\n",   
      st_tr->timestamp,
      tile,
      st_tr->item_wr_req,
      st_tr->item_wr_reg,
      st_tr->item_wr_trdata,
      st_tr->item_wr_data,
      st_tr->item_rd_req,
      st_tr->item_rd_reg,
      st_tr->item_rd_data,
      st_tr->ni_wr_pending,
      st_tr->ni_wr_reg,
      st_tr->ni_wr_trdata,
      st_tr->ni_wr_data,
      st_tr->ni_rd_pending,
      st_tr->ni_rd_reg,
      st_tr->ni_rd_dst,
      st_tr->ni_rd_data,
      st_tr->ni_req,
      st_tr->ni_reg,
      st_tr->ni_dst,
      st_tr->ni_data,
      st_tr->ni_rd_avail,
      st_tr->ni_wr_avail
      );
}
#endif

void handle_debug_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  register int *index;

  if ((pparams.mode == 13) && (pparams.line_m13 > MAX_LINE_M13)) {
    pparams.line_m13 = 0;
    fseek(pparams.fd_m13, 0, SEEK_SET);
  }


#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    switch(cmd) {
      case COMMAND_DEBUG_U2M_FIRST_ITEM:
        index = &(mango_data[tile].U2M_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete U2M debug info for tile %d\n", tile);
        }

        mango_data[tile].U2M_debug[0] = value;
        *index = 1;
        break;

      case COMMAND_DEBUG_U2M_REMAINING_ITEMS:
        index = &(mango_data[tile].U2M_debug_index);
        mango_data[tile].U2M_debug[*index] = value;

        if(*index < (U2M_DEBUG_SIZE -1)) {
          *index += 1;
        } else {
          print_U2M_debug(tile);
          *index = 0;
        }
        break;

      case COMMAND_DEBUG_M2U_FIRST_ITEM:
        index = &(mango_data[tile].M2U_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete M2U debug info for tile %d\n", tile);
        }

        mango_data[tile].M2U_debug[0] = value;
        *index = 1;
        break;

      case COMMAND_DEBUG_M2U_REMAINING_ITEMS:
        index = &(mango_data[tile].M2U_debug_index);
        mango_data[tile].M2U_debug[*index] = value;

        if(*index < (M2U_DEBUG_SIZE -1)) {
          *index += 1;
        } else {
          print_M2U_debug(tile);
          *index = 0;
        }
        break;

      case COMMAND_DEBUG_INJECT_FIRST_ITEM:
        index = &(mango_data[tile].INJECT_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete INJECT debug info for tile %d\n", tile);
        }

        mango_data[tile].INJECT_debug[0] = value;
        *index = 1;
        break;

      case COMMAND_DEBUG_INJECT_REMAINING_ITEMS:
        index = &(mango_data[tile].INJECT_debug_index);
        mango_data[tile].INJECT_debug[*index] = value;

        if(*index < (INJECT_DEBUG_SIZE -1)) {
          *index += 1;
        } else {
          print_INJECT_debug(tile, NOT_APPLICABLE);
          *index = 0;
        }
        break;

      case COMMAND_DEBUG_EJECT_FIRST_ITEM:
        index = &(mango_data[tile].EJECT_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete EJECT debug info for tile %d\n", tile);
        }

        mango_data[tile].EJECT_debug[0] = value;
        *index = 1;
        break;

      case COMMAND_DEBUG_EJECT_REMAINING_ITEMS:
        index = &(mango_data[tile].EJECT_debug_index);
        mango_data[tile].EJECT_debug[*index] = value;

        if(*index < (EJECT_DEBUG_SIZE -1)) {
          *index += 1;
        } else {
          print_EJECT_debug(tile, NOT_APPLICABLE);
          *index = 0;
        }
        break;

      case COMMAND_DEBUG_MC_FIRST_ITEM:
        index = &(mango_data[tile].MC_BLOCK_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete MC BLOCK debug info for tile %d\n", tile);
        }

        *index = 1; 
        mango_data[tile].MC_BLOCK_debug[0] = value;
        break;

      case COMMAND_DEBUG_MC_REMAINING_ITEM:
        index = &(mango_data[tile].MC_BLOCK_debug_index);
        mango_data[tile].MC_BLOCK_debug[*index] = value;

        if (*index < (BLOCK_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_BLOCK_debug(tile, NOT_APPLICABLE, MC_TYPE);
          *index = 0;
        }
        break;

      case COMMAND_DEBUG_TILEREG_FIRST_ITEM:
        index = &(mango_data[tile].TR_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete TILEREG debug info for tile %d\n", tile);
        }

        mango_data[tile].TR_debug[0] = value;
        *index = 1;
        break;

      case COMMAND_DEBUG_TILEREG_REMAINING_ITEMS:
        index = &(mango_data[tile].TR_debug_index);
        mango_data[tile].TR_debug[*index] = value;

        if(*index < (MANGO_TR_DEBUG_SIZE -1)) {
          *index += 1;
        } else {
          print_MANGO_TR_debug(tile);
          *index = 0;
        }
        break;

      default:
        // nothig to do. It is not a DEBUG command, but we are in DEBUG mode
        //fprintf(stderr, "debug command %02X not supported yet\n", cmd);
        ;
    }
  }
#endif

  if (subtile != NOT_APPLICABLE) {
    switch (cmd) {
      case COMMAND_PEAK_DEBUG_CORE_FIRST_ITEM:
        index = &(unit_data[tile][subtile].CORE_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete CORE debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].CORE_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_CORE_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].CORE_debug_index);
        unit_data[tile][subtile].CORE_debug[*index] = value;
        if (*index < (CORE_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_CORE_debug(tile, subtile);
          *index = 0;
        }
        break;

      case COMMAND_PEAK_DEBUG_L1D_FIRST_ITEM:
        index = &(unit_data[tile][subtile].L1D_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete L1D debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].L1D_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_L1D_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].L1D_debug_index);
        unit_data[tile][subtile].L1D_debug[*index] = value;
        if (*index < (L1D_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_L1D_debug(tile, subtile);
          *index = 0;
        }
        break;

      case COMMAND_PEAK_DEBUG_L2_FIRST_ITEM:
        index = &(unit_data[tile][subtile].L2_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete L2 debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].L2_debug[0] = value;
        break;
        break;

      case COMMAND_PEAK_DEBUG_L2_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].L2_debug_index);
        unit_data[tile][subtile].L2_debug[*index] = value;
        if (*index < (L2_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_L2_debug(tile, subtile);
          *index = 0;
        }
        break;


      case COMMAND_PEAK_DEBUG_BLOCK_L1D_FIRST_ITEM:
        index = &(unit_data[tile][subtile].L1D_BLOCK_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete L1D BLOCK debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].L1D_BLOCK_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_BLOCK_L1D_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].L1D_BLOCK_debug_index);
        unit_data[tile][subtile].L1D_BLOCK_debug[*index] = value;
        if (*index < (BLOCK_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_BLOCK_debug(tile, subtile, L1D_TYPE);
          *index = 0;
        }
        break;

      case COMMAND_PEAK_DEBUG_BLOCK_L2_FIRST_ITEM:
        index = &(unit_data[tile][subtile].L2_BLOCK_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete L2 BLOCK debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].L2_BLOCK_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_BLOCK_L2_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].L2_BLOCK_debug_index);
        unit_data[tile][subtile].L2_BLOCK_debug[*index] = value;
        if (*index < (BLOCK_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_BLOCK_debug(tile, subtile, L2_TYPE);
          *index = 0;
        }
        break;

      case COMMAND_PEAK_DEBUG_MC_FIRST_ITEM:
        index = &(unit_data[tile][subtile].MC_BLOCK_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete MC BLOCK debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].MC_BLOCK_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_MC_REMAINING_ITEM:
        index = &(unit_data[tile][subtile].MC_BLOCK_debug_index);
        unit_data[tile][subtile].MC_BLOCK_debug[*index] = value;
        if (*index < (BLOCK_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_BLOCK_debug(tile, subtile, MC_TYPE);
          *index = 0;
        }
        break;

      case COMMAND_PEAK_DEBUG_INJECT_FIRST_ITEM:
        index = &(unit_data[tile][subtile].INJECT_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete INJECT debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].INJECT_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_INJECT_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].INJECT_debug_index);
        unit_data[tile][subtile].INJECT_debug[*index] = value;
        if (*index < (INJECT_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_INJECT_debug(tile, subtile);
          *index = 0;
        }
        break;

      case COMMAND_PEAK_DEBUG_EJECT_FIRST_ITEM:
        index = &(unit_data[tile][subtile].EJECT_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete EJECT debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].EJECT_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_EJECT_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].EJECT_debug_index);
        unit_data[tile][subtile].EJECT_debug[*index] = value;
        if (*index < (EJECT_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_EJECT_debug(tile, subtile);
          *index = 0;
        }
        break;

      case COMMAND_PEAK_DEBUG_TR_FIRST_ITEM:
        index = &(unit_data[tile][subtile].TR_debug_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete TR debug info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].TR_debug[0] = value;
        break;

      case COMMAND_PEAK_DEBUG_TR_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].TR_debug_index);
        unit_data[tile][subtile].TR_debug[*index] = value;
        if (*index < (TR_DEBUG_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_TR_debug(tile, subtile);
          *index = 0;
        }
        break;

      default:
        ;
        // nothig to do. It is not a DEBUG command, but we are in DEBUG mode
        //fprintf(stderr, "debug command %02X not supported yet\n", cmd);
    } 
  }
}

void print_BLOCK_info(register unsigned int tile, register unsigned int subtile) {
  register unsigned char *data;

#ifdef MANGO
  data = &(mango_data[tile].MC_BLOCK_info[0]);
#else
  data = &(unit_data[tile][subtile].MC_BLOCK_info[0]);

#endif

  register struct MC_info_st *mc_block = (struct MC_info_st *)data;

#ifdef MANGO  
  fprintf(stdout, "[MC   M_%2u  T_  ]  Addr: %08x Block content: %08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x\n",
#else
  fprintf(stdout, "[MC   M_%2u  T_%2u]  Addr: %08x Block content: %08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x-%08x\n",
#endif      
        tile, 
#ifndef MANGO        
        subtile,
#endif        
       mc_block->address,
       mc_block->word[15], mc_block->word[14], mc_block->word[13], mc_block->word[12], mc_block->word[11], mc_block->word[10], mc_block->word[9], mc_block->word[8],
       mc_block->word[7], mc_block->word[6], mc_block->word[5], mc_block->word[4], mc_block->word[3], mc_block->word[2], mc_block->word[1], mc_block->word[0]);
} 
void handle_memory_reading_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  register int *index;

  if (subtile == NOT_APPLICABLE) {
    switch(cmd) {
      case COMMAND_MC_DATA_FIRST_ITEM:
#ifdef MANGO
        index = &(mango_data[tile].MC_BLOCK_info_index);
#else
        index = &(unit_data[tile][subtile].MC_BLOCK_info_index);
#endif
        if (*index != 0) {
          fprintf(stderr, "Detected incomplete MC BLOCK info info for tile %d\n", tile);
        }

        *index = 1;
#ifdef MANGO 
        mango_data[tile].MC_BLOCK_info[0] = value;
#else
        unit_data[tile][subtile].MC_BLOCK_info[0] = value;
#endif
        break;

      case COMMAND_MC_DATA_REMAINING_ITEMS:
#ifdef MANGO
        index = &(mango_data[tile].MC_BLOCK_info_index);
        mango_data[tile].MC_BLOCK_info[*index] = value;
#else
        index = &(unit_data[tile][subtile].MC_BLOCK_info_index);
        unit_data[tile][subtile].MC_BLOCK_info[*index] = value;
#endif

        if (*index < (MC_READ_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_BLOCK_info(tile, NOT_APPLICABLE);
          *index = 0;
        }
        break;

      default:
        // nothig to do. It is not a DEBUG command, but we are in DEBUG mode
        //fprintf(stderr, "debug command %02X not supported yet\n", cmd);
        ;
    }
  }
} 



void print_CONSOLE_info(register unsigned int tile, register unsigned int subtile) {
  register unsigned char *data;

#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    data = &(mango_data[tile].CONSOLE[0]);
  } else {
    data = &(unit_data[tile][subtile].CONSOLE[0]);
  }
#else
  data = &(unit_data[tile][subtile].CONSOLE[0]);
#endif

  if (subtile == NOT_APPLICABLE) {
    fprintf(stdout, "[CONSOLE  M_%2u  T_  ] %s", tile, (char *)data);
  } else {
    fprintf(stdout, "[CONSOLE  M_%2u  T_%2u] %s", tile, subtile, (char *)data);
  }
} 





void handle_console_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  register int *index;

  
  if ((cmd == COMMAND_CONSOLE) || (cmd == COMMAND_PEAK_CONSOLE)) {
#ifdef MANGO
    if (subtile == NOT_APPLICABLE) {
      index = &(mango_data[tile].CONSOLE_index);
      mango_data[tile].CONSOLE[*index] = value;
      *index += 1;
      mango_data[tile].CONSOLE[*index] = 0;
    } else {
      index = &(unit_data[tile][subtile].CONSOLE_index);
      unit_data[tile][subtile].CONSOLE[*index] = value;
      *index += 1;
      unit_data[tile][subtile].CONSOLE[*index] = 0;
    }
#else
    index = &(unit_data[tile][subtile].CONSOLE_index);
    unit_data[tile][subtile].CONSOLE[*index] = value;
    *index += 1;
    unit_data[tile][subtile].CONSOLE[*index] = 0;
#endif
  }

  if ((cmd == COMMAND_CONSOLE) || (cmd == COMMAND_PEAK_CONSOLE)) {
    if (value == 0x0A) {
      // completed message
      print_CONSOLE_info(tile, subtile);
      *index = 0;
    }
  }
}

void handle_raw_console_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  if ((cmd == COMMAND_PEAK_CONSOLE) && (subtile != NOT_APPLICABLE)) {
    unsigned char val = value;
    fwrite(&val, 1, 1, stdout);
    fflush(stdout);
  }
}

void print_TR_reading(register unsigned int tile, register unsigned int subtile) {
  register struct TR_info_st *tr_info;

#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    tr_info = (struct TR_info_st *)&(mango_data[tile].TR_reading[0]);
  } else {
    tr_info = (struct TR_info_st *)&(unit_data[tile][subtile].TR_reading[0]);
  }
#else
  tr_info = (struct TR_info_st *)&(unit_data[tile][subtile].TR_reading[0]);
#endif

  if (subtile == NOT_APPLICABLE) {
    fprintf(stdout, "[TR   M_%2u  T_  ] Reg: %04u, Data: %08X, uint_val: %u\n", tile, tr_info->reg, tr_info->value, tr_info->value);
  } else {
    fprintf(stdout, "[TR   M_%2u  T_%2u] Reg: %04u, Data: %08X, uint_val: %u\n", tile, subtile, tr_info->reg, tr_info->value, tr_info->value);
  }
}

void handle_tr_reading_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  register int *index;

#ifdef MANGO
  if (subtile == NOT_APPLICABLE) {
    switch (cmd) {
    case COMMAND_TILEREG_FIRST_ITEM:
      index = &(mango_data[tile].TR_reading_index);

      if (*index != 0) {
        fprintf(stderr, "Detected incomplete TR_READING info for tile %d\n", tile);
      }

      *index = 1; 
      mango_data[tile].TR_reading[0] = value;
      break;

    case COMMAND_TILEREG_REMAINING_ITEMS:
      index = &(mango_data[tile].TR_reading_index);
      mango_data[tile].TR_reading[*index] = value;

      if (*index < (TR_SIZE-1)) {
        *index += 1;
      } else {
        // completed message
        print_TR_reading(tile, NOT_APPLICABLE);
        *index = 0;
      }
      break;
    default:
      //nothing to do. It is not a TR_READING command, but we are in TR_READING mode
      //fprintf(stderr, "TR READING command %02X not supported\n", cmd);
      ;
    }
  }
#endif

  if (subtile != NOT_APPLICABLE) {
    switch (cmd) {
      case COMMAND_PEAK_TILEREG_FIRST_ITEM:
        index = &(unit_data[tile][subtile].TR_reading_index);

        if (*index != 0) {
          fprintf(stderr, "Detected incomplete TR READING info for tile %d\n", tile);
        }

        *index = 1; 
        unit_data[tile][subtile].TR_reading[0] = value;
        break;

      case COMMAND_PEAK_TILEREG_REMAINING_ITEMS:
        index = &(unit_data[tile][subtile].TR_reading_index);
        unit_data[tile][subtile].TR_reading[*index] = value;
        if (*index < (TR_SIZE-1)) {
          *index += 1;
        } else {
          // completed message
          print_TR_reading(tile, subtile);
          *index = 0;
        }
        break;

      default:
        ;
        //nothing to do. It is not a TR_READING command, but we are in TR_READING mode
        //fprintf(stderr, "TR READING command %02X not supported\n", cmd);
    }
  }
}


void handle_echo_and_notification_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  char msg[256];

  if ((cmd == COMMAND_ECHO) || (cmd == COMMAND_PEAK_ECHO)) {
    switch (value) {
      case 0x84: strcpy(msg, "DEBUG, STATS or ECHO ENABLED/DISABLED");
                 break;
      case 0x8c: strcpy(msg, "MEMORY BLOCK WRITTEN");
                 break;
      case 0x3f: strcpy(msg, "RESET DONE");
                 break;
      case 0x3e: strcpy(msg, "MEM CALIB DONE");
                 break;
      case 0x3d: strcpy(msg, "MEM CALIB LOST");
                 break;
      case 0x3c: strcpy(msg, "ITEM DATA LOST AT INPUT FIFO");
                 break;
      case 0xac: strcpy(msg, "TILE REGISTER READ");
                 break;
      case 0xa8: strcpy(msg, "TILE REGISTER WRITTEN");
                 break;
      case 0x21: strcpy(msg, "L1D PROTOCOL LOADED");
                 break;
      case 0x20: strcpy(msg, "L2D PROTOCOL LOADED");
                 break;
      case 0x94: strcpy(msg, "TASK REQUESTED");
                 break;
      case 0xa4: strcpy(msg, "KEYBOARD INTERRUPT");
                 break;
      case 0x9c: strcpy(msg, "PAUSE, RESUME or TICKS CLOCK");
                 break;
      default:  sprintf(msg, "%02X", value);
    }

    if (subtile == NOT_APPLICABLE) {
      fprintf(stdout, "[ECHO/NOTI  M_%2u  T_  ]  msg: %s\n", tile, msg);
    } else {
      fprintf(stdout, "[ECHO/NOTI  M_%2u  T_%2u]  msg: %s\n", tile, subtile, msg);
    }
  }
} 



void handle_stats_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  fprintf(stderr, "handle stats item deprecated\n");
}

void handle_raw_item(register unsigned int tile, register unsigned int subtile, register unsigned int cmd, register unsigned char value) {
  fprintf(stdout, "item received: tile %04u, subtile: %04u, command %02X (%02u), value %02X\n", tile, subtile, cmd, cmd, value);
}
 
 
void update_seq_number(const unsigned char payload[4]) {

  unsigned int seq_number = ((unsigned int)payload[0] << 24) + ((unsigned int)payload[1] << 16) + ((unsigned int)payload[2] << 8) + ((unsigned int)payload[3]);  


  if ((seq_number < seq_num_ant) && (seq_number != 0)) {
    // overflow. The right value for seq_number should be 0, otherwise there are some lost frames
    num_lost_frames += (U_INT32_T_MAX-seq_num_ant) + seq_number;
    fprintf(stderr, "LOST FRAMES: overflow, seq_num = %d, seq_num_ant=%d\n", seq_number, seq_num_ant);
  } else if ((seq_num_ant != 0) && ((seq_num_ant+1) != seq_number) && (seq_number != 0)) {
    // the right equation should be always seq_num = seq_num_ant + 1.
    // Exceptions: intermediate reset that makes seq_number = 0;
    num_lost_frames += (seq_number - seq_num_ant);
    fprintf(stderr, "LOST FRAMES: seq_num = %d, seq_num_ant=%d\n", seq_number, seq_num_ant);
  }
  seq_num_ant = seq_number;
 
}


int check_supported_mode(int mode) {
  int i = 0;
  int sm;
  do {
    sm = supported_modes[i++];
    if (sm == mode)
      return 1;
  } while (sm != -1);

  return 0;
}



int get_policy(char p, int *policy) {
  switch (p) {
    case 'f': *policy = SCHED_FIFO;     return 1;
    case 'r': *policy = SCHED_RR;       return 1;
    case 'o': *policy = SCHED_OTHER;    return 1;
    default:  return 0;
  }
}

void display_sched_attr(int policy, struct sched_param *param) {
  fprintf(stdout, "    policy=%s, priority=%d\n",
      (policy == SCHED_FIFO)  ? "SCHED_FIFO" :
      (policy == SCHED_RR)    ? "SCHED_RR" :
      (policy == SCHED_OTHER) ? "SCHED_OTHER" :
      "???",
      param->sched_priority);
}

void display_thread_sched_attr(pthread_t tid, FILE *fd, char *msg) {
  int policy, s;
  struct sched_param param;

  s = pthread_getschedparam(tid, &policy, &param);
  if (s != 0) {
    fprintf(stderr, "Couldn't get scheduling params for displaying them. %s\n", strerror(s));
  } else {
    fprintf(fd, "Scheduler settings for %s\n", msg);
    fprintf(fd, "    policy=%s, priority=%d\n",
      (policy == SCHED_FIFO)  ? "SCHED_FIFO" :
      (policy == SCHED_RR)    ? "SCHED_RR" :
      (policy == SCHED_OTHER) ? "SCHED_OTHER" :
      "???",
      param.sched_priority);
  }
}


void display_thread_affinity(pthread_t tid, FILE *fd, char *msg) {
  int status;
  cpu_set_t cs;

  status = pthread_getaffinity_np(tid, sizeof(cpu_set_t), &cs);
  if (status != 0) {
    fprintf(stderr, "Couldn't get affinity for %s. %s\n", msg, strerror(status));
  } else {
    fprintf(fd, "Affinity for %s set to CPU(s):", msg);
    int i;
    for (i = 0; i < CPU_SETSIZE; i++) {
      if (CPU_ISSET(i, &cs)) {
        fprintf(fd, "%d  ", i);
      }
    }
    fprintf(fd, "\n");
  }
} 


void end_program(int signum) {
  if ((signum == SIGINT) || (signum == SIGTERM)) {
    if (pcap_info.handler != NULL) {
      pcap_breakloop(pcap_info.handler);
    }

    end_flag = 1;
  }
}




void show_help(char *prog) {
  printf("USAGE: %s [-m <mode>] [-t <tile>] [-m13f <mod13fname>] (-d <device> [-pcap [-pcapf <fname>] [-rotate] [-fsize <size>] [-bm <size>] [-bf <size>]] || -pcap [-pcapf <fname>] [-offline [-rotate [-i <first_file_index>]]] || -hnlib -d <device> [[-hnfile <fname>] [-rotate] [-fsize <size>] [-bf <size>] || -hnlib [-hnfile <fname> [-offline [-rotate [-i <first_file_index>]]]) [-verbose]\n", prog);
  printf("\t%8s  output mode (default 0). 0=console, 1=stats one tile, 2=debug, 3=echo, 4=raw,\n", "-m");
  printf("\t%8s                           5=stats all tiles, 11=raw console, 12=tilereg, 13=debug to file (1M debug traces kept).\n", "");
  printf("\t%8s  tile id. 0, 1, 2..., 999=all tiles (default 999).\n", "-t");
#ifdef MANGO  
  printf("\t%8s  subtile id. 0, 1, 2..., 999=all subtiles (default 999). Only relevant for the MANGO project\n", "-st");
#endif  
  printf("\t%8s  ethernet device in which listen to. Only for processes that require connect to the physical device.\n", "-d");
  printf("\t%8s                                      In case of using pcap only one process requires to connect to the ethernet.\n", "");
  printf("\t%8s                                      In case of using hnlib only one process requires to connect to the HNlib socket. Use -d 'hn' to dump to file or -d 'hnmem' to process item directly (it is slower than previous mode) in that case.\n", "");
  printf("\t%8s  file name for mode 13 debug file (default debug.out).\n", "-m13f");
  printf("\t%8s  use pcap library.\n", "-pcap");
  printf("\t%8s  file for storing frames when using libpcap (default debug.pcap).\n", "-pcapf");
  printf("\t%8s  interpret the data in pcap/hnlib file and ends up without waiting infinitely for new data\n", "-offline");
  printf("\t%8s  rotate pcap/hnlib file size when 'fsize' size is reached. Also used for offline analysis when pcap/hnlib live capture with rotation has been used. Three rotating files are used (default no)\n", "-rotate");
  printf("\t%8s  first index of the rotate files that has to be used in offline analysis. (default 0).\n", "-i");
  printf("\t%8s  file size for the rotating files in GB. Must be integer (default %ld GB, max is %ld GB)\n", "-fsize", ROTATE_FILE_DEFAULT_SIZE >> 30, ROTATE_FILE_MAX_SIZE >> 30);
  printf("\t%8s  buffer size allocated in the kernel for the received frames. Input the value in MB. (default %d MB)\n", "-bm", PCAP_BUFFER_MEM_SIZE >> 20);
  printf("\t%8s  buffer size allocated for BLOCK writings on the pcap/hnlib capture file. Input the value in MB. (default %d MB, max is %d MB)\n", "-bf", LIVE_CAPTURE_FILE_BUFFER_SIZE >> 20, LIVE_CAPTURE_FILE_BUFFER_SIZE >> 20);
  printf("\t%8s  use hn library.\n", "-hnlib");
  printf("\t%8s  file for storing items when using hnlib (default debug.hnlib).\n", "-hnfile");
  printf("\t%8s  enable verbose mode\n", "-verbose");
  printf("\n\n  The most efficient call is to save captured frames to file for offline analysis:\n");
  printf("\t\t\t%s -d <dev> -pcap -pcapf <file name> [-rotate] [-fsize <size>]\n", prog);
  printf("\t\t\t%s -d hn -hnlib -hnfile <file name> [-rotate] [-fsize <size>]\n", prog);
  printf("\n\n 'kill -s SIGUSR1 <pid>' shows current stats in a live pcap capture\n"); 
  printf("\n\n 'kill -s SIGUSR2 <pid>' flushes to the current live capture file in a live capture\n"); 
  printf("\n\n Ctrl-C ends the program\n\n\n");
}




int parse_arguments(int argc, char **argv) {

  if (argc < 2) {
    show_help(argv[0]);
    return 0;
  }

  pparams.mode = 0;
  pparams.tile = 999;
  pparams.subtile = 999;
  pparams.devname[0] = 0;
  strcpy(pparams.m13fname,  "debug.out");
  pparams.use_pcap = 0;
  pparams.use_hnlib = 0;
  pparams.verbose = 0;
  pparams.live_capture = 0;
  pparams.line_m13 = 0;
  pparams.fd_m13 = NULL;
  pparams.offline = 0;
  pparams.current_livecap_file_index = 0;
  pparams.rotate = 0;
  pparams.rotate_file_size = ROTATE_FILE_DEFAULT_SIZE;
  pparams.pcap_buffer_size = PCAP_BUFFER_MEM_SIZE;
  pparams.live_capture_file_block_size  = LIVE_CAPTURE_FILE_BUFFER_SIZE;
  pparams.offline_first_file_index = 0;

  char live_capture_fname[256];
  strcpy(live_capture_fname, "live_capture.raw");

  int i = 1;
  do {
    if (strcmp(argv[i], "-m") == 0) {
      pparams.mode = atoi(argv[i+1]);
      i += 2;
    } else if (strcmp(argv[i], "-t") == 0) {
      pparams.tile = (unsigned int)atoi(argv[i+1]);
      i += 2;
    } else if (strcmp(argv[i], "-st") == 0) {
      pparams.subtile = (unsigned int)atoi(argv[i+1]);
      i += 2;
     } else if (strcmp(argv[i], "-d") == 0) {
      strcpy(pparams.devname, argv[i+1]);
      i += 2;
    } else if (strcmp(argv[i], "-m13f") == 0) {
      strcpy(pparams.m13fname, argv[i+1]);
      i += 2;
    } else if (strcmp(argv[i], "-pcap") == 0) {
      pparams.use_pcap = 1;
      i += 1;
    } else if (strcmp(argv[i], "-pcapf") == 0) {
      strcpy(live_capture_fname, argv[i+1]);
      i += 2;
    } else if (strcmp(argv[i], "-offline") == 0) {
      pparams.offline = 1;
      i += 1;
    } else if (strcmp(argv[i], "-rotate") == 0) {
      pparams.rotate = 1;
      i += 1;
    } else if (strcmp(argv[i], "-fsize") == 0) {
      pparams.rotate_file_size = atol(argv[i+1]) << 30;
      i += 2;
    } else if (strcmp(argv[i], "-i") == 0) {
      pparams.offline_first_file_index = atoi(argv[i+1]);
      i += 2;
    } else if (strcmp(argv[i], "-bm") == 0) {
      pparams.pcap_buffer_size = atoi(argv[i+1]) << 20;
      i += 2;
    } else if (strcmp(argv[i], "-bf") == 0) {
      pparams.live_capture_file_block_size = atoi(argv[i+1]) << 20;
      i += 2;
    } else if (strcmp(argv[i], "-verbose") == 0) {
      pparams.verbose = 1;
      i += 1;
    } else if (strcmp(argv[i], "-hnlib") == 0) {
      pparams.use_hnlib = 1;
      i += 1;
    } else if (strcmp(argv[i], "-hnfile") == 0) {
      strcpy(live_capture_fname, argv[i+1]);
      i += 2;
    } else {
      i += 1;
    }
  } while(i < argc);


  if (pparams.live_capture_file_block_size > LIVE_CAPTURE_FILE_BUFFER_SIZE) {
    pparams.live_capture_file_block_size = LIVE_CAPTURE_FILE_BUFFER_SIZE;
  }

  if (((pparams.use_pcap == 1) && (strlen(pparams.devname) > 0)) || ((pparams.use_hnlib == 1) && (strlen(pparams.devname) > 0))) {
    pparams.live_capture = 1;
    if (pparams.rotate == 1) {
      if (pparams.rotate_file_size > ROTATE_FILE_MAX_SIZE) {
        pparams.rotate_file_size = ROTATE_FILE_MAX_SIZE;
      }

      // rotate, the file for writing is the given in the command line with suffix .0 and .1
      // Up to the moment three rotating files are allowed
      int j;
      for (j = 0; j < NUM_ROTATE_FILES; j++) {
        sprintf(pparams.livecapfname[j], "%s.%d", live_capture_fname, j);
      }
    } else {
      // no rotate. file for writing is the given in the command line
      strcpy(pparams.livecapfname[pparams.current_livecap_file_index], live_capture_fname);
    }
  } else {
    // no live capture, so the pcap file contains the frame to interpret
    if ((pparams.offline == 1) && (pparams.rotate == 1)) {
      int j;
      for (j = 0; j < NUM_ROTATE_FILES; j++) {
        sprintf(pparams.livecapfname[j], "%s.%d", live_capture_fname, j);
      }
    } else {
      strcpy(pparams.livecapfname[pparams.current_livecap_file_index], live_capture_fname);
    }
  }

  return 1;
}




void show_params() {
  fprintf(stdout, "Given arguments:\n");
  fprintf(stdout, "%30s: %d\n", "mode", pparams.mode);
  fprintf(stdout, "%30s: %d\n", "tile", pparams.tile);  
#ifdef MANGO  
  fprintf(stdout, "%30s: %d\n", "subtile", pparams.subtile);
#endif  
  fprintf(stdout, "%30s: %s\n", "device", strlen(pparams.devname) > 0 ? pparams.devname : "none");
  fprintf(stdout, "%30s: %s\n", "pcap", pparams.use_pcap ? "yes" : "no");
  fprintf(stdout, "%30s: %s\n", "hnlib", pparams.use_hnlib ? "yes" : "no");
  fprintf(stdout, "%30s: %s\n", "rotate", pparams.rotate ? "yes (three files will be used)" : "no");
  fprintf(stdout, "%30s: %ld bytes (max is %ld bytes)\n", "rotate file size", pparams.rotate_file_size, ROTATE_FILE_MAX_SIZE);
  fprintf(stdout, "%30s: %s\n", "offline", pparams.offline ? "yes" : "no");
  fprintf(stdout, "%30s: %s\n", "m13 file", pparams.m13fname);
  fprintf(stdout, "%30s: %s\n", "capture file (pcap/hnlib)", pparams.livecapfname[0]);
  fprintf(stdout, "%30s: %d\n", "offline rotate first file index", pparams.offline_first_file_index);
  fprintf(stdout, "%30s: %d bytes (max is %d bytes)\n", "WR block buffer size", pparams.live_capture_file_block_size, LIVE_CAPTURE_FILE_BUFFER_SIZE);
  fprintf(stdout, "%30s: %d bytes\n", "RECV buffer size", pparams.pcap_buffer_size);
}


void show_stats() {
  printf("\n");

  if (pparams.live_capture == 1) {
    show_live_capture_stats();
  } else {
    if (pparams.use_pcap == 1) {
     fprintf(stdout, "%lu packets captured\n", captured_packets);
     fprintf(stdout, "%u packets lost\n", num_lost_frames);
    }

    if (pparams.use_hnlib == 1) {
      printf("%zu bytes, %zu items received\n", hnlib_info.stats.total_items * 4, hnlib_info.stats.total_items);
    }
  }

  fflush(stdout);
}

void show_live_capture_stats() {
  if (pparams.use_pcap == 1) {
    pcap_stats(pcap_info.handler, &(pcap_info.stats));

    printf("%d packets received by filter\n", pcap_info.stats.ps_recv);
    printf("%d packets dropped by NI\n", pcap_info.stats.ps_ifdrop);
    printf("%d packets dropped by kernel\n", pcap_info.stats.ps_drop);
  }

  if (pparams.use_hnlib == 1) {
    printf("%zu bytes, %zu items received\n", hnlib_info.stats.total_items * 4, hnlib_info.stats.total_items);
  }
}




int init_capture_system() {
  sigset_t set;

  // Do not care of SIGUSR1, the main thread will do it
  sigemptyset(&set);
  sigaddset(&set, SIGUSR1); 
  sigaddset(&set, SIGUSR2); 
  pthread_sigmask(SIG_BLOCK, &set, NULL);


  if (pparams.use_pcap == 1) {
    return init_pcap_capture_system();
  } else if (pparams.use_hnlib == 1) {
    return init_hnlib_capture_system();
  }
  memset(&sock, 0, sizeof(struct socket_s));
  return init_sock_capture_system();
}

int init_hnlib_capture_system() {
  int status;

  memset(&hnlib_info.stats, 0, sizeof(hnlib_info.stats));

  if (pparams.live_capture == 1) {
    // use hnlib for getting items in an efficient way through the called live capture
    // which consists of sending every item captured to file for offline processing
    // we set filter to receive any item from the system
    hn_filter_t filter;
    filter.target = HN_FILTER_TARGET_MANGO;
    filter.mode   = HN_FILTER_APPL_MODE_RAWFRAMES;
    filter.tile   = pparams.tile;
    filter.core   = pparams.subtile;
    uint32_t init_lib;
    init_lib = hn_initialize(filter, NO_PARTITION_STRATEGY, 1, 0, 1);
    if (init_lib != HN_SUCCEEDED) return -3;

    hnlib_info.handler = fopen(pparams.livecapfname[pparams.current_livecap_file_index], "wb");
    if (hnlib_info.handler == NULL) {
      fprintf(stderr, "error opening hnlib file %s for writing: %s\n", pparams.livecapfname[pparams.current_livecap_file_index], strerror(errno));
      return -1;
    }
    setbuffer(hnlib_info.handler, hnlib_info.buf_file, pparams.live_capture_file_block_size); 

    pthread_mutex_init(&(hnlib_info.file_mutex), NULL);

    struct sched_param sp;

    //sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
    //status = pthread_setschedparam(pthread_self(), SCHED_FIFO, &sp);
    //if (status != 0) {
    //  fprintf(stderr, "Couldn't configure scheduling policy for live pcap capture thread. Program will still run with default scheduling policy. %s\n", strerror(status));
    //}
      
    pthread_attr_t *sched_attr = &(hnlib_info.sched_attr);
    status = pthread_attr_init(sched_attr);
    if (status != 0) {
      fprintf(stderr, "Couldn't init scheduling attributes for threads. %s\n", strerror(status));
      return -1;
    }

    // we are self-confident that policy and inheritance will success
    sp.sched_priority = 0;
    pthread_attr_setinheritsched(sched_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(sched_attr, SCHED_OTHER);
    status = pthread_attr_setschedparam(sched_attr, &sp);
    if (status != 0) {
      fprintf(stderr, "Couldn't set scheduling policies for threads. %s\n", strerror(status));
      return -1;
    }

    status = pthread_create(&(hnlib_info.show_stats_tid), sched_attr, show_stats_thread, NULL);
    if (status != 0) {
      fprintf(stderr, "Couldn't create the shower stats thread, but program will still run. %s\n.\n", strerror(status));
    }

    status = pthread_create(&(hnlib_info.flush_tid), sched_attr, flush_hnlib_file_thread, NULL);
    if (status != 0) {
      fprintf(stderr, "Couldn't create the flsuh pcap file thread, but program will still run. %s\n.\n", strerror(status));
    }
    
    if (pparams.rotate == 1) { 
      status = pthread_create(&(hnlib_info.rotate_tid), NULL, rotate_hnlib_file, NULL);
      if (status != 0) {
        fprintf(stderr, "Couldn't create the rotating hnlib file thread. %s.\n", strerror(status));
        return -1;
      }

      cpu_set_t cs_rotate_thread;
      CPU_ZERO(&cs_rotate_thread);
      CPU_SET(1, &cs_rotate_thread);
      status = pthread_setaffinity_np(hnlib_info.rotate_tid, sizeof(cpu_set_t), &cs_rotate_thread);
      if (status != 0) {
        fprintf(stderr, "Couldn't set affinity for rotate hnlib thread. Program will still run. %s\n", strerror(status));
      }
    }


    return 0;

  } else {
    // use hnlib, but not for capturing items, just for printing info from hnlib_file
    do {
      // If offline and rotate are given, then the three rotate files are process as a unique one. To avoid incomplete frames detection
      if ((pparams.offline == 1) && (pparams.rotate == 1)) {
        pparams.current_livecap_file_index = pparams.offline_first_file_index;
      }
      hnlib_info.handler = fopen(pparams.livecapfname[pparams.current_livecap_file_index], "rb");
      if (hnlib_info.handler == NULL) {
        fprintf(stderr, "Couldn't open file %s: %s. But be quiet!!! It will be opened when exists\n", pparams.livecapfname[pparams.current_livecap_file_index], strerror(errno));
        sleep(5);
        //return -1;
      }
    } while ((hnlib_info.handler == NULL) && (end_flag == 0));

    return 0;
  }
}

int init_pcap_capture_system() {
  int status;
  char errbuf[PCAP_ERRBUF_SIZE];
  char filter_exp[] = "ether dst ff:ff:ff:ff:ff:ff and ether src 5a:1:2:3:4:5"; //"ether dst da:1:2:3:4:5 and ether src 5a:1:2:3:4:5";

  if (pparams.live_capture == 1) {
    // use pcap for capturing packets
    struct sched_param sp;

    cpu_set_t cs_capture_thread;
    CPU_ZERO(&cs_capture_thread);
    CPU_SET(0, &cs_capture_thread);
    status = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cs_capture_thread);
    if (status != 0) {
      fprintf(stderr, "Couldn't set affinity for the live capture pcap thread. Program will still run. %s\n", strerror(status));
    }

    pcap_info.handler = pcap_create(pparams.devname, errbuf);
    pthread_mutex_init(&(pcap_info.pcap_file_mutex), NULL);

    sp.sched_priority = sched_get_priority_max(SCHED_FIFO);
    status = pthread_setschedparam(pthread_self(), SCHED_FIFO, &sp);
    if (status != 0) {
      fprintf(stderr, "Couldn't configure scheduling policy for live pcap capture thread. Program will still run with default scheduling policy. %s\n", strerror(status));
    }
      
    pthread_attr_t *sched_attr = &(pcap_info.sched_attr);
    status = pthread_attr_init(sched_attr);
    if (status != 0) {
      fprintf(stderr, "Couldn't init scheduling attributes for threads. %s\n", strerror(status));
      return -1;
    }

    // we are self-confident that policy and inheritance will success
    sp.sched_priority = 0;
    pthread_attr_setinheritsched(sched_attr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(sched_attr, SCHED_OTHER);
    status = pthread_attr_setschedparam(sched_attr, &sp);
    if (status != 0) {
      fprintf(stderr, "Couldn't set scheduling policies for threads. %s\n", strerror(status));
      return -1;
    }

    status = pthread_create(&(pcap_info.show_stats_tid), sched_attr, show_stats_thread, NULL);
    if (status != 0) {
      fprintf(stderr, "Couldn't create the shower stats thread, but program will still run. %s\n.\n", strerror(status));
    }

    status = pthread_create(&(pcap_info.flush_pcapf_tid), sched_attr, flush_pcap_file_thread, NULL);
    if (status != 0) {
      fprintf(stderr, "Couldn't create the flsuh pcap file thread, but program will still run. %s\n.\n", strerror(status));
    }

    if (pparams.rotate == 1) { 
      status = pthread_create(&(pcap_info.rotate_tid), NULL, rotate_pcap_file, NULL);
      if (status != 0) {
        fprintf(stderr, "Couldn't create the rotating pcap file thread. %s.\n", strerror(status));
        return -1;
      }

      cpu_set_t cs_rotate_thread;
      CPU_ZERO(&cs_rotate_thread);
      CPU_SET(1, &cs_rotate_thread);
      status = pthread_setaffinity_np(pcap_info.rotate_tid, sizeof(cpu_set_t), &cs_rotate_thread);
      if (status != 0) {
        fprintf(stderr, "Couldn't set affinity for rotate pcap thread. Program will still run. %s\n", strerror(status));
      }
    }
      
    pcap_info.handler = pcap_create(pparams.devname, errbuf);
    if (pcap_info.handler == NULL) {
      fprintf(stderr, "Couldn't open device %s: %s\n", pparams.devname, errbuf);
      return -1;
    }

    pcap_set_snaplen(pcap_info.handler, 1518);
    pcap_set_promisc(pcap_info.handler, 1);
    pcap_set_timeout(pcap_info.handler, 1000);
    pcap_set_buffer_size(pcap_info.handler, pparams.pcap_buffer_size); 
    pcap_set_datalink(pcap_info.handler, DLT_EN10MB);


    if (pcap_activate(pcap_info.handler) != 0) {
      fprintf(stderr, "Couldn't activate pcap. %s\n", pcap_geterr(pcap_info.handler));
      return -1;
    }

    if (pcap_compile(pcap_info.handler, &(pcap_info.fp), filter_exp, 0, 0) == -1) {
      fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(pcap_info.handler));
      return -1;
    }

    if (pcap_setfilter(pcap_info.handler, &(pcap_info.fp)) == -1) {
      fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(pcap_info.handler));
      return -1;
    }
    // prepare file for frame capture 
    if ((pcap_info.dumper = pcap_dump_open(pcap_info.handler, pparams.livecapfname[pparams.current_livecap_file_index])) == NULL) {
      fprintf(stderr, "Couldn't open %s: %s", pparams.livecapfname[pparams.current_livecap_file_index], pcap_geterr(pcap_info.handler));
      return -1;
    }
    pcap_dump_flush(pcap_info.dumper);

    FILE *fd = pcap_dump_file(pcap_info.dumper);
    setbuffer(fd, pcap_info.buf_file, pparams.live_capture_file_block_size);

    return 0;

  } else {
    // use pcap, but not for capturing eth packets, just for printing info from pcap_file
    do {
      // If offline and rotate are given, then the three rotate files are process as a unique one. To avoid incomplete frames detection
      if ((pparams.offline == 1) && (pparams.rotate == 1)) {
        pparams.current_livecap_file_index = pparams.offline_first_file_index;
      }
      pcap_info.handler = pcap_open_offline(pparams.livecapfname[pparams.current_livecap_file_index], errbuf);
      if (pcap_info.handler == NULL) {
        fprintf(stderr, "Couldn't open file %s: %s. But be quiet!!! It will be opened when exists\n", pparams.livecapfname[pparams.current_livecap_file_index], errbuf);
        sleep(5);
        //return -1;
      }
    } while ((pcap_info.handler == NULL) && (end_flag == 0));

    return 0;

  } 
}


int init_sock_capture_system() {
  int fd_socket;
  struct sockaddr_ll sin;
  struct ifreq ifr;

  //Creates a packet level interface socket descriptor, RAW mode to allow send and 
  //recevie any packet type over ethernet
  //  if ((mode!=10) && (mode!=9) && (mode!=1) && (mode!=5) && (mode!=8) && (mode!=0) && (mode!=11)) printf("Socket request:... ");
  fd_socket = socket (PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if ( fd_socket < 0 ) {
    fprintf(stderr, "Couldn't create socket. %s\n", strerror(errno));
    return -1;
  }
  //  if ((mode!=10) && (mode!=9) && (mode!=1) && (mode!=5) && (mode!=8) && (mode!=0) && (mode!=11)) printf(" granted number: %d\n",fd_socket);

  if (fcntl(fd_socket, F_SETFL, O_NONBLOCK) == -1) {
    fprintf(stderr, "Couldn't open NON-BLOCK socket. %s\n", strerror(errno));
  }

  strcpy (ifr.ifr_name, pparams.devname);

  //  if ((mode!=10) && (mode!=9) && (mode!=1) && (mode!=5) && (mode!=8) && (mode!=0) && (mode!=11)) printf("over interface number: %s\n",ifr.ifr_name);

  if (ioctl(fd_socket, SIOCGIFINDEX, &ifr) < 0) {
    fprintf(stderr, "Couldn't get settings for device %s. %s\n", pparams.devname, strerror(errno));
    return -1;
  }

  memset (&sin, 0, sizeof(sin)); 
  sin.sll_family   = AF_PACKET;
  sin.sll_protocol = htons(ETH_P_ALL);
  sin.sll_ifindex  = ifr.ifr_ifindex;

  if (ioctl(fd_socket, SIOCGIFFLAGS, &ifr) < 0) {
    fprintf(stderr, "Couldn't configure device %s. %s\n", pparams.devname, strerror(errno));
    return -1;
  }

  ifr.ifr_flags |= IFF_PROMISC;

  if (ioctl(fd_socket, SIOCSIFFLAGS, &ifr) < 0) {
    fprintf(stderr, "Couldn't configure PROMISC mode for device %s. %s\n", pparams.devname, strerror(errno));
    return -1;
  }

  if (bind(fd_socket, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
    fprintf(stderr, "Couldn't configure device %s. %s\n", pparams.devname, strerror(errno));
    return -1;
  }

  sock.fd = fd_socket;

  return 0;
}



void init_data_structure() {
#ifdef MANGO  
  memset(unit_data, 0, NUMBER_OF_TILES*NUMBER_OF_SUBTILES*sizeof(struct data_s));
  memset(mango_data, 0, NUMBER_OF_TILES*sizeof(struct mango_data_s));
#else
  memset(unit_data, 0, NUMBER_OF_TILES*sizeof(struct data_s));
#endif  
}



void free_pcap_structures() {
  pcap_freecode(&(pcap_info.fp));

  if (pcap_info.dumper != NULL)
    pcap_dump_close(pcap_info.dumper);

  if (pcap_info.handler != NULL)
    pcap_close(pcap_info.handler);

  if (pparams.live_capture == 1) {
    if (pparams.rotate == 1) {
      pthread_attr_destroy(&(pcap_info.sched_attr));
    }

    pthread_mutex_destroy(&(pcap_info.pcap_file_mutex));
  }
}

void free_hnlib_structures() {
  if (hnlib_info.handler != NULL)
    fclose(hnlib_info.handler);

  if (pparams.live_capture == 1) {
    if ((pparams.rotate == 1)) {
      pthread_attr_destroy(&(hnlib_info.sched_attr));
    }

    pthread_mutex_destroy(&(hnlib_info.file_mutex));
  }

  hn_end();
}


void free_sock_structures() {
  if (sock.fd > 0) {
    close(sock.fd);
  }
}


