#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hn.h"

int main(int argc, char **argv)
{
  uint32_t wait;
  uint32_t id;
  uint32_t addr_src;
  uint32_t addr_dst;
  uint32_t tile_src;
  uint32_t tile_dst;
  uint32_t to_unit;
  uint32_t to_mem;
  uint32_t to_ext;
  unsigned long long size;
  uint32_t check;

  if (argc != 11) {
    printf("Use: %s <tile_src> <addr_src> <size> <tile_dst> <addr_dst> <to_unit> <to_mem> <to_ext> <wait> <check_res>\n", argv[0]);
    exit(1);
  }
  tile_src = atoi(argv[1]);
  addr_src = atoi(argv[2]);
  size     = atol(argv[3]);
  tile_dst = atoi(argv[4]);
  addr_dst = atoi(argv[5]);
  to_unit  = atoi(argv[6]);
  to_mem   = atoi(argv[7]);
  to_ext   = atoi(argv[8]);
  wait     = atoi(argv[9]);
  check    = atoi(argv[10]);

  hn_filter_t filter;
  if (hn_initialize(filter, 1, 1, 0, 0) != HN_SUCCEEDED) {
    printf("HN initialization error\n");
    exit(1);
  }

  char *buf;
  char *buf1;

  if (check == 1) {
    int i;
    buf  = (char *)malloc(size);
    buf1 = (char *)malloc(size);

    for (i = 0; i < size; i++)
      buf[i] = (char)(rand() >> 24);

    hn_write_memory(tile_src, addr_src, size, buf);
  }

  printf("registering dma...\n");
  if ( hn_register_dma_operation (&id) != HN_SUCCEEDED ) {
    printf("error while registering dma\n");
    hn_end();
    exit(1);
  }
  printf("dma registered with id %d\n", id);

  printf("Programming DMA: tile_src %d, addr_src %08x, size %llu, tile_dst %d, addr_dst %08x, to_unit %d, to_mem %d, to_ext %d, wait %d check_result %d\n", 
          tile_src, addr_src, size, tile_dst, addr_dst, to_unit, to_mem, to_ext, wait, check);
  if (to_unit) hn_dma_to_unit(id, addr_src, tile_src, size, tile_dst, wait);
  if (to_mem)  hn_dma_to_mem(id, addr_src, tile_src, size, addr_dst, tile_dst, wait);
  if (to_ext)  hn_dma_to_ext(id, addr_src, tile_src, size, tile_dst, wait);

  printf("releasing dma...\n");
  hn_release_dma_operation(id);

  if (check == 1) {
    hn_read_memory(tile_dst, addr_dst, size, buf1);
    if (memcmp(buf, buf1, size) != 0) {
      printf("the buffers differ in iter\n");
    }

    free(buf1);
    free(buf);
  }

  hn_end();

  return 0;
} 
