#ifndef __HN_LIST_H__
#define __HN_LIST_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 23, 2018
// File Name: hn_list.h
// Design Name:
// Module Name: HN runtime library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    This is a utility to work with double linked list of any type.
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*!
 * \brief defines what to check to know the list iteration has finished
 */
#define HN_LIST_ITER_END  -2

/*!
 * \brief Double linked list node
 */
typedef struct list_node_st
{
  void                *data;  //!< Pointer to the data stored in the node
  struct list_node_st *prev;  //!< Pointer to the previous element in th list
  struct list_node_st *next;  //!< Pointer to the next element in the list
}list_node_t;

//! \name List creation and destruction
//@{ 

/*!
 * \brief Creates a new list
 *
 * \returns the list identificator
 * \retval -1 if the list could not be created
 */
int hn_list_create_list();

/*!
 * \brief Destroy resources occupied by the given list
 */
void hn_list_destroy_list(unsigned int id);

//@}

//! \name Data insertion functions
//@{

/*!
 * \brief Adds an element to the begin of the list
 *
 * \param [in] id The list Id.
 * \param [in] ele The element to add
 * \retval  0 In case of success
 * \retval -1 In case of failure
 */
int hn_list_add_to_begin(unsigned int id, void *ele);

/*!
 * \brief Adds an element to the end of the list
 *
 * \param [in] id The list Id.
 * \param [in] ele The element to add
 * \retval  0 In case of success
 * \retval -1 In case of failure
 */
int hn_list_add_to_end(unsigned int id, void *ele);

//@}

//! \name Node deletion functions
//@{

/*!
 * \brief Removes the element in the head of the list
 *
 * \param [in] id The list Id.
 */
void hn_list_remove_head(unsigned int id);

/*!
 * \brief Removes the element in the tail of the list
 *
 * \param [in] id The list Id.
 */
void hn_list_remove_tail(unsigned int id);

/*!
 * \brief Removes the n-th element of the list
 *
 * \param [in] id The list Id.
 * \param [in] n The n-th element to remove
 */
void hn_list_remove_n_th(unsigned int id, int n_th);

/*!
 * \brief Removes the element with the given index
 *
 * \param [in] id The list Id.
 * \param [in] index the index to remove if exists
 */
void hn_list_remove_element_at(unsigned int id, unsigned int index);

/*!
 * \brief Removes all the elements in the list
 *
 * Removes all the elements in the list, so next call
 * to hn_list_is_empty() shall return 1
 *
 * \param [in] id The list Id.
 *
 */
void hn_list_remove_all(unsigned int id);

//@}


//! \name Data access functions
//@{

/*!
 * \brief Gets the element of the list at head position
 *
 * \param [in] id The list Id.
 *
 * \return a pointer to the n-th element
 * \retval NULL if the list has less than n elements
 */
void *hn_list_get_data_at_head(unsigned int id);

/*!
 * \brief Gets the element of the list at tail position
 *
 * \param [in] id The list Id.
 *
 * \return a pointer to the n-th element
 * \retval NULL if the list has less than n elements
 */
void *hn_list_get_data_at_tail(unsigned int id);

/*!
 * \brief Gets the n-th element of the list
 *
 * \param [in] id The list Id.
 * \param [in] n The n-th index
 * \return a pointer to the n-th element
 * \retval NULL if the list has less than n elements
 */
void* hn_list_get_data_n_th(unsigned int id, int n_th);

/*!
 * \brief Gets the element of the list at the given index
 *
 * \param [in] id The list Id.
 * \param [in] index The index 
 * \return a pointer to the element at index position
 * \retval NULL if the list has less than n elements
 */
void* hn_list_get_data_at_position(unsigned int id, unsigned int index);

//@}

//! \name List status checking functions
//@{

/*!
 * \brief Checks if the list is empty of elements
 *
 * \param [in] id The list Id.
 * \retval  1 if empty
 * \retval  0 if not empty
 * \retval -1 if the list has not been initialized
 */
int hn_list_is_empty(unsigned int id);

//@}


//! \name Data iteration functions
//@{

/*
 * \brief Gets an iterator to the heading of the list
 *
 * \param [in] id The list Id.
 * \return an iterator to the begining of the list
 * \retval -1 if there is a failure, p.e., the list has not been created yet
 * \retval -2 if the list is empty
 *
 * \deprecated { This function has been deprecated since version 0. Use \see{#hn_list_iter_front_begin} instead of}
 */
int hn_list_iter_begin(unsigned int id);

/*
 * \brief Checks if the iteration proccess should finish
 *
 * \param [in] id The list Id.
 * \param [in] iter the current iterator returned by begin/end/next/back
 * \return if the iteration process should finish
 *
 * \deprecated { This function has been deprecated since version 0. Use \see{#hn_list_iter_end} instead of}
 */
int hn_list_iter_exit(unsigned int id, int iter);

/*
 * \brief Gets an iterator to the heading of the list
 *
 * \param [in] id The list Id.
 * \return an iterator to the begining of the list
 */
int hn_list_iter_front_begin(unsigned int id);

/*
 * \brief Gets an iterator to the tail of the list
 *
 * \param [in] id The list Id.
 * \return an iterator to the tail of the list
 */
int hn_list_iter_reverse_begin(unsigned int id);

/*
 * \brief Gets an iterator to the end of the list
 *
 * It can be used for checking when the iteration process finishes
 *
 *
 * \param [in] id The list Id.
 * \returns an iterator to the end of the list
 */
int hn_list_iter_end(unsigned int id, int iter);

/*
 * \brief Gets an iterator to the next element of the given one
 *
 * \param [in] id The list Id.
 * \param [in] iter iterator for which the next element will be provided
 * \return an iterator to the next element of the one provided
 */
int hn_list_iter_next(unsigned int id, int iter);

/*
 * \brief Gets an iterator to the previous element of the given one
 *
 * \param [in] id The list Id.
 * \param [in] iter iterator for which the previous element will be provided
 * \return an iterator to the previous element of the one provided
 */
int hn_list_iter_back(unsigned int id, int iter);

//@}

#endif
