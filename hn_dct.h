#ifndef __HN_DCT_H__
#define __HN_DCT_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: February 22, 2018
// File Name: hn_dct.h
// Design Name:
// Module Name: HN library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    HN functions for DCT header file
//
//
// Dependencies: NONE
//
// Revision:
//   TODO add here revisions and author
//
// Additional Comments: NONE
//
//   TODO describe here additions
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//! \file

#ifdef __cplusplus
extern "C" {
#endif

//--------------------------------------------------------------------------------------------------
//BBBBBBBBBEEEEEEEEEEEEEEWWWWWWWWWWWWWWAAAAAAAAAAARRRRRRRRRRRRREEEEEEEEEEEEE!!!!!!!!!!!!!!!!!!!!!!!!
//
// DCT MODELS GO FROM 100 TO 149 INCLUSIVE TO BE ALIGNED WITH BBQUE
//-------------------------------------------------------------------------------------------------- 
/*!
 * \brief Model id for the base configuration 
 */
#define HN_DCT_MODEL_BASE  100 

/*!
 * \brief Returns an string representation for the unit model given
 *
 * \param [in] model the model the string representation will be for
 * \return an string representatiton of the model
 * \retval NULL the model does not match any of the supported models
 */
const char *hn_dct_to_str_unit_model(uint32_t model);

/*!
 * \brief Gets the utilization of a DCT unit
 *
 * \param [in] tile the tile the DCT unit utilization is going to be obtained for
 * \param [out] utilization the utilization of the given unit
 * \retval HN_SUCCEEDED in case of success or one of the error codes in hn.h otherwise
 */
uint32_t hn_dct_get_utilization(uint32_t tile, uint32_t *utilization);

// hn_dct_run_kernel
//  short: runs DCT unit
//  argument tile: Tile where the DCT unit is located
uint32_t hn_dct_run_kernel(uint32_t tile);

// TODO add here DCT function prototypes

#ifdef __cplusplus
}
#endif

#endif
