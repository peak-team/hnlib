///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 23, 2018
// File Name: hn_list.c
// Design Name:
// Module Name: HN library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    An static implementation of the hn_list utility
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>

#include "hn_logging.h"
#include "hn_list.h"

// maximum number of nodes in the list allowed in this static implementation
#define HN_LIST_MAX_POOL_SIZE  32

// maximum number of lists that can be created
#define HN_LIST_MAX_LISTS      32

//////////////////////////////////////////////////////////////////////////////

/*
 * List data type, which a local static heap to allocate elements 
 * and the corresponding presence vector to know which indexes has been allocated
 */
typedef struct list_st
{
  list_node_t *head;  //Pointer to the head of the list
  list_node_t *tail;  //Pointer to the tail of the list

  // pool of nodes for the list
  list_node_t pool_nodes[HN_LIST_MAX_POOL_SIZE];   

  // presence vector, which pool entries are busy
  int pool_busy[HN_LIST_MAX_POOL_SIZE];   

  // condition variable for the num_iter_active variable
  pthread_cond_t  cond_num_iter_active;

  // to know whether the list is in iteration mode currently. In that case it cannot be modified
  int             num_iter_active;
}list_t;


//////////////////////////////////////////////////////////////////////////////
/*
 * Function prototypes
 */

/*
 * Gets the next index free to use in the pool of nodes when inserting an element into the given listi
 * (internal use only)
 */
static int get_next_node_free_index(unsigned int id);

/*
 * Gets a free index in the pool list (internal use only)
 */
static int get_next_list_free_index();

/*
 * Checks whether the list is empty or not (internal use only)
 */
static int is_list_empty(list_t *lst);

/*
 * Removes the element in head position (internal use only)
 */
static void remove_head(list_t *lst);

/*
 * Removes the element in tail position (internal use only)
 */
static void remove_tail(list_t *lst);

/*
 * Cleanup routines for threads
 */
static void clean_pool_access_mutex(void *args);

///////////////////////////////////////////////////////////////////////////////

/*
 * Variable definition
 */


// pool of lists
static list_t pool_list[HN_LIST_MAX_LISTS]; 
static int    pool_list_entry_busy[HN_LIST_MAX_LISTS];

// to access to the pool in a thread safa mode
static pthread_mutex_t pool_access_mutex = PTHREAD_MUTEX_INITIALIZER;


////////////////////////////////////////////////////////////////////////////////

/*
 * Function definition
 */

// Gets the next index free in the pool of nodes for the given list
int get_next_node_free_index(unsigned int id)
{
  list_t *lst = &(pool_list[id]);
  if (lst->tail == NULL) return 0;

  int last = (ptrdiff_t)(lst->tail - lst->pool_nodes);
  int next = last;
  do
  {
    next = (next + 1) % HN_LIST_MAX_POOL_SIZE;
    //log_verbose("getting next index for list %u --> next=%d, last=%d", id, next, last);
    if (lst->pool_busy[next] == 0) return next;
  }while(next != last);

  return -1;
}

// Gets the next index free in the pool of lists
int get_next_list_free_index()
{
  static int last_busy = -1;

  if (last_busy == -1) 
  {
    last_busy = 0;
    return 0;
  }

  register int next = last_busy;
  do
  {
    next = (next + 1) % HN_LIST_MAX_LISTS;
    //log_verbose("getting next list index free --> next=%d, last=%d", next, last_busy);
    if (pool_list_entry_busy[next] == 0) 
    {
      last_busy = next;
      return next;
    }
  }while(next != last_busy);

  return -1;
}

// utility function used by other ones. Take into account that it is not thread safe
int is_list_empty(list_t *lst)
{
  return ((lst == NULL) || (lst->tail == NULL));
}

// utility function to remove the node in head of the list. Take into account that it is not thread safe
void remove_head(list_t *lst)
{
  if (lst != NULL)
  {
    int index = (ptrdiff_t)(lst->head - lst->pool_nodes);

    lst->head = lst->head->next;

    if ((lst->head != NULL) && (lst->head == lst->tail))
    {
      // now there is an element only
      lst->head->next = NULL;
      lst->head->prev = NULL;
    }

    // Is it the unique element in the list
    if (lst->head == NULL)
      lst->tail = NULL;

    lst->pool_busy[index] = 0;
    lst->pool_nodes[index].next = NULL;
    lst->pool_nodes[index].prev = NULL;
  }
}

// utility function to remove the node in tail of the list. Take into account that it is not thread safe
void remove_tail(list_t *lst)
{
  if (lst != NULL)
  {
    int index = (ptrdiff_t)(lst->tail - lst->pool_nodes);

    lst->tail = lst->tail->prev;

    if ((lst->tail != NULL) && (lst->head == lst->tail))
    {
      // now there is an element only
      lst->head->next = NULL;
      lst->head->prev = NULL;
    }

    // Is it the unique element in the list
    if (lst->tail == NULL)
      lst->head = NULL;

    lst->pool_busy[index] = 0;
    lst->pool_nodes[index].next = NULL;
    lst->pool_nodes[index].prev = NULL;
   }
}

// Creates a new list returning an Id to access to it and -1 if it cannot be created
int hn_list_create_list()
{
  int next_free = get_next_list_free_index();

  pthread_cleanup_push(clean_pool_access_mutex, NULL);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();
  
  log_verbose("hn_list_create_list: got a free list index at %d", next_free);

  if (next_free > -1) 
  {
    // initialize mutex and condition variable for this list
    int rs_cond   = pthread_cond_init(&pool_list[next_free].cond_num_iter_active, NULL);
    pool_list[next_free].num_iter_active = 0;

    if (rs_cond == 0)
    { // everything fine!!!
      // allocate memory for the list
      pool_list_entry_busy[next_free] = 1;

      // initialize the list
      pool_list[next_free].head = NULL;
      pool_list[next_free].tail = NULL;

      memset(pool_list[next_free].pool_nodes, 0, HN_LIST_MAX_POOL_SIZE*sizeof(list_node_t));
      memset(pool_list[next_free].pool_busy, 0, HN_LIST_MAX_POOL_SIZE*sizeof(int));
    } 
    else
    { // something wrong!!! let's free the resources
      pthread_cond_destroy(&pool_list[next_free].cond_num_iter_active);
      next_free = -1;
    }
  }

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return next_free;
}

void hn_list_destroy_list(unsigned int id)
{
  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    if (pool_list_entry_busy[id] == 1)
    {
      // now, let's destroy the list by deallocating resources
      pool_list_entry_busy[id] = 0; 
      pthread_cond_destroy(&pool_list[id].cond_num_iter_active);
      log_verbose("hn_list: destroyed list %u", id);
    }
  }

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);
}

// Adds to the begining of the list
int hn_list_add_to_begin(unsigned int id, void *ele)
{
  int res = -1;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    int next_free = get_next_node_free_index(id);
    if (next_free >= 0)
    {
      int empty = is_list_empty(lst);

      // allocate an element in the pool and insert the element
      lst->pool_busy[next_free] = 1;
      lst->pool_nodes[next_free].data = ele;
      lst->pool_nodes[next_free].next = NULL;
      lst->pool_nodes[next_free].prev = NULL;

      log_verbose("hn_list_add_to_begin: Index got %d, list empty? %s", next_free, empty? "yes" : "no");

      if (empty)
      {
        lst->head = &(lst->pool_nodes[next_free]);
        lst->tail = lst->head;
      }
      else
      {
        // update pointers
        // First, backup copy of  head pointer
        list_node_t *old_head = lst->head;

        // Second, the element just inserted is the new head 
        lst->head = &(lst->pool_nodes[next_free]);

        // Third, the old head previous field will point to new head
        old_head->prev = lst->head;

        // Four, the next field of the new head will point to the old head
        lst->head->next = old_head;

        // Five, the tail is not moved
      }

      res = 0;
    }
  }

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return res;
}


// Adds an element to end of the list
int hn_list_add_to_end(unsigned int id, void *ele)
{
  int res = -1;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    int next_free = get_next_node_free_index(id);
    if (next_free >= 0)
    {
      int empty = is_list_empty(lst);

      // allocate an element in the pool and insert the element
      lst->pool_busy[next_free] = 1;
      lst->pool_nodes[next_free].data = ele;
      lst->pool_nodes[next_free].next = NULL;
      lst->pool_nodes[next_free].prev = NULL;

      log_verbose("hn_list_add_to_end: Index got %d, list empty? %s", next_free, empty? "yes" : "no");

      if (empty)
      {
        lst->head = &(lst->pool_nodes[next_free]);
        lst->tail = lst->head;
      }
      else
      {
        // update pointers
        // First, backup copy of tail pointer
        list_node_t *old_tail = lst->tail;

        // Second, the element just inserted is the new tail
        lst->tail = &(lst->pool_nodes[next_free]);

        // Third, the old tile next field will point to new tail
        old_tail->next = lst->tail;

        // Four, the previous field of the new tail will point to the old tail
        lst->tail->prev = old_tail;

        // Five, the head is not moved
      }

      res = 0;
    }
  }

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return res;
}


// Removes the element in head position
void hn_list_remove_head(unsigned int id)
{
  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    if (!is_list_empty(lst)) 
    {
      remove_head(lst);
    }
  }

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);
}


// Removes the element in tail position
void hn_list_remove_tail(unsigned int id)
{
  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    if (!is_list_empty(lst)) 
    {
      remove_tail(lst);
    }
  }

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);
}


// Removes the n-th element in the list. Elements start at 0
void hn_list_remove_n_th(unsigned int id, int n_th)
{
  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    if (!is_list_empty(lst)) 
    {
      // iterate upto the n-th element (if it exists)
      int i = 0;
      list_t *lst = &(pool_list[id]);
      list_node_t *current = lst->head;
      while ((current != lst->tail) && (i < n_th))
      {
        i++;
        current = current->next;
      }

      if (i == n_th) 
      {
        // the n-th element exists
        if (current == lst->head)
          remove_head(lst);
        else if (current == lst->tail)
          remove_tail(lst);
        else
        {
          // neither the head nor the tail element
          //
          int index = (ptrdiff_t)(current - lst->pool_nodes);

          current->prev->next = current->next;
          current->next->prev = current->prev;

          lst->pool_busy[index] = 0;
          lst->pool_nodes[index].next = NULL;
          lst->pool_nodes[index].prev = NULL;
        }
      }
    }
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);
}


// Removes the element at the given index
void hn_list_remove_element_at(unsigned int id, unsigned int index)
{
  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    if ((lst != NULL) && !is_list_empty(lst) && (index < HN_LIST_MAX_POOL_SIZE) && (lst->pool_busy[index] == 1)) 
    {
      // index inside of allowed range and allocated
      //
      list_node_t *current = (list_node_t *)(lst->pool_nodes + index);
      if (current == lst->head)
        remove_head(lst);
      else if (current == lst->tail)
        remove_tail(lst);
      else
      {
        // neither the head nor the tail element
        //
        current->prev->next = current->next;
        current->next->prev = current->prev;

        lst->pool_busy[index] = 0;
        lst->pool_nodes[index].next = NULL;
        lst->pool_nodes[index].prev = NULL;
      }
    }
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);
}

// Removes all the elements in the list (resets the list)
void hn_list_remove_all(unsigned int id)
{
  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    // wait until all the iteration operations finish before destroying this list
    while (lst->num_iter_active > 0)
    {
      pthread_cond_wait(&lst->cond_num_iter_active, &pool_access_mutex);
    }

    if (lst != NULL) 
    {
      lst->head = NULL;
      lst->tail = NULL;

      memset(lst->pool_nodes, 0, HN_LIST_MAX_POOL_SIZE*sizeof(list_node_t));
      memset(lst->pool_busy, 0, HN_LIST_MAX_POOL_SIZE*sizeof(int));
    }
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);
}

// Gets the element at head
void *hn_list_get_data_at_head(unsigned int id)
{
  void *res = NULL;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    if ((lst != NULL) && (lst->head != NULL))
    {
      res = lst->head->data;
    }
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return res;
}

// Gets the element at tail
void *hn_list_get_data_at_tail(unsigned int id)
{
  void *res = NULL;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    if ((lst != NULL) && (lst->tail != NULL))
    {
      res = lst->tail->data;
    }
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return res;
}

// Gets the data corresponding to the n-th elment of the list. Elements start at 0
void *hn_list_get_data_n_th(unsigned int id, int n_th)
{
  void *res = NULL;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    if ((lst != NULL) && (lst->head != NULL))
    {
      // iterate upto the n-th element (if it exists)
      int i = 0;
      list_node_t *current = lst->head;
      while ((current != lst->tail) && (i < n_th))
      {
        i++;
        current = current->next;
      }

      if (i == n_th) 
      {
        // the n-th element exists
        res = current->data;
      }
    }
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return res;
}


// Gets the data of the element at the given index
void *hn_list_get_data_at_position(unsigned int id, unsigned int index)
{
  void *res = NULL;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
   
    if (lst != NULL)
    {
      if ((index < HN_LIST_MAX_POOL_SIZE) && (lst->pool_busy[index] == 1))
      {
        list_node_t *current = (list_node_t *)(lst->pool_nodes + index);
        res = current->data;
      }
    }
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return res;
}

// Checks the empty status of the list
int hn_list_is_empty(unsigned int id)
{
  int res = 1;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);
  pthread_testcancel();
  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);
    res = is_list_empty(lst); 
  }  

  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);

  return res;
}


// Gets an iterator to the begining of the list
int hn_list_iter_begin(unsigned int id)
{
  assert(0); // this function is valid never more 
  return 0;
}



// checks if the iteration process has to be finished
int hn_list_iter_exit(unsigned int id, int iter)
{
  assert(0); // this function is valid never more
  return 0;
}

// Gets an iterator to the head of the list
int hn_list_iter_front_begin(unsigned int id)
{
  int iter = HN_LIST_ITER_END;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);  
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);

    if ((lst != NULL) && (lst->head != NULL))
    {
      lst->num_iter_active++;
      iter = (ptrdiff_t)(lst->head - lst->pool_nodes);
      pthread_cond_broadcast(&lst->cond_num_iter_active);
    }
    
  }
  
  pthread_mutex_unlock(&pool_access_mutex); 
  pthread_cleanup_pop(0);

  return iter;
}


// Gets an iterator to the tail of the list
int hn_list_iter_reverse_begin(unsigned int id)
{
  int iter = HN_LIST_ITER_END;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);  
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);

    if ((lst != NULL) && (lst->tail != NULL))
    {
      lst->num_iter_active++;
      iter = (ptrdiff_t)(lst->tail - lst->pool_nodes);
      pthread_cond_broadcast(&lst->cond_num_iter_active);
    }
  }
  
  pthread_mutex_unlock(&pool_access_mutex); 
  pthread_cleanup_pop(0);

  return iter;
}

// finishes the iteration process 
int hn_list_iter_end(unsigned int id, int iter)
{
  int res = 0;
  if (iter == HN_LIST_ITER_END)
  {
    pthread_cleanup_push(clean_pool_access_mutex, &id);
    pthread_mutex_lock(&pool_access_mutex); 
    pthread_testcancel();

    res = 1;
    if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
    {
      list_t *lst = &(pool_list[id]);
      lst->num_iter_active--;
      pthread_cond_broadcast(&lst->cond_num_iter_active);
    }

    pthread_mutex_unlock(&pool_access_mutex); 
    pthread_cleanup_pop(0);
  }

  return res;
}

// Gets an iterator to the next element of the list
int hn_list_iter_next(unsigned int id, int iter)
{
  int next_iter = HN_LIST_ITER_END;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex); 
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);

    if ((iter >= 0) && (iter < HN_LIST_MAX_POOL_SIZE) && (lst != NULL) && (lst->pool_busy[iter] == 1))
    {
      list_node_t *node = (list_node_t *)(lst->pool_nodes + iter);
      if (node != lst->tail)
      {
        node = node->next;
        next_iter = (ptrdiff_t)(node - lst->pool_nodes);
      }
    }
  }
  
  pthread_mutex_unlock(&pool_access_mutex); 
  pthread_cleanup_pop(0);

  return next_iter;
}

// Gets an iterator to the next element of the list
int hn_list_iter_back(unsigned int id, int iter)
{
  int next_iter = HN_LIST_ITER_END;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);  
  pthread_testcancel();

  if ((id < HN_LIST_MAX_LISTS) && (pool_list_entry_busy[id] == 1))
  {
    list_t *lst = &(pool_list[id]);

    if ((iter >= 0) && (iter < HN_LIST_MAX_POOL_SIZE) && (lst != NULL) && (lst->pool_busy[iter] == 1))
    {
      list_node_t *node = (list_node_t *)(lst->pool_nodes + iter);
      if (node != lst->head)
      {
        node = node->prev;
        next_iter = (ptrdiff_t)(node - lst->pool_nodes);
      }
    }
  }
  
  pthread_mutex_unlock(&pool_access_mutex); 
  pthread_cleanup_pop(0);

  return next_iter;
}

void clean_pool_access_mutex(void *args) 
{
  unsigned int id = *((unsigned int *)args);
  log_verbose("hn_list: cleanup routine called on global pool access", id);
  pool_list[id].num_iter_active = 0;
  pthread_cond_broadcast(&pool_list[id].cond_num_iter_active);
  pthread_mutex_unlock(&pool_access_mutex);
}

