#ifndef __HN_FILTER_H__
#define __HN_FILTER_H__ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: february 20, 2018
// Design Name:
// Module Name: Heterogeneous node library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    hn library
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif


// hn_filter modes
#define HN_FILTER_APPL_MODE_CONSOLE       0
#define HN_FILTER_APPL_MODE_DEBUG         1
#define HN_FILTER_APPL_MODE_ECHO          2
#define HN_FILTER_APPL_MODE_RAWFRAMES     3
#define HN_FILTER_APPL_MODE_SYNC_READS    4
#define HN_FILTER_APPL_MODE_UNIT_INFO     16
//#define HN_FILTER_APPL_MODE_UNIT_STATS    17  // not used yet, need to read arch id....
#define HN_FILTER_APPL_MODE_NONE          255  // this application mode will not be reading data from daemon...

// hn_filter targets
#define HN_FILTER_TARGET_MANGO            0
#define HN_FILTER_TARGET_PEAK             1
#define HN_FILTER_TARGET_NUPLUS           2
#define HN_FILTER_TARGET_DCT              3
#define HN_FILTER_TARGET_TETRAPOD         4

// hn_filter special tiles
#define HN_FILTER_ALL_TILES               999


  /*!
 * \brief data type used to represent an application filter
 */
typedef struct hn_filter_st
{
  unsigned int target;
  unsigned int mode;
  unsigned int tile;
  unsigned int core;
}hn_filter_t;


#ifdef __cplusplus
}
#endif


#endif // ifndef __HN_FILTER_H__
