#ifndef __HN_TETRAPOD_H__
#define __HN_TETRAPOD_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: February 12, 2018
// File Name: hn_tetrapod.h
// Design Name:
// Module Name: HN library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    HN functions for tetrapod header file
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//! \file

#ifdef __cplusplus
extern "C" {
#endif

//--------------------------------------------------------------------------------------------------
//BBBBBBBBBEEEEEEEEEEEEEEWWWWWWWWWWWWWWAAAAAAAAAAARRRRRRRRRRRRREEEEEEEEEEEEE!!!!!!!!!!!!!!!!!!!!!!!!
//
// TETRAPOD MODELS GO FROM 150 TO 199 INCLUSIVE TO BE ALIGNED WITH BBQUE
//-------------------------------------------------------------------------------------------------- 
/*!
 * \brief Model id for the 4-depth 4-width data path configuration
 */
#define HN_TETRAPOD_MODEL_DE4WI4  150 

/*!
 * \brief Returns an string representation for the unit model given
 *
 * \param [in] model the model the string representation will be for
 * \return an string representatiton of the model
 * \retval NULL the model does not match any of the supported models
 */
const char *hn_tetrapod_to_str_unit_model(uint32_t model);

/*!
 * \brief Gets the utilization of a TETRAPOD unit
 *
 * \param [in] tile the tile the TETRAPOD unit utilization is going to be obtained for
 * \param [out] utilization the utilization of the given unit
 * \retval HN_SUCCEEDED in case of success or one of the error codes in hn.h otherwise
 */
uint32_t hn_tetrapod_get_utilization(uint32_t tile, uint32_t *utilization);

#ifdef __cplusplus
}
#endif

#endif
