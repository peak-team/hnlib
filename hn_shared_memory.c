///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomam10@gap.upv.es)
//
// Create Date: February 12, 2018
// File Name: hn_shared_memory.h
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

// shm operations
#include <sys/mman.h>
#include <sys/stat.h>        // For mode constants
#include <fcntl.h>           // For O_* constants
#include <unistd.h>          // for close() function call


#include "hn_logging.h"

#define SHM_POSIX_NAME_MAX_LEN        64

static const char *base_name = "/hn_shm_buffer"; // file name
static uint32_t    bname_index = 0; // buffer name index, used to create buffer name

/*
 * \brief
 *
 * \param[in] name name of the file (buffer in shm) to open
 * \param[in] size size of the requested buffer
 * \param[in] ptr double pointer to store base address of the opened buffer in shm
 * \param[out] fd file descriptor associated to the opened file
 * \param[in] oflgas flags for shm_open function (file opening flags)
 * \param[in] pflags flags for mmap function (file access flags)
 * \param[in] mode 
 * \retrurn 0 if buffer in shared memory area was succesfully opened, otherwise return error code
 */
uint32_t hn_shared_memory_open(char *name, uint32_t size, void **ptr, int *fd, int oflags, int pflags, int mode);


/*
 *
 */
uint32_t hn_shared_memory_open(char *name, uint32_t size, void **ptr, int *fd, int oflags, int pflags, int mode)
{
  int       shm_fd;    // file descriptor, from shm_open()
  void     *shm_base;  // base address, from mmap()
  int       rv_ftr;    // return value for ftruncate

  //log_debug("shared memory buffer opened with name: %s    size: %u", name, size);
  
  // create the shared memory segment as if it was a file
  shm_fd = shm_open(name, oflags, mode);
  if (shm_fd == -1) {
    log_error("opening shm buffer, opening shared memory with name %s  failed: %s\n", name, strerror(errno));
    return 1;
  }

  // configure the size of the shared memory segment
  rv_ftr = ftruncate(shm_fd, size);

  if (rv_ftr != 0)
  {
    log_error("could not allocate mem for the shm buffuer, ftruncate failed: %s\n", strerror(errno));
    // close and shm_unlink?
    close(shm_fd);
    return 2;
  }

  // map the shared memory segment to the address space of the process
  shm_base = mmap(0, size, pflags, MAP_SHARED, shm_fd, 0);
  if (shm_base == MAP_FAILED) {
    log_error("creating shm buffer, map failed: %s\n", strerror(errno));
    // close and shm_unlink?
    close(shm_fd);
    return 1;
  }
 
  log_debug("shared memory buffer opened with name: %s    fd: %d    ptr:0x%08X", name, shm_fd, shm_base);

  *ptr = shm_base;
  *fd  = shm_fd;

  return 0;
}

/*
 *
 */
uint32_t hn_shared_memory_open_buffer(char *name, uint32_t size, void **ptr, int *fd)
{
  uint32_t rv;

  //rv = hn_shared_memory_open(size, ptr, fd, name, O_READONLY, PROT_READ, 0666);
  // since both daemon and application can write on the buffer, both sides can read and write
  rv = hn_shared_memory_open(name, size, ptr, fd, O_RDWR, PROT_READ | PROT_WRITE, 0666);
  
  return rv;
}

/*
 *
 */
uint32_t hn_shared_memory_create_buffer(char *name, uint32_t size, void **ptr, int *fd)
{
  uint32_t rv = 0;
  uint32_t it = 100;

  do {
  // create
  //set name for requested shm buffer
    snprintf(name, SHM_POSIX_NAME_MAX_LEN, "%s_%u", base_name, bname_index);
    bname_index++;
    rv = hn_shared_memory_open(name, size, ptr, fd, O_CREAT | O_RDWR | O_EXCL, PROT_READ | PROT_WRITE, 0666);
    it--;
    if (rv != 0){
      log_warn("creating new buffer in shm area, buffer with name %s already exists... another daemon is running or a previous run was wrongly closed. Please manually remove unused buffers", name);
      log_debug("   attempt #%u", (100-it));
    }
  } while ((rv != 0) && (it> 0));

  if(rv == 0)log_debug("   buffer created after %u attempts", (100-it));
  else log_debug("   buffer Not created, return error");
  return rv;
}

/*
 *
 */
uint32_t hn_shared_memory_close_buffer(uint32_t size, void *shm_addr, int shm_fd)
{
  // remove the mapped shared memory segment from the address space of the process
  if (munmap(shm_addr, size) != 0)
  {
    log_error("shared memory close_buffer unmap failed: %s", strerror(errno));
    errno = 0;
    return 1;
  }

  // close the shared memory segment as if it was a file
  if (close(shm_fd) != 0)
  {
    log_error("shared memory close_buffer close failed: %s", strerror(errno));
    errno = 0;
    return 2;
  }

  return 0;
}

/*
 *
 */
uint32_t hn_shared_memory_destroy_buffer(char *name, uint32_t size, void *shm_addr, int shm_fd)
{
  int rv;

  // close
  rv = hn_shared_memory_close_buffer(size, shm_addr, shm_fd);
  if (rv != 0)
  {
    log_error ("shared memory destroy_buffer, close buffer failed: %u", rv);
    return (uint32_t)rv;
  }

  // destroy
  // remove the shared memory segment from the file system
  rv = shm_unlink(name);
  if (rv != 0)
  {
    log_error("shared memory destroy_buffer, unlinking buffer %s: %s", name, strerror(errno));
    errno = 0;
    return 1;
  }

  return 0;
 }

/*
 *
 */
//uint32_t hn_shared_memory_write_to_ddr_memory(void *ptr, uint32_t size, uint32_t address_dst, uint32_t tile_dst, uint32_t blocking_transfer)
//{
//  return 1;
//}

/*
 *
 */
//uint32_t hn_shared_memory_read_from_ddr_memory(void *ptr, uint32_t size, uint32_t address_dst, uint32_t tile_dst, uint32_t blocking_transfer)
//{
//  return 1;
//}

//*********************************************************************************************************************
// end of file hn_shared_memory.c
//*********************************************************************************************************************
