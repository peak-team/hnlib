# HN Makefile

instdir    ?= /opt/mango
build_type ?= "Release"   # pass build_type=Debug on command line to enable compilation with debug

sources := hn.c hn_peak.c hn_nuplus.c hn_dct.c hn_tetrapod.c hn_utils.c hn_logging.c hn_shared_memory.c hn_list.c hn_rr_arb.c
images  := img/peakos.img img/peak_protocol.img
headers := $(sources:.c=.h) hn_filter.h
objects := $(sources:.c=.o)
name    := libhn
CC      := gcc
CFLAGS  += -Wall -fPIC
INCLUDE_PATH := -I./hn_daemon -I./


ifeq ($(build_type),Debug)
	CFLAGS += -g -DDEBUG 
endif

ifeq ($(build_type),Verbose)
  CFLAGS += -g -DDEBUG -DVERBOSE
endif


.PHONY: all install install_libs install_headers clean

all: $(name).a $(name).so
	@echo "HN library successfully built"

%.o: %.c
	@echo "Building " $< "..."
	$(CC) $(CFLAGS) $(INCLUDE_PATH) -c $< -o $@ -lrt

$(name).a: $(objects)
	ar -cvr $@ $(objects)

$(name).so: $(objects)
	$(CC) -shared -o $@ $(objects) -lrt


install: install_libs  install_headers
	@echo "HN library: Installing images..."
	@if [ ! -d "$(instdir)/usr/share" ]; then mkdir -p $(instdir)/usr/share; fi
	@cp $(images) $(instdir)/usr/share
	@cd test_bitfiles && sudo instdir=$(instdir) make install && cd ..
	@echo "HN library: Installation complete!"
	@echo

install_libs: $(instdir)/lib/$(name).so $(instdir)/lib/$(name).a

$(instdir)/lib/$(name).so: $(name).so
	@echo "HN library: Installing " $^
	@if [ ! -d "$(instdir)/lib" ]; then mkdir -p $(instdir)/lib; fi
	@cp $^ $(instdir)/lib/

$(instdir)/lib/$(name).a: $(name).a
	@echo "HN library: Installing " $^
	@if [ ! -d "$(instdir)/lib" ]; then mkdir -p $(instdir)/lib; fi
	@cp $^ $(instdir)/lib/

install_headers: $(instdir)/include/$(name)

$(instdir)/include/$(name): $(headers)
	@echo "HN library: Installing headers..."
	@if [ ! -d "$@" ]; then mkdir -p $@; fi
	@cp $(headers) $@

clean :
	@rm -f *.o *.a
	@rm -f *.so

