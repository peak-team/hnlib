///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  V. Scotti (vinc94@gmail.com)

#include "hn_rr_arb.h"
#include "hn_logging.h"

#include <stddef.h>

rr_arb_t hn_rr_arb_create(uint32_t requestors, uint32_t *weights, uint32_t num_weights)
{
  rr_arb_t arb;

  arb.requests_num = requestors;
  arb.index = requestors-1;
  arb.counter = 0;
  arb.weighted_arb = 0;
  arb.weights = num_weights != 0 ? weights : NULL;
  arb.num_weights = num_weights;
  arb.weight_ptr = num_weights-1;

  return arb;
}

uint32_t rr_arb_next(rr_arb_t *arb)
{
  if (arb->weighted_arb == 0 && arb->weights != NULL) {
    arb->weighted_arb = 1;

    return arb->weights[arb->weight_ptr];
  } else {
    arb->index++;
    arb->index %= arb->requests_num;
    arb->counter++;

    return arb->index;
  }
}

void rr_arb_stop(rr_arb_t *arb)
{
  arb->counter = 0;
  arb->weighted_arb = 0;

  if (arb->num_weights != 0) {
    arb->weight_ptr++;
    arb->weight_ptr %= arb->num_weights;
  }
}

uint32_t rr_arb_round_complete(rr_arb_t *arb)
{
  return arb->counter == arb->requests_num;
}
