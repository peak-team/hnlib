#ifndef __HN_PEAK_H__
#define __HN_PEAK_H__ 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: february 12, 2018
// File Name: hn_peak.h
// Design Name:
// Module Name: Heterogeneous node runtime library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    HN PEAK functions for HN library header file
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif


/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

//--------------------------------------------------------------------------------------------------
//BBBBBBBBBEEEEEEEEEEEEEEWWWWWWWWWWWWWWAAAAAAAAAAARRRRRRRRRRRRREEEEEEEEEEEEE!!!!!!!!!!!!!!!!!!!!!!!!
//
// PEAK MODELS GO FROM 0 TO 49 INCLUSIVE TO BE ALIGNED WITH BBQUE
//-------------------------------------------------------------------------------------------------- 
/*!
 * \brief PEAK Model ids
 */
#define HN_PEAK_MANYCORE_0      0
#define HN_PEAK_MANYCORE_1      1
#define HN_PEAK_MANYCORE_2      2
#define HN_PEAK_MANYCORE_3      3
#define HN_PEAK_MANYCORE_4      4
#define HN_PEAK_MANYCORE_5      5
#define HN_PEAK_MANYCORE_6      6


// Subcommands for PEAK cores for DEBUG_AND_STATS
#define HN_PEAK_COMMAND_ECHO_DISABLE        5
#define HN_PEAK_COMMAND_ECHO_ENABLE         6
#define HN_PEAK_COMMAND_CORE_DEBUG_DISABLE  21
#define HN_PEAK_COMMAND_CORE_DEBUG_ENABLE   22
#define HN_PEAK_COMMAND_CACHE_COHERENCE_PROTOCOL_DEBUG_DISABLE  5
#define HN_PEAK_COMMAND_CACHE_COHERENCE_PROTOCOL_DEBUG_ENABLE   6
#define HN_PEAK_COMMAND_CORE_FORENSE_DEBUG_DISABLE  23
#define HN_PEAK_COMMAND_CORE_FORENSE_DEBUG_ENABLE   24


/**********************************************************************************************************************
 **
 ** Public function prototypes
 **
 **********************************************************************************************************************
 **/

  // -------------------------------------------------------------------------------------------------
// hn_peak_to_str_unit_model
//   short: Returns an string representation for the unit model given
//   argument model: The model the string representation will be for
const char *hn_peak_to_str_unit_model(uint32_t model);

  // ---------------------------------------------------------------------------------------------
// hn_boot_peak
//   short: boots a PEAK unit in a tile
//   argument tile: Tile where the unit is located
//   argument tile_memory: Tile where the memory image (if needed) is located)
//   argument addr: Starting address of the memory image (if needed)
//   argument protocol_img_path: path of image file of memory coherence protocol
//   argument kernel_img_path: kernel image file path
uint32_t hn_peak_boot(uint32_t tile, uint32_t tile_memory, uint32_t addr, const char *protocol_img_path, const char *kernel_img_path);

// ---------------------------------------------------------------------------------------------------
// hn_peak_write_protocol
//   short: Loads a coherence protocol located in a file into a PEAK unit located in a given tile
//   argument file_name: File with the protocol image
//   argument tile: MANGO tile where the unit is located
uint32_t hn_peak_write_protocol(const char *file_name, uint32_t tile);

// --------------------------------------------------------------------------------------------------
// hn_peak_run_kernel
//  short: sets a kernel to run into a PEAK unit, the kernel image must be already loaded in 
//         memory and the TLB properly configured
//  argument tile: Tile where the PEAK unit is located
//  argument addr: Virtual address where the image will be found
//  argument args: string including all arguments
uint32_t hn_peak_run_kernel(uint32_t tile, uint32_t addr, char *args);

// --------------------------------------------------------------------------------------------------
// hn_peak_set_new_pc
//   short: Sets a PC address to a given core in a PEAK unit located in a given tile
//   argument tile: MANGO tile where the unit is located
//   argument core: PEAK core id
//   argument addr: New PC address to be written
uint32_t hn_peak_set_new_pc(uint32_t tile, uint32_t core, uint32_t addr);
    
// --------------------------------------------------------------------------------------------------
// hn_peak_unfreeze
//   short: Unfreezes a core in a PEAK unit located in a given tile
//   argument tile: MANGO tile where the unit is located
//   argument core: PEAK core ID to be woken up
uint32_t hn_peak_unfreeze(uint32_t tile, uint32_t core);   

// --------------------------------------------------------------------------------------------------
// hn_peak_reset
//   short: Resets PEAK unit located in a given tile
//   argument tile: MANGO tile where the PEAK unit is located
uint32_t hn_peak_reset(uint32_t tile);

// -------------------------------------------------------------------------------------------------
// hn_peak_get_console_char
//   short: Gets a console character from a UNIT (the MANGO tile is defined in the filter)
//   argument core: Core from where a character is received
//   argument c: pointer to character returned
uint32_t hn_peak_get_console_char(uint32_t *core, char *c);

// ---------------------------------------------------------------------------------------------------
// hn_peak_send_char
//   short: Sends a character to a PEAK core located in a certain unit
//   argument tile: Tile where the PEAK unit is located
//   argument core: Core inside PEAK who will receive the character
//   argument ch  : character
uint32_t hn_peak_send_char(uint32_t tile, uint32_t core, char c);

// ---------------------------------------------------------------------------------------------------
// send echo enable command to peak core in mango Tile
uint32_t hn_peak_echo_enable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// send echo disable command to peak core in mango Tile
uint32_t hn_peak_echo_disable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// send core debug enable command to peak core in mango Tile
uint32_t hn_peak_debug_core_enable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// send core debug disable command to peak core in mango Tile
uint32_t hn_peak_debug_core_disable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// send protocol debug enable command to peak core in mango Tile
uint32_t hn_peak_debug_protocol_enable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// send protocol debug disable command to peak core in mango Tile
uint32_t hn_peak_debug_protocol_disable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// send core forense debug enable command to peak core in mango Tile
uint32_t hn_peak_debug_core_forense_enable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// send core forense debug disable command to peak core in mango Tile
uint32_t hn_peak_debug_core_forense_disable(uint32_t tile, uint32_t core);

// ---------------------------------------------------------------------------------------------------
// hn_peak_get_utilization
//   short: Returns average utilization of all PEAK cores in a unit
//   argument tile: tile where the PEAK unit is located
//   argument utilization: Pointer to the variable where utilization is returned
uint32_t hn_peak_get_utilization(uint32_t tile, uint32_t *utilization);

// --------------------------------------------------------------------------------------------------
// hn_peak_read_register
//   short: Reads a TileReg register in the PEAK core in PEAK unit located in a given tile
//   argument tile: MANGO tile where the PEAK unit is located
//   argument core: PEAK tile where the TileReg register is located
//   argument addr: Address of the register
//   argument data: pointer where read data will be stored, 
//                  if NULL no read will be made, but command will be sent, useful for tx/rx term
uint32_t hn_peak_read_register(uint32_t tile, uint32_t core, uint32_t addr, uint32_t *data);

// --------------------------------------------------------------------------------------------------
// hn_peak_write_register
//   short: Writes a TileReg register in the PEAK core in PEAK unit located in a given tile
//   argument tile: MANGO tile where the PEAK unit is located
//   argument core: PEAK tile where the TileReg register is located
//   argument addr: Address of the register
//   argument data: Value to write
uint32_t hn_peak_write_register(uint32_t tile, uint32_t core, uint32_t addr, uint32_t data);

#ifdef __cplusplus
}
#endif

//*********************************************************************************************************************
// end of file hn_peak.h
//*********************************************************************************************************************
#endif
