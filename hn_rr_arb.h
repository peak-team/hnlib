#ifndef __HN_RR_ARB_H__
#define __HN_RR_ARB_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  V. Scotti (vinc94@gmail.com)

#include <stdint.h>

/*!
 * \brief Round robin arbiter structure
 */
typedef struct rr_arb_st
{
  uint32_t requests_num;
  uint32_t index;
  uint32_t counter;
  uint32_t weighted_arb;
  uint32_t *weights;
  uint32_t num_weights;
  uint32_t weight_ptr;
} rr_arb_t;

//! \name Arbiter creation and destruction
//@{ 

/*!
 * \brief Creates a new round robin arbiter
 *
 * \param [in] requestors The number of requestors
 * \param [in] weights The pointer to weights vector, or NULL
 * \param [in] num_weights The number of weights in the vector, if any
 * \returns the arbiter
 */
rr_arb_t hn_rr_arb_create(uint32_t requestors, uint32_t *weights, uint32_t num_weights);

//@}

//! \name Arbitration functions
//@{

/*!
 * \brief Get the index of the next client to be polled
 *
 * \param [in] arb The arbiter
 */
uint32_t rr_arb_next(rr_arb_t *arb);

/*!
 * \brief Marks the current requestor as being served
 *
 * \param [in] arb The arbiter
 */
void rr_arb_stop(rr_arb_t *arb);

/*!
 * \brief Returns 1 if all requestors have been polled
 *
 * \param [in] arb The arbiter
 */
uint32_t rr_arb_round_complete(rr_arb_t *arb);

//@}

#endif
