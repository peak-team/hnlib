///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node background communication process
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __HN_UTILS_H__
#define __HN_UTILS_H__


/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdint.h>
#include <sys/time.h>

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/

/******************************************************************************
 * @brief gets the decimal value represented by a character representing an HEX
 *
 * @param digit ASCII char to process
 * 
 * @return value of character passed as parameter
 ******************************************************************************
 **/
unsigned char hn_utils_hex_to_val(char digit);

/******************************************************************************
 * @brief Convert 32 bit hex value as char string to its value 
 *
 * @param hex_value string containing hex value
 * 
 * @return converted value
 ******************************************************************************
 **/unsigned long int hn_utils_hex_addr_to_ul( uint8_t hex_value[8]);

float get_elapsed_time_seconds(struct timespec start, struct timespec end);

#endif

#ifdef __cplusplus
}
#endif

//*********************************************************************************************************************
// end of file hn_utils.cpp
//*********************************************************************************************************************

