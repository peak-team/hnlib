///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 22, 2018
// File Name: hn_dct.c
// Design Name:
// Module Name: HN library
// Project Name:  MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    HN functions for DCT. Implementation file
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.0.1 - Created File - R. Tornero (ratorga@gap.upv.es)
//   TODO add here revisions and author
//
// Additional Comments: NONE
//
//   TODO describe here additions
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

#include "hn.h"
#include "hn_private_common.h"
#include "hn_dct.h"


const char *hn_dct_to_str_unit_model(uint32_t model) {
  switch(model) {
    case HN_DCT_MODEL_BASE         : return "DCT0";
    default                        : return NULL;
  }
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_dct_get_utilization(uint32_t tile, uint32_t *utilization)
{
  HN_INITIALIZE_CHECK;
  // TODO
  *utilization = 10;
  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_dct_run_kernel(uint32_t tile)
{
  HN_INITIALIZE_CHECK;
  log_debug("hn_dct_run_kernel: tile %d", tile);

  // We send the key strokes to PEAKos to launch the kernel
  uint32_t rv;
  rv = hn_write_register(tile, 0x00000005, 0x00000001);
  if (rv != HN_SUCCEEDED) {
      return rv;
  }

  return HN_SUCCEEDED;
}

