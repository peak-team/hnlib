# What's in this folder?

This folder contains:

1. The bitstream files for the mango architecture 17. 
2. The proFPGA configuration file (.cfg) to be used with the *profpga_run* tool for 
uploading and configuring the required FPGAs in the HN cluster connected to GN0 appropiately.
3. This README.md file.

## ARCHITECTURE DESCRIPTION

The mango_arch_17 architecture consists of a 2x2 2D mesh of mango tiles, which are implemented in a
single Virtex 7 V2000T FPGA plugged in the proFPGA motherboard 2 of the HN cluster connected to GN0.

The connection to the HN cluster is based on MMI64/USB.

### Type of Units

Next, the type of unit for each tile.

* TILE 0: PEAK with 2 cores
* TILE 1: PEAK with 2 cores
* TILE 2: PEAK with 2 cores
* TILE 3: PEAK with 2 cores

Every unit works at 40Mhz.

### Memories

* TILE 0: 2GB DDR3


