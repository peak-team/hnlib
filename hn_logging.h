#ifndef __HN_LOGGING_H__
#define __HN_LOGGING_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_logging.h
// Design Name:
// Module Name: HN library
// Project Name: MANGO
// Target Devices: 
// Tool Versions:
// Description:
//
//    HN logging system header
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//! \file

/*! 
 * \brief logs debug messages 
 *
 * \TODO depending on compilation log_debug messages are recorder or not
 */
#ifdef DEBUG
  #define log_debug _log_debug
#else
  #define log_debug(...) ((void) 0)
#endif

/*! 
 * \brief logs verbose messages 
 *
 * \TODO depending on compilation log_verbose messages are recorder or not
 */
#ifdef VERBOSE
  #define log_verbose _log_verbose
#else
  #define log_verbose(...) ((void) 0)
#endif


//! \brief compile flag to enable/disable log in a pthread safe way
#define HN_LOGGING_PTHREAD_SAFE


/*!
 * \enum hn_log_types_t
 * \brief types of logging supported
 */
typedef enum 
{ 
  HN_LOG_TO_FILE=0, 
  HN_LOG_TO_STDOUT 
}hn_log_type_t;


//! Default log file
#define HN_LOG_DEFAULT_FILE "/tmp/hn_messages.log"

//! \name Initialization and shutdown
//@{


/*! 
 * \brief Initializes the logging system
 *
 * There are two log modes supported: I) printing to the stdout and II) writing to a file
 *
 * \param [in] mode desired mode
 * \parblock
 * The possible modes consist of the macros HN_LOG_TO_FILE or HN_LOG_TO_STDOUT
 * \endparblock
 * \param [i] flog_name name of the logging file to use when HN_LOG_TO_FILE mode has been set
 * \parblock
 * This parameter can be set to NULL. In that case the HN_LOG_DEFAULT_FILE value is used as file name
 * \parblock
 * \retval  0 In case of success
 * \retval -1 In case of failure
 */
int hn_log_initialize(hn_log_type_t mode, char *flog_name);

/*!
 * \brief Closes the logging system
 * 
 * \retval 0 always
 */
int hn_log_close();

//@}

//! \name Logging level functions
//@{

/*!
 * \brief logs error messages
 */
void log_error(const char *fmt, ...);

/*!
 * \brief logs warning messages
 */
void log_warn(const char *fmt, ...);

/*!
 * \brief logs info messages
 */
void log_info(const char *fmt, ...);

/*!
 * \brief logs notice messages
 */
void log_notice(const char *fmt, ...);

/*!
 * \brief logs debug messages
 */
void _log_debug(const char *fmt, ...);

/*!
 * \brief logs verbose messages
 */
void _log_verbose(const char *fmt, ...);

//@}

#endif
