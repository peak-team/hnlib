///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 30, 2018
// File Name: test_application_server.c
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Test for the application server master/slave threads
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/select.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <time.h>

#define _HN_SHARED_STRUCTURES_DEFINITION
#include "hn_daemon.h"
#include "hn_logging.h"
#include "hn_exit_program.h"
#include "hn_shared_structures.h"

int main(int argc, char **argv)
{

  int sock;
  struct sockaddr_un server;

  sock = socket(AF_UNIX, SOCK_STREAM, 0);
  if (sock < 0)
  {
    perror("opening socket");
    exit(1);
  }

  server.sun_family = AF_UNIX;
  strcpy(server.sun_path, HN_DAEMON_SOCKET_UNIX_PORT);

  if (connect(sock, (struct sockaddr *)&server, sizeof(struct sockaddr_un)) < 0)
  {
    close(sock);
    perror("connecting to the socket");
    exit(1);
  }

  srandom(time(NULL));

  hn_exit_program_install_handler(SIGINT);
  hn_log_initialize(HN_LOG_TO_STDOUT, NULL);

  // Intermediate buffer used for reading & writing the socket
  unsigned char buf_read[2048];
  unsigned char buf_write[2048];
  int i;

  for (i = 0; i < 2048; i++)
    buf_write[i] = (unsigned char)random();

  // for select+read&write,
  fd_set rfds;
  fd_set wfds;
  struct timeval tv;
  int select_rs;
  int wait_read = 0;
  while (!hn_exit_program_is_terminated())
  {
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    FD_SET(sock, &rfds);
    FD_SET(sock, &wfds);
    /* wait upto 5 s */
    tv.tv_sec = 5;
    tv.tv_usec = 0;

    select_rs = select(sock+1, &rfds, &wfds, NULL, &tv);
    if (select_rs > 0)
    {
      int ready_read  = FD_ISSET(sock, &rfds);
      int ready_write = FD_ISSET(sock, &wfds);

      if (ready_read)
      { // the socket has data to read from
        log_debug("client: socket available for reading");

        size_t bytes_read = read(sock, buf_read, 2048);
        if (bytes_read == 0)
        { // connection closed on the other end
          log_debug("client: exiting in read because the other end has disconnected");
          hn_exit_program_terminate(0);
          break;
        } 
        else if (bytes_read > 0)
        {
          wait_read = 0;
          log_debug("client: bytes read %zu", bytes_read);
          if (memcmp(buf_read, buf_write, bytes_read) != 0)
            log_error("client: buffer written different from buffer read. BAD!");
          else
            log_info("client: buffer written equals to buffer read. GOOD!");
        }
      }

      if ((ready_write) && (wait_read == 0))
      { // the socket is available for writing in it
        log_debug("client: socket available for writing");
        ssize_t bytes_written =  write(sock, buf_write, 2048);
        if ((bytes_written < 0) && (errno == EPIPE))
        { // connection closed by the other end
          log_debug("client: exiting in write because the other end has disconnected");
          hn_exit_program_terminate(0);
          break;
        }
        wait_read = 1;
      }
    }
  }


  close(sock);
  hn_log_close();

  return 0;
}


