///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 19, 2018
// Design Name: 
// Module Name: iface mmi64 
// Project Name: HN daemon
// Target Devices:
// Tool Versions:
// Description:
//   Handle communications interface in the host side.
//   Open communications interface with proFPGA devices, there are two possibilites depending on the utilized HW device
//     - mmi64 module ( mmi64 fm pcie module )
//     - profpga      ( motherboard )
//   Close communications interface
//   Manage calls to profpga mmi64 library to send and read data to/from HN system
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments:
//   WARNING: when hn_profpga_handle is in use, in case that the interface is not properly closed ithis could block 
//            the communications interface of proFPGA tools with the MotherBoards requiryng to reboot of the Motherboard
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/

// system header files
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <netinet/in.h>
#include <time.h>
#include <locale.h>

// prodesign header files
#include "profpga_error.h"

#include "mmi64.h"
#include "mmi64_defines.h"
#include "mmi64_module_upstreamif.h"
#include "mmi64_module_regif.h"
#include "profpga.h"
#include "profpga_logging.h"
#include "libconfig.h"

#include "mmi64_crossplatform.h"

// other header files
#include "hn_shared_structures.h"
#include "hn_buffer_fifo.h"

#include "hn_utils.h"
#include "hn_logging.h"
#include "hn_list.h"
#include "hn_iface_mmi64.h"
#include "hn_daemon.h"


/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

// Initialize communications interface
//   open mmi64 interface handler (detect mmi64 or mmi64fmpcie)
//   detect modules in hn_system for
//     items               going to  fpga
//     items            coming from  fpga
//     burst transfers     going to  fpga
//     burst transfers  coming from  fpga 
//   configure & start modules which send data from fpga (mmi64_upstreamif)
//

#define CHECK(status)  if (status!=E_PROFPGA_OK) { \
  printf(NOW("ERROR: %s\n"), profpga_strerror(status)); \
  return status;  }


#define PRODESIGN_MOTHERBOARD_GEN_1 0
#define PRODESIGN_MOTHERBOARD_GEN_2 1

#define ERROR_IFACE_MMI64_INTERFACE_NAME_NOT_FOND  1  //reassign error code


#define MODULE_IO_MMI64_MOTHERBOARD_NAME "mmi64"
#define MODULE_IO_MMI64FMPCIE_NAME       "mmi64fmpcie"
#define MODULE_IO_MMI64PLI_NAME          "mmi64pli"

#ifndef MMI64_MB_CFG_FNAME
  #define MMI64_MB_CFG_FNAME       "profpga_mmi64_generic.cfg"
#endif

#ifndef MMI64_FMPCIE_CFG_FNAME
  #define MMI64_FMPCIE_CFG_FNAME   "profpga_mmi64_fmpcie.cfg"
#endif

#ifndef MMI64_PLI_CFG_FNAME
  #define MMI64_PLI_CFG_FNAME      "profpga_mmi64_pli.cfg"
#endif

// from UD006-1.24 HDL Design Library - Reference Manual
//  The module ID range 0x0000_0000 - 0x7FFF_FFFF is available for user defined IDs.
//  So we define following moduleID Ranges
//     0x0000_1000 to 0x0000_1FFF  for items               going to  fpga
//     0x0000_2000 to 0x0000_2FFF  for items            coming from  fpga
//     0x0000_3000 to 0x0000_3FFF  for burst transfers     going to  fpga
//     0x0000_4000 to 0x0000_4FFF  for burst transfers  coming from  fpga 

#define MAX_NUMBER_OF_HN_MODULES_PER_TYPE              32   // range is 0x0000_0FFF -> 4096 modules MAX per each type and direction

#define MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_LOW    0x00001000
#define MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_HIGH   0x00001FFF
#define MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW      0x00002000
#define MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_HIGH     0x00002FFF
#define MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_LOW  0x00003000
#define MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_HIGH 0x00003FFF
#define MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_LOW    0x00004000
#define MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_HIGH   0x00004FFF
/*
#define MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_LOW    0x00000002
#define MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_HIGH   0x00000002
#define MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW      0x00000003
#define MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_HIGH     0x00000003
#define MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_LOW  0x00000004
#define MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_HIGH 0x00000004
#define MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_LOW    0x00000005
#define MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_HIGH   0x00000005
*/

/**********************************************************************************************************************
 **
 ** Private data structures
 **
 **********************************************************************************************************************
 **/
static const char *PROFPGA_BOARDS[32]={
  //top connector
  "TA1","TA2","TB1","TB2","TC1","TC2","TD1","TD2",
  "TA3","TA4","TB3","TB4","TC3","TC4","TD3","TD4",
  //bottom connector
  "BA1","BA2","BB1","BB2","BC1","BC2","BD1","BD2",
  "BA3","BA4","BB3","BB4","BC3","BC3","BD3","BD4"
}; 
#define VN_ID_DATA_BURST_START                         3

// structure to communicate with each thread
typedef struct upstream_io_module_s {
  
//  // the read thread itself
//  pthread_t    read_thread;
//
//  // mutex to communicate with the thread
//  pthread_mutex_t mutex;
  mmi64_module_t      *module;           // pointer to the mmi64 module
  mmi64_upstreamif_t  *upstreamif;       // pointer to the upstreamif object
  int                  instance;         // the thread instance number
  //time_t               start_time;       // the time when the data transfer was started
  //uint64_t             number_of_words;  // each thread counts the number of 64-bit words he received correctly
  mmi64_data_t        *buffer; // buffer for the read thread to receive the data
  uint32_t           (*fptr_get_data)( char *, uint32_t *, uint32_t );  // Currently using index of array including all downstream modules (not by type)
} upstream_io_module_t;

typedef struct downstream_io_module_s {
  mmi64_module_t      *module;           // pointer to the mmi64 module
  int                  instance;         // the module index in the array of downstream register interfaces number
  mmi64_error_t       (*fptr_send_data)( void *, uint32_t, mmi64_module_t *, hn_data_burst_channel_t *, hn_shm_buffer_transfer_t *);  // Currently using index of array including all   upstream modules (not by type)  
} downstream_io_module_t;


struct hn_header_burst_st
{
  uint64_t command:6;     // command for burst transfer, only accepts WR
  uint64_t tile:12;      // tile where MC is located
  uint64_t size:32;      // burst transfer payload size in number of bytes (header is excluded)
  uint64_t sender:12;    // sender of the transfer, NOT used
  uint64_t id:4;         // burst transfer ID, currently unused
  uint64_t address:32;   // base address of the transfer in the MC
  uint64_t padding_1:30; // padding to fill 512 bits
  uint64_t padding_2;    // padding to fill 512 bits
  uint64_t padding_3;    // padding to fill 512 bits
  uint64_t padding_4;    // padding to fill 512 bits
  uint64_t padding_5;    // padding to fill 512 bits
  uint64_t padding_6;    // padding to fill 512 bits
  uint64_t padding_7;    // padding to fill 512 bits

}__attribute__((packed));
typedef struct hn_header_burst_st hn_header_burst_t;


/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
static mmi64_module_t   *hn_io_modules_downstream_generic_item [MAX_NUMBER_OF_HN_MODULES_PER_TYPE];   // array containing pointers to modules managing generic items going from    PCHost  to  FPGA
static mmi64_module_t   *hn_io_modules_upstream_generic_item [MAX_NUMBER_OF_HN_MODULES_PER_TYPE];     // array containing pointers to modules managing generic items going from    FPGA    to  PCHost
static mmi64_module_t   *hn_io_modules_downstream_burst_transfer [MAX_NUMBER_OF_HN_MODULES_PER_TYPE]; // array containing pointers to modules managing bursty transfers data from  PCHost  to  FPGA
static mmi64_module_t   *hn_io_modules_upstream_burst_transfer [MAX_NUMBER_OF_HN_MODULES_PER_TYPE];   // array containing pointers to modules managing bursty transfers data from  FPGA    to  PCHost
static uint32_t          hn_io_modules_downstream_generic_item_count   = 0;    // number of modules in hn_modules_downstream_generic_item array
static uint32_t          hn_io_modules_upstream_generic_item_count     = 0;      //
static uint32_t          hn_io_modules_downstream_burst_transfer_count = 0;  //
static uint32_t          hn_io_modules_upstream_burst_transfer_count   = 0;    //

//uint32_t         hn_io_moudles_upstream_buffer_count[MAX_NUMBER_OF_HN_MODULES_PER_TYPE + MAX_NUMBER_OF_HN_MODULES_PER_TYPE]; // array_containing the number of values in queue of each buffer associated to the upstream modules

static mmi64_domain_t   *hn_mmi64_handle    = NULL;  // handle to profpga system
static profpga_handle_t *hn_profpga_handle  = NULL;  // handle to profpga system
static uint32_t          iface_is_mmi64_mb;
static uint32_t          iface_is_mmi64fmpcie;
static uint32_t          iface_is_mmi64pli;
static uint32_t          iface_initialized  = 0;

static mmi64_module_t   *hn_io_modules_downstream [ 2 * MAX_NUMBER_OF_HN_MODULES_PER_TYPE];   // array containing pointers to modules managing generic items going from    PCHost  to  FPGA
static mmi64_module_t   *hn_io_modules_upstream   [ 2 * MAX_NUMBER_OF_HN_MODULES_PER_TYPE];     // array containing pointers to modules managing generic items going from    FPGA    to  PCHost
static uint32_t          hn_io_modules_downstream_count  = 0;    // number of modules in hn_modules_downstream array
static uint32_t          hn_io_modules_upstream_count    = 0;      //

upstream_io_module_t    *upstream_module_data      = NULL;
downstream_io_module_t  *downstream_module_data    = NULL;
hn_mmi64_module_t       *hn_downstream_module_data = NULL;
hn_mmi64_module_t       *hn_upstream_module_data   = NULL;

static int up_MinItemMessageSize   = 1;
static int up_MinBurstMessageSize  = 2048;
static int up_BufferSizeExp     = 26;

#ifdef STORE_RAW_ITEMS
FILE *fd_items;
#endif

/**********************************************************************************************************************
 **
 ** Private function prototypes
 **
 **********************************************************************************************************************
 **/

/*! 
 * \brief handler for messages related to proFPGA mmi64 infrastructure.
 *
 *  Display messages through stdout during execution, formats text string for modelsim simulation mode
 *
 * \param[in] message_type
 * \param[fmt] pointer to text string to display
 * \param[...] params to replace in the text string by the value of these pameters
 * \return 0 if interface is successfully initialized, otherwise return an error code.
 */
int mmi64_message_handler(const int messagetype, const char *fmt,...);

/*! 
 * \brief Display settings of mmi64 upstreamif module on HN system.
 *
 * \param[in] instancenumber index of the module, just representative for the display message
 * \param[in] module pointer to the mmi64 upstreamif module. 
 * \return 0 if information is succesfully collected. otherwire return error code.
 */
mmi64_error_t mmi64_upstream_module_report_settings(int instancenumber, mmi64_module_t *module);

/*! 
 * \brief Initialize resources and modules.
 *
 *  Initialize data structures on the Host side, configure and initialize upstreamif module on the HN side
 *
 * \return 0 if all resources are succesfully allocated and modules successfully configured. otherwire return error code.
 */
mmi64_error_t mmi64_upstream_modules_start ( void );

/*! 
 * \brief Initialize downstream modules list 
 
 * fill informtation to manage downstream data flows,
 * pair each mmi64 downstream regif module with its index, point to mmi64_module and send_data function
 *
 */
uint32_t mmi64_downstream_module_info_start ( void );

/*! 
 * \brief Search for modules of desired type and that id is the range specified by the received parameters 
 *
 * Search in the list of modules detected in HN system for modules matching type (regif or upstream) and that its id is in the 
 * range specified by the received params. The id_range mechanism is used to distinguish between generic_items modules and burst_transfer modules
 *
 * \param[in] mmi64_domain pointer to proFPGA mmi64_domain handler
 * \param[in] module_type type of module to search (upstream or regif(downstream))
 * \param[out] identified_modules_by_id_range array of modules
 * \param[out] identified_modules_by_id_range_count number of modules added to the list
 * \param[in] identified_modules_by_id_range_low lowest value for ID to include in the list a mmi64 module
 * \param[in] identified_modules_by_id_range_high highest value for ID to include in the list a mmi64 module
 * \return 0 if scan operations were succesfully performed (even if no module vas found), otherwise return error code
 */
uint32_t iface_mmi64_scan_and_identify_modules (
    mmi64_domain_t*     mmi64_domain,
    mmi64_module_type_t module_type,                    // type of mmi64 module (regif or downstream)
    mmi64_module_t**    identified_modules_by_id_range,
    uint32_t*           identified_modules_by_id_range_count,
    const uint32_t      identified_modules_by_id_range_low,
    const uint32_t      identified_modules_by_id_range_high
);

/*! 
 * \brief Scan, identify and initialize mmi64 modules, upstreamif and regif(downstream). Initialize local data structures used to handle modules
 *
 */
uint32_t iface_mmi64_set_io_modules (void);

/*! 
 * \brief Set mmi64 insfrastructure handler
 *
 * get handler from mmi64 library. Open communications with mmi64 infracture, 
 * DEADLOCK in communications may occur if the handler is not properly closed at the end of the execution of the daemon
 *
 * \param[in] iface_name name of the mmi64 interface to utilize, two possible options
 *   mmi64 - for communications through the motherboard mmi64_handler
 *   mmi64fmpcie - for communications through the PCIe mmi64_handler
 * \return 0 if handler is succesfully loaded, otherwise return error code
 */
uint32_t iface_mmi64_load_mmi64_handler(const char* iface_name);




// mb motherboard index
// fm device index
// temperature pointer to variable to store acquired temperature
uint32_t iface_mmi64_get_fm_temperature(int mb_index, char *fm_desc, int *temperature);

uint32_t iface_mmi64_get_motherboard_temperature(int mb_index, int *temperature);

uint32_t get_index_of_fm_desc(char *fm, int *fm_index);
uint32_t get_index_of_mb_desc(char *mb_desc, int *mb_index);

/*! 
 * \brief send data through downstream module from pc application to module in HN system (32bit words)
 *
 * \param[in] addr pointer first address of buffer to transfer
 * \param[in] len_bytes  number of bytes to transfer
 * \param[in] module_type
 * \param[in] module_index
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 */
//   /*uint32_t module_type,*/ uint32_t module_index
mmi64_error_t hn_iface_mmi64_send_data_generic_item( void *addr, uint32_t len_bytes, mmi64_module_t *mmi64_downstream_module);

/*! 
 * \brief get data of buffer associated to upstream module in HN system (32bit words)
 *
 * \param[in] addr pointer first address of buffer where data will be stored
 * \param[in] len_bytes  number of bytes to read 
 * \param[in] module_type
 * \param[in] module_index 
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 */
uint32_t hn_iface_mmi64_get_data_generic_item( char *addr, uint32_t *len_bytes, uint32_t module_index);

/*! 
 * \brief get data of buffer associated to upstream module in HN system
 *
 * \param[in] addr pointer first address of buffer where data will be stored
 * \param[in] len_bytes  number of bytes to read 
 * \param[in] module_type
 * \param[in] module_index 
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 */
uint32_t hn_iface_mmi64_get_data_burst_transfer( char *addr, uint32_t *len_bytes, uint32_t module_index);

//*********************************************************************************************************************
//*********************************************************************************************************************
//*********************************************************************************************************************
//*********************************************************************************************************************


/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
struct timespec get_elapsed_time(struct timespec start, struct timespec end)
{
  struct timespec temp;
  if ((end.tv_nsec - start.tv_nsec) < 0) 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  } 
  else 
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }
  return temp;
}

unsigned long int get_elapsed_time_nanoseconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;


  temp = get_elapsed_time(start, end);
  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;
  
  return elapsed_nanoseconds;
}




/**********************************************************************************************************************
 **
 ** Local functions definition
 **
 **********************************************************************************************************************
 **/
uint32_t hn_iface_mmi64_upstream_buffer_get_data_count(uint32_t upstream_module_index, uint32_t *data_count);

int mmi64_message_handler(const int messagetype, const char *fmt,...)
{
  int n;
  va_list ap;
#ifdef HDL_SIM
    static int mmi64_g_last_message_had_newline = 1;

    // to print the actual simulation time (only after a message was printed with \n at the end)
    if (mmi64_g_last_message_had_newline)
    {
      fprintf (stdout, "%s: ", mmi64_now(NULL));
    }
    mmi64_g_last_message_had_newline = 0;
    if (strlen(fmt))
    {
      if (fmt[strlen(fmt)-1] == '\n')
      {
        mmi64_g_last_message_had_newline = 1;
      }
    }
#endif
  va_start(ap, fmt);
  n = vfprintf(stdout, fmt, ap);
  va_end(ap);
  return n;
}
//*********************************************************************************************************************
//*********************************************************************************************************************
mmi64_error_t mmi64_upstream_module_report_settings(int instancenumber, mmi64_module_t *module) 
{
  mmi64_error_t  status = E_MMI64_OK;
  uint8_t        module_version;
  uint16_t       mmi64_max_message_size;
  uint16_t       mmi64_min_message_size;
  uint32_t       fifo_size;
  mmi64_bool_t   enabled;

  // query status from hardware
  log_verbose("query status from upstream module");
  if ((status = mmi64_upstreamif_get_settings(module, 
                                              &module_version,
                                              &mmi64_max_message_size, 
                                              &mmi64_min_message_size, 
                                              &fifo_size, 
                                              &enabled)) != E_MMI64_OK)
  {
    log_error("ERROR: mmi64_upstreamif_get_settings() failed with %s", mmi64_strerror(status));
    return status;
  }


  // print status
  log_debug("Upstream module #%d is %s:", instancenumber, (enabled ? "enabled" : "disabled"));
  log_debug("   module id                : %08X", module->id);
  log_debug("   module version           : %d", module_version);
  log_debug("   max. mmi64 message size  : %d", mmi64_max_message_size);
  log_debug("   min. mmi64 message size  : %d", mmi64_min_message_size);
  log_debug("   size of the hardware FIFO: 2^%d words", fifo_size);
  return status;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
mmi64_error_t mmi64_upstream_modules_start ( void ) 
{
  mmi64_error_t       status = E_MMI64_OK;
  int                 i;
  uint32_t            current_module_id;


  // initialize the thread data structures : assign buffers and functions
  //and initialize the upstream modules in the fpga
  log_debug("Starting upstreamif modules");

  log_debug("Create upstream_modules_list");
  upstream_module_list = hn_list_create_list();

  if (upstream_module_list < 0)
  {
    log_error("upstream module list not created");
    return (upstream_module_list);
  }

  // allocate memory for all thread data structures
  if ((upstream_module_data = (upstream_io_module_t *) malloc (sizeof (upstream_io_module_t) * hn_io_modules_upstream_count)) == NULL)
  {
   log_error("ERROR: out of memory");
    return E_MMI64_WRONG_PARAMETER;
  }
  memset (upstream_module_data, 0, sizeof (upstream_io_module_t) * hn_io_modules_upstream_count);

  if ((hn_upstream_module_data = (hn_mmi64_module_t *) malloc (sizeof (hn_mmi64_module_t) * hn_io_modules_upstream_count)) == NULL)
  {
   log_error("ERROR: out of memory");
    return E_MMI64_WRONG_PARAMETER;
  }
  memset (hn_upstream_module_data, 0, sizeof (hn_mmi64_module_t) * hn_io_modules_upstream_count);


  // initialize structures and buffers for all detected upstream modules
  for (i = 0; i < (int) hn_io_modules_upstream_count; i ++)
  {
    log_debug("Starting upstream interface #%d", i);
    upstream_module_data[i].instance = i;
    upstream_module_data[i].module   =  hn_io_modules_upstream[i];
    current_module_id = hn_io_modules_upstream[i]->id;

    // report current settings
    if ((status = mmi64_upstream_module_report_settings(i, upstream_module_data[i].module)) != E_MMI64_OK)
    {
      return status;
    }

    // initialize upstream interface
    log_debug("Initialize upstreamif #%d", i);
    log_debug("      using blocking init");

    if((current_module_id >= MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW) && (current_module_id <= MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_HIGH)) {
      status = mmi64_upstreamif_blocking_init (upstream_module_data[i].module, (uint16_t) MMI64_PAYLOAD_LENGTH_MAX, (uint16_t) up_MinItemMessageSize, up_BufferSizeExp, &(upstream_module_data[i].upstreamif));
    } else {
      status = mmi64_upstreamif_blocking_init (upstream_module_data[i].module, (uint16_t) MMI64_PAYLOAD_LENGTH_MAX, (uint16_t) up_MinBurstMessageSize, up_BufferSizeExp, &(upstream_module_data[i].upstreamif));
    }

    if ( status != E_MMI64_OK)
    {
      log_error("mmi64_upstreamif_blocking_init() failed with %s", mmi64_strerror(status));
      return status;
    }
    
    // create buffer for read thread
    if ((upstream_module_data[i].buffer = (mmi64_data_t *) malloc (sizeof (mmi64_data_t) * (1L << up_BufferSizeExp))) == NULL)
    {
      log_error("out of memory");
      return E_MMI64_WRONG_PARAMETER;
    }

    // set read function: Generic item (32bits) or burst (64bits)
    log_debug("Setting get_data function module id 0x%08X", current_module_id);
    if((current_module_id >= MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW) && (current_module_id <= MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_HIGH))
    {
      log_debug("  to generic item get_data function");
      upstream_module_data[i].fptr_get_data = &hn_iface_mmi64_get_data_generic_item;
    }
    else if ((current_module_id >= MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_LOW) && (current_module_id <= MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_HIGH))
    {
      log_debug("  to burst transfer get_data function");
       upstream_module_data[i].fptr_get_data = &hn_iface_mmi64_get_data_burst_transfer;
    }
    else
    {
      log_error("ERROR: upstream module id out of expected ranges for generic items or burst data transfers");
      return E_MMI64_WRONG_PARAMETER;
    }

    // start data transfer
    log_debug("Enable data transfer for upstream module #%d on HN side", i);
    if ((status = mmi64_upstreamif_start (upstream_module_data[i].upstreamif)) != E_MMI64_OK)
    {
      log_error("ERROR: mmi64_upstreamif_start() failed with %s", mmi64_strerror(status));
      return status;
    }

    // report current settings
    if ((status = mmi64_upstream_module_report_settings(i, upstream_module_data[i].module)) != E_MMI64_OK)
    {
      return status;
    }
  }

  for (i = 0; i < (int) hn_io_modules_upstream_count; i ++)
  {
    current_module_id = hn_io_modules_upstream[i]->id;

    hn_upstream_module_data[i].index = i;
    hn_upstream_module_data[i].id    = current_module_id;
    if (current_module_id == MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW) {
      hn_upstream_module_data[i].type = GENERIC_ITEM_UPSTREAM;
    } else if((current_module_id > MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW) && (current_module_id <= MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_HIGH)) {
      hn_upstream_module_data[i].type = DEBUG_ITEM_UPSTREAM;
    } else if ((current_module_id >= MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_LOW) && (current_module_id <= MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_HIGH)) {
      hn_upstream_module_data[i].type = BURST_TRANSFER_UPSTREAM;
    }
    else
    {
      log_error("ERROR: upstream module id out of expected ranges for generic items or burst data transfers");
      return 2; // JM replace with out of range error code
    }

    hn_list_add_to_end(upstream_module_list, &hn_upstream_module_data[i]);
  }

#ifdef STORE_RAW_ITEMS
  char *home = getenv("HOME");
  char fname[256];
  sprintf(fname, "%s/hn_daemon.items", home);
  fd_items = fopen(fname, "wb");
#endif

  return status;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
// fill informtation to manage downstream data flows, pairing each downstream regif module with their index, pointer to mmi64_module and send_data function
uint32_t mmi64_downstream_module_info_start ( void ) 
{
  uint32_t            status = 0;
  int                 i;
  uint32_t            current_module_id;
  
  // initialize the data structures : assign buffers and functions
  log_debug("Init downstream modules data structures");

  log_debug("Create downstream_modules_list");
  downstream_module_list = hn_list_create_list();

  if (downstream_module_list < 0)
  {
    log_error("downstream module list not created");
    return (downstream_module_list);
  }

  // allocate memory for all thread data structures
  log_debug("create mmi64 iface local module info struct");
  if ((downstream_module_data = (downstream_io_module_t *) malloc (sizeof (downstream_io_module_t) * hn_io_modules_downstream_count)) == NULL)
  {
    log_error("ERROR: out of memory. could not initialize downstream modules structures");
    return 1; // JM replace with out of memory error code
  }
  memset (downstream_module_data, 0, sizeof (downstream_io_module_t) * hn_io_modules_downstream_count);
  
  log_debug("create mmi64 iface global module info struct");
  fflush(stdout);
  if ((hn_downstream_module_data = (hn_mmi64_module_t *) malloc (sizeof (hn_mmi64_module_t) * hn_io_modules_downstream_count)) == NULL)
  {
    log_error("ERROR: out of memory. could not initialize hn downstream modules structures");
    return 1; // JM replace with out of memory error code
  }
  memset (hn_downstream_module_data, 0, sizeof (hn_mmi64_module_t) * hn_io_modules_downstream_count);

  // initialize structures and buffers for all detected downstream modules
  // local structures
  for (i = 0; i < (int) hn_io_modules_downstream_count; i ++)
  {
    log_debug("Starting downstream module data #%d", i);
    downstream_module_data[i].instance = i;
    downstream_module_data[i].module   = hn_io_modules_downstream[i];

    current_module_id = hn_io_modules_downstream[i]->id;
    //log_debug("Setting send_data function module id 0x%08X", current_module_id);

    //if((current_module_id >= MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_LOW) && (current_module_id <= MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_HIGH))
    //{
    //  log_debug("  to generic item send_data function");
    //  downstream_module_data[i].fptr_send_data = &hn_iface_mmi64_send_data_generic_item;
    //}
    //else if ((current_module_id >= MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_LOW) && (current_module_id <= MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_HIGH))
    //{
    //  log_debug("  to burst transfer send_data function");
    //   downstream_module_data[i].fptr_send_data = &hn_iface_mmi64_send_data_burst_transfer;
    //}
    //else
    //{
    //  log_error("ERROR: downstream module id out of expected ranges for generic items or burst data transfers");
    //  return 2; // JM replace with out of range error code
    //}
  }
  // hn structures
  for (i = 0; i < (int) hn_io_modules_downstream_count; i ++)
  {
    log_debug("Starting hn downstream module data #%d", i);
    current_module_id = hn_io_modules_downstream[i]->id;

    hn_downstream_module_data[i].index = i;
    hn_downstream_module_data[i].id    = current_module_id;
    if((current_module_id >= MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_LOW) && (current_module_id <= MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_HIGH))
    {
      hn_downstream_module_data[i].type = GENERIC_ITEM_DOWNSTREAM;
    }
    else if ((current_module_id >= MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_LOW) && (current_module_id <= MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_HIGH))
    {
       hn_downstream_module_data[i].type = BURST_TRANSFER_DOWNSTREAM;
    }
    else
    {
      log_error("ERROR: downstream module id out of expected ranges for generic items or burst data transfers");
      return 2; // JM replace with out of range error code
    }

    hn_list_add_to_end(downstream_module_list, &hn_downstream_module_data[i]);
  }
  
  return status;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t iface_mmi64_scan_and_identify_modules (
    mmi64_domain_t*     mmi64_domain,
    mmi64_module_type_t module_type,                    // type of mmi64 module (regif or downstream)
    mmi64_module_t**    identified_modules_by_id_range,
    uint32_t*           identified_modules_by_id_range_count,
    const uint32_t      identified_modules_by_id_range_low,
    const uint32_t      identified_modules_by_id_range_high
){
  // First identify by type, then by range of ID, hn related modules IDs are in range [0x0000_0001, 0x0000_00FF], motherboard modules are in range [0xF000_0000, 0xFFFF_FFFF]
  mmi64_module_t** identified_modules;         // identify_by_id can return multiple modules (if same bitfile is used in multiple FPGAs or if same Module ID is used)
  uint32_t         identified_modules_count;
  uint32_t         identified_modules_for_hn_count;
  //uint32_t         identified_modules_for_hn_index[MAX_NUMBER_OF_HN_MODULES_PER_TYPE];  // max number of supported modules for this purpose
  uint32_t         ret;
  uint32_t         iter;

  ret = (uint32_t)mmi64_identify_by_type(mmi64_domain, module_type, &identified_modules, &identified_modules_count);
  if (ret != 0)
  {
    // error, return error code
    log_error("error getting mmi64 module pointers, mmi64_identify_by_type returned : %u", ret);
    return ret;
  }

  log_verbose("   found %2u modules", (unsigned int) identified_modules_count);
  for(iter = 0; iter < identified_modules_count; iter++)
  {
    log_verbose("     type match for module: %2u   id: %08x", iter, (uint32_t)identified_modules[iter]->id);
  }
  
  identified_modules_for_hn_count = 0;
  for(iter = 0; iter < identified_modules_count; iter++)
  {
    //printf("       comparing id %08X is in range of [%08X:%08X]\n", )
    if((identified_modules[iter]->id >= identified_modules_by_id_range_low) && (identified_modules[iter]->id <= identified_modules_by_id_range_high))
    {
      log_debug("   hn module in range: %2u   module index: %2u   id: %08x", identified_modules_for_hn_count, iter, (uint32_t)identified_modules[iter]->id);
      //identified_modules_for_hn_index[identified_modules_for_hn_count] = iter;
      // assign pointer of module to array
      identified_modules_by_id_range[identified_modules_for_hn_count] = identified_modules[iter];
      identified_modules_for_hn_count++;
    }
  }

  *identified_modules_by_id_range_count = identified_modules_for_hn_count;
  
  return ret;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t iface_mmi64_set_io_modules (void){
  uint32_t        ret_code;
  uint32_t        iter;

  ret_code = 0;
  
  // Search all the mmi64 modules in the hn_system to identify all available modules for io, for generic items and bursty transfers
  log_debug("starting modules initialization/detection process");
  
  log_debug("  scanning global upstream modules arrays/counters");
  ret_code = iface_mmi64_scan_and_identify_modules ( hn_mmi64_handle, MMI64_TID_M_UPSTREAMIF, &hn_io_modules_upstream[0], &hn_io_modules_upstream_count, 0x00000002, 0x0000F000);
  if (ret_code != 0)
  {
    log_error("getting global upstream modules information");
    return ret_code;
  }
  log_debug("    found %2u io modules for upstream", hn_io_modules_upstream_count);
  for (iter = 0; iter < hn_io_modules_upstream_count; iter++)
  {
    log_debug("      hn module id: %08x", (uint32_t)hn_io_modules_upstream[iter]->id);
  }
  
  log_debug("  scanning global downstream modules arrays/counters");
  ret_code = iface_mmi64_scan_and_identify_modules ( hn_mmi64_handle, MMI64_TID_M_REGIF, &hn_io_modules_downstream[0], &hn_io_modules_downstream_count,  0x00000002, 0x0000F000);
  if (ret_code != 0)
  {
    log_error("getting global downstream modules information");
    return ret_code;
  }
  log_debug("    found %2u io modules for downstream", hn_io_modules_downstream_count);
  for (iter = 0; iter < hn_io_modules_downstream_count; iter++)
  {
    log_debug("      hn module id: %08x", (uint32_t)hn_io_modules_downstream[iter]->id);
  }

  log_debug("  scanning downstream generic item modules");
  ret_code = iface_mmi64_scan_and_identify_modules ( hn_mmi64_handle, MMI64_TID_M_REGIF, &hn_io_modules_downstream_generic_item[0], &hn_io_modules_downstream_generic_item_count, MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_LOW,MODULE_ID_HN_IO_DOWNSTREAM_GENERIC_ITEM_HIGH);
  if (ret_code != 0)
  {
    log_error("getting downstream generic item modules information");
    return ret_code;
  }
  log_debug("    found %2u io modules for downstream generic items transfers", hn_io_modules_downstream_generic_item_count);
  for (iter = 0; iter < hn_io_modules_downstream_generic_item_count; iter++)
  {
    log_debug("      hn module id: %08x", (uint32_t)hn_io_modules_downstream_generic_item[iter]->id);
  }

  log_debug("  scanning upstream generic item modules");
  ret_code = iface_mmi64_scan_and_identify_modules ( hn_mmi64_handle, MMI64_TID_M_UPSTREAMIF, &hn_io_modules_upstream_generic_item[0], &hn_io_modules_upstream_generic_item_count, MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW,MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_HIGH);
  if (ret_code != 0)
  {
    log_error("getting upstream generic item modules information");
    return ret_code;
  }
  log_debug("    found %2u io modules for upstream generic items transfers", hn_io_modules_upstream_generic_item_count);
  for (iter = 0; iter < hn_io_modules_upstream_generic_item_count; iter++)
  {
    log_debug("      hn module id: %08x", (uint32_t)hn_io_modules_upstream_generic_item[iter]->id);
  }

  log_debug("  scanning downstream burst transfer modules");
  ret_code = iface_mmi64_scan_and_identify_modules ( hn_mmi64_handle, MMI64_TID_M_REGIF, &hn_io_modules_downstream_burst_transfer[0], &hn_io_modules_downstream_burst_transfer_count, MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_LOW, MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_HIGH);
  if (ret_code != 0)
  {
    log_error("getting downstream burst transfer modules information");
    return ret_code;
  }
  log_debug("    found %2u io modules for downstream burst transfers", hn_io_modules_downstream_burst_transfer_count);
  for (iter = 0; iter < hn_io_modules_downstream_burst_transfer_count; iter++)
  {
    log_debug("      hn module id: %08x", (uint32_t)hn_io_modules_downstream_burst_transfer[iter]->id);
  }

  log_debug("   scanning upstream burst transfer modules");
  ret_code = iface_mmi64_scan_and_identify_modules ( hn_mmi64_handle, MMI64_TID_M_UPSTREAMIF, &hn_io_modules_upstream_burst_transfer[0], &hn_io_modules_upstream_burst_transfer_count, MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_LOW, MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_HIGH);
  if (ret_code != 0)
  {
    log_error("getting upstream burst transfer modules information");
    return ret_code;
  }
  log_debug("    found %2u io modules for upstream burst transfers", hn_io_modules_upstream_burst_transfer_count);
  for (iter = 0; iter < hn_io_modules_upstream_burst_transfer_count; iter++)
  {
    log_debug("      hn module id: %08x", (uint32_t)hn_io_modules_upstream_burst_transfer[iter]->id);
  } 


  log_debug("  found %2u io modules for upstream", hn_io_modules_upstream_count);
  for (iter = 0; iter < hn_io_modules_upstream_count; iter++)
  {
    log_debug("     hn module id: %08x", (uint32_t)hn_io_modules_upstream[iter]->id);
  } 
  log_debug("  found %2u io modules for downstream", hn_io_modules_downstream_count);
  for (iter = 0; iter < hn_io_modules_downstream_count; iter++)
  {
    log_debug("     hn module id: %08x", (uint32_t)hn_io_modules_downstream[iter]->id);
  }
  log_debug("  found %2u io modules for downstream generic items transfers", hn_io_modules_downstream_generic_item_count);
  for (iter = 0; iter < hn_io_modules_downstream_generic_item_count; iter++)
  {
    log_debug("     hn module id: %08x", (uint32_t)hn_io_modules_downstream_generic_item[iter]->id);
  }
  log_debug("  found %2u io modules for upstream generic items transfers", hn_io_modules_upstream_generic_item_count);
  for (iter = 0; iter < hn_io_modules_upstream_generic_item_count; iter++)
  {
    log_debug("     hn module id: %08x", (uint32_t)hn_io_modules_upstream_generic_item[iter]->id);
  }
  log_debug("  found %2u io modules for downstream burst transfers", hn_io_modules_downstream_burst_transfer_count);
  for (iter = 0; iter < hn_io_modules_downstream_burst_transfer_count; iter++)
  {
    log_debug("     hn module id: %08x", (uint32_t)hn_io_modules_downstream_burst_transfer[iter]->id);
  }
  log_debug("  found %2u io modules for upstream burst transfers", hn_io_modules_upstream_burst_transfer_count);
  for (iter = 0; iter < hn_io_modules_upstream_burst_transfer_count; iter++)
  {
    log_debug("     hn module id: %08x", (uint32_t)hn_io_modules_upstream_burst_transfer[iter]->id);
  } 

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t iface_mmi64_load_mmi64_handler (const char* iface_name)
{
  uint32_t           ret_code;
  int32_t            mh_status; // message_handler install status
  profpga_error_t    status;
  int32_t            cmp_iface_name_mmi64_mb;
  int32_t            cmp_iface_name_mmi64fmpcie;
  int32_t            cmp_iface_name_mmi64pli;
  profpga_handle_t*  profpga_handle;  // handle to profpga system
  mmi64_domain_t*    mmi64_handle;    // handle to profpga system
  char               cfgfilename[2048];

  ret_code = ERROR_IFACE_MMI64_INTERFACE_NAME_NOT_FOND;  // no error

  iface_initialized    = 0;
  iface_is_mmi64_mb    = 0;
  iface_is_mmi64fmpcie = 0;
  iface_is_mmi64pli    = 0;
  cmp_iface_name_mmi64_mb    = strcmp(iface_name, MODULE_IO_MMI64_MOTHERBOARD_NAME);
  cmp_iface_name_mmi64fmpcie = strcmp(iface_name, MODULE_IO_MMI64FMPCIE_NAME);
  cmp_iface_name_mmi64pli    = strcmp(iface_name, MODULE_IO_MMI64PLI_NAME);

  if(cmp_iface_name_mmi64_mb == 0)
  {
    log_notice("Interface match found for mmi64 motherboard");
    iface_is_mmi64_mb = 1;
    ret_code = 0;
  }
  else if (cmp_iface_name_mmi64fmpcie == 0)
  {
    log_notice("Iterface match found for mm64 FM PCIe module");
    iface_is_mmi64fmpcie = 1;
    ret_code = 0;
  }
  else if (cmp_iface_name_mmi64pli == 0)
  {
    log_notice("Iterface match found for mmi64 PLI module");
    iface_is_mmi64pli = 1;
    ret_code = 0;
  }
  else
  {
    log_error("Unexpected interface name \"%s\"", iface_name);
  }

  // if iface is mmi64 (motherboard), load mb and then mmi64 handler
  // if iface is mmi64 fm pcie module, then directly load mmi64 module handler
  profpga_handle = NULL;
  mmi64_handle   = NULL;

  mh_status = profpga_set_message_handler(mmi64_message_handler);
  if (mh_status!=0)
  {
    log_error("Failed to install message handler (code %d)", mh_status);
    return mh_status;
  }

  if (iface_is_mmi64_mb == 1)
  {
    // connect to system
    log_debug("Open connection to profpga platform...");
    strcpy(cfgfilename, MMI64_MB_CFG_FNAME);
    status = profpga_open (&profpga_handle, cfgfilename);
    if (status!=E_PROFPGA_OK)
    {
      log_error("Failed connect to PROFPGA system (%s)", profpga_strerror(status));
      return status;
    }

    hn_mmi64_handle   = profpga_handle->mmi64_domain;
    hn_profpga_handle = profpga_handle;

  }
  else if ( iface_is_mmi64fmpcie == 1 )
  {
    log_debug("Open direct connection to mmi64 module..."); 
    strcpy(cfgfilename,MMI64_FMPCIE_CFG_FNAME);
    status =(profpga_error_t)mmi64_open_by_config_fname(&mmi64_handle, cfgfilename);
    if (status!=E_PROFPGA_OK)
    { 
      log_error("Failed connect to mmi64 fm pcie system (%s)", profpga_strerror(status));
      hn_mmi64_handle   = NULL;
      hn_profpga_handle = NULL;
      return status;
    }

    log_debug("Open connection to profpga platform for temperature purposes...");
    strcpy(cfgfilename, MMI64_MB_CFG_FNAME);
    status = profpga_open (&profpga_handle, cfgfilename);
    if (status!=E_PROFPGA_OK)
    {
      log_error("Failed connect to MB of PROFPGA system (%s)", profpga_strerror(status));
      log_error("Functions to get temperature are disabled");
      log_warn("System will continue loading, pcie connection for communications is running");
      hn_profpga_handle = NULL;
    }

    hn_mmi64_handle   = mmi64_handle;
    hn_profpga_handle = profpga_handle;
  }
  else if (iface_is_mmi64pli == 1)
  {
    // connect to system
    log_debug("Open connection to Verilog PLI backend...");
    strcpy(cfgfilename, MMI64_PLI_CFG_FNAME);
    log_debug("Using %s config file...", cfgfilename);
    status = profpga_open (&profpga_handle, cfgfilename);
    if (status!=E_PROFPGA_OK)
    {
      log_error("Failed connect to Verilog PLI backend (%s)", profpga_strerror(status));
      return status;
    }

    hn_mmi64_handle   = profpga_handle->mmi64_domain;
    hn_profpga_handle = profpga_handle;

    status = profpga_up (profpga_handle);
    if (status!=E_PROFPGA_OK)
    {
      log_error("Failed motherboard initialization on PLI backend (%s)", profpga_strerror(status));
      return status;
    }
  }
  else 
  {
    log_error("Handler for this interface type not implemented yed");
  }

  if (hn_mmi64_handle == NULL)
  {
    log_error("mmi64 module handler not loaded");
    return ERROR_IFACE_MMI64_INTERFACE_NAME_NOT_FOND; 
  }

  // scan for MMI64 modules
  log_debug("Scan mmi64 hardware...");
  status = (profpga_error_t)mmi64_identify_scan(hn_mmi64_handle);
  CHECK(status);
  
  // print scan results
  status = (profpga_error_t)mmi64_info_print(hn_mmi64_handle);
  CHECK(status);
  
  // at this point, the handlers are initialized, store in global var to later close it
  iface_initialized    = 1;

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t iface_mmi64_get_fm_temperature(int mb_index, char *fm_desc, int *temperature){
  profpga_error_t      status;
  profpga_mb_status_t  mb_status;
  profpga_fm_status_t  fm_status;
  int                  temp;
  int                  fm_index;
  uint32_t             conversion_status;
  
  unsigned char pgood_p12v_a1;    //!< status of psu_a1_pgood signal (P12V for section a1)
  unsigned char pgood_p12v_c1;    //!< status of psu_c1_pgood signal (P12V for section c1) (not available on MB-1M)
  unsigned char pgood_p12v_a3;    //!< status of psu_a3_pgood signal (P12V for section a3) (not available on MB-1M/MB-2M)
  unsigned char pgood_p12v_c3;    //!< status of psu_c3_pgood signal (P12V for section c3) (not available on MB-1M/MB-2M)
  unsigned char ta1_present_n;    //!< status of ta1_present_n signal
  unsigned char tc1_present_n;    //!< status of tc1_present_n signal (not available on MB-1M)
  unsigned char ta3_present_n;    //!< status of ta3_present_n signal (not available on MB-1M/MB-2M)
  unsigned char tc3_present_n;    //!< status of tc3_present_n signal (not available on MB-1M/MB-2M)

  //struct timespec t_start;
  //struct timespec t_end;
  //struct timespec t1;
  //struct timespec t2;
  //struct timespec t3;
  //struct timespec t4;
  //unsigned long t_total;
  //unsigned long t_access_mb;

  //clock_gettime(CLOCK_MONOTONIC, &t_start);

  log_verbose("get temperature of phys device for mb_index %d  fm_desc %s\n", mb_index, fm_desc);

  if(hn_profpga_handle == NULL) {
    log_error("attempting to get temperature of fpga module, but motherboard handler is not loaded, return error value");
    *temperature = -1;
    return 1;
  }

  //clock_gettime(CLOCK_MONOTONIC, &t1);
  pthread_mutex_lock(&mutex_write_mmi64);
  pthread_mutex_lock(&mutex_read_mmi64);
  //clock_gettime(CLOCK_MONOTONIC, &t2);
  status = profpga_get_mb_status( hn_profpga_handle, mb_index, &mb_status);
  //clock_gettime(CLOCK_MONOTONIC, &t3);
  pthread_mutex_unlock(&mutex_read_mmi64);
  pthread_mutex_unlock(&mutex_write_mmi64);
  //clock_gettime(CLOCK_MONOTONIC, &t4);

  //t_total     = get_elapsed_time_nanoseconds(t1,t4);
  //t_access_mb = get_elapsed_time_nanoseconds(t2,t3);
  //log_notice("JM10, time to acquire and release locks for mb:  %'10lu us", t_total/((unsigned long) 1000));

  if (status!=E_PROFPGA_OK) { 
    printf("ERROR: Failed to get_mb_status[MB%d]  (%s)\n", mb_index+1, profpga_strerror(status));
    fflush(stdout);
    return status;
  }

#ifdef LEGACY_PROFPGA_LIBS
  pgood_p12v_a1 = mb_status.pgood_p12v_a1;
  pgood_p12v_c1 = mb_status.pgood_p12v_c1;
  pgood_p12v_a3 = mb_status.pgood_p12v_a3;
  pgood_p12v_c3 = mb_status.pgood_p12v_c3;
  ta1_present_n = mb_status.ta1_present_n;
  tc1_present_n = mb_status.tc1_present_n;
  ta3_present_n = mb_status.ta3_present_n;
  tc3_present_n = mb_status.tc3_present_n;

#else
  switch (mb_status.mb_gen) {
    case (PRODESIGN_MOTHERBOARD_GEN_1): 
        pgood_p12v_a1 = mb_status.u.mb_gen1.pgood_p12v_a1;
        pgood_p12v_c1 = mb_status.u.mb_gen1.pgood_p12v_c1;
        pgood_p12v_a3 = mb_status.u.mb_gen1.pgood_p12v_a3;
        pgood_p12v_c3 = mb_status.u.mb_gen1.pgood_p12v_c3;
        ta1_present_n = mb_status.u.mb_gen1.ta1_present_n;
        tc1_present_n = mb_status.u.mb_gen1.tc1_present_n;
        ta3_present_n = mb_status.u.mb_gen1.ta3_present_n;
        tc3_present_n = mb_status.u.mb_gen1.tc3_present_n;
      break;
    case (PRODESIGN_MOTHERBOARD_GEN_2):
        log_warn("Detected gen 2 motherboard.... funtion has to be completed, value could not be correct ");
        pgood_p12v_a1 = mb_status.u.mb_gen2.pgood_p12v_fa1;
        pgood_p12v_c1 = mb_status.u.mb_gen2.pgood_p12v_fb1;
        pgood_p12v_a3 = mb_status.u.mb_gen2.pgood_p12v_fa2;
        pgood_p12v_c3 = mb_status.u.mb_gen2.pgood_p12v_fb2;
        ta1_present_n = mb_status.u.mb_gen2.fa1_ta1_present_n;
        tc1_present_n = mb_status.u.mb_gen2.fa1_ta2_present_n;
        ta3_present_n = mb_status.u.mb_gen2.fa1_tb1_present_n;
        tc3_present_n = mb_status.u.mb_gen2.fa1_tb2_present_n;
      break;
    default:
        // error, unexpected mb gen
        log_error("NOT supported mb_gen version (%d), only mb_gen1(%d) and mb_gen2(%d) suported", 
            mb_status.mb_gen,PRODESIGN_MOTHERBOARD_GEN_1, PRODESIGN_MOTHERBOARD_GEN_2);
        pgood_p12v_a1 = 0;
        pgood_p12v_c1 = 0;
        pgood_p12v_a3 = 0;
        pgood_p12v_c3 = 0;
        ta1_present_n = 0;
        tc1_present_n = 0;
        ta3_present_n = 0;
        tc3_present_n = 0;

        status = 1;
      break;
  }
#endif


  if(! (   ((strcmp(fm_desc, "TA1")== 0) && (pgood_p12v_a1==1) && ta1_present_n)
        || ((strcmp(fm_desc, "TC1")== 0) && (pgood_p12v_c1==1) && tc1_present_n)
        || ((strcmp(fm_desc, "TA3")== 0) && (pgood_p12v_a3==1) && ta3_present_n)
        || ((strcmp(fm_desc, "TC3")== 0) && (pgood_p12v_c3==1) && tc3_present_n) )
    )
  {
    *temperature = -1;
    log_error("getting temperature: Requested mb MB%d  and/or fpga module %s not detected in system", mb_index+1, fm_desc);
    return 1;
  }
  
  //get the index of the fpga module
  conversion_status = get_index_of_fm_desc(fm_desc, &fm_index);
  if(conversion_status != 0)
  {
    log_error("getting temperature for fpga module index");
    return conversion_status;
  }

  // despite that we are requesting the status of the fm, we pass the handler of the profpga ctrl module
  //clock_gettime(CLOCK_MONOTONIC, &t1);
  pthread_mutex_lock(&mutex_write_mmi64);
  pthread_mutex_lock(&mutex_read_mmi64);
  status = profpga_get_fm_status(hn_profpga_handle,mb_index,fm_index,&fm_status);  
  pthread_mutex_unlock(&mutex_read_mmi64);
  pthread_mutex_unlock(&mutex_write_mmi64);
  //clock_gettime(CLOCK_MONOTONIC, &t4);

  //t_total     = get_elapsed_time_nanoseconds(t1,t4);
  //log_notice("JM10, time to acquire and release locks for fm module:  %'10lu us", t_total/((unsigned long) 1000));
  
  if (status != E_PROFPGA_OK)
  {
    *temperature = -1;
    log_error("Failed to get_fm_status[MB%d][%s]  (%s)", mb_index+1, PROFPGA_BOARDS[fm_index], profpga_strerror(status) );
    return status;
  }

  switch(fm_status.fm_type){
    case PROFPGA_FM_XC7V2000T:
         //log_debug("MB%d module %s type PROFPGA_FM_XC7V2000T",      mb_index+1, PROFPGA_BOARDS[fm_index]);
         temp=fm_status.u.fm_xc7v2000t.temperature;
         break;
    case PROFPGA_FM_XC7VyxxxxT_R3:
         //log_debug("MB%d module %s type PROFPGA_FM_XC7VyxxxxT_R3",  mb_index+1, PROFPGA_BOARDS[fm_index]);
         temp=fm_status.u.fm_xc7vyxxxxt_r3.temperature;
         break;
    case PROFPGA_FM_XC7Zxxx_R1_R4:
         //log_debug("MB%d module %s type PROFPGA_FM_XC7Zxxx_R1_R4",  mb_index+1, PROFPGA_BOARDS[fm_index]);
         temp=fm_status.u.fm_xc7zxxx_r1_r4.temperature;
         break;    
    case PROFPGA_FM_XCVU440_R1_R2:
         //log_debug("MB%d module %s type PROFPGA_FM_XCVU440_R1_R2 ", mb_index+1, PROFPGA_BOARDS[fm_index]);
         temp=fm_status.u.fm_xcvu440_r1_r2.temperature;
         break;     
    case PROFPGA_FM_XCXUXXX_R1:
         //log_debug("MB%d module %s type PROFPGA_FM_XCXUXXX_R1",     mb_index+1, PROFPGA_BOARDS[fm_index]);
         temp=fm_status.u.fm_xcxuxxx_r1.temperature;
         break;
    default:
         //debug("MB%d module %s type default (PROFPGA_FM_XC7V2000T)", mb_index+1, PROFPGA_BOARDS[fm_index]);
         temp=fm_status.u.fm_xc7v2000t.temperature;
         break;
    }

    *temperature = temp;

    //clock_gettime(CLOCK_MONOTONIC, &t_end);
    //t_total     = get_elapsed_time_nanoseconds(t_start,t_end);
    //log_notice("JM10, time total to get temperature for fm module:  %'10lu us", t_total/((unsigned long) 1000));
    
    return status;
}

//*********************************************************************************************************************
//
//  Get motherboard temperature, temporary disable clock temperature to avoid warning msg since it is not returned
//
//*********************************************************************************************************************
uint32_t iface_mmi64_get_motherboard_temperature(int mb, int *temperature){
  profpga_mb_status_t mb_status;
  profpga_error_t status;

  int mb_temp;
  //int clk_temp;
 printf("JM10, attempting to get motherboard_temperature for mb_index %d \n", mb);
 fflush(stdout);

  if(hn_profpga_handle == NULL) {
    log_error("attempting to get temperature of motherboard, but motherboard handler is not loaded");
    *temperature = -1;
    return 1;
  }

  pthread_mutex_lock(&mutex_write_mmi64);
  pthread_mutex_lock(&mutex_read_mmi64);
  status = profpga_get_mb_status( hn_profpga_handle, mb, &mb_status);
  pthread_mutex_unlock(&mutex_read_mmi64);
  pthread_mutex_unlock(&mutex_write_mmi64);

  if (status!=E_PROFPGA_OK) { 
    *temperature = -1;
    log_error("ERROR: Failed to get_mb_status[MB%d]  (%s)\n", mb+1, profpga_strerror(status));
    fflush(stdout);
    return status;
  }

#ifdef LEGACY_PROFPGA_LIBS
  mb_temp  = mb_status.fpga_temperature;
  //clk_temp = mb_status.clk_temperature;  
#else
  switch (mb_status.mb_gen) {
    case (PRODESIGN_MOTHERBOARD_GEN_1): 
        mb_temp  = mb_status.u.mb_gen1.fpga_temperature;
        //clk_temp = mb_status.u.mb_gen1.clk_temperature;
      break;
    case (PRODESIGN_MOTHERBOARD_GEN_2):
        mb_temp  = mb_status.u.mb_gen2.fpga_temperature;
        //clk_temp = mb_status.u.mb_gen2.clk_temperature;
      break;
    default:
        // error, unexpected mb gen
        log_error("NOT supported mb_gen version (%d), only mb_gen1(%d) and mb_gen2(%d) suported", 
            mb_status.mb_gen, PRODESIGN_MOTHERBOARD_GEN_1, PRODESIGN_MOTHERBOARD_GEN_2);
        mb_temp  = -1;
        //clk_temp = -1;
        status = 1;
      break;
  }
#endif

  log_debug("PROFPGA_MB%d status :",mb+1);
  //log_debug("motherboard: %2d   mb fpga temp:%d °C    clk_temp: %d °C\n", mb+1, mb_temp, clk_temp);
  log_debug("motherboard: %2d   mb fpga temp:%d °C\n", mb+1, mb_temp);
  *temperature = mb_temp;

  return 0;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t get_index_of_fm_desc(char *fm, int *fm_index){

  uint32_t ret = 0;
  int      ind = -1;
 
  if(strcmp(fm, "TA1") == 0) {
    ind = 0;
  }
  else if(strcmp(fm, "TA2") == 0) {
    ind = 1;
  }
  else if(strcmp(fm, "TB1") == 0) {
    ind = 2;
  }
  else if(strcmp(fm, "TB2") == 0) {
    ind = 3;
  }
  else if(strcmp(fm, "TC1") == 0) {
    ind = 4;
  }
  else if(strcmp(fm, "TC2") == 0) {
    ind = 5;
  }
  else if(strcmp(fm, "TD1") == 0) {
    ind = 6;
  }
  else if(strcmp(fm, "TD2") == 0) {
    ind = 7;
  }
  else if(strcmp(fm, "TA3") == 0) {
    ind = 8;
  }
  else if(strcmp(fm, "TA4") == 0) {
    ind = 9;
  }
  else if(strcmp(fm, "TB3") == 0) {
    ind = 10;
  }
  else if(strcmp(fm, "TB4") == 0) {
    ind = 11;
  }
  else if(strcmp(fm, "TC3") == 0) {
    ind = 12;
  }
  else if(strcmp(fm, "TC4") == 0) {
    ind = 13;
  }
  else if(strcmp(fm, "TD3") == 0) {
    ind = 14;
  }
  else if(strcmp(fm, "TD4") == 0) {
    ind = 15;
  }
  else if(strcmp(fm, "BA1") == 0) {
    ind = 16;
  }
  else if(strcmp(fm, "BA2") == 0) {
    ind = 17;
  }
  else if(strcmp(fm, "BB1") == 0) {
    ind = 18;
  }
  else if(strcmp(fm, "BB2") == 0) {
    ind = 19;
  }
  else if(strcmp(fm, "BC1") == 0) {
    ind = 20;
  }
  else if(strcmp(fm, "BC2") == 0) {
    ind = 21;
  }
  else if(strcmp(fm, "BD1") == 0) {
    ind = 22;
  }
  else if(strcmp(fm, "BD2") == 0) {
    ind = 23;
  }
  else if(strcmp(fm, "BA3") == 0) {
    ind = 24;
  }
  else if(strcmp(fm, "BA4") == 0) {
    ind = 25;
  }
  else if(strcmp(fm, "BB3") == 0) {
    ind = 26;
  }
  else if(strcmp(fm, "BB4") == 0) {
    ind = 27;
  }
  else if(strcmp(fm, "BC3") == 0) {
    ind = 28;
  }
  else if(strcmp(fm, "BC4") == 0) {
    ind = 29;
  }
  else if(strcmp(fm, "BD3") == 0) {
    ind = 30;
  }
  else if(strcmp(fm, "BD4") == 0) {
    ind = 31;
  }
  else {
    ind = -1;
    log_error("Unexpected position description for a connector %s", fm);
  }

  *fm_index = ind;

  return ret;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t get_index_of_mb_desc(char *mb_desc, int *mb_index){

  uint32_t ret = 0;
  int      ind = -1;
  int      cmp;
  char    *str_index;

  cmp = strncmp(mb_desc, "MB", 2);

  if (cmp != 0){
    log_error("Unexpected position description for a motherboard %s", mb_desc);
    *mb_index = -1;
    return 1;
  }

  str_index = mb_desc + 2;
  ind = atoi(str_index) - 1;

  log_debug ("Motherboard  %s  -> str_ind %s  profgpa_index %d ", mb_desc, str_index, ind);
  
  *mb_index = ind;
  return ret;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_get_device_temperature(char *mb_desc, char *fm_desc, int *temperature){

  uint32_t             request_temp_of_mb;
  int                  mb_index;
  uint32_t             conversion_status;
  uint32_t             request_status;
  
  request_temp_of_mb = 1;

  if (mb_desc == NULL)
  {
    log_error("Request temperature, no motherboard specified");
    return 1;
  }

  // get the index of the motherboard, we need it in any case
  conversion_status = get_index_of_mb_desc(mb_desc, &mb_index);
  if(conversion_status != 0)
  {
    log_error("getting motherboard index");
    return conversion_status;
  }

  // check whether requested temp is of motherboard or of the fpga
  if(fm_desc != NULL)
  {
    request_temp_of_mb = 0;
  }

  if (request_temp_of_mb != 0)
  {
    //request temperature of motherboard
    request_status = iface_mmi64_get_motherboard_temperature(mb_index, temperature);
  }
  else
  {
    //request temperature of fpga device
    request_status = iface_mmi64_get_fm_temperature(mb_index, fm_desc, temperature);
  }

  return request_status;

 }


/**********************************************************************************************************************
 **
 ** Public functions
 **
 **********************************************************************************************************************
 **/
uint32_t hn_iface_mmi64_init(const char* iface_name)
{
  uint32_t init_status;

  log_debug("MMI64 initializing interface %s", iface_name);

  // load handlers
  init_status = iface_mmi64_load_mmi64_handler (iface_name);
  if(init_status != 0)
  {
    // error in intialization, abort initialization and return error
    log_error("initializing mmi64 handler");
    return init_status;
  }

  // scan mmi64 io modules for data transfer
  init_status = iface_mmi64_set_io_modules();
  if(init_status != 0)
  {
    // error in intialization, abort initialization and return error
    log_error("initializing io modules");
    hn_iface_mmi64_release();
    return init_status;
  }

  // Check presence of ,at least, the generic item modules, one per direction.
  // these are the minimal requisitions to communicate with the HN system
  if(hn_io_modules_downstream_generic_item_count < 1)
  {
    log_error ("At least one downstream module for generic items must be present in the system, found %u", hn_io_modules_downstream_generic_item_count);
    return 1;
  }
  else if (hn_io_modules_upstream_generic_item_count < 1)
  {
    log_error ("At least one upstream module for generic items must be present in the system, found %u", hn_io_modules_upstream_generic_item_count);
    return 1;
  }

  // initialize upstreamif modules info structs for data transmission management and start modules on HN system 
  init_status = mmi64_upstream_modules_start();

  if (init_status != 0)
  {
    log_error("setting upstream modules info structs and stating upstream modules");
    hn_iface_mmi64_release();
    return init_status;
  }


  //// initialize downstream modules info structs for data transmission management
  init_status = mmi64_downstream_module_info_start();
  if (init_status != 0)
  {
    log_error("setting downstream modules info structure");
    hn_iface_mmi64_release();
    return init_status;
  }

  // all done, notify and return
  log_debug("MMI64 interface \"%s\" succesfully initialized", iface_name);

  return 0;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
// Release buffers, close communications interface with HN system,
// WARNING: when hn_profpga_handle is in use, in case that the interface is not properly closed ithis could block 
//          the communications interface of proFPGA tools with the MotherBoards requiryng to reboot of the Motherboard
uint32_t hn_iface_mmi64_release (void)
{
  uint32_t       ret_code;
  uint32_t       ind;
  mmi64_error_t  status;

  ret_code = 0;

#ifdef STORE_RAW_ITEMS
  if (fd_items != NULL) fclose(fd_items);
#endif

  // FIXME JM all the mmi64 functions could be stacked if the platform gets stacked to. Could we change for other type
  //       with timeout or similar

  // release resources for upstream modules
  // stop upstream interfaces and release buffers
  // stop and release everything
  if(upstream_module_data != NULL)
  {
    log_debug("Stopping upstream modules data transmission at HN side");
    for (ind = 0; ind < hn_io_modules_upstream_count; ind++)
    {
      // stop data transfer
      log_debug("  stopping instance #%u", ind);
      // let's throw away queued data
      uint32_t words_in_fifo = 0;
      hn_iface_mmi64_upstream_buffer_get_data_count((uint32_t)ind, &words_in_fifo);
      mmi64_upstreamif_get_data_flush(upstream_module_data[ind].upstreamif, upstream_module_data[ind].buffer, &words_in_fifo);
      status = mmi64_upstreamif_stop (upstream_module_data[ind].upstreamif);
      if (status != E_MMI64_OK)
      {
        log_error("  mmi64_upstreamif_stop() failed with %s", mmi64_strerror(status));
        ret_code = (uint32_t)status;
      }

      // release buffer memory
      free (upstream_module_data[ind].buffer);

      // destroy upstreamif FIFO
      log_debug("  destroying upstreamif #%d on host side", ind);
      status = mmi64_upstreamif_destroy (&(upstream_module_data[ind].upstreamif));
      if (status != E_MMI64_OK)
      {
        log_error("  mmi64_upstreamif_destroy() failed with %s", mmi64_strerror(status));
        ret_code = (uint32_t)status;
      }

      // report current settings
      status = mmi64_upstream_module_report_settings(ind, upstream_module_data[ind].module);
      if (status != E_MMI64_OK) 
        return (uint32_t)status;
    }
    // free upstream_module_data
    log_debug("Release memory of upstream modules info structures");
    free (upstream_module_data);
  }
  else
  {
    log_debug("Upstream modules were not initialized, will not atempt to stop them nor release their resources");
  }
  
  // Do anything for downstream modules ??
  if (downstream_module_data != NULL)
  {
    log_debug("Release memory of downstream modules info structures");
    free (downstream_module_data);
  }
  else
  {
    log_debug("Downstream modules were not initialized, will not attempt to release their resources");
  }

  ////////////
  ////// following close procedure could be shortened, is so long for testing and debugging errors on interface connection and startup process
  // close connection to profpga or mmi64 handle
  if (iface_initialized != 0)
  {
    if (iface_is_mmi64_mb == 1  )
    {
      log_debug("Close connection to fpga motherboard");
      if (hn_profpga_handle != NULL)
      {
        profpga_close (&hn_profpga_handle);
      }
      else
      {
        // in case the connection was not stablised it will not be an error
        log_error("attempting to close profpga connection, but handler points to null");
      }
    }
    else if (iface_is_mmi64fmpcie == 1 )
    {
      log_debug("Close connection to mmi64 pcie device");
      if (hn_mmi64_handle != NULL)
      {
        mmi64_close(&hn_mmi64_handle);
      }
      else
      {
        log_error("attempting to close mmi64 module connection, but handler points to null");
      }
    }
    else if (iface_is_mmi64pli == 1 )
    {
      log_debug("Close connection to mmi64 PLI backend");
      if (hn_profpga_handle != NULL)
      {
        profpga_close(&hn_profpga_handle);
      }
      else
      {
        log_error("attempting to close mmi64 module connection, but handler points to null");
      }
    }
    else
    {
      // Notify error, try to close everything
      log_error("unexpected io communications device, attempting to close every opened handler");
      if (hn_mmi64_handle != NULL)
      {
        log_debug("Close connection to mmi64 pcie device");
        mmi64_close(&hn_mmi64_handle);
      }
      if(hn_profpga_handle != NULL)
      {
        log_debug("Close connection to fpga motherboard");
        profpga_close(&hn_profpga_handle);
      }
    }
  }
  else
  {
    log_debug("MMI64 interface is not fully initialized, searching for any open handler");
    if (hn_mmi64_handle != NULL)
    {
      log_debug("Found mmi64 handler. Close connection to mmi64 pcie device");
      mmi64_close(&hn_mmi64_handle);
    }
    if(hn_profpga_handle != NULL)
    {
      log_debug("Found profgpa handler. Close connection to fpga motherboard");
      profpga_close (&hn_profpga_handle);
    }
  }

  // Release memory related to hn downstream modules for hn global access
  if(hn_downstream_module_data != NULL)
  {
    log_debug("Release hn downstream modules info");
    free(hn_downstream_module_data);
  }
  if(downstream_module_list >= 0)
  {
    log_debug("Destroy list of hn downstream modules info");
    hn_list_destroy_list(downstream_module_list);
  }

  log_debug("All profpga/mmi64 commumications resources released");

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_is_initialized(uint32_t *initialized) {
  
  *initialized = iface_initialized;

  return 0;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_get_number_of_io_modules_of_type(uint32_t module_type, uint32_t *num_modules)
{
  uint32_t ret_code;

  ret_code = 0;

  *num_modules = -1;

  if ((iface_initialized != 0) && ((hn_mmi64_handle != NULL) || (hn_profpga_handle != NULL)))
  {
    // system has been initialized
    //*num_modules = hn_io_modules_upstream_generic_item_count +  hn_io_modules_upstream_burst_transfer_count;
    if (module_type == GENERIC_ITEM_DOWNSTREAM) {
      *num_modules = hn_io_modules_downstream_generic_item_count;
    } else if  (module_type == GENERIC_ITEM_UPSTREAM ) {
      *num_modules = hn_io_modules_upstream_generic_item_count;
    } else if  (module_type == BURST_TRANSFER_DOWNSTREAM) {
      *num_modules = hn_io_modules_downstream_burst_transfer_count;
    } else if  (module_type == BURST_TRANSFER_UPSTREAM) {
      *num_modules = hn_io_modules_upstream_burst_transfer_count;
    } else{
      log_error("@hn_get_number_of_io_modules_of_type: unexpected module type");
      *num_modules = -1;
    }
  } else {
    log_error("@hn_get_number_of_io_modules_of_type: system has not been properly initialized");
    ret_code = 1; // replace with proper error code value
  }

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_get_number_of_upstream_modules(uint32_t *num_modules)
{
  uint32_t ret_code;

  ret_code = 0;

  *num_modules = 0;

  if ((iface_initialized != 0) && ((hn_mmi64_handle != NULL) || (hn_profpga_handle != NULL)))
  {
    // system has been initialized
    //*num_modules = hn_io_modules_upstream_generic_item_count +  hn_io_modules_upstream_burst_transfer_count;
    *num_modules = hn_io_modules_upstream_count;
  } else {
    log_error("@hn_get_number_of_upstream_modules: system has not been properly initialized");
    ret_code = 1; // replace with proper error code value
  }

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_get_number_of_downstream_modules(uint32_t *num_modules)
{
  uint32_t ret_code;

  ret_code = 0;

  *num_modules = 0;

  if ((iface_initialized != 0) && ((hn_mmi64_handle != NULL) || (hn_profpga_handle != NULL)))
  {
    // system has been initialized
    *num_modules = hn_io_modules_downstream_count;
  } else
  {
    log_error("@hn_get_number_of_downstream_modules: system has not been properly initialized");
    ret_code = 1; // replace with proper error code value
  }

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_get_upstream_module_type(uint32_t upstream_module_index, hn_mmi64_module_type_t *module_type)
{
  uint32_t ret_code;

  ret_code = 0;

  upstream_io_module_t io_module = upstream_module_data[upstream_module_index];

  if ((iface_initialized != 0) && ((hn_mmi64_handle != NULL) || (hn_profpga_handle != NULL)))
  {
    int ind;

    for (ind = 0; ind < hn_io_modules_upstream_generic_item_count; ind++) {
      if (hn_io_modules_upstream_generic_item[ind]->id == io_module.module->id) {
        if (io_module.module->id == MODULE_ID_HN_IO_UPSTREAM_GENERIC_ITEM_LOW) {
          *module_type = GENERIC_ITEM_UPSTREAM;
        } else {
          *module_type = DEBUG_ITEM_UPSTREAM;
        }

        return 0;
      }
    }
    for (ind = 0; ind < hn_io_modules_upstream_burst_transfer_count; ind++) {
      if (hn_io_modules_upstream_burst_transfer[ind]->id == io_module.module->id) {
        *module_type = BURST_TRANSFER_UPSTREAM;
        return 0;
      }
    }

    log_error("@hn_iface_mmi64_get_upstream_module_type: requested upstream module not found");
    ret_code = 1; // not found
  } else
  {
    log_error("@hn_iface_mmi64_get_upstream_module_type: system has not been properly initialized");
    ret_code = 1; // replace with proper error code value
  }

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_upstream_buffer_get_data_count(uint32_t upstream_module_index, uint32_t *data_count)
{
  uint32_t ret_code;
  //uint32_t data_count;
  mmi64_error_t status;
//  uint32_t ind;
//  uint32_t ind_buffer;
//  uint32_t number_of_upstream_modules;

  ret_code = 0;

  //if(buffer_count_v == NULL)

  // get values from upstreamif buffers, and store in array
  // protect with mutex if needed, we can not update the values when a thread is reading ...!!
  
  /*
  number_of_upstream_modules = hn_io_modules_upstream_generic_item_count + hn_io_modules_upstream_burst_transfer_count;
  ind_buffer = 0;
  for (ind = 0; ind < hn_io_modules_upstream_generic_item_count; ind++, ind_buffer++) {
    *values_in_buffer[ind_buffer] = hn_io_modules_upstream_generic_item[ind].mmi64_get_fifo_count;
  }
  for (ind = 0; ind < hn_io_modules_upstream_burst_transfer_count; ind++) {
    *values_in_buffer[ind_buffer] = hn_io_modules_upstream_burst_transfer[ind].mmi64_get_fifo_count;
  }
  */

    // un altra proposta a les linies que hi ha dalt depen de si el valor de get_fifo_count s'actualitza automaticament....
    // aleshores, en la funció d'inicialització, només caldria crear un vector de punteros que apuntara al les variables que contenen la info, 
    // i per tant, en aquesta funció només caldria copiar els valors en el array passat com a parametre....
    // o inclús millor encara (consultar-ho amb Rafa): pasar els punteros al thread de nivell superior, y que no calga ni cridar a aquesta funció....

//-------------------------------------------------------------------------------
// la nova proposta es nomes tornar el numero de bytes en un buffer especific, molt mes facil, aixo si,
//     ara ja no distinguixc entre generic o burst_transfer

#if defined(HDL_SIM) && defined(LEGACY_PROFPGA_LIBS)
  mmi64_bool_t empty;
  status = mmi64_upstreamif_fifo_is_empty(upstream_module_data[upstream_module_index].upstreamif, &empty);
  *data_count = empty ? 0 : 1;
#else
  status = mmi64_upstreamif_get_data_count(upstream_module_data[upstream_module_index].upstreamif, data_count);
  if (status != 0)
  {
    log_error("@hn_iface_mmi64_get_num_bytes_in_upstream_buffer, getting data count from fifo");
  }
#endif

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_upstream_buffer_is_not_empty(uint32_t upstream_module_index, uint32_t *has_data)
{
  uint32_t      ret_code;
  mmi64_error_t status;
  mmi64_bool_t  state;

  ret_code = 0;

  // we could NOT check if the index of the upstream module is in range of current modules to avoid any possible out of range condition
  //  so we could speed up an operation that runs many times
  if (upstream_module_index >= hn_io_modules_upstream_count)
  {
    log_error("@hn_iface_mmi64_upstream_buffer_is_not_empty, requested index out of range");
    log_error("  requested: %2u,  but max index is %u", upstream_module_index, hn_io_modules_upstream_count-1);
    return 1; // JM replace with error code out_of_range
  }

  status = mmi64_upstreamif_fifo_is_empty(upstream_module_data[upstream_module_index].upstreamif, &state);

  if (status != E_MMI64_OK)
  {
    log_error("@hn_iface_mmi64_upstream_buffer_is_not_empty, checking empty status of buffer for upstream module %u", upstream_module_index);
    return 2; // JM replace with error code
  }

  if (state == mmi64_true)
  {
    *has_data = 0;
  } else
  {
    *has_data = 1;
  }

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_get_vn_id_for_fifo(uint32_t fifo_type, uint32_t module_index, uint32_t *vn_id)
{
  if (fifo_type == FIFO_TYPE_UPSTREAM) {
    // we could NOT check if the index of the upstream module is in range of current modules to avoid any possible out of range condition
    //  so we could speed up an operation that runs many times
    if (module_index >= hn_io_modules_upstream_count)
    {
      log_error("@hn_iface_mmi64_get_vn_id_for_fifo, requested index out of range for upstream module");
      log_error("  requested: %2u,  but max index is %u", module_index, hn_io_modules_upstream_count-1);
      return 1; // JM replace with error code out_of_range
    }

    // The mapping is:
    // First data burst upstream  -> VN 3
    // Second data burst upstream -> VN 4
    // ...
    *vn_id = upstream_module_data[module_index].module->id + VN_ID_DATA_BURST_START - MODULE_ID_HN_IO_UPSTREAM_BURST_TRANSFER_LOW;
  } else {
    // is for downstream
    // we could NOT check if the index of the downstream module is in range of current modules to avoid any possible out of range condition
    //  so we could speed up an operation that runs many times
    if (module_index >= hn_io_modules_downstream_count)
    {
      log_error("@hn_iface_mmi64_get_vn_id_for_fifo, requested index out of range for downstream module");
      log_error("  requested: %2u,  but max index is %u", module_index, hn_io_modules_downstream_count-1);
      return 2; // JM replace with error code out_of_range
    }

    // The mapping is:
    // First data burst upstream  -> VN 3 + hn_io_modules_upstream_count 
    // Second data burst upstream -> VN 3 + hn_io_modules_upstream_count + 1
    // ... 
    *vn_id = downstream_module_data[module_index].module->id + VN_ID_DATA_BURST_START - MODULE_ID_HN_IO_DOWNSTREAM_BURST_TRANSFER_LOW;

  }
  return 0;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
mmi64_error_t hn_iface_mmi64_send_data_generic_item(void *addr, uint32_t len_bytes, mmi64_module_t *mmi64_downstream_module)
{
  mmi64_error_t  ret_code;
  uint32_t      *base_addr_uint32;

  base_addr_uint32 = (uint32_t *) addr;
 
  // FIXME (ratoga -> vscotti): this loop should not be in this module. It should be handled in the backend_send_to_fpga thread function  
  log_debug("  send_data_generic_item: let's check whether there is enough space at HW FIFO to store the downstream item transfer");
  uint32_t free_space = 0;
  while (free_space < 2*len_bytes/sizeof(uint32_t)) {
    pthread_mutex_lock(&mutex_write_mmi64);
    //pthread_mutex_lock(&mutex_read_mmi64);
    mmi64_regif_read_32(mmi64_downstream_module, 0, 1, &free_space);
    pthread_mutex_unlock(&mutex_write_mmi64);
    //pthread_mutex_unlock(&mutex_read_mmi64);

#ifndef HDL_SIM
    if (free_space < 2*len_bytes/sizeof(uint32_t)) {
      struct timespec downstream_sleep;
      downstream_sleep.tv_sec = 0;
      downstream_sleep.tv_nsec = 100;
      nanosleep(&downstream_sleep, NULL);
    }
#endif
  }

  log_debug("  @send_data_generic_item: sending %4u bytes to module id: %08x", len_bytes, mmi64_downstream_module->id);

  // the compiler should remove this loop when compiling w/o verbose
  unsigned char v0 __attribute__((unused));
  unsigned char v1 __attribute__((unused));
  unsigned char v2 __attribute__((unused));
  unsigned char v3 __attribute__((unused));
  char          *base_addr;
  uint32_t      curr_byte_index;
  base_addr     = (char *)addr;
  for (curr_byte_index = 0; curr_byte_index < len_bytes; ) {
    v0 = base_addr[curr_byte_index++] & 0xFF;
    v1 = base_addr[curr_byte_index++] & 0xFF;
    v2 = base_addr[curr_byte_index++] & 0xFF;
    v3 = base_addr[curr_byte_index++] & 0xFF;

    log_verbose("    item:  %02X%02X%02X%02X", v3, v2, v1, v0 );
  }

  pthread_mutex_lock(&mutex_write_mmi64);
  //pthread_mutex_lock(&mutex_read_mmi64);
  ret_code = mmi64_regif_write_32_ack(mmi64_downstream_module, 0, len_bytes/sizeof(uint32_t), base_addr_uint32);
  pthread_mutex_unlock(&mutex_write_mmi64);
  //pthread_mutex_unlock(&mutex_read_mmi64);

  return ret_code;
}

//*********************************************************************************************************************
/*
 * \ brief get items coming from HN system, from generic_item upstream interface queue
 *
 */
uint32_t hn_iface_mmi64_get_data_generic_item(char *addr, uint32_t *len_bytes, uint32_t module_index)
{
  uint32_t        ret_code;
  uint32_t        words_in_fifo;
  uint32_t        requested_mmi64_words;
  uint32_t        mmi64_words_read;

  upstream_io_module_t *current_module;

  ret_code = 0;

  mmi64_error_t status   = E_MMI64_OK;
  requested_mmi64_words  = (*len_bytes) / sizeof(mmi64_data_t);

  //we should check that module index is in range of upstream module count
  current_module = &upstream_module_data[module_index];

  log_debug("@hn_iface_mmi64_get_data_generic_item, mmi64 read data module index %2u   module id %08X", module_index,  upstream_module_data[module_index].module->id);

  // let's get the mutex to read from upstream module
  pthread_mutex_lock(&mutex_read_mmi64);
  hn_iface_mmi64_upstream_buffer_get_data_count(module_index, &words_in_fifo);

  log_debug("@hn_iface_mmi64_get_data_generic_item, instance %2u received %d words", module_index,  words_in_fifo);
  log_verbose("@hn_iface_mmi64_get_data_generic_item, len_bytes = %d req_mmi64_words = %d req_items = %d", *len_bytes, requested_mmi64_words,  (*len_bytes) / 4);

  if (words_in_fifo > requested_mmi64_words) {
    mmi64_words_read = requested_mmi64_words;
  } else {
    mmi64_words_read = words_in_fifo;
  }

  log_info("@hn_iface_mmi64_get_data_generic_item, instance %2u requesting %d words %lu items, %lu bytes", module_index,  mmi64_words_read, mmi64_words_read*2, mmi64_words_read*8);

  status = mmi64_upstreamif_get_data_flush(current_module->upstreamif, current_module->buffer, &mmi64_words_read);

  // let's release the muthex whatever the result of the get_data function
  pthread_mutex_unlock(&mutex_read_mmi64);

  if ((status != E_MMI64_OK) && (status != E_MMI64_FIFO_FLUSH))
  {
    log_error("@mmi64_upstreamif_get_data_flush() failed at instance %d with %s", current_module->instance, mmi64_strerror(status));
    return 0;
  }

  if (mmi64_words_read == 1)
  {
    log_debug("@iface_mmi64_get_data_generic_item, instance %2d received  2 items  %016lx",current_module->instance, current_module->buffer[0]);
  }
  else
  {
    log_debug("@iface_mmi64_get_data_generic_item, instance %2d received %d items", current_module->instance, mmi64_words_read * 2);
    log_debug("   first item in chunk: %016lx    last item in chunk: %016lx", current_module->buffer[0], current_module->buffer[mmi64_words_read-1]);
  }

  // copy 32bit
#ifdef VERBOSE
  for(int c = 0; c < mmi64_words_read; c++)
  {
    log_verbose("     item: %08X", (uint32_t)current_module->buffer[c]);
    log_verbose("     item: %08X", (uint32_t)(current_module->buffer[c] >> 32));
  }
#endif

#ifdef STORE_RAW_ITEMS
  if (fd_items != NULL) fwrite(current_module->buffer, 8, mmi64_words_read, fd_items);
#endif

  *len_bytes = mmi64_words_read * 4 * 2;
  memcpy(addr, current_module->buffer, *len_bytes);

  return ret_code;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * \ brief Send data to mmi64 downstream interface moudule in a more efficient way. Send data in bursts of 64bit words
 *
 * \param[in] addr pointer to start address of transfer
 * \param[in] len_bytes number of bytes to transfer
 * \param[in] mmi64_downstream_module pointer to mmi64 module that will receive data on HN system side
 * \return 0 if transfer was successfully performed, otherwise return error code
 */
mmi64_error_t hn_iface_mmi64_send_data_burst_transfer(void *addr, uint32_t len_bytes,uint32_t module_index, hn_shm_buffer_transfer_t shm_operation)
{
  mmi64_error_t      ret_code;
  mmi64_module_t    *mmi64_downstream_module;
  void              *shm_addr;
  uint64_t          *base_addr;
  uint32_t           num_words;
  hn_header_burst_t  burst_header;
  uint64_t          *burst_header_p;

  // split transfer in several write operations, as needed per max mmi64 iface payload size, due to iface limitations and/or reception module fifos on hn_side
  uint32_t   num_transfers; // number of transfers the burst will be splitted on
  uint32_t   c_t_index;     // current transfer index
  uint32_t   c_t_len;       // current transfer size in bytes, multiple of 8bytes (64bitwords)
  uint64_t   c_t_address_offset; // address offset (from shm base) for current transfer
  uint64_t  *c_t_address;   // current transfer base address

  mmi64_downstream_module =  hn_io_modules_downstream_burst_transfer[module_index];

  log_debug("@ hn_iface_mmi64_send_data_burst_transfer -> shm transfer for %u bytes", len_bytes);

  num_words = len_bytes / sizeof(uint64_t); // get number of 64 bit words to write, mmi64_write function receives as param the number of words to write

  log_debug ("hn_iface_mmi64_send_data_burst_transfer for shm write, %u bytes", len_bytes);
  log_verbose("  operation_type %u    dst tile %u    dst addr %08X   size %u bytes (%u words)    local shm_buff_addr 0x%016x    blocking %u",
      shm_operation.type, shm_operation.tile, shm_operation.addr, len_bytes, num_words, shm_operation.shm_addr, shm_operation.blocking
      );

  shm_addr  = (void *)(shm_operation.shm_addr);

  if((shm_operation.type) == HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER)
  {
    // compose msg header and send msg header, and then send data
 
    burst_header.command = 10;
    burst_header.tile    = shm_operation.tile;
    burst_header.size    = len_bytes;
    burst_header.sender  = 0;
    burst_header.id      = 5;
    burst_header.address = shm_operation.addr;

    burst_header_p = (uint64_t *)(&burst_header);

    pthread_mutex_lock(&mutex_write_mmi64);
    ret_code = mmi64_regif_write_64_noack(mmi64_downstream_module, 0, 8, burst_header_p);
    //pthread_mutex_unlock(&mutex_write_mmi64);
    if (ret_code != E_MMI64_OK) 
    {
      log_error("Error writing header of burst transfer to HN system");
       pthread_mutex_unlock(&mutex_write_mmi64);
      return HN_SHM_OPERATION_ERROR;
    }
    else 
    {
      num_transfers = num_words / MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN;
      if (( num_words % MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN) != 0)
      {
        num_transfers = num_transfers + 1;
      }
      c_t_len = MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN;
      base_addr = (uint64_t *)shm_addr;

      log_debug("iface_mmi64_send_burst_data: split burst in %u mmi64 transfers", num_transfers);

      for (c_t_index = 0; c_t_index < (num_transfers-1); c_t_index++)
      {
        c_t_address_offset = c_t_index * c_t_len;
        c_t_address        = base_addr + c_t_address_offset;

        log_debug("send burst transfer %7u of %7u", c_t_index+1, num_transfers);
        //pthread_mutex_lock(&mutex_write_mmi64);
        ret_code = mmi64_regif_write_64_noack(mmi64_downstream_module, 0, c_t_len, c_t_address);
        //pthread_mutex_unlock(&mutex_write_mmi64);
        if (ret_code != E_MMI64_OK) 
        {
          log_error("Error writing data of burst transfer to HN system");
           pthread_mutex_unlock(&mutex_write_mmi64);
          return HN_SHM_OPERATION_ERROR;
        }
      }
      // send last chunk of data from the shm buffer
      log_debug("send last burst transfer %7u of %7u", c_t_index+1, num_transfers);
      c_t_len = num_words % MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN;
      if(c_t_len == 0)
      {
        c_t_len = MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN;
      }
      c_t_address_offset = (c_t_index * MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN);
      c_t_address        = base_addr + c_t_address_offset;
      //pthread_mutex_lock(&mutex_write_mmi64);
      ret_code = mmi64_regif_write_64_noack(mmi64_downstream_module, 0, c_t_len, c_t_address);
      pthread_mutex_unlock(&mutex_write_mmi64);

      log_debug("iface_mmi64_send_burst_data: send last chunk of data, total %u mmi64 transfers",  num_transfers);

      if (ret_code != E_MMI64_OK) 
      {
        log_error("Error writing last data of burst transfer to HN system");
        return HN_SHM_OPERATION_ERROR;
      }
    }     
  }
  else if ((shm_operation.type) == HN_COMMAND_READ_SHARED_MEMORY_BUFFER)
  {
    log_error("@hn_iface_mmi64_send_data_burst_transfer, unexpected command, read_shared_memory_buffer should be triggered as dma transfer ");
  }
  else
  {
    log_error("@hn_iface_mmi64_send_data_burst_transfer, UNEXPTECTED OPERATION (%d)for a downstream burst transfer", shm_operation.type);
  }

  return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * \ brief Get data from mmi64 upstream interface moudule in a more efficient way. 
 *
 * \param[in] addr pointer to start address of transfer
 * \param[in] len_bytes number of bytes to transfer
 * \param[in] mmi64_downstream_module pointer to mmi64 module that will receive data on HN system side
 * \return 0 if transfer was successfully performed, otherwise return error code
 */

uint32_t hn_iface_mmi64_get_data_burst_transfer(char *addr, uint32_t *len_bytes, uint32_t module_index)
{
  uint32_t        ret_code;
  uint32_t        words_in_fifo;
  uint32_t        mmi64_words_read;
  uint32_t        bytes_read;
  uint32_t        requested_mmi64_words;

  upstream_io_module_t *current_module;

  ret_code = 0;

  mmi64_error_t status   = E_MMI64_OK;

  // each word in 8-byte (64-bit)
  requested_mmi64_words  = (*len_bytes) / 8;

  //we should check that module index is in range of upstream module count
  current_module = &upstream_module_data[module_index];

  log_debug("@hn_iface_mmi64_get_data_burst_transfer, mmi64 read data module index %2u   module id %08X", module_index,  upstream_module_data[module_index].module->id);

  // let's lock the mutex before checking any data of the upstream module
  pthread_mutex_lock(&mutex_read_mmi64);
  hn_iface_mmi64_upstream_buffer_get_data_count(module_index, &words_in_fifo);

  log_debug("@hn_iface_mmi64_get_data_burst_transfer, instance %2u received %d words", module_index,  words_in_fifo);

  log_debug("@hn_iface_mmi64_get_data_burst_transfer, len_bytes = %d req_mmi64_words = %d", *len_bytes, requested_mmi64_words);

  if (words_in_fifo > requested_mmi64_words) {
    mmi64_words_read = requested_mmi64_words;
  } else {
    mmi64_words_read = words_in_fifo;
  }

  log_debug("@hn_iface_mmi64_get_data_burst_transfer, instance %2u requesting %d words", module_index,  mmi64_words_read);
  status = mmi64_upstreamif_get_data_flush(current_module->upstreamif, current_module->buffer, &mmi64_words_read);
  //lets release the mutex whatever the status of the operation
  pthread_mutex_unlock(&mutex_read_mmi64);
  if ((status != E_MMI64_OK) && (status != E_MMI64_FIFO_FLUSH))
  {
    log_error("@mmi64_upstreamif_get_data_flush() failed at instance %d with %s", current_module->instance, mmi64_strerror(status));
    return 0;
  }

  bytes_read = mmi64_words_read * 8;

  if (mmi64_words_read == 1)
  {
    log_debug("@iface_mmi64_get_data_burst_transfer, instance %2d received  1 data word %016lx",current_module->instance, current_module->buffer[0]);
  }
  else
  {
    log_debug("@iface_mmi64_get_data_burst_transfer, instance %2d received %d data words", current_module->instance, mmi64_words_read );
  }

  memcpy(addr, current_module->buffer, bytes_read);

  *len_bytes = bytes_read;

  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_send_items( void *addr, uint32_t len_bytes, uint32_t module_index)
{
  profpga_error_t   status;
  uint32_t          ret_code;
  uint32_t          c_burst;     // index of current transfer
  uint32_t          c_burst_len; // length of current transfer
  uint32_t          num_bursts;  // total number of transfers
  uint32_t          buff_lo_ind;
#ifdef VERBOSE
  uint32_t          buff_hi_ind;
#endif
  void             *curr_addr_base; // pointer to start address current transfer
  mmi64_module_t   *mmi64_downstream_module;
 
  mmi64_downstream_module = hn_io_modules_downstream_generic_item[module_index];
 
  log_debug("@hn_iface_mmi64_send_items, send items through downstream module index %u, module id 0x%08X\n", module_index, mmi64_downstream_module->id);
  ret_code = 0;

  if (len_bytes == 0)
  {
    return 1;
  }
  // max downstream payload to guarantee no packet loss
  num_bursts = len_bytes / MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN ;
  if ((len_bytes % MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN) != 0)
  {
    num_bursts = num_bursts + 1;
  }
  c_burst_len = MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN;
  
  log_debug("@hn_iface_mmi64_send_data,  initializing data transfer for %3d bytes in %2d bursts of %2d bytes", len_bytes, num_bursts, c_burst_len);

  for (c_burst = 0; c_burst < (num_bursts - 1); c_burst++)
  {
    buff_lo_ind = (c_burst * MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN );
#ifdef VERBOSE
    buff_hi_ind = (c_burst * MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN ) + c_burst_len - 1; 
    log_debug("@hn_iface_mmi64_send_data, Writing iteration loop %2d / %2d, burst of %d datawords [%4d:%4d]",
        c_burst+1, num_bursts , c_burst_len, buff_lo_ind, buff_hi_ind);
#endif
    curr_addr_base = addr + buff_lo_ind;
    status = hn_iface_mmi64_send_data_generic_item((void *)curr_addr_base, c_burst_len, mmi64_downstream_module);

    //log_debug("  current frame sent");
    CHECK(status);
  }

  // send last burst of items
  c_burst_len = len_bytes % MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN;
  if(c_burst_len == 0)
  {
    c_burst_len = MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN;
  }

  buff_lo_ind = (c_burst * MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN );
#ifdef VERBOSE
  buff_hi_ind = (c_burst * MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN ) + c_burst_len - 1;

  log_debug("@hniface_mmi64_send_data, Writing                %2d / %2d, burst of %d bytes [%4d:%4d]", 
      c_burst+1, num_bursts , c_burst_len, buff_lo_ind, buff_hi_ind);
#endif

  curr_addr_base = addr + buff_lo_ind;
  status =  hn_iface_mmi64_send_data_generic_item((void *)curr_addr_base, c_burst_len, mmi64_downstream_module);
  
  //log_debug("   ...done, check status");
  CHECK(status);
  
  log_debug("@hniface_mmi64_send_data, all data sent, returning");
  //fflush(stdout);

  return ret_code;
}


//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_iface_mmi64_get_data( char *addr, uint32_t *num_bytes, /*uint32_t module_type,*/ uint32_t module_index)
{
  return upstream_module_data[module_index].fptr_get_data(addr, num_bytes, module_index);
}

//*********************************************************************************************************************
// end of file iface_mmi64.c
//*********************************************************************************************************************

