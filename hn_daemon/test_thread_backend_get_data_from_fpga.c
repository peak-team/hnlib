///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 30, 2018
// Design Name: 
// Module Name: Interface connectivity test 
// Project Name: HN daemon
// Target Devices:
// Tool Versions:
// Description:
//  Open communication channel (handler) with profpga-mmi64 modules
//  Scan for MMI64 modules in HN system that belong to the daemon communications infrastructure
//  Start thread reading data comming from upstream modules in HN system
//    Will display received items, and flush the data
//  wait for User to cancel the program execution
//  Close communications channel and release resources
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <time.h>

#define _HN_SHARED_STRUCTURES_DEFINITION
#include "hn_shared_structures.h"

#include "hn_iface_mmi64.h"
#include "hn_exit_program.h"
#include "hn_logging.h"
#include "hn_list.h"
#include "hn_thread_backend_get_data_from_fpga_function.h"

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
static  const char *args_for_thread = "Sin PEAKos No hay Paraiso";
static  pthread_t th_backend_get_data_from_fpga;

/**********************************************************************************************************************
 **
 ** Local Functions implementation
 **
 **********************************************************************************************************************
 **/
// signal handler, on signal reception, sets to 0 global variable to notify threads to stop running
void signal_handler(int sig) 
{
  log_notice("Test cancellation signal captured. Signal value #%d\n", sig);
  hn_exit_program_terminate(0);  
}

/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/
int main (int argc, char *argv[]) 
{
  uint32_t status;

  if (hn_log_initialize(HN_LOG_TO_STDOUT, NULL) < 0)
  {
    printf("ERROR initializing log system: %s\n", strerror(errno));
    return 2;
  }
  log_info("\n");
  log_info("---------------------------------------------------------");
  log_info(" This is a basic test for the thread reading data from   ");
  log_info("   the upstream interface modules                        ");
  log_info("      open communications interface                      ");
  log_info("      scan for mmi64 modules for HN communications       ");
  log_info("      start thread reading data                          ");
  log_info("      close communications interface                     ");
  log_info("---------------------------------------------------------");

  log_debug("registering ctrl+c signal to terminate test");
  // register sigterm signal to gently exit the daemon process
  signal(SIGINT, signal_handler);

  log_info("");
  log_info("Initializing mmi64 communications interface through usb device");
  status = hn_iface_mmi64_init("mmi64");

  if(status != 0)
 {
    log_error("Error, something went unexpectedly wrong");
    log_error("mmi64 support in hn_daemon not initialized");
    log_error("No handler to proFPGA systems loaden nor buffer allocated");
    log_error("Test cancelled");
    return status;
  }

  log_info("");
  log_info("handler to mmi64 infrastructure succesfully loaded");
  log_info("pointers to mmi64 hn modules succesfully assigned");
  log_info("upstream modules initialized\n");
  log_info("");

  log_info("press ctrl+c to finish the test");
  log_info("");

  log_info(" creating read thread");
  pthread_create(&th_backend_get_data_from_fpga , NULL, hn_thread_backend_get_data_from_fpga_function, (void *)args_for_thread);

  // wait for thread finalization
  pthread_join(th_backend_get_data_from_fpga, NULL);

  log_info("... stopping test");
  log_info("");
  log_info("Release resources");
  hn_iface_mmi64_release();
  
  hn_log_close();

  printf("\n");
  printf("All done\n");
  printf("  Bye bye\n");
  printf("\n");
 
  return status;
}

//*********************************************************************************************************************
// end of file test_thread_backend_get_data_from_fpga.c
//*********************************************************************************************************************

