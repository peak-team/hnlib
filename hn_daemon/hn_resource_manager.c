///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Flich (jflich@disca.upv.es)
//
// Create Date: January 25, 2018
// File Name: hn_resource_manager.c
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//      Implements the resource manager, which also provides info about resources 
//
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "hn.h"
#include "hn_logging.h"
#include "hn_resource_manager.h"

#define HN_RESOURCE_MANAGER_TEST_ARCHITECTURE  999

// Ports
#define OPORT_NORTH 0
#define OPORT_EAST  1
#define OPORT_WEST  2
#define OPORT_SOUTH 3
#define OPORT_LOCAL 4

// function prototypes
static int      hn_rscmgt_fill_info();
static uint32_t hn_rscmgt_distance(uint32_t tile_src, uint32_t tile_dst);
static uint32_t hn_rscmgt_route_hop(uint32_t tile_src, uint32_t tile_dst);
static uint32_t hn_rscmgt_next_hop(uint32_t tile, uint32_t port);
static int      hn_rscmgt_fill_mango_arch_14();
static int      hn_rscmgt_fill_mango_arch_17();
static int      hn_rscmgt_fill_mango_arch_20();
static void     hn_rscmgt_initialize_common();


// resource manager data
static hn_rscmgt_info_t     rscmgt_info;        // resource management info

static hn_rscmgt_status_t   rscmgt_status;      // status of the resource manager. Used when initializing

static hn_rscmgt_registered_interrupt_t    rscmgt_registered_interrupt[HN_RSCMGT_MAX_INTS];
static hn_rscmgt_registered_dma_t          rscmgt_registered_dma[HN_RSCMGT_MAX_DMAS];

static uint32_t             lock;               // lock for resource management access from clients

/*
 * This function initializes the resource manager
 *
 * the argument indicates the init mode: getting ARCH_ID from FPGA or using a testing ARCH_ID (no fpga connection)
 */
void hn_rscmgt_initialize(hn_rscmgt_init_mode_t mode) {

  // let's put in this function all the things that are common to all the init modes
  hn_rscmgt_initialize_common();

  if (mode == HN_RESOURCE_MANAGER_INIT_MODE_NO_FPGA) {
    // The resource manager initializes by reseting the system and getting the architecture ID
    hn_rscmgt_reset();

    rscmgt_info.arch_id = HN_RESOURCE_MANAGER_TEST_ARCHITECTURE;
    unsigned int arch_id = hn_rscmgt_get_arch_id();
    hn_rscmgt_set_arch_id(arch_id);
  
    rscmgt_status = HN_RESOURCE_MANAGER_STATUS_RUNNING;

    log_info("Resource manager initialized");
  } else if (mode == HN_RESOURCE_MANAGER_INIT_MODE_FPGA) {
    log_info("Resource manager will be initialized in a distributed way");
  }
}


/*
 * This function ends the resource manager
 */
void hn_rscmgt_end() {
  rscmgt_status = HN_RESOURCE_MANAGER_STATUS_NOT_INITIALIZED;
  memset(&rscmgt_info, 0, sizeof(rscmgt_info));

  log_info("Resource manager ended");
}

/*
 * Resets the info structure
 */
void hn_rscmgt_reset() {
  rscmgt_status = HN_RESOURCE_MANAGER_STATUS_RESETING;
  memset(&rscmgt_info, 0, sizeof(rscmgt_info));
}

/*
 * Gets the current arch id
 */
int hn_rscmgt_get_arch_id() {
  rscmgt_status = HN_RESOURCE_MANAGER_STATUS_GETTING_ARCH_ID;
  return rscmgt_info.arch_id;
}

/*
 * Sets the architecture id
 */
void hn_rscmgt_set_arch_id(unsigned int arch_id)
{
  rscmgt_info.arch_id = arch_id;
  if (hn_rscmgt_fill_info() == 0) {
    rscmgt_status = HN_RESOURCE_MANAGER_STATUS_RUNNING;
    
    log_notice("hn_rscmgt: architecture set to %u", rscmgt_info.arch_id);
  } else {
    rscmgt_status = HN_RESOURCE_MANAGER_STATUS_NOT_INITIALIZED;
  
    log_error("Resource manager could not initialize arch %u", arch_id);
  }
}

/*
 * Gets the status the mgr it is currently
 */
hn_rscmgt_status_t hn_rscmgt_get_status()
{
  return rscmgt_status;
}

/*
 * Gets a constant pointer to the rscmgt structure
 */
const hn_rscmgt_info_t *hn_rscmgt_get_info() {
  return &rscmgt_info;
}


/*
 * This function fills the resource management info
 *
 * Returns 0 if there is an architecture that match the given arch id
 * or -1 otherwise
 */
int hn_rscmgt_fill_info() {
  int i;
  int res = -1;

  switch (rscmgt_info.arch_id) {
    case 0: res = 0; break;
    case 1: res = 0; break;
    case 11:
    case 12:{
                res = 0;
                rscmgt_info.num_tiles = 2;
                rscmgt_info.num_rows  = 1;
                rscmgt_info.num_cols  = 2;
                rscmgt_info.num_vns   = 7;
                // cluster bw
                rscmgt_info.read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                rscmgt_info.avail_read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                rscmgt_info.write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                rscmgt_info.avail_write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                //
                for (i=0;i<2;i++) {
                  strcpy(rscmgt_info.tile_info[i].motherboard, "MB1");
                  strcpy(rscmgt_info.tile_info[i].fpga, "TA1");
                  rscmgt_info.tile_info[i].type      = 0;
                  rscmgt_info.tile_info[i].num_cores = 0;
                  rscmgt_info.tile_info[i].memory_size = 0;
                  rscmgt_info.tile_info[i].first_memory_slot = NULL;
                  rscmgt_info.tile_info[i].free_memory = 0;
                  // assignments
                  rscmgt_info.tile_info[i].assigned = 0;
                  rscmgt_info.tile_info[i].preassigned = 0;
                  // memory bandwidth
                  rscmgt_info.tile_info[i].read_memory_bw = 0;
                  rscmgt_info.tile_info[i].avail_read_memory_bw = 0;
                  rscmgt_info.tile_info[i].write_memory_bw = 0;
                  rscmgt_info.tile_info[i].avail_write_memory_bw = 0;
                  rscmgt_info.tile_info[i].north_port_bw = 0;
                  rscmgt_info.tile_info[i].avail_north_port_bw = 0;
                  // network router bandwidth (output ports) 
                  rscmgt_info.tile_info[i].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                }
                rscmgt_info.tile_info[0].num_cores = 2;
                rscmgt_info.tile_info[1].num_cores = 1;
                               // We define a 2GB memory in tile 0
                rscmgt_info.tile_info[0].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
                rscmgt_info.tile_info[0].free_memory = rscmgt_info.tile_info[0].memory_size;
                rscmgt_info.tile_info[0].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
                hn_rscmgt_memory_slot_t *ms = rscmgt_info.tile_info[0].first_memory_slot;
                ms->size = rscmgt_info.tile_info[0].memory_size;
                ms->start_address = 0x00000000;
                ms->end_address   = 0x7fffffff;
                ms->next_memory_slot = NULL;
                rscmgt_info.tile_info[0].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[0].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[0].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[0].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
            }
                break;
    case 14://subset of the final HN cluster architecture
                hn_rscmgt_fill_mango_arch_14();
                res = 0;
                break;

    case 17://subset of the final HN cluster architecture
                hn_rscmgt_fill_mango_arch_17();
                res = 0;
                break;

    case 20://yet another subset of the final HN cluster architecture
                hn_rscmgt_fill_mango_arch_20();
                res = 0;
                break;

    case 999:   {
                // test architecture
                res = 0;
                rscmgt_info.num_tiles = 64;
                rscmgt_info.num_rows  = 8;
                rscmgt_info.num_cols  = 8;
                rscmgt_info.num_vns   = 3;
                // cluster bw
                rscmgt_info.read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                rscmgt_info.avail_read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                rscmgt_info.write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                rscmgt_info.avail_write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
                //
                for (i=0;i<64;i++) {
                  rscmgt_info.tile_info[i].type      = 1;
                  rscmgt_info.tile_info[i].num_cores = 16;
                  rscmgt_info.tile_info[i].memory_size = 0;
                  rscmgt_info.tile_info[i].first_memory_slot = NULL;
                  rscmgt_info.tile_info[i].free_memory = 0;
                  // assignments
                  rscmgt_info.tile_info[i].assigned = 0;
                  rscmgt_info.tile_info[i].preassigned = 0;
                  // memory bandwidth
                  rscmgt_info.tile_info[i].read_memory_bw = 0;
                  rscmgt_info.tile_info[i].avail_read_memory_bw = 0;
                  rscmgt_info.tile_info[i].write_memory_bw = 0;
                  rscmgt_info.tile_info[i].avail_write_memory_bw = 0;
                  rscmgt_info.tile_info[i].north_port_bw = 0;
                  rscmgt_info.tile_info[i].avail_north_port_bw = 0;
                  // network router bandwidth (output ports) 
                  rscmgt_info.tile_info[i].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                  rscmgt_info.tile_info[i].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
                }
                // this is not real placement, but it is not relevant since it is a test architecture
                for (i=0;i<16;i++) {
                  strcpy(rscmgt_info.tile_info[i].motherboard, "MB1");
                  strcpy(rscmgt_info.tile_info[i].fpga, "TA1");
                }
                for (i=16;i<32;i++) {
                  strcpy(rscmgt_info.tile_info[i].motherboard, "MB1");
                  strcpy(rscmgt_info.tile_info[i].fpga, "TC1");
                }
                for (i=32;i<48;i++) {
                  strcpy(rscmgt_info.tile_info[i].motherboard, "MB1");
                  strcpy(rscmgt_info.tile_info[i].fpga, "TA3");
                }
                for (i=48;i<64;i++) {
                  strcpy(rscmgt_info.tile_info[i].motherboard, "MB1");
                  strcpy(rscmgt_info.tile_info[i].fpga, "TC3");
                }
                // We define a 2GB memory in tile 0
                rscmgt_info.tile_info[0].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
                rscmgt_info.tile_info[0].free_memory = rscmgt_info.tile_info[0].memory_size;
                rscmgt_info.tile_info[0].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
                hn_rscmgt_memory_slot_t *ms = rscmgt_info.tile_info[0].first_memory_slot;
                ms->size = rscmgt_info.tile_info[0].memory_size;
                ms->start_address = 0x00000000;
                ms->end_address   = 0x7fffffff;
                ms->next_memory_slot = NULL;
                rscmgt_info.tile_info[0].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[0].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[0].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[0].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                // We define a 1GB memory in tile 7
                rscmgt_info.tile_info[7].memory_size = (unsigned long long)1024 * 1024 * 1024 * 1;  // 1 GB
                rscmgt_info.tile_info[7].free_memory = rscmgt_info.tile_info[7].memory_size;
                rscmgt_info.tile_info[7].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
                ms = rscmgt_info.tile_info[7].first_memory_slot;
                ms->size = rscmgt_info.tile_info[7].memory_size;
                ms->start_address = 0x00000000;
                ms->end_address   = 0x3fffffff;
                ms->next_memory_slot = NULL;
                rscmgt_info.tile_info[7].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[7].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[7].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[7].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                 // We define a 4GB memory in tile 56
                rscmgt_info.tile_info[56].memory_size = (unsigned long long)1024 * 1024 * 1024 * 4;  // 4 GB
                rscmgt_info.tile_info[56].free_memory = rscmgt_info.tile_info[56].memory_size;
                rscmgt_info.tile_info[56].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
                ms = rscmgt_info.tile_info[56].first_memory_slot;
                ms->size = rscmgt_info.tile_info[56].memory_size;
                ms->start_address = 0x00000000;
                ms->end_address   = 0xffffffff;
                ms->next_memory_slot = NULL;
                rscmgt_info.tile_info[56].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[56].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[56].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[56].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                 // We define a 2GB memory in tile 63
                rscmgt_info.tile_info[63].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
                rscmgt_info.tile_info[63].free_memory = rscmgt_info.tile_info[63].memory_size;
                rscmgt_info.tile_info[63].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
                ms = rscmgt_info.tile_info[63].first_memory_slot;
                ms->size = rscmgt_info.tile_info[63].memory_size;
                ms->start_address = 0x00000000;
                ms->end_address   = 0x7fffffff;
                ms->next_memory_slot = NULL;
                rscmgt_info.tile_info[63].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[63].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[63].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                rscmgt_info.tile_info[63].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
                }
                 break;
  }

  return res;
}

/*
 * \brief This function delivers information about the closest memory (to a tile) with the available memory size
 */
uint32_t hn_rscmgt_find_memory(uint32_t tile, unsigned long long size, uint32_t *tile_mem, uint32_t *starting_addr) {

  log_info("Resource manager: Find memory close to tile %d with memory segment size %llu", tile, size);

  // we search all tiles with memories and see whether there is a free memory slot and annotate also the distance to the
  // tile, we select the closest one
  int found;
  int tile_found;
  int tile_found_distance;
  unsigned int address;
  int i;

  // Patch, just to provide memory segments aligned to 64 bytes
  unsigned long long size_aligned = size + ((((size + 63)/64)*64) - size);  
  found = 0;
  tile_found_distance = 99999999;
  for (i=0;i<rscmgt_info.num_tiles;i++) {
    // we search every tile
    if (rscmgt_info.tile_info[i].free_memory >= size_aligned) {
      // there is enough free memory in this tile, let's check if there is enough slot
      hn_rscmgt_memory_slot_t *ms = rscmgt_info.tile_info[i].first_memory_slot;
      while (ms != NULL) {
        if (ms->size >= size_aligned) {
          // there is one memory slot large enough
          // if distance is shorter than previous ones, then we annotate it
          if (hn_rscmgt_distance(tile, i) < tile_found_distance) {
            found = 1;
            tile_found = i;
            tile_found_distance = hn_rscmgt_distance(tile, i);
            address = ms->start_address;
          }
        }
        ms = ms->next_memory_slot;
      }
    }
  }

  *tile_mem = tile_found;
  *starting_addr = address;

  if (!found) log_warn("Resource manager: Find memory not found");
  else log_info("Memory found at tile %d with starting address %08x", tile_found, address);
  return found;
}

/*
 * \brief This function delivers information about the memories with the available memory size
 */
void hn_rscmgt_find_memories(unsigned long long size, uint32_t **tile_mem, uint32_t **starting_addr, uint32_t *num_memories) {
  // we search all the tiles with memories and see whether there is a free memory slot that first
  int num_memories_local = 0;
  int i;

  // first we check the number of memories that satisfy the request
  for (i=0;i<rscmgt_info.num_tiles;i++) {
    // we search every tile
    if (rscmgt_info.tile_info[i].free_memory >= size) {
      // there is enough free memory in this tile, let's check if there is enough slot
      hn_rscmgt_memory_slot_t *ms = rscmgt_info.tile_info[i].first_memory_slot;
      while (ms != NULL) {
        if (ms->size >= size) {
          // there is one memory slot large enough, let's annotate it
          num_memories_local++;
          break;
        }
        ms = ms->next_memory_slot;
      }
    }
  }

  if (num_memories_local != 0) {
    // we create the exact memory amount to deliver the result
    *tile_mem = malloc(num_memories_local * sizeof(uint32_t));
    *starting_addr = malloc(num_memories_local * sizeof(uint32_t));

    // we loop again to annotate the tiles and the starting address
    num_memories_local = 0;
    for (i=0;i<rscmgt_info.num_tiles;i++) {
      // we search every tile
      if (rscmgt_info.tile_info[i].free_memory >= size) {
        // there is enough free memory in this tile, let's check if there is enough slot
        hn_rscmgt_memory_slot_t *ms = rscmgt_info.tile_info[i].first_memory_slot;
        while (ms != NULL) {
          if (ms->size >= size) {
            // there is one memory slot large enough, let's annotate it
            (*tile_mem)[num_memories_local] = i;
            (*starting_addr)[num_memories_local] = ms->start_address;
            num_memories_local++;
            break;
          }
          ms = ms->next_memory_slot;
        }
      }
    }
  }

  *num_memories = num_memories_local;
}

/*
 * \brief This function allocates a memory segment
 */
uint32_t hn_rscmgt_allocate_memory(uint32_t tile, uint32_t addr, unsigned long long size) {

  log_info("Resource manager: Allocation of memory in tile %d, addr [%08x - %08x], size %llu", tile, addr, addr+size-1, size);

  // Patch, just to provide memory segments aligned to 64 bytes
  unsigned long long size_aligned = size + ((((size + 63)/64)*64) - size);  
  if (rscmgt_info.tile_info[tile].free_memory < size_aligned) {
    log_error("Resource manager: Allocation of memory failed, not enough memory on requested tile");
    return 0;  // error, not enough memory in that tile
  }

  // we search within the free slots of the memory
  int found = 0;
  hn_rscmgt_memory_slot_t *ms = rscmgt_info.tile_info[tile].first_memory_slot;
  hn_rscmgt_memory_slot_t *prev_ms;
  while (ms != NULL) {
    if ( (ms->start_address <= addr) && (ms->end_address >= addr + size_aligned - 1) ) {
      found = 1;
      break;
    }
    prev_ms = ms;
    ms = ms->next_memory_slot;
  }

  if (!found) {
    log_error("Resource manager: Allocation of memory failed, memory address already mapped or large enough memory segment not available");
    return 0; // error, no large enough chunck found
  }

  // we adapt free memory
  rscmgt_info.tile_info[tile].free_memory -= size_aligned;

  // we adapt the chunck
  if (size_aligned == ms->size) {
    // we remove the complete memory slot
    if (rscmgt_info.tile_info[tile].first_memory_slot == ms) {
      rscmgt_info.tile_info[tile].first_memory_slot = ms->next_memory_slot;
      free(ms);
    } else {
      prev_ms->next_memory_slot = ms->next_memory_slot;
      free(ms);
    }
  } else if (ms->start_address == addr) {
    ms->size = ms->size - size_aligned;
    ms->start_address = addr+size_aligned;
  } else if ( (addr + size_aligned - 1) == ms->end_address) {
    ms->size = ms->size - size_aligned;
    ms->end_address = addr - 1;
  } else {
    // we create a new memory slot for the second part of the current chunck and adapt both
    hn_rscmgt_memory_slot_t *ms2 = malloc(sizeof(hn_rscmgt_memory_slot_t));
    ms2->size                   = ms->size - size_aligned - (addr - ms->start_address);
    ms2->start_address          = addr + size_aligned;
    ms2->end_address            = ms->end_address;
    ms2->next_memory_slot       = ms->next_memory_slot;
    ms->size                    = addr - ms->start_address + 1;
    ms->end_address             = addr - 1;
    ms->next_memory_slot        = ms2;
  }

  log_info("Resource manager: Allocation of memory granted");
  return 1;
}

/*
 * \brief This function releases a memory segment
 */
uint32_t hn_rscmgt_release_memory(uint32_t tile, uint32_t addr, unsigned long long size) {

  log_info("Resource manager: Release of memory in tile %d, addr [%08x - %08x], size %llu", tile, addr, addr+size-1, size);

  // Patch, just to provide memory segments aligned to 64 bytes
  unsigned long long size_aligned = size + ((((size + 63)/64)*64) - size);  
  if (rscmgt_info.tile_info[tile].memory_size < size_aligned) {
    log_error("Resource manager: Relase of memory failed as the tile has no memory large enough");
    return 0; // this tile has not such memory size
  }

  // we search within the free slots of the memory one that has a starting address higher than the one to release
  int found = 0;
  hn_rscmgt_memory_slot_t *ms = rscmgt_info.tile_info[tile].first_memory_slot;
  hn_rscmgt_memory_slot_t *prev_ms = NULL;
  while (ms != NULL) {
    if (ms->start_address > addr) {
      found = 1;
      break;
    }
    prev_ms = ms;
    ms = ms->next_memory_slot;
  }

  if (!found) {
    if (rscmgt_info.tile_info[tile].first_memory_slot == NULL) {
      // we create a slot for the memory segment
      ms = malloc(sizeof(hn_rscmgt_memory_slot_t));
      rscmgt_info.tile_info[tile].first_memory_slot = ms;
      ms->start_address = addr;
      ms->end_address   = addr + size_aligned - 1;
      ms->size          = size_aligned;
      ms->next_memory_slot = NULL;
    } else {
      // we add at the end of the previous memory segment (the last one)
      if (prev_ms->end_address + 1 == addr) {
        prev_ms->end_address = addr + size_aligned - 1;
        prev_ms->size = prev_ms->size + size_aligned;
      } else {
        // we create a slot for the memory segment
        ms = malloc(sizeof(hn_rscmgt_memory_slot_t));
        prev_ms->next_memory_slot = ms;
        ms->start_address = addr;
        ms->end_address   = addr + size_aligned - 1;
        ms->size          = size_aligned;
        ms->next_memory_slot = NULL;
      }   
    }
  } else {
    if ( (prev_ms != NULL) && (prev_ms->end_address + 1 == addr) && (ms->start_address == addr + size_aligned) ) {
      // previous slot and next slot are boundary with slot to relase: Join all them
      prev_ms->end_address = ms->end_address;
      prev_ms->size = prev_ms->size + size_aligned + ms->size;
      prev_ms->next_memory_slot = ms->next_memory_slot;
      free(ms);
    } else if ( (prev_ms != NULL) && (prev_ms->end_address + 1 == addr) ) {
      // previous slot is boundary with slot to release: Join both
      prev_ms->end_address = addr + size_aligned - 1;
      prev_ms->size = prev_ms->size + size_aligned;
    } else if ( ms->start_address == addr + size_aligned ) {
      // next slot is boundary with slot to release: Join both
      ms->start_address = addr;
      ms->size = ms->size + size_aligned;
    } else {
      // slot to release is not boundary neither with previous nor with next
      // we create a slot for the memory segment and put it before the current one
      hn_rscmgt_memory_slot_t *ms2 = malloc(sizeof(hn_rscmgt_memory_slot_t));
      ms2->start_address = addr;
      ms2->end_address   = addr + size_aligned - 1;
      ms2->size          = size_aligned;
      ms2->next_memory_slot = ms;
      if (prev_ms == NULL) {
        rscmgt_info.tile_info[tile].first_memory_slot = ms2; 
      } else {
        prev_ms->next_memory_slot = ms2;
      }
    }   
  }

  // we adapt free memory
  rscmgt_info.tile_info[tile].free_memory += size_aligned;

  return 1;
}

/*
 * \brief This function delivers the available network bandwidth between two tiles
 */
unsigned long long hn_rscmgt_get_available_network_bandwidth(uint32_t tile_src, uint32_t tile_dst) {

  // We follow the path and get minimum available port bandwidth
  uint32_t src = tile_src;
  uint32_t dst = tile_dst;
  unsigned long long bw;
  uint32_t first_hop = 1;
  uint32_t op;
  do { 
    op = hn_rscmgt_route_hop(src, dst);
    switch (op) {
      case OPORT_NORTH: if (first_hop) {
                          bw = rscmgt_info.tile_info[src].avail_north_port_bw;
                        } else if (rscmgt_info.tile_info[src].avail_north_port_bw < bw) {
                          bw = rscmgt_info.tile_info[src].avail_north_port_bw;
                        }
                        break;
      case OPORT_EAST: if (first_hop) {
                          bw = rscmgt_info.tile_info[src].avail_east_port_bw;
                        } else if (rscmgt_info.tile_info[src].avail_east_port_bw < bw) {
                          bw = rscmgt_info.tile_info[src].avail_east_port_bw;
                        }
                        break;
       case OPORT_WEST: if (first_hop) {
                          bw = rscmgt_info.tile_info[src].avail_west_port_bw;
                        } else if (rscmgt_info.tile_info[src].avail_west_port_bw < bw) {
                          bw = rscmgt_info.tile_info[src].avail_west_port_bw;
                        }
                        break;
       case OPORT_SOUTH: if (first_hop) {
                          bw = rscmgt_info.tile_info[src].avail_south_port_bw;
                        } else if (rscmgt_info.tile_info[src].avail_south_port_bw < bw) {
                          bw = rscmgt_info.tile_info[src].avail_south_port_bw;
                        }
                        break;
       case OPORT_LOCAL: if (first_hop) {
                          bw = rscmgt_info.tile_info[src].avail_local_port_bw;
                        } else if (rscmgt_info.tile_info[src].avail_local_port_bw < bw) {
                          bw = rscmgt_info.tile_info[src].avail_local_port_bw;
                        }
                        break;
       default         : log_error("Resource manager: Routing path error"); return 0; break;    
    }
    src = hn_rscmgt_next_hop(src, op);
    first_hop = 0;
  } while (src != dst);
  return bw;
}

/*
 * \brief This function delivers the available read memory bandwidth in a tile
 */
unsigned long long hn_rscmgt_get_available_read_memory_bandwidth(uint32_t tile) {
  return rscmgt_info.tile_info[tile].avail_read_memory_bw;
}

/*
 * \brief This function delivers the available write memory bandwidth in a tile
 */
unsigned long long hn_rscmgt_get_available_write_memory_bandwidth(uint32_t tile) {
  return rscmgt_info.tile_info[tile].avail_write_memory_bw;
}

/*
 * \brief This function delivers the available read cluster bandwidth
 */
unsigned long long hn_rscmgt_get_available_read_cluster_bandwidth() {
  return rscmgt_info.avail_read_cluster_bw;
}

/*
 * \brief This function delivers the available write cluster bandwidth
 */
unsigned long long hn_rscmgt_get_available_write_cluster_bandwidth() {
  return rscmgt_info.avail_write_cluster_bw;
}

/*
 * \brief This function reserves network bandwidth between two tiles
 */
uint32_t hn_rscmgt_reserve_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw) {
  log_info("Resource management: reserve network bandwidth request (path %d-%d, bw %llu)", tile_src, tile_dst, bw);

  // We check the path has enough bandwidth
  uint32_t src = tile_src;
  uint32_t dst = tile_dst;
  uint32_t op;
  do { 
    op = hn_rscmgt_route_hop(src, dst);
    switch (op) {
      case OPORT_NORTH: if (rscmgt_info.tile_info[src].avail_north_port_bw < bw) {
                          log_error("Resource management: not enough network bandwidth");
                          return 0;
                        }
                        break;
      case OPORT_EAST: if (rscmgt_info.tile_info[src].avail_east_port_bw < bw) {
                          log_error("Resource management: not enough network bandwidth");
                          return 0;
                        }
                        break;
       case OPORT_WEST: if (rscmgt_info.tile_info[src].avail_west_port_bw < bw) {
                          log_error("Resource management: not enough network bandwidth");
                          return 0;
                        }
                        break;
      case OPORT_SOUTH: if (rscmgt_info.tile_info[src].avail_south_port_bw < bw) {
                          log_error("Resource management: not enough network bandwidth");
                          return 0;
                        }
                        break;
      case OPORT_LOCAL: if (rscmgt_info.tile_info[src].avail_local_port_bw < bw) {
                          log_error("Resource management: not enough network bandwidth");
                          return 0;
                        }
                        break;
       default         : log_error("Resource manager: Routing path error"); return 0; break;    
    }
    src = hn_rscmgt_next_hop(src, op);
  } while (src != dst);

  // Now we reserve the bw
  src = tile_src;
  dst = tile_dst;
  do { 
    op = hn_rscmgt_route_hop(src, dst);
    switch (op) {
      case OPORT_NORTH: rscmgt_info.tile_info[src].avail_north_port_bw -= bw;
                        break;
      case OPORT_EAST:  rscmgt_info.tile_info[src].avail_east_port_bw -= bw;
                        break;
       case OPORT_WEST: rscmgt_info.tile_info[src].avail_west_port_bw -= bw;
                        break;
      case OPORT_SOUTH: rscmgt_info.tile_info[src].avail_south_port_bw -= bw;
                        break;
      case OPORT_LOCAL: rscmgt_info.tile_info[src].avail_local_port_bw -= bw;
                        break;
       default         : log_error("Resource manager: Routing path error"); return 0; break;    
    }
    src = hn_rscmgt_next_hop(src, op);
  } while (src != dst);

  return 1;
}

/*
 * \brief This function reserves read memory bandwidth in a tile
 */
uint32_t hn_rscmgt_reserve_read_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  log_info("Resource management: reserve read memory bandwidth request (tile %d, bw %llu)", tile, bw);

  if (rscmgt_info.tile_info[tile].avail_read_memory_bw < bw) {
    log_error("Resource management: not enough read memory bandwidth");
    return 0;
  }
  rscmgt_info.tile_info[tile].avail_read_memory_bw -= bw;
  return 1;
}

/*
 * \brief This function reserves write memory bandwidth in a tile
 */
uint32_t hn_rscmgt_reserve_write_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  log_info("Resource management: reserve write memory bandwidth request (tile %d, bw %llu)", tile, bw);

  if (rscmgt_info.tile_info[tile].avail_write_memory_bw < bw) {
    log_error("Resource management: not enough write memory bandwidth");
    return 0;
  }
  rscmgt_info.tile_info[tile].avail_write_memory_bw -= bw;
  return 1;
}

/*
 * \brief This function reserves read cluster bandwidth
 */
uint32_t hn_rscmgt_reserve_read_cluster_bandwidth(unsigned long long bw) {
  log_info("Resource management: reserve read cluster bandwidth request (bw %llu, avail %llu)", bw, rscmgt_info.avail_read_cluster_bw);

  if (rscmgt_info.avail_read_cluster_bw < bw) {
    log_error("Resource management: not enough read cluster bandwidth");
    return 0;
  }
  rscmgt_info.avail_read_cluster_bw -= bw;
  return 1;
}

/*
 * \brief This function reserves write cluster bandwidth
 */
uint32_t hn_rscmgt_reserve_write_cluster_bandwidth(unsigned long long bw) {
  log_info("Resource management: reserve write cluster bandwidth request (bw %llu)", bw);

  if (rscmgt_info.avail_write_cluster_bw < bw) {
    log_error("Resource management: not enough write cluster bandwidth");
    return 0;
  }
  rscmgt_info.avail_write_cluster_bw -= bw;
  return 1;
}

/*
 * \brief This function releases network bandwidth between two tiles
 */
uint32_t hn_rscmgt_release_network_bandwidth(uint32_t tile_src, uint32_t tile_dst, unsigned long long bw) {
  log_info("Resource management: release network bandwidth request (path %d-%d, bw %llu)", tile_src, tile_dst, bw);

  // We check the path will not exceed bandwidth
  uint32_t src = tile_src;
  uint32_t dst = tile_dst;
  uint32_t op;
  do { 
    op = hn_rscmgt_route_hop(src, dst);
    switch (op) {
      case OPORT_NORTH: if (rscmgt_info.tile_info[src].north_port_bw < rscmgt_info.tile_info[src].avail_north_port_bw + bw) {
                          log_error("Resource management: network bandwidth exceeded");
                          return 0;
                        }
                        break;
      case OPORT_EAST: if (rscmgt_info.tile_info[src].east_port_bw < rscmgt_info.tile_info[src].avail_east_port_bw + bw) {
                          log_error("Resource management: network bandwidth exceeded");
                          return 0;
                        }
                        break;
      case OPORT_WEST: if (rscmgt_info.tile_info[src].west_port_bw < rscmgt_info.tile_info[src].avail_west_port_bw + bw) {
                          log_error("Resource management: network bandwidth exceeded");
                          return 0;
                        }
                        break;
      case OPORT_SOUTH: if (rscmgt_info.tile_info[src].south_port_bw < rscmgt_info.tile_info[src].avail_south_port_bw + bw) {
                          log_error("Resource management: network bandwidth exceeded");
                          return 0;
                        }
                        break;
      case OPORT_LOCAL: if (rscmgt_info.tile_info[src].local_port_bw < rscmgt_info.tile_info[src].avail_local_port_bw + bw) {
                          log_error("Resource management: network bandwidth exceeded");
                          return 0;
                        }
                        break;
       default         : log_error("Resource manager: Routing path error"); return 0; break;    
    }
    src = hn_rscmgt_next_hop(src, op);
  } while (src != dst);

  // Now we release the bw
  src = tile_src;
  dst = tile_dst;
  do { 
    op = hn_rscmgt_route_hop(src, dst);
    switch (op) {
      case OPORT_NORTH: rscmgt_info.tile_info[src].avail_north_port_bw += bw;
                        break;
      case OPORT_EAST:  rscmgt_info.tile_info[src].avail_east_port_bw += bw;
                        break;
       case OPORT_WEST: rscmgt_info.tile_info[src].avail_west_port_bw += bw;
                        break;
      case OPORT_SOUTH: rscmgt_info.tile_info[src].avail_south_port_bw += bw;
                        break;
      case OPORT_LOCAL: rscmgt_info.tile_info[src].avail_local_port_bw += bw;
                        break;
       default         : log_error("Resource manager: Routing path error"); return 0; break;    
    }
    src = hn_rscmgt_next_hop(src, op);
  } while (src != dst);

  return 1;
}

/*
 * \brief This function releases read memory bandwidth in a tile
 */
uint32_t hn_rscmgt_release_read_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  log_info("Resource management: release read memory bandwidth request (tile %d, bw %llu)", tile, bw);

  if (rscmgt_info.tile_info[tile].read_memory_bw < rscmgt_info.tile_info[tile].avail_read_memory_bw + bw) {
    log_error("Resource management: read memory bandwidth exceeded");
    return 0;
  }
  rscmgt_info.tile_info[tile].avail_read_memory_bw += bw;
  return 1;
}

/*
 * \brief This function releases write memory bandwidth in a tile
 */
uint32_t hn_rscmgt_release_write_memory_bandwidth(uint32_t tile, unsigned long long bw) {
  log_info("Resource management: release write memory bandwidth request (tile %d, bw %llu)", tile, bw);

  if (rscmgt_info.tile_info[tile].write_memory_bw < rscmgt_info.tile_info[tile].avail_write_memory_bw + bw) {
    log_error("Resource management: write memory bandwidth exceeded");
    return 0;
  }
  rscmgt_info.tile_info[tile].avail_write_memory_bw += bw;
  return 1;
}

/*
 * \brief This function releases read cluster bandwidth
 */
uint32_t hn_rscmgt_release_read_cluster_bandwidth(unsigned long long bw) {
  log_info("Resource management: release read cluster bandwidth request (bw %llu, avail %llu)", bw, rscmgt_info.avail_read_cluster_bw);

  if (rscmgt_info.read_cluster_bw < rscmgt_info.avail_read_cluster_bw + bw) {
    log_error("Resource management: read cluster bandwidth exceeded");
    return 0;
  }
  rscmgt_info.avail_read_cluster_bw += bw;
  return 1;
}

/*
 * \brief This function releases write cluster bandwidth
 */
uint32_t hn_rscmgt_release_write_cluster_bandwidth(unsigned long long bw) {
  log_info("Resource management: release write cluster bandwidth request (bw %llu)", bw);

  if (rscmgt_info.write_cluster_bw < rscmgt_info.avail_write_cluster_bw + bw) {
    log_error("Resource management: write cluster bandwidth exceeded");
    return 0;
  }
  rscmgt_info.avail_write_cluster_bw += bw;
  return 1;
}

/*
 * \brief This function returns a set of tiles according to the requested types and number of tiles
 * external_use means that the function has been called from the collector and therefore, log must be outputed
 * if external_use is set to 0 means another function uses this one privately and no log must be provided
 */
uint32_t hn_rscmgt_find_units_set(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t *tiles_dst, uint32_t *types_dst, uint32_t reset_preassignments, uint32_t external_use) {

  if (external_use) log_info("Resource manager: Find units set request");

  // for each tile requested we keep the tile we find and the distance to tile, minimizing its distance
  uint32_t tile_found[HN_RSCMGT_MAX_TILES];
  uint32_t found[HN_RSCMGT_MAX_TILES];
  uint32_t i;
  uint32_t j;

  // we reset all previous preassignments
  if (reset_preassignments) for (i=0;i<rscmgt_info.num_tiles;i++) rscmgt_info.tile_info[i].preassigned = 0;


  // first we initialize the found vector
  for (i=0;i<num_tiles;i++) found[i] = 0;

  int search_again;
  do {
    search_again = 0;
    // now we search all tiles
    for (i=0;i<rscmgt_info.num_tiles;i++) {
      for (j=0;j<num_tiles;j++) {
        if ( (!rscmgt_info.tile_info[i].assigned) && (rscmgt_info.tile_info[i].type == types[j]) && (!rscmgt_info.tile_info[i].preassigned) ) {
          if (!found[j]) {
            found[j] = 1;
            tile_found[j] = i;
            rscmgt_info.tile_info[i].preassigned = 1;
          } else {
            if (hn_rscmgt_distance(tile, tile_found[j]) > hn_rscmgt_distance(tile, i)) {
              rscmgt_info.tile_info[tile_found[j]].preassigned = 0;
              rscmgt_info.tile_info[i].preassigned = 1;
              tile_found[j] = i;
              search_again = 1;   // the one we unpreassigned can be used for another type of tile
            }
          }
        }
      }
    }
  } while (search_again);

  // Let's check we found the set
  for (i=0;i<num_tiles;i++) {
    if (!found[i]) {
      if (external_use) log_warn("Set not found");
      return 0;
    }
  }
    
  // Let's build the set
  if ( (tiles_dst != NULL) && (types_dst != NULL) ) {
    for (i=0;i<num_tiles;i++) {
      tiles_dst[i] = tile_found[i];
      types_dst[i] = types[i];       // we return the set in the same type order
    }
  }
  return 1;
}

/*
 * \brief This function returns all possible (disjoint) set of tiles according to the request types and number of tiles
 */
uint32_t hn_rscmgt_find_units_sets(uint32_t tile, uint32_t num_tiles, uint32_t types[], uint32_t ***tiles_dst, uint32_t ***types_dst, uint32_t *num) {
  log_info("Resource manager: Find units sets request");
  int i;

  // we call find_units_set function as many times as possible to see how many sets we can find
  uint32_t num_sets = 0;
  if (!hn_rscmgt_find_units_set(tile, num_tiles, types, NULL, NULL, 1, 0)) {
    log_warn("No sets found");
    *num = num_sets;
    return 0;
  } else {
    do {
      num_sets++;
    } while (hn_rscmgt_find_units_set(tile, num_tiles, types, NULL, NULL, 0, 0));
  }

  // Now, we create the memory for all the sets
  *tiles_dst = malloc(num_sets*sizeof(uint32_t *));
  *types_dst = malloc(num_sets*sizeof(uint32_t *));
  for (i=0;i<num_sets;i++) {
    (*tiles_dst)[i] = malloc(num_tiles*sizeof(uint32_t));
    (*types_dst)[i] = malloc(num_tiles*sizeof(uint32_t));
    hn_rscmgt_find_units_set(tile, num_tiles, types, (*tiles_dst)[i], (*types_dst)[i], (i==0), 0);
  }
  *num = num_sets;
  return 1;
}

/*
 * \brief This function reserves a set of tiles
 */
void hn_rscmgt_reserve_units_set(uint32_t num_tiles, uint32_t *tiles) {
  log_info("Resource manager: Reserve unit set with %d tiles", num_tiles);

  if (tiles == NULL) log_warn("Resource manager: no tiles provided, nothing done!");

  int i;
  for (i=0;i<num_tiles;i++) {
    if (rscmgt_info.tile_info[tiles[i]].assigned) log_warn("Tile assigned previously");
    rscmgt_info.tile_info[tiles[i]].assigned = 1;
  }
}

/*
 * \brief This function releases a set of tiles
 */
void hn_rscmgt_release_units_set(uint32_t num_tiles, uint32_t *tiles) {
  log_info("Resource manager: Release unit set with %d tiles", num_tiles);

  int i;
  for (i=0;i<num_tiles;i++) {
    if (!rscmgt_info.tile_info[tiles[i]].assigned) log_warn("Tile not assigned previously");
    rscmgt_info.tile_info[tiles[i]].assigned = 0;
  }
}


/*
 * \brief This function delivers the number of tiles
 */
void hn_rscmgt_get_num_tiles(uint32_t *num_tiles, uint32_t *num_tiles_x, uint32_t *num_tiles_y) {
  *num_tiles   = rscmgt_info.num_tiles;
  *num_tiles_x = rscmgt_info.num_cols;
  *num_tiles_y = rscmgt_info.num_rows;
}

// gets the memory size for a tile
void hn_rscmgt_get_memory_size(uint32_t tile, uint32_t *mem_size) {
  *mem_size = 0;

  if (tile < rscmgt_info.num_tiles) {
    *mem_size = rscmgt_info.tile_info[tile].memory_size;
  }
}

// gets the number of networks
void hn_rscmgt_get_num_vns(uint32_t *num_vns) {
  *num_vns = rscmgt_info.num_vns;
}

// gets the info associated to a tile
void hn_rscmgt_get_tile_info(uint32_t tile, uint32_t *tile_type, uint32_t *tile_subtype, uint32_t *mem_size) {
  *mem_size     = 0;
  *tile_type    = 0;
  *tile_subtype = 0;

  if (tile < rscmgt_info.num_tiles) {
    *mem_size     = rscmgt_info.tile_info[tile].memory_size;
    *tile_type    = rscmgt_info.tile_info[tile].type;
    *tile_subtype = rscmgt_info.tile_info[tile].subtype;
  }
}


/*
 * \brief This function registers an interrupt service
 */
uint32_t hn_rscmgt_register_int(uint32_t tile, uint16_t vector_mask, uint32_t client_id, uint32_t *id) {
  int i;
  // we search for a free slot
  for (i=0;i<HN_RSCMGT_MAX_INTS;i++) if (rscmgt_registered_interrupt[i].in_use == 0) break;
  if (i!=HN_RSCMGT_MAX_INTS) {
    rscmgt_registered_interrupt[i].in_use      = 1;
    rscmgt_registered_interrupt[i].client_id   = client_id;
    rscmgt_registered_interrupt[i].tile        = tile;
    rscmgt_registered_interrupt[i].vector_mask = vector_mask;
    *id = i;
    return 1;
  }
  return 0;
}

/*
 * \brief This function provides associated info to an interrupt entry
 */
uint32_t hn_rscmgt_get_interrupt_info(uint32_t id, uint32_t *tile, uint16_t *vector) {
  if (rscmgt_registered_interrupt[id].in_use) {
    *tile = rscmgt_registered_interrupt[id].tile;
    *vector = rscmgt_registered_interrupt[id].vector_mask;
    return 1;
  }
  return 0;
}

/*
 * \brief This function provides whether an interrupt associated to an entry and vector has been received
 */
uint32_t hn_rscmgt_received_interrupt(uint32_t id, uint16_t vector) {
  if (rscmgt_registered_interrupt[id].in_use) {
    if (rscmgt_registered_interrupt[id].received_interrupts & vector) return 1;
  }
  return 0;
}

/*
 * \brief This function cancels an interrupt associated to an entry and vector has been received
 */
void hn_rscmgt_cancel_interrupt(uint32_t id, uint16_t vector) {
  if (rscmgt_registered_interrupt[id].in_use) {
    rscmgt_registered_interrupt[id].received_interrupts &= ~(vector);
  }
}

/*
 * \brief This function annotates a wait on an interrupt associated to an entry and vector has been received
 */
void hn_rscmgt_annotate_wait_on_interrupt(uint32_t id, uint16_t vector) {
  if (rscmgt_registered_interrupt[id].in_use) {
    rscmgt_registered_interrupt[id].wait_on_interrupts = vector;
  }
}

/*
 * \brief This function releases an interrupt entry
 */
uint32_t hn_rscmgt_release_int(uint32_t id) {

  if (rscmgt_registered_interrupt[id].in_use) {
    rscmgt_registered_interrupt[id].in_use = 0;
    return 1;
  }
  return 0;
}

/*
 * \brief This functions returns the client that is waiting for a given interrupt
 */
uint32_t hn_rscmgt_find_waiting_client_to_interrupt(uint32_t tile, uint16_t vector_mask, uint32_t *client_id, uint32_t *id) {
 uint32_t i;                                
 for (i=0;i<HN_RSCMGT_MAX_INTS;i++) {
   if ( (rscmgt_registered_interrupt[i].in_use) &&
         (rscmgt_registered_interrupt[i].tile == tile) &&
         (rscmgt_registered_interrupt[i].vector_mask & vector_mask) &&
         (rscmgt_registered_interrupt[i].wait_on_interrupts & vector_mask) ) {
      *client_id = rscmgt_registered_interrupt[i].client_id;
      *id = i;
      return 1;
   }
 }
 return 0;
}

/*
 * \brief This function registers a DMA service
 */
uint32_t hn_rscmgt_register_dma(uint32_t *id, uint32_t client_id) {
  int i;
  // we search for a free slot
  for (i=0;i<HN_RSCMGT_MAX_DMAS;i++) if (rscmgt_registered_dma[i].in_use == 0) break;
  if (i!=HN_RSCMGT_MAX_DMAS) {
    rscmgt_registered_dma[i].in_use      = 1;
    rscmgt_registered_dma[i].client_id   = client_id;
    *id = i;
    return 1;
  }
  return 0;
}

/*
 * \brief This function writes the dma data into an entry
 */
uint32_t hn_rscmgt_write_dma_operation(uint32_t id, uint32_t channel, uint32_t tile_src, uint32_t addr_src, unsigned long long size, uint32_t tile_dst, uint32_t addr_dst, uint32_t to_unit, uint32_t to_mem, uint32_t to_ext, uint32_t notify) {
  if (rscmgt_registered_dma[id].in_use == 0) return 0;

  rscmgt_registered_dma[id].tile_src = tile_src;
  rscmgt_registered_dma[id].addr_src = addr_src;
  rscmgt_registered_dma[id].size     = size;
  rscmgt_registered_dma[id].tile_dst = tile_dst;
  rscmgt_registered_dma[id].addr_dst = addr_dst;
  rscmgt_registered_dma[id].channel  = channel;
  rscmgt_registered_dma[id].to_unit  = to_unit;
  rscmgt_registered_dma[id].to_mem   = to_mem;
  rscmgt_registered_dma[id].to_ext   = to_ext;
  rscmgt_registered_dma[id].notify   = notify;
  rscmgt_registered_dma[id].completed= 0;
  return 1;
}

/*
 * \brief This function finds a client waiting on a completion of a DMA device
 */

uint32_t hn_rscmgt_find_waiting_client_to_dma(uint32_t tile, uint32_t channel, uint32_t *client_id, uint32_t *id) {

  uint32_t i;

  for (i=0;i<HN_RSCMGT_MAX_DMAS;i++) {
    if ( rscmgt_registered_dma[i].in_use && (rscmgt_registered_dma[i].tile_src == tile) && (rscmgt_registered_dma[i].channel == channel) && (rscmgt_registered_dma[i].notify == 1) ) {
      *id = i;
      *client_id = rscmgt_registered_dma[i].client_id;
      return 1;
    }
  }
  return 0;
}

/*
 * \brief This function annotates a wait on a dma
 */
void hn_rscmgt_annotate_wait_on_dma(uint32_t id, uint32_t value) {
  rscmgt_registered_dma[id].notify = value;
}

/*
 * \brief This function releases a DMA entry
 */
uint32_t hn_rscmgt_release_dma(uint32_t id) {
  if (rscmgt_registered_dma[id].in_use) {
    rscmgt_registered_dma[id].in_use = 0;
    return 1;
  }
  return 0;
}

/*
 * \brief This function gets the descritpors of the physical location of a tile in the system
 */
uint32_t hn_rscmgt_get_tile_location(uint32_t tile, char *mb, char *fm) {
  strcpy(mb, rscmgt_info.tile_info[tile].motherboard);
  strcpy(fm, rscmgt_info.tile_info[tile].fpga);
  // we are not currently doing any error control...we could request an index out of bounds
  return 0;
}

// ------------------------------------------------------------------------------------------
// Private functions

/*
 * \brief This function returns the manhattan distance between two tiles
 */
uint32_t hn_rscmgt_distance(uint32_t tile_src, uint32_t tile_dst) {
 uint32_t x_src = tile_src % rscmgt_info.num_cols;
 uint32_t y_src = tile_src / rscmgt_info.num_cols;
 uint32_t x_dst = tile_dst % rscmgt_info.num_cols;
 uint32_t y_dst = tile_dst / rscmgt_info.num_cols;

 return abs(x_src - x_dst) + abs(y_src - y_dst);
}

/*
 * \brief This function performs routing
 */
uint32_t hn_rscmgt_route_hop(uint32_t tile_src, uint32_t tile_dst) {
 uint32_t x_src = tile_src % rscmgt_info.num_cols;
 uint32_t y_src = tile_src / rscmgt_info.num_cols;
 uint32_t x_dst = tile_dst % rscmgt_info.num_cols;
 uint32_t y_dst = tile_dst / rscmgt_info.num_cols;

 if (x_src < x_dst) return OPORT_EAST;
 if (x_src > x_dst) return OPORT_WEST;
 if (y_src < y_dst) return OPORT_SOUTH;
 if (y_src > y_dst) return OPORT_NORTH;
 return OPORT_LOCAL;
}

/*
 * \brief This function performs next hop
 */
uint32_t hn_rscmgt_next_hop(uint32_t tile, uint32_t port) {
  if (port == OPORT_EAST) return tile + 1;
  if (port == OPORT_WEST) return tile - 1;
  if (port == OPORT_NORTH) return tile - rscmgt_info.num_cols;
  if (port == OPORT_SOUTH) return tile + rscmgt_info.num_cols;
  return tile;
}


// fills rscmgt_info for mango arch 14
int hn_rscmgt_fill_mango_arch_14() {
  rscmgt_info.num_tiles = 8;
  rscmgt_info.num_rows  = 2;
  rscmgt_info.num_cols  = 4;
  rscmgt_info.num_vns   = 3;

  // cluster bw
  // FIXME
  rscmgt_info.read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.avail_read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.avail_write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;

  // Tile 0 
  strcpy(rscmgt_info.tile_info[0].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[0].fpga, "TA1");
  rscmgt_info.tile_info[0].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[0].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[0].num_cores = 2;

  //memory
  rscmgt_info.tile_info[0].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
  rscmgt_info.tile_info[0].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[0].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
  hn_rscmgt_memory_slot_t *ms_arch14_t0 = rscmgt_info.tile_info[0].first_memory_slot;
  ms_arch14_t0->size = rscmgt_info.tile_info[0].memory_size;
  ms_arch14_t0->start_address = 0x00000000;
  ms_arch14_t0->end_address   = 0x7fffffff;
  ms_arch14_t0->next_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[0].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  // assignments
  rscmgt_info.tile_info[0].assigned = 0;
  rscmgt_info.tile_info[0].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[0].north_port_bw = 0;
  rscmgt_info.tile_info[0].avail_north_port_bw = 0;
  rscmgt_info.tile_info[0].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].west_port_bw = 0;
  rscmgt_info.tile_info[0].avail_west_port_bw = 0;
  rscmgt_info.tile_info[0].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 1 
  strcpy(rscmgt_info.tile_info[1].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[1].fpga, "TA1");
  rscmgt_info.tile_info[1].type = HN_TILE_FAMILY_DCT;
  rscmgt_info.tile_info[1].subtype = HN_DCT_MODEL_BASE;
  rscmgt_info.tile_info[1].num_cores = 1;

  //memory
  rscmgt_info.tile_info[1].memory_size = 0;
  rscmgt_info.tile_info[1].free_memory = rscmgt_info.tile_info[1].memory_size;
  rscmgt_info.tile_info[1].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[1].read_memory_bw = 0;
  rscmgt_info.tile_info[1].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[1].write_memory_bw = 0;
  rscmgt_info.tile_info[1].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[1].assigned = 0;
  rscmgt_info.tile_info[1].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[1].north_port_bw = 0;
  rscmgt_info.tile_info[1].avail_north_port_bw = 0;
  rscmgt_info.tile_info[1].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 2 
  strcpy(rscmgt_info.tile_info[2].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[2].fpga, "TA3");

  rscmgt_info.tile_info[2].type = HN_TILE_FAMILY_TETRAPOD;
  rscmgt_info.tile_info[2].subtype = HN_TETRAPOD_MODEL_DE4WI4;
  rscmgt_info.tile_info[2].num_cores = 1;
  //memory
  rscmgt_info.tile_info[2].memory_size = 0;
  rscmgt_info.tile_info[2].free_memory = rscmgt_info.tile_info[2].memory_size;
  rscmgt_info.tile_info[2].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[2].read_memory_bw = 0;
  rscmgt_info.tile_info[2].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[2].write_memory_bw = 0;
  rscmgt_info.tile_info[2].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[2].assigned = 0;
  rscmgt_info.tile_info[2].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[2].north_port_bw = 0;
  rscmgt_info.tile_info[2].avail_north_port_bw = 0;
  rscmgt_info.tile_info[2].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 3 
  strcpy(rscmgt_info.tile_info[3].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[3].fpga, "TA3");
  rscmgt_info.tile_info[3].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[3].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[3].num_cores = 2;
  //memory
  rscmgt_info.tile_info[3].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
  rscmgt_info.tile_info[3].free_memory = rscmgt_info.tile_info[3].memory_size;
  rscmgt_info.tile_info[3].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
  hn_rscmgt_memory_slot_t *ms_arch14_t3 = rscmgt_info.tile_info[3].first_memory_slot;
  ms_arch14_t3->size = rscmgt_info.tile_info[3].memory_size;
  ms_arch14_t3->start_address = 0x00000000;
  ms_arch14_t3->end_address   = 0x7fffffff;
  ms_arch14_t3->next_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[3].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[3].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[3].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[3].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  // assignments
  rscmgt_info.tile_info[3].assigned = 0;
  rscmgt_info.tile_info[3].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[3].north_port_bw = 0;
  rscmgt_info.tile_info[3].avail_north_port_bw = 0;
  rscmgt_info.tile_info[3].east_port_bw = 0;
  rscmgt_info.tile_info[3].avail_east_port_bw = 0;
  rscmgt_info.tile_info[3].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 4 
  strcpy(rscmgt_info.tile_info[4].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[4].fpga, "TA1");
  rscmgt_info.tile_info[4].type = HN_TILE_FAMILY_DCT;
  rscmgt_info.tile_info[4].subtype = HN_DCT_MODEL_BASE;
  rscmgt_info.tile_info[4].num_cores = 1;
  //memory
  rscmgt_info.tile_info[4].memory_size = 0;
  rscmgt_info.tile_info[4].free_memory = rscmgt_info.tile_info[4].memory_size;
  rscmgt_info.tile_info[4].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[4].read_memory_bw = 0;
  rscmgt_info.tile_info[4].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[4].write_memory_bw = 0;
  rscmgt_info.tile_info[4].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[4].assigned = 0;
  rscmgt_info.tile_info[4].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[4].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].west_port_bw = 0;
  rscmgt_info.tile_info[4].avail_west_port_bw = 0;
  rscmgt_info.tile_info[4].south_port_bw = 0;
  rscmgt_info.tile_info[4].avail_south_port_bw = 0;
  rscmgt_info.tile_info[4].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 5 
  strcpy(rscmgt_info.tile_info[5].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[5].fpga, "TA1");
  rscmgt_info.tile_info[5].type = HN_TILE_FAMILY_NUPLUS;
  rscmgt_info.tile_info[5].subtype = HN_NUPLUS_MODEL_BASE;
  rscmgt_info.tile_info[5].num_cores = 1;
  //memory
  rscmgt_info.tile_info[5].memory_size = 0;
  rscmgt_info.tile_info[5].free_memory = rscmgt_info.tile_info[5].memory_size;
  rscmgt_info.tile_info[5].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[5].read_memory_bw = 0;
  rscmgt_info.tile_info[5].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[5].write_memory_bw = 0;
  rscmgt_info.tile_info[5].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[5].assigned = 0;
  rscmgt_info.tile_info[5].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[5].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].south_port_bw = 0;
  rscmgt_info.tile_info[5].avail_south_port_bw = 0;
  rscmgt_info.tile_info[5].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 6 
  strcpy(rscmgt_info.tile_info[6].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[6].fpga, "TA3");
  rscmgt_info.tile_info[6].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[6].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[6].num_cores = 1;
  //memory
  rscmgt_info.tile_info[6].memory_size = 0;
  rscmgt_info.tile_info[6].free_memory = rscmgt_info.tile_info[6].memory_size;
  rscmgt_info.tile_info[6].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[6].read_memory_bw = 0;
  rscmgt_info.tile_info[6].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[6].write_memory_bw = 0;
  rscmgt_info.tile_info[6].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[6].assigned = 0;
  rscmgt_info.tile_info[6].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[6].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].south_port_bw = 0;
  rscmgt_info.tile_info[6].avail_south_port_bw = 0;
  rscmgt_info.tile_info[6].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 7 
  strcpy(rscmgt_info.tile_info[7].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[7].fpga, "TA3");
  rscmgt_info.tile_info[7].type = HN_TILE_FAMILY_NUPLUS;
  rscmgt_info.tile_info[7].subtype = HN_NUPLUS_MODEL_BASE;
  rscmgt_info.tile_info[7].num_cores = 1;
  //memory
  rscmgt_info.tile_info[7].memory_size = 0;
  rscmgt_info.tile_info[7].free_memory = rscmgt_info.tile_info[7].memory_size;
  rscmgt_info.tile_info[7].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[7].read_memory_bw = 0;
  rscmgt_info.tile_info[7].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[7].write_memory_bw = 0;
  rscmgt_info.tile_info[7].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[7].assigned = 0;
  rscmgt_info.tile_info[7].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[7].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].east_port_bw = 0;
  rscmgt_info.tile_info[7].avail_east_port_bw = 0;
  rscmgt_info.tile_info[7].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].south_port_bw = 0;
  rscmgt_info.tile_info[7].avail_south_port_bw = 0;
  rscmgt_info.tile_info[7].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  return 0;
}

// fills rscmgt_info for mango arch 17
int hn_rscmgt_fill_mango_arch_17() {
  rscmgt_info.num_tiles = 4;
  rscmgt_info.num_rows  = 2;
  rscmgt_info.num_cols  = 2;
  rscmgt_info.num_vns   = 3;

  // cluster bw
  // FIXME
  rscmgt_info.read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.avail_read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.avail_write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;

  // Tile 0
  //strcpy(rscmgt_info.tile_info[0].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[0].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[0].fpga, "TA1");
  rscmgt_info.tile_info[0].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[0].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[0].num_cores = 2;
  rscmgt_info.tile_info[0].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
  rscmgt_info.tile_info[0].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[0].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
  hn_rscmgt_memory_slot_t *ms_arch17_t0 = rscmgt_info.tile_info[0].first_memory_slot;
  ms_arch17_t0->size = rscmgt_info.tile_info[0].memory_size;
  ms_arch17_t0->start_address = 0x00000000;
  ms_arch17_t0->end_address   = 0x7fffffff;
  ms_arch17_t0->next_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[0].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  // assignments
  rscmgt_info.tile_info[0].assigned = 0;
  rscmgt_info.tile_info[0].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[0].north_port_bw = 0;
  rscmgt_info.tile_info[0].avail_north_port_bw = 0;
  rscmgt_info.tile_info[0].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].west_port_bw = 0;
  rscmgt_info.tile_info[0].avail_west_port_bw = 0;
  rscmgt_info.tile_info[0].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 1 
  //strcpy(rscmgt_info.tile_info[1].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[1].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[1].fpga, "TA1");
  rscmgt_info.tile_info[1].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[1].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[1].num_cores = 1;
  //memory
  rscmgt_info.tile_info[1].memory_size = 0;
  rscmgt_info.tile_info[1].free_memory = rscmgt_info.tile_info[1].memory_size;
  rscmgt_info.tile_info[1].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[1].read_memory_bw = 0;
  rscmgt_info.tile_info[1].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[1].write_memory_bw = 0;
  rscmgt_info.tile_info[1].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[1].assigned = 0;
  rscmgt_info.tile_info[1].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[1].north_port_bw = 0;
  rscmgt_info.tile_info[1].avail_north_port_bw = 0;
  rscmgt_info.tile_info[1].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 2 
  //strcpy(rscmgt_info.tile_info[2].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[2].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[2].fpga, "TA1");
  rscmgt_info.tile_info[2].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[2].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[2].num_cores = 1;
  //memory
  rscmgt_info.tile_info[2].memory_size = 0;
  rscmgt_info.tile_info[2].free_memory = rscmgt_info.tile_info[2].memory_size;
  rscmgt_info.tile_info[2].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[2].read_memory_bw = 0;
  rscmgt_info.tile_info[2].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[2].write_memory_bw = 0;
  rscmgt_info.tile_info[2].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[2].assigned = 0;
  rscmgt_info.tile_info[2].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[2].north_port_bw = 0;
  rscmgt_info.tile_info[2].avail_north_port_bw = 0;
  rscmgt_info.tile_info[2].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 3 
  //strcpy(rscmgt_info.tile_info[3].motherboard, "MB1");
  strcpy(rscmgt_info.tile_info[3].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[3].fpga, "TA1");
  rscmgt_info.tile_info[3].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[3].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[3].num_cores = 2;
  //memory
  rscmgt_info.tile_info[3].memory_size = 0;
  rscmgt_info.tile_info[3].free_memory = rscmgt_info.tile_info[3].memory_size;
  rscmgt_info.tile_info[3].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[3].read_memory_bw = 0;
  rscmgt_info.tile_info[3].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[3].write_memory_bw = 0;
  rscmgt_info.tile_info[3].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[3].assigned = 0;
  rscmgt_info.tile_info[3].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[3].north_port_bw = 0;
  rscmgt_info.tile_info[3].avail_north_port_bw = 0;
  rscmgt_info.tile_info[3].east_port_bw = 0;
  rscmgt_info.tile_info[3].avail_east_port_bw = 0;
  rscmgt_info.tile_info[3].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  return 0;
}

// fills rscmgt_info for mango arch 20
int hn_rscmgt_fill_mango_arch_20() {
  rscmgt_info.num_tiles = 8;
  rscmgt_info.num_rows  = 2;
  rscmgt_info.num_cols  = 4;
  rscmgt_info.num_vns   = 3;

  // cluster bw
  // FIXME
  rscmgt_info.read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.avail_read_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;
  rscmgt_info.avail_write_cluster_bw = (unsigned long long) 1024 * 1024 * 1024 * 1;

  // Tile 0 
  strcpy(rscmgt_info.tile_info[0].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[0].fpga, "TA1");
  rscmgt_info.tile_info[0].type = HN_TILE_FAMILY_TETRAPOD;
  rscmgt_info.tile_info[0].subtype = HN_TETRAPOD_MODEL_DE4WI4;
  rscmgt_info.tile_info[0].num_cores = 2;
  //memory
  rscmgt_info.tile_info[0].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
  rscmgt_info.tile_info[0].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[0].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
  hn_rscmgt_memory_slot_t *ms_arch14_t0 = rscmgt_info.tile_info[0].first_memory_slot;
  ms_arch14_t0->size = rscmgt_info.tile_info[0].memory_size;
  ms_arch14_t0->start_address = 0x00000000;
  ms_arch14_t0->end_address   = 0x7fffffff;
  ms_arch14_t0->next_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[0].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[0].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  // assignments
  rscmgt_info.tile_info[0].assigned = 0;
  rscmgt_info.tile_info[0].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[0].north_port_bw = 0;
  rscmgt_info.tile_info[0].avail_north_port_bw = 0;
  rscmgt_info.tile_info[0].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].west_port_bw = 0;
  rscmgt_info.tile_info[0].avail_west_port_bw = 0;
  rscmgt_info.tile_info[0].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[0].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 1 
  strcpy(rscmgt_info.tile_info[1].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[1].fpga, "TA1");
  rscmgt_info.tile_info[1].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[1].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[1].num_cores = 1;
  //memory
  rscmgt_info.tile_info[1].memory_size = 0;
  rscmgt_info.tile_info[1].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[1].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[1].read_memory_bw = 0;
  rscmgt_info.tile_info[1].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[1].write_memory_bw = 0;
  rscmgt_info.tile_info[1].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[1].assigned = 0;
  rscmgt_info.tile_info[1].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[1].north_port_bw = 0;
  rscmgt_info.tile_info[1].avail_north_port_bw = 0;
  rscmgt_info.tile_info[1].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[1].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 2 
  strcpy(rscmgt_info.tile_info[2].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[2].fpga, "TA3");
  rscmgt_info.tile_info[2].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[2].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[2].num_cores = 1;
  //memory
  rscmgt_info.tile_info[2].memory_size = 0;
  rscmgt_info.tile_info[2].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[2].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[2].read_memory_bw = 0;
  rscmgt_info.tile_info[2].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[2].write_memory_bw = 0;
  rscmgt_info.tile_info[2].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[2].assigned = 0;
  rscmgt_info.tile_info[2].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[2].north_port_bw = 0;
  rscmgt_info.tile_info[2].avail_north_port_bw = 0;
  rscmgt_info.tile_info[2].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[2].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 3 
  strcpy(rscmgt_info.tile_info[3].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[3].fpga, "TA3");
  rscmgt_info.tile_info[3].type = HN_TILE_FAMILY_DCT;
  rscmgt_info.tile_info[3].subtype = HN_DCT_MODEL_BASE;
  rscmgt_info.tile_info[3].num_cores = 2;
  //memory
  rscmgt_info.tile_info[3].memory_size = (unsigned long long)1024 * 1024 * 1024 * 2;  // 2 GB
  rscmgt_info.tile_info[3].free_memory = rscmgt_info.tile_info[3].memory_size;
  rscmgt_info.tile_info[3].first_memory_slot = malloc(sizeof(hn_rscmgt_memory_slot_t));
  hn_rscmgt_memory_slot_t *ms_arch14_t3 = rscmgt_info.tile_info[3].first_memory_slot;
  ms_arch14_t3->size = rscmgt_info.tile_info[3].memory_size;
  ms_arch14_t3->start_address = 0x00000000;
  ms_arch14_t3->end_address   = 0x7fffffff;
  ms_arch14_t3->next_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[3].read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[3].avail_read_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[3].write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  rscmgt_info.tile_info[3].avail_write_memory_bw = (unsigned long long)1024 * 1024 * 40 * 64;
  // assignments
  rscmgt_info.tile_info[3].assigned = 0;
  rscmgt_info.tile_info[3].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[3].north_port_bw = 0;
  rscmgt_info.tile_info[3].avail_north_port_bw = 0;
  rscmgt_info.tile_info[3].east_port_bw = 0;
  rscmgt_info.tile_info[3].avail_east_port_bw = 0;
  rscmgt_info.tile_info[3].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_south_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[3].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 4 
  strcpy(rscmgt_info.tile_info[4].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[4].fpga, "TA1");
  rscmgt_info.tile_info[4].type = HN_TILE_FAMILY_PEAK;
  rscmgt_info.tile_info[4].subtype = HN_PEAK_MANYCORE_0;
  rscmgt_info.tile_info[4].num_cores = 1;
  //memory
  rscmgt_info.tile_info[4].memory_size = 0;
  rscmgt_info.tile_info[4].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[4].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[4].read_memory_bw = 0;
  rscmgt_info.tile_info[4].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[4].write_memory_bw = 0;
  rscmgt_info.tile_info[4].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[4].assigned = 0;
  rscmgt_info.tile_info[4].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[4].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].west_port_bw = 0;
  rscmgt_info.tile_info[4].avail_west_port_bw = 0;
  rscmgt_info.tile_info[4].south_port_bw = 0;
  rscmgt_info.tile_info[4].avail_south_port_bw = 0;
  rscmgt_info.tile_info[4].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[4].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 5 
  strcpy(rscmgt_info.tile_info[5].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[5].fpga, "TA1");
  rscmgt_info.tile_info[5].type = HN_TILE_FAMILY_NUPLUS;
  rscmgt_info.tile_info[5].subtype = HN_NUPLUS_MODEL_BASE;
  rscmgt_info.tile_info[5].num_cores = 1;
  //memory
  rscmgt_info.tile_info[5].memory_size = 0;
  rscmgt_info.tile_info[5].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[5].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[5].read_memory_bw = 0;
  rscmgt_info.tile_info[5].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[5].write_memory_bw = 0;
  rscmgt_info.tile_info[5].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[5].assigned = 0;
  rscmgt_info.tile_info[5].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[5].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].south_port_bw = 0;
  rscmgt_info.tile_info[5].avail_south_port_bw = 0;
  rscmgt_info.tile_info[5].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[5].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 6 
  strcpy(rscmgt_info.tile_info[6].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[6].fpga, "TA3");
  rscmgt_info.tile_info[6].type = HN_TILE_FAMILY_DCT;
  rscmgt_info.tile_info[6].subtype = HN_DCT_MODEL_BASE;
  rscmgt_info.tile_info[6].num_cores = 1;
  //memory
  rscmgt_info.tile_info[6].memory_size = 0;
  rscmgt_info.tile_info[6].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[6].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[6].read_memory_bw = 0;
  rscmgt_info.tile_info[6].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[6].write_memory_bw = 0;
  rscmgt_info.tile_info[6].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[6].assigned = 0;
  rscmgt_info.tile_info[6].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[6].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_east_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].south_port_bw = 0;
  rscmgt_info.tile_info[6].avail_south_port_bw = 0;
  rscmgt_info.tile_info[6].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[6].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  // Tile 7 
  strcpy(rscmgt_info.tile_info[7].motherboard, "MB2");
  strcpy(rscmgt_info.tile_info[7].fpga, "TA3");
  rscmgt_info.tile_info[7].type = HN_TILE_FAMILY_NUPLUS;
  rscmgt_info.tile_info[7].subtype = HN_NUPLUS_MODEL_BASE;
  rscmgt_info.tile_info[7].num_cores = 1;
  //memory
  rscmgt_info.tile_info[7].memory_size = 0;
  rscmgt_info.tile_info[7].free_memory = rscmgt_info.tile_info[0].memory_size;
  rscmgt_info.tile_info[7].first_memory_slot = NULL;
  // memory bandwidth
  rscmgt_info.tile_info[7].read_memory_bw = 0;
  rscmgt_info.tile_info[7].avail_read_memory_bw = 0;
  rscmgt_info.tile_info[7].write_memory_bw = 0;
  rscmgt_info.tile_info[7].avail_write_memory_bw = 0;
  // assignments
  rscmgt_info.tile_info[7].assigned = 0;
  rscmgt_info.tile_info[7].preassigned = 0;
  // network router bandwidth (output ports) 
  rscmgt_info.tile_info[7].north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].avail_north_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].east_port_bw = 0;
  rscmgt_info.tile_info[7].avail_east_port_bw = 0;
  rscmgt_info.tile_info[7].west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].avail_west_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].south_port_bw = 0;
  rscmgt_info.tile_info[7].avail_south_port_bw = 0;
  rscmgt_info.tile_info[7].local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;
  rscmgt_info.tile_info[7].avail_local_port_bw = (unsigned long long)1024 * 1024 * 40 * 64 / 8;

  return 0;
}

/*
 * This function initializes the common parts to all init modes
 */
void hn_rscmgt_initialize_common() {
  rscmgt_status = HN_RESOURCE_MANAGER_STATUS_NOT_INITIALIZED;

  // we initialize interrupt vector
  memset(&rscmgt_registered_interrupt, 0, sizeof(rscmgt_registered_interrupt));

  // we initialize the lock
  lock = 0;
}

/*!
 * \brief Gets the resource manager lock
 */
uint32_t hn_rscmgt_get_lock() {return lock;}

/*!
 * \brief Sets the resource manager lock
 */
void hn_rscmgt_set_lock() {lock = 1;}

/*!
 * \brief Reset the resource manager lock
 */
void hn_rscmgt_reset_lock() {lock = 0;}


