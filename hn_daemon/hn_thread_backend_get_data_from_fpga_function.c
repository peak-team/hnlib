///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 19, 2018
// Design Name: 
// Module Name: thread backend from fpga 
// Project Name: HN daemon
// Target Devices:
// Tool Versions:
// Description:
//     Read incoming items of from HN_System on FPGA.
//     Each incoming items fifo is associated to one upstream module on the fpga, this is forced by MMI64 code.
//     This thread reads data from each fifo based on a selected policy to guaranty QOS (TO-DO: implement QOS arbiter),
//           currently reads in round robin manner.
//     Read items are propagated to the application in two different ways, depending on data transfer type
//           - regular items: are stored in a fifo to later be read and processed by the thread_item_dispatcher
//           - burst transfer data: is stored in memory, trasnfer status is checked in order to send completion item notification
//             to the application which programmed the transfer when the app requested to be notified on transfer completion.
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <time.h>

#include "hn_list.h"
#include "hn_utils.h"
#include "hn_exit_program.h"
#include "hn_logging.h"
#include "hn_buffer_fifo.h"
#include "hn_iface_mmi64.h"
#include "hn_rr_arb.h"
#include "hn_thread_backend_get_data_from_fpga_function.h"
#include "hn_thread_item_collector_function.h"

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/
// this values will be stored in common, globals....
#define HN_TBGDFF_ARBITER_POLICY_ROUND_ROBIN  (uint32_t)0  // replace macro name to a simpler and reusable name
#define HN_TBGDFF_ARBITER_POLICY_WEIGHTS      (uint32_t)1

/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

static uint32_t arbiter_policy = HN_TBGDFF_ARBITER_POLICY_ROUND_ROBIN;

static pthread_mutex_t upstream_buffer_count_refresh_lock;   // mutex signal for timestruct, .... necessary ? (check with Rafa)
static struct timespec upstream_buffer_count_refresh_ts;     // timestruct


static uint32_t number_of_upstream_fifos;

static pthread_mutex_t channel_recv_lock;   // mutex signal for timestruct, .... necessary ? (check with Rafa)
rr_arb_t channel_recv_arb;

/**********************************************************************************************************************
 **
 ** Private function prototypes
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Private functions definition
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Public functions 
 **
 **********************************************************************************************************************
 **/

void hn_thread_backend_get_data_from_fpga_set_weights(uint32_t *weights, uint32_t num_weights)
{
  pthread_mutex_lock(&channel_recv_lock);
  channel_recv_arb = hn_rr_arb_create(shm_running_transfers_table.number_of_physical_channels, weights, num_weights);
  pthread_mutex_unlock(&channel_recv_lock);
}

//*********************************************************************************************************************
//*********************************************************************************************************************
// values will initially only contain wheights when policy is set to WEIGHTS
//        array must be the same size as number of upstream interfaces
uint32_t hn_thread_backend_get_data_from_fpga_set_arbiter_policy (uint32_t policy, uint32_t *values) 
{

  // assignar la politica en forma de punters a funcio, per a no haver d'estar comprobant la politica cada volta
  //   donat que la politica no canviara constantment, es una comprobació que ens podrem estalviar....

  uint32_t ret_code;

  ret_code = 0;

  if (policy == HN_TBGDFF_ARBITER_POLICY_ROUND_ROBIN) 
  {
    arbiter_policy = HN_TBGDFF_ARBITER_POLICY_ROUND_ROBIN;
    log_info("thread backend from fpga. arbiter policy set to Round_Robin");
  }
  else if (policy == HN_TBGDFF_ARBITER_POLICY_WEIGHTS)
  {
    // check weights 
    //  values must sum 100%, and number of values must match number of upstreamif modules
    // ...number of upstreamifs cannot change while the daemon is running, otherwise the daemon can fail
    //    a function to update/refresh configuration is needed to support HW modifications while the daemon is running
    if (values == NULL)
    {
      log_error("BAD parameters attempting to set read from fpga arbiter policy, NULL pointer for weights vector");
      return 1; // error notifying
    }
    // update policy
    arbiter_policy = HN_TBGDFF_ARBITER_POLICY_WEIGHTS;
    log_info("thread backend from fpga. arbiter policy set to Weights");

  }
  else
  {
    log_error("@hn_thread_backend_get_data_from_fpga_set_arbiter_policy, unknown policy: %u", policy);
    log_error("arbiter policy not updated");
    return 2;
  }

  return ret_code;
}
//*********************************************************************************************************************
//*********************************************************************************************************************
//uint32_t hn_thread_backend_get_data_from_fpga_get_arbiter_policy (uint32_t *policy, uint32_t *values);

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_thread_backend_get_data_from_fpga_set_buffer_count_refresh (struct timespec ts)
{
  //log_debug("upstream_set_buffer_count_refresh, entering function and waiting for mutex");
  pthread_mutex_lock(&upstream_buffer_count_refresh_lock);
  log_debug("upstream_set_buffer_count_refresh, update value to: %2lds %6ldns", ts.tv_sec, ts.tv_nsec);
  upstream_buffer_count_refresh_ts.tv_sec = ts.tv_sec;
  upstream_buffer_count_refresh_ts.tv_nsec = ts.tv_nsec;
  pthread_mutex_unlock(&upstream_buffer_count_refresh_lock);
  //log_debug("upstream_set_buffer_count_refresh, done, mutex released");

  return 0;
}


//*********************************************************************************************************************
//*********************************************************************************************************************
// this function is called upon a thread cancelation
static void clean_backend_get_data_thread_mutexes(void *args) {
  pthread_mutex_unlock(&mutex_write_mmi64);
  pthread_mutex_unlock(&mutex_read_mmi64);
  log_verbose("backend_get_data thread: cleanup routine called for profpga interfaces mutex!");
}

//*********************************************************************************************************************
//*********************************************************************************************************************
// void ?? o cal passar-li parametres??
#define MY_USELESS_BUFFER_SIZE 1<<29
#define UPSTREAM_DMA_TRANSFER_SIZE MAX_BYTES_PER_BURST_TRANSFER

void *hn_thread_backend_get_data_from_fpga_function( void *argv )
{
  uint32_t status;
  uint32_t iface_is_initialized;
  uint8_t *my_useless_buffer = malloc(MY_USELESS_BUFFER_SIZE);
  static int retval = 0;

  pthread_mutex_init(&upstream_buffer_count_refresh_lock, NULL);
  pthread_mutex_init(&channel_recv_lock, NULL);

  // initialization of internal variables
  pthread_mutex_lock(&upstream_buffer_count_refresh_lock);
  upstream_buffer_count_refresh_ts.tv_sec = 0;
  upstream_buffer_count_refresh_ts.tv_nsec = 200;
  pthread_mutex_unlock(&upstream_buffer_count_refresh_lock);

  status = hn_iface_mmi64_is_initialized(&iface_is_initialized);
  if(status)
  {
    log_error("FATAL checking status of mmi64 interface");
    // this will not happen, but in such a case, we should call hn_release....
    retval = -1;
    return &retval;
  }

  if(iface_is_initialized == 0)
  {
    log_error("hn_thread_backend_get_data_from_fpga_function mmi64 interface has not been initialized yet...");
    retval = -2;
    return &retval;
  }
   
  status = hn_get_number_of_upstream_modules (&number_of_upstream_fifos);
  if (status != 0 )
  {
    log_error("@hn_thread_backend_get_data_from_fpga_function: error getting number of upstream modules, err_code %u  num_modules %u\n",
        status, number_of_upstream_fifos);
    retval = -3;
    return &retval;
  }

  channel_recv_arb = hn_rr_arb_create(shm_running_transfers_table.number_of_physical_channels, NULL, 0);
  rr_arb_t channel_assignment_arb = hn_rr_arb_create(shm_running_transfers_table.number_of_physical_channels, NULL, 0);
        

  // main code of the thread.
  // loop, until the daemon requests to terminate the thread
  pthread_cleanup_push(clean_backend_get_data_thread_mutexes, NULL);
  while (!hn_exit_program_is_terminated()) {
    uint32_t something_to_read = 0;
    uint32_t fifo_index;
    uint32_t fifo_has_data;
    uint32_t items_fifo_count = 0;

    // ITEMS FIRST
    uint32_t upstr_mods;
    for (hn_get_number_of_upstream_modules(&upstr_mods), fifo_index = 0; fifo_index < upstr_mods; fifo_index++) {
      hn_mmi64_module_type_t module_type;

      hn_iface_mmi64_get_upstream_module_type(fifo_index, &module_type);

      if (module_type != BURST_TRANSFER_UPSTREAM) {
        items_fifo_count++;
        hn_iface_mmi64_upstream_buffer_is_not_empty(fifo_index, &fifo_has_data);

        if(fifo_has_data != 0)
        {
          something_to_read = 1;

          log_debug("@hn_thread_backend_get_data_from_fpga_function: incoming items detected for channel %2u", fifo_index);
          break;
        }
      }
    }

    // THEN DATA BURST

    if (!something_to_read) {
      while (!rr_arb_round_complete(&channel_recv_arb))
      {
        pthread_mutex_lock(&channel_recv_lock);
        fifo_index = rr_arb_next(&channel_recv_arb) + items_fifo_count;
        pthread_mutex_unlock(&channel_recv_lock);

        hn_iface_mmi64_upstream_buffer_is_not_empty(fifo_index, &fifo_has_data);

        if(fifo_has_data != 0)
        {
          something_to_read = 1;

          log_debug("@hn_thread_backend_get_data_from_fpga_function: incoming data burst detected for channel %2u", fifo_index);
          //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: incoming data burst detected for channel associated to fifo_index %2u\n", fifo_index);
          break;
        }
      }
      pthread_mutex_lock(&channel_recv_lock);
      rr_arb_stop(&channel_recv_arb);
      pthread_mutex_unlock(&channel_recv_lock);
    }

    if(hn_exit_program_is_terminated()) {
      log_notice("hn_tread_backend_get_data_from_fpga_function detected that daemon is stopping...");
      return NULL;
    }

    if (fifo_has_data == 1)
    {
      uint32_t bytes_to_read;
      uint32_t bytes_read;
      bytes_to_read = MY_USELESS_BUFFER_SIZE;
      bytes_read = bytes_to_read;

      log_debug("  will try to read from upstream fifo %2u    %4u bytes",fifo_index, bytes_to_read);
      
      // to avoid printing msgs for regular items 
      //if(fifo_index != 0) {
      //  log_warn("JM10,  will try to read from upstream fifo %2u\n",fifo_index);
      //}
      hn_iface_mmi64_get_data( (char *)my_useless_buffer, &bytes_read,fifo_index);
      if(bytes_read == 0)
      {
        log_error("Error, did not read any value");
        continue;
      }

      log_debug("  got from upstream fifo %2u    %4u bytes", fifo_index, bytes_read);

      //if(fifo_index != 0) {
      //  log_warn("JM10,  got from upstream fifo %2u    %4u bytes\n", fifo_index, bytes_read);
      //}

      hn_mmi64_module_type_t module_type;
      hn_iface_mmi64_get_upstream_module_type(fifo_index, &module_type);

      if (module_type == GENERIC_ITEM_UPSTREAM) {
        log_debug("@hn_thread_backend_get_data_from_fpga_function: got generic items");
        hn_buffer_fifo_write_bytes_from_buf(to_process_item_queue, my_useless_buffer, bytes_read);
      } else if (module_type == DEBUG_ITEM_UPSTREAM) {
        log_debug("@hn_thread_backend_get_data_from_fpga_function: got debug items");
        //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: got debug items\n");
        hn_buffer_fifo_write_bytes_from_buf(debug_item_queue, my_useless_buffer, bytes_read);
      } else if (module_type == BURST_TRANSFER_UPSTREAM) {
        uint32_t found;
        log_debug("@hn_thread_backend_get_data_from_fpga_function: got data");
        //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: got data\n");
        uint32_t table_channel_entry_index;
        uint32_t vn_id;
        uint32_t channel_id;

        hn_iface_mmi64_get_vn_id_for_fifo(FIFO_TYPE_UPSTREAM, fifo_index, &vn_id);
        log_debug("@hn_thread_backend_get_data_from_fpga_function: the data is coming from VN %d", vn_id);
        //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: the data is coming from VN %d\n", vn_id);

        found = 0;
        channel_id = vn_id - 3;

        /*
        uint32_t jm10_index;
        log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: channel_id %u,  MAX entries %u  free entries %u\n",
            channel_id,
            MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL,
            shm_running_transfers_table.channel[channel_id].upstream_free_entries 
            );

        log_warn("JM10, channel (free entries)  ");
        for(jm10_index = 0; jm10_index < shm_running_transfers_table.number_of_physical_channels; jm10_index++) {
          log_warn(" %2u (%2u)   ", jm10_index, shm_running_transfers_table.channel[jm10_index].upstream_free_entries);
        }
        log_warn("\n");
        */

        if(shm_running_transfers_table.channel[channel_id].upstream_free_entries < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL)
        {
          for (table_channel_entry_index = 0; table_channel_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL; ) 
          {
            if((shm_running_transfers_table.channel[channel_id].upstream_entries[table_channel_entry_index].free == 0) 
                && (shm_running_transfers_table.channel[channel_id].upstream_entries[table_channel_entry_index].finished == 0) )
            {
              found = 1;
              log_debug("@hn_thread_backend_get_data_from_fpga_function:  entry for running transfer detected for shm_running_transfers_table.channel[%2u].upstream_entries[%2u]", channel_id, table_channel_entry_index);
              //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function:  entry for running transfer detected for shm_running_transfers_table.channel[%2u].upstream_entries[%2u]\n", channel_id, table_channel_entry_index);

              break;
            }
            table_channel_entry_index++;
          }
        }

        if (!found) {
          log_error("Got upstream data but no transfers registered, throwing away data!");
        } else {
          hn_shm_running_transfers_table_entry_t *table_entry = &(shm_running_transfers_table.channel[channel_id].upstream_entries[table_channel_entry_index]);
          hn_shm_buffer_transfer_t *tx = &(shm_running_transfers_table.channel[channel_id].upstream_entries[table_channel_entry_index].config);

          char *dst = (char *) tx->shm_addr;

          log_debug("@hn_thread_backend_get_data_from_fpga_function: found a transfer associated to this VN, details: %016X %016X", table_entry, tx);
          log_debug("@hn_thread_backend_get_data_from_fpga_function: dma_id = %d channel_id = %d size= %u counter = %u", table_entry->dma_id, channel_id, tx->size, table_entry->counter);

          //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: found a transfer associated to this VN, details: %016X %016X", table_entry, tx);
          //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: dma_id = %d channel_id = %d size= %u counter = %u", table_entry->dma_id, channel_id, tx->size, table_entry->counter);

          uint32_t programmed_size = UPSTREAM_DMA_TRANSFER_SIZE;
          if ((tx->size - table_entry->counter) < UPSTREAM_DMA_TRANSFER_SIZE) {
            programmed_size = tx->size - table_entry->counter;
          }

          if (programmed_size - table_entry->chunk_counter < bytes_read) {
            log_error("FATAL detected that transfer address exceeded the shared memory buffer!");
            log_error("NOT all data writen in shm buffer, just the necessary to fill the buffer and notify finalization of the transfer to the app, with the error code");
            log_error("    transfer remaining bytes: %u  read: %u \n", programmed_size - table_entry->chunk_counter, bytes_read);
            log_error("unhandled data may continue coming");
            retval = 1;
            programmed_size = tx->size - table_entry->chunk_counter;
            memcpy(dst, my_useless_buffer, programmed_size);
          } else {
            log_debug("@hn_thread_backend_get_data_from_fpga_function, Upstream chunk received\n");

            memcpy(dst, my_useless_buffer, bytes_read);
            tx->shm_addr += bytes_read;
            table_entry->chunk_counter += bytes_read;
          }

          if (table_entry->chunk_counter == programmed_size) {
            log_debug("Upstream chunk transfer completed!");
            //log_warn("JM10, Upstream chunk transfer completed!\n");

            shm_running_transfers_table.channel[channel_id].locked_for_ack_transfers = 0;
            table_entry->chunk_counter = 0;
            table_entry->finished = 1;
            tx->addr += programmed_size;
            table_entry->counter += programmed_size;

            if (tx->size == table_entry->counter) {
              struct timespec end_time;

              clock_gettime(CLOCK_MONOTONIC, &end_time);
              
              //float elapsed_in_sec = (float) get_elapsed_time_seconds(table_entry->start_time, end_time);
              log_debug("Upstream transfer completed, BW = %6.3f KB/s", ((float)(tx->size) / 1024.) / ((float) get_elapsed_time_seconds(table_entry->start_time, end_time)));
              //log_warn("JM10, Upstream transfer completed, BW = %6.3f KB/s\n", ((float)(tx->size) / 1024.) / elapsed_in_sec);

              shm_running_transfers_table.channel[channel_id].upstream_free_entries++;
              table_entry->free = 1;
              hn_rscmgt_release_dma(table_entry->dma_id);

              if(tx->blocking == 1)
              {
                log_warn("JM10, Upstream transfer was blocking, sending notification backwards to client %llu\n", tx->client_id);
                hn_command_shm_buffer_manage_status_t shm_operation_status;

                shm_operation_status.info.command = tx->type;
                shm_operation_status.status       = HN_SHM_OPERATION_OK;
                shm_operation_status.client_id    = tx->client_id;

                hn_buffer_fifo_write_bytes_from_buf(shm_notification_fifo, &shm_operation_status, sizeof(hn_command_shm_buffer_manage_status_t));
              }
            }
          }
        }
      }
    }

    while (hn_buffer_fifo_get_num_elements(upstream_bursttransfer_command_fifo) > 0) {
      hn_shm_buffer_transfer_t tx;

      uint32_t upstream_free_entries = 0;
      uint32_t table_channel_index;
      uint32_t table_channel_entry_index;

      //log_warn("JM10, detected upstream dma transfer command"); // this means that there is a new upstream transfer command to be processed

      while (upstream_free_entries == 0 && !rr_arb_round_complete(&channel_assignment_arb)) {
        table_channel_index = rr_arb_next(&channel_assignment_arb);

        pthread_mutex_lock( &(shm_running_transfers_table.mutex) );
        upstream_free_entries   = shm_running_transfers_table.channel[table_channel_index].upstream_free_entries;
        // JM10, de moment nomes permet allotjar una rafega amb ack.....per a simplificar el codi...pero l'he de canviar fins que no modifique el HDL per tal de que no envien irq
        // per tal de no mexclar rafegyues que requerixquen irq amb rafegues que no...ja que no se d'on venen les irqs
        pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );
      }

      rr_arb_stop(&channel_assignment_arb);

      if(upstream_free_entries)
      {
        hn_buffer_fifo_read_elements_to_buf(upstream_bursttransfer_command_fifo, &tx, 1);

        table_channel_entry_index = 0;
        pthread_mutex_lock( &(shm_running_transfers_table.mutex) );
        
        log_debug("@hn_thread_backend_get_data_from_fpga_function:    processing cmd type %u    dst tile %u    dst addr %08X   size %u bytes (%u words -- %u frames)    local shm_buff_addr 0x%016x    blocking %u",
            tx.type, tx.tile, tx.addr, tx.size, (tx.size)/sizeof(uint64_t),(tx.size / 8) / MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN + 1, tx.shm_addr, tx.blocking );

        // esto significa que hi ha un canal lliure per poder processar la peticio de transferencia
        //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function:    processing cmd type %u    dst tile %u    dst addr %08X   size %u bytes (%lu words -- %u frames)    local shm_buff_addr %p    blocking %u",
        //    tx.type, tx.tile, tx.addr, tx.size, (tx.size)/sizeof(uint64_t),(tx.size / 8) / MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN + 1, tx.shm_addr, tx.blocking );

        
        while (!shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].free && (table_channel_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL))
        {
          log_debug("@hn_thread_backend_get_data_from_fpga_function: searching free entry for new cmd, increasing table_channel_entry_index");
          table_channel_entry_index++;
        }

        if(table_channel_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL)
        {
          uint32_t roundup;

          log_debug("@hn_thread_backend_get_data_from_fpga_function: setting burst transfer cmd configuration to shm_running_transfers_table.channel[%2u].upstream_entries[%2u]", table_channel_index, table_channel_entry_index);
          // esto significa que hi ha una entrada lliure al canal i que vai a poder processar la peticio de transferencia
          //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function:    setting burst transfer cmd configuration to shm_running_transfers_table.channel[%2u].upstream_entries[%2u]", table_channel_index, table_channel_entry_index);

          // we found a free entry, process shm command and store into 
          //shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].dma_id                     = dma_id;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.burst_channel_index = table_channel_entry_index; // cmd[cmd_index]->burst_channel_index;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.client_id           = tx.client_id;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.channel             = tx.channel;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.type                = tx.type;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.shm_addr            = tx.shm_addr;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.tile                = tx.tile;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.addr                = tx.addr;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.size                = tx.size;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.blocking            = tx.blocking;

          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].free             = 0;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].finished         = 1; // mark as pending for DMA transfer setup
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].counter          = 0;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].chunk_counter          = 0;

          log_debug("@hn_thread_backend_get_data_from_fpga_function: registering transfer start time");
          clock_gettime(CLOCK_MONOTONIC, &shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].start_time);

          roundup = ((tx.size % MAX_BYTES_PER_BURST_TRANSFER) == 0) ? 0 : 1;
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].num_pending_acks = tx.size / MAX_BYTES_PER_BURST_TRANSFER + roundup ; // ((num_bytes / bytes_per_word) / words per frame)
          shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].waiting_for_ack  = 1; // used to indicate whether a transfer is finished and waiting for an ack

          // reduce number of free entries for this channel
          shm_running_transfers_table.channel[table_channel_index].upstream_free_entries--;
          
          log_debug("@hn_thread_backend_get_data_from_fpga_function:    stored cmd config type %u    dst tile %u    dst addr %08X   size %u bytes   local shm_buff_addr 0x%016x    blocking %u",
              shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.type,
              shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.tile,
              shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.addr,
              shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.size,
              shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.shm_addr,
              shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.blocking
              );
          //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function:    channel %2u  now has %2u free entries", table_channel_index, shm_running_transfers_table.channel[table_channel_index].upstream_free_entries);
          //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function:    stored cmd config type %u    dst tile %u    dst addr %08X   size %u bytes   local shm_buff_addr %p    blocking %u",
          //    shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.type,
          //    shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.tile,
          //    shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.addr,
          //    shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.size,
          //    shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.shm_addr,
          //    shm_running_transfers_table.channel[table_channel_index].upstream_entries[table_channel_entry_index].config.blocking
          //    );

        }
        else
        {
          log_error("FATAL, detected bad handling of free entries in shm burst transfers table.");
        }
      }

      pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );
    }

    // run missing DMA transfers
    uint32_t channel_index;
    uint32_t channel_entry_index;
    for (channel_index = 0; channel_index < shm_running_transfers_table.number_of_physical_channels; channel_index++) {
      for (channel_entry_index = 0; channel_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL; channel_entry_index++) {

        if (shm_running_transfers_table.channel[channel_index].locked_for_ack_transfers == 0) {
          if (shm_running_transfers_table.channel[channel_index].upstream_entries[channel_entry_index].free == 0 && shm_running_transfers_table.channel[channel_index].upstream_entries[channel_entry_index].finished == 1) {
            uint32_t dma_id;
            uint32_t chunk_size;
            hn_shm_buffer_transfer_t               *tx = &shm_running_transfers_table.channel[channel_index].upstream_entries[channel_entry_index].config;
            hn_shm_running_transfers_table_entry_t *table_entry = &(shm_running_transfers_table.channel[channel_index].upstream_entries[channel_entry_index]);
            uint32_t pending_transfer_bytes;

            pending_transfer_bytes = tx->size - table_entry->counter;
            chunk_size = UPSTREAM_DMA_TRANSFER_SIZE;
            if ( pending_transfer_bytes < UPSTREAM_DMA_TRANSFER_SIZE) {
              chunk_size = pending_transfer_bytes;
            }

            log_debug("@hn_thread_backend_get_data_from_fpga_function: found pending transfer on channel %u slot %u", channel_index, channel_entry_index);
            log_debug("@hn_thread_backend_get_data_from_fpga_function: registering DMA transfer");

            //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: found pending transfer on channel %u slot %u\n", channel_index, channel_entry_index);


            hn_rscmgt_register_dma (&dma_id, tx->client_id);
            shm_running_transfers_table.channel[channel_index].upstream_entries[channel_entry_index].dma_id = dma_id;

            hn_rscmgt_write_dma_operation(dma_id, channel_index, tx->tile, tx->addr,
                                                             chunk_size, /*tile_dst*/ 0,
                                                             /*addr_dst*/ 0, /*to_unit*/ 0,
                                                             /*to_mem*/ 0, /*to_ext*/ 1, /*notify*/ 0);

            log_debug("@hn_thread_backend_get_data_from_fpga_function: launching DMA transfer");
            
            //log_warn("JM10, @hn_thread_backend_get_data_from_fpga_function: launching DMA transfer, tile %2u  channel_index %2u  addr 0x%08x  chunk_size %u", tx->tile, channel_index, tx->addr, chunk_size);

            hn_shared_memory_launch_upstream_transfer(tx->tile, channel_index, tx->addr, chunk_size, /*tile_dst*/ 0, /*addr_dst*/ 0);

            shm_running_transfers_table.channel[channel_index].upstream_entries[channel_entry_index].finished = 0;

            shm_running_transfers_table.channel[channel_index].locked_for_ack_transfers = 1;

            break;
          }
        }

      }
    }

    if (something_to_read == 0) {
      nanosleep(&upstream_buffer_count_refresh_ts, NULL);
    }
  } // end of main loop of the thread
  pthread_cleanup_pop(0);

  log_notice("hn_tread_backend_get_data_from_fpga_function detected that daemon is stopping...");

  free(my_useless_buffer);

  return &retval;
}

//********************************************************************************************************************* 
// end of file hn_thread_backend_get_data_from_fpga.c
//*********************************************************************************************************************

