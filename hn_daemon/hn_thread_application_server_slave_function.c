///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 26, 2018
// File Name: hn_thread_application_server_slave_function.c
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Application server slave thread implementation.
//
//    This thread communicate and work with the application clients. 
//    It is an end point between an application and the daemon
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <sys/select.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "hn_logging.h"
#include "hn_exit_program.h"
#include "hn_buffer_fifo.h"
#include "hn_shared_structures.h"
#include "hn_request_handler.h"
#include "hn_item_handler.h"
#include "hn_thread_application_server_slave_function.h"

// maximum number of request that can be processed at once
#define HN_MAX_REQUESTS 10

// maximum buffer size managed by this thread
#define HN_MAX_BUFFER_SIZE_FROM_APP  32768
#define HN_MAX_BUFFER_SIZE_TO_APP    1<<26

  
// application server slave thread entry point
void *hn_thread_application_server_slave_function(void *args)
{
  hn_thread_application_server_slave_args_t *targs = (hn_thread_application_server_slave_args_t *)args;
  int sock = targs->sock;
  int input_fifo = targs->client_info.input_fifo;
  int output_fifo = targs->client_info.output_fifo;

  // thread return variable
  static int retval = 0;
  
  // Target buffer used for writing into the socket
  unsigned char *buf_socket_write = (unsigned char *)malloc(HN_MAX_BUFFER_SIZE_TO_APP);
  if (buf_socket_write == NULL) {
    log_error("application_server_slave_thread: client id=%d could not allocate enough buffer space", targs->client_info.cid);
    retval = -2;
    return &retval;
  }

  // Target buffer for reading the socket
  unsigned char buf[HN_MAX_BUFFER_SIZE_FROM_APP];

  // Request vector
  hn_request_t req[HN_MAX_REQUESTS];

 
  unsigned int sock_read_buf_offset = 0; // where to put the data in the buffer when reading from the socket

  // size of the elements in the input_fifo (normally items, so 4 bytes; but...)
  //size_t input_fifo_element_size = hn_buffer_fifo_get_element_size(input_fifo);

  // for select+read&write,
  fd_set rfds;
  fd_set wfds;
  struct timeval tv;
  int select_rs;

  // number of pending bytes to process by the handler
  size_t bytes_pending = 0;

  pthread_mutex_lock(&targs->client_info.health_mtx);
  while ((!hn_exit_program_is_terminated()) && (targs->client_info.health_status != HN_CLIENT_HEALTH_STATUS_LIVE))
  {
    pthread_cond_wait(&targs->client_info.health_cond, &targs->client_info.health_mtx);
  }
  pthread_mutex_unlock(&targs->client_info.health_mtx);

  log_info("application_server_slave_thread ready for app_id=%llu", targs->client_info.cid);

  
  unsigned long long total_bytes_to_app = 0;
  while (!hn_exit_program_is_terminated())
  {
    FD_ZERO(&rfds);
    FD_ZERO(&wfds);
    FD_SET(sock, &rfds);
    FD_SET(sock, &wfds);
    /* wait upto 5 s */
    tv.tv_sec = 5;
    tv.tv_usec = 0;

    select_rs = select(sock+1, &rfds, &wfds, NULL, &tv);
    if (select_rs > 0)
    {
      int ready_read  = FD_ISSET(sock, &rfds);
      int ready_write = FD_ISSET(sock, &wfds);

      if (ready_read)
      { // the socket has data to read from
        //log_debug("slave thread: socket available for reading");

        /*
         * let's try to get the maximum number of bytes that fit in buf
         * Notice, that could be some bytes that has not been processed in the previous iteration,
         * just because of they do not compose a complete command request
         */
        size_t  max_bytes_to_read = HN_MAX_BUFFER_SIZE_FROM_APP - sock_read_buf_offset;
        if (max_bytes_to_read > 0)
        {
          ssize_t bytes_read = read(sock, buf + sock_read_buf_offset, max_bytes_to_read);
          sock_read_buf_offset = 0;  // reset for the next iteration

          if (bytes_read == 0)
          {
            log_debug("application_server_slave_thread: reading on socket: the other end has gone away. Exiting...");
            break;
          }
          else if (bytes_read < 0)
          { // connection closed on the other end
            log_debug("application_server_slave_thread: reading on socket, but %s. Exiting...", strerror(errno));
            break;
          } 

          // The next converts the data read from the socket into requests (green rectangle in the figure)
          int num_req = 0;
          size_t bytes_processed = 0;
          bytes_pending += bytes_read;
          do
          {
            log_debug("application_server_slave_thread: bytes_read_from_socket=%zd bytes_pending_for_processing=%zu", bytes_read, bytes_pending);

            num_req = hn_request_handle(req, HN_MAX_REQUESTS, buf, bytes_pending, &bytes_processed);
            if (num_req > 0)
            { // some request detected
              hn_buffer_fifo_write_bytes_from_buf(output_fifo, &req, num_req*sizeof(hn_request_t));
              bytes_pending -= bytes_processed;
              memmove(buf, buf + bytes_processed, bytes_pending);
              bytes_processed = 0;
            }
            else if (bytes_processed == bytes_pending)
            { // no request detected, but the buffer has been flushed
              // may the data be corrupted? It seems that
              bytes_pending        = 0;
              sock_read_buf_offset = 0;
            }
            else
            { // no request detected.
              // Perhaps could there be less than a complete command request ?
              log_debug("application_server_slave_thread: bytes_pending=%zu", bytes_pending); 
              sock_read_buf_offset = bytes_pending;
              bytes_processed = 0;
            }
          } while ((num_req > 0) && (bytes_pending > 0));
        }
        else
        {
          log_error("application_server_slave_thread: buf=full and data could not be converted into request. Throwing away!");
          sock_read_buf_offset = 0;
        }
      }

      if ((ready_write) && (hn_buffer_fifo_get_num_bytes(input_fifo) > 0))
      { // the socket is available for writing in it
        // FIXME the select awake up continously because the socket is almost of the time ready for writing in it
        //log_verbose("slave thread: socket available for writing");

        
        size_t bytes_to_write = hn_buffer_fifo_read_bytes_to_buf(input_fifo, buf_socket_write, HN_MAX_BUFFER_SIZE_TO_APP);


        // let's write to the socket
        ssize_t bytes_written = write(sock, buf_socket_write, bytes_to_write);
        total_bytes_to_app += bytes_written;
        if ((bytes_written < 0) && (errno == EPIPE))
        {  // connection closed on the other end
          log_debug("application_server_slave_thread: writing on socket, but connection closed on the other end. Exiting...");
          break;
        }

        if (bytes_written < bytes_to_write) {
          log_error("application_server_slave_thread: bytes_to_write = %zd, bytes_written = %zu to the client %llu", bytes_to_write, bytes_written, targs->client_info.cid);
        }

        //log_info("application_server_slave_thread: written_this_time=%llu   total_bytes=%llu   sent to app_id=%llu", bytes_written, total_bytes_to_app, targs->client_info.cid);
      }
    }
  }

  free(buf_socket_write);
  close(sock);
  log_info("application_server_slave_thread: exiting for client id %llu...", targs->client_info.cid);

  return &retval;
}

