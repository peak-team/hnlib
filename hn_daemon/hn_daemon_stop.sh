#!/bin/bash

#abort()
#{
#    echo >&2 '
#error, 
#  possible causes 
#    hn_background_daemon not running 
#    not enough permision to stop the process, only sudo can do it
#      '
#    ps -ef | grep -w hn_background_daemon
#    echo "An error occurred. Exiting..." >&2
#    exit 1
#}
#
#trap 'abort' 0
#
#set -e

check_process() {
  echo "$ts: checking $1"
  [ "$1" = "" ]  && return 0
  [ `pgrep -n $1` ] && return 1 || return 0
}


# timestamp
ts=`date +%T`
echo "$ts: checking if hn_daemon is running ..."
check_process "hn_daemon" 
if [ $? -eq 0 ]
then 
{
  echo "$ts: not running"
}
else
{
  echo "$ts: if running, only sudo will succeed stopping it "
  ps -ef | grep -w "hn_daemon" | grep -v grep | awk '{print $2}'| xargs kill -15
  ps -ef | grep -w "hn_daemon"

#  check_process "hn_background_daemon"
#  if [ $? -eq 0 ]
#  then 
#  {
#    echo "$ts: successfully terminated"
#  }
#  else
#  {
#    echo "$ts: something failed, process is still running"
#    echo "     not enough permision to stop the process? only sudo can do it"
#    echo "     more than one instance? unexpected, please check it "
#  }; fi
};fi

#trap : 0

echo >&2 '
quit now

'
