#ifndef __HN_MANGO_ITEM_COMMANDS_H__
#define __HN_MANGO_ITEM_COMMANDS_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: February 28, 2018
// File Name: hn_mango_item_commands.h
// Design Name:
// Module Name: HN Daemon
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    Here, we define the item commands for MANGO
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*! 
 * \name Upstream commands
 *
 * When adding a new upstream command remember to add
 * an entry in the enum \see{#hn_item_command_type_t} 
 * of the hn_item_handler.h and create the item handler
 * function in hn_item_handler.c 
 * (follow same pattern as for the others)
 */
//@{
//
 

//! Console command type
#define COMMAND_CONSOLE                         0

// TODO let's comment those defines as the above and prefix them with HN_MANGO

#define COMMAND_TILE_STATS_FIRST_ITEM           1
#define COMMAND_TILE_STATS_REMAINING_ITEMS      2
#define COMMAND_TILEREG_FIRST_ITEM              3
#define COMMAND_TILEREG_REMAINING_ITEMS         4
#define COMMAND_ECHO                            5
#define COMMAND_DEBUG_MC_FIRST_ITEM             10
#define COMMAND_DEBUG_MC_REMAINING_ITEM         11
#define COMMAND_UNIT_DATA_FIRST                 16
#define COMMAND_UNIT_DATA_REMAINING             17
#define COMMAND_DEBUG_U2M_FIRST_ITEM            18
#define COMMAND_DEBUG_U2M_REMAINING_ITEMS       19
#define COMMAND_DEBUG_M2U_FIRST_ITEM            20
#define COMMAND_DEBUG_M2U_REMAINING_ITEMS       21
#define COMMAND_DEBUG_INJECT_FIRST_ITEM         12
#define COMMAND_DEBUG_INJECT_REMAINING_ITEMS    13
#define COMMAND_DEBUG_EJECT_FIRST_ITEM          14
#define COMMAND_DEBUG_EJECT_REMAINING_ITEMS     15
#define COMMAND_MC_DATA_FIRST_ITEM              22
#define COMMAND_MC_DATA_REMAINING_ITEMS         23
#define COMMAND_INTERRUPTS_FIRST_ITEM           24
#define COMMAND_INTERRUPTS_REMAINING_ITEMS      25


//
//@}

//! \name Downstream commands
//@{
//

//! Configure debug and stats command type 
#define HN_MANGO_ITEM_COMMAND_CONFIGURE_DEBUG_AND_STATS    33

// TODO let's comment the next defines as the above

#define COMMAND_CONFIGURE_TILE                  34
#define COMMAND_READ_TILEREG                    35
#define COMMAND_WRITE_TILEREG                   36
#define COMMAND_READ_MEMORY_BLOCK               40
#define COMMAND_WRITE_MEMORY_BLOCK              41
#define COMMAND_PROTOCOL_WRITE                  42
#define COMMAND_RESET_SYSTEM                    43
#define HN_MANGO_ITEM_COMMAND_CLOCK             44
#define COMMAND_READ_MEMORY_WORD                45
#define COMMAND_WRITE_MEMORY_WORD               46
#define COMMAND_WRITE_MEMORY_HALF               47
#define COMMAND_WRITE_MEMORY_BYTE               48


// functions for command CLOCK
#define HN_MANGO_ITEM_COMMAND_FUNCTION_PAUSE_CLOCK         0x01
#define HN_MANGO_ITEM_COMMAND_FUNCTION_RESUME_CLOCK        0x02
#define HN_MANGO_ITEM_COMMAND_FUNCTION_RUNFOR_CLOCK        0x80

// functions for DEBUG_AND_STATS
#define HN_MANGO_ITEM_COMMAND_FUNCTION_STATS_DISABLE                1
#define HN_MANGO_ITEM_COMMAND_FUNCTION_STATS_ENABLE                 2         
#define HN_MANGO_ITEM_COMMAND_FUNCTION_ECHO_DISABLE                 3
#define HN_MANGO_ITEM_COMMAND_FUNCTION_ECHO_ENABLE                  4
#define HN_MANGO_ITEM_COMMAND_FUNCTION_MC_DEBUG_DISABLE             9
#define HN_MANGO_ITEM_COMMAND_FUNCTION_MC_DEBUG_ENABLE              10
#define HN_MANGO_ITEM_COMMAND_FUNCTION_U2M_DEBUG_ENABLE             32
#define HN_MANGO_ITEM_COMMAND_FUNCTION_U2M_DEBUG_DISABLE            31
#define HN_MANGO_ITEM_COMMAND_FUNCTION_M2U_DEBUG_ENABLE             34
#define HN_MANGO_ITEM_COMMAND_FUNCTION_M2U_DEBUG_DISABLE            33
#define HN_MANGO_ITEM_COMMAND_FUNCTION_INJECT_DEBUG_ENABLE          14
#define HN_MANGO_ITEM_COMMAND_FUNCTION_INJECT_DEBUG_DISABLE         13
#define HN_MANGO_ITEM_COMMAND_FUNCTION_EJECT_DEBUG_ENABLE           18
#define HN_MANGO_ITEM_COMMAND_FUNCTION_EJECT_DEBUG_DISABLE          17
#define HN_MANGO_ITEM_COMMAND_FUNCTION_TILEREG_DEBUG_ENABLE         26
#define HN_MANGO_ITEM_COMMAND_FUNCTION_TILEREG_DEBUG_DISABLE        25


#define HN_MANGO_TILEREG_TO_UNIT                 5            // TO FIX (where to put)
#define HN_MANGO_TILEREG_UNIT_INTERRUPTS_REG     13           // TO FIX (where to put)
#define HN_MANGO_TILEREG_DMA_REG                 12           // TO FIX (where to put)

#define HN_MANGO_UNIT_ID                         5            // TO FIX (where to put)
#define HN_MANGO_MEM_ID                          2            // TO FIX (where to put)
#define HN_MANGO_EXT_ID                          0            // TO FIX (where to put)
// TO FIX: EXT_ID is using the same value as L1_cache: In principle there should be
// no conflict as L1_cache is used inside an accelerator and EXT is used at MANGO level
//@}

#endif
