#include <stdio.h>
#include <stdlib.h>

#include "hn_logging.h"
#include "hn_resource_manager.h"

hn_rscmgt_info_t *rscmgt_info;

void fn_show_tiles_with_memories() {
  int i;
  for (i=0;i<rscmgt_info->num_tiles;i++) {
    if (rscmgt_info->tile_info[i].memory_size != 0) {
      printf("Tile %d: type %d, number of cores %d, memory size %llu, read memory bw %llu (avail %llu) write memory bw %llu (avail %llu).", 
                                                                           i, rscmgt_info->tile_info[i].type,
                                                                           rscmgt_info->tile_info[i].num_cores,
                                                                           rscmgt_info->tile_info[i].free_memory,
                                                                           rscmgt_info->tile_info[i].read_memory_bw,
                                                                           rscmgt_info->tile_info[i].avail_read_memory_bw,
                                                                           rscmgt_info->tile_info[i].write_memory_bw,
                                                                           rscmgt_info->tile_info[i].avail_write_memory_bw);
      hn_rscmgt_memory_slot_t *ms = rscmgt_info->tile_info[i].first_memory_slot;
      printf(" ptr=%p ", ms);
      while (ms != NULL) {
        printf(" %llu [%08x, %08x]", ms->size, ms->start_address, ms->end_address);
        ms = ms->next_memory_slot;
      } 
      printf("\n");
    }
  }
}

int main(int argc, char **argv)
{
  int                i;
  int                entry;
  uint32_t           request_type[50000];
  unsigned long long request_bw[50000];
  uint32_t           request_num_tiles[50000];
  uint32_t           src_tile[50000];
  uint32_t           dst_tile[50000];
  uint32_t           mem_tile[50000];
  uint32_t           tiles_dst_ptr[50000][10];
  uint32_t           types_dst_ptr[50000][10];
  unsigned long long size[50000];
  unsigned int       tile_mem[50000];
  unsigned int       starting_address[50000];
  unsigned int       mapped[50000];
  unsigned int       num_iterations;
  unsigned int       num_mapped_segments;
  unsigned int       tile;
  unsigned int       addr;
  unsigned long long bw;
  uint32_t           types[64];
 
  uint32_t x;
  uint32_t num_mappings = 0;
   
  // initialization
  hn_log_initialize(HN_LOG_TO_FILE, "sal");
  hn_rscmgt_initialize();

  rscmgt_info = hn_rscmgt_get_info();

  printf(" test info ptr: %p\n", rscmgt_info);

  // we list resources with memories
  printf("Architecture id: %d\n", rscmgt_info->arch_id);
  printf("Number of tiles: %d (%d x %d)\n", rscmgt_info->num_tiles, rscmgt_info->num_rows, rscmgt_info->num_cols);
  fn_show_tiles_with_memories();

  // Test 1: We request a memory from each tile with memory segment of size 1MB
  printf("Test1: requesting (finding, not assigning it) a memory multiple times, one from each tile\n");
  for (i=0;i<rscmgt_info->num_tiles;i++) {
    if (hn_rscmgt_find_memory(i, 1024 * 1024, &tile, &addr)) {
      printf("memory found at tile %d, starting address %08x\n", tile, addr);
    } else {
      printf("not found from tile %d\n", i);
    }
  }

  
  // Test 2: We ask randomly for memory segment maps and unmaps
  printf("Test1: random mapping/unmapping memory segments---------------------------------------------\n");
  for (i=0;i<50000;i++) mapped[i] = 0;
  num_mapped_segments = 0;
  num_iterations = 10;
  printf("playing with random mapping and unmapping during %d iterations...\n", num_iterations);
  for (i=0;i<num_iterations;i++) {
    entry = random() % 50000;
    if (mapped[entry]) {
      hn_rscmgt_release_memory(tile_mem[entry], starting_address[entry], size[entry]);
      mapped[entry] = 0;
      num_mapped_segments--;
    } else {
      int x;
      x = random() % 4;
      size[entry] = (x==0)? 1024 * 64: (x==1)? 1024 * 1024 * 2: (x==2)? 1024 * 128: 1024 * 512;
      mapped[entry] = hn_rscmgt_find_memory(36, size[entry], &tile_mem[entry], &starting_address[entry]);
      if (mapped[entry]) {
        num_mapped_segments++;
        hn_rscmgt_allocate_memory(tile_mem[entry], starting_address[entry], size[entry]);
      }
    } 
  }
  printf("finished\n\n");

  printf("mapped segments: %d\n", num_mapped_segments);
  fn_show_tiles_with_memories();

  printf("unmapping rest of segments...\n");
  while (num_mapped_segments) {
    i = random() % 50000;
    if (mapped[i]) { 
      hn_rscmgt_release_memory(tile_mem[i], starting_address[i], size[i]);
      //fn_show_tiles_with_memories();
      mapped[i] = 0;
      num_mapped_segments--;
    }
  }
  printf("finished\n\n");

  printf("mapped segments: %d\n", num_mapped_segments);
  fn_show_tiles_with_memories();
  printf("End of test------------------------------------------------------------------------\n");
  
  printf("\n\nTest3: randomly reserving and releasing cluster, memory and network bandwidth\n");
  num_iterations = 10;
  num_mappings = 0;
  for (i=0;i<50000;i++) mapped[i] = 0;

  printf("performing %d iterations...\n", num_iterations);
  for (i=0;i<num_iterations;i++) {
    entry = random() % 50000;
    if (mapped[entry]) {
      switch (request_type[entry]) {
        case 0: hn_rscmgt_release_read_cluster_bandwidth(request_bw[entry]); break;
        case 1: hn_rscmgt_release_write_cluster_bandwidth(request_bw[entry]); break;
        case 2: hn_rscmgt_release_read_memory_bandwidth(mem_tile[entry], request_bw[entry]); break;
        case 3: hn_rscmgt_release_write_memory_bandwidth(mem_tile[entry], request_bw[entry]); break;
        case 4: hn_rscmgt_release_network_bandwidth(src_tile[entry], dst_tile[entry], request_bw[entry]); break;
      }
      num_mappings--;
      mapped[entry] = 0;
    } else {
      x = random() % 5;
      switch (x) {
        case 0:  bw = hn_rscmgt_get_available_read_cluster_bandwidth();
                 if (bw > 0) {
                   num_mappings++;
                   mapped[entry] = 1;
                   request_bw[entry] = bw / 100;
                   request_type[entry] = 0;
                   hn_rscmgt_reserve_read_cluster_bandwidth(request_bw[entry]);
                 }
                 break;
        case 1:  bw = hn_rscmgt_get_available_write_cluster_bandwidth();
                 if (bw > 0) {
                   num_mappings++;
                   mapped[entry] = 1;
                   request_bw[entry] = bw / 100;
                   request_type[entry] = 1;
                   hn_rscmgt_reserve_write_cluster_bandwidth(request_bw[entry]);
                 }
                 break;
        case 2:  x = random() % 4;
                 if (x==0) mem_tile[entry] = 0; else if (x==1) mem_tile[entry] = 7; else if (x==2) mem_tile[entry] = 56; else mem_tile[entry] = 63;
                 bw = hn_rscmgt_get_available_read_memory_bandwidth(mem_tile[entry]);
                 if (bw > 0) {
                   num_mappings++;
                   mapped[entry] = 1;
                   request_bw[entry] = bw / 100;
                   request_type[entry] = 2;
                   hn_rscmgt_reserve_read_memory_bandwidth(mem_tile[entry], request_bw[entry]);
                 }
                 break;
        case 3:  x = random() % 4;
                 if (x==0) mem_tile[entry] = 0; else if (x==1) mem_tile[entry] = 7; else if (x==2) mem_tile[entry] = 56; else mem_tile[entry] = 63;
                 bw = hn_rscmgt_get_available_write_memory_bandwidth(mem_tile[entry]);
                 if (bw > 0) {
                   num_mappings++;
                   mapped[entry] = 1;
                   request_bw[entry] = bw / 100;
                   request_type[entry] = 3;
                   hn_rscmgt_reserve_write_memory_bandwidth(mem_tile[entry], request_bw[entry]);
                 }
                 break;
        case 4:  src_tile[entry] = random() % rscmgt_info->num_tiles;
                 dst_tile[entry] = random() % rscmgt_info->num_tiles;
                 bw = hn_rscmgt_get_available_network_bandwidth(src_tile[entry], dst_tile[entry]);
                 if (bw > 0) {
                   num_mappings++;
                   mapped[entry] = 1;
                   request_bw[entry] = bw / 100;
                   request_type[entry] = 4;
                   hn_rscmgt_reserve_network_bandwidth(src_tile[entry], dst_tile[entry], request_bw[entry]);
                 }
                 break;
      }
    }
  }

  printf("done\n");
  printf("current number of mappings active: %d\n", num_mappings);
  fn_show_tiles_with_memories();
  printf("releasing mappings...\n");
  for (i=0;i<50000;i++) {
    if (mapped[i]) {
      switch (request_type[i]) {
        case 0: hn_rscmgt_release_read_cluster_bandwidth(request_bw[i]); break;
        case 1: hn_rscmgt_release_write_cluster_bandwidth(request_bw[i]); break;
        case 2: hn_rscmgt_release_read_memory_bandwidth(mem_tile[i], request_bw[i]); break;
        case 3: hn_rscmgt_release_write_memory_bandwidth(mem_tile[i], request_bw[i]); break;
        case 4: hn_rscmgt_release_network_bandwidth(src_tile[i], dst_tile[i], request_bw[i]); break;
      }
    }
  }
  printf("done\n");
  fn_show_tiles_with_memories();


  printf("checking all physical bandwidth is available...\n");
  if (rscmgt_info->read_cluster_bw != rscmgt_info->avail_read_cluster_bw) printf("Error, not all read cluster bandwidth available\n");
  if (rscmgt_info->write_cluster_bw != rscmgt_info->avail_write_cluster_bw) printf("Error, not all write cluster bandwidth available\n");
  x = 0;
  if (rscmgt_info->tile_info[x].read_memory_bw != rscmgt_info->tile_info[x].avail_read_memory_bw) printf("Error, not all read memory bandwidth available in tile %d\n", x);
  if (rscmgt_info->tile_info[x].write_memory_bw != rscmgt_info->tile_info[x].avail_write_memory_bw) printf("Error, not all write memory bandwidth available in tile %d\n", x);
  x = 7;
  if (rscmgt_info->tile_info[x].read_memory_bw != rscmgt_info->tile_info[x].avail_read_memory_bw) printf("Error, not all read memory bandwidth available in tile %d\n", x);
  if (rscmgt_info->tile_info[x].write_memory_bw != rscmgt_info->tile_info[x].avail_write_memory_bw) printf("Error, not all write memory bandwidth available in tile %d\n", x);
  x = 56;
  if (rscmgt_info->tile_info[x].read_memory_bw != rscmgt_info->tile_info[x].avail_read_memory_bw) printf("Error, not all read memory bandwidth available in tile %d\n", x);
  if (rscmgt_info->tile_info[x].write_memory_bw != rscmgt_info->tile_info[x].avail_write_memory_bw) printf("Error, not all write memory bandwidth available in tile %d\n", x);
  x = 63;
  if (rscmgt_info->tile_info[x].read_memory_bw != rscmgt_info->tile_info[x].avail_read_memory_bw) printf("Error, not all read memory bandwidth available in tile %d\n", x);
  if (rscmgt_info->tile_info[x].write_memory_bw != rscmgt_info->tile_info[x].avail_write_memory_bw) printf("Error, not all write memory bandwidth available in tile %d\n", x);
  for (x=0;x<rscmgt_info->num_tiles;x++) {
    if (rscmgt_info->tile_info[x].north_port_bw != rscmgt_info->tile_info[x].avail_north_port_bw) printf("Error, not all north port bandwidth available in tile %d\n", x);
    if (rscmgt_info->tile_info[x].east_port_bw != rscmgt_info->tile_info[x].avail_east_port_bw) printf("Error, not all east port bandwidth available in tile %d\n", x);
    if (rscmgt_info->tile_info[x].west_port_bw != rscmgt_info->tile_info[x].avail_west_port_bw) printf("Error, not all west port bandwidth available in tile %d\n", x);
    if (rscmgt_info->tile_info[x].south_port_bw != rscmgt_info->tile_info[x].avail_south_port_bw) printf("Error, not all south port bandwidth available in tile %d\n", x);
    if (rscmgt_info->tile_info[x].local_port_bw != rscmgt_info->tile_info[x].avail_local_port_bw) printf("Error, not all local port bandwidth available in tile %d\n", x);
  }


 num_iterations = 10;
 num_mappings = 0;
 for (i=0;i<64;i++) types[i] = 1;
 printf("test 4: Tiles assignment and releasing (%d iterations)...\n", num_iterations);
 // We ask for random number of tiles but always the same type of unit
 for (i=0; i < num_iterations; i ++) {
  x = random() % 20;
  printf("testing entry: %d\n", x);
  if (mapped[x]) {
    hn_rscmgt_release_units_set(request_num_tiles[x], tiles_dst_ptr[x]);
    num_mappings--;
    mapped[x] = 0;
  } else {
    request_num_tiles[x] = random() % 10;
    if (hn_rscmgt_find_units_set(36, request_num_tiles[x], types, tiles_dst_ptr[x], types_dst_ptr[x], 1)) {
      hn_rscmgt_reserve_units_set(request_num_tiles[x], tiles_dst_ptr[x]);
      mapped[x] = 1;
      num_mappings++;
    }
  }
 }

 printf("number of mappins: %d\n", num_mappings);
 printf("releasing all mappings...\n");
 for (i=0;i<20;i++) {
   if (mapped[i]) {
     hn_rscmgt_release_units_set(request_num_tiles[i], tiles_dst_ptr[i]);
     num_mappings--;
     mapped[i] = 0;
   }
 }

 num_iterations = 10;
 num_mappings = 0;
 for (i=0;i<64;i++) types[i] = 1;
 printf("test 5: Tiles assignment and releasing (%d iterations), this time with as many sets as possible...\n", num_iterations);
 // We ask for random number of tiles but always the same type of unit
 uint32_t nt;
 uint32_t *p1;
 uint32_t *p2;
 uint32_t num;
 for (i=0;i<num_iterations;i++) {
   nt = random() % 16;
   printf("searching sets of size %d\n", nt);
   if (hn_rscmgt_find_units_sets(36, nt, types, &p1, &p2, &num)) {
     printf("number of sets: %d\n", num);
     for (x=0;x<num;x++) hn_rscmgt_reserve_units_set(nt, p1[x]);
     for (x=0;x<num;x++) hn_rscmgt_release_units_set(nt, p1[x]);
     // we free memory resources
     for (x=0;x<num;x++) {free(p1[x]); free(p2[x]);}
     free(p1);
     free(p2);
   }
 }

  printf("end\n");
  hn_rscmgt_end();

  return 0;
}
