#ifndef __HN_SHARED_STRUCTURES_H__
#define __HN_SHARED_STRUCTURES_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 26, 2018
// File Name: hn_shared_structures.h
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//   Shared structures declaration among HN daemon threads
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stddef.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/time.h>

#include "hn_daemon.h"
#include "hn_resource_manager.h"
#include "hn.h"

/*!
 * \brief Health status for an application client
 */
typedef enum
{
  HN_CLIENT_HEALTH_STATUS_BORN = 0, //<! The client is born, but it is not enough maturated 
  HN_CLIENT_HEALTH_STATUS_LIVE,     //<! Now the client is in the best of its life
  HN_CLIENT_HEALTH_STATUS_ZOMBIE,   //<! Now it is zombie, the client has gone away, but there still are requests
  HN_CLIENT_HEALTH_STATUS_RIP       //<! Now client can RIP, no pending requests
}hn_client_health_status_t;

/*!
 * \brief data type used to represent an application client in the daemon 
 */
typedef struct hn_client_info_st
{
  unsigned long long        cid;           //<! client unique identificator
  unsigned int              input_fifo;    //<! input client fifo Id (from daemon to client)
  unsigned int              output_fifo;   //<! output client fifo Id (from client to daemon)
  hn_client_health_status_t health_status; //<! keeps the health status of the application client
  hn_filter_t               filter;        //<! Filter registered by the app
  pthread_mutex_t           health_mtx;    //<! Mutex to access in mutual exclusion to health_status variable
  pthread_cond_t            health_cond;   //<! Condition variable to sync master/slave appl. server threads
}hn_client_info_t;

/*!
 * \brief data type used to identify the type of an mmi64 moudle  
 */
typedef enum hn_mmi64_module_type_e {
  GENERIC_ITEM_DOWNSTREAM,
  GENERIC_ITEM_UPSTREAM,
  DEBUG_ITEM_UPSTREAM,
  BURST_TRANSFER_DOWNSTREAM,
  BURST_TRANSFER_UPSTREAM
} hn_mmi64_module_type_t;

/*!
 * \brief MMI64 module information data type
 *
 * This is used by the collector to map a Buffer FIFO to a
 * downstream interface
 */
typedef struct hn_mmi64_module_st
{
  uint32_t                id;       //!< Unique Id of the downstream module on the FPGA side
  hn_mmi64_module_type_t  type;     //!< Type of traffic supported by the module: Generic or burst
  uint32_t                index;    //!< Index of the module in the hn_mmi64 interface
}hn_mmi64_module_t;

/*
 * \brief Data burst channel data type
 *
 */
typedef struct hn_data_burst_channel_st
{
  unsigned int        fifo;      //!< The Buffer FIFO id
  hn_mmi64_module_t   module;    //!< The module info the FIFO is mapped to
  uint32_t            vn_id;     //!< The Virtual Network the channel is mapped to, can later be used to dynamically set the VN for the transfer...., asigning a different module based on the VN
  uint8_t             locked;    //!< USED only for burstransfers, indicates whether there is an ongoing burst transfer. Only one running burst transfer is allowed per channel
  unsigned long long  client_id; //!< USED only for burstransfers, indicated the id of the client that triggered the burst transfer, and that has to be notified at the end of the transfer....
}hn_data_burst_channel_t;


typedef struct hn_shm_buffer_transfer_st
{
  uint32_t                 burst_channel_index;
  unsigned long long       client_id;  //!< client that requested the transfer
  hn_data_burst_channel_t *channel;    //!< channel used for ongoing transfer, will be annotated during transfer process
  int                      type;       //!< \see{#hn_command_common_t}
  void                    *shm_addr;   //!< start addres for trasnfer in shm buffer   
  uint32_t                 tile;       //!< tile where Memory is located in HN system
  uint32_t                 addr;       //!< memory address of memory in HN system
  uint32_t                 size;       //!< number of bytes to transfer
  uint32_t                 blocking;   //!< determines whether the daemon has to return a message to application when transfer is finished (blocking)
  //uint8_t                  enable_ack_notification; //!< for blocking transfers, when this variable is set to != 0, the backgroung will configure the the HN system to send the ack when the downstream transfer is finished
}hn_shm_buffer_transfer_t;

//---------------
#define MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL 4
// define the max size for partial transfer, a burst transfer will be splitted in several transfers in case that size exeeds following value -> 64bytes(one block) x 1000 blocks
#define MAX_BYTES_PER_BURST_TRANSFER (64*1000)
// currently set to 2 GB transfer, the total of the mem, so transfer is sent as one single huge transfer
//      compiles complains on peak3 about value out of range when no uint32_t casting is specified
//#define MAX_BYTES_PER_BURST_TRANSFER ((uint32_t)((uint32_t)2*(uint32_t)1024*(uint32_t)1024*(uint32_t)1024))
typedef struct hn_shm_running_transfers_table_entry_st
{
  uint32_t                  free;     //!< indicates whether this entry is free or contains data of currently ongoing transfer
  uint32_t                  finished; //!< for downstream transfers: indicates whether this transfer has finished sending data to HN system, but can be still waiting for an irq, so the backend thread should not enter send function for this entry; for upstream transfers: indicates when the backend thread should launch a new DMA transfer
  hn_shm_buffer_transfer_t  config;
  uint32_t                  num_pending_acks;
  uint32_t                  waiting_for_ack;
  uint32_t                  counter; // amount of bytes transferred / received
  struct timespec           start_time;
  uint32_t                  dma_id; // just for upstream, id of the registered DMA
  uint32_t                  chunk_counter; // just for upstream, amount of bytes received of this sub-transfer
}hn_shm_running_transfers_table_entry_t;

typedef struct hn_shm_running_transfers_table_channel_entry_st
{
  uint8_t                                 downstream_free_entries; //!< indicates whether this entry is free or contains data of currently ongoing transfer
  uint8_t                                 upstream_free_entries; //!< indicates whether this entry is free or contains data of currently ongoing transfer
  uint8_t                                 locked_for_ack_transfers;
  //uint32_t                                physical_downstr_module_index;
  hn_shm_running_transfers_table_entry_t  downstream_entries[MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL];
  hn_shm_running_transfers_table_entry_t  upstream_entries[MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL];
}hn_shm_running_transfers_table_channel_entry_t;

typedef struct hn_shm_running_transfers_table_st 
{
  pthread_mutex_t                                mutex; // mutex to access the table content in a thread-safe manner
  uint32_t                                       number_of_physical_channels;
  hn_shm_running_transfers_table_channel_entry_t *channel;
}hn_shm_running_transfers_table_t;

//--------------


#ifndef _HN_SHARED_STRUCTURES_DEFINITION
//HERE!!! All the shared structures that threads must used declared as externals. The main program
//        (hn_daemon.c) shall define them

/*!
 * \brief List of clients in which a node data is a pointer to the client info \see{ #client_info_t }
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
extern unsigned int client_list;

/*!
 * \brief List of downstream communication channels. Associates a downstream fifo on the daemon 
 *  with the information of the downstream module on the FPGA receiving the data
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
//extern unsigned int downstream_channel_list;
extern unsigned int downstream_item_command_fifo;
extern unsigned int downstream_bursttransfer_command_fifo;
extern unsigned int upstream_bursttransfer_command_fifo;
extern hn_shm_running_transfers_table_t shm_running_transfers_table;

/*!
 * \brief List of downstream modules on the FPGA
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
extern unsigned int downstream_module_list;

/*!
 * \brief List of downstream communication channels. Associates a downstream fifo on the daemon 
 *  with the information of the downstream module on the FPGA receiving the data
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
extern unsigned int upstream_channel_list;

/*!
 * \brief List of downstream modules on the FPGA
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
extern unsigned int upstream_module_list;

// Variables for mutual exclusion to profpga and mmi64 hardware resources access
extern pthread_mutex_t mutex_write_mmi64;
extern pthread_mutex_t mutex_read_mmi64;

// Variables for mutual exclusion in resources access
extern pthread_mutex_t mutex_access_variables;
extern unsigned char   lock_tile_register_access[HN_MAX_TILES];
extern unsigned int    associated_read_register_client[HN_MAX_TILES];
extern unsigned char   lock_memory[HN_MAX_TILES];
extern unsigned int    associated_read_memory_client[HN_MAX_TILES];

extern unsigned int    associated_rscmgt_client;                  // client that made a request/access to the resource manager
extern unsigned int    lock_rscmgt_access;                        // lock to the resource manager

/*!
 * \brief to process item queue
 *
 * JFLICH to improve description
 */
extern unsigned int to_process_item_queue;    // fifo for generic items comming from FPGA (input to dispatcher
extern unsigned int debug_item_queue;         // fifo for debug items comming from FPGA (input to dispatcher
extern unsigned int rscmgt_fifo;              // fifo for resource management replies (between collector and dispatcher)

/*!
 * \brief Condition variable to drive the initialization of the resource manager
 */
extern pthread_cond_t  cond_rscmgt_init;

/*!
 * \brief Shared memory buffers between applications. 
 *
 * This list contains the information stored and shared among threads on the daemon side
 * This list has an analogue one on the HN library side, that list is local to the hn.c
 */
extern unsigned int shared_memory_buffer_list;

/*!
 * \brief Fifo for Shared memory operation notifications from daemon to application 
 */
extern unsigned int shm_notification_fifo;

/*!
 * \brief Fifo for HW device temperature requests from app. The deamon will respond to app through this fifo
 */
extern unsigned int temperature_notification_fifo;

//  --------------- STATS MONITOR
/*!
 * \brief Fifo for Statistics Monitoring requests from app. The deamon will respond to app through this fifo (data and status of command requests)
 */
extern unsigned int stats_monitor_notification_fifo;
// Mutex to read/write activity stats of tiles/cores
extern pthread_mutex_t  mutex_stats_monitor_table;
extern pthread_mutex_t  mutex_stats_monitor_config_access;
extern pthread_cond_t   cond_stats_monitor_config_change;
extern pthread_cond_t   cond_collector_init;
// Table containing activity stats of tiles/cores
extern uint64_t         stats_monitor_polling_period_us; 
extern unsigned int     stats_monitor_tiles_list ;
extern hn_stats_monitor_t stats_monitor_table[HN_MAX_TILES][HN_MAX_CORES_PER_TILE];
extern uint32_t         stats_monitor_active;


#else
//HERE!!!! the same as above, but without extern keyword
/*
 *  list of clients
 *
 *  Contains pointers to elements of type client_info_t (see hn_shared_structures.h)
 */
unsigned int client_list = -1;

/*!
 * \brief List of downstream communication channels. Associates a downstream fifo on the daemon 
 *  with the information of the downstream module on the FPGA receiving the data
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
//unsigned int downstream_channel_list = -1;
unsigned int downstream_item_command_fifo = -1;
unsigned int downstream_bursttransfer_command_fifo = -1;
unsigned int upstream_bursttransfer_command_fifo = -1;
hn_shm_running_transfers_table_t shm_running_transfers_table;

/*!
 * \brief List of downstream modules on the FPGA
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
unsigned int downstream_module_list = -1;

/*!
 * \brief List of downstream communication channels. Associates a downstream fifo on the daemon 
 *  with the information of the downstream module on the FPGA receiving the data
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
unsigned int upstream_channel_list = -1;

/*!
 * \brief List of downstream modules on the FPGA
 *
 * Use the list through the functions in \see{#hn_list.h}
 */
unsigned int upstream_module_list = -1;

/*!
 * \brief to process item queue
 *
 * JFLICH to improve description
 */
unsigned int to_process_item_queue;
unsigned int debug_item_queue;
unsigned int rscmgt_fifo;

// Variables for mutual exclusion to profpga and mmi64 hardware resources access
pthread_mutex_t mutex_write_mmi64;
pthread_mutex_t mutex_read_mmi64;

// Variables for mutual exclusion in resources access
pthread_mutex_t mutex_access_variables;
unsigned char   lock_tile_register_access[HN_MAX_TILES];
unsigned int    associated_read_register_client[HN_MAX_TILES];
unsigned char   lock_memory[HN_MAX_TILES];
unsigned int    associated_read_memory_client[HN_MAX_TILES];

unsigned int    associated_rscmgt_client;                  // client that made a request/access to the resource manager
unsigned int    lock_rscmgt_access;                        // lock to the resource manager

/*!
 * \brief Condition variable to drive the initialization of the resource manager
 */
pthread_cond_t  cond_rscmgt_init;

/*!
 * \brief Shared memory buffers between applications. 
 *
 * This list contains the information stored and shared among threads on the daemon side
 * This list has an analogue one on the HN library side, that list is local to the hn.c
 */
unsigned int shared_memory_buffer_list = -1;

/*!
 * \brief Fifo for Shared memory operation notifications from daemon to application 
 */
unsigned int shm_notification_fifo = -1;

/*!
 * \brief Fifo for HW device temperature requests from app. The deamon will respond to app through this fifo
 */
unsigned int temperature_notification_fifo = -1;

//  --------------- STATS MONITOR
/*!
 * \brief Fifo for Statistics Monitoring requests from app. The deamon will respond to app through this fifo (data and status of command requests)
 */
unsigned int stats_monitor_notification_fifo = -1;

// Mutex to read/write activity stats of tiles/cores
pthread_mutex_t mutex_stats_monitor_table;
pthread_mutex_t mutex_stats_monitor_config_access;
pthread_cond_t  cond_stats_monitor_config_change;
pthread_cond_t  cond_collector_init;
// Table containing activity stats of tiles/cores
unsigned long stats_monitor_polling_period_us = 500000; 
unsigned int  stats_monitor_tiles_list = -1;
hn_stats_monitor_t stats_monitor_table[HN_MAX_TILES][HN_MAX_CORES_PER_TILE];
uint32_t         stats_monitor_active = 0;

#endif  //_HN_SHARED_STRUCTURES_DEFINITION


#ifndef _HN_SHARED_SERVER_MASTER_DEFINITION
extern unsigned long long hn_stats_monitor_client_id;
#else
unsigned long long hn_stats_monitor_client_id = -1;
#endif

#endif
