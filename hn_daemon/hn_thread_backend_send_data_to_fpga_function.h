///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 29, 2018
// Design Name: 
// Module Name: thread backend send data to fpga 
// Project Name: HN daemon
// Target Devices:
// Tool Versions:
// Description:
//     Send outgoing data from applications queues to HN_System on FPGA.
//
//     This thread reads data from each fifo based on a selected policy to guaranty QOS (TO-DO: implement QOS arbiter),
//           currently reads in round robin manner.
//     Read data is propagated to the mmi64 library send function  in two different ways, depending on data transfer type
//           - regular items: read from items fifo(s)
//           - burst transfer data: is read from memory, trasnfer status is checked in order to send completion item notification
//             to the application which programmed the transfer when the app requested to be notified on transfer completion.
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __HN_THREAD_BACKEND_SEND_DATA_TO_FPGA_FUNCTION_H__
#define __HN_THREAD_BACKEND_SEND_DATA_TO_FPGA_FUNCTION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "hn_shared_structures.h" 

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdint.h>



/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Public function prototypes
 **
 **********************************************************************************************************************
 **/

void hn_thread_backend_send_data_to_fpga_set_weights(uint32_t *weights, uint32_t num_weights);

/*!
 * \brief Set arbiter policy to read data from queues containing data from applications going to HN system 
 *
 * \param[in] policy read policy (round robin, weights, ...)
 * \values[in] pointer to array containing extra configuration information for the different policies 
 * \return 0 if policy was successfully set/updated, otherwise return error code
 */
uint32_t hn_thread_backend_send_data_to_fpga_set_arbiter_policy (uint32_t policy, uint32_t *values);

///*!
// * \brief Get arbiter policy to read data from queues containing data from applications going to HN system
// *
// * \param[in] policy pointer to variable to store arbiter policy
// * \values[in] pointer to array containing extra configuration information for the different policies 
// * \return 0 if policy was successfully set/updated, otherwise return error code
// */
//uint32_t hn_thread_backend_get_data_from_fpga_get_arbiter_policy (uint32_t *policy, uint32_t *values);


/*!
 * \brief Set time the thread sleeps, when there is no more data to send to the FPGA in te downstream queues before.
 * before checking the buffers occupation again.
 *
 * \param[in] policy read policy (round robin, weights, ...)
 * \values[in] pointer to array containing extra configuration information for the different policies 
 * \return 0 if policy was successfully set/updated, otherwise return error code
 */
uint32_t hn_thread_backend_send_data_to_fpga_set_buffer_count_refresh (struct timespec ts);


// void ?? o cal passar-li parametres??
/*!
 * \brief Read data from downstream fifos and foward it to downstream modulesthe FPGA
 *
 *        Check for data availability in upstreamif buffers, read data from buffers following arbiter policy
 * \param NONE
 * \return NONE
 */
void *hn_thread_backend_send_data_to_fpga_function( void *argv);



#ifdef __cplusplus
}
#endif

#endif

//*********************************************************************************************************************
// end of file hn_thread_backend_send_data_to_fpga.h
//*********************************************************************************************************************

