///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_socket_unix.c
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Hetoregeneous Node support for unix socket type process communication
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "hn_logging.h"
#include "hn_socket_unix.h"

int hn_socket_unix_initialize(const char *port, hn_socket_unix_info_t* si_un) 
{
  int sock;

  // creating socket
  sock = socket(AF_UNIX, SOCK_STREAM, 0);
  if (sock < 0) 
  { 
    return -1;
  }

  // initializing the socket unix info struct
  si_un->sock = sock;
  si_un->addr.sun_family = AF_UNIX;
  strcpy(si_un->addr.sun_path, port);

  // binding socket to the port
  if (bind(si_un->sock, (struct sockaddr *)&(si_un->addr), sizeof(struct sockaddr_un)) < 0) 
  {
    return -1;
  }

  log_debug("Socket UNIX initialized and binded to port %s", port);

  return 0;
}

int hn_socket_unix_close(hn_socket_unix_info_t *si_un)
{
  struct stat file_info;

  close(si_un->sock);
  unlink(si_un->addr.sun_path);

  // checking if the port has been unlinked completely
  if ((stat(si_un->addr.sun_path, &file_info) < 0) && (errno == ENOENT))
  {
    log_debug("Socket UNIX closed, port %s un-binded and unlinked from file system", si_un->addr.sun_path);
  } else {
    log_warn("Socket UNIX closed, port %s un-binded, but not unlinked from file system", si_un->addr.sun_path);
  }

  return 0;
}


