# HN daemon Makefile


CXX                ?= g++
CC                 ?= gcc
build_type         ?= "Release"
instdir            ?= /opt/mango
prodesign_dir      ?= /opt/prodesign/profpga/proFPGA-2016B
mmi64_generic_file ?= $(instdir)/usr/share/profpga_mmi64_generic.cfg
mmi64_fmpcie_file  ?= $(instdir)/usr/share/profpga_mmi64_fmpcie.cfg
daemon_log_file    ?= /tmp/hn_daemon.log
daemon_log_mode    ?= HN_LOG_TO_STDOUT  # other mode is FILE
general_defines    ?= 

profpga_ver        := $(shell basename $(prodesign_dir))
legacy_support     := $(shell bash -c '[[ $$0 < "proFPGA-2017A" ]] && echo "yes" || echo "no"' $(profpga_ver))

name    := hn_daemon
bins    := $(name) $(name)_stop.sh
headers := hn_daemon.h
sources := hn_daemon.c \
           hn_logging.c \
           hn_socket_unix.c \
           hn_list.c \
           hn_rr_arb.c \
           hn_utils.c \
           hn_buffer_fifo.c \
           hn_request_handler.c \
           hn_item_handler.c \
           hn_exit_program.c \
           hn_thread_application_server_master_function.c \
           hn_thread_application_server_slave_function.c \
           hn_thread_stats_monitor_function.c \
           hn_iface_mmi64.c \
           hn_thread_backend_get_data_from_fpga_function.c \
           hn_thread_backend_send_data_to_fpga_function.c \
           hn_thread_item_collector_function.c \
           hn_thread_item_dispatcher_function.c \
           hn_resource_manager.c \
           hn_shared_memory.c

test_sources := test_hn_list.c \
                test_hn_buffer_fifo.c \
                test_iface_connectivity.c \
                test_thread_backend_get_data_from_fpga.c \
                test_application_server.c \
                test_resource_manager.c

profpga_cfg_files := profpga_mmi64_generic.cfg profpga_mmi64_fmpcie.cfg

DEPDIR  := .deps


obj-files := $(sources:%.c=%.o)
ccflags-y := -Wall $(general_defines)
ldflags-y := -lmmi64 -lprofpga -lconfig -ldl -pthread -lmmi64pli -lrt 
dflags-y  = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
incl-dir  := -I../ -I$(prodesign_dir)/include/
test-bins := $(test_sources:%.c=%)
obj-files-test := $(filter-out hn_daemon.o, $(obj-files))

ifdef MMI64_DISABLE
  ccflags-y += -DHN_DAEMON_MMI64_DISABLE
endif

ifdef FOREGROUND
  ccflags-y +=  -DHN_DAEMON_FOREGROUND 
endif

ifeq ($(legacy_support),yes)
  ccflags-y += -DLEGACY_PROFPGA_LIBS
endif

ifeq ($(build_type),Debug)
  ccflags-y += -g -DDEBUG 
endif

ifeq ($(build_type),Verbose)
  ccflags-y += -g -DDEBUG -DVERBOSE
endif

ifeq ($(daemon_log_mode),FILE)
  daemon_log_mode := HN_LOG_TO_FILE
endif

ifndef FOREGROUND
  daemon_log_mode := HN_LOG_TO_FILE
endif


.PHONY: all test clean install instal_bins install_headers install_profpga_cfg_files

all: $(name)

hn_list.o : ../hn_list.c $(DEPDIR)/hn_list.d
	@if [ ! -d $(DEPDIR) ]; then mkdir -p $(DEPDIR); fi
	$(CC) $(ccflags-y) $(dflags-y) $(incl-dir) -c $< -o $@
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

hn_utils.o : ../hn_utils.c $(DEPDIR)/hn_utils.d
	@if [ ! -d $(DEPDIR) ]; then mkdir -p $(DEPDIR); fi
	$(CC) $(ccflags-y) $(dflags-y) $(incl-dir) -c $< -o $@
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

hn_rr_arb.o : ../hn_rr_arb.c $(DEPDIR)/hn_rr_arb.d
	@if [ ! -d $(DEPDIR) ]; then mkdir -p $(DEPDIR); fi
	$(CC) $(ccflags-y) $(dflags-y) $(incl-dir) -c $< -o $@
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

hn_logging.o : ../hn_logging.c $(DEPDIR)/hn_logging.d
	@if [ ! -d $(DEPDIR) ]; then mkdir -p $(DEPDIR); fi
	$(CC) $(ccflags-y) $(dflags-y) $(incl-dir) -c $< -o $@
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

hn_shared_memory.o : ../hn_shared_memory.c $(DEPDIR)/hn_shared_memory.d
	@if [ ! -d $(DEPDIR) ]; then mkdir -p $(DEPDIR); fi
	$(CC) $(ccflags-y) $(dflags-y) $(incl-dir) -c $< -o $@
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

%.o : %.c
%.o : %.c $(DEPDIR)/%.d
	@if [ ! -d $(DEPDIR) ]; then mkdir -p $(DEPDIR); fi
	$(CC) $(ccflags-y) $(incl-dir) $(dflags-y) -DHN_DAEMON_LOG_MODE=$(daemon_log_mode) -DHN_DAEMON_LOG_FILENAME=\"$(daemon_log_file)\" -DMMI64_MB_CFG_FNAME=\"$(mmi64_generic_file)\" -DMMI64_FMPCIE_CFG_FNAME=\"$(mmi64_fmpcie_file)\" -c $<
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

test_resource_manager : test_resource_manager.o hn_resource_manager.o hn_logging.o
	$(CXX) -L$(prodesign_dir)/lib/linux_x86_64  $^ -o $@ $(ldflags-y)

test_iface_connectivity : test_iface_connectivity.o hn_iface_mmi64.o hn_logging.o hn_list.o
	$(CXX) -L$(prodesign_dir)/lib/linux_x86_64  $^ -o $@ $(ldflags-y)

test_thread_backend_get_data_from_fpga : test_thread_backend_get_data_from_fpga.o hn_thread_backend_get_data_from_fpga_function.o hn_iface_mmi64.o hn_list.o hn_exit_program.o hn_logging.o
	$(CXX) -L$(prodesign_dir)/lib/linux_x86_64  $^ -o $@ $(ldflags-y)

test_% : test_%.o $(obj-files-test)
	$(CXX) -L$(prodesign_dir)/lib/linux_x86_64  $^ -o $@ $(ldflags-y)

test : $(test-bins)

$(name): $(obj-files) 
	$(CXX) -L$(prodesign_dir)/lib/linux_x86_64 $^ -o $@ $(ldflags-y)

install: install_bins install_headers install_profpga_cfg_files

install_bins: $(instdir)/bin/$(name)

$(instdir)/bin/$(name): $(bins)
	@if [ ! -d "$(instdir)/bin" ]; then mkdir -p $(instdir)/bin; fi
	@echo "Installing the HN daemon..."
	@cp $(bins) $(instdir)/bin

install_headers: $(instdir)/include/$(name)

$(instdir)/include/$(name): $(headers)
	@if [ ! -d "$@" ]; then mkdir -p $@; fi
	@echo "Installing the HN daemon headers..."
	@cp $(headers) $@

install_profpga_cfg_files: $(profpga_cfg_files)
	@if [ ! -d "$(instdir)/usr/share" ]; then mkdir -p $(instdir)/usr/share; fi
	@echo "Installing the HN daemon shared files..."
	@cp $(profpga_cfg_files) $(instdir)/usr/share

clean:
	@rm -rf $(DEPDIR)
	@rm -f *.o $(name) $(test-bins)

$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

include $(wildcard $(patsubst %,$(DEPDIR)/%.d,$(basename $(sources))))

