///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Flich (jflich@disca.upv.es)
//
// Create Date: January 25, 2018
// File Name: hn_thread_item_dispatcher_function.c
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Dispatcherr thread to process all incoming items from the HN system
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

#include "hn_daemon.h"
#include "hn_logging.h"
#include "hn_list.h"
#include "hn_utils.h"
#include "hn_buffer_fifo.h"
#include "hn_socket_unix.h"
#include "hn_socket_inet.h"
#include "hn_shared_structures.h"
#include "hn_exit_program.h"
#include "hn_item_commands.h"
#include "hn_item_handler.h"
#include "hn_resource_manager.h"
#include "hn_thread_backend_send_data_to_fpga_function.h"

#include "hn_thread_stats_monitor_function.h"

#include "hn_iface_mmi64.h"

// defines
#define DISPATCHER_SLEEP_MICROSECONDS 100
#define HN_DISPATCHER_DEBUG_ITEM_BUFFER_SIZE 1<<24

#define MAX_TILES HN_MAX_TILES

// function prototypes
void hn_decompose_item(uint32_t item, uint32_t *tile, uint32_t *command, uint32_t *byte);
void fn_dispatch_item(uint32_t item);
void fn_dispatch_raw_items_to_clients(uint32_t *items, int num_items);
void fn_dispatch_unit_item(uint32_t tile, uint32_t item);
void fn_dispatch_rsc_data(hn_data_rsc_t *data);
void fn_dispatch_shm_notification(hn_command_shm_buffer_manage_status_t shm_buf_notification);
void fn_dispatch_temperature_notification(hn_command_temperature_response_t temp_buf_notification);
void fn_dispatch_stats_monitor_notification(hn_command_stats_monitor_response_t stats_monitor_notifiaction_response);


// this function is called upon a thread cancelation
static void unlock_mutex(void *args) {
  pthread_cond_broadcast(&cond_rscmgt_init);
  pthread_mutex_unlock(&mutex_access_variables);
  log_verbose("item_dispatcher thread: cleanup routine called, perhaps was canceled!");
}

// this function is called upon a thread cancelation
static void clean_access_variable_mutex(void *args) {
  pthread_mutex_unlock(&mutex_access_variables);
  pthread_mutex_unlock(&mutex_stats_monitor_table);

  log_verbose("item_dispatcher_thread: cleanup routine called for access_variable_mutex, perhaps was canceled!");
}

u_int32_t item;                          // to store received item
u_int8_t  num_rcv_reg_items[MAX_TILES];  // number or items received for read register operation
u_int8_t  num_rcv_mem_items[MAX_TILES];  // number of items received for read memory operation
u_int32_t rcv_reg_value[MAX_TILES];      // to build the received register value
uint8_t   rcv_mem_value[MAX_TILES][64];  // to build the received memory value (block of 64 bytes)
u_int32_t num_rcv_unit_items[MAX_TILES]; // number of unit items received from a given tile 
u_int32_t rcv_unit_item[MAX_TILES];      // item received for a given tile

u_int32_t num_rcv_int_items[MAX_TILES];  // number of items received for interrupts
u_int32_t rcv_int_value[MAX_TILES];      // to build the interrupts received

hn_data_rsc_t data_rsc;                  // to store resource data
hn_command_shm_buffer_manage_status_t shm_notification; //to store received notification from daemon to aplication related to shared memory buffers management 

hn_command_temperature_response_t     temperature_notification;

hn_command_stats_monitor_response_t       stats_monitor_notification;

// Each tilereg read consists of 6 items, 2 items are for the address and four items are for the value
// we need to keep track of the index of current item for each core of each tile, to properly store the info of the processed item in 
// matrix to store the index of the item processed (per core and per tile)
static uint32_t unit_rcv_reg_addr[HN_MAX_TILES][HN_MAX_CORES_PER_TILE];
// matrix to store the already received items (per core and per tile)
static uint32_t unit_rcv_reg_value[HN_MAX_TILES][HN_MAX_CORES_PER_TILE];
// number of received items for this reg (index)
static uint32_t unit_num_rcv_reg_items[HN_MAX_TILES][HN_MAX_CORES_PER_TILE];

uint32_t items[HN_DISPATCHER_DEBUG_ITEM_BUFFER_SIZE];
int      num_items = 0;

/* 
 * This function loops geting items, processing them and dispatching the data to the corresponding application client
 */
void *hn_thread_item_dispatcher_function(void *args) {

  int i;
  int j;

  static int retval = 0;

  // we initialize local variables
  for (i=0;i<MAX_TILES;i++) {
    num_rcv_reg_items[i] = 0;
    num_rcv_mem_items[i] = 0;
    rcv_reg_value[i]     = 0;
    for (j=0;j<64; j++) rcv_mem_value[i][j] = 0;
    num_rcv_unit_items[i] = 0;
    rcv_unit_item[i] = 0;
    num_rcv_int_items[i] = 0;
    rcv_int_value[i] = 0;
    for(j=0;j<HN_MAX_CORES_PER_TILE;j++) unit_num_rcv_reg_items[i][j] = 0;
  }

  // Resource manager init procedure
  pthread_cleanup_push(unlock_mutex, NULL);
  pthread_mutex_lock(&mutex_access_variables);

  if (hn_rscmgt_get_status() != HN_RESOURCE_MANAGER_STATUS_RUNNING) {
    while ((!hn_exit_program_is_terminated()) && (hn_rscmgt_get_status() != HN_RESOURCE_MANAGER_STATUS_GETTING_ARCH_ID)) {
      pthread_cond_wait(&cond_rscmgt_init, &mutex_access_variables);
    }

    if (!hn_exit_program_is_terminated()) {
      // what to do with reset items? I mean, how many I must read
      // Simply, let's command get_arch_id and discard any received item until the arch id item come up
      int arch_id_item_found = 0;
      uint8_t buf[4];
      while (!hn_exit_program_is_terminated() && !arch_id_item_found) {
        int n = hn_buffer_fifo_read_elements_to_buf(to_process_item_queue, &item, 1);
        if (n == 1) {
          hn_item_t *i = (hn_item_t *)&item;
          size_t num_bytes = hn_item_upstream_handle(i, 1, buf, 4);
          if (num_bytes == 4) {
            // we take profit that reset items do not return any bytes
            unsigned int arch_id = ((hn_mango_tilereg_0_t *)buf)->arch_id;
            hn_rscmgt_set_arch_id(arch_id);
            log_info("Setting architecture ID...");
            arch_id_item_found = 1;
          } 
        } 
      }
    }
  }
  pthread_cond_signal(&cond_rscmgt_init);
  pthread_mutex_unlock(&mutex_access_variables);
  pthread_cleanup_pop(0);
  // Resource manager init procedure end

  log_info("item_dispatcher_thread: ready...");

  // We loop endless
  uint32_t sleep = 0;   // if we loop and do not find data then we sleep for a while
  pthread_cleanup_push(clean_access_variable_mutex, NULL);
  while (!hn_exit_program_is_terminated()) {

    if (sleep) usleep(DISPATCHER_SLEEP_MICROSECONDS);
    sleep = 1;  // by default, if we find data we cancel sleep

    // we endless read and process elements from different fifos: to_process_item_queue and rscmgt_fifo

    // We get an item from the input fifo
    int num_items = hn_buffer_fifo_read_elements_to_buf(to_process_item_queue, &item, 1);
    if (num_items == 1) {sleep = 0; fn_dispatch_item(item);}

    num_items = hn_buffer_fifo_read_elements_to_buf(debug_item_queue, items, HN_DISPATCHER_DEBUG_ITEM_BUFFER_SIZE);
    if (num_items > 0) {sleep = 0; fn_dispatch_raw_items_to_clients(items, num_items);}

    // We get data from the rscmgt_fifo
    int num_elements = hn_buffer_fifo_read_elements_to_buf(rscmgt_fifo, &data_rsc, 1);
    if (num_elements == 1) {sleep = 0; fn_dispatch_rsc_data(&data_rsc);}

    // We get the shared memory area notification messages
    int num_shm_notifications = hn_buffer_fifo_read_elements_to_buf(shm_notification_fifo, &shm_notification, 1);
    if (num_shm_notifications == 1) {sleep = 0; fn_dispatch_shm_notification(shm_notification);}

    int num_temp_notifications = hn_buffer_fifo_read_elements_to_buf(temperature_notification_fifo, &temperature_notification, 1);
    if (num_temp_notifications == 1) {sleep = 0; fn_dispatch_temperature_notification(temperature_notification);}

    int num_stats_notifications = hn_buffer_fifo_read_elements_to_buf(stats_monitor_notification_fifo, &stats_monitor_notification, 1);
    if (num_stats_notifications == 1) {sleep = 0; fn_dispatch_stats_monitor_notification(stats_monitor_notification);}

  }
  pthread_cleanup_pop(0);

  log_info("item_dispatcher_thread: exiting...");
  return &retval;
}




/*
 * fn_dispatch_unit_item. This function dispatches a unit's item
 */
void fn_dispatch_unit_item(uint32_t tile, uint32_t item) {
  //int found;
  int iter;

  // we first decompose the item
  uint32_t subtile;
  uint32_t command;
  uint32_t payload;
  //uint32_t was_first = 0;
  
  hn_decompose_item(item, &subtile, &command, &payload);
  // now we case the different commands
  switch (command) {
    case COMMAND_CONSOLE:  
                          // we search all the clients and those with a filter match will be notified with the console item
                          //found = 0;
                          iter = hn_list_iter_front_begin(client_list);
                          while (!hn_list_iter_end(client_list, iter)) {
                            hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
                            if ((client->filter.mode == HN_FILTER_APPL_MODE_CONSOLE) &&  
                                (client->filter.tile == 999 || client->filter.tile == tile) && 
                                (client->filter.core == 999 || client->filter.core == subtile)) {
                                     //found = 1;
                                     hn_buffer_fifo_write_elements_from_buf(client->input_fifo, &payload, 1);
                            }
                            iter = hn_list_iter_next(client_list, iter);
                          }
                          // JFLICH, Why is this an error?
                          //if (!found) log_error("associated client with a read register operation not found\n");
                          break;
    case COMMAND_TILEREG_FIRST_ITEM:
                          log_verbose("  UNIT TILEREG     first item (%d)   tile: %u   subtile: %u", command, tile, subtile);
                          if ((unit_num_rcv_reg_items[tile][subtile] != 6) && (unit_num_rcv_reg_items[tile][subtile] != 0)) {
                            log_error("hn_tread_item_dispatcher, detected incomplete Tilereg read for tile %d, subtile %d, received %u of 6", tile, subtile, unit_num_rcv_reg_items[tile][subtile]);
                          }
                          unit_num_rcv_reg_items[tile][subtile] = 0; // we resync...so if we do not receive an complete reg read we will resync reading
                          unit_rcv_reg_value[tile][subtile] = 0;
                          unit_rcv_reg_addr[tile][subtile] = 0;
                          //was_first = 1;
    case COMMAND_TILEREG_REMAINING_ITEMS:
                          //if (!was_first)
                          log_verbose("  UNIT TILEREG item for   tile: %u   subtile: %u   index (%d)  payload: %u", tile, subtile, unit_num_rcv_reg_items[tile][subtile], payload);
                          // we search all the clients and those with a filter match will be notified with the console item
                          switch (unit_num_rcv_reg_items[tile][subtile]) {
                                 case 0: unit_rcv_reg_value[tile][subtile] = unit_rcv_reg_value[tile][subtile] | (payload      ); break;
                                 case 1: unit_rcv_reg_value[tile][subtile] = unit_rcv_reg_value[tile][subtile] | (payload <<  8); break;
                                 case 2: unit_rcv_reg_value[tile][subtile] = unit_rcv_reg_value[tile][subtile] | (payload << 16); break;
                                 case 3: unit_rcv_reg_value[tile][subtile] = unit_rcv_reg_value[tile][subtile] | (payload << 24); break;
                                 case 4: unit_rcv_reg_addr[tile][subtile]  = unit_rcv_reg_addr[tile][subtile]  | (payload      ); break;
                                 case 5: unit_rcv_reg_addr[tile][subtile]  = unit_rcv_reg_addr[tile][subtile]  | (payload <<  8); break;
                          }
                          unit_num_rcv_reg_items[tile][subtile]++;

                          if (unit_num_rcv_reg_items[tile][subtile] == 6) {
                            log_verbose("  UNIT TILEREG complete read   tile: %u   subtile: %u   reg_addr: %u   value %u", tile, subtile, unit_rcv_reg_addr[tile][subtile], unit_rcv_reg_value[tile][subtile]);
                            iter = hn_list_iter_front_begin(client_list);
                            uint32_t client_index = 0;
                            while (!hn_list_iter_end(client_list, iter)) {
                              //log_info("JM10, checking client : %u", client_index);
                              client_index++;
                              // We here distinguish between the hn_stats_monitor thread and the rest of the clients, to the stats monitor we send more info in a different format: tile, subtile, address, value
                              hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
                              if (client->cid == hn_stats_monitor_client_id)
                              {
                                log_debug("sending tilereg read data to hn_stats_monitor, full data struct");
                                // data could go for the stats monitor !!!.... the stats mon will filter out any tilereg read that is not stats
                                hn_command_unit_read_register_response_t  rsp;
                                rsp.common.command = HN_COMMAND_PEAK_READ_REGISTER;
                                rsp.tile     = tile;
                                rsp.subtile  = subtile;
                                rsp.reg      = unit_rcv_reg_addr[tile][subtile];
                                rsp.value    = unit_rcv_reg_value[tile][subtile];

                                hn_buffer_fifo_write_elements_from_buf(client->input_fifo, &rsp, sizeof(hn_command_unit_read_register_response_t));
                              }
                              else if (    (client->filter.mode == HN_FILTER_APPL_MODE_SYNC_READS)
                                        && (client->filter.tile == 999 || client->filter.tile == tile)
                                        && (client->filter.core == 999 || client->filter.core == subtile) 
                                        && (!hn_stats_monitor_reg_is_for_stats(tile, unit_rcv_reg_addr[tile][subtile])))
                              {
                                log_warn("sending unit tilereg read to regular client, send only plain reg value, we should avoid sending data to any client who did not request it, ");
                                hn_buffer_fifo_write_elements_from_buf(client->input_fifo, &unit_rcv_reg_value[tile][subtile], 4);
                              }
                              iter = hn_list_iter_next(client_list, iter);
                            }
                            //log_info("JM10");
                          }

                          break;

    default:              log_verbose("non registered handler for UNIT item type (%d)\n", command);
                          break;
  }
}

/*
 * hn_decompose_item. This function takes an item and decomposes it in its fields (tile, command, byte)
 */
void hn_decompose_item(uint32_t item, uint32_t *tile, uint32_t *command, uint32_t *byte)
{
  //uint32_t item_aux = ntohl(item);
  *tile = (item >> 14) & 0x00000FFF;
  *command = (item >> 8) & 0x0000003F;
  *byte = item & 0x000000FF;  
}

/*
 * fn_dispatch_raw_items. This function dispatches raw items
 */
void fn_dispatch_raw_items_to_clients(uint32_t *items, int num_items) {
  int       iter;

  // We send the item to all clients with filter RAWFRAMES
  iter = hn_list_iter_front_begin(client_list);
  while (!hn_list_iter_end(client_list, iter)) {
    hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
    if (client->filter.mode == HN_FILTER_APPL_MODE_RAWFRAMES) {
      log_verbose("item_dispatcher_thread: sending %d items (%lu bytes) to the client id %d with RAWFRAMES filter", num_items, num_items*sizeof(*items), client->cid);
      hn_buffer_fifo_write_bytes_from_buf(client->input_fifo, items, sizeof(uint32_t)*num_items);
    }
    iter = hn_list_iter_next(client_list, iter);
  }
}

/*
 * fn_dispatch_item. This function dispatches a read item
 */
void fn_dispatch_item(uint32_t item) {
  int       iter;
  int       i;
  int       found;
  uint32_t  client_id;
  uint32_t  id;

  // We send the item to all clients with filter RAWFRAMES
  iter = hn_list_iter_front_begin(client_list);
  while (!hn_list_iter_end(client_list, iter)) {
    hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
    if (client->filter.mode == HN_FILTER_APPL_MODE_RAWFRAMES) {
      hn_buffer_fifo_write_bytes_from_buf(client->input_fifo, &item, sizeof(item));
    }
    iter = hn_list_iter_next(client_list, iter);
  }

  // we process the item. First we get the type of item and its payload
  uint32_t tile;
  uint32_t command;
  uint32_t payload;
  hn_decompose_item(item, &tile, &command, &payload);
  // based on the command we perform an action
  switch (command) {
    case COMMAND_TILEREG_FIRST_ITEM:
    case COMMAND_TILEREG_REMAINING_ITEMS:
                               log_verbose("item_dispatcher_thread: received TR item 0x%08x", item);
                               // We store the item value depending on the current number of items received from the tile
                               // If we complete the needed number of items then we send the value read to the 
                               // associated application client fifo
                               switch (num_rcv_reg_items[tile]) {
                                 case 0: rcv_reg_value[tile] = rcv_reg_value[tile] | payload; break;
                                 case 1: rcv_reg_value[tile] = rcv_reg_value[tile] | (payload << 8); break;
                                 case 2: rcv_reg_value[tile] = rcv_reg_value[tile] | (payload << 16); break;
                                 case 3: rcv_reg_value[tile] = rcv_reg_value[tile] | (payload << 24); break;
                               }
                               num_rcv_reg_items[tile]++;
                               if (num_rcv_reg_items[tile] == 6) {

                                 // we store the value into the associated fifo to the client application
                                 // requesting this read
                                 // for this, first we search the client and its associated input queue
                                 found = 0;
                                 iter = hn_list_iter_front_begin(client_list);
                                 while (!hn_list_iter_end(client_list, iter)) {
                                   hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
                                   if (client->cid == associated_read_register_client[tile]) {
                                     found = 1;
                                     hn_buffer_fifo_write_elements_from_buf(client->input_fifo, &rcv_reg_value[tile], 4);
                                   }
                                   iter = hn_list_iter_next(client_list, iter);
                                 }
                                 if (!found) log_error("associated client with a read register operation not found\n");
                                 // we release the lock for read registers
                                 pthread_mutex_lock( &mutex_access_variables );
                                 pthread_testcancel();
                                 lock_tile_register_access[tile] = 0;
                                 pthread_mutex_unlock( &mutex_access_variables );
                                 // we prepare for the next read register operation
                                 num_rcv_reg_items[tile] = 0;
                                 rcv_reg_value[tile]     = 0;
                               }
                               break;

    case COMMAND_MC_DATA_FIRST_ITEM: 
    case COMMAND_MC_DATA_REMAINING_ITEMS:
                               log_verbose("item_dispatcher_thread: received MC item 0x%08x", item);
                               // We store the item value depending on the current number of items received for memory read
                               // If we complete the needed number of items then we send the value read to the 
                               // associated application client fifo
                               rcv_mem_value[tile][num_rcv_mem_items[tile]] = payload;
                               num_rcv_mem_items[tile]++;
                               if (num_rcv_mem_items[tile] == 68) {

                                 // we store the value into the associated fifo to the client application
                                 // requesting this read
                                 // for this, we first search the client and its associated input queue
                                 found = 0;
                                 iter = hn_list_iter_front_begin(client_list);
                                 while (!hn_list_iter_end(client_list, iter)) {
                                   hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
                                   if (client->cid == associated_read_memory_client[tile]) {
                                     found = 1;
                                     hn_buffer_fifo_write_elements_from_buf(client->input_fifo, &rcv_mem_value[tile], 64);
                                   }
                                   iter = hn_list_iter_next(client_list, iter);
                                 }
                                 if (!found) log_error("associated client with a read memory operation not found\n");
                                 // we release the lock for read memory
                                 pthread_mutex_lock( &mutex_access_variables );  
                                 pthread_testcancel();
                                 lock_memory[tile] = 0;
                                 pthread_mutex_unlock( &mutex_access_variables );
                                 // we prepare for the next read memory operation
                                 num_rcv_mem_items[tile] = 0;
                                 for (i=0;i<64;i++) rcv_mem_value[tile][i] = 0;
                               }
                               break;

    case COMMAND_INTERRUPTS_FIRST_ITEM:
    case COMMAND_INTERRUPTS_REMAINING_ITEMS:
                               log_verbose("item_dispatcher_thread: received INT item 0x%08x", item);
                               // We store the item value depending on the current number of items received
                               switch (num_rcv_int_items[tile]) {
                                 case 0: rcv_int_value[tile] = rcv_int_value[tile] | payload; break;
                                 case 1: rcv_int_value[tile] = rcv_int_value[tile] | (payload << 8); break;
                                 case 2: rcv_int_value[tile] = rcv_int_value[tile] | (payload << 16); break;
                               }
                               num_rcv_int_items[tile]++;
                               if (num_rcv_int_items[tile] == 3) {
                                 // We have received interrupts, the upper 16 bits are related to DMA and other devices whereas the lower
                                 // 16 bits are related to the interrupts generated by the unit. We deal with these two sets separately
                                 
                                 log_debug("@item_dispatcher -> Received interrupt signal from tile %2u with value 0x%08X", tile, rcv_int_value[tile]& 0x00FFFFFF);

                                 // We deal with the unit interrupts. To do this, we search whether there is a client waiting for a matching interrupt
                                 uint16_t unit_interrupts = rcv_int_value[tile] & 0x0000ffff;
                                 while ( hn_rscmgt_find_waiting_client_to_interrupt (tile, unit_interrupts, &client_id, &id) ) {
                                    
                                   // The interrupt was registered and will trigger a notification to the associated client
                                   // first we search associated client and its associated input queue
                                   found = 0;
                                   iter = hn_list_iter_front_begin(client_list);
                                   while (!hn_list_iter_end(client_list, iter)) {
                                     hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
                                     if (client->cid == client_id) {
                                       found = 1;
                                       data_rsc.result = HN_RSC_INT_RECEIVED;
                                       hn_buffer_fifo_write_elements_from_buf(client->input_fifo, &data_rsc, sizeof(data_rsc));
                                     }
                                     iter = hn_list_iter_next(client_list, iter);
                                   }
                                   if (!found) log_error("associated client with a wait on an interrupt operation not found\n");
                                   // We remove the wait
                                   hn_rscmgt_annotate_wait_on_interrupt(id, 0);

                                 }

                                 // Now we deal with the upper part of the received interrupts. At this moment, the highest bit corresponds to a DMA 
                                 // completion operation
                                 uint32_t dma_int = (rcv_int_value[tile] & 0x00F00000) >> 20;
                                 if (dma_int) {
                                   log_debug("DMA interrupt (completion) received from tile %d, mask = 0x%08X", tile, dma_int);

                                   uint32_t ch_mask, dma_chan;
                                   for (ch_mask = 1, dma_chan = 0; ch_mask <= 0xF; ch_mask = ch_mask << 1, dma_chan++)
                                   {
                                     if (dma_int & ch_mask) {
                                       while ( hn_rscmgt_find_waiting_client_to_dma (tile, dma_chan, &client_id, &id) ) {
                                         // There is one client waiting for this dma, let's wake him up
                                         found = 0;
                                         iter = hn_list_iter_front_begin(client_list);
                                         while (!hn_list_iter_end(client_list, iter)) {
                                           hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
                                           if (client->cid == client_id) {
                                             found = 1;
                                             data_rsc.result = HN_RSC_DMA_FINISHED;
                                             hn_buffer_fifo_write_elements_from_buf(client->input_fifo, &data_rsc, sizeof(data_rsc));
                                           }
                                           iter = hn_list_iter_next(client_list, iter);
                                         }
                                         if (!found) log_error("associated client with a wait on an DMA operation not found\n");
                                         // We remove the wait
                                         hn_rscmgt_annotate_wait_on_dma(id, 0);
                                       }
                                     }
                                   }
                                 }

                                 // SE mango_tile.v
                                 // interrupts_fromUNIT_toINI is 16 bits, so range [19:16] will be utilized for burst down channels
                                 // so we can leave the other four for the upstream DMA channels
                                 uint32_t burst_down_int     = (rcv_int_value[tile] & 0x000F0000);

                                 if(burst_down_int)
                                 {
                                   uint32_t                                num_modules_dwnstr_generic_item = -1;
                                   uint32_t                                ch_mask;
                                   uint32_t                                dnstr_burst_ch_index;
                                   hn_command_shm_buffer_manage_status_t   shm_operation_status;
                                   log_debug("BURST transfer of downstream burst channel completion detected, IRQ mask: 0x%08X", burst_down_int);
                                   hn_get_number_of_io_modules_of_type(GENERIC_ITEM_DOWNSTREAM, &num_modules_dwnstr_generic_item);

                                   // we need to find out wich channel position is .... so, we can find the same approximation as for creating the channels...
                                   // they are sequentially created with the IDs, so we can search until getting the appropiate id
                                   // we are looking for the downstream channel number that is in the same index as the postion of the irq bit
                                   for (ch_mask = 0x00010000, dnstr_burst_ch_index = 0;  ch_mask <= 0x000F0000; ch_mask = ch_mask*2, dnstr_burst_ch_index++)
                                   {
                                     if (burst_down_int & ch_mask)
                                     {
                                       uint32_t ch_entry_index;
                                       uint8_t  entry_found;
                                       uint8_t  notify_client;

                                       log_debug("@item_dispatcher, irq mask matches channel %u", dnstr_burst_ch_index);

                                       // the entry index in the table matches the index of the irq, so we can directly scan for the finished transfer entry in the channel of the table
                                       entry_found   = 0;
                                       notify_client = 0;
                                       pthread_mutex_lock( &(shm_running_transfers_table.mutex) );

                                       for (ch_entry_index = 0; ch_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL ; ch_entry_index++) 
                                       {
                                         log_debug("@item_dispatcher, checking entry %u,  free %u  contains_data: %s   waiting for ack: %u -> %s", 
                                             ch_entry_index,
                                             shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].free,
                                             (shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].free != 0)? "yes":"no",
                                             shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].waiting_for_ack,
                                             (shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].waiting_for_ack != 0)? "yes":"no"
                                             );

                                         // For this code to work properly only ONE transfer waiting for ack CAN be active per channel
                                         if ((shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].free == 0) &&  (shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].waiting_for_ack != 0) )
                                         {
                                           entry_found = 1;
                                           if (shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].num_pending_acks > 0)
                                           {
                                             shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].num_pending_acks--;
                                             if(shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].num_pending_acks == 0)
                                             {
                                               struct timespec end_time;

                                               shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].free = 1;
                                               shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].waiting_for_ack = 0;
                                               shm_running_transfers_table.channel[dnstr_burst_ch_index].locked_for_ack_transfers = 0;
                                               // in case that transfer is not waiting for ack, the number of free entries will be updated on transmission function
                                               shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_free_entries++;
                                               shm_operation_status.client_id = shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].config.client_id;
                                               notify_client = shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].config.blocking;

                                               clock_gettime(CLOCK_MONOTONIC, &end_time);
                                               float elapsed_in_sec = (float) get_elapsed_time_seconds(shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].start_time, end_time);

                                               log_notice("@item_dispatcher, downstream transfer completed, BW = %6.3f KB/s", ((float)(shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].config.size) / 1024.) / elapsed_in_sec);

                                               log_debug("@item_dispatcher, Detected end of burst transfer with notification. channel %u, entry %u", dnstr_burst_ch_index, ch_entry_index);
                                             }
                                             else
                                             {
                                               // ack of partial transfer, dismiss ack
                                               log_debug("@item_dispatcher, ack for partial transfer, dismiss irq. channel %u, entry %u pending acks %u", 
                                                   dnstr_burst_ch_index, ch_entry_index, shm_running_transfers_table.channel[dnstr_burst_ch_index].downstream_entries[ch_entry_index].num_pending_acks);
                                             }

                                           }
                                           else 
                                           {
                                             log_error("Received more acks than expected for channel %u, entry %u", dnstr_burst_ch_index, ch_entry_index);
                                             log_error("DAEMON will not notify app, will mark entry as free");
                                           }

                                           break; // break loop, so ch_entry_index keeps the value of current entry to later get client_id and do not spend unnecessary time in the loop
                                         }
                                       }
                                       // release mutex as soon as possible, later notify client
                                       pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );

                                       if(entry_found == 0)
                                       {
                                         log_error("Received IRQ notification for downstream burst transfer on channel %d, but no ongoing transfer info found", dnstr_burst_ch_index);
                                       }

                                       if( notify_client != 0) 
                                       {
                                         shm_operation_status.info.command = HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER;
                                         shm_operation_status.status       = HN_SHM_OPERATION_OK;
                                         //shm_operation_status.client_id    = shm_running_transfers_table.channel[dnstr_burst_ch_index].entry[ch_entry_index].config.client_id; already done before releasing the table mutex

                                         //send notification to application... will it be waiting if it is not a blocking operation....
                                         // if the transfer is not blocking this could generate an error...receiving unexpected data...
                                         log_debug("  posting irq notification for client %llu into shm_notification_fifo id: %d", shm_operation_status.client_id, shm_notification_fifo);
                                         hn_buffer_fifo_write_bytes_from_buf(shm_notification_fifo, &shm_operation_status, sizeof(hn_command_shm_buffer_manage_status_t));
                                       }
                                     }
                                   }
                                 }

                                 // we prepare for the next interrupt operation
                                 num_rcv_int_items[tile] = 0;
                                 rcv_int_value[tile]     = 0;
                               }
                               break;
 
    case COMMAND_UNIT_DATA_FIRST:
    case COMMAND_UNIT_DATA_REMAINING:
                               // for resync
                               if ((command == COMMAND_UNIT_DATA_FIRST) && (num_rcv_unit_items[tile] != 0)) {
                                 num_rcv_unit_items[tile] = 0;
                                 log_warn("item_dispatcher_thread: resync done for unit %u", tile);
                               }
                               log_verbose("item_dispatcher_thread: received unit item 0x%08x", item);
                               // this is a unit item, so we need to collect several items from the same unit and
                               // reconstruct the item
                               // So, we build the items as they come and once completed we take an action
                               switch (num_rcv_unit_items[tile]) {
                                 case 0: rcv_unit_item[tile] = rcv_unit_item[tile] | payload;
                                         break;
                                 case 1: rcv_unit_item[tile] = rcv_unit_item[tile] | (payload << 8);
                                         break;
                                 case 2: rcv_unit_item[tile] = rcv_unit_item[tile] | (payload << 16);
                                         break;
                                 case 3: rcv_unit_item[tile] = rcv_unit_item[tile] | (payload << 24);
                                         break;
                               }
                               num_rcv_unit_items[tile]++;
                               if (num_rcv_unit_items[tile] == 4) {
                                 // we dispatch the unit item
                                 log_verbose("item_dispatcher_thread: extracted unit item for tile %u item 0x%08x", tile, rcv_unit_item[tile]);
                                 fn_dispatch_unit_item(tile, rcv_unit_item[tile]);
                                 // we prepare for next item for the tile unit
                                 num_rcv_unit_items[tile] = 0;
                                 rcv_unit_item[tile]      = 0;
                               }
                               break;

    default:                   //log_error("Non expected item type (%d) received by dispatcher thread\n", command);
                               break;
  }
}
 
//---------------------------------------------------------------------------------------------------------------------
void fn_dispatch_rsc_data(hn_data_rsc_t *data) {
  int       iter;
  int       found;

  log_verbose("dispatcher thread: read 1 rsc data");

  // we store the data into the associated fifo to the client application
  // requesting this operation
  // for this, first we search the client and its associated input queue
  found = 0;
  iter = hn_list_iter_front_begin(client_list);
  while (!hn_list_iter_end(client_list, iter)) {
    hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
    if (client->cid == data->client_id) {
      found = 1;
      hn_buffer_fifo_write_bytes_from_buf(client->input_fifo, data, sizeof(*data));
    }
    iter = hn_list_iter_next(client_list, iter);
  }
  if (!found) log_error("associated client with a pending rscmgt operation not found\n");
}

//---------------------------------------------------------------------------------------------------------------------
void fn_dispatch_shm_notification(hn_command_shm_buffer_manage_status_t shm_buf_notification)
{
  int       iter;
  int       found;

  //log_debug("dispatcher, send shm operation notification to client....");
  log_verbose("dispatcher thread: read 1 shared memory notification to application");

  // we store the data into the associated fifo to the client application
  // requesting this operation
  // for this, first we search the client and its associated input queue
  found = 0;
  iter  = hn_list_iter_front_begin(client_list);

  while (!hn_list_iter_end(client_list, iter)) {
    hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
    if (client->cid == shm_buf_notification.client_id) {
      found = 1;
      hn_buffer_fifo_write_bytes_from_buf(client->input_fifo, &shm_buf_notification, sizeof(shm_buf_notification));
    }
    iter = hn_list_iter_next(client_list, iter);
  }

  if (!found) log_error("associated client with a pending shared memory notification operation not found\n");
}

//---------------------------------------------------------------------------------------------------------------------
void fn_dispatch_temperature_notification(hn_command_temperature_response_t temp_buf_notification)
{
  int       iter;
  int       found;

  log_verbose("dispatcher thread: read 1 temperature response notification to application");

  // we store the data into the associated fifo to the client application
  // requesting this operation
  // for this, first we search the client and its associated input queue
  found = 0;
  iter  = hn_list_iter_front_begin(client_list);

  while (!hn_list_iter_end(client_list, iter)) {
    hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
    if (client->cid == temp_buf_notification.client_id) {
      found = 1;
      hn_buffer_fifo_write_bytes_from_buf(client->input_fifo, &temp_buf_notification, sizeof(hn_command_temperature_response_t));
    }
    iter = hn_list_iter_next(client_list, iter);
  }

  if (!found) log_error("associated client with a pending temperature response notification operation not found\n");
}

//---------------------------------------------------------------------------------------------------------------------
void fn_dispatch_stats_monitor_notification(hn_command_stats_monitor_response_t stats_monitor_notification)
{
  int       iter;
  int       found;

  log_debug("dispatcher thread: read 1 stats monitor response notification to application");

  // we store the data into the associated fifo to the client application
  // requesting this operation
  // for this, first we search the client and its associated input queue
  found = 0;
  iter  = hn_list_iter_front_begin(client_list);

  while (!hn_list_iter_end(client_list, iter)) {
    hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
    if (client->cid == stats_monitor_notification.client_id) {
      found = 1;
      log_debug("dispatcher send stats data to client %d", stats_monitor_notification.client_id);
      hn_buffer_fifo_write_bytes_from_buf(client->input_fifo, &stats_monitor_notification, sizeof(hn_command_stats_monitor_response_t));
    }
    iter = hn_list_iter_next(client_list, iter);
  }

  if (!found) log_error("associated client with a pending stats monitor response notification operation not found\n");

}
