///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 23, 2018
// File Name: hn_buffer_fifo.c
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    HN Buffer FIFO functions implementation.
//    It uses hn_list to implement the a list of buffers in FIFO mode
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: 
//
//  TODO re-code in a more efficient way, taking into account that access must be thread-safe
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>

#include "hn_buffer_fifo.h"
#include "hn_logging.h"

// Defines the size of the pool of FIFOs
#define HN_BUFFER_FIFO_MAX_POOL_SIZE 32

typedef struct hn_buffer_fifo_status_st
{
  int num_operations_active;       // num operations active in a fifo
  pthread_mutex_t mutex_fifo;      // FIFO mutex for accessing to the rd/wr pointers
  pthread_cond_t  cond_op_active;  // condition variable for num_operations_active
  pthread_cond_t  cond_fifo;       // condition variable for sync between read and write operations
  int num_write_operations;        // Number of threads writing in the fifo (should be 1 or 0)
  pthread_cond_t  cond_write_fifo; // condition variable for num_write_operations
  pthread_mutex_t mutex_write_fifo;// mutex for access num_write_operations
}hn_buffer_fifo_status_t;

/////////////////////////////////////////////////////////////////////

/*
 * Gets the next index free in the pool in a cyclic way.
 *
 * Returns -1 in case of all indexes occupied
 */
static int get_next_index_free();

/*
 * Gets the number of bytes that can be read in the given buffer FIFO
 *
 * It is not safe for threads, so it must be called in mutual exclusion
 */
static size_t get_num_bytes(unsigned int id);

/*
 * Copies upto num_bytes from the FIFO with the given Id to dst buffer
 */
static size_t copy_from_fifo_to_buf(unsigned int id, void *dst, size_t num_bytes);

/*
 * Generates a string representation of a FIFO state
 */
static const char *to_str_fifo_state(unsigned int id) __attribute__((unused));

/*
 * Cleanup routine for threads
 */
static void clean_fifo_mutex(void *args);
static void clean_write_fifo_mutex(void *args);
static void clean_pool_access_mutex(void *args);

////////////////////////////////////////////////////////////////////////
/*
 * Variable definition
 */

static hn_buffer_fifo_t pool_fifos[HN_BUFFER_FIFO_MAX_POOL_SIZE];

// presence vector, which pool entries are busy
static int pool_fifos_entry_busy[HN_BUFFER_FIFO_MAX_POOL_SIZE];

// to access the pool in a thread safe mode
static pthread_mutex_t pool_access_mutex = PTHREAD_MUTEX_INITIALIZER;

// Keeps the status info for every fifo
static hn_buffer_fifo_status_t pool_fifos_status[HN_BUFFER_FIFO_MAX_POOL_SIZE];

//////////////////////////////////////////////////////////////////////

// Gets the next index free in the pool of fifo buffers
static int get_next_index_free()
{
  // this static is for local scope, but it is initialized only once
  static int last_busy = -1;

  if (last_busy == -1) 
  {
    last_busy = 0;
    return 0;
  }

  register int next = last_busy;
  do
  {
    next = (next + 1) % HN_BUFFER_FIFO_MAX_POOL_SIZE;
    if (pool_fifos_entry_busy[next] == 0) 
    {
      last_busy = next;
      return next;
    }
  }while(next != last_busy);

  return -1;
}


// Gets the number of bytes in a FIFO
// Take into account to use lock/unlock before/after the call
// Also, id has to be a valid entry in the pool of FIFOs
size_t get_num_bytes(unsigned int id)
{
  register unsigned int rd_ptr = pool_fifos[id].rd_ptr;
  register unsigned int wr_ptr = pool_fifos[id].wr_ptr;
  register unsigned int state  = pool_fifos[id].state;

  // Next, get fifo size in bytes & copy data
  size_t bytes_in_fifo = 0;
  if (state == HN_BUFFER_FIFO_STATE_FULL)
  {
    bytes_in_fifo = HN_BUFFER_FIFO_MAX_SIZE;
  }
  else if (state == HN_BUFFER_FIFO_STATE_EMPTY)
  {
    bytes_in_fifo = 0;
  }
  else if (state == HN_BUFFER_FIFO_STATE_OK)
  {
    if (wr_ptr > rd_ptr)
    {
      bytes_in_fifo = (wr_ptr - rd_ptr);
    }
    else if (wr_ptr < rd_ptr)
    {
      bytes_in_fifo = (HN_BUFFER_FIFO_MAX_SIZE) - rd_ptr + wr_ptr;
    } 
  } 

  return bytes_in_fifo;
}

// copies upto num_bytes from Id FIFO to dst 
// Take into account to use lock/unlock before/after the call
// Also, id has to be a valid entry in the pool of FIFOs
size_t copy_from_fifo_to_buf(unsigned int id, void *dst, size_t num_bytes)
{
  unsigned char *src = (unsigned char *)pool_fifos[id].data;
  register unsigned int rd_ptr = pool_fifos[id].rd_ptr;
  register unsigned int wr_ptr = pool_fifos[id].wr_ptr;
  register size_t bytes_in_fifo = get_num_bytes(id);
  register size_t bytes_read = num_bytes > bytes_in_fifo ? bytes_in_fifo : num_bytes; 
  if (wr_ptr > rd_ptr)
  {
    // in this case the copy is direct
    memcpy(dst, src + rd_ptr, bytes_read);
  }
  else
  {
    // two possibilites in this case
    register int turn_around = ((HN_BUFFER_FIFO_MAX_SIZE) - rd_ptr) - bytes_read;
    if (turn_around < 0)
    {
      // the number of bytes to read does turn around the end of the FIFO
      size_t len_chunk = (HN_BUFFER_FIFO_MAX_SIZE) - rd_ptr;
      memcpy(dst, src + rd_ptr, len_chunk); 
      size_t next_start = len_chunk;
      size_t second_len_chunk = bytes_read - len_chunk; 
      memcpy((unsigned char *)dst + next_start, src, second_len_chunk);
      log_verbose("hn_buffer_fifo: read operation for fifo %u turns around. bytes_to_read=%zu first_copy(len_chunk=%zu,rd_ptr=%u,buf_offset=0) second_copy(len_chunk=%zu,rd_ptr=0,buf_offset=%zu)", 
                 id, bytes_read, len_chunk, rd_ptr, second_len_chunk, next_start);
    }
    else
    {
      // the number of bytes to read does not turn around the end of the FIFO
      memcpy(dst, src + rd_ptr, bytes_read);
    }
  }

  return bytes_read;
}


const char *to_str_fifo_state(unsigned int id)
{
  switch (pool_fifos[id].state)
  {
    case HN_BUFFER_FIFO_STATE_EMPTY: return "empty";
    case HN_BUFFER_FIFO_STATE_FULL : return "full"; 
    case HN_BUFFER_FIFO_STATE_OK   : return "ok"; 
    default                        : return "UNK";
  }
}

// creates a buffer fifo which element size is equal to ele_size
// The function must have an only return point to the end
int hn_buffer_fifo_create(size_t ele_size) 
{
  int rs_mutex, rs_cond, rs_cond1, rs_mutex2, rs_cond2;
  
  pthread_mutex_lock(&pool_access_mutex);

  // checks for free indexes in the pool to allocate the Buffer FIFO
  int index = get_next_index_free();

  log_verbose("hn_buffer_fifo_create: got free index at %d", index);

  if (index > -1)
  {
    // initializes the mutex and the condition variable for the FIFO
    rs_mutex  = pthread_mutex_init(&pool_fifos_status[index].mutex_fifo, NULL);
    rs_cond   = pthread_cond_init(&pool_fifos_status[index].cond_fifo, NULL);
    rs_cond1  = pthread_cond_init(&pool_fifos_status[index].cond_op_active, NULL);
    rs_mutex2 = pthread_mutex_init(&pool_fifos_status[index].mutex_write_fifo, NULL);
    rs_cond2  = pthread_cond_init(&pool_fifos_status[index].cond_write_fifo, NULL);

    if ((rs_mutex == 0) && (rs_cond == 0) && (rs_cond1 == 0) && (rs_mutex2 == 0) && (rs_cond2 == 0)) 
    {
      // allocates FIFO in the pool, but only when we know any other intermediate steps are done correctly 
      pool_fifos[index].element_size = ele_size;
      pool_fifos[index].rd_ptr = 0;
      pool_fifos[index].wr_ptr = 0;
      pool_fifos[index].state  = HN_BUFFER_FIFO_STATE_EMPTY;
      pool_fifos_entry_busy[index] = 1; 

      // status info for the FIFO
      pool_fifos_status[index].num_operations_active = 0;
      pool_fifos_status[index].num_write_operations  = 0;
    } else {
      // something wrong in the initialization. Destroying resources
      pthread_mutex_destroy(&pool_fifos_status[index].mutex_fifo);
      pthread_cond_destroy(&pool_fifos_status[index].cond_fifo);
      pthread_cond_destroy(&pool_fifos_status[index].cond_op_active);
      pthread_mutex_destroy(&pool_fifos_status[index].mutex_write_fifo);
      pthread_cond_destroy(&pool_fifos_status[index].cond_write_fifo);
      index = -1;
    }
  }

  pthread_mutex_unlock(&pool_access_mutex);

  return index;
}

// frees resources occupied by a FIFO in the pool
int hn_buffer_fifo_destroy(unsigned int id)
{
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1))
  {
    // wait until any active operation in the fifo finish
    pthread_mutex_lock(&pool_access_mutex);
    
    pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);
    pool_fifos_entry_busy[id] = 0;
    pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
    pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);


    while (pool_fifos_status[id].num_operations_active > 0)
    {
      pthread_cond_wait(&pool_fifos_status[id].cond_op_active, &pool_access_mutex);
    }
    
    pool_fifos[id].element_size = 0;
    pool_fifos[id].rd_ptr = 0;
    pool_fifos[id].wr_ptr = 0;
    pool_fifos[id].state  = HN_BUFFER_FIFO_STATE_EMPTY;

    // now, we deallocate the resource

    /* 
     * RTG: I think we can destroy the condition variables and the mutex_fifo with no colateral issue 
     * because of the thread has the mutex and num_operation_active is 0 => no other thread is
     * in the middle of an operation
     */
    pthread_cond_destroy(&pool_fifos_status[id].cond_op_active);
    pthread_cond_destroy(&pool_fifos_status[id].cond_fifo);
    pthread_mutex_destroy(&pool_fifos_status[id].mutex_fifo);
    pthread_mutex_destroy(&pool_fifos_status[id].mutex_write_fifo);
    pthread_cond_destroy(&pool_fifos_status[id].cond_write_fifo);
    
    pool_fifos_status[id].num_operations_active = 0;  // redundant, but...
    pool_fifos_status[id].num_write_operations  = 0;

    pthread_mutex_unlock(&pool_access_mutex);

    return 0;
  }

  return -1;
}



// Gets the number of bytes in the buffer FIFO
size_t hn_buffer_fifo_get_num_bytes(unsigned int id)
{
  pthread_mutex_lock(&pool_access_mutex);

  // id out of the range or not allocated
  if ((id >= HN_BUFFER_FIFO_MAX_POOL_SIZE) || (pool_fifos_entry_busy[id] == 0))
  {
    pthread_mutex_unlock(&pool_access_mutex);
    return 0;
  }
  
  /*
   *  writes down the operation and unlock the access to the pool
   *  Were are now confident that this FIFO will not be deleted just in the middle of the operation
   */
  pool_fifos_status[id].num_operations_active++;
  pthread_mutex_unlock(&pool_access_mutex);

  // performs the operation
  pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);
  size_t bytes_in_fifo = get_num_bytes(id);
  pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);

  // decrement number of active operations
  pthread_mutex_lock(&pool_access_mutex);
  pool_fifos_status[id].num_operations_active--;
  pthread_cond_signal(&pool_fifos_status[id].cond_op_active);
  pthread_mutex_unlock(&pool_access_mutex);

  return bytes_in_fifo;
}

size_t hn_buffer_fifo_get_num_elements(unsigned int id)
{
  size_t num_ele = 0;


  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    size_t ele_size = pool_fifos[id].element_size;
    if (ele_size > 0) 
    {

      size_t bytes = hn_buffer_fifo_get_num_bytes(id);
      num_ele = bytes / ele_size;
    }

    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);

  }

  pthread_mutex_unlock(&pool_access_mutex);

  return num_ele;
}

// gets the element size of a FIFO
size_t hn_buffer_fifo_get_element_size(unsigned int id)
{
  size_t ele_size = 0;

  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    ele_size = pool_fifos[id].element_size;

    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);

  }

  pthread_mutex_unlock(&pool_access_mutex);

  return ele_size;

}

// gets a constant data pointer to an element of the FIFO
const void *hn_buffer_fifo_get_element(unsigned int id)
{
  void *res = NULL;

  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    // FIXME Is the lock actually needed?
    // First, lock access to the 'id' FIFO
    pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);

    if (pool_fifos[id].state != HN_BUFFER_FIFO_STATE_EMPTY) //(pool_fifos[id].rd_ptr != pool_fifos[id].wr_ptr)
    {
      res = (unsigned char *)pool_fifos[id].data + pool_fifos[id].rd_ptr;
    }

    // signal other thread that could be waiting for space in the FIFO to write data
    pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
    pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);

    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);

  }

  pthread_mutex_unlock(&pool_access_mutex);

  return res;
}


// Reads a number of elements from the FIFO. To do it, num_ele is converted to bytes
// Take into account that the FIFO buffer is circular.
size_t hn_buffer_fifo_read_elements_to_buf(unsigned int id, void *buf, size_t num_ele)
{
  size_t ele_read = 0;


  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1) && (buf != NULL) && (num_ele > 0))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    size_t ele_size = pool_fifos[id].element_size;
    if (ele_size > 0) 
    {
      // Aqui viene la juerga...
      //
      // FIXME Is the lock actually needed?
      // First, lock access to the 'id' FIFO
      pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);

      // Next, get fifo size in bytes & copy data to buf
      size_t bytes_in_fifo = get_num_bytes(id);
      size_t len = num_ele * ele_size;
      size_t num_ele_in_fifo = bytes_in_fifo / ele_size;
      if (num_ele_in_fifo > 0) 
      //if (bytes_in_fifo >= len)  
      {
 
        len = (bytes_in_fifo >= len) ? len : num_ele_in_fifo * ele_size;
        size_t bytes_read = copy_from_fifo_to_buf(id, buf, len);

        // update fifo state if needed
        if (bytes_in_fifo == bytes_read)
        {
          pool_fifos[id].state = HN_BUFFER_FIFO_STATE_EMPTY;
        }
        else
        {
          pool_fifos[id].state = HN_BUFFER_FIFO_STATE_OK;
        }

        log_verbose("hn_buffer_fifo: read %zu / %zu bytes from fifo %u into buf %p BEFORE_READING(rd_ptr=%zu, bytes_in_fifo=%zu) AFTER_READING(bytes_in_fifo=%zu, rd_ptr=%u fifo_state=%s)", 
          bytes_read, len, id, buf, pool_fifos[id].rd_ptr, bytes_in_fifo, 
          bytes_in_fifo - bytes_read, (pool_fifos[id].rd_ptr + bytes_read) % (HN_BUFFER_FIFO_MAX_SIZE), 
          to_str_fifo_state(id)); 

        // Update read pointer
        pool_fifos[id].rd_ptr += bytes_read;
        pool_fifos[id].rd_ptr %= (HN_BUFFER_FIFO_MAX_SIZE);
        ele_read = bytes_read/ele_size;
        assert(ele_read <= num_ele);
      }

      // signal other thread that could be waiting for space in the FIFO to write data
      pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
      pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);
    }

    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);
  }

  pthread_mutex_unlock(&pool_access_mutex);

  return ele_read;
}



// Reads a number of bytes from the FIFO.
// Take into account that the FIFO buffer is circular.
size_t hn_buffer_fifo_read_bytes_to_buf(unsigned int id, void *buf, size_t num_bytes)
{
  size_t bytes_read = 0;


  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated...
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1) && (buf != NULL) && (num_bytes > 0))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    // Aqui viene la juerga...
    //
    // FIXME Is the lock actually needed?
    // First, lock access to the 'id' FIFO
    pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);

    // Next, get fifo size in bytes & copy data to buf
    size_t bytes_in_fifo = get_num_bytes(id);
    size_t len = (bytes_in_fifo >= num_bytes) ? num_bytes : bytes_in_fifo;
    if (bytes_in_fifo > 0) //(wr_ptr != rd_ptr) 
    {
      bytes_read = copy_from_fifo_to_buf(id, buf, len);

      // update fifo state if needed
      if (bytes_in_fifo == bytes_read)
      {
        pool_fifos[id].state = HN_BUFFER_FIFO_STATE_EMPTY;
      }
      else
      {
        pool_fifos[id].state = HN_BUFFER_FIFO_STATE_OK;
      }

      log_verbose("hn_buffer_fifo: read %zu / %zu bytes from fifo %u into buf %p BEFORE_READING(rd_ptr=%zu, bytes_in_fifo=%zu) AFTER_READING(bytes_in_fifo=%zu, rd_ptr=%u fifo_state=%s)", 
          bytes_read, num_bytes, id, buf, pool_fifos[id].rd_ptr, bytes_in_fifo, 
          bytes_in_fifo - bytes_read, (pool_fifos[id].rd_ptr + bytes_read) % (HN_BUFFER_FIFO_MAX_SIZE), 
          to_str_fifo_state(id)); 

      // Update read pointer
      pool_fifos[id].rd_ptr += bytes_read;
      pool_fifos[id].rd_ptr %= (HN_BUFFER_FIFO_MAX_SIZE);
    }

    // signal other thread that could be waiting for space in the FIFO to write data
    pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
    pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);

    // signal other threads that can be waiting to destroy the FIFO.
    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);
  }
  
  pthread_mutex_unlock(&pool_access_mutex);
  
  return bytes_read;
}
  


// Reads elements from the Buffer FIFO with the given Id and writes them into the given buffer FIFO
size_t hn_buffer_fifo_read_elements_to_fifo(unsigned int id_src, unsigned int id_dst, size_t num_ele)
{
  // TODO
  return 0;
}

// Reads bytes from the Buffer FIFO with the given Id and writes them into the given buffer FIFO
size_t hn_buffer_fifo_read_bytes_to_fifo(unsigned int id_src, unsigned int id_dst, size_t num_bytes)
{
  // TODO
  return 0;
}

// Copies a number of elements from the FIFO. To do it, num_ele is converted to bytes
// Take into account that the FIFO buffer is circular.
size_t hn_buffer_fifo_copy_elements_to_buf(unsigned int id, void *buf, size_t num_ele)
{
  size_t ele_read = 0;

  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    size_t ele_size = pool_fifos[id].element_size;
    if (ele_size > 0) 
    {
      // Aqui viene la juerga...
      //
      // FIXME Is the lock actually needed?
      // First, lock access to the 'id' FIFO
      pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);

      // Next, get fifo size in bytes & copy data to buf
      size_t bytes_in_fifo = get_num_bytes(id);
      size_t len = num_ele * ele_size;
      size_t num_ele_in_fifo = bytes_in_fifo / ele_size;
      if (num_ele_in_fifo > 0) 
      //if (bytes_in_fifo >= len)  
      {
 
        len = (bytes_in_fifo >= len) ? len : num_ele_in_fifo * ele_size;
        size_t bytes_read = copy_from_fifo_to_buf(id, buf, len);

        ele_read = bytes_read/ele_size;
        assert(ele_read <= num_ele);
      }

      // signal other thread that could be waiting for space in the FIFO to write data
      pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
      pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);
    }

    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);
  }

  pthread_mutex_unlock(&pool_access_mutex);

  return ele_read;
}

// Copies a number of bytes from the FIFO.
// Take into account that the FIFO buffer is circular.
size_t hn_buffer_fifo_copy_bytes_to_buf(unsigned int id, void *buf, size_t num_bytes)
{
  size_t bytes_read = 0;


  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated...
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1) && (buf != NULL) && (num_bytes > 0))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    // Aqui viene la juerga...
    //
    // FIXME Is the lock actually needed?
    // First, lock access to the 'id' FIFO
    pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);

    // Next, get fifo size in bytes & copy data to buf
    size_t bytes_in_fifo = get_num_bytes(id);
    size_t len = num_bytes;
    if (bytes_in_fifo > 0)
    {
      bytes_read = copy_from_fifo_to_buf(id, buf, len);
    }

    // signal other thread that could be waiting for space in the FIFO to write data
    pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
    pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);

    // signal other threads that can be waiting to destroy the FIFO.
    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);
  }
  
  pthread_mutex_unlock(&pool_access_mutex);
  
  return bytes_read;
}
 
// Writes elements to the FIFO. To do it, num_ele is converted to bytes
// Take into account that the FIFO is circular
size_t hn_buffer_fifo_write_elements_from_buf(unsigned int id, void *buf, size_t num_ele)
{
  size_t ele_written = 0;

  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    size_t ele_size = pool_fifos[id].element_size;
    if (ele_size > 0) 
    {

      size_t bytes = hn_buffer_fifo_write_bytes_from_buf(id, buf, num_ele*ele_size);
      ele_written = bytes / ele_size;
    }

    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);
  }

  pthread_mutex_unlock(&pool_access_mutex);

  return ele_written;
}


// Writes bytes to the FIFO.
// Take into account that the FIFO is circular
// All the data is written, if not the thread will be lock to the point there is some space
size_t hn_buffer_fifo_write_bytes_from_buf(unsigned int id, void *buf, size_t num_bytes)
{
  size_t bytes_written = num_bytes;

  pthread_cleanup_push(clean_pool_access_mutex, &id);
  pthread_mutex_lock(&pool_access_mutex);

  // checks id out of the range or not allocated...
  if ((id < HN_BUFFER_FIFO_MAX_POOL_SIZE) && (pool_fifos_entry_busy[id] == 1) && (buf != NULL) && (num_bytes > 0))
  {
    pool_fifos_status[id].num_operations_active++;
    pthread_mutex_unlock(&pool_access_mutex);

    // Aqui viene la juerga...
    //

    // let's block other threads while there is another one writing something
    pthread_cleanup_push(clean_write_fifo_mutex, &id);
    pthread_mutex_lock(&pool_fifos_status[id].mutex_write_fifo);
    while (pool_fifos_status[id].num_write_operations > 0)
    {
      pthread_cond_wait(&pool_fifos_status[id].cond_write_fifo, &pool_fifos_status[id].mutex_write_fifo);

      // checks whether there is no thread connected to the FIFO. 
      // If this happens then it does not mind to write more on it
      if (pool_fifos_entry_busy[id] == 0) 
      { 
        // release the writing operation
        pthread_cond_signal(&pool_fifos_status[id].cond_write_fifo);
        pthread_mutex_unlock(&pool_fifos_status[id].mutex_write_fifo);

        // signal other threads that can be waiting to destroy the FIFO.
        pthread_mutex_lock(&pool_access_mutex);
        pool_fifos_status[id].num_operations_active--;
        pthread_cond_signal(&pool_fifos_status[id].cond_op_active); 
        pthread_mutex_unlock(&pool_access_mutex);

        return 0;
      }
     }
    pthread_cleanup_pop(0);

    pool_fifos_status[id].num_write_operations++;
    pthread_cond_signal(&pool_fifos_status[id].cond_write_fifo);
    pthread_mutex_unlock(&pool_fifos_status[id].mutex_write_fifo);

    // Now, we know there is not anyone writing, just to not merge things

    pthread_cleanup_push(clean_fifo_mutex, &id);
    size_t len = num_bytes;
    while (len > 0)
    { // there is some space in the buffer for writing
      pthread_mutex_lock(&pool_fifos_status[id].mutex_fifo);

      unsigned char *fifo_buffer = pool_fifos[id].data;
      size_t bytes_in_fifo       = get_num_bytes(id);
      size_t free_space          = (HN_BUFFER_FIFO_MAX_SIZE) - bytes_in_fifo;
      size_t bytes_to_write      = len > free_space ? free_space : len;

      while (free_space == 0)
      {
        /*
         * not enough space in the FIFO for writing any thing
         * so, we lock the thread in a cond_fifo condition variable
         * and we wait the reading function will awake the thread through
         * pthread_cond_signal() function
         */
        pthread_cond_wait(&pool_fifos_status[id].cond_fifo, &pool_fifos_status[id].mutex_fifo);

        bytes_in_fifo  = get_num_bytes(id);
        free_space     = (HN_BUFFER_FIFO_MAX_SIZE) - bytes_in_fifo;
        bytes_to_write = len > free_space ? free_space : len;

        // checks whether there is no thread connected to the FIFO. 
        // If this happens then it does not mind to write more on it
        if (pool_fifos_entry_busy[id] == 0) 
        {
          pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
          pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);

          // release the writing operation
          pthread_mutex_lock(&pool_fifos_status[id].mutex_write_fifo);
          pool_fifos_status[id].num_write_operations--;
          pthread_cond_signal(&pool_fifos_status[id].cond_write_fifo);
          pthread_mutex_unlock(&pool_fifos_status[id].mutex_write_fifo);

          // signal other threads that can be waiting to destroy the FIFO.
          pthread_mutex_lock(&pool_access_mutex);
          pool_fifos_status[id].num_operations_active--;
          pthread_cond_signal(&pool_fifos_status[id].cond_op_active); 
          pthread_mutex_unlock(&pool_access_mutex);

          return bytes_written - len;
        }
      }

      // checks whether there is no thread connected to the FIFO. 
      // If this happens then it does not mind to write more on it
      if (pool_fifos_entry_busy[id] == 0) 
      {
        // release the writing operation
        pthread_mutex_lock(&pool_fifos_status[id].mutex_write_fifo);
        pool_fifos_status[id].num_write_operations--;
        pthread_cond_signal(&pool_fifos_status[id].cond_write_fifo);
        pthread_mutex_unlock(&pool_fifos_status[id].mutex_write_fifo);

        // signal other threads that can be waiting to destroy the FIFO.
        pthread_mutex_lock(&pool_access_mutex);
        pool_fifos_status[id].num_operations_active--;
        pthread_cond_signal(&pool_fifos_status[id].cond_op_active); 
        pthread_mutex_unlock(&pool_access_mutex);

        return bytes_written - len;
      }

      // two cases
      register unsigned int wr_ptr = pool_fifos[id].wr_ptr;
      register int turn_around = ((HN_BUFFER_FIFO_MAX_SIZE) - wr_ptr) - bytes_to_write;
      if (turn_around < 0)
      {
        // the number of bytes to write does turn around the end of the FIFO
        size_t len_chunk = (HN_BUFFER_FIFO_MAX_SIZE) - wr_ptr;
        memcpy(fifo_buffer + wr_ptr, buf, len_chunk); 
        size_t next_start = len_chunk;
        size_t second_len_chunk = bytes_to_write - len_chunk; 
        memcpy(fifo_buffer, (void *)((unsigned char *)buf + next_start), second_len_chunk);
        log_verbose("hn_buffer_fifo: write operation into fifo %u turns around. bytes_to_copy=%zu first_copy(len_chunk=%zu,wr_ptr=%u,buf_offset=0) second_copy(len_chunk=%zu,wr_ptr=0,buf_offset=%zu)", 
            id, bytes_to_write, len_chunk, wr_ptr, second_len_chunk, next_start);
      }
      else
      {
        // the number of bytes to write does not turn around the end of the FIFO
        memcpy(fifo_buffer + wr_ptr, buf, bytes_to_write);
      }
     
      // update fifo state if needed
      if (bytes_to_write == free_space)
      {
        pool_fifos[id].state = HN_BUFFER_FIFO_STATE_FULL;
      } 
      else
      {
        pool_fifos[id].state = HN_BUFFER_FIFO_STATE_OK;
      }

      // update pointers
      pool_fifos[id].wr_ptr += bytes_to_write;
      pool_fifos[id].wr_ptr %= (HN_BUFFER_FIFO_MAX_SIZE);
      
      log_verbose("hn_buffer_fifo: written %zu / %zu bytes into the fifo %u from buf %p BEFORE_WRITING(free_space=%zu, bytes_in_fifo=%zu, wr_ptr=%u) AFTER_WRITING(free_space=%zu, bytes_in_fifo=%zu, wr_ptr=%u, fifo_state=%s) rest_to_write=%zu", 
          bytes_to_write, len, id, buf, 
          free_space, bytes_in_fifo, wr_ptr, 
          free_space - bytes_to_write, bytes_in_fifo + bytes_to_write, pool_fifos[id].wr_ptr, 
          to_str_fifo_state(id), len - bytes_to_write);

      buf = (unsigned char *)buf + bytes_to_write;
      len -= bytes_to_write;
 
      pthread_cond_signal(&pool_fifos_status[id].cond_fifo);
      pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);
    }
    pthread_cleanup_pop(0);
   
    // release the writing operation
    pthread_mutex_lock(&pool_fifos_status[id].mutex_write_fifo);
    pool_fifos_status[id].num_write_operations--;
    pthread_cond_signal(&pool_fifos_status[id].cond_write_fifo);
    pthread_mutex_unlock(&pool_fifos_status[id].mutex_write_fifo);
 
    // signal other threads that can be waiting to destroy the FIFO.
    pthread_mutex_lock(&pool_access_mutex);
    pool_fifos_status[id].num_operations_active--;
    pthread_cond_signal(&pool_fifos_status[id].cond_op_active);
  }
  
  pthread_mutex_unlock(&pool_access_mutex);
  pthread_cleanup_pop(0);
  
  return bytes_written;
}


// Writes elements from the the given Buffer FIFO to the Buffer FIFO with the given Id
size_t hn_buffer_fifo_write_elements_from_fifo(unsigned int id_dst, unsigned int id_src, size_t num_ele)
{
  // TODO
  return 0;
} 


// Writes bytes from the the given Buffer FIFO to the Buffer FIFO with the given Id
size_t hn_buffer_fifo_write_bytes_from_fifo(unsigned int id_dst, unsigned int id_src, size_t num_bytes)
{
  // TODO
  return 0;
} 


void clean_fifo_mutex(void *arg)
{
  unsigned int id = *(unsigned int *)arg;

  log_debug("hn_buffer_fifo: cleanup routine called for fifo %u on global fifo access", id);
  pool_fifos[id].state = HN_BUFFER_FIFO_STATE_EMPTY;
  pthread_cond_broadcast(&pool_fifos_status[id].cond_fifo);
  pthread_mutex_unlock(&pool_fifos_status[id].mutex_fifo);
}

void clean_write_fifo_mutex(void *arg)
{
  unsigned int id = *(unsigned int *)arg;

  log_debug("hn_buffer_fifo: cleanup routine called for fifo %u on write fifo access", id);
  pool_fifos_status[id].num_write_operations = 0;
  pthread_cond_broadcast(&pool_fifos_status[id].cond_write_fifo);
  pthread_mutex_unlock(&pool_fifos_status[id].mutex_write_fifo);
}

void clean_pool_access_mutex(void *arg)
{
  unsigned int id = *((unsigned int *)arg);

  log_debug("hn_buffer_fifo: cleanup routine called for fifo %u on pool access", id);
  pool_fifos_status[id].num_operations_active = 0;
  pthread_cond_broadcast(&pool_fifos_status[id].cond_op_active);
  pthread_mutex_unlock(&pool_access_mutex);
}
