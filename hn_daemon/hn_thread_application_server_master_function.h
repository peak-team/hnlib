#ifndef __HN_THREAD_APPLICATION_SERVER_MASTER_FUNCTION_H__
#define __HN_THREAD_APPLICATION_SERVER_MASTER_FUNCTION_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 26, 2018
// File Name: hn_thread_application_server_master_function.h
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Application server master thread.
//
//    This thread listen to the connection socket for the apps and creates the 
//    slave to communicate and work with them
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "hn_socket_unix.h"
#include "hn_socket_inet.h"

//! Number of maximum slave threads that can be managed
#define HN_THREAD_APPLICATION_SERVER_MAX_NUM_SLAVES   1024


/*!
 * \brief Arguments required by the application server master thread
 */
typedef struct hn_thread_application_server_master_args_st
{
  hn_socket_unix_info_t sock_un;
  hn_socket_inet_info_t sock_inet;
}hn_thread_application_server_master_args_t;


/*!
 * \brief Entry point for the application server master thread
 *
 * \param [in] args pointer to the thread args. It must be
 *             a pointer to the data type \see{#application_server_master_args_t}
 */
void *hn_thread_application_server_master_function(void *args);


#endif
