///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 23, 2018
// File Name: hn_exit_program.c
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Utility to know flag the termination of the program in an ordered way
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <signal.h>

// atomic variable to know if the program has terminated or not.
volatile static sig_atomic_t terminated_var = 0;

// status that the caller to terminate_program can set and can be queried with hn_exit_program_get_exit_status()
volatile static sig_atomic_t exit_status = 0;


// flags the termination of the program
void hn_exit_program_terminate(int status)
{
  terminated_var = 1;
  exit_status = status;
}

// checks the termination of the program. 
int hn_exit_program_is_terminated()
{
  return (terminated_var == 1);
}

// gets the exit status set when calling terminate_program() function
int hn_exit_program_get_exit_status()
{
  return exit_status;
}

// Install a termination program handler for a signal given its number
int hn_exit_program_install_handler(int signum)
{
#ifdef HDL_SIM
  return 0;
#else
  sigset_t set;
  struct sigaction act;

  // To block all possible signals when running the handler
  sigfillset(&set);

  // fill the action for the signal to use to terminate the program
  act.sa_handler = hn_exit_program_terminate;
  act.sa_mask = set;
  act.sa_flags = SA_ONESHOT;
  int res = sigaction(signum, &act, NULL);
  
  // the daemon will block all the signals, except for the elected for terminating the program
  sigdelset(&set, signum);
  sigdelset(&set, SIGUSR1);
  sigdelset(&set, SIGRTMIN);
  sigdelset(&set, SIGRTMIN+1);
  pthread_sigmask(SIG_BLOCK, &set, NULL);

  return res;
#endif
}
