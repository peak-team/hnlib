///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 29, 2018
// Design Name: 
// Module Name: thread backend send data to fpga 
// Project Name: HN daemon
// Target Devices:
// Tool Versions:
// Description:
//     Send outgoing data from applications queues to HN_System on FPGA.
//
//     This thread reads data from each fifo based on a selected policy to guaranty QOS (TO-DO: implement QOS arbiter),
//           currently reads in round robin manner.
//     Read data is propagated to the mmi64 library send function  in two different ways, depending on data transfer type
//           - regular items: read from items fifo(s)
//           - burst transfer data: is read from memory, trasnfer status is checked in order to send completion item notification
//             to the application which programmed the transfer when the app requested to be notified on transfer completion.
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

#include "hn_shared_structures.h"
#include "hn_exit_program.h"
#include "hn_iface_mmi64.h"
#include "hn_logging.h"
#include "hn_buffer_fifo.h"
#include "hn_list.h"
#include "hn_rr_arb.h"
#include "hn_thread_backend_send_data_to_fpga_function.h"

/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/
// this values will be stored in common, globals....
#define HN_ARBITER_POLICY_ROUND_ROBIN  (uint32_t)0
#define HN_ARBITER_POLICY_WEIGHTS      (uint32_t)1

/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/

static uint32_t arbiter_policy = HN_ARBITER_POLICY_ROUND_ROBIN; // Arbiter policy to read data from downstream queues

static pthread_mutex_t downstream_buffer_count_refresh_lock; // mutex signal for timestruct, .... necessary ? (check with Rafa)
static struct timespec downstream_buffer_count_refresh_ts;   // timestruct

static uint32_t number_of_downstream_modules  = 1;
static uint32_t number_of_downstream_channels = 1;  // number of communication channels in downstream. Each channel contains a fifo and is associated to one downstream mmi64 module

static pthread_mutex_t channel_send_lock; // mutex signal for timestruct, .... necessary ? (check with Rafa)
rr_arb_t channel_send_arb;

//static uint32_t arbiter_rr_current_index;

/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Local Functions implementation
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/

void hn_thread_backend_send_data_to_fpga_set_weights(uint32_t *weights, uint32_t num_weights)
{
  pthread_mutex_lock(&channel_send_lock);
  channel_send_arb = hn_rr_arb_create(shm_running_transfers_table.number_of_physical_channels, weights, num_weights);
  pthread_mutex_unlock(&channel_send_lock);
}

//*********************************************************************************************************************
//*********************************************************************************************************************
// values will initially only contain wheights when policy is set to WEIGHTS
//        array must be the same size as number of upstream interfaces
uint32_t hn_thread_backend_send_data_to_fpga_set_arbiter_policy (uint32_t policy, uint32_t *values) 
{

  // assignar la politica en forma de punters a funcio, per a no haver d'estar comprobant la politica cada volta
  //   donat que la politica no canviara constantment, es una comprobació que ens podrem estalviar....

  uint32_t ret_code;

  ret_code = 0;

  if (policy == HN_ARBITER_POLICY_ROUND_ROBIN)
  {
    arbiter_policy = HN_ARBITER_POLICY_ROUND_ROBIN;
    log_info("thread backend to fpga. arbiter policy set to Round_Robin");
  }
  else if (policy == HN_ARBITER_POLICY_WEIGHTS)
  {
    // check weights 
    //  values must sum 100%, and number of values must match number of upstreamif modules
    // ...number of upstreamifs cannot change while the daemon is running, otherwise the daemon can fail
    //    a function to update/refresh configuration is needed to support HW modifications while the daemon is running
    if (values == NULL)
    {
      log_error("BAD parameters attempting to set send to fpga arbiter policy, NULL pointer for weights vector");
      return 1; // error notifying
    }
    // update policy
    arbiter_policy = HN_ARBITER_POLICY_WEIGHTS;
    log_info("send_data_to_fpga thread arbiter policy set to Weights");
  } 
  else 
  {
    log_error("@hn_thread_backend_send_data_to_fpga_set_arbiter_policy, unknown policy: %u", policy);
    log_error("arbiter policy not updated");
    return 2;
  }


  return ret_code;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
uint32_t hn_thread_backend_send_data_to_fpga_set_buffer_count_refresh (struct timespec ts)
{
  log_debug("downstream_set_buffer_count_refresh entering function and waiting for mutex");
  pthread_mutex_lock(&downstream_buffer_count_refresh_lock);
  log_info("downstream_set_buffer_count_refresh, update value: %2lds %6ldns", ts.tv_sec, ts.tv_nsec);
  downstream_buffer_count_refresh_ts.tv_sec = ts.tv_sec;
  downstream_buffer_count_refresh_ts.tv_nsec = ts.tv_nsec;
  pthread_mutex_unlock(&downstream_buffer_count_refresh_lock);
  log_debug("downstream_set_buffer_count_refresh, done, mutex released");

  return 0;
}

//*********************************************************************************************************************
//*********************************************************************************************************************
// hi haura que pasarli politica d'arbitratge ....
/*
static uint32_t arbiter_rr_get_next_channel_index ( void )
{
  uint32_t next_index;

  //currently only generic items traffic, that come from upstream module index 0 in HN system
  next_index = (arbiter_rr_current_index + 1) % number_of_downstream_channels;
  arbiter_rr_current_index = next_index;

  log_debug("hn_thread_backend_send_data_to_fpga: arbiter_rr returns token for channel %2u", arbiter_rr_current_index);

  return next_index;
}
*/
//*********************************************************************************************************************
//*********************************************************************************************************************
/*!
 * \brief
 *
 * \param[in] channel channel id
 * \param[out] fifo_has_data pointer to variable to store whether fifo of channel contains data or is empty
 *
 */
/*uint32_t hn_downstream_channel_has_data_and_is_not_locked (int channel, uint32_t *fifo_has_data)
{
  uint32_t                 status;
  hn_data_burst_channel_t *downstream_channel;
  int                     fifo_id;
  size_t                  elements_in_fifo;

  status = 0;

  //fifo_id = (int *)hn_list_get_data_at_position(downstream_fifos_list, fifo_iter);
  downstream_channel = (hn_data_burst_channel_t *)hn_list_get_data_at_position(downstream_channel_list, channel);
  // check error, return 
  if(downstream_channel == NULL)
  {
    *fifo_has_data = 0;
    return -1;
  }
  
  fifo_id = downstream_channel->fifo;
  elements_in_fifo = hn_buffer_fifo_get_num_elements(fifo_id);

  if((elements_in_fifo > 0) && (downstream_channel->locked == 0))
  {
    *fifo_has_data = 1;
  }
  else
  {
    *fifo_has_data = 0;
  }
  
  //log_debug("fifo of channel %d is %s empty", channel, *fifo_has_data?"NOT":"   ");

  return status;
}
*/
//*********************************************************************************************************************
//*********************************************************************************************************************
/*!
 * \brief
 *
 * \param[in] channel channel id
 * \param[out] fifo_has_data pointer to variable to store whether fifo of channel contains data or is empty
 *
 */
/*
uint32_t hn_downstream_channel_has_data (int channel, uint32_t *fifo_has_data)
{
  uint32_t                 status;
  hn_data_burst_channel_t *downstream_channel;
  int                     fifo_id;
  size_t                  elements_in_fifo;

  status = 0;

  //fifo_id = (int *)hn_list_get_data_at_position(downstream_fifos_list, fifo_iter);
  downstream_channel = (hn_data_burst_channel_t *)hn_list_get_data_at_position(downstream_channel_list, channel);
  // check error, return 
  if(downstream_channel == NULL)
  {
    *fifo_has_data = 0;
    return -1;
  }
  
  fifo_id = downstream_channel->fifo;
  elements_in_fifo = hn_buffer_fifo_get_num_elements(fifo_id);

  if(elements_in_fifo > 0)
  {
    *fifo_has_data = 1;
  }
  else
  {
    *fifo_has_data = 0;
  }
  
  //log_debug("fifo of channel %d is %s empty", channel, *fifo_has_data?"NOT":"   ");

  return status;
}
*/
//*********************************************************************************************************************
//*********************************************************************************************************************
/*
uint32_t hn_downstream_buffer_get_data_count(uint32_t channel, uint32_t *fifo_count)
{
  uint32_t                 status;
  unsigned int             fifo_id;
  hn_data_burst_channel_t *downstream_channel;
  size_t                   elements_in_fifo;
  status = 0;

  downstream_channel = (hn_data_burst_channel_t *)hn_list_get_data_at_position(downstream_channel_list, channel);
  fifo_id = downstream_channel->fifo;
  elements_in_fifo = hn_buffer_fifo_get_num_elements(fifo_id);

  //log_debug("fifo of channel %d has %zu elements", channel, elements_in_fifo);

  *fifo_count = (uint32_t)elements_in_fifo;

  return status;
}
*/
//*********************************************************************************************************************
//*********************************************************************************************************************
// get data from fifo
// call fifo list, and copy data from fifo into buffer passed as parameter
// Currently copies ...... bytes (all bytes in fifo or tries to copy as many as received as param in transfer_size
/*
uint32_t hn_downstream_buffer_get_data_to_send_from_fifo (uint32_t channel, unsigned char *buff, uint32_t *transfer_size)
{
  uint32_t                 status;
  unsigned int             fifo_id;
  hn_data_burst_channel_t *downstream_channel;
  size_t                   read_bytes;
  
  downstream_channel = (hn_data_burst_channel_t *)hn_list_get_data_at_position(downstream_channel_list, channel);
  fifo_id = downstream_channel->fifo;

  read_bytes = hn_buffer_fifo_read_bytes_to_buf(fifo_id, buff, *transfer_size);
  *transfer_size = (uint32_t)read_bytes;

  log_debug("fifo of channel %d, associated to module 0x%08x has %zu bytes", channel, downstream_channel->module.id, read_bytes);
  status = 0;

  return status;

}
*/
// We also need to add "getters" and "setters" functions for this configuration settings
// Where will this information be stored?? in the main thread of the daemon? or here?
//*********************************************************************************************************************
//*********************************************************************************************************************
/*
uint32_t hn_thread_backend_downstream_module_for_fifo(uint32_t channel_id,uint32_t *downstream_module_index, hn_data_burst_channel_t **channel)
{
  // JM TO-DO set real functionality when we decide how this transfers will be managed
  uint32_t                 status;
   hn_data_burst_channel_t *downstream_channel;
  
  status = 0;
  downstream_channel = (hn_data_burst_channel_t *)hn_list_get_data_at_position(downstream_channel_list, channel_id);
  
  if((downstream_channel->module.type != GENERIC_ITEM_DOWNSTREAM) && (downstream_channel->module.type != BURST_TRANSFER_DOWNSTREAM))
  {
    log_error("Detected transfer of something that is not an item!");
    log_error("  downstream module index should be: %u", downstream_channel->module.index);
    return 1;
  }
  *downstream_module_index = downstream_channel->module.index;
  *channel = downstream_channel;
  log_debug("updating channel id to %u for channel id %u, downstream_channel: 0x%08p", channel_id, downstream_channel->module.index, downstream_channel->module.id);

  return status;
}
*/

// this function is called upon a thread cancelation
static void clean_backend_send_data_thread_mutexes(void *args) {
  pthread_mutex_unlock(&mutex_write_mmi64);
  pthread_mutex_unlock(&mutex_read_mmi64);
  log_verbose("backend_send_data thread: cleanup routine called for profpga interfaces mutex!");
}


//*********************************************************************************************************************
//*********************************************************************************************************************
void *hn_thread_backend_send_data_to_fpga_function ( void *argv )
{
  uint32_t status;
  uint32_t iface_is_initialized;
  static int retval = 0; 
  //uint32_t   number_of_channels_for_items;

  pthread_mutex_init(&downstream_buffer_count_refresh_lock, NULL);
  pthread_mutex_init(&channel_send_lock, NULL);

  pthread_mutex_lock(&downstream_buffer_count_refresh_lock);
  downstream_buffer_count_refresh_ts.tv_sec = 0;
  downstream_buffer_count_refresh_ts.tv_nsec = 200;
  pthread_mutex_unlock(&downstream_buffer_count_refresh_lock);

  status = hn_iface_mmi64_is_initialized(&iface_is_initialized);
  if(status)
  {
    log_error("FATAL checking status of mmi64 interface\n\n");
    // this will not happen, but in such a case, we should call hn_release....
    retval = -1;
    return &retval;
  }
  if(iface_is_initialized == 0)
  {
    log_error("@hn_thread_backend_get_data_from_fpga_function, mmi64 interface has not been initialized yet...");
    retval = -2;
    return &retval;
  }

  status = hn_get_number_of_downstream_modules (&number_of_downstream_modules);
  if (status != 0 )
  {
    log_debug("@hn_thread_backend_send_data_to_fpga_function, error getting number of downstream modules, err_code %u  num_modules %u",
        status, number_of_downstream_modules);
    retval = -3;
    return &retval;
  }

  //// PHYSICAL CHANNELS ARE SEQUENTIALLY CREATED, FIRST FOR ITEMS AND THEN FOR BURSTS
  //if (hn_get_number_of_io_modules_of_type(GENERIC_ITEM_DOWNSTREAM, &number_of_channels_for_items))
  //{
  //  log_debug("@hn_thread_backend_send_data_to_fpga_function: error getting number of downstream channels for items");
  //  retval = -4;
  //  return &retval;
  //}

  // set number of fifos containing data from HN applications, and where from this thread will get data to send to the HN system
  number_of_downstream_channels = number_of_downstream_modules;

  uint32_t         something_to_send;
  uint32_t         downstream_module_index;
  uint32_t         table_channel_index;
  uint32_t         table_channel_entry_index;
  uint32_t         ongoing_transfer_detected;
  uint32_t         transfer_size; // number of bytes to send through the io interface to the HN system
  unsigned char    buff[MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN * 4];     //buffer of downstream data to HN system, currently for items (words *4 bytes)
  size_t           elements_in_fifo;

  channel_send_arb = hn_rr_arb_create(shm_running_transfers_table.number_of_physical_channels, NULL, 0);
  rr_arb_t channel_assignment_arb = hn_rr_arb_create(shm_running_transfers_table.number_of_physical_channels, NULL, 0);

  // JM10 TO-DO add description to performed operations describing the thread flow

  // main code of the thread.
  // loop, until the daemon requests to terminate the thread
  // active polling status from downstream fifos and active transfers
  // check if there is something in the items fifo
  //    call send function
  // check for already initiated burst transfers
  // check for incoming burst commands in the burst command fifo
  //   check for free entry in table
  //     add command to table of current transfers


  pthread_cleanup_push(clean_backend_send_data_thread_mutexes, NULL);
  while (!hn_exit_program_is_terminated()) 
  {
    something_to_send = 0;

    //-------------------------------------------------------------------------
    // ITEMS FIFO
    // check items fifo, and send avail data
    elements_in_fifo = hn_buffer_fifo_get_num_elements(downstream_item_command_fifo);
    if (elements_in_fifo > 0)
    {
      log_debug("@backend thread send data, items fifo has %zu items to send\n", elements_in_fifo);
      something_to_send = 1;
      // get data, to send
      transfer_size =  hn_buffer_fifo_read_bytes_to_buf(downstream_item_command_fifo, &buff[0], sizeof(buff));
      // call send function 
      downstream_module_index = 0; // JM10, se que el inddex del modul de items es el 0....
      hn_iface_mmi64_send_items((char *)&buff[0], transfer_size, downstream_module_index);
    }

    //-------------------------------------------------------------------------
    // BURST-TRANSFERS -- RUNNING TRANSFERS
    // JM10 check burst data transfers table to continue ongoing transfers
    pthread_mutex_lock( &(shm_running_transfers_table.mutex) );
    // JM10, de moment tot va al channel 0, aixina que no cal fer arbitre ni altres comprobacions....
    ongoing_transfer_detected = 0;

    while (!rr_arb_round_complete(&channel_send_arb))
    {
      pthread_mutex_lock(&channel_send_lock);
      table_channel_index = rr_arb_next(&channel_send_arb);
      pthread_mutex_unlock(&channel_send_lock);

      if(shm_running_transfers_table.channel[table_channel_index].downstream_free_entries < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL)
      {
        // ongoing transfer detected, get entry to send
        for (table_channel_entry_index = 0; table_channel_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL; ) 
        {
          if(    (shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].free == 0) 
              && (shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].finished == 0) )
          {
            // this is the entry, entry is not free and transfer is not finished
            ongoing_transfer_detected = 1;
            log_debug("@backend thread send data, ongoing transfer detected at entry shm_running_transfers_table.channel[%2u].downstream_entries[%2u]", table_channel_index, table_channel_entry_index);

            //log_debug("JM10    stored cmd config for [0][0]type %u    dst tile %u    dst addr %08X   size %u bytes   local shm_buff_addr 0x%016x    blocking %u",
            //    shm_running_transfers_table.channel[table_channel_index].entry[0].config.type,
            //    shm_running_transfers_table.channel[table_channel_index].entry[0].config.tile,
            //    shm_running_transfers_table.channel[table_channel_index].entry[0].config.addr,
            //    shm_running_transfers_table.channel[table_channel_index].entry[0].config.size,
            //    shm_running_transfers_table.channel[table_channel_index].entry[0].config.shm_addr,
            //    shm_running_transfers_table.channel[table_channel_index].entry[0].config.blocking
            //    );
            break;
          }
          table_channel_entry_index++;
        }
        break;
      }
    }
    pthread_mutex_lock(&channel_send_lock);
    rr_arb_stop(&channel_send_arb);
    pthread_mutex_unlock(&channel_send_lock);

    pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );


    if (ongoing_transfer_detected != 0)
    {
      void      *shm_addr;
      uint32_t   num_bytes_to_send;
      uint32_t   mc_addr;

      something_to_send = 1;
      log_debug("@backend thread send data, processing shm_running_transfers_table.channel[%2u].downstream_entries[%2u]", table_channel_index, table_channel_entry_index);


      // channel is already assigned since the transfer is already running
      downstream_module_index = table_channel_index;

      // we already have the channel and the entry
      // so, we now call the transfer function with the appropiate parameters, and then update them

      // at this point we do not need to lock the mutex, since no other thread will be writing in this entry.....
      // this combination of channel-entry is already occupied, so the only acces point should be this and later the called send data function (not a thread)
      // BUT we take caution measures until the code is fully developed and tested
      // we can minimize the critical section of data that can be accessed and modified by the other thread in case of system malfunction, to only access that 
      // variables with the mutex locked
      log_debug("@backend thread send data, lock mutex to read transfer configuration");
      pthread_mutex_lock( &(shm_running_transfers_table.mutex) );
      log_debug("@backend thread send data, mutex locked");

      // I need to set current transfer address pointer and the amount of bytes to transfer,
      // the send function will add the burst header
      shm_addr          = shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.shm_addr;
      num_bytes_to_send = shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.size - shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].counter;
      mc_addr           = shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.addr;
     
      if(num_bytes_to_send > MAX_BYTES_PER_BURST_TRANSFER)
      {
        num_bytes_to_send = MAX_BYTES_PER_BURST_TRANSFER;
      }

      hn_iface_mmi64_send_data_burst_transfer(shm_addr, num_bytes_to_send, downstream_module_index, (shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config));

      // Update configuration of the transfers, check whether the burst is finished, and release table entry in case that the transfer does not require irq
      //  we need to update the size of remaining data to transfer, as well as the addresses of the shm buffer and the target addr on the MC
      shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.shm_addr = shm_addr + num_bytes_to_send;
      shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.addr     =  mc_addr + num_bytes_to_send;
      shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].counter += num_bytes_to_send;

      if(shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.size == shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].counter)
      {
        // all bytes for current transfer send, end of transmission
        shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].finished = 1;

        // release entry if this transfer was not expecting irq from system
        //if(shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.blocking == 0)
        //{
        //  shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].free = 1;
        //  shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].waiting_for_ack = 0;
        //  shm_running_transfers_table.channel[table_channel_index].downstream_free_entries = shm_running_transfers_table.channel[table_channel_index].downstream_free_entries + 1;
        //}
        //else
        //{
          // mark ack pending so we can now check at the collector which of the transfers is waiting for the irq notifying the end of the transfer on the hn side
          //shm_running_transfers_table.channel[table_channel_index].entry[table_channel_entry_index].free = 0;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].waiting_for_ack = 1;
          // do not increase the number of free entries yet
        //}
      }
      // release mutex
      pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );
      log_debug("@backend thread send data, mutex UN-locked");
    }

    //-------------------------------------------------------------------------
    // BURST-TRANSFERS COMMANDS FIFO
    // check burst data transfer commands fifo, check for free space in table (to-do add arbitration to select channel) and add new entry
    while (hn_buffer_fifo_get_num_elements(downstream_bursttransfer_command_fifo) > 0)
    {
      uint32_t                   downstream_free_entries = 0;
      uint32_t                   channel_locked;
      hn_shm_buffer_transfer_t   cmd; // burst_transfer_command

      something_to_send = 1;

      //log_debug("@backend thread send data -> burst command fifo has 1 command");

      while ((downstream_free_entries == 0 || channel_locked != 0) && !rr_arb_round_complete(&channel_assignment_arb)) {
        table_channel_index = rr_arb_next(&channel_assignment_arb);

        pthread_mutex_lock( &(shm_running_transfers_table.mutex) );
        downstream_free_entries   = shm_running_transfers_table.channel[table_channel_index].downstream_free_entries;
        // JM10, de moment nomes permet allotjar una rafega amb ack.....per a simplificar el codi...pero l'he de canviar fins que no modifique el HDL per tal de que no envien irq
        // per tal de no mexclar rafegyues que requerixquen irq amb rafegues que no...ja que no se d'on venen les irqs
        channel_locked = shm_running_transfers_table.channel[table_channel_index].locked_for_ack_transfers;
        pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );

        //if((downstream_free_entries == 0) || (channel_locked != 0)) printf("JM10, downstream_free_entries = %u  channel_locked = %u\n", downstream_free_entries, channel_locked);
      }
      //log_debug("@backend thread send data -> burst command fifo has 1 command ---- found channel");

      
      rr_arb_stop(&channel_assignment_arb);

      if((downstream_free_entries ) && (!channel_locked ))
      {
        hn_buffer_fifo_read_elements_to_buf(downstream_bursttransfer_command_fifo, &cmd, 1);

        // process all the commands and add the entries in the table, it could happen that new entries are released while processing the ccommands, but we do not take care of that
        //  the number of free entries can only grow, it cannot decrease beacause it can only happen in this function
        table_channel_entry_index = 0;
        pthread_mutex_lock( &(shm_running_transfers_table.mutex) );
        
        // WARNING this code could lead to endless loop if free entries are not properly handled

        log_debug("@hn_thread_backend_send_data_to_fpga_function, processing cmd type %u    dst tile %u    dst addr %08X   size %u bytes (%u words)    local shm_buff_addr 0x%016x    blocking %u",
            cmd.type, cmd.tile, cmd.addr, cmd.size, (cmd.size)/sizeof(uint64_t),
            cmd.shm_addr, cmd.blocking );
        
        while (!shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].free && (table_channel_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL))
        {
          //log_debug("JM10 searching free entry for new cmd, increasing table_channel_entry_index");
          table_channel_entry_index++;
        }

        if(table_channel_entry_index < MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL)
        {
          uint32_t roundup;
          //uint32_t burst_chunk_length = MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN
          uint32_t burst_chunk_length =  MAX_BYTES_PER_BURST_TRANSFER;

          log_debug("@hn_thread_backend_send_data_tofpga_function, setting burst transfer cmd configuration to shm_running_transfers_table.channel[%2u].downstream_entries[%2u]", table_channel_index, table_channel_entry_index);
          // we found a free entry, process shm command and store into 
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.burst_channel_index = table_channel_entry_index; // cmd[cmd_index]->burst_channel_index;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.client_id           = cmd.client_id;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.channel             = cmd.channel;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.type                = cmd.type;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.shm_addr            = cmd.shm_addr;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.tile                = cmd.tile;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.addr                = cmd.addr;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.size                = cmd.size;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.blocking            = cmd.blocking;

          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].free             = 0;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].finished         = 0;
          
          roundup = ((cmd.size  % burst_chunk_length) == 0) ? 0 : 1;
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].num_pending_acks = (cmd.size / burst_chunk_length) + roundup ; // ((num_bytes / bytes_per_word) / words per frame)
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].waiting_for_ack  = 1; // used to indicate whether a transfer is finished and waiting for an ack
          shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].counter  = 0; // used to indicate whether a transfer is finished and waiting for an ack

          //log_debug("@backend thread send data, registering transfer start time");
          clock_gettime(CLOCK_MONOTONIC, &shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].start_time);

          // reduce number of free entries for this channel
          shm_running_transfers_table.channel[table_channel_index].downstream_free_entries--;

          if (cmd.blocking)
          {
            shm_running_transfers_table.channel[table_channel_index].locked_for_ack_transfers = 1;
            //log_debug("@hn_thread_backend_send_data_to_fpga_function, shm_running_transfers_table.channel[%u].locked_for_ack_transfers = 1", table_channel_index);
          }

          log_debug("@hn_thread_backend_send_data_to_fpga_function, stored cmd config type %u    dst tile %u    dst addr %08X   size %u bytes   local shm_buff_addr 0x%016x    blocking %u splitted in %u small transfers (pending_acks if blocking)",
              shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.type,
              shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.tile,
              shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.addr,
              shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.size,
              shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.shm_addr,
              shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].config.blocking,
              shm_running_transfers_table.channel[table_channel_index].downstream_entries[table_channel_entry_index].num_pending_acks
              );
        }
        else
        {
          log_error("FATAL @hn_thread_backend_send_data_to_fpga_function, detected bad handling of free entries in shm burst transfers table.");
        }

        pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );
        //log_debug("@backend thread send data, mutex UN-locked");

      }
    }

    // if there is nothing to send, sleep to not stress the cpu unnecessarily
    if (something_to_send == 0)
    {
      // Nothing to send, sleeping to not saturate communications
      nanosleep(&downstream_buffer_count_refresh_ts, NULL);
    }

  } // end of main loop of the thread
  pthread_cleanup_pop(0);


  log_notice("@hn_tread_backend_send_data_to_fpga_function detected that daemon is stopping...");
  return &retval;
}

//********************************************************************************************************************* 
// end of file hn_thread_backend_send_data_to_fpga_function.c
//*********************************************************************************************************************

