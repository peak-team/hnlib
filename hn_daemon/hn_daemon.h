#ifndef __HN_DAEMON_H__
#define __HN_DAEMON_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_daemon.h
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node communication process daemon header
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>

#include "hn_filter.h"
#include "hn.h"

//! \name HN UNIX socket parameters
//@{

//! Enables UNIX socket in the daemon
#define HN_DAEMON_SOCKET_UNIX

//! Default socket unix port
#define HN_DAEMON_SOCKET_UNIX_PORT  "/tmp/hn_daemon_socket"

//@}


//! \name HN INET socket parameters
//@{

//! Enables INET sockets in the daemon
//#define HN_DAEMON_SOCKET_INET

//! Default socket INET port
#define HN_DAEMON_SOCKET_INET_PORT  7879

//@}


//! \name HN lib interchanged structures for commands
//@{

#define SHM_POSIX_NAME_MAX_LEN        64

#define HN_FPGA_PHYS_LOC_DESCRIPTOR_MAX_LEN 6

/*!
 * \brief Command enumeration 
 *
 * Here there are the commands that an HN-lib based application can send to daemon
 */
typedef enum 
{
  HN_COMMAND_SET_FILTER = 0,
  HN_COMMAND_RESET,
  HN_COMMAND_READ_REGISTER,
  HN_COMMAND_WRITE_REGISTER,
  HN_COMMAND_READ_MEMORY_WORD,
  HN_COMMAND_WRITE_MEMORY_WORD,
  HN_COMMAND_READ_MEMORY_HALF,
  HN_COMMAND_WRITE_MEMORY_HALF,
  HN_COMMAND_READ_MEMORY_BYTE,
  HN_COMMAND_WRITE_MEMORY_BYTE,
  HN_COMMAND_READ_MEMORY_BLOCK,
  HN_COMMAND_WRITE_MEMORY_BLOCK,
  HN_COMMAND_READ_SHARED_MEMORY_BUFFER,
  HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER,
  HN_COMMAND_CREATE_SHARED_MEMORY_BUFFER,
  HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER,
  HN_COMMAND_ENABLE_MEMORY_DEBUG,
  HN_COMMAND_DISABLE_MEMORY_DEBUG,
  HN_COMMAND_ENABLE_CORE_DEBUG,
  HN_COMMAND_DISABLE_CORE_DEBUG,
  HN_COMMAND_ENABLE_CACHE_CONTROLLER_DEBUG,
  HN_COMMAND_DISBALE_CACHE_CONTROLLER_DEBUG,
  HN_COMMAND_CONFIGURE_DEBUG_AND_STATS,
  HN_COMMAND_PEAK_WRITE_PROTOCOL,
  HN_COMMAND_PEAK_KEY_INTERRUPT,
  HN_COMMAND_PEAK_SET_PROCESSOR_ADDRESS,
  HN_COMMAND_PEAK_CONFIGURE_TILE,
  HN_COMMAND_PEAK_RESET,
  HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS,
  HN_COMMAND_PEAK_READ_REGISTER,
  HN_COMMAND_PEAK_WRITE_REGISTER,
  HN_COMMAND_REGISTER_INTS,
  HN_COMMAND_TRIGGER_INTS,
  HN_COMMAND_WAIT_INTS,
  HN_COMMAND_RELEASE_INTS,
  HN_COMMAND_REGISTER_DMA,
  HN_COMMAND_TRIGGER_DMA,
  HN_COMMAND_GET_STATUS_DMA,
  HN_COMMAND_RELEASE_DMA,
  HN_COMMAND_FIND_MEMORY_RSC,
  HN_COMMAND_FIND_MEMORIES_RSC,
  HN_COMMAND_ALLOCATE_MEMORY_RSC,
  HN_COMMAND_RELEASE_MEMORY_RSC,
  HN_COMMAND_SET_BACKEND_WEIGHTS,
  HN_COMMAND_GET_NETWORK_BANDWIDTH_RSC,
  HN_COMMAND_GET_READ_MEMORY_BANDWIDTH_RSC,
  HN_COMMAND_GET_WRITE_MEMORY_BANDWIDTH_RSC,
  HN_COMMAND_GET_READ_CLUSTER_BANDWIDTH_RSC,
  HN_COMMAND_GET_WRITE_CLUSTER_BANDWIDTH_RSC,
  HN_COMMAND_RESERVE_NETWORK_BANDWIDTH_RSC,
  HN_COMMAND_RESERVE_READ_MEMORY_BANDWIDTH_RSC,
  HN_COMMAND_RESERVE_WRITE_MEMORY_BANDWIDTH_RSC,
  HN_COMMAND_RESERVE_READ_CLUSTER_BANDWIDTH_RSC,
  HN_COMMAND_RESERVE_WRITE_CLUSTER_BANDWIDTH_RSC,
  HN_COMMAND_RELEASE_NETWORK_BANDWIDTH_RSC,
  HN_COMMAND_RELEASE_READ_MEMORY_BANDWIDTH_RSC,
  HN_COMMAND_RELEASE_WRITE_MEMORY_BANDWIDTH_RSC,
  HN_COMMAND_RELEASE_READ_CLUSTER_BANDWIDTH_RSC,
  HN_COMMAND_RELEASE_WRITE_CLUSTER_BANDWIDTH_RSC,
  HN_COMMAND_FIND_UNITS_SET_RSC,
  HN_COMMAND_FIND_UNITS_SETS_RSC,
  HN_COMMAND_RESERVE_UNITS_SET_RSC,
  HN_COMMAND_RELEASE_UNITS_SET_RSC,
  HN_COMMAND_GET_NUM_TILES_RSC,
  HN_COMMAND_GET_MEMORY_SIZE_RSC,
  HN_COMMAND_GET_NUMBER_OF_NETWORKS_RSC,
  HN_COMMAND_GET_TILE_INFO_RSC,
  HN_COMMAND_LOCK_RESOURCES_ACCESS_RSC,
  HN_COMMAND_UNLOCK_RESOURCES_ACCESS_RSC,
  HN_COMMAND_GET_TEMPERATURE_OF_FPGA,
  HN_COMMAND_GET_TEMPERATURE_OF_MOTHERBOARD,
  HN_COMMAND_CLOCK,
  HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_TILE,   // enable/        statistics automatic monitorization on collector thread for a given tile
  HN_COMMAND_CONFIGURE_STATS_MONITOR_UNSET_TILE, //       /disable statistics automatic monitorization on collector thread for a given tile
  HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_PERIOD, // set            statistics automatic monitorization on collector thread, polling interval for all tiles
  HN_COMMAND_READ_STATS_MONITOR,                 // get            statistics for a given tile, it has to previously be set
  // Add more just before this comment
  HN_COMMAND_END_LIST = 255
}hn_command_type_t;

typedef enum 
{
  HN_SHM_OPERATION_OK = 0,
  HN_SHM_OPERATION_ERROR
} hn_shm_operation_status_t;

typedef enum 
{
  HN_TEMPERATURE_OPERATION_OK = 0,
  HN_TEMPERATURE_OPERATION_ERROR
} hn_temperature_operation_status_t;


/*!
 * \brief Data common to all the commands
 */
typedef struct hn_command_common_st
{
  hn_command_type_t  command;   //!< The command 
}hn_command_common_t;
 
/*!
 * \brief Set filter command
 */
typedef struct hn_command_set_filter_st
{
  hn_command_common_t common;  //!< command
  uint32_t            tile;    //!< The tile the filter is going to set for
  uint32_t            core;    //!< The subtile the filter is going to set for
  uint32_t            target;  //!< Target filter
  uint32_t            mode;    //!< Mode of the filter
}hn_command_set_filter_t;

/*!
 * \brief Reset command
 */
typedef struct hn_command_reset_st
{
  hn_command_common_t common;   //!< The command 
  uint32_t            tile;     //!< The tile this command is for
}hn_command_reset_t;

/*!
 * \brief Read register command
 */
typedef struct hn_command_read_register_st
{
  hn_command_common_t common;   //!< see{#hn_command_common_t}
  uint32_t            tile;     //!< The tile this command is for
  uint16_t             reg;   //!< Tile Register to read
}hn_command_read_register_t;

/*!
 * \brief Write register command
 */
typedef struct hn_command_write_register_st
{
  hn_command_common_t common;   //!< see{#hn_command_common_t}
  uint32_t            tile;     //!< The tile this command is for
  uint16_t             reg;   //!< Tile Register to read
  uint32_t             data;  //!< Data to write
}hn_command_write_register_t;

/*!
 * \brief Write memory byte command
 */
typedef struct hn_command_write_memory_byte_st
{
  hn_command_common_t common;   //!< see{#hn_command_common_t}
  uint32_t            tile;     //!< The tile this command is for
  uint32_t             addr;  //!< Address to write
  uint8_t              data;  //!< Data to write
}hn_command_write_memory_byte_t;

/*!
 * \brief Write memory half command
 */
typedef struct hn_command_write_memory_half_st
{
  hn_command_common_t common;   //!< see{#hn_command_common_t}
  uint32_t            tile;     //!< The tile this command is for
  uint32_t             addr;  //!< Address to write
  uint16_t             data;  //!< Data to write
}hn_command_write_memory_half_t;

/*!
 * \brief Write memory word command
 */
typedef struct hn_command_write_memory_word_st
{
  hn_command_common_t common;   //!< see{#hn_command_common_t}
  uint32_t            tile;     //!< The tile this command is for
  uint32_t             addr;  //!< Address to write
  uint32_t             data; //!< Data to write
}hn_command_write_memory_word_t;

/*!
 * \brief Write memory block command
 */
typedef struct hn_command_write_memory_block_st
{
  hn_command_common_t common;    //!< see{#hn_command_common_t}
  uint32_t            tile;      //!< The tile this command is for
  uint32_t             addr;      //!< Address to write
  uint8_t              data[64];  //!< Data to write
}hn_command_write_memory_block_t;

/*!
 * \brief Read memory word command
 */
typedef struct hn_command_read_memory_word_st
{
  hn_command_common_t common;    //!< see{#hn_command_common_t}
  uint32_t            tile;      //!< The tile this command is for
  uint32_t             addr;      //!< Address to write
}hn_command_read_memory_word_t;

/*!
 * \brief Read memory block command
 */
typedef struct hn_command_read_memory_block_st
{
  hn_command_common_t common;    //!< see{#hn_command_common_t}
  uint32_t            tile;      //!< The tile this command is for
  uint32_t             addr;      //!< Address to write
}hn_command_read_memory_block_t;

/*!
 * \brief PEAK write protocol command
 */
typedef struct hn_command_peak_write_protocol_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for
  uint32_t             size;       //!< size
  uint8_t              data[8192]; //!< Data to write
}hn_command_peak_write_protocol_t;

/*!
 * \brief PEAK key interrupt command
 */
typedef struct hn_command_peak_key_interrupt_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for
  uint8_t              data;       //!< Key
}hn_command_peak_key_interrupt_t;

/*!
 * \brief PEAK set processor address command
 */
typedef struct hn_command_peak_set_processor_address_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for 
  uint32_t             addr;       //!< Address
}hn_command_peak_set_processor_address_t;

/*!
 * \brief PEAK configure tile command
 */
typedef struct hn_command_peak_configure_tile_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for
  uint32_t             conf;       //!< Configuration
}hn_command_peak_configure_tile_t;

/*!
 * \brief PEAK reset command
 */
typedef struct hn_command_peak_reset_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for 
}hn_command_peak_reset_t;

/*!
 * \brief PEAK configure debug and stats command
 */
typedef struct hn_command_peak_configure_debug_and_stats_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for
  uint32_t             conf;       //!< Configuration
}hn_command_peak_configure_debug_and_stats_t;

/*!
 * \brief PEAK read tilereg register command
 */
typedef struct hn_command_peak_read_register_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for
  uint32_t            reg;        //!< The register in tilereg to be read
}hn_command_peak_read_register_t;

/*!
 * \brief PEAK write tilereg register command
 */
typedef struct hn_command_peak_write_register_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for
  uint32_t            reg;        //!< The register in tilereg to be read
  uint32_t            value;      //!< The value to write in the register
}hn_command_peak_write_register_t;

/*!
 * \brief interrupts-related command
 */
typedef struct hn_command_int_st
{
  hn_command_common_t common;      //!< see{#hn_command_common_t}
  uint32_t            tile;        //!< The tile this command is for
  uint32_t            id;          //!< Interrupt identificator
  uint16_t            vector_mask; //!< Interrupt vector mask
}hn_command_int_t;


// this struct is conceived to pass data from the dispatcher to the hn_stats_monitor,
// its usage could be extended to the client applications to notify more info
// for stats monitor it is necessary because it sends all the reqs for the same tile at the same time
// the responses should arrive ordered, but in case of loss of any flit, it would be irrecorable
// this way we can overcome eventual flit loss
typedef struct hn_command_unit_read_register_response_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            tile;       //!< The tile this command is for
  uint32_t            subtile;    //!< The subtile inside the tile this command is for
  uint32_t            reg;        //!< The register in tilereg to be read
  uint32_t            value;      //!< The value contained in the register
}hn_command_unit_read_register_response_t;

/*!
 * \brief temperature management commands
 */
typedef struct hn_command_temperature_request_st
{
  hn_command_common_t  info;       //!< \see{#hn_command_common_t} 
  uint32_t             tile;       //!< requested temperature of this tile (fpga where this tile is located)
  unsigned long long   client_id;  //!< id of the client that requested the temperature
//  char                 mb[6];      //!< text string describing motherboard where tile is located
//  char                 fm[6];      //!< text string describing fpga module where tile is located
}hn_command_temperature_request_t;

typedef struct hn_command_temperature_response_st
{
  hn_command_common_t  info;       //!< \see{#hn_command_common_t} 
  uint32_t             tile;       //!< requested temperature of this tile (fpga where this tile is located)
  unsigned long long   client_id;  //!< id of the client that requested the temperature
  uint32_t             status;     //!< status of the request operation, ok or error
  int32_t              temperature;//!< temperature in celsius degrees of fpga where the requested tile is located
}hn_command_temperature_response_t;

/*!
 * \brief weights related command
 */
typedef struct hn_command_set_backend_weights_st {
  hn_command_common_t common;       //!< see{#hn_command_common_t}
  uint32_t            weights[100]; //!< The weights vector
  uint32_t            num_weights;  //!< Number of valid weights
} hn_command_set_backend_weights_t;

/*!
 * \brief burst transfer related command
 * management of the buffers, create and destroy buffers
 */
typedef struct hn_shm_buffer_info_st
{
  unsigned long long   client_id;  //!< id of the client owner of this buffer UNUSED 
  char                 name[SHM_POSIX_NAME_MAX_LEN];   //!< name of the buffer in shared memory to destroy
  uint32_t             size;       //!< size of the buffer in bytes
  void                *addr;       //!< pointer to base address of memory in shm area
  int                  fd;         //!< file descriptor, from shm_open()
}hn_shm_buffer_info_t;

typedef struct hn_command_shm_buffer_create_st
{
  hn_command_common_t  info;       //!< \see{#hn_command_common_t} 
  uint32_t             size;       //!< size of the buffer in bytes
}hn_command_shm_buffer_create_t;

typedef struct hn_command_shm_buffer_destroy_st
{
  hn_command_common_t  info;       //!< \see{#hn_command_common_t} 
  char                 name[SHM_POSIX_NAME_MAX_LEN];   //!< name of the buffer in shared memory to destroy
  uint32_t             size;       //!< size of the buffer to destroy
}hn_command_shm_buffer_destroy_t;

typedef struct hn_command_shm_buffer_transfer_st
{
  hn_command_common_t  info;       //!< \see{#hn_command_common_t}
  char                 name[SHM_POSIX_NAME_MAX_LEN];  //!< name of the created buffer
  uint32_t             offset;     //!< offset from base address of the buffer       OFFSET IN BYTES!!
  uint32_t             tile;       //!< tile where Memory is located in HN system
  uint32_t             addr;       //!< memory address of memory in HN system        TRANSLATED to (char *)
  uint32_t             size;       //!< number of bytes to transfer
  uint32_t             blocking;   //!< function blocks until shm operation is completed
}hn_command_shm_buffer_transfer_t;

typedef struct hn_command_shm_buffer_manage_status_st
{
  hn_command_common_t  info;       //!< \see{#hn_command_common_t} 
  uint32_t             status;     //!< status of the creation operation. 0 if buffer succesfully created
  uint32_t             client_id;  //!< client associated with the request/reply
  char                 name[SHM_POSIX_NAME_MAX_LEN];   //!< name of the new buffer in shared memory
}hn_command_shm_buffer_manage_status_t;


/*!
 * \brief dma-related command
 */
typedef struct hn_command_dma_st
{
  hn_command_common_t common;     //!< see{#hn_command_common_t}
  uint32_t            id;         //!< transaction id
  uint32_t            tile_src;   //!< source tile
  uint32_t            addr_src;   //!< source address
  uint32_t            size;       //!< size of the transaction
  uint32_t            tile_dst;   //!< destination tile
  uint32_t            addr_dst;   //!< destination address
  uint32_t            to_unit;    //!< Flag whether the DMA is to a unit
  uint32_t            to_mem;     //!< Flag whether the DMA is to a memory
  uint32_t            to_ext;     //!< Flag whether the DMA is to an IO device
  uint32_t            notify;     //!< Whether required notification upon ending 
}hn_command_dma_t;

/*!
 * \brief resource manager related command
 */
typedef struct hn_command_rsc_st
{
  hn_command_common_t common;   //!< see{#hn_command_common_t}
  uint32_t            tile;     //!< The tile this command is for (when the other types of tiles do not apply)
  uint32_t            tile_mem; //!< Target tile for related memory requests
  uint32_t            tile_src; //!< Source tile for BW reservation requests or getting tile info 
  uint32_t            tile_dst; //!< Destination tile for BW reservation requests
  uint32_t            bw;       //!< BW to reserve 
  uint32_t            addr;     //!< Memory address to allocacte 
  uint32_t            size;     //!< Size of the memory to allocate
  uint32_t            num_tiles;  //!< Number of tiles for commands related to requesting tile sets
}hn_command_rsc_t;

/*!
 * \brief Return codes for resource manager request
 */
typedef enum {
  HN_RSC_NOTFOUND = 0,
  HN_RSC_FOUND,
  HN_RSC_OPERATION_FAILED,
  HN_RSC_OPERATION_SUCCES,
  HN_RSC_INT_RECEIVED,
  HN_RSC_INT_ERROR,
  HN_RSC_DMA_ERROR,
  HN_RSC_OK,
  HN_RSC_DATA,
  HN_RSC_DMA_FINISHED
} hn_rsc_notification_t;

/*!
 * \brief Resource manager response structure
 *
 * This is a global response structure that contains
 * all the fields for every possible request. Therefore,
 * the useful fields depend on the request
 */
typedef struct hn_data_rsc_st
{
  hn_rsc_notification_t result;          //!< Result code of the operation
  uint32_t              client_id;       //!< Client associated with this data
  uint32_t              num_memories;    //!< Number of memories found that satisfy the request
  uint32_t              tile_mem;        //!< TODO
  uint32_t              tile;            //!< TODO
  uint32_t              type;            //!< Type (family) of the tile (PEAK, NUPLUS, DCT, TETRAPOD...)
  uint32_t              subtype;         //!< The config implemented in the unit inside the tile (PEAK_0...)
  uint32_t              starting_addr;   //!< TODO
  uint32_t              bw;              //!< TODO
  uint32_t              num_tiles;       //!< Number of tiles in the implemented architecture
  uint32_t              num_tiles_x;     //!< Number of tiles in the X-dimension of the mesh
  uint32_t              num_tiles_y;     //!< Number of tiles in the Y-dimension of the mesh
  uint32_t              num_sets;        //!< Number of sets found that satisfy the request
  uint32_t              mem_size;        //!< Size of the memory requested
  uint32_t              num_vns;         //!< Number of networks in the implemented architecture 
  uint32_t              id;              //!< Interrupt id
}hn_data_rsc_t;

/*!
 * \brief debug enable/disable command structure
 */
typedef struct hn_command_configure_debug_and_stats_st
{
  hn_command_common_t common;            //<! common structure shared by all commands \see{#hn_command_common_t}
  uint32_t            tile;              //<! tile this command is for
  uint32_t            config_function;   //<! function to configure in the tile
}hn_command_configure_debug_and_stats_t;

/*!
 * \brief clock command structure
 */
typedef struct hn_command_clock_st
{
  hn_command_common_t common;            //<! common structure shared by all commands \see{#hn_command_common_t}
  uint32_t            tile;              //<! tile this command is for
  uint32_t            clock_function;    //<! function to set the system FPGA clock
  uint32_t            num_cycles;        //<! value used for some clock functions 
}hn_command_clock_t;

/*!
 *
 */
//typedef enum
//{
//  HN_STATS_MONITOR_DISABLE = 0,
//  HN_STATS_MONITOR_ENABLE,
//  HN_STATS_MONITOR_SET_PERIOD,
//} hn_stats_monitor_operation_type_t;
typedef enum 
{
  HN_STATS_MONITOR_OPERATION_OK = 0,
  HN_STATS_MONITOR_OPERATION_ERROR_OUT_OF_RANGE,
  HN_STATS_MONITOR_OPERATION_ERROR_EMPTY_TILE,
  HN_STATS_MONITOR_OPERATION_ERROR
} hn_stats_monitor_operation_status_t;
typedef struct hn_command_stats_monitor_st
{
  hn_command_common_t   common;
  uint32_t              tile;
  //uint32_t              core;      // for read stats only, in case we would like to ask stats for a single core
  uint32_t              client_id;
  uint32_t              polling_period;
} hn_command_stats_monitor_t;

typedef struct hn_command_stats_monitor_response_st
{
  hn_command_common_t  common;          //<! common structure shared by all commands \see{#hn_command_common_t}
  uint32_t             tile;            //<! tile this stats belong to
  uint32_t             num_cores;       //<! number of cores in the tile, will return 1 config response + num_cores data responses
  uint32_t             client_id;       //<! id of the client to return the stats
  uint32_t             polling_period;  //<! polling period set after a request to set this value
  uint32_t             status;          //<! status of the respone, ok or error_type
//uint32_t             core_type        //<! will return in the config response, indicates type of cores (type of data struct ) that will return in the following stats responses  
  hn_stats_monitor_t   core_stats;      //<! struct containing the stats for current core \see{#hn_stats_monitor_t}
} hn_command_stats_monitor_response_t;




//@}

#endif
