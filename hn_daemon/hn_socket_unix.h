#ifndef __HN_SOCKET_UNIX_H__
#define __HN_SOCKET_UNIX_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_socket_unix.h
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Hetoregeneous Node support for unix socket type process communication
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>

/*! 
 * \brief Socket info struct for a unix socket. 
 *
 * The socket is composed of a unix address and a socket descriptor fields
 */
typedef struct hn_socket_unix_info_st
{
  struct sockaddr_un addr;  //!< unix socket address. \sa { unix(7) man page }
  int                sock;  //!< Socket descriptor
}hn_socket_unix_info_t;



//! \name Initialization and shutdown
///@{
//
/*!
 * \brief Initializes a unix socket
 *
 * \param [in] port Server port
 * \param [out] si_un Pointer to a socket info data type initialized
 * \retval  0 In case of success
 * \retval -1 In case of failure. system variable errno contains the error code
 */
int hn_socket_unix_initialize(const char *port, hn_socket_unix_info_t* si_un);

/*!
 * \brief Closes an open unix socket
 *
 * \param si_un pointer to a socket unix info data type
 * \retval 1 always
 */
int hn_socket_unix_close(hn_socket_unix_info_t *si_un);

///@}

#endif
