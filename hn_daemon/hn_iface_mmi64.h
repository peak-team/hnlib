///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 19, 2018
// Design Name: 
// Module Name: iface mmi64 
// Project Name: HN daemon
// Target Devices:
// Tool Versions:
// Description:
//   Handle communications interface in the host side.
//   Open communications interface with proFPGA devices, there are two possibilites depending on the utilized HW device
//     - mmi64 module ( mmi64 fm pcie module )
//     - profpga      ( motherboard )
//   Close communications interface
//   Manage calls to profpga mmi64 library to send and read data to/from HN system
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments:
//   WARNING: when hn_profpga_handle is in use, in case that the interface is not properly closed ithis could block 
//            the communications interface of proFPGA tools with the MotherBoards requiryng to reboot of the Motherboard
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __HN_IFACE_MMI64_H__
#define __HN_IFACE_MMI64_H__

#ifdef __cplusplus
extern "C" {
#endif


/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdint.h>

#include "hn_shared_structures.h"



/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/
#define MMI64_DOWNSTREAM_ITEMS_MAX_MMI64_PAYLOAD_LEN   512  // for regular items downstrm modules

// max burst transfer for mmi64 is 65280 as stated in prodesign AN017
//  BUT the value has to be properly set to allow at least one transfer on flight from the fifo full signal rises.
//  Fifo depth is 16384 words, full threshold rises at 10000 and gets low at 6000 to be sure that 
//  at least two tranfers could be on flight while the avail signal reaches the mmi64 flow control logic
#define MMI64_DOWNSTREAM_BURSTTRANSFER_MAX_MMI64_PAYLOAD_LEN   4096 // NUMBER of words in frame, for burst transfers

#define FIFO_TYPE_UPSTREAM   0
#define FIFO_TYPE_DOWNSTREAM 1
/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Public function prototypes
 **
 **********************************************************************************************************************
 **/

/*! 
 * \brief Initialize communications interface with HN device via mmi64 interface.
 *
 *  Initializes communications device passed as parameter in iface_name. Utilizes mmi64 library.
 *
 * \param[in] iface_name name of mmi64 interface to utilize for communications with HN system
 * \return 0 if interface is successfully initialized, otherwise return an error code.
 */
uint32_t hn_iface_mmi64_init(const char* iface_name);

/*! 
 * \brief Release local resources and close communications interface with HN device.
 *
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 *  function may not return if mmi64 interface is in deadlock and does not respond to close() function call
 */
uint32_t hn_iface_mmi64_release (void);

/*! 
 * \brief check whether communications interface has been initialized.
 *
 * \param[out] initialized pointer to variable to store whether the interface has already been intialized (1) or not (0)
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 *  function may not return if mmi64 interface is in deadlock and does not respond to close() function call
 */
uint32_t hn_iface_mmi64_is_initialized(uint32_t *initialized);

/*! 
 * \brief get temperature of device.
 *
 * \param[in] mb motherboard id
 * \param[out] temp temperaturte of motherboard
 * \return 0 if information was succesfully acquired, otherwise return an error code.
 */
uint32_t  hn_iface_mmi64_get_device_temperature(char *mb_desc, char *fm_desc, int *temperature);

/*! 
 * \brief Get number of modules of type passed as parameter in HN system 
 *
 * \param[in]  type of module to get number of buffers
 * \param[out] pointer to variable to store the number of modules of the requested type
 * \return 0 if system is initialized, otherwise return error code
 */
uint32_t hn_get_number_of_io_modules_of_type(uint32_t module_type, uint32_t *num_modules);

/*! 
 * \brief Get total number of upstream modules in HN system 
 *
 * \param[out] pointer to variable to store the number of modules of the requested type
 * \return 0 if system is initialized, otherwise return error code
 */
uint32_t hn_get_number_of_upstream_modules(uint32_t *num_modules);

/*! 
 * \brief Get total number of downstream modules in HN system 
 *
 * \param[out] pointer to variable to store the number of modules of the requested type
 * \return 0 if system is initialized, otherwise return error code
 */
uint32_t hn_get_number_of_downstream_modules(uint32_t *num_modules);

/*! 
 * \brief Get the type of upstream module (items or data bursts)
 *
 * \param[in]  upstream_module_index index of upstream module buffer
 * \param[out] module_type pointer to variable to store the module type
 * \return 0 if the module is found, otherwise return error code
 */
uint32_t hn_iface_mmi64_get_upstream_module_type(uint32_t upstream_module_index, hn_mmi64_module_type_t *module_type);

/*!
 * \brief Check whether upstream buffer is empty
 *
 * \param[in]  upstream_module_index index of upstream module buffer
 * \param[out] has_data status of the buffer ( set to 0 if empty, otherwise set to 1)
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 */
uint32_t hn_iface_mmi64_upstream_buffer_is_not_empty(uint32_t upstream_module_index, uint32_t *has_data);

/*! 
 * \brief Get the VN associated to the given upstream data burst module
 *
 * \param[in]  upstream_module_index index of upstream module buffer
 * \param[out] VN id
 * \return 0 if the upstream module is found, otherwise return error code
 */
uint32_t hn_iface_mmi64_get_vn_id_for_fifo(uint32_t is_for_upstream_module, uint32_t upstream_module_index, uint32_t *vn_id);
/*!
 * \brief send data through downstream module from pc application to module in HN system (32bit words)
 *
 * \param[in] addr pointer first address of buffer to transfer
 * \param[in] len_bytes  number of bytes to transfer
 * \param[in] module_index
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 */
uint32_t hn_iface_mmi64_send_items( void *addr, uint32_t len_bytes, uint32_t module_index);

/*! 
 * \brief get data of buffer associated to upstream module in HN system (32bit words)
 *
 * \param[in] addr pointer first address of buffer where data will be stored
 * \param[in] num_words  number of words to read  (1 word = 1 item)
 * \param[in] module_type
 * \param[in] module_index 
 * \return 0 if close operations are succesfully performed, otherwise return an error code.
 */
uint32_t hn_iface_mmi64_get_data( char *addr, uint32_t *num_bytes,uint32_t module_index);

//*****************************************************************************
/*! 
 * \brief send data through downstream module from pc application to module in HN system (64bit words)
 *
 * \param[in] addr pointer first address of buffer to transfer
 * \param[in] len_bytes  number of bytes to transfer
 * \param[in] module_type
 * \param[in] module_index
 * \param[in] config
 * \return 0 if close operations are succesfully performed, otherwise return an error code
 */
uint32_t hn_iface_mmi64_send_data_burst_transfer( void *addr, uint32_t len_bytes, uint32_t module_index, hn_shm_buffer_transfer_t config);


#ifdef __cplusplus
}
#endif

#endif

//*********************************************************************************************************************
// end of file iface_mmi64.h
//*********************************************************************************************************************

