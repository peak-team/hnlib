///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 29, 2018
// File Name: hn_item_handler.c
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//   Handler for the items
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <string.h>

#include "hn_daemon.h"
#include "hn_logging.h"
#include "hn_item_handler.h"


// Internal module item handler for upstream items dictionary data type
typedef struct hn_item_upstream_handler_dict_st
{
  hn_item_upstream_handler_t handle;
}hn_item_upstream_handler_dict_t;
//------------------------------------------------------------------------------------


// Internal module item handler for downstream items dictionary data type
typedef struct hn_item_downstream_handler_dict_st
{
  hn_item_downstream_handler_t handle;
}hn_item_downstream_handler_dict_t;
//-------------------------------------------------------------------------------------

// default upstream handler for read register items
static size_t hn_item_upstream_handler_default_for_item_command_read_register(hn_item_t *item, void *buf);

//--------------------------------------------------------------------------------------------------------------

// default downstream item handler for read register items
static size_t hn_item_downstream_handler_default_for_item_command_read_register(hn_item_t *item_vector, 
               size_t item_vector_size, unsigned int tile, unsigned int subtile, void *buf, size_t buf_size);

// default downstream item handler for reset command
static size_t hn_item_downstream_handler_default_for_item_command_reset(hn_item_t *item_vector, 
               size_t item_vector_size, unsigned int tile, unsigned int subtile, void *buf, size_t buf_size);

//---------------------------------------------------------------------------------------------------------------

// Defaults handlers. Used when no external registration of handlers occurs for a command
static hn_item_upstream_handler_dict_t DEFAULT_ITEM_UPSTREAM_HANDLER[HN_ITEM_UPSTREAM_COMMAND_END_LIST] =
{
  [HN_ITEM_UPSTREAM_COMMAND_READ_REGISTER_FIRST] = { hn_item_upstream_handler_default_for_item_command_read_register },
  [HN_ITEM_UPSTREAM_COMMAND_READ_REGISTER_REMAINING] = { hn_item_upstream_handler_default_for_item_command_read_register }
};


// dictionary of handlers
static  hn_item_upstream_handler_dict_t item_upstream_handler[HN_ITEM_UPSTREAM_COMMAND_END_LIST] =
{
  [HN_ITEM_UPSTREAM_COMMAND_READ_REGISTER_FIRST] = { hn_item_upstream_handler_default_for_item_command_read_register },
  [HN_ITEM_UPSTREAM_COMMAND_READ_REGISTER_REMAINING] = { hn_item_upstream_handler_default_for_item_command_read_register }
};
//---------------------------------------------------------------------------------------------------

// Defaults handlers. Used when no external registration of handlers occurs for a command
static hn_item_downstream_handler_dict_t DEFAULT_ITEM_DOWNSTREAM_HANDLER[HN_ITEM_DOWNSTREAM_COMMAND_END_LIST] =
{
  [HN_ITEM_DOWNSTREAM_COMMAND_RESET] = { hn_item_downstream_handler_default_for_item_command_reset },
  [HN_ITEM_DOWNSTREAM_COMMAND_READ_REGISTER] = { hn_item_downstream_handler_default_for_item_command_read_register },
};


// dictionary of handlers
static  hn_item_downstream_handler_dict_t item_downstream_handler[HN_ITEM_DOWNSTREAM_COMMAND_END_LIST] =
{
  [HN_ITEM_DOWNSTREAM_COMMAND_RESET] = { hn_item_downstream_handler_default_for_item_command_reset },
  [HN_ITEM_DOWNSTREAM_COMMAND_READ_REGISTER] = { hn_item_downstream_handler_default_for_item_command_read_register },
};
//---------------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------
// Registration/Deregistration of handlers
//----------------------------------------------------------------------------------------------------

// registers an upstream item handler for a command type
int hn_item_upstream_register_handler(hn_item_upstream_command_type_t type, hn_item_upstream_handler_t handle)
{
  item_upstream_handler[type].handle = handle;
  return 0;
}

// deregisters an upstream item handler for a command type
int hn_item_upstream_deregister_handler(hn_item_upstream_command_type_t type)
{
  item_upstream_handler[type].handle = DEFAULT_ITEM_UPSTREAM_HANDLER[type].handle;
  return 0;
}

// registers a downstream item handler for a command type
int hn_item_downstream_register_handler(hn_item_downstream_command_type_t type, hn_item_downstream_handler_t handle)
{
  item_downstream_handler[type].handle = handle;
  return 0;
}

// deregisters a dowstream item handler for a command type
int hn_item_downstream_deregister_handler(hn_item_downstream_command_type_t type)
{
  item_downstream_handler[type].handle = DEFAULT_ITEM_DOWNSTREAM_HANDLER[type].handle;
  return 0;
}
//---------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------
// Default extractors handlers
//----------------------------------------------------------------------------------------------------

// handles the incoming items
size_t hn_item_upstream_handle(hn_item_t *item_buf, size_t num_items, void *buf, size_t buf_size)
{
  size_t item_size = sizeof(hn_item_t);
  size_t num_items_processed = 0;
  size_t current_item_index = 0;
  size_t buf_offset = 0;
  size_t bytes_returned = 0;
  if ((item_buf != NULL) && (buf != NULL))
  {
    while ((current_item_index < num_items) && ((buf_offset+item_size) <= buf_size))
    {
      hn_item_t *item = &(item_buf[current_item_index]);  
      if ((item != NULL) && (item->command < HN_ITEM_UPSTREAM_COMMAND_END_LIST) && (item_upstream_handler[item->command].handle != NULL))
      {
        size_t bytes = item_upstream_handler[item->command].handle(item, ((unsigned char *)buf + buf_offset));
        buf_offset += bytes;
        num_items_processed++;
        bytes_returned += bytes;
      }
      else 
      { // the item could not be handled, it should not happen
        log_warn("hn_item_upstream_handle: there is no handler for item %08X with command %u", 
            *((uint32_t *)item), item->command);
      }
      current_item_index++;
    }
  }

  return bytes_returned;
}


// default handler for the read register item
size_t hn_item_upstream_handler_default_for_item_command_read_register(hn_item_t *item, void *buf)
{
  static uint8_t reg[6];
  static uint8_t reg_offset = 0;
  size_t res = 0;

  if (reg_offset < 6)
  {
    reg[reg_offset++] = item->payload;
    res = 0;
  }

  if (reg_offset == 6)
  {
    *((uint32_t *)buf) = *((uint32_t *)reg);
    reg_offset = 0;
    res = 4;
  }

  log_debug("hn_item_upstream_handler: read register for item %08X (tile %u cmd %u payload %02X (%u))", 
     *((uint32_t *)item), item->tile, item->command, item->payload, item->payload);

  return res;
}
//-------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------
// Default generators handlers
//----------------------------------------------------------------------------------------------------

// handles the incoming items
size_t hn_item_downstream_handle(hn_item_t *item_buf, size_t item_buf_size, unsigned int tile,
        unsigned int subtile, hn_item_downstream_command_type_t cmd, void *buf, size_t buf_size)
{
  size_t num_items_gen = 0;
  if ((item_buf != NULL) && (buf != NULL))
  {
    if ((cmd < HN_ITEM_DOWNSTREAM_COMMAND_END_LIST) && (item_downstream_handler[cmd].handle != NULL))
    {
      num_items_gen += item_downstream_handler[cmd].handle(item_buf, item_buf_size, tile, subtile, buf, buf_size);
    } 
    else
    {
      log_error("hn_item_dowstream_handler: item for command %u could not be generated", cmd);
    }
  }

  return num_items_gen;
}

// default handler for the reset item
size_t hn_item_downstream_handler_default_for_item_command_reset(hn_item_t *item_vector, 
        size_t item_vector_size, unsigned int tile, unsigned int subtile, void *buf, size_t buf_size)
{
  const size_t num_items_to_gen = 1;
  
  if (item_vector_size >= num_items_to_gen)
  {
    item_vector[0].tile    = tile;
    item_vector[0].command = COMMAND_RESET_SYSTEM;
    item_vector[0].payload = 0;
    item_vector[0].padding = 0;
  } 
  else
  {
    return 0;
  }

  return num_items_to_gen;
}

// default handler for the read register item
size_t hn_item_downstream_handler_default_for_item_command_read_register(hn_item_t *item_vector, 
        size_t item_vector_size, unsigned int tile, unsigned int subtile, void *buf, size_t buf_size)
{
  int i;
  const size_t num_items_to_gen = 2;
  
  if ((item_vector_size >= num_items_to_gen) && (buf_size >= num_items_to_gen))
  {
    size_t offset = 0;
    for (i = 0; i < num_items_to_gen; i++) 
    {
      item_vector[i].tile    = tile;
      item_vector[i].command = COMMAND_READ_TILEREG;
 
      // takes into account little endian in case of buf is of type uintX_t
      item_vector[i].payload = *((unsigned char *)buf + offset);
      item_vector[i].padding = 0;
      offset += 1;
    }
  } 
  else
  {
    return 0;
  }

  return num_items_to_gen;
}

// default handler for the write register item
size_t hn_item_downstream_handler_default_for_item_command_write_register(hn_item_t *item_vector, 
        size_t item_vector_size, unsigned int tile, unsigned int subtile, void *buf, size_t buf_size)
{
  int i;
  const size_t num_items_to_gen = 6;
  
  if ((item_vector_size >= num_items_to_gen) && (buf_size >= num_items_to_gen))
  {
    size_t offset = 0;
    for (i = 0; i < num_items_to_gen; i++) 
    {
      item_vector[i].tile    = tile;
      item_vector[i].command = COMMAND_WRITE_TILEREG;
 
      // takes into account little endian in case of buf is of type uintX_t
      item_vector[i].payload = *((unsigned char *)buf + offset);
      item_vector[i].padding = 0;
      offset += 1;
    }
  } 
  else
  {
    return 0;
  }

  return num_items_to_gen;
}

