///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 29, 2018
// File Name: hn_request_handler.c
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//   Handler for the requests comming from the application clients
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <string.h>

#include "hn_daemon.h"
#include "hn_logging.h"
#include "hn_request_handler.h"

// Internal module request handler dictionary data type
typedef struct hn_request_handler_dict_st
{
  hn_request_handler_t handle;
}hn_request_handler_dict_t;



/////////////////////////////////////
// Private Prototype definition
/////////////////////////////////////

static size_t hn_request_handler_default_for_command_reset(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_set_filter(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_read_register(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_write_register(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_write_memory_byte(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_write_memory_half(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_write_memory_word(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_write_memory_block(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_read_memory_word(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_read_memory_block(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_create_shared_memory_buffer(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_destroy_shared_memory_buffer(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_transfer_shared_memory_buffer(hn_request_t *req, void *buf, size_t buf_size);  //!< read data from  DDR memory in HN system, write data to  shared memory buffer
static size_t hn_request_handler_default_for_command_peak_write_protocol(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_peak_key_interrupt(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_peak_set_processor_address(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_peak_configure_tile(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_peak_reset(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_peak_configure_debug_and_stats(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_peak_read_register(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_peak_write_register(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_register_ints(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_trigger_ints(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_wait_ints(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_ints(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_register_dma(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_trigger_dma(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_get_status_dma(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_dma(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_find_memory_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_find_memories_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_allocate_memory_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_memory_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_set_backend_weights(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_get_network_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_get_read_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_get_write_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_get_read_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_get_write_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_reserve_network_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_reserve_read_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_reserve_write_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_reserve_read_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_reserve_write_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_network_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_read_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_write_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_read_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_release_write_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_find_units_set_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_find_units_sets_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_reserve_units_set_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_release_units_set_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_get_num_tiles_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_get_num_vns_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_get_memory_size_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_get_tile_info_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_lock_resources_access_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_unlock_resources_access_rsc(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_get_temperature_of_device(hn_request_t *req, void *buf, size_t buf_size);
static size_t hn_request_handler_default_for_command_configure_debug_and_stats(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_clock(hn_request_t *req, void *buf, size_t buf_size); 
static size_t hn_request_handler_default_for_command_stats_monitor(hn_request_t *req, void *buf, size_t buf_size);

////////////////////////////////////////
// Local variable definition
////////////////////////////////////////

// Defaults handlers. Used when no external registration of handlers occurs for a command
static hn_request_handler_dict_t DEFAULT_HANDLER[HN_COMMAND_END_LIST] =
{
  [HN_COMMAND_SET_FILTER]                             = {hn_request_handler_default_for_command_set_filter},
  [HN_COMMAND_RESET]                                  = {hn_request_handler_default_for_command_reset},
  [HN_COMMAND_READ_REGISTER]                          = {hn_request_handler_default_for_command_read_register},
  [HN_COMMAND_WRITE_REGISTER]                         = {hn_request_handler_default_for_command_write_register},
  [HN_COMMAND_WRITE_MEMORY_BYTE]                      = {hn_request_handler_default_for_command_write_memory_byte},
  [HN_COMMAND_WRITE_MEMORY_HALF]                      = {hn_request_handler_default_for_command_write_memory_half},
  [HN_COMMAND_WRITE_MEMORY_WORD]                      = {hn_request_handler_default_for_command_write_memory_word},
  [HN_COMMAND_WRITE_MEMORY_BLOCK]                     = {hn_request_handler_default_for_command_write_memory_block},
  [HN_COMMAND_READ_MEMORY_WORD]                       = {hn_request_handler_default_for_command_read_memory_word},
  [HN_COMMAND_READ_MEMORY_BLOCK]                      = {hn_request_handler_default_for_command_read_memory_block},
  [HN_COMMAND_CREATE_SHARED_MEMORY_BUFFER]            = {hn_request_handler_default_for_command_create_shared_memory_buffer},
  [HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER]           = {hn_request_handler_default_for_command_destroy_shared_memory_buffer},
  [HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER]             = {hn_request_handler_default_for_command_transfer_shared_memory_buffer},
  [HN_COMMAND_READ_SHARED_MEMORY_BUFFER]              = {hn_request_handler_default_for_command_transfer_shared_memory_buffer},
  [HN_COMMAND_PEAK_WRITE_PROTOCOL]                    = {hn_request_handler_default_for_command_peak_write_protocol},
  [HN_COMMAND_PEAK_KEY_INTERRUPT]                     = {hn_request_handler_default_for_command_peak_key_interrupt},
  [HN_COMMAND_PEAK_SET_PROCESSOR_ADDRESS]             = {hn_request_handler_default_for_command_peak_set_processor_address},
  [HN_COMMAND_PEAK_CONFIGURE_TILE]                    = {hn_request_handler_default_for_command_peak_configure_tile},
  [HN_COMMAND_PEAK_RESET]                             = {hn_request_handler_default_for_command_peak_reset},
  [HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS]         = {hn_request_handler_default_for_command_peak_configure_debug_and_stats},
  [HN_COMMAND_PEAK_READ_REGISTER]                     = {hn_request_handler_default_for_command_peak_read_register},
  [HN_COMMAND_PEAK_WRITE_REGISTER]                    = {hn_request_handler_default_for_command_peak_write_register},
  [HN_COMMAND_REGISTER_INTS]                          = {hn_request_handler_default_for_command_register_ints},
  [HN_COMMAND_TRIGGER_INTS]                           = {hn_request_handler_default_for_command_trigger_ints},
  [HN_COMMAND_WAIT_INTS]                              = {hn_request_handler_default_for_command_wait_ints},
  [HN_COMMAND_RELEASE_INTS]                           = {hn_request_handler_default_for_command_release_ints},
  [HN_COMMAND_REGISTER_DMA]                           = {hn_request_handler_default_for_command_register_dma},
  [HN_COMMAND_TRIGGER_DMA]                            = {hn_request_handler_default_for_command_trigger_dma},
  [HN_COMMAND_GET_STATUS_DMA]                         = {hn_request_handler_default_for_command_get_status_dma},
  [HN_COMMAND_RELEASE_DMA]                            = {hn_request_handler_default_for_command_release_dma},
  [HN_COMMAND_FIND_MEMORY_RSC]                        = {hn_request_handler_default_for_command_find_memory_rsc},
  [HN_COMMAND_FIND_MEMORIES_RSC]                      = {hn_request_handler_default_for_command_find_memories_rsc}, 
  [HN_COMMAND_ALLOCATE_MEMORY_RSC]                    = {hn_request_handler_default_for_command_allocate_memory_rsc},
  [HN_COMMAND_RELEASE_MEMORY_RSC]                     = {hn_request_handler_default_for_command_release_memory_rsc},
  [HN_COMMAND_SET_BACKEND_WEIGHTS]                    = {hn_request_handler_default_for_command_set_backend_weights},
  [HN_COMMAND_GET_NETWORK_BANDWIDTH_RSC]              = {hn_request_handler_default_for_command_get_network_bandwidth_rsc},
  [HN_COMMAND_GET_READ_MEMORY_BANDWIDTH_RSC]          = {hn_request_handler_default_for_command_get_read_memory_bandwidth_rsc},
  [HN_COMMAND_GET_WRITE_MEMORY_BANDWIDTH_RSC]         = {hn_request_handler_default_for_command_get_write_memory_bandwidth_rsc},
  [HN_COMMAND_GET_READ_CLUSTER_BANDWIDTH_RSC]         = {hn_request_handler_default_for_command_get_read_cluster_bandwidth_rsc},
  [HN_COMMAND_GET_WRITE_CLUSTER_BANDWIDTH_RSC]        = {hn_request_handler_default_for_command_get_write_cluster_bandwidth_rsc},
  [HN_COMMAND_RESERVE_NETWORK_BANDWIDTH_RSC]          = {hn_request_handler_default_for_command_reserve_network_bandwidth_rsc},
  [HN_COMMAND_RESERVE_READ_MEMORY_BANDWIDTH_RSC]      = {hn_request_handler_default_for_command_reserve_read_memory_bandwidth_rsc},
  [HN_COMMAND_RESERVE_WRITE_MEMORY_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_reserve_write_memory_bandwidth_rsc},
  [HN_COMMAND_RESERVE_READ_CLUSTER_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_reserve_read_cluster_bandwidth_rsc},
  [HN_COMMAND_RESERVE_WRITE_CLUSTER_BANDWIDTH_RSC]    = {hn_request_handler_default_for_command_reserve_write_cluster_bandwidth_rsc},
  [HN_COMMAND_RELEASE_NETWORK_BANDWIDTH_RSC]          = {hn_request_handler_default_for_command_release_network_bandwidth_rsc},
  [HN_COMMAND_RELEASE_READ_MEMORY_BANDWIDTH_RSC]      = {hn_request_handler_default_for_command_release_read_memory_bandwidth_rsc},
  [HN_COMMAND_RELEASE_WRITE_MEMORY_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_release_write_memory_bandwidth_rsc},
  [HN_COMMAND_RELEASE_READ_CLUSTER_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_release_read_cluster_bandwidth_rsc},
  [HN_COMMAND_RELEASE_WRITE_CLUSTER_BANDWIDTH_RSC]    = {hn_request_handler_default_for_command_release_write_cluster_bandwidth_rsc},
  [HN_COMMAND_FIND_UNITS_SET_RSC]                     = {hn_request_handler_default_for_command_find_units_set_rsc},
  [HN_COMMAND_FIND_UNITS_SETS_RSC]                    = {hn_request_handler_default_for_command_find_units_sets_rsc},
  [HN_COMMAND_RESERVE_UNITS_SET_RSC]                  = {hn_request_handler_default_for_command_reserve_units_set_rsc},
  [HN_COMMAND_RELEASE_UNITS_SET_RSC]                  = {hn_request_handler_default_for_command_release_units_set_rsc},
  [HN_COMMAND_GET_NUM_TILES_RSC]                      = {hn_request_handler_default_for_command_get_num_tiles_rsc},
  [HN_COMMAND_GET_NUMBER_OF_NETWORKS_RSC]             = {hn_request_handler_default_for_command_get_num_vns_rsc},
  [HN_COMMAND_GET_MEMORY_SIZE_RSC]                    = {hn_request_handler_default_for_command_get_memory_size_rsc},
  [HN_COMMAND_GET_TILE_INFO_RSC]                      = {hn_request_handler_default_for_command_get_tile_info_rsc},
  [HN_COMMAND_LOCK_RESOURCES_ACCESS_RSC]              = {hn_request_handler_default_for_command_lock_resources_access_rsc},
  [HN_COMMAND_UNLOCK_RESOURCES_ACCESS_RSC]            = {hn_request_handler_default_for_command_unlock_resources_access_rsc},
  [HN_COMMAND_GET_TEMPERATURE_OF_FPGA]                = {hn_request_handler_default_for_command_get_temperature_of_device},
  [HN_COMMAND_GET_TEMPERATURE_OF_MOTHERBOARD]         = {hn_request_handler_default_for_command_get_temperature_of_device},
  [HN_COMMAND_CONFIGURE_DEBUG_AND_STATS]              = {hn_request_handler_default_for_command_configure_debug_and_stats},
  [HN_COMMAND_CLOCK]                                  = {hn_request_handler_default_for_command_clock},
  [HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_TILE]       = {hn_request_handler_default_for_command_stats_monitor},
  [HN_COMMAND_CONFIGURE_STATS_MONITOR_UNSET_TILE]     = {hn_request_handler_default_for_command_stats_monitor},
  [HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_PERIOD]     = {hn_request_handler_default_for_command_stats_monitor},
  [HN_COMMAND_READ_STATS_MONITOR]                     = {hn_request_handler_default_for_command_stats_monitor}

};


// dictionary of handlers
static hn_request_handler_dict_t request_handler[HN_COMMAND_END_LIST] =
{
  [HN_COMMAND_SET_FILTER]                             = {hn_request_handler_default_for_command_set_filter},
  [HN_COMMAND_RESET]                                  = {hn_request_handler_default_for_command_reset},
  [HN_COMMAND_READ_REGISTER]                          = {hn_request_handler_default_for_command_read_register},
  [HN_COMMAND_WRITE_REGISTER]                         = {hn_request_handler_default_for_command_write_register},
  [HN_COMMAND_WRITE_MEMORY_BYTE]                      = {hn_request_handler_default_for_command_write_memory_byte},
  [HN_COMMAND_WRITE_MEMORY_HALF]                      = {hn_request_handler_default_for_command_write_memory_half},
  [HN_COMMAND_WRITE_MEMORY_WORD]                      = {hn_request_handler_default_for_command_write_memory_word},
  [HN_COMMAND_WRITE_MEMORY_BLOCK]                     = {hn_request_handler_default_for_command_write_memory_block},
  [HN_COMMAND_READ_MEMORY_WORD]                       = {hn_request_handler_default_for_command_read_memory_word},
  [HN_COMMAND_READ_MEMORY_BLOCK]                      = {hn_request_handler_default_for_command_read_memory_block},
  [HN_COMMAND_CREATE_SHARED_MEMORY_BUFFER]            = {hn_request_handler_default_for_command_create_shared_memory_buffer},
  [HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER]           = {hn_request_handler_default_for_command_destroy_shared_memory_buffer},
  [HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER]             = {hn_request_handler_default_for_command_transfer_shared_memory_buffer},
  [HN_COMMAND_READ_SHARED_MEMORY_BUFFER]              = {hn_request_handler_default_for_command_transfer_shared_memory_buffer},
  [HN_COMMAND_PEAK_WRITE_PROTOCOL]                    = {hn_request_handler_default_for_command_peak_write_protocol},
  [HN_COMMAND_PEAK_KEY_INTERRUPT]                     = {hn_request_handler_default_for_command_peak_key_interrupt},
  [HN_COMMAND_PEAK_SET_PROCESSOR_ADDRESS]             = {hn_request_handler_default_for_command_peak_set_processor_address},
  [HN_COMMAND_PEAK_CONFIGURE_TILE]                    = {hn_request_handler_default_for_command_peak_configure_tile},
  [HN_COMMAND_PEAK_RESET]                             = {hn_request_handler_default_for_command_peak_reset},
  [HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS]         = {hn_request_handler_default_for_command_peak_configure_debug_and_stats},
  [HN_COMMAND_PEAK_READ_REGISTER]                     = {hn_request_handler_default_for_command_peak_read_register},
  [HN_COMMAND_PEAK_WRITE_REGISTER]                    = {hn_request_handler_default_for_command_peak_write_register},
  [HN_COMMAND_REGISTER_INTS]                          = {hn_request_handler_default_for_command_register_ints},
  [HN_COMMAND_TRIGGER_INTS]                           = {hn_request_handler_default_for_command_trigger_ints},
  [HN_COMMAND_WAIT_INTS]                              = {hn_request_handler_default_for_command_wait_ints},
  [HN_COMMAND_RELEASE_INTS]                           = {hn_request_handler_default_for_command_release_ints},
  [HN_COMMAND_REGISTER_DMA]                           = {hn_request_handler_default_for_command_register_dma},
  [HN_COMMAND_TRIGGER_DMA]                            = {hn_request_handler_default_for_command_trigger_dma},
  [HN_COMMAND_GET_STATUS_DMA]                         = {hn_request_handler_default_for_command_get_status_dma},
  [HN_COMMAND_RELEASE_DMA]                            = {hn_request_handler_default_for_command_release_dma},
  [HN_COMMAND_FIND_MEMORY_RSC]                        = {hn_request_handler_default_for_command_find_memory_rsc},
  [HN_COMMAND_FIND_MEMORIES_RSC]                      = {hn_request_handler_default_for_command_find_memories_rsc}, 
  [HN_COMMAND_ALLOCATE_MEMORY_RSC]                    = {hn_request_handler_default_for_command_allocate_memory_rsc},
  [HN_COMMAND_RELEASE_MEMORY_RSC]                     = {hn_request_handler_default_for_command_release_memory_rsc},
  [HN_COMMAND_SET_BACKEND_WEIGHTS]                    = {hn_request_handler_default_for_command_set_backend_weights},
  [HN_COMMAND_GET_NETWORK_BANDWIDTH_RSC]              = {hn_request_handler_default_for_command_get_network_bandwidth_rsc},
  [HN_COMMAND_GET_READ_MEMORY_BANDWIDTH_RSC]          = {hn_request_handler_default_for_command_get_read_memory_bandwidth_rsc},
  [HN_COMMAND_GET_WRITE_MEMORY_BANDWIDTH_RSC]         = {hn_request_handler_default_for_command_get_write_memory_bandwidth_rsc},
  [HN_COMMAND_GET_READ_CLUSTER_BANDWIDTH_RSC]         = {hn_request_handler_default_for_command_get_read_cluster_bandwidth_rsc},
  [HN_COMMAND_GET_WRITE_CLUSTER_BANDWIDTH_RSC]        = {hn_request_handler_default_for_command_get_write_cluster_bandwidth_rsc},
  [HN_COMMAND_RESERVE_NETWORK_BANDWIDTH_RSC]          = {hn_request_handler_default_for_command_reserve_network_bandwidth_rsc},
  [HN_COMMAND_RESERVE_READ_MEMORY_BANDWIDTH_RSC]      = {hn_request_handler_default_for_command_reserve_read_memory_bandwidth_rsc},
  [HN_COMMAND_RESERVE_WRITE_MEMORY_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_reserve_write_memory_bandwidth_rsc},
  [HN_COMMAND_RESERVE_READ_CLUSTER_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_reserve_read_cluster_bandwidth_rsc},
  [HN_COMMAND_RESERVE_WRITE_CLUSTER_BANDWIDTH_RSC]    = {hn_request_handler_default_for_command_reserve_write_cluster_bandwidth_rsc},
  [HN_COMMAND_RELEASE_NETWORK_BANDWIDTH_RSC]          = {hn_request_handler_default_for_command_release_network_bandwidth_rsc},
  [HN_COMMAND_RELEASE_READ_MEMORY_BANDWIDTH_RSC]      = {hn_request_handler_default_for_command_release_read_memory_bandwidth_rsc},
  [HN_COMMAND_RELEASE_WRITE_MEMORY_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_release_write_memory_bandwidth_rsc},
  [HN_COMMAND_RELEASE_READ_CLUSTER_BANDWIDTH_RSC]     = {hn_request_handler_default_for_command_release_read_cluster_bandwidth_rsc},
  [HN_COMMAND_RELEASE_WRITE_CLUSTER_BANDWIDTH_RSC]    = {hn_request_handler_default_for_command_release_write_cluster_bandwidth_rsc},
  [HN_COMMAND_FIND_UNITS_SET_RSC]                     = {hn_request_handler_default_for_command_find_units_set_rsc},
  [HN_COMMAND_FIND_UNITS_SETS_RSC]                    = {hn_request_handler_default_for_command_find_units_sets_rsc},
  [HN_COMMAND_RESERVE_UNITS_SET_RSC]                  = {hn_request_handler_default_for_command_reserve_units_set_rsc},
  [HN_COMMAND_RELEASE_UNITS_SET_RSC]                  = {hn_request_handler_default_for_command_release_units_set_rsc},
  [HN_COMMAND_GET_NUM_TILES_RSC]                      = {hn_request_handler_default_for_command_get_num_tiles_rsc},
  [HN_COMMAND_GET_NUMBER_OF_NETWORKS_RSC]             = {hn_request_handler_default_for_command_get_num_vns_rsc},
  [HN_COMMAND_GET_MEMORY_SIZE_RSC]                    = {hn_request_handler_default_for_command_get_memory_size_rsc},
  [HN_COMMAND_GET_TILE_INFO_RSC]                      = {hn_request_handler_default_for_command_get_tile_info_rsc},
  [HN_COMMAND_LOCK_RESOURCES_ACCESS_RSC]              = {hn_request_handler_default_for_command_lock_resources_access_rsc},
  [HN_COMMAND_UNLOCK_RESOURCES_ACCESS_RSC]            = {hn_request_handler_default_for_command_unlock_resources_access_rsc},
  [HN_COMMAND_GET_TEMPERATURE_OF_FPGA]                = {hn_request_handler_default_for_command_get_temperature_of_device},
  [HN_COMMAND_GET_TEMPERATURE_OF_MOTHERBOARD]         = {hn_request_handler_default_for_command_get_temperature_of_device},
  [HN_COMMAND_CONFIGURE_DEBUG_AND_STATS]              = {hn_request_handler_default_for_command_configure_debug_and_stats},
  [HN_COMMAND_CLOCK]                                  = {hn_request_handler_default_for_command_clock},
  [HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_TILE]       = {hn_request_handler_default_for_command_stats_monitor},
  [HN_COMMAND_CONFIGURE_STATS_MONITOR_UNSET_TILE]     = {hn_request_handler_default_for_command_stats_monitor},
  [HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_PERIOD]     = {hn_request_handler_default_for_command_stats_monitor},
  [HN_COMMAND_READ_STATS_MONITOR]                     = {hn_request_handler_default_for_command_stats_monitor}
};



// the size of the hn_command_info_t data type
static size_t command_common_size = sizeof(hn_command_common_t);






//////////////////////////////////////////////
// Implementation of the functions
/////////////////////////////////////////////




// registers a handler for a command type
int hn_request_register_handler(hn_command_type_t type, hn_request_handler_t handle)
{
  request_handler[type].handle = handle;
  return 0;
}

// deregisters a handler for a command type
int hn_request_deregister_handler(hn_command_type_t type)
{
  request_handler[type].handle = DEFAULT_HANDLER[type].handle;
  return 0;
}

// handles some requests
size_t hn_request_handle(hn_request_t *req_vector, size_t max_request, void *buf, size_t buf_size, size_t *bytes_consumed)
{
  size_t num_req = 0;
  size_t buf_size_left = buf_size;
  size_t offset = 0;
  size_t bytes_consumed_this_req = 0;
  if ((req_vector != NULL) && (buf != NULL))
  {
    while ((num_req < max_request) && (buf_size_left >= command_common_size) && (offset < buf_size))
    {
      hn_command_common_t *cmd_info = (hn_command_common_t *)((unsigned char *)buf + offset);
      if ((cmd_info != NULL) && (cmd_info->command < HN_COMMAND_END_LIST) && (request_handler[cmd_info->command].handle != NULL))
      {
        bytes_consumed_this_req = request_handler[cmd_info->command].handle(&(req_vector[num_req]), (unsigned char *)(buf) + offset, buf_size_left);
        if (bytes_consumed_this_req > 0)
        {
          num_req++;
          offset += bytes_consumed_this_req;
          buf_size_left -= bytes_consumed_this_req;
          *bytes_consumed += bytes_consumed_this_req;
        }
        else
        {
          log_verbose("hn_request_handler: could not be extracted a complete request.");
          break;
        }
      } 
      else
      {
        log_error("hn_request_handler: could not be extracted command type from the application client request. Flushing %zu bytes of the buffer...", buf_size_left);
        offset          = buf_size;
        *bytes_consumed = buf_size;
        buf_size_left   = 0;
        break;
      }
    }
  }

  log_verbose("hn_request_handler: buf_size=%zu buf_offset=%zu buf_size_left=%zu total_bytes_consumed=%zu total_req_created=%zu", buf_size, offset, buf_size_left, *bytes_consumed, num_req);

  return num_req;
}


size_t hn_request_handler_default_for_command_reset(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_reset_size = sizeof(hn_command_reset_t);
  if (cmd_reset_size > buf_size) return 0;

  hn_command_reset_t *cmd_reset = (hn_command_reset_t *)buf;
  req->command = cmd_reset->common.command;
  req->tile    = cmd_reset->tile;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: reset command. tile=%u", req->tile);

  return cmd_reset_size;
}

size_t hn_request_handler_default_for_command_set_filter(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_set_filter_t);
  if (cmd_size > buf_size) return 0;

  hn_command_set_filter_t *cmd = (hn_command_set_filter_t *)buf;
  hn_filter_t *filter = (hn_filter_t *)malloc(sizeof(hn_filter_t));
  filter->target = cmd->target;
  filter->mode   = cmd->mode;
  filter->tile   = cmd->tile;
  filter->core   = cmd->core;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->size    = sizeof(*filter);
  req->data    = filter;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: set_filter_command. tile=%u filter %zu bytes (target=%u tile=%u mode=%u core=%u)", 
            req->tile, sizeof(*filter), filter->target, filter->tile, filter->mode, filter->core);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_read_register(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_read_register_t);
  if (cmd_size > buf_size) return 0;

  hn_command_read_register_t *cmd = (hn_command_read_register_t *)buf;
  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->reg;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_debug("hn_request_handler: read_register_command. tile=%u register=%u", req->tile, req->fpga_address);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_write_register(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_write_register_t);
  if (cmd_size > buf_size) return 0;

  hn_command_write_register_t *cmd = (hn_command_write_register_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->data));
  *value = cmd->data;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->reg;
  req->size    = sizeof(cmd->data);
  req->data    = value;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;

  log_debug("hn_request_handler: write_register_command. tile=%u register=%u value=%u",
            req->tile, req->fpga_address, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_write_memory_byte(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_write_memory_byte_t);
  if (cmd_size > buf_size) return 0;

  hn_command_write_memory_byte_t *cmd = (hn_command_write_memory_byte_t *)buf;
  uint8_t *value = (uint8_t *)malloc(sizeof(cmd->data));
  *value = cmd->data;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->addr;
  req->size    = sizeof(cmd->data);
  req->data    = value;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: write_memory_byte_command. tile=%u address=0x%08x value=%u", 
            req->tile, req->fpga_address, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_write_memory_half(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_write_memory_half_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_write_memory_half_t *cmd = (hn_command_write_memory_half_t *)buf;
  uint16_t *value = (uint16_t *)malloc(sizeof(cmd->data));
  *value = cmd->data;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->addr;
  req->size    = sizeof(cmd->data);
  req->data    = value;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: write_memory_half_command. tile=%u address=0x%08x value=%u", 
            req->tile, req->fpga_address, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_write_memory_word(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_write_memory_word_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_write_memory_word_t *cmd = (hn_command_write_memory_word_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->data));
  *value = cmd->data;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->addr;
  req->size    = sizeof(cmd->data);
  req->data    = value;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: write_memory_word_command. tile=%u address=0x%08x value=%u", 
            req->tile, req->fpga_address, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_write_memory_block(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_write_memory_block_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_write_memory_block_t *cmd = (hn_command_write_memory_block_t *)buf;
  uint8_t *value = (uint8_t *)malloc(sizeof(cmd->data));
  memcpy(value, cmd->data, sizeof(cmd->data) / sizeof(uint8_t));

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->addr;
  req->size    = sizeof(cmd->data);
  req->data    = value;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;

  log_debug("hn_request_handler: write_memory_block_command. tile=%u address=0x%08x block_size=%u bytes content=%02x%02x%02x%02x...%02x%02x%02x%02x", 
            req->tile, req->fpga_address, req->size, *(uint8_t *)(req->data), *((uint8_t *)req->data+1), *(uint8_t *)(req->data+2), ((uint8_t *)req->data)[3],
            *((uint8_t *)req->data+60), *((uint8_t *)req->data+61), *((uint8_t *)req->data+62), *((uint8_t *)req->data+63));

  return cmd_size;
}

size_t hn_request_handler_default_for_command_read_memory_word(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_read_memory_word_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_read_memory_word_t *cmd = (hn_command_read_memory_word_t *)buf;
  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->addr;

  log_notice("hn_request_handler: read_memory_word_command. tile=%u address=0x%08x", req->tile, req->fpga_address);

  return cmd_size;
} 

size_t hn_request_handler_default_for_command_read_memory_block(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_read_memory_block_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_read_memory_block_t *cmd = (hn_command_read_memory_block_t *)buf;
  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->fpga_address = cmd->addr;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;
  req->size    = 0;
  req->data    = NULL;

  log_notice("hn_request_handler: read_memory_block_command. tile=%u address=0x%08x", req->tile, req->fpga_address);

  return cmd_size;
} 

size_t hn_request_handler_default_for_command_create_shared_memory_buffer(hn_request_t *req, void *buf, size_t buf_size)
{
  // check buf is greater or equal than command
  if (buf_size < sizeof(hn_command_shm_buffer_create_t)) return 0;
  
  hn_command_shm_buffer_create_t *cmd = (hn_command_shm_buffer_create_t *)buf;
  size_t cmd_size = sizeof(hn_command_shm_buffer_create_t); // size in bytes

  req->command = cmd->info.command;
  req->size    = cmd_size;
  req->data    = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf, req->size);
  
  log_notice("hn_request_handler: create_shared_memory_buffer command");
  return cmd_size;
}

size_t hn_request_handler_default_for_command_destroy_shared_memory_buffer(hn_request_t *req, void *buf, size_t buf_size)
{
  // check buf is greater or equal than command
  if (buf_size < sizeof(hn_command_shm_buffer_destroy_t)) return 0;
  
  hn_command_shm_buffer_destroy_t *cmd = (hn_command_shm_buffer_destroy_t *)buf;
  size_t cmd_size = sizeof(hn_command_shm_buffer_destroy_t);// size in bytes

  req->command = cmd->info.command;
  req->size    = cmd_size;
  req->data = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf, req->size);

  log_notice("hn_request_handler: destroy_shared_memory_buffer command %s", cmd->name);
  return cmd_size;
}


size_t hn_request_handler_default_for_command_transfer_shared_memory_buffer(hn_request_t *req, void *buf, size_t buf_size)
{
  log_debug("hn_request_handler_default_for_command_transfer_shared_memory_buffer");

  if (buf_size < sizeof(hn_command_shm_buffer_transfer_t)) return 0;
 
  hn_command_shm_buffer_transfer_t *cmd = (hn_command_shm_buffer_transfer_t *)buf;
  size_t cmd_size = sizeof(hn_command_shm_buffer_transfer_t);

  log_debug("command: %s    buffer_name %s    transfer size: %u bytes    transfer offset %u bytes ", 
      (cmd->info.command == HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER)?"wr":"rd", cmd->name, cmd->size, cmd->offset);
  req->command = cmd->info.command;
  req->size    = cmd_size;
  req->data    = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf, req->size);

  return cmd_size;
}


size_t hn_request_handler_default_for_command_peak_write_protocol(hn_request_t *req, void *buf, size_t buf_size)
{
  // FIXME check buf is greater or equal than command
  size_t cmd_size = sizeof(hn_command_peak_write_protocol_t);
  if (buf_size < cmd_size)
  {
    log_warn("hn_request_handler: peak_write_protocol_command. The buffer does not contain the complete command");
    return 0;
  }

  hn_command_peak_write_protocol_t *cmd = (hn_command_peak_write_protocol_t *)buf;

  void *value = malloc(cmd->size);
  memcpy(value, cmd->data, cmd->size);

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;
  req->size    = cmd->size;
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: peak_write_protocol_command. tile=%u subtile=%u protocol=%u bytes",
            req->tile, req->subtile, req->size);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_peak_key_interrupt(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_peak_key_interrupt_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_peak_key_interrupt_t *cmd = (hn_command_peak_key_interrupt_t *)buf;
  uint8_t *value = (uint8_t *)malloc(sizeof(cmd->data));
  *value = cmd->data;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;
  req->size    = sizeof(cmd->data);
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: peak_key_interrupt_command. tile=%u subtile=%u data=%c", 
            req->tile, req->subtile, *(char *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_peak_set_processor_address(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_peak_set_processor_address_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_peak_set_processor_address_t *cmd = (hn_command_peak_set_processor_address_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->addr));
  *value = cmd->addr;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;
  req->size    = sizeof(cmd->addr);
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: peak_set_processor_address_command. tile=%u subtile=%u address=0x%08x", 
            req->tile, req->subtile, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_peak_configure_tile(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_peak_configure_tile_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_peak_configure_tile_t *cmd = (hn_command_peak_configure_tile_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->conf));
  *value = cmd->conf;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;
  req->size    = sizeof(cmd->conf);
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: peak_configure_tile_command. tile=%u subtile=%u conf=%u", 
            req->tile, req->subtile, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_peak_reset(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_peak_reset_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_peak_reset_t *cmd = (hn_command_peak_reset_t *)buf;
  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: peak_reset_command. tile=%u subtile=%u", req->tile, req->subtile);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_peak_configure_debug_and_stats(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_peak_configure_debug_and_stats_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_peak_configure_debug_and_stats_t *cmd = (hn_command_peak_configure_debug_and_stats_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->conf));
  *value = cmd->conf;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;
  req->size    = sizeof(cmd->conf);
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: peak_configure_debug_and_stats_command. tile=%u subtile=%u conf=%u", 
            req->tile, req->subtile, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_peak_read_register(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_peak_read_register_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_peak_read_register_t *cmd = (hn_command_peak_read_register_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->reg));
  *value = cmd->reg;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;
  req->size    = sizeof(cmd->reg);
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: peak_read_register_command. tile=%u subtile=%u register=%u", 
            req->tile, req->subtile, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_peak_write_register(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_peak_write_register_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_peak_write_register_t *cmd = (hn_command_peak_write_register_t *)buf;
  uint32_t *value = (uint32_t *)calloc(2, sizeof(uint32_t));
  value[0] = cmd->reg;
  value[1] = cmd->value;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = cmd->subtile;
  req->size    = 2*sizeof(uint32_t);
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: peak_write_register_command. tile=%u subtile=%u reg=%u value=%u (%08x)", 
            req->tile, req->subtile, *(uint32_t *)req->data, *((uint32_t *)req->data+1));

  return cmd_size;
}

size_t hn_request_handler_default_for_command_register_ints(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_int_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_int_t *cmd = (hn_command_int_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->vector_mask));
  *value = cmd->vector_mask;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->size    = sizeof(cmd->vector_mask);
  req->data    = value;

  // the next ones are not used for this command
  req->subtile = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: reqister_ints_command. tile=%u vector_mask=%u", 
            req->tile, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_trigger_ints(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_int_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_int_t *cmd = (hn_command_int_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->vector_mask));
  *value = cmd->vector_mask;

  req->command    = cmd->common.command;
  req->rsc_req.id = cmd->id;
  req->size       = sizeof(cmd->vector_mask);
  req->data       = value;

  // the next ones are not used for this command
  req->tile = 0;
  req->subtile = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: trigger_ints_command. id=%u vector_mask=%u", 
            cmd->id, cmd->vector_mask);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_wait_ints(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_int_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_int_t *cmd = (hn_command_int_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->vector_mask));
  *value = cmd->vector_mask;

  req->command    = cmd->common.command;
  req->rsc_req.id = cmd->id;
  req->size       = sizeof(cmd->vector_mask);
  req->data       = value;

  // the next ones are not used for this command
  req->subtile = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: wait_ints_command. id=%u vector_mask=%u", 
            cmd->id, cmd->vector_mask);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_ints(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_int_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_int_t *cmd = (hn_command_int_t *)buf;
  req->command    = cmd->common.command;
  req->rsc_req.id = cmd->id;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: release_ints_command. id=%u", cmd->id); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_register_dma(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_dma_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_dma_t *cmd = (hn_command_dma_t *)buf;
  req->command    = cmd->common.command;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: register_dma"); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_trigger_dma(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_dma_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_dma_t *cmd = (hn_command_dma_t *)buf;
  req->command      = cmd->common.command;
  req->dma_req.id       = cmd->id;
  req->dma_req.tile_src = cmd->tile_src;
  req->dma_req.addr_src = cmd->addr_src;
  req->dma_req.size     = cmd->size;
  req->dma_req.tile_dst = cmd->tile_dst;
  req->dma_req.addr_dst = cmd->addr_dst;
  req->dma_req.to_unit  = cmd->to_unit;
  req->dma_req.to_mem   = cmd->to_mem;
  req->dma_req.to_ext   = cmd->to_ext;
  req->dma_req.notify   = cmd->notify;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: trigger_dma"); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_get_status_dma(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_dma_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_dma_t *cmd = (hn_command_dma_t *)buf;
  req->command      = cmd->common.command;
  req->dma_req.id   = cmd->id;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: get_status_dma"); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_dma(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_dma_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_dma_t *cmd = (hn_command_dma_t *)buf;
  req->command      = cmd->common.command;
  req->dma_req.id   = cmd->id;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: release_dma"); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_lock_resources_access_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: lock_resources_access_rsc command."); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_unlock_resources_access_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: unlock_resources_access_rsc command."); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_find_memory_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;
  req->rsc_req.size = cmd->size;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: find_memory_rsc command. tile %u, size %llu", cmd->tile_mem, cmd->size); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_find_memories_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command = cmd->common.command;
  req->rsc_req.size = cmd->size;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: find_memories_rsc command. size %llu", cmd->size); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_allocate_memory_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;
  req->fpga_address = cmd->addr;
  req->rsc_req.size = cmd->size;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: allocate_memory_rsc command. tile %u, address %08x, size %llu", cmd->tile_mem, cmd->addr, cmd->size); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_memory_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;
  req->fpga_address = cmd->addr;
  req->rsc_req.size = cmd->size;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: release_memory_rsc command. tile %u, address %08x, size %llu", cmd->tile_mem, cmd->addr, cmd->size); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_set_backend_weights(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_set_backend_weights_t);
  if (cmd_size > buf_size) return 0;

  hn_command_set_backend_weights_t *cmd = (hn_command_set_backend_weights_t *)buf;
  req->command      = cmd->common.command;
  req->size         = cmd->num_weights;
  req->data         = malloc(sizeof(uint32_t) * req->size);
  memcpy(req->data, cmd->weights, req->size * sizeof(uint32_t));

  //uint32_t *w = req->data;
  log_notice("hn_request_handler: set_backend_weights command. num_weights %llu", req->size); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_get_network_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command           = cmd->common.command;
  req->rsc_req.tile_src  = cmd->tile_src;
  req->rsc_req.tile_dst  = cmd->tile_dst;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;
  req->fpga_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: get_network_bandwidth_rsc command. tile_src %u, tile_dst %u", cmd->tile_src, cmd->tile_dst); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_get_read_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: get_read_memory_bandwidth_rsc command. tile %u", cmd->tile_mem); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_get_write_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: get_write_memory_bandwidth_rsc command. tile %u", cmd->tile_mem); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_get_read_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: get_read_cluster_bandwidth_rsc command."); 

  return cmd_size;
}
size_t hn_request_handler_default_for_command_get_write_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: get_write_cluster_bandwidth_rsc command."); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_reserve_network_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command          = cmd->common.command;
  req->rsc_req.tile_src = cmd->tile_src;
  req->rsc_req.tile_dst = cmd->tile_dst;
  req->rsc_req.bw       = cmd->bw;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;
  req->fpga_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: reserve_network_bandwidth_rsc command. tile_src %u, tile_dst %u", cmd->tile_src, cmd->tile_dst); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_reserve_read_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: reserve_read_memory_bandwidth_rsc command. tile %u", cmd->tile_mem); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_reserve_write_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: reserve_write_memory_bandwidth_rsc command. tile %u", cmd->tile_mem); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_reserve_read_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: reserve_read_cluster_bandwidth_rsc command."); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_reserve_write_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: reserve_write_cluster_bandwidth_rsc command."); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_network_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command          = cmd->common.command;
  req->rsc_req.tile_src = cmd->tile_src;
  req->rsc_req.tile_dst = cmd->tile_dst;
  req->rsc_req.bw       = cmd->bw;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->host_address = 0;
  req->fpga_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: release_network_bandwidth_rsc command. tile_src %u, tile_dst %u", cmd->tile_src, cmd->tile_dst); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_read_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: release_read_memory_bandwidth_rsc command. tile %u", cmd->tile_mem); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_write_memory_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->tile         = cmd->tile_mem;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: release_write_memory_bandwidth_rsc command. tile %u", cmd->tile_mem); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_read_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: release_read_cluster_bandwidth_rsc command."); 

  return cmd_size;
}

size_t hn_request_handler_default_for_command_release_write_cluster_bandwidth_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_rsc_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  req->command      = cmd->common.command;
  req->rsc_req.bw   = cmd->bw;

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->size         = 0;
  req->data         = NULL;

  log_notice("hn_request_handler: release_write_cluster_bandwidth_rsc command."); 

  return cmd_size;
}

static size_t hn_request_handler_default_for_command_find_units_set_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  // check command + vector of types received
  if (buf_size < cmd_size + sizeof(uint32_t)*cmd->num_tiles) return 0;

  req->command           = cmd->common.command;
  req->rsc_req.num_tiles = cmd->num_tiles;

  // let's copy the list of types
  req->size = cmd->num_tiles*sizeof(uint32_t);
  req->data = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf + cmd_size, req->size);

  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: find_units_set_rsc command. num_tiles %u", cmd->num_tiles); 

  return cmd_size + sizeof(uint32_t)*cmd->num_tiles;
}

static size_t hn_request_handler_default_for_command_find_units_sets_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  // check command + vector of types received
  if (buf_size < cmd_size + sizeof(uint32_t)*cmd->num_tiles) return 0;

  req->command           = cmd->common.command;
  req->tile              = cmd->tile;
  req->rsc_req.num_tiles = cmd->num_tiles;
  
  // let's copy the list of types
  req->size = cmd->num_tiles*sizeof(uint32_t);
  req->data = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf + cmd_size, req->size);

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: find_units_sets_rsc command. tile %u num_tiles %u", cmd->tile, cmd->num_tiles); 

  return cmd_size + sizeof(uint32_t)*cmd->num_tiles;
}

static size_t hn_request_handler_default_for_command_reserve_units_set_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  // check command + vector of types received
  if (buf_size < cmd_size + sizeof(uint32_t)*cmd->num_tiles) return 0;

  req->command           = cmd->common.command;
  req->tile              = cmd->tile;
  req->rsc_req.num_tiles = cmd->num_tiles;

  // let's copy the list of types
  req->size = cmd->num_tiles*sizeof(uint32_t);
  req->data = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf + cmd_size, req->size);

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: reserve_units_set_rsc command. tile %u num_tiles %u", cmd->tile, cmd->num_tiles); 

  return cmd_size + sizeof(uint32_t)*cmd->num_tiles;
}

static size_t hn_request_handler_default_for_command_release_units_set_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  // check command + vector of types received
  if (buf_size < cmd_size + sizeof(uint32_t)*cmd->num_tiles) return 0;

  req->command           = cmd->common.command;
  req->tile              = cmd->tile;
  req->rsc_req.num_tiles = cmd->num_tiles;
  
  // let's copy the list of types
  req->size = cmd->num_tiles*sizeof(uint32_t);
  req->data = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf + cmd_size, req->size);

  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: release_units_set_rsc command. tile %u num_tiles %u", cmd->tile, cmd->num_tiles); 

  return cmd_size + sizeof(uint32_t)*cmd->num_tiles;
}

static size_t hn_request_handler_default_for_command_get_num_tiles_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  req->command           = cmd->common.command;
  
  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: get_num_tiles_rsc command."); 

  return cmd_size;
}

static size_t hn_request_handler_default_for_command_get_num_vns_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  req->command           = cmd->common.command;
  
  // the next ones are not used for this command
  req->tile         = 0;
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: get_num_vns_rsc command."); 

  return cmd_size;
}

static size_t hn_request_handler_default_for_command_get_memory_size_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  req->command           = cmd->common.command;
  req->tile              = cmd->tile_mem;
  
  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: get_memory_size_rsc command. tile=%u", req->tile); 

  return cmd_size;
}

static size_t hn_request_handler_default_for_command_get_tile_info_rsc(hn_request_t *req, void *buf, size_t buf_size)
{
  // check all command struct received
  if (buf_size < sizeof(hn_command_rsc_t)) return 0;

  hn_command_rsc_t *cmd = (hn_command_rsc_t *)buf;
  size_t cmd_size = sizeof(*cmd);

  req->command           = cmd->common.command;
  req->tile              = cmd->tile;
  
  // the next ones are not used for this command
  req->subtile      = 0;
  req->fpga_address = 0;
  req->host_address = 0;
  req->data         = NULL;
  req->size         = 0;

  log_notice("hn_request_handler: get_tile_info_src command. tile=%u", req->tile); 

  return cmd_size;
}

static size_t hn_request_handler_default_for_command_get_temperature_of_device(hn_request_t *req, void *buf, size_t buf_size)
{

  // check buf is greater or equal than command
  if (buf_size < sizeof(hn_command_temperature_request_t)) return 0;
  
  log_debug("hn_request_handler: get_temperature_of_device");

  hn_command_temperature_request_t *cmd = (hn_command_temperature_request_t *)buf;
  size_t cmd_size = sizeof(hn_command_temperature_request_t);

  req->command = cmd->info.command;
  req->size    = cmd_size;
  req->data    = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf, req->size);

  return cmd_size;
}
  
size_t hn_request_handler_default_for_command_configure_debug_and_stats(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_configure_debug_and_stats_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_configure_debug_and_stats_t *cmd = (hn_command_configure_debug_and_stats_t *)buf;
  uint32_t *value = (uint32_t *)malloc(sizeof(cmd->config_function));
  *value = cmd->config_function;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->subtile = 0;
  req->size    = sizeof(cmd->config_function);
  req->data    = value;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;

  log_notice("hn_request_handler: configure_debug_and_stats_command. tile=%u config_function=%u", 
            req->tile, *(uint32_t *)req->data);

  return cmd_size;
}

size_t hn_request_handler_default_for_command_clock(hn_request_t *req, void *buf, size_t buf_size)
{
  size_t cmd_size = sizeof(hn_command_clock_t);
  if (cmd_size > buf_size) return 0;
  
  hn_command_clock_t *cmd = (hn_command_clock_t *)buf;
  hn_clock_info_t *clk_info = (hn_clock_info_t *)malloc(sizeof(hn_clock_info_t));
  clk_info->clock_function  = cmd->clock_function;
  clk_info->num_cycles      = cmd->num_cycles;

  req->command = cmd->common.command;
  req->tile    = cmd->tile;
  req->size    = sizeof(hn_clock_info_t *);
  req->data    = clk_info;

  // the next ones are not used for this command
  req->fpga_address = 0;
  req->host_address = 0;
  req->subtile = 0;

  log_notice("hn_request_handler: clock. tile=%u clock_function=%u num_cycles=%u", 
            req->tile, clk_info->clock_function, clk_info->num_cycles);

  return cmd_size;
}


size_t hn_request_handler_default_for_command_stats_monitor(hn_request_t *req, void *buf, size_t buf_size)
{
  // check buf is greater or equal than command
  if (buf_size < sizeof(hn_command_stats_monitor_t)) return 0;

  hn_command_stats_monitor_t *cmd = (hn_command_stats_monitor_t *)buf;
  size_t cmd_size = sizeof(hn_command_stats_monitor_t);

  req->command = cmd->common.command;
  req->size    = cmd_size;
  req->data    = malloc(req->size);
  memcpy(req->data, (uint8_t *)buf, req->size);


  return cmd_size;
}


