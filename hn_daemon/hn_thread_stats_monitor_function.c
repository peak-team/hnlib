///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Flich (jflich@disca.upv.es)
//
// Create Date: January 5, 2018
// File Name: hn_thread_stats_monitor_function.c
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    thread to manage statistics gathering from HN system
//    Stores statistics into a shared structure of the hn_daemon, so upon a read stats requests the collector will read
//    the table and it will return the data
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// included files
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>


#include "hn_shared_structures.h"
#include "hn_resource_manager.h"
#include "hn_request_handler.h"
#include "hn_logging.h"
#include "hn_list.h"
#include "hn_buffer_fifo.h"
#include "hn_exit_program.h"

// macros
// following values MUST be the same for hn_thread_application_server_slave and the stats_monitor
//    any discrepance without value verification and validation could lead to an unexpected behaviour....
#define HN_NUM_PORTS_IN_UNIT_ROUTER     5
#define HN_STATS_NSTATS_PORT_NORTH      4
#define HN_STATS_NSTATS_PORT_EAST       3
#define HN_STATS_NSTATS_PORT_WEST       2
#define HN_STATS_NSTATS_PORT_SOUTH      1
#define HN_STATS_NSTATS_PORT_LOCAL      0


#define HN_STATS_MON_MAX_BUFFER_SIZE           32768
#define HN_STATS_MON_MAX_TILEREGS_MAX             20
#define HN_STATS_MON_MAX_REQUESTS                 (HN_STATS_MON_MAX_TILEREGS_MAX + HN_MAX_VNS*HN_NUM_PORTS_IN_UNIT_ROUTER) // max size of the arra
#define HN_STATS_MON_MAX_TILEREGS_FAMILY_PEAK     13 // we do not count the network stats, they are gathered sepparatedly
#define HN_STATS_ITEMS_PER_TILEREG_READ            6

// NOTICE: the value of HN_STATS_MON_MAX_TILEREGS should be modified in case that any UNIT has more than HN_STATS_MON_MAX_TILEREGS values to read

// PEAK units - Register assignment for statistics

#define TIMESTAMP_LSW_REG                 7 //  0  Timestamp LSB register
#define TIMESTAMP_MSW_REG                 2 //  1  Timestamp MSB register
#define N_L1I_HITS_REG                    9 //  2  Stats: number of hits in L1I cache
#define N_L1I_MISS_REG                   10 //  3  Stats: number of misses in L1I cache
#define N_L1D_HITS_REG                   11 //  4  Stats: number of hits in L1D cache
#define N_L1D_MISS_REG                   12 //  5  Stats: number of misses in L1D cache
#define N_INST_EXEC_REG                  13 //  6  Stats: number of instructions executed register (for manager)
#define N_BTB_PRE_REG                    14 //  7  Stats: number of BTB prediction register (for manager)
#define N_BTB_MISPRE_REG                 15 //  8  Stats: number of BTB mispredictions register (for manager)
#define N_CYCLES_CORE_REG                16 //  9  Stats: number of core cycles executed register (for manager)
#define MGT_N_L2_HITS_REG                21 // 10  Stats: number of hits in L2 cache bank (for manager only)
#define MGT_N_L2_MISS_REG                22 // 11  Stats: number of misses in L2 cache bank (for manager only)
#define MGT_N_MC_ACCESS_REG              23 // 12  Stats: number of MC accesses (for manager only)

// network stats, special reg, cyclic. We have to send : Number_of_ports * number_of_VNs requests to gather all the netstats
#define T_STATS_REG                       3 // Network statistics register (this is a cyclic register providing all the network stats), HOW MANY READS DO I NEED TO PERFORM ?????



// global variables
const hn_rscmgt_info_t *rsc_info;
pthread_mutex_t         mutex_tmp;

uint32_t  table_num_clients_requesting_stats_from_tile[HN_MAX_TILES];


typedef struct hn_thread_application_args_st
{ // BEWARE!!!! hn_client_info_t has to be the first field in the struct
  hn_client_info_t client_info;      //!< Information about the client
  int sock;                          //!< Socket the slave thread uses to communicate with the application client
}hn_thread_application_args_t;



//  Public function 
uint32_t hn_stats_monitor_reg_is_for_stats(uint32_t tile, uint32_t reg_addr) {
  uint32_t reg_is_for_stats = 0;
  uint32_t tile_type;

  tile_type = rsc_info->tile_info[tile].type;

  if(tile_type == HN_TILE_FAMILY_PEAK)
  {
    switch (reg_addr) {
      case TIMESTAMP_LSW_REG:
      case TIMESTAMP_MSW_REG:
      case T_STATS_REG:
      case N_L1I_HITS_REG:
      case N_L1I_MISS_REG:
      case N_L1D_HITS_REG:
      case N_L1D_MISS_REG:
      case N_INST_EXEC_REG:
      case N_BTB_PRE_REG:
      case N_BTB_MISPRE_REG:
      case N_CYCLES_CORE_REG:
      case MGT_N_L2_HITS_REG:
      case MGT_N_L2_MISS_REG:
      case MGT_N_MC_ACCESS_REG:
        reg_is_for_stats = 1;
        break;
      default: 
        reg_is_for_stats = 0;
        break;
    }
  }

  return reg_is_for_stats;
}

//---------------------------------------------------------------------------
uint32_t hn_stats_monitor_add_client_for_tile_stats(uint32_t tile_id, unsigned long long cliend_id)
{
  table_num_clients_requesting_stats_from_tile[tile_id] = table_num_clients_requesting_stats_from_tile[tile_id] + 1;
  return 0;
}

//---------------------------------------------------------------------------
uint32_t hn_stats_monitor_remove_client_for_tile_stats(uint32_t tile_id, unsigned long long cliend_id)
{
  uint32_t rv = 1;

  if(table_num_clients_requesting_stats_from_tile[tile_id] > 0) 
  {
    table_num_clients_requesting_stats_from_tile[tile_id] = table_num_clients_requesting_stats_from_tile[tile_id] - 1;
    rv = 0;
  } 
  return rv;
}

//---------------------------------------------------------------------------
uint32_t hn_stats_monitor_get_num_clients_for_tile_stats(uint32_t tile_id, uint32_t *num_clients)
{
  *num_clients =  table_num_clients_requesting_stats_from_tile[tile_id];
  return 0;
}
//---------------------------------------------------------------------------

// function templates
//void hn_thread_stats_monitor_function(void);
static void clean_stats_monitor_mutex(void *args);
//---------------------------------------------------------------------------------------------------------------------
// function implementation
//---------------------------------------------------------------------------------------------------------------------

/*
 * this function is called upon a thread cancelation
 */
static void clean_stats_monitor_mutex(void *args) {
  pthread_mutex_unlock(&mutex_stats_monitor_table);
  pthread_mutex_unlock(&mutex_stats_monitor_config_access);

  log_verbose("stats_monitor thread: cleanup routine called, perhaps was canceled!");
}

/*
 * this function is called upon a thread cancelation
 */
static void clean_tmp_mutex(void *args) {
  pthread_mutex_unlock(&mutex_tmp);
  pthread_cond_broadcast(&cond_collector_init);
}

/*
 * this function return the time elsapsed between two timestramps in us
 */
uint64_t get_elapsed_time_us(struct timespec *tstart,struct timespec *tend)
{
  struct   timespec t_lapse;
  uint64_t t_us;

  if ((tend->tv_nsec - tstart->tv_nsec) < 0) 
  {
    t_lapse.tv_sec  = tend->tv_sec - tstart->tv_sec - 1;
    t_lapse.tv_nsec = 1000000000 + tend->tv_nsec - tstart->tv_nsec;
  } 
  else 
  {
    t_lapse.tv_sec  = tend->tv_sec  - tstart->tv_sec;
    t_lapse.tv_nsec = tend->tv_nsec - tstart->tv_nsec;
  }

  t_us = (t_lapse.tv_sec * 1000000) + (t_lapse.tv_nsec / 1000);

  return t_us;

}


/*
 * Main thread
 */
void *hn_thread_stats_monitor_function(void *args)
{
  static int        retval;
  uint32_t          sleep_active; // indicates whether the thread will sleep or not before sending a new tile request for all stats
  uint64_t          us_to_sleep;
  uint64_t          process_incoming_items_time_us; // us that took to process the read items, to decrement us_to_sleep time 
  int               rs_mutex;
  hn_thread_application_args_t *targs;
  //int             sock
  int               input_fifo;
  int               output_fifo;
  hn_request_t      req[HN_STATS_MON_MAX_REQUESTS];
  unsigned char     buffer_from_dispatcher[HN_STATS_MON_MAX_BUFFER_SIZE];
  uint32_t          unit_num_rcv_stats_items[HN_MAX_TILES][HN_MAX_CORES_PER_TILE];
  uint32_t          num_vns;

  // IMPRTANT: IN the timestamp ( and dny 64bit value) we first send the read of the LSB regsiter so we will receive it first, and then we will reset the value of the timestamp
  // the other option is to make partial assignment of the value
  uint32_t tile_family_peak_regs[HN_STATS_MON_MAX_TILEREGS_FAMILY_PEAK] = { 
    TIMESTAMP_LSW_REG,  TIMESTAMP_MSW_REG,  N_L1I_HITS_REG,       N_L1I_MISS_REG,             N_L1D_HITS_REG,
    N_L1D_MISS_REG,     N_INST_EXEC_REG,    N_BTB_PRE_REG,        N_BTB_MISPRE_REG,           N_CYCLES_CORE_REG,
    MGT_N_L2_HITS_REG,  MGT_N_L2_MISS_REG,  MGT_N_MC_ACCESS_REG 
  };

  log_debug("starting stats monitor thread");
  //printf("JM10, size of tile_family_peak_regs is %zu -> elements %zu\n", sizeof(tile_family_peak_regs), sizeof(tile_family_peak_regs)/sizeof(tile_family_peak_regs[0]));
  
  targs = (hn_thread_application_args_t *)args;
  //sock = targs->sock;
  input_fifo  = targs->client_info.input_fifo;
  output_fifo = targs->client_info.output_fifo;  

  // set the filter type of this "virtual client" 
  targs->client_info.filter.target = HN_FILTER_TARGET_PEAK;
  targs->client_info.filter.mode   = HN_FILTER_APPL_MODE_SYNC_READS;
  targs->client_info.filter.tile   = 999;
  targs->client_info.filter.core   = 999;

  rs_mutex = pthread_mutex_init(&mutex_tmp, NULL);
  if (rs_mutex != 0) {
    log_error("stats_monitor, mutex for stats monitor not initialized");
    log_info("Will sleep for 10 seconds, to be sure the system will be running");
    sleep(10);
  }
  else
  {
    log_debug("locks for stats monitor initialization succesfully intialized");
    // get arch info
    pthread_cleanup_push(clean_tmp_mutex, NULL);
    pthread_mutex_lock(&mutex_tmp);
    if (hn_rscmgt_get_status() != HN_RESOURCE_MANAGER_STATUS_RUNNING) 
    {
      log_debug("stats_monitor_thread waiting for collector initialization");
      if (!hn_exit_program_is_terminated()) {
        pthread_cond_wait(&cond_collector_init, &mutex_tmp);
      }
      log_debug("stats_monitor_thread,continue initialization");
    }
    pthread_mutex_unlock(&mutex_tmp);
    pthread_cleanup_pop(0);
  }
  
  rsc_info = hn_rscmgt_get_info();
  num_vns  = rsc_info->num_vns;

  uint32_t tile_ind;
  uint32_t subtile_ind;
  for(tile_ind = 0; tile_ind < HN_MAX_TILES; tile_ind++)
  {
    for(subtile_ind = 0; subtile_ind < HN_MAX_CORES_PER_TILE; subtile_ind++)
    {
      unit_num_rcv_stats_items[tile_ind][subtile_ind] = 0; 
    }
  }

  pthread_cleanup_push(clean_stats_monitor_mutex, NULL);

  uint32_t tmp_index;
  for(tmp_index = 0; tmp_index < HN_MAX_TILES ; tmp_index ++) {
    table_num_clients_requesting_stats_from_tile[tmp_index] = 0;
  }


  log_info("stats_monitor_thread: ready...");
  process_incoming_items_time_us = 0;

  while (!hn_exit_program_is_terminated()) 
  {

    int       iter;
    uint32_t *tile_id_at_iter_ptr;
    uint32_t  tile_id;

    pthread_mutex_lock(&mutex_stats_monitor_config_access);
    //printf(" program_is_terminated : %s   stats_monitor_active: %s\n", hn_exit_program_is_terminated()?"yes":" no", stats_monitor_active?"yes":" no"); fflush(stdout);
    while((!hn_exit_program_is_terminated()) && (stats_monitor_active == 0) )
    {
      log_info("stats_monitor_thread, waiting...");
      pthread_cond_wait(&cond_stats_monitor_config_change, &mutex_stats_monitor_config_access);
      log_info("stats_monitor_thread, start statistics gathering every %u us", stats_monitor_polling_period_us);

    }
    pthread_mutex_unlock(&mutex_stats_monitor_config_access);


    if ( !hn_exit_program_is_terminated() && (!hn_list_is_empty(stats_monitor_tiles_list)) )
    {
      //printf("JM10, stats_monitor_function, check tiles to send tilereg read requests..\n"); fflush(stdout);
      
      iter = hn_list_iter_front_begin(stats_monitor_tiles_list);

      while (!hn_list_iter_end(stats_monitor_tiles_list, iter))
      {
        uint32_t tile_type;
        uint32_t num_cores;


        tile_id_at_iter_ptr = (uint32_t *)hn_list_get_data_at_position(stats_monitor_tiles_list, iter);
        tile_id =  *tile_id_at_iter_ptr; 
        //printf("JM10, tile %u\n", tile_id); fflush(stdout);

        // add new read register request to the fifo, for all regs of all tiles
        // We assume that the tile is of type peak !!
        tile_type = rsc_info->tile_info[tile_id].type;
        num_cores = rsc_info->tile_info[tile_id].num_cores;
        if(tile_type == HN_TILE_FAMILY_PEAK)
        {
          //log_info("      tile type detected family PEAK\n");
          uint32_t curr_core;

          if(num_cores > 0)
          {
            // compose all the requests and write them into the buffer at once (better performance)
            for(curr_core = 0; curr_core < num_cores; curr_core++)
            {
              uint32_t curr_reg;
              uint32_t nstats_read_index;

              // send read for all registers related to statistics
              // prepare all requests and write them all at one to improve performance
              //printf("JM10,   create requests for core %u, read %d regs\n", curr_core, HN_STATS_MON_MAX_TILEREGS_FAMILY_PEAK); fflush(stdout);
              for(curr_reg = 0; curr_reg < HN_STATS_MON_MAX_TILEREGS_FAMILY_PEAK; curr_reg++)
              {
                uint32_t *value;
                
                value  = (uint32_t *)malloc(sizeof(uint32_t));
                *value = tile_family_peak_regs[curr_reg];

                // fill only the needed fields of the struct,the rest are not even initialized
                req[curr_reg].command  = HN_COMMAND_PEAK_READ_REGISTER; // Command 
                req[curr_reg].tile     = tile_id;           // Tile
                req[curr_reg].subtile  = curr_core;         // Tile inside of the Unit this command is for. For single-unit tiles set 0 
                req[curr_reg].size     = sizeof(uint32_t);  // Size of the data command 
                req[curr_reg].data     = value;             // Pointer to the data required for the command

              }
              /*
              printf("JM10, %d requests to send\n", HN_STATS_MON_MAX_TILEREGS_FAMILY_PEAK); fflush(stdout);
              for(curr_reg = 0; curr_reg < HN_STATS_MON_MAX_TILEREGS_FAMILY_PEAK; curr_reg++)
              {
                uint32_t *vp;
                vp =  req[curr_reg].data;
                printf("      #%2u   cmd: %d    tile: %2u    core: %2u    size_of_reg: %2u bytes    #reg: %u\n", 
                    curr_reg, req[curr_reg].command, req[curr_reg].tile, req[curr_reg].subtile, req[curr_reg].size, (uint32_t)(*vp)
                    );
                fflush(stdout);
              }
              */
              log_debug("writing  stats requests for tile %u  core %u",tile_id, curr_core);
              hn_buffer_fifo_write_bytes_from_buf(output_fifo, &req, HN_STATS_MON_MAX_TILEREGS_FAMILY_PEAK*sizeof(hn_request_t));
              for (nstats_read_index = 0; nstats_read_index < (num_vns*HN_NUM_PORTS_IN_UNIT_ROUTER); nstats_read_index++)
              {
                 uint32_t *value;
                
                value  = (uint32_t *)malloc(sizeof(uint32_t));
                *value = (uint32_t)T_STATS_REG;

                // fill only the needed fields of the struct,the rest are not even initialized
                req[nstats_read_index].command  = HN_COMMAND_PEAK_READ_REGISTER; // Command 
                req[nstats_read_index].tile     = tile_id;           // Tile
                req[nstats_read_index].subtile  = curr_core;         // Tile inside of the Unit this command is for. For single-unit tiles set 0 
                req[nstats_read_index].size     = sizeof(uint32_t);  // Size of the data command 
                req[nstats_read_index].data     = value;             // Pointer to the data required for the command
              }
              log_debug("writing nstats requests for tile %u  core %u  reg %u",tile_id, curr_core, (uint32_t)T_STATS_REG);
              //printf("JM10, writing nstats requests for tile %u  core %u  reg %u\n",tile_id, curr_core, *value);
              hn_buffer_fifo_write_bytes_from_buf(output_fifo, &req, ((num_vns*HN_NUM_PORTS_IN_UNIT_ROUTER))*sizeof(hn_request_t));
            }
          }
        }
        else if ( tile_type == HN_TILE_FAMILY_NONE)
        {
          log_error("NOT supported feature: Request Read statistics for an empty tile, family type: %d", tile_type);
        }
        else 
        {
          // To-Do add rest of families
          log_error("NOT supported feature: Request Read statistics for this family type: %d", tile_type);
        }

        iter = hn_list_iter_next(stats_monitor_tiles_list, iter);
      }
    
    }
    //else {
    //  printf("program terminated or list is empty\n");fflush(stdout);
    //}
    // if program is not terminated, sleep
    if(!hn_exit_program_is_terminated())
    {
      pthread_mutex_lock(&mutex_stats_monitor_config_access);
      
      // let's decrease all the time elapsed since the thread woke up until this point of the code
      // us_to_sleep = stats_monitor_polling_period_us - elapsed_time
      if(process_incoming_items_time_us >= stats_monitor_polling_period_us) 
      {
        // reading data took too much, will not sleep at all
        sleep_active = 0;
      }
      else
      {
        sleep_active = 1;
        us_to_sleep  = stats_monitor_polling_period_us - process_incoming_items_time_us;
      }

      pthread_mutex_unlock(&mutex_stats_monitor_config_access);
      
      if(sleep_active)
      {
        log_debug("stats monitor, sleeping %lu us", us_to_sleep);
        usleep(us_to_sleep);
      }
    }

    // if program is not terminated after sleep, check for incoming data from dispatcher (tilereg info from tiles)
    if(!hn_exit_program_is_terminated())
    {
      size_t bytes_in_fifo = hn_buffer_fifo_get_num_bytes(input_fifo);
      // get the time stamp to then reduce it from next sleep interval
      if(bytes_in_fifo > 0)
      {
        struct  timespec ts;
        struct  timespec te;
        size_t  bytes_to_read;
        size_t  elements_in_fifo;
        size_t  bytes_read;
        size_t  element_index;
        hn_command_unit_read_register_response_t   * tilereg_read_data;

        // first I need to get the time, and then discount the sleep time from the main thread
        clock_gettime(CLOCK_MONOTONIC, &ts);
        // let's process all the incoming data to update stats...
        //log_info("JM10, something to read...");
        
        //read data in multiple of an item, an item comes in a uint32_t
        bytes_in_fifo    = (bytes_in_fifo > HN_STATS_MON_MAX_BUFFER_SIZE) ? HN_STATS_MON_MAX_BUFFER_SIZE : bytes_in_fifo;
        elements_in_fifo = bytes_in_fifo     /  sizeof(hn_command_unit_read_register_response_t);
        bytes_to_read    = elements_in_fifo  *  sizeof(hn_command_unit_read_register_response_t);
        
        bytes_read = hn_buffer_fifo_read_bytes_to_buf(input_fifo, buffer_from_dispatcher, bytes_to_read);
        // now we have the data, so we can process the items
        
         if (bytes_read != bytes_to_read) log_error ("hn_thread_stats_monitor, Error reading data from input fifo");
        //log_info("      read %zu elements %zu bytes from %zu bytes ", elements_in_fifo, bytes_read, bytes_in_fifo);
        
        tilereg_read_data = (hn_command_unit_read_register_response_t *)&buffer_from_dispatcher;
        pthread_mutex_lock(&mutex_stats_monitor_table);
        for(element_index = 0; element_index < elements_in_fifo; element_index++)
        {
          log_verbose("setting stats for    tile: %2u   core: %2u    reg_addr: %2u    reg_val: %u",
              tilereg_read_data->tile, tilereg_read_data->subtile, tilereg_read_data->reg, tilereg_read_data->value);
          switch(tilereg_read_data->reg) {
            case TIMESTAMP_LSW_REG:
              {
                uint64_t v1;
                v1 = tilereg_read_data->value & 0x00000000FFFFFFFF;
                stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].timestamp =  v1 ;
                fflush(stdout);
              }
              break;
            case TIMESTAMP_MSW_REG:
              {
                uint64_t v1;
                v1 = tilereg_read_data->value;
                stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].timestamp = 
                      stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].timestamp | (v1 << 32);
              }
              break;
            case T_STATS_REG:
              {
                uint32_t vn;
                uint32_t port;

                vn   = unit_num_rcv_stats_items[tilereg_read_data->tile][tilereg_read_data->subtile] / HN_NUM_PORTS_IN_UNIT_ROUTER;
                port = unit_num_rcv_stats_items[tilereg_read_data->tile][tilereg_read_data->subtile] % HN_NUM_PORTS_IN_UNIT_ROUTER;
                //stats_monitor_table[tilereg_read_data->tile].[tilereg_read_data->subtile].
                //uint32_t fake_value = 0x00000000 | ((tilereg_read_data->tile) << 24) | ((tilereg_read_data->subtile) << 16) | unit_num_rcv_stats_items[tilereg_read_data->tile][tilereg_read_data->subtile];
                switch(port){
                  case HN_STATS_NSTATS_PORT_NORTH:
                    stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].nstats[vn].flits_ejected_north = tilereg_read_data->value;// fake_value;//tilereg_read_data->value;
                    break;
                  case HN_STATS_NSTATS_PORT_EAST:
                    stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].nstats[vn].flits_ejected_east  = tilereg_read_data->value;// fake_value;//tilereg_read_data->value;
                    break;
                  case HN_STATS_NSTATS_PORT_WEST:
                    stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].nstats[vn].flits_ejected_west  = tilereg_read_data->value;// fake_value;//tilereg_read_data->value;
                    break;
                  case HN_STATS_NSTATS_PORT_SOUTH:
                    stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].nstats[vn].flits_ejected_south = tilereg_read_data->value;// fake_value;//tilereg_read_data->value;
                    break;
                  case HN_STATS_NSTATS_PORT_LOCAL:
                    stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].nstats[vn].flits_ejected_local = tilereg_read_data->value;// fake_value;//tilereg_read_data->value;
                    break;
                  default:
                    log_error("stats_monitor, received net stats for unexpected port  tile %2u  subtile %2u  vn %2u  port %2u\n", tilereg_read_data->tile, tilereg_read_data->subtile, vn, port );
                    break;
                }
                
                log_verbose("received nstats for tile %2u   core %2u  VN %2u  port %2u  value %10d\n",tilereg_read_data->tile, tilereg_read_data->subtile, vn, port, tilereg_read_data->value);
                if(unit_num_rcv_stats_items[tilereg_read_data->tile][tilereg_read_data->subtile] < (num_vns * HN_NUM_PORTS_IN_UNIT_ROUTER - 1)) {
                  unit_num_rcv_stats_items[tilereg_read_data->tile][tilereg_read_data->subtile]++;
                } 
                else {
                  unit_num_rcv_stats_items[tilereg_read_data->tile][tilereg_read_data->subtile] = 0;
                }
              }
              break;
            case N_L1I_HITS_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].l1i_hits    = tilereg_read_data->value;
              break;
            case N_L1I_MISS_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].l1i_misses  = tilereg_read_data->value;
              break;
            case N_L1D_HITS_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].l1d_hits    = tilereg_read_data->value;
              break;
            case N_L1D_MISS_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].l1d_misses  = tilereg_read_data->value;
              break;
            case N_INST_EXEC_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].core_instr  = tilereg_read_data->value;
              break;
            case N_BTB_PRE_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].btb_predict = tilereg_read_data->value;
              break;
            case N_BTB_MISPRE_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].btb_misspredict = tilereg_read_data->value;
              break;
            case N_CYCLES_CORE_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].core_cycles = tilereg_read_data->value;
              break;
            case MGT_N_L2_HITS_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].l2_hits     = tilereg_read_data->value;
              break;
            case MGT_N_L2_MISS_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].l2_misses   = tilereg_read_data->value;
              break;
            case MGT_N_MC_ACCESS_REG:
              stats_monitor_table[tilereg_read_data->tile][tilereg_read_data->subtile].mc_accesses = tilereg_read_data->value;
              break;
            default: 
              log_verbose("tilereg read tile: %u  core: %u  reg_addr: %u was not stats", tilereg_read_data->tile, tilereg_read_data->subtile, tilereg_read_data->reg);
            break;
          }
          tilereg_read_data++;
        }
        pthread_mutex_unlock(&mutex_stats_monitor_table);

        clock_gettime(CLOCK_MONOTONIC, &te);
        process_incoming_items_time_us = get_elapsed_time_us(&ts,&te);
      }
      else
      {
        // something is not going well.... sysmtem congested ?? 
        // we may want to avoid posting new data..or sleep a little bit fore to not overload the system with unresponded requests
        log_warn("stats monitor, no stats from hn_system received yet, it is taking too long, system may be under congestion");
      }
    }

  }

  pthread_cleanup_pop(0);

  pthread_mutex_destroy(&mutex_tmp);
  log_info("stats_monitor_thread: exiting...");

  return &retval;
}
  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// End of file hn_thread_stats_monitor_function.h
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
