#include <stdio.h>
#include <string.h>

#include "hn_logging.h"
#include "hn_buffer_fifo.h"

// basic test. It does not take into account threads

int main(int argc, char **argv)
{
  hn_init_log(HN_LOG_TO_STDOUT, NULL);

  log_info("prepared for test unit for hn_buffer_fifo");

  int id[34];
  int err;

  log_info("TEST1: create buffer FIFOs...");
  if ((id[0] = hn_buffer_fifo_create(4)) < 0)
    log_error("TEST1: NOT passed!");
  else
    log_info("TEST1: passed!");

  log_info("TEST2: destroy buffer FIFOs...");
  err = hn_buffer_fifo_destroy(id[0]);
  int err1 = hn_buffer_fifo_destroy(0);
  if ((err == 0) && (err1 < 0))
    log_info("TEST2: passed");
  else 
    log_error("TEST2: NOT passed!");

  
  unsigned char ro_buf[1024];
  unsigned char buf[1024];
  for (int i = 0; i < 1024; i++)
    ro_buf[i] = (unsigned char)i;
  log_info("TEST3: Write bytes to a buffer FIFO...");
  size_t ele1 = hn_buffer_fifo_write_elements_from_buf(id[0], ro_buf, 2);
  id[0] = hn_buffer_fifo_create(8);
  size_t ele2 = hn_buffer_fifo_write_elements_from_buf(id[0], ro_buf, 2);
  if ((ele1 != 0) || (ele2 != 2))
    log_error("TEST3: NOT passed");
  else
    log_info("TEST3: passed");

  log_info("TEST4: Read bytes from buffer FIFO to an external buffer...");
  ele1 = hn_buffer_fifo_read_elements_to_buf(3, buf, 2);
  ele2 = hn_buffer_fifo_read_elements_to_buf(id[0], buf, 2);
  err = memcmp(ro_buf, buf, ele2*4);
  err1 = hn_buffer_fifo_destroy(id[0]);
  if ((ele1 != 0) || (ele2 != 2) || (err != 0) || (err1 != 0))
    log_error("TEST4: NOT passed");
  else
    log_info("TEST4: passed");


  log_info("TEST5: Write bytes between two buffer FIFOs...");
  log_warn("TEST5: not implemented yet!");

  hn_close_log();

  return 0;
}
