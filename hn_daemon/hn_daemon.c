///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_daemon.c
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node background communication process daemon
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define _GNU_SOURCE
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

// Number of static threads in the daemon
#define HN_DAEMON_NUM_THREADS  5

// Default log mode to use if not specified otherway
#ifndef HN_DAEMON_LOG_MODE
#define HN_DAEMON_LOG_MODE HN_LOG_TO_STDOUT
#endif

// Default filename in case of HN_LOG_TOFILE and no filename is provided
#ifndef HN_DAEMON_LOG_FILENAME
#define HN_DAEMON_LOG_FILENAME HN_LOG_DEFAULT_FILE
#endif

#define _HN_SHARED_STRUCTURES_DEFINITION
#include "hn_daemon.h"
#include "hn_logging.h"
#include "hn_exit_program.h"
#include "hn_list.h"
#include "hn_buffer_fifo.h"
#include "hn_socket_unix.h"
#include "hn_socket_inet.h"
#include "hn_shared_structures.h"
#include "hn_shared_memory.h"
#include "hn_iface_mmi64.h"
#include "hn_thread_item_dispatcher_function.h"
#include "hn_thread_item_collector_function.h"
#include "hn_thread_backend_get_data_from_fpga_function.h"
#include "hn_thread_backend_send_data_to_fpga_function.h"
#include "hn_thread_application_server_master_function.h"
#include "hn_thread_application_server_slave_function.h"

/////////////////////////////////////////////////////////////////////////////////////
/*
 * Structs and typedefs
 */

// defines the prototype of a thread function as a type
typedef void* (*thread_function_t)(void *arg);

////////////////////////////////////////////////////////////////////////////////////
/*
 * Local function prototypes
 */
/*
 * This function is the core for creating the main HN daemon threads
 *
 * Apart from the threads created in this function there are other threads
 * created dinamically as applications request connection to the daemon.
 * The creation of these threads is managed in the hn_thread_application_server_master_function
 *
 * retuns 0 if all the static threads has been created succesfully and -1 otherwise
 */
static int create_threads();

/*
 * This function must contain a call to pthread_join for every created thread using
 * the thread_create() function. Also, this function takes into account the waiting for
 * the join of the threads created through the hn_thread_application_server_master_function
 */
static void join_threads();

/*
 * Initializes the resources used in some of the daemon threads
 *
 * returns 0 if everything has been initialized correctly and -1 otherwise
 */
static int initialize_resources(char *ifname);

/*
 * Destroyes initialized resources
 */
static void destroy_resources();

//////////////////////////////////////////////////////////////////////////////////
/*
 * Global variable definition
 */

// let's define the server sockets
static hn_socket_unix_info_t  sock_unix_info;
static hn_socket_inet_info_t  sock_inet_info;

// Thread ids
static pthread_t thread_id[HN_DAEMON_NUM_THREADS];

// name of the threads
static const char *thread_name[HN_DAEMON_NUM_THREADS] =
{
  "application server master",
  "item dispatcher",
  "item collector",
  "backend to FPGA",
  "backend from FPGA"
};

#ifdef HDL_SIM
int mmi64_main(int argc, char **argv)
{
  char *ifname = "mmi64pli";
#else
int main(int argc, char* argv[])
{
  char  *ifname; // interface for mmi64
  
  if (argc != 2)
  {
    printf("ERROR: bad number of arguments. Usage\n");
    printf("       %s <ifname>\n", argv[0]);
    printf("       ifname options:\n");
    printf("           mmi64       -- mmi64 interface through motherboard eth/usb connectors \n");
    printf("           mmi64fmpcie -- mmi64 interface through external pcie module \n");
    printf("           mmi64pli    -- mmi64 interface for simulation. HW running on modelsim simulator\n");
    return -1;
  }

  ifname = argv[1];
#endif

#ifndef HN_DAEMON_FOREGROUND
  pid_t  process_id = 0;
  pid_t  sid = 0;

  // Create child process
  process_id = fork();
  // Indication of fork() failure
  if (process_id < 0)
  {
    printf("ERROR starting daemon: %s\n", strerror(errno));
    return 1;
  }
  
  // PARENT PROCESS. We are goint to kill it.
  if (process_id > 0)
  {
    // this is the end of this process
    // return success in exit status
    return 0;
    //this is not an error, this process just finishes
  }
  
  //set new session
  sid = setsid();

  if(sid < 0)
  {
    // Return failure
    printf("ERROR starting the daemon: %s\n", strerror(errno));
    return 1;
  }
#endif

  fclose(stdin);
  fclose(stderr);
  //fclose(stdout);


  //unmask the file mode
  umask(0); 

  if (hn_log_initialize(HN_DAEMON_LOG_MODE, HN_DAEMON_LOG_FILENAME) < 0)
  {
    printf("ERROR initializing log system: %s\n", strerror(errno));
    return 2;
  }

  log_info("HN daemon pid %d", getpid());

#ifdef HN_DAEMON_FOREGROUND
  // let's install a termination program handler for the SIGINT (Ctrl-C) signal
  hn_exit_program_install_handler(SIGINT);
#else
  // let's install a termination program handler for the SIGTERM signal
  hn_exit_program_install_handler(SIGTERM);
#endif

  int err = initialize_resources(ifname);

  if (err == 0) {
    // no errors initializing resources
    // So, let's create the static threads and wait for their completion
    if (create_threads() == 0)
    {
      printf("\nhn_daemon is running\n");
      join_threads();
    }
  }


  printf("\nstopping hn_daemon\n");
  destroy_resources();
  hn_log_close();
  printf("\n");
  fflush(stdout);
  return 0;
}


/***************************************************************
 * Program function implementation
 ***************************************************************/


int create_threads()
{
  int res_create_thread[HN_DAEMON_NUM_THREADS];
  int error = 0;

  memset(res_create_thread, 0, sizeof(res_create_thread));
  // let's create the application server master thread
  static hn_thread_application_server_master_args_t app_server_args;
  memcpy(&(app_server_args.sock_un), &sock_unix_info, sizeof(hn_socket_unix_info_t));
  memcpy(&(app_server_args.sock_inet), &sock_inet_info, sizeof(hn_socket_inet_info_t));
  res_create_thread[0] = pthread_create(&(thread_id[0]), NULL, &hn_thread_application_server_master_function, &app_server_args);

  // let's create the item dispatcher thread
  //static ... item_dispatcher_args = ;
  res_create_thread[1] = pthread_create(&(thread_id[1]), NULL, &hn_thread_item_dispatcher_function, NULL);
 
  
  // let's create the item collector thread
  //static ... item_collector_args = ; // No args currently passed to thread
  res_create_thread[2] = pthread_create(&(thread_id[2]), NULL, &hn_thread_item_collector_function, NULL);

  // let's create the backend to FPGA thread
  //static ... backend_to_fpga_args = ; // No args currently passed to thread
  res_create_thread[3] = pthread_create(&(thread_id[3]), NULL, &hn_thread_backend_send_data_to_fpga_function, NULL);

  // let's create the backend from FPGA thread
  //static ... backend_from_fpga_args = ; // No args currently passed to thread
  res_create_thread[4] = pthread_create(&(thread_id[4]), NULL, &hn_thread_backend_get_data_from_fpga_function, NULL);



  // let's check creation results 
  int i;
  for (i = 0; i < HN_DAEMON_NUM_THREADS; i++)
  {
    if (res_create_thread[i] != 0)
    {
      log_error("daemon: %s thread could not be created: %s", thread_name[i], strerror(errno));
      error = -1;
    }
  }

  // try to finish the program in an ordered way
  if (error == -1) 
  {
    hn_exit_program_terminate(1);
  }

  return error;
}


void join_threads() 
{
  int i;
  struct timespec timeout;
  int threads_terminated = 0;
  int *retval[HN_DAEMON_NUM_THREADS];

  pthread_join(thread_id[0], (void **)&retval[0]);
  log_debug("thread %s has finished with status %d", thread_name[0], *retval[0]);
  threads_terminated++;

  for(i = 1; i < HN_DAEMON_NUM_THREADS; i++)
  {

    timeout.tv_sec  = 5;
    timeout.tv_nsec = 0;
    if (pthread_timedjoin_np(thread_id[i], (void **)&retval[i], &timeout) == 0)
    { 
      threads_terminated++;
      if (retval[i] != NULL)
        log_debug("thread %s has finished with status %d", thread_name[i], *retval[i]);
      else
        log_debug("thread %s has finished with status 0", thread_name[i]);

      thread_id[i] = 0;
    } 
    else
    {
      // cancel the threads
      pthread_cancel(thread_id[i]);
      log_debug("thread %s is going to be cancelled", thread_name[i]);
    }
  }

  for(i = 1; i < HN_DAEMON_NUM_THREADS; i++)
  {
    if (thread_id[i] != 0)
    {
      if (pthread_join(thread_id[i], (void **)&retval[i]) == 0)
      {
        threads_terminated++;
        if ((retval[i] != NULL) && (retval[i] != PTHREAD_CANCELED))
          log_debug("thread %s has finished with status %d", thread_name[i], *retval[i]);
        else
          log_debug("thread %s has finished with status ??", thread_name[i]);

        thread_id[i] = 0;
      }
    }
  }

  log_debug("Terminated %d / %d threads", threads_terminated, HN_DAEMON_NUM_THREADS);
}


// returns 0 if everything has been initialized correctly and -1 otherwise
int initialize_resources(char *ifname)
{
  int res = 0;
  int t;

  // lets open the mmi64 communications interface with the HN system
#ifndef HN_DAEMON_MMI64_DISABLE  
  if(hn_iface_mmi64_init(ifname) != 0)
  {
    log_error("initializing mmi64 communications interface");
    res = -1;
  }
  else
  {
    log_info("%s interface succesfully initialized", ifname);
  }
#else
  log_warn("MMI64 interface disabled");
#endif  

  // let's open the application server sockets 
#ifdef HN_DAEMON_SOCKET_UNIX
  if (hn_socket_unix_initialize(HN_DAEMON_SOCKET_UNIX_PORT, &sock_unix_info) < 0) 
  {
    log_error("initializing UNIX socket port %s: %s", HN_DAEMON_SOCKET_UNIX_PORT, strerror(errno));
    res = -1;
  }
  else
  {
    log_debug("hn_daemon socket unix successfully initialized");
  }
#endif

#ifdef HN_DAEMON_SOCKET_INET
  assert(0);  // FIXME: socket inet not supported yet. 
  if (hn_socket_inet_initialize(HN_DAEMON_SOCKET_INET_PORT, &sock_inet_info) < 0) 
  {
    log_debug("initializing INET socket port %d: %s", port, strerror(errno));
    res = -1;
  }
  else
  {
    log_debug("hn_daemon socket inet successfully initialized");
  }

#endif

  // let's create the shared client list
  int lst = hn_list_create_list();
  if (lst == -1)
  {
    log_error("client list could not be created");
    res = -1;
  } 
  else
  {
    client_list = (unsigned int)lst;
    log_debug("hn_daemon: client list created with id %u", client_list);
  }

  //// let's create the downstream channel list
  //lst = hn_list_create_list();
  //if (lst == -1)
  //{
  //  log_error("downstream channel list could not be created");
  //  res = -1;
  //} 
  //else
  //{
  //  downstream_channel_list = (unsigned int)lst;
  //  log_debug("hn_daemon: downstream channel list created with id %u", downstream_channel_list);
  //}


  // let's create the upstream channel list
  lst = hn_list_create_list();
  if (lst == -1)
  {
    log_error("hn_daemon, upstream channel list could not be created");
    res = -1;
  } 
  else
  {
    upstream_channel_list = (unsigned int)lst;
    log_debug("hn_daemon: upstream channel list created with id %u", upstream_channel_list);
  }


  unsigned int iter;

  //// Downstream generic items channels
  //for (iter = hn_list_iter_front_begin(downstream_module_list); !hn_list_iter_end(downstream_module_list, iter); iter = hn_list_iter_next(downstream_module_list, iter))
  //{
  //  hn_mmi64_module_t *mmi64_module = hn_list_get_data_at_position(downstream_module_list, iter);
  //
  //  if (mmi64_module->type == GENERIC_ITEM_DOWNSTREAM) {
  //    hn_data_burst_channel_t *downstream_channel = malloc(sizeof(hn_data_burst_channel_t));
  //
  //    downstream_channel->locked       = 0;
  //    downstream_channel->fifo         = hn_buffer_fifo_create(4);
  //    downstream_channel->module.id    = mmi64_module->id;
  //    downstream_channel->module.type  = mmi64_module->type;
  //    downstream_channel->module.index = mmi64_module->index;
  //
  //    // get VN ID associated with this IO module
  //    //hn_iface_mmi64_get_vn_id_for_fifo(FIFO_TYPE_DOWNSTREAM ,downstream_channel->module.index, &downstream_channel->vn_id);
  //    log_warn("Downstream item channel for module %u, assigned vn 0", mmi64_module->id);
  //    downstream_channel->vn_id = 0;
  //
  //    log_debug("downstream generic fifo created with id %u  on VN %d", downstream_channel->fifo, downstream_channel->vn_id);
  //
  //    hn_list_add_to_end(downstream_channel_list, downstream_channel);
  //  }
  //}
  //
  //hn_list_iter_end(downstream_module_list, HN_LIST_ITER_END);
  //
  //// Downstream DATA BURST channel
  //for (iter = hn_list_iter_front_begin(downstream_module_list); !hn_list_iter_end(downstream_module_list, iter); iter = hn_list_iter_next(downstream_module_list, iter))
  //{
  //  hn_mmi64_module_t *mmi64_module = hn_list_get_data_at_position(downstream_module_list, iter);
  //
  //  if (mmi64_module->type == BURST_TRANSFER_DOWNSTREAM) {
  //    hn_data_burst_channel_t *downstream_channel = malloc(sizeof(hn_data_burst_channel_t));
  //
  //    downstream_channel->locked       = 0;
  //    downstream_channel->fifo         = hn_buffer_fifo_create(sizeof(hn_shm_buffer_transfer_t));
  //    downstream_channel->module.id    = mmi64_module->id;
  //    downstream_channel->module.type  = mmi64_module->type;
  //    downstream_channel->module.index = mmi64_module->index;
  //
  //    // get VN ID associated with this IO module
  //    hn_iface_mmi64_get_vn_id_for_fifo(FIFO_TYPE_DOWNSTREAM ,downstream_channel->module.index, &downstream_channel->vn_id);
  //
  //    log_debug("downstream shm fifo created with id %u  on VN %d", downstream_channel->fifo, downstream_channel->vn_id);
  //
  //    hn_list_add_to_end(downstream_channel_list, downstream_channel);
  //  }
  //}
  //hn_list_iter_end(downstream_module_list, HN_LIST_ITER_END);
   // Let's initialize the to_process_item queue
 
  int fifo;
  // items fifo
  fifo =  hn_buffer_fifo_create(4);
  
  if (fifo < 0)
  {
    log_error("hn_daemon, fifo for generic items in downstream not initialized");
    res = -1;
  }
  else
  {
    log_debug("hn_daemon, fifo for generic items in downstream to HN system succesfully initialized with id %d", fifo);
  }
  downstream_item_command_fifo = (unsigned int)fifo;

  // burst transfers commands fifo
  fifo = hn_buffer_fifo_create(sizeof(hn_shm_buffer_transfer_t));
  if (fifo < 0)
  {
    log_error("hn_daemon, fifo for burst transfer operations in downstream not initialized");
    res = -1;
  }
  else
  {
    log_debug("hn_daemon, fifo for burst transfer operation in downstream to HN system succesfully initialized with id %d", fifo);
  }
  downstream_bursttransfer_command_fifo = fifo;
  
  // burst transfers commands fifo
  fifo = hn_buffer_fifo_create(sizeof(hn_shm_buffer_transfer_t));
  if (fifo < 0)
  {
    log_error("hn_daemon, fifo for burst transfer operations in downstream not initialized");
    res = -1;
  }
  else
  {
    log_debug("hn_daemon, fifo for burst transfer operation in downstream to HN system succesfully initialized with id %d", fifo);
  }
  upstream_bursttransfer_command_fifo = fifo;

  int shm_mutex = pthread_mutex_init( &(shm_running_transfers_table.mutex), NULL );
  if (shm_mutex != 0) {
    log_error("hn_daemon, mutex for accessing shm downstream transfers table access not initialized");
    res = -1;
  }
  else
  {
    uint32_t num_channs;
    uint32_t rv;
    uint32_t ind_chann;
    uint32_t ind_entry;

    log_debug("hn_daemon, mutex for accessing shm downstream transfers table succesfully intialized");
    //let's initialize the table
    rv = hn_get_number_of_io_modules_of_type(BURST_TRANSFER_DOWNSTREAM, &num_channs);
    if(rv != 0) {
      num_channs = 0;
      log_error("Error getting number of downstream burst channels");
      res = -1;
    }
   
   log_debug("hn_daemon, creating shm ongoing transfers table for %u channels, %d entries per channel\n",
        num_channs, MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL);
    shm_running_transfers_table.number_of_physical_channels = num_channs;
    if(num_channs != 0){
      pthread_mutex_lock( &(shm_running_transfers_table.mutex) );
      shm_running_transfers_table.channel = malloc(num_channs *sizeof(hn_shm_running_transfers_table_channel_entry_t));
      for(ind_chann = 0; ind_chann < shm_running_transfers_table.number_of_physical_channels; ind_chann++)
      {
        shm_running_transfers_table.channel[ind_chann].downstream_free_entries  = MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL;
        shm_running_transfers_table.channel[ind_chann].upstream_free_entries    = MAX_CONCURRENT_BURST_TRANSFERS_PER_PHYS_CHANNEL;
        shm_running_transfers_table.channel[ind_chann].locked_for_ack_transfers = 0;

        for (ind_entry = 0; ind_entry < shm_running_transfers_table.channel[ind_chann].downstream_free_entries; ind_entry++)
        {
          shm_running_transfers_table.channel[ind_chann].downstream_entries[ind_entry].free     = 1;
          shm_running_transfers_table.channel[ind_chann].downstream_entries[ind_entry].finished = 0;
        }

        for (ind_entry = 0; ind_entry < shm_running_transfers_table.channel[ind_chann].upstream_free_entries; ind_entry++)
        {
          shm_running_transfers_table.channel[ind_chann].upstream_entries[ind_entry].free     = 1;
          shm_running_transfers_table.channel[ind_chann].upstream_entries[ind_entry].finished = 0;
        }
      }
      pthread_mutex_unlock( &(shm_running_transfers_table.mutex) );
    }
  }

  
  //-----
  for (iter = hn_list_iter_front_begin(upstream_module_list); !hn_list_iter_end(upstream_module_list, iter); iter = hn_list_iter_next(upstream_module_list, iter))
  {
    hn_mmi64_module_t *mmi64_module = hn_list_get_data_at_position(upstream_module_list, iter);

    if (mmi64_module->type == BURST_TRANSFER_UPSTREAM) {
      hn_data_burst_channel_t *upstream_channel = malloc(sizeof(hn_data_burst_channel_t));

      upstream_channel->locked       = 0;
      upstream_channel->module.id    = mmi64_module->id;
      upstream_channel->module.type  = mmi64_module->type;
      upstream_channel->module.index = mmi64_module->index;

      // get VN ID associated with this IO module
      hn_iface_mmi64_get_vn_id_for_fifo(FIFO_TYPE_UPSTREAM ,upstream_channel->module.index, &upstream_channel->vn_id);

      log_debug("hn_daemon, upstream shm fifo created on VN %d", upstream_channel->vn_id);

      hn_list_add_to_end(upstream_channel_list, upstream_channel);
    }
  }

  hn_list_iter_end(upstream_module_list, HN_LIST_ITER_END);

  // let's initialize the locks for the access to the profpga/mmi64 hardware resources
  int prof_mutex_rv = pthread_mutex_init(&mutex_write_mmi64, NULL);
  if (prof_mutex_rv != 0) {
    log_error("hn_daemon, mutex for mmi64 hardware access in write mode not initialized");
    res = -1;
  }
  else {
    log_debug("mutex for mmi64 hardware access in write mode succesfully intialized");
  }
  
  
  prof_mutex_rv = pthread_mutex_init(& mutex_read_mmi64, NULL);
  if (prof_mutex_rv != 0) {
    log_error("hn_daemon, mutex for mmi64 hardware access in read mode not initialized");
    res = -1;
  }
  else {
    log_debug("mutex for mmi64 hardware access in read mode succesfully intialized");
  }


  // Let's initialize the register, memory locks and rscmgt lock, check for error on locks initialization
  int rs_mutex =pthread_mutex_init( &mutex_access_variables, NULL );
  if (rs_mutex != 0) {
    log_error("hn_daemon, mutex for shared variables access not initialized");
    res = -1;
  }
  else
  {
    log_debug("locks for thread-safe access of shared data succesfully intialized");
  }
  for (t=0;t<HN_MAX_TILES;t++) {
    lock_tile_register_access[t] = 0;
    lock_memory[t] = 0;
  }
  lock_rscmgt_access = 0;


  int s = pthread_cond_init(&cond_rscmgt_init, NULL);
  if (s != 0) 
  {
    log_error("initializing condition variable for resource manager initialization procedure");
    res = -1;
  }

  // Let's initialize the to_process_item queue
  int bfr =  hn_buffer_fifo_create(sizeof(uint32_t));
  if (bfr < 0)
  {
    log_error("queues of generic items for upstream channel not initialized");
    res = -1;
  }
  else
  {
    log_debug("buffer for generic items comming from HN system succesfully initialized with id %d", bfr);
  }
  to_process_item_queue = (unsigned int)bfr;

  // Let's initialize the debug_item queue
  bfr =  hn_buffer_fifo_create(sizeof(uint32_t));
  if (bfr < 0)
  {
    log_error("queues of debug items for upstream channel not initialized");
    res = -1;
  }
  else
  {
    log_debug("buffer for debug items comming from HN system succesfully initialized with id %d", bfr);
  }
  debug_item_queue = (unsigned int)bfr;

  // Let's initialize the rscmgt_fifo
  bfr =  hn_buffer_fifo_create(sizeof(hn_data_rsc_t));
  if (bfr < 0)
  {
    log_error("resource manager fifo not initialized");
    res = -1;
  }
  else
  {
    log_debug("resource manager fifo succesfully initialized with id %d", bfr);
  }
  rscmgt_fifo = (unsigned int)bfr;

  // let's create shared memory buffers list
  lst = hn_list_create_list();
  if (lst == -1)
  {
    log_error("shared memory buffers list could not be created");
    res = -1;
  } 
  else
  {
    shared_memory_buffer_list = (unsigned int)lst;
    log_debug("hn_daemon: shared memory buffers list created with id %u", shared_memory_buffer_list);
  }

  // Let's initialize the shared memory notification fifo
  bfr =  hn_buffer_fifo_create(sizeof(hn_command_shm_buffer_manage_status_t));
  if (bfr < 0)
  {
    log_error("shared memory fifo not initialized");
    res = -1;
  }
  else
  {
    log_debug("shared memory notification fifo succesfully initialized");
  }
  shm_notification_fifo = (unsigned int)bfr;

  // Let's initialize the temperature notification fifo
  bfr =  hn_buffer_fifo_create(sizeof(hn_command_temperature_response_t));
  if (bfr < 0)
  {
    log_error("temperature notification fifo not initialized");
    res = -1;
  }
  else
  {
    log_debug("temperature notification fifo succesfully initialized");
  }
  temperature_notification_fifo = (unsigned int)bfr;


  // Let's initialize the stats notification fifo
  bfr =  hn_buffer_fifo_create(sizeof(hn_command_stats_monitor_response_t));
  if (bfr < 0)
  {
    log_error("statistics monitor notification fifo not initialized");
    res = -1;
  }
  else
  {
    log_debug("statistics notification fifo succesfully initialized");
  }
  stats_monitor_notification_fifo = (unsigned int)bfr;
 
  // Let's initialize the lock for the stats monitor table
  int statsmon_mutex_rv = pthread_mutex_init(&mutex_stats_monitor_table, NULL);
  if (statsmon_mutex_rv != 0) {
    log_error("mutex for statistics monitor table access not initialized");
    res = -1;
  }
  else {
    log_debug("mutex for statistics monitor table access succesfully intialized");
  }

  // Let's initialize the lock for the stats monitor
  statsmon_mutex_rv = pthread_mutex_init(&mutex_stats_monitor_config_access, NULL);
  if (statsmon_mutex_rv != 0) {
    log_error("mutex for statistics monitor config parameters not initialized");
    res = -1;
  }
  else {
    log_debug("mutex for statistics monitor config_parameters succesfully intialized");
  }

  // let's create the list of tiles for stats automatic monitorization
  lst = hn_list_create_list();
  if (lst == -1) {
    log_error("stats monitor list for tiles could not be created");
    res = -1;
  } 
  else {
    stats_monitor_tiles_list = (unsigned int)lst;
    log_debug("list of tiles for stats monitor created with id %u", stats_monitor_tiles_list);
  }

  s = pthread_cond_init(&cond_stats_monitor_config_change, NULL);
  if (s != 0) 
  {
    log_error("initializing condition variable for statistisc monitor change notification");
    res = -1;
  }
  else {
    log_debug("condition variable for statistisc monitor change notification successfully initialized");
  }

  s = pthread_cond_init(&cond_collector_init, NULL);
  if (s != 0) 
  {
    log_error("initializing condition variable daemon initialized notification to stats_monitor");
    res = -1;
  }
  else {
    log_debug("condition variable of daemon running for statistisc monitor successfully initialized");
  }


  // end of implementationfor stats monitor

  log_info("initializing resource manager...");
#ifdef HN_DAEMON_MMI64_DISABLE  
  hn_rscmgt_initialize(HN_RESOURCE_MANAGER_INIT_MODE_NO_FPGA);
#else
  hn_rscmgt_initialize(HN_RESOURCE_MANAGER_INIT_MODE_FPGA);
#endif

  return res;
}


// destroyes the resources initilized through initialize_resources()
void destroy_resources()
{
  log_info("Releasing resources");
  
  // let's free the created resources
#ifdef HN_DAEMON_SOCKET_UNIX
  hn_socket_unix_close(&sock_unix_info);
#endif
#ifdef HN_DAEMON_SOCKET_INET
  hn_socket_inet_close(&sock_inet_info);
#endif
  log_info("hn_daemon socket closed");

  // let's destroy the client list
  hn_list_destroy_list(client_list);
  log_debug("hn_daemon: destroyed client_list %u", client_list);

  pthread_mutex_destroy( &(shm_running_transfers_table.mutex));
  log_debug("hn_daemon: destroyed downstream running shm transfers table mutex");
  free(shm_running_transfers_table.channel);
  log_debug("hn_daemon: destroyed downstream running shm transfers table ");

  // lets destroy the downstream fifo for items
  hn_buffer_fifo_destroy(downstream_item_command_fifo);
  log_debug("hn_daemon: destroyed downstream fifo buffer for items %u", downstream_item_command_fifo);
  // lets destroy the downstream fifo for burst commands
  hn_buffer_fifo_destroy(downstream_bursttransfer_command_fifo);
  log_debug("hn_daemon: destroyed downstream fifo buffer for burst transfers %u", downstream_bursttransfer_command_fifo);
  hn_buffer_fifo_destroy(upstream_bursttransfer_command_fifo);
  log_debug("hn_daemon: destroyed upstream fifo buffer for burst transfers %u", upstream_bursttransfer_command_fifo);
  
  // let's destroy channel list
  //hn_list_destroy_list(downstream_channel_list);
  //log_debug("hn_daemon: destroyed downstream_channel_list %u", downstream_channel_list);
  hn_list_destroy_list(upstream_channel_list);
  log_debug("hn_daemon: destroyed upstream_channel_list %u", upstream_channel_list);

  // Let's destroy the to_process_item_queue
  hn_buffer_fifo_destroy(to_process_item_queue);
  log_debug("hn_daemon: destroyed to_process_item_queue %u", to_process_item_queue);

  // Let's destroy the debug_item_queue
  hn_buffer_fifo_destroy(debug_item_queue);
  log_debug("hn_daemon: destroyed debug_item_queue %u", debug_item_queue);

  // let's destroy the resource manager fifo
  hn_buffer_fifo_destroy(rscmgt_fifo);
  log_debug("hn_daemon: destroyed rscmgt_fifo %u", rscmgt_fifo);

  while (!hn_list_is_empty(shared_memory_buffer_list)) { 
    int iter; 
    uint32_t rdu; 
    hn_shm_buffer_info_t *shm_buffer; 
    iter = hn_list_iter_front_begin(shared_memory_buffer_list); 
    shm_buffer = (hn_shm_buffer_info_t *)hn_list_get_data_at_position(shared_memory_buffer_list, iter); 
    rdu = hn_shared_memory_destroy_buffer(shm_buffer->name, shm_buffer->size, shm_buffer->addr, shm_buffer->fd); 
    if (rdu != 0) { 
      log_error("hn_daemon: destroying resources: removing shared memory buffer %s", shm_buffer->name); 
    } 
    // close the list, to release semaphores 
    hn_list_iter_end(shared_memory_buffer_list, HN_LIST_ITER_END); 
    hn_list_remove_head(shared_memory_buffer_list); 
  }

  // let's destroy the shared memory buffers information list
  hn_list_destroy_list(shared_memory_buffer_list);
  log_debug("hn_daemon: destroyed shared_memory_buffer_list %u", shared_memory_buffer_list);

  // lets destroy the shared memory notificiation fifo
  hn_buffer_fifo_destroy(shm_notification_fifo);
  log_debug("hn_daemon: destroyed shm_notification_fifo %u", shm_notification_fifo);

  // lets destroy the temperature notificiation fifo
  hn_buffer_fifo_destroy(temperature_notification_fifo);
  log_debug("hn_daemon: destroyed temperature_notification_fifo %u", temperature_notification_fifo);

  // lets destroy the statistics monitor notification fifo
  hn_buffer_fifo_destroy(stats_monitor_notification_fifo);
  log_debug("hn_daemon: destroyed stats_monitor_notification_fifo %u", stats_monitor_notification_fifo);
  
  // let's destroy the mutexes of the statistics monitor table
  pthread_mutex_destroy(&mutex_stats_monitor_table);
  log_debug("hn_daemon: destroyed stats_monitor_table mutex");
  
  // lets destroy the statistics monitor for the stats_monitor configuration parameters access
  pthread_mutex_destroy(&mutex_stats_monitor_config_access);
  log_debug("hn_daemon: destroyed stats_monitor_config_access mutex");
  
  // lets destroy the statistics monitor condition variable that notifies any config change to the stats_monitor thread
  pthread_cond_destroy(&cond_stats_monitor_config_change);
  log_debug("hn_daemon: destroyed stats_monitor_config_change condition variable");
  
  // lets destroy the condition variable that notifies to the stats_monitor thread that the daemon in running
  pthread_cond_destroy(&cond_collector_init);
  log_debug("hn_daemon: destroyed cond_collector_init condition variable");
  
  // let's destroy the tiles list for stats monitor
  hn_list_destroy_list(stats_monitor_tiles_list);
  log_debug("hn_daemon: destroyed stats monitor tiles list %u", stats_monitor_tiles_list);
  
  // lets destroy the mutexes to access the prodesign communications interface profpga and mmi64
  pthread_mutex_destroy(&mutex_write_mmi64);
  pthread_mutex_destroy(&mutex_read_mmi64);
  log_debug("hn_daemon: destroyed mutexes for profpga communication interface access");

  log_info("hn_resources released");
 
  // let's destroy the condition variable used in the resource manager initialization procedure
  // between collector and dispatcher
  pthread_cond_destroy(&cond_rscmgt_init);

  // let's destroy the register locks
  pthread_mutex_destroy( &mutex_access_variables );

#ifndef HN_DAEMON_MMI64_DISABLE
  // this is the last thing to as it could be got stacked.
  hn_iface_mmi64_release();
  log_info("mmi64 communications interface released");
#endif

  log_info("closing resource manager");
  hn_rscmgt_end();

  log_info("all done");
}


