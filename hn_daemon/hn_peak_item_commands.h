#ifndef __HN_PEAK_ITEM_COMMANDS_H__
#define __HN_PEAK_ITEM_COMMANDS_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: February 28, 2018
// File Name: hn_peak_item_commands.h
// Design Name:
// Module Name: HN DAEMON 
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    Here, we define the item commands for PEAK
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*! 
 * \name Upstream commands
 *
 * When adding a new upstream command remember to add
 * an entry in the enum \see{#hn_item_command_type_t} 
 * of the hn_item_handler.h and create the item handler
 * function in hn_item_handler.c 
 * (follow same pattern as for the others)
 */
//@{
//
 

//! Console command type
//#define HN_PEAK_COMMAND_CONSOLE                         0

// TODO let's comment those defines as the above and prefix with HN_PEAK


//
//@}

//! \name Downstream commands
//@{
//

#define COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS  33
#define COMMAND_PEAK_WRITE_PROTOCOL             42
#define COMMAND_PEAK_CONFIGURE_TILE             34
#define COMMAND_PEAK_SET_PROCESSOR_ADDRESS      38
#define COMMAND_PEAK_KEY_INTERRUPT              45
#define COMMAND_PEAK_READ_REGISTER              35 
#define COMMAND_PEAK_WRITE_REGISTER             36

// ITEM values for specific actions
#define PEAK_ENABLE_CORE_DEBUG_VALUE             0x0000       // TO FIX
#define PEAK_DISABLE_CORE_DEBUG_VALUE            0x0000       // TO FIX
#define PEAK_ENABLE_CACHE_CONTROLLER_DEBUG_VALUE 0x0000       // TO FIX
#define PEAK_DISABLE_CACHE_CONTROLLER_DEBUG_VALUE 0x0000       // TO FIX
#define PEAK_ENABLE_MEMORY_DEBUG_VALUE           0x0000       // TO FIX
#define PEAK_DISABLE_MEMORY_DEBUG_VALUE          0x0000       // TO FIX

//@}

#endif
