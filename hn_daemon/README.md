# HN DAEMON

This is the daemon used by the HN library for connecting to the HN system

# Compilation environment variables

1) FOREGROUND=y, for compiling the daemon as a foreground process

  FOREGROUND=y make

2) build_type={Debug/Verbose}, for enabling debug and verbose modes

3) daemon_log_mode=FILE, for storing all the log messages to a file
   The file can be provided with daemon_log_file variable. By default
   is set to /tmp/hn_daemon.log

4) legacy_support=yes, for compiling daemon for proFPGA tools previous to 2018
   for proFPGA2018 and later versions set legacy_support=no

## Compilation example

   make clean && make build_type=Debug FOREGROUND=y legacy_support=no daemon_log_mode=FILE make


