#ifndef __HN_BUFFER_FIFO_H__
#define __HN_BUFFER_FIFO_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 23, 2018
// File Name: hn_buffer_fifo.h
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    HN Buffer in FIFO mode
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stddef.h>

//! Maximum fifo size in bytes
#define HN_BUFFER_FIFO_MAX_SIZE  1<<25

/*!
 * \brief Possible states in a FIFO
 */
typedef enum
{
  HN_BUFFER_FIFO_STATE_EMPTY = 0,
  HN_BUFFER_FIFO_STATE_FULL,
  HN_BUFFER_FIFO_STATE_OK
}hn_buffer_fifo_state_t;

/*! 
 * \brief The Buffer FIFO structure
 *
 * The Buffer FIFO structure is thread safe through the hn_buffer_fifo.c code
 */
typedef struct hn_buffer_fifo_st
{
  unsigned char           data[HN_BUFFER_FIFO_MAX_SIZE];   //!< raw fifo data
  unsigned int            rd_ptr;                          //!< write pointer
  unsigned int            wr_ptr;                          //!< read pointer
  hn_buffer_fifo_state_t  state;                           //!< state of the FIFO
  size_t                  element_size;                    //!< size of each element (in bytes) in this fifo
}hn_buffer_fifo_t;

//@{
//! \name Creation and destruction buffer FIFO functions

/*!
 * \brief Creates a new buffer fifo in the list of internal HN FIFOs
 *
 * \param [in] ele_size the size of each element in the fifo
 * \return The Id of the created FIFO
 * \retval -1 In case of failure, p.e., when all the ids are busy 
 */
int hn_buffer_fifo_create(size_t ele_size);

/*!
 * \brief Destroyes the resources used by the Buffer FIFO with the given Id
 *
 * \param [in] id the Buffer FIFO id
 * \retval  0 In case of success
 * \retval -1 The id is not occupied by a Buffer FIFO
 */
int hn_buffer_fifo_destroy(unsigned int id);

//@}


//@{
//! \name misc

/*!
 * \brief Gets the number of bytes in the given buffer FIFO
 *
 * \param [in] id the Id of the buffer FIFO
 * \return the number of bytes in the FIFO that can be read
 */
size_t hn_buffer_fifo_get_num_bytes(unsigned int id);

/*!
 * \brief Gets the number of elements in the given buffer FIFO
 *
 * \param [in] id the Id of the buffer FIFO
 * \return the number of elements in the FIFO that can be read
 */
size_t hn_buffer_fifo_get_num_elements(unsigned int id);

/*!
 * \brief Gets the element size of the given Buffer FIFO
 *
 * \param [in] id the Id fo the buffer FIFO
 * \returns the size of the elemens in the FIFO or 0 if the FIFO it is not a valid Buffer FIFO id
 */
size_t hn_buffer_fifo_get_element_size(unsigned int id);


/*!
 * \brief Gets a constant data pointer to the data
 *
 * Data cannot be modified through this pointer
 *
 * \param [in] id the Id of the buffer FIFO
 * \returns a pointer to the data in the read position of the FIFO
 */
const void *hn_buffer_fifo_get_element(unsigned int id);

//@}



//@{
//! \name Read from buffer FIFO functions

/*!
 * \brief Reads a chunk of elements from the Buffer FIFO with the given Id
 *
 * \param [in] id The Buffer FIFO Id
 * \param [out] buf Buffer where read data will be stored
 * \param [in] num_ele The number of elements to read
 * \return the number of elements read actually
 * */
size_t hn_buffer_fifo_read_elements_to_buf(unsigned int id, void *buf, size_t num_ele);


/*!
 * \brief Reads a chunk of bytes from the FIFO with the given Id
 *
 * \param [in] id The Buffer FIFO Id.
 * \param [out] buf The buffer the FIFO content read will be written to
 * \param [in] num_bytes The number of bytes to read
 * \return the number of bytes read actually
 */
size_t hn_buffer_fifo_read_bytes_to_buf(unsigned int id, void *buf, size_t num_bytes);


/*!
 * \brief Reads a chunk of elements from the FIFO with the given Id and writes them to the given buffer FIFO
 *
 * \param [in] id The Buffer FIFO Id. where the data will be read from
 * \param [out] buf The buffer FIFO Id where the content read will be written to
 * \param [in] num_ele The number of elements to read in the size of the id_src buffer FIFO Id
 * \return the number of elements read actually
 */
size_t hn_buffer_fifo_read_elements_to_fifo(unsigned int id_src, unsigned int id_dst, size_t num_ele);


/*!
 * \brief Reads a chunk of bytes from the FIFO with the given Id and writes them to the given buffer FIFO
 *
 * \param [in] id_src The Buffer FIFO Id. where the data will be read from
 * \param [in] id_dst The buffer FIFO Id where the content read will be written to
 * \param [in] num_bytes The number of bytes to read
 * \return the number of bytes read actually
 */
size_t hn_buffer_fifo_read_bytes_to_fifo(unsigned int id_src, unsigned int id_dst, size_t num_bytes);

//@}

//@{
//! \name Copy from buffer FIFO functions

/*!
 * \brief Copies a chunk of elements from the Buffer FIFO with the given Id
 *
 * \param [in] id The Buffer FIFO Id
 * \param [out] buf Buffer where read data will be stored
 * \param [in] num_ele The number of elements to read
 * \return the number of elements read actually
 * */
size_t hn_buffer_fifo_copy_elements_to_buf(unsigned int id, void *buf, size_t num_ele);

/*!
 * \brief Copies a chunk of bytes from the Buffer FIFO with the given Id
 *
 * \param [in] id The Buffer FIFO Id
 * \param [out] buf Buffer where read data will be stored
 * \param [in] num_bytes The number of bytes to read
 * \return the number of bytes read actually
 * */
size_t hn_buffer_fifo_copy_bytes_to_buf(unsigned int id, void *buf, size_t num_bytes);


//@}


//@{
//! \name Write to buffer FIFO functions

/*!
 * \brief Writes a chunk of elements to the Buffer FIFO with the given Id
 *
 * \param [in] id The Buffer FIFO Id.
 * \param [in] buf The buffer the content is going to be written to the Buffer FIFO
 * \param [in] num_ele The number of elements to write
 * \returns the number of elements written actually
 */
size_t hn_buffer_fifo_write_elements_from_buf(unsigned int id, void *buf, size_t num_ele);


/*!
 * \brief Writes a chunk of bytes to the Buffer FIFO with the given Id
 *
 * \param [in] id The Buffer FIFO Id
 * \param [in] buf The buffer the content is going to be written to the Buffer FIFO
 * \param [in] num_bytes The number of bytes to write
 * \return the number of bytes written actually
 */
size_t hn_buffer_fifo_write_bytes_from_buf(unsigned int id, void *buf, size_t num_bytes);


/*!
 * \brief Writes a chunk of elements from the given Buffer FIFO to the Buffer FIFO with the given Id
 *
 * \param [in] id_dst The Buffer FIFO Id in which the data will be written
 * \param [in] id_src The buffer FIFO Id the content is going to be written to the Buffer FIFO 'id_dst'
 * \param [in] num_ele The number of elements to write in the size of the id_src Buffer FIFO Id
 * \return the number of elements written actually
 */
size_t hn_buffer_fifo_write_elements_from_fifo(unsigned int id_dst, unsigned int id_src, size_t num_ele);

/*!
 * \brief Writes a chunk of bytes from the given Buffer FIFO to the Buffer FIFO with the given Id
 *
 * \param [in] id_dst The Buffer FIFO Id in which the data will be written
 * \param [in] id_src The buffer FIFO Id the content is going to be written to the Buffer FIFO 'id_dst'
 * \param [in] num_bytes The number of bytes to write
 * \return the number of bytes written actually
 */
size_t hn_buffer_fifo_write_bytes_from_fifo(unsigned int id_dst, unsigned int id_src, size_t num_bytes);

//@}

#endif
