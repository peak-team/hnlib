///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Flich (jflich@disca.upv.es)
//
// Create Date: June 5, 2018
// File Name: hn_thread_stats_monitor_function.h
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    thread to manage statistics gathering from HN system
//    Stores statistics into a shared structure of the hn_daemon, so upon a read stats requests the collector will read
//    the table and it will return the data
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

uint32_t hn_stats_monitor_reg_is_for_stats(uint32_t tile, uint32_t reg_addr);
uint32_t hn_stats_monitor_add_client_for_tile_stats(uint32_t tile_id, unsigned long long cliend_id);
uint32_t hn_stats_monitor_remove_client_for_tile_stats(uint32_t tile_id, unsigned long long cliend_id);
uint32_t hn_stats_monitor_get_num_clients_for_tile_stats(uint32_t tile_id, uint32_t *num_clients);

/* 
 * Main thread of the stats monitor.
 * This function creates another thread that will trigger statistics read from tiles when stats are enabled and 
 * loops, waiting configuration data:
 *   add tile
 *   remove tile
 *   statistics monitoring period
 */
void *hn_thread_stats_monitor_function(void *args);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// End of file hn_thread_stats_monitor_function.h
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
