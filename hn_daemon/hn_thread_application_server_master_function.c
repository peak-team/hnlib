///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 26, 2018
// File Name: hn_thread_application_server_master_function.c
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Application server master thread implementation.
//
//    This thread listen to the connection socket for the apps and creates the 
//    slave to communicate and work with them
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define _GNU_SOURCE 

#include <sys/socket.h>
#include <sys/select.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#define _HN_SHARED_SERVER_MASTER_DEFINITION
#include "hn_list.h"
#include "hn_logging.h"
#include "hn_buffer_fifo.h"
#include "hn_exit_program.h"
#include "hn_request_handler.h"
#include "hn_shared_structures.h"

#include "hn_thread_application_server_master_function.h"
#include "hn_thread_application_server_slave_function.h"
#include "hn_thread_stats_monitor_function.h"

// macros
#define SOCKET_ID_STATS_MONITOR ((int)-1)


// structure the master thread keeps for every slave
typedef struct thread_slave_info_st
{ // BEWARE!!!! hn_application_server_slave_args_t should be the first field in the struct
  hn_thread_application_server_slave_args_t slave_args;  // arguments required by every app server slave thread
  pthread_t tid;                                         // id of the thread
  int       *retval;                                     // exit status of the thread
}thread_slave_info_t;


/*
 * Creates a new slave thread
 */
static int create_slave(int slave_socket);

/*
 * Joins the slave threads
 */
static int join_slaves();

/*
 * Remove RIP clients
 */
static void remove_client(int cli);



// Unique client identifer generator 
static unsigned long long int unique_client_id = 0;


// the application server master thread entry point
void *hn_thread_application_server_master_function(void *args)
{
  hn_thread_application_server_master_args_t *targs = (hn_thread_application_server_master_args_t *)args;

  int slave_socket;
  int server_socket_unix = targs->sock_un.sock;
  static int retval = 0;

  // at the moment only socket unix

  log_info("application_server_master_thread: ready...");

  // for select+accept,
  fd_set rfds;
  struct timeval tv;
  int select_rs;


  // create Virtual client for automated statistics monitoring
  if (create_slave(SOCKET_ID_STATS_MONITOR) == -1)
  {
    log_error("Stats monitor virtual client could not be created");
    log_error("Terminate hn_daemon");
    hn_exit_program_terminate(1);
  }

  listen(server_socket_unix, 5);
  while (!hn_exit_program_is_terminated())
  {
    FD_ZERO(&rfds);
    FD_SET(server_socket_unix, &rfds);
    /* wait upto 5 s */
    tv.tv_sec = 5;
    tv.tv_usec = 0;

    select_rs = select(server_socket_unix+1, &rfds, NULL, NULL, &tv);
    if (select_rs > 0)
    {
      // something to read in the socket
      slave_socket = accept(server_socket_unix, 0, 0);
      if (slave_socket == -1)
      {
        log_error("Slave socket could not be created: %s", strerror(errno));
      }
      else
      {
        // creation of the slave thread
        if (create_slave(slave_socket) == -1)
        {
          /*
           * something wrong creating the slave thread
           * let's finish the connection with the application client
           */
          close(slave_socket);
        }
      }
    }

    // Let's see if some slave thread has terminated to remove from the list
    join_slaves();
  }

  // termination program flagged, let's join the created threads before returning
  join_slaves();

  log_info("application_server_master_thread: exiting...");
  return &retval;
}

// creates a new application server slave for communicate with the new connected application client
int create_slave(int slave_socket)
{
  int res = -1;
  int input_fifo = -1;
  int output_fifo = -1;

  thread_slave_info_t *tinfo = (thread_slave_info_t *)malloc(sizeof(thread_slave_info_t));
  if (tinfo != NULL)
  {

    // let's create the fifos to communicate with the dispatcher and the collector threads
    input_fifo = hn_buffer_fifo_create(1);
    output_fifo = hn_buffer_fifo_create(sizeof(hn_request_t));

    if (input_fifo < 0)
      log_error("application_server_master_thread: creating slave: input FIFO for application server slave could not be create");

    if (output_fifo < 0)
      log_error("application_server_master_thread: creating slave: output FIFO for application server slave could not be created");

    if ((input_fifo >= 0) && (output_fifo >= 0))
    { // let's fill in the thread info fields
      tinfo->slave_args.sock = slave_socket;
      tinfo->slave_args.client_info.input_fifo = input_fifo;
      tinfo->slave_args.client_info.output_fifo = output_fifo;

      // FIXME could we run out of client ids? I do not think so... 
      // A better approach would be to use the tinfo.tid field
      tinfo->slave_args.client_info.cid = unique_client_id++;
      tinfo->slave_args.client_info.health_status = HN_CLIENT_HEALTH_STATUS_BORN;
      pthread_mutex_init(&tinfo->slave_args.client_info.health_mtx, NULL);
      pthread_cond_init(&tinfo->slave_args.client_info.health_cond, NULL);

      // let's create the slave thread
      tinfo->retval = NULL;
      int rs;
      if (slave_socket == SOCKET_ID_STATS_MONITOR) {
        hn_stats_monitor_client_id = tinfo->slave_args.client_info.cid;
        log_debug("application_server_master_thread: setting stats_mon client id: %lu", hn_stats_monitor_client_id);
        rs = pthread_create(&(tinfo->tid), NULL, &hn_thread_stats_monitor_function, tinfo);

      } else {
        rs = pthread_create(&(tinfo->tid), NULL, &hn_thread_application_server_slave_function, tinfo);
      }

      if (rs != 0)
      {
        log_error("application_server_master_thread: creating slave: thread could not be created: %s", strerror(rs));
        res = -1;
      }
      else
      { // everything was ok
        res = 0;
      }
    }
  }

  // handle errors
  if (res == -1)
  { // some error in the slave creation, deallocate resources already allocated
    if (input_fifo > 0)
      hn_buffer_fifo_destroy(input_fifo);

    if (output_fifo > 0)
      hn_buffer_fifo_destroy(output_fifo);

    if (tinfo != NULL)
    {
      pthread_mutex_destroy(&tinfo->slave_args.client_info.health_mtx);
      pthread_cond_destroy(&tinfo->slave_args.client_info.health_cond);
      free(tinfo);
    }
  } 
  else 
  { // no errors. Let's add the client info data to the client list
    if (hn_list_add_to_end(client_list, tinfo) == -1)
    { // the slave could not be added to the client list => new error in creating the slave
      pthread_cancel(tinfo->tid);
      hn_buffer_fifo_destroy(input_fifo);
      hn_buffer_fifo_destroy(output_fifo);
      pthread_mutex_destroy(&tinfo->slave_args.client_info.health_mtx);
      pthread_cond_destroy(&tinfo->slave_args.client_info.health_cond);
      //avoid closing non existing sockets (virtual client threads)
      if(tinfo->slave_args.sock >= 0) {
        close(tinfo->slave_args.sock);
      }
      free(tinfo);
      res = -1;
      
      log_error("application_server_master_thread: creating slave: error adding to the client list");
    }
  }

  if (res != -1)
  {
    log_notice("application_server_master_thread: A new client slave has been created for app_id=%llu with input_fifo %u and output_fifo %u", 
        tinfo->slave_args.client_info.cid, 
        tinfo->slave_args.client_info.input_fifo, tinfo->slave_args.client_info.output_fifo);

    pthread_mutex_lock(&tinfo->slave_args.client_info.health_mtx);
    tinfo->slave_args.client_info.health_status = HN_CLIENT_HEALTH_STATUS_LIVE;
    pthread_cond_signal(&tinfo->slave_args.client_info.health_cond);
    pthread_mutex_unlock(&tinfo->slave_args.client_info.health_mtx);
  }
  return res;
}

// search for terminated slaves an deallocates their resources
int join_slaves()
{
  int client_iter_to_remove = -1;

  // iterate on the client list to search for already joined slaves
  int iter = hn_list_iter_front_begin(client_list);
  while (!hn_list_iter_end(client_list, iter))
  {
    thread_slave_info_t *tinfo = (thread_slave_info_t *)hn_list_get_data_at_position(client_list, iter);
    if (tinfo != NULL)
    {
      hn_client_info_t *cinfo = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
      if ((cinfo != NULL) && (cinfo->health_status == HN_CLIENT_HEALTH_STATUS_LIVE) && (pthread_tryjoin_np(tinfo->tid, (void **)&tinfo->retval) == 0))
      {
        /*
         * this thread has terminated
         * Let's remove it from the client list and deallocate its resources
         */
        log_debug("application_server_master_thread: joining slave: slave thread at client list position %d finished with status %d", iter, *((int *)tinfo->retval));
        log_notice("application_server_master_thread: client id %llu has disconnected", cinfo->cid);

        // Mark the client as zombie, since its application client has gone away
        pthread_mutex_lock(&cinfo->health_mtx);
        cinfo->health_status = HN_CLIENT_HEALTH_STATUS_ZOMBIE;
        log_debug("application_server_master_thread: client id %llu health %s", cinfo->cid, "ZOMBIE");
        pthread_mutex_unlock(&cinfo->health_mtx);
      }

      if ((cinfo != NULL) && (cinfo->health_status == HN_CLIENT_HEALTH_STATUS_RIP) && (client_iter_to_remove == -1)) 
      { // Now, we are confident about removing the thread info kept
        log_debug("application_server_master_thread: client id %llu health %s", cinfo->cid, "RIP");

        // save the position of the element to remove 
        client_iter_to_remove = iter;
      }
    }
    else
    { // an element in the client list is NULL and this should not happen ever
      log_error("application_server_master_thread: joining slave: element at position %d in the client list is NULL", iter);
    }

    iter = hn_list_iter_next(client_list, iter);  
  }

  remove_client(client_iter_to_remove);

  return 0;
}


void remove_client(int client)
{
  // let's free resources
  if (client >= 0) 
  {
    thread_slave_info_t *tinfo = (thread_slave_info_t *)hn_list_get_data_at_position(client_list, client);
    hn_client_info_t    *cinfo = (hn_client_info_t *)hn_list_get_data_at_position(client_list, client);

    if (tinfo != NULL)
    {
      log_verbose("application_server_master_thread: deallocating resources for client id %llu", cinfo->cid);

      hn_list_remove_element_at(client_list, (unsigned int)client);
      hn_buffer_fifo_destroy(cinfo->input_fifo);
      hn_buffer_fifo_destroy(cinfo->output_fifo);
      pthread_mutex_destroy(&cinfo->health_mtx);
      pthread_cond_destroy(&cinfo->health_cond);

      log_verbose("application_server_master_thread: resources for client id %llu deallocated", cinfo->cid);

      free(tinfo);
    }
  }
}
