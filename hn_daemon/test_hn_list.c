#include <stdio.h>

#include "hn_logging.h"
#include "hn_list.h"

int main(int argc, char **argv)
{
  int i;

  hn_init_log(HN_LOG_TO_STDOUT, NULL);

  log_info("prepared for test unit for hn_list");

  int id = hn_list_create_list();
  if (id < 0) 
  {
    log_error("Failed to create a list");
    return 0;
  }

  log_info("TEST1: Initially, list should be empty...");
  if (hn_list_is_empty(id))
  {
    log_info("TEST1: passed!");
  }
  else
  {
    log_error("TEST1: NOT passed!");
  }



  log_info("TEST2: adding element to the list in head position...");
  int ele[4] = { 0, 1, 2, 3};
  for (i = 0; i < 4; i++)
  {
    hn_list_add_to_begin(id, &(ele[i]));
  }
  int err = 0;
  for (i = 0; i < 4; i++)
  {
    int *e = (int *)hn_list_get_data_n_th(id, i);
    if (*e != ele[3-i]) err = 1;
  }
  if (err == 1)
    log_error("TEST2: NOT passed!");
  else
    log_info("TEST2: passed!");




  log_info("TEST3: removing from head position...");
  for (i = 0; i < 4; i++)
  {
    hn_list_remove_head(id);
    int *e = (int *)hn_list_get_data_n_th(id, 0);
    if ((i < 3) && (*e != ele[2-i]))
    {
      log_error("Element at head is %d, but should be %d", *e, ele[2-i]);
    }
  }
  if (hn_list_is_empty(id))
  {
    log_info("TEST3: passed!");
  }
  else
  {
    log_error("TEST3: NOT passed!");
  }




  log_info("TEST4: adding element to the list in tail position...");
  for (i = 0; i < 4; i++)
  {
    hn_list_add_to_end(id, &(ele[i]));
  }
  for (i = 0; i < 4; i++)
  {
    int *e = (int *)hn_list_get_data_n_th(id, i);
    if (*e != ele[i]) err = 1;
  }
  if (err == 1)
    log_error("TEST4 NOT passed!");
  else
    log_info("TEST4 passed!");



  log_info("TEST5: removing from tail position...");
  for (i = 0; i < 4; i++)
  {
    hn_list_remove_tail(id);
    int *e = (int *)hn_list_get_data_n_th(id, 2-i);
    if ((i < 3) && (*e != ele[2-i]))
    {
      log_error("Element at tail is %d, but should be %d", *e, ele[2-i]);
    }
  }
  if (hn_list_is_empty(id))
  {
    log_info("TEST5: passed!");
  }
  else
  {
    log_error("TEST5: NOT passed!");
  }


  log_info("TEST6: Getting elements at position 2, 1, 3, 0...");
  for (i = 0; i < 4; i++)
  {
    hn_list_add_to_end(id, &(ele[i]));
  }
  err = 0;
  int *e1 = (int *)hn_list_get_data_at_position(id, 2);
  int *e2 = (int *)hn_list_get_data_n_th(id, 2);
  if ((*e1 != *e2) || (*e1 != ele[2]))
  {
    log_error("Element at 2 is %d or %d, but should be %d", *e1, *e2, ele[2]);
    err = 1;
  }
  e1 = (int *)hn_list_get_data_at_position(id, 1);
  e2 = (int *)hn_list_get_data_n_th(id, 1);
  if ((*e1 != *e2) || (*e1 != ele[1]))
  {
    log_error("Element at 1 is %d or %d, but should be %d", *e1, *e2, ele[1]);
    err = 1;
  }
  e1 = (int *)hn_list_get_data_at_position(id, 3);
  e2 = (int *)hn_list_get_data_n_th(id, 3);
  if ((*e1 != *e2) || (*e1 != ele[3]))
  {
    log_error("Element at 3 is %d or %d, but should be %d", *e1, *e2, ele[3]);
    err = 1;
  }
  e1 = (int *)hn_list_get_data_at_position(id, 0);
  e2 = (int *)hn_list_get_data_n_th(id, 0);
  if ((*e1 != *e2) || (*e1 != ele[0]))
  {
    log_error("Element at 0 is %d or %d, but should be %d", *e1, *e2, ele[0]);
    err = 1;
  }
  if (err == 1)
    log_error("TEST6 NOT passed!");
  else
    log_info("TEST6 passed!");

  hn_list_remove_all(id);

  log_info("TEST7: Getting elements at HEAD...");
  err = 0;
  for (i = 0; i < 4; i++)
  {
    hn_list_add_to_begin(id, &(ele[i]));
    e1 = (int *)hn_list_get_data_at_head(id);
    if ((*e1 != ele[i]))
    {
      log_error("Element at HEAD is %d, but should be %d", *e1,  ele[2]);
      err = 1;
    }
  }
  if (err == 1)
    log_error("TEST7 NOT passed!");
  else
    log_info("TEST7 passed!");

  hn_list_remove_all(id);

  log_info("TEST8: Getting elements at TAIL...");
  err = 0;
  for (i = 0; i < 4; i++)
  {
    hn_list_add_to_end(id, &(ele[i]));
    e1 = (int *)hn_list_get_data_at_tail(id);
    if ((*e1 != ele[i]))
    {
      log_error("Element at TAIL is %d, but should be %d", *e1,  ele[2]);
      err = 1;
    }
  }
  if (err == 1)
    log_error("TEST8 NOT passed!");
  else
    log_info("TEST8 passed!");

  hn_list_destroy_list(id);



  log_info("TEST9: creating several lists...");
  int ids[4];
  err = 0;
  for (i = 0; i < 4; i++)
  {
    ids[i] = hn_list_create_list();
    if (hn_list_is_empty(ids[i]) < 1)
    {
      err = 1;
      log_error("list should exist and be empty");
    }
  }
  if (err == 1)
    log_error("TEST9 NOT passed!");
  else
    log_info("TEST9 passed!");

 
  log_info("TEST10: checking empty on a uninitalized list...");
  if (hn_list_is_empty(100) < 0)
    log_info("TEST10: passed!");
  else
    log_error("TEST10: not passed!");

  
  log_info("TEST11: deleting lists...");
  err = 0;
  for (i = 0; i < 4; i++)
  {
    hn_list_destroy_list(ids[i]);
    if (hn_list_is_empty(ids[i]) > -1)
    {
      err = 1;
      log_error("list should not exist");
    }
  }
  if (err == 1)
    log_error("TEST11 NOT passed!");
  else
    log_info("TEST11 passed!");

  log_info("TEST12: iterating on the begining of a non existing list...");
  if (hn_list_iter_begin(0) == -1)
    log_info("TEST12: passed!");
  else
    log_error("TEST12: NOT passed!");

  log_info("TEST13: iterating on the tail of a non existing list...");
  if (hn_list_iter_end(0) == -1)
    log_info("TEST13: passed!");
  else
    log_error("TEST13: NOT passed!");

  log_info("TEST14: iterating on the next of a non existing list...");
  if (hn_list_iter_next(0, 1) == -1)
    log_info("TEST14: passed!");
  else
    log_error("TEST14: NOT passed!");

  log_info("TEST15: iterating on the begin of an existing empty list...");
  id = hn_list_create_list();
  if (hn_list_iter_begin(id) == -2)
    log_info("TEST15: passed!");
  else
    log_error("TEST15: NOT passed!");
  
  log_info("TEST16: iterating on the end of an existing empty list...");
  if (hn_list_iter_end(id) == -2)
    log_info("TEST16: passed!");
  else
    log_error("TEST16: NOT passed!");
 
  log_info("TEST17: iterating on the next of an existing empty list...");
  if (hn_list_iter_next(id, 0) == -1)
    log_info("TEST17: passed!");
  else
    log_error("TEST17: NOT passed!");

  log_info("TEST18: iterating on the elements of a list...");
  err = 0;
  for (i = 0; i < 4; i++)
  {
    hn_list_add_to_end(id, &(ele[i]));
  }
  int m = 0;
  int iter;
  for (iter = hn_list_iter_begin(id); !hn_list_iter_exit(id, iter); iter = hn_list_iter_next(id, iter))
  {
    int *e = (int *)hn_list_get_data_at_position(id, iter);
    log_debug("Checking iter %d", m);
    if ((e != NULL) && (ele[m] != *e))
    {
      err = 1;
      log_error("element should be %d, but it is %d", ele[m], *e);
    }
    m++;
  }
  if (err == 1)
    log_error("TEST18 NOT passed!");
  else
    log_info("TEST18 passed!");
  

  return 0;
}
