#ifndef __HN_SOCKET_INET_H__
#define __HN_SOCKET_INET_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_socket_inet.h
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Hetoregeneous Node support for INET socket type communication
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

/*! 
 * \brief Socket info struct for a INET socket. 
 *
 * The socket is composed of an INET address and a socket descriptor fields
 */
typedef struct socket_inet_info_st
{
  struct sockaddr_in addr;  //!< INET socket address. \sa { ip(7) man page }
  int                sock;  //!< Socket descriptor
}hn_socket_inet_info_t;



//! \name Initialization and shutdown
///@{
//
/*!
 * \brief Initializes an INET socket
 *
 * \param [in] port Server port
 * \param [out] si_inet Pointer to a socket info data type initialized
 * \retval  1 In case of success
 * \retval  0 In case of failure
 *
 * \TODO to implement, since we are not providing support in the first versions 
 */
int hn_init_socket_inet(int port, hn_socket_inet_info_t* si_inet);

/*!
 * \brief Closes an open INET socket
 *
 * \param si_inet pointer to a socket INET info data type
 * \retval 1 always
 *
 * \TODO to implement, since we are not providing support in the first versions
 */
int hn_close_socket_inet(hn_socket_inet_info_t *si_inet);

///@}

#endif
