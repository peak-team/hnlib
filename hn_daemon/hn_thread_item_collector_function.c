///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Flich (jflich@disca.upv.es)
//
// Create Date: January 25, 2018
// File Name: hn_thread_item_collector_function.c
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Collector thread to process all incoming requests from all connected applications and inject
//    items into the corresponding FIFO
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "hn_daemon.h"
#include "hn_iface_mmi64.h"
#include "hn_item_commands.h"
#include "hn_logging.h"
#include "hn_list.h"
#include "hn_buffer_fifo.h"
#include "hn_request_handler.h"
#include "hn_exit_program.h"
#include "hn_shared_structures.h"
#include "hn_resource_manager.h"
#include "hn_shared_memory.h"
#include "hn_thread_backend_get_data_from_fpga_function.h"
#include "hn_thread_backend_send_data_to_fpga_function.h"
#include "hn_thread_stats_monitor_function.h"

// defines
#define COLLECTOR_SLEEP_MICROSECONDS 100
// set minimum polling interval to 200 ms
#define HN_STATS_MONITOR_POLLING_PERIOD_MIN 200

// global variables
const hn_rscmgt_info_t *rsc_info;


// function prototypes
uint32_t hn_compose_item(uint32_t tile, uint32_t command, char byte);
uint32_t hn_compose_peak_item(uint32_t tile, uint32_t command, char byte);
void hn_prepare_items_for_read_reg_operation(uint32_t tile, uint32_t reg, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_write_reg_operation(uint32_t tile, uint32_t reg, uint32_t data, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_read_memblock_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_read_memword_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_read_memhalf_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_read_membyte_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_write_memblock_operation(uint32_t tile, uint32_t addr, uint8_t *data, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_write_memword_operation(uint32_t tile, uint32_t addr, uint32_t *data, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_write_memhalf_operation(uint32_t tile, uint32_t addr, uint32_t *data, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_write_membyte_operation(uint32_t tile, uint32_t addr, uint32_t *data, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_trigger_interrupt_command(uint32_t tile, uint16_t vector, uint32_t *item_vector, uint32_t *num_items);
void hn_prepare_items_for_trigger_dma_command(uint32_t tile_src, uint32_t chan, uint32_t addr_src, unsigned long long size, uint32_t tile_dst, uint32_t addr_dst, uint32_t to_unit, uint32_t to_mem, uint32_t to_ext, uint32_t *item_vector, uint32_t *num_items);

uint32_t hn_shared_memory_create_buffer_operation(hn_command_shm_buffer_create_t *shm_cmd, char * name);
uint32_t hn_shared_memory_destroy_buffer_operation(hn_command_shm_buffer_destroy_t *shm_cmd);
void hn_prepare_data_for_shm_transfer_operation(hn_command_shm_buffer_transfer_t *shm_operation, hn_shm_buffer_info_t *shm_buffer_info, hn_shm_buffer_transfer_t *transfer_info, uint32_t client_id);
uint32_t hn_shared_memory_transfer_buffer_operation(hn_command_shm_buffer_transfer_t *shm_cmd, hn_shm_buffer_info_t **shm_buffer_info);

uint32_t hn_get_temperature_of_device_operation(hn_command_temperature_request_t *cmd, int32_t *temperature);

uint32_t hn_stats_monitor_find_tile_in_list(uint32_t tile, int *position_in_list);
uint32_t hn_stats_monitor_add_tile(uint32_t tile, unsigned long long client_id);     // add tile to list of statst monitor
uint32_t hn_stats_monitor_remove_tile(uint32_t tile, unsigned long long client_id);  // remove tile from list of stats monitor


// this function is called upon a thread cancelation
static void unlock_mutex(void *args) {
  pthread_cond_broadcast(&cond_rscmgt_init);
  pthread_mutex_unlock(&mutex_access_variables);
  pthread_mutex_unlock(&mutex_stats_monitor_config_access);
  pthread_cond_broadcast(&cond_stats_monitor_config_change);
  pthread_cond_broadcast(&cond_collector_init);
  log_verbose("item_collector thread: cleanup routine called, perhaps was canceled!");
}

// this function is called upon a thread cancelation
static void clean_access_variable_mutex(void *args) {
  pthread_mutex_unlock(&mutex_access_variables);
  pthread_mutex_unlock(&mutex_write_mmi64);
  pthread_mutex_unlock(&mutex_read_mmi64);
  pthread_mutex_unlock(&mutex_stats_monitor_table);
  pthread_mutex_unlock(&mutex_stats_monitor_config_access);
  pthread_cond_broadcast(&cond_stats_monitor_config_change);
  pthread_cond_broadcast(&cond_collector_init);
  log_verbose("item_collector thread: cleanup routine called for access_variable_mutex, perhaps was canceled!");
}

// This function ....
void send_resume_clk(int s)
{
  uint32_t item = hn_compose_item(0, HN_MANGO_ITEM_COMMAND_CLOCK, (char)HN_MANGO_ITEM_COMMAND_FUNCTION_RESUME_CLOCK);
  log_verbose("item_collector thread: SIGUSR for generating resume clock item");
  hn_buffer_fifo_write_elements_from_buf(0, &item, 1);
}

int filter_out_item_fifo;  // When set the items will not be written to the fifo item queue
int id_item_fifo;

/*
 * This function loops quering each input FIFO, processes the info and injects the data into the
 * corresponding FIFO associated to the injection part of the daemon
 */
void *hn_thread_item_collector_function(void *args) 
{
  uint32_t            item_vector[10000];     // vector used to generate items to be injected
  uint32_t            num_items;
  uint32_t            item;
  uint32_t            i;
  uint32_t            j;
  uint32_t            tile;
  uint16_t            vector;
  uint32_t            tile_mem;
  uint32_t            starting_addr; 
  uint32_t           *tile_mems = NULL;
  uint32_t           *starting_addrs = NULL;
  uint32_t            num_memories;
  unsigned long long  bw;
  uint32_t            found;
  uint32_t           *tiles_dst = NULL;
  uint32_t           *types_dst = NULL;
  uint32_t          **tiles_dsts = NULL;
  uint32_t          **types_dsts = NULL;
  uint32_t            num;
  uint32_t            num_tiles;
  uint32_t            num_tiles_x;
  uint32_t            num_tiles_y;
  uint32_t            id;
  static int          retval;
  hn_request_t        data;
  hn_data_rsc_t       rscmgt_reply;
  sigset_t            set;
  struct              sigaction act;

  retval               = 0;  // this thread return variable

  // To block all possible signals when running the handler
  sigfillset(&set);

  // fill the action for the signal to use to terminate the program
  act.sa_handler = send_resume_clk;
  act.sa_mask = set;
  act.sa_flags = 0;
  sigaction(SIGUSR1, &act, NULL);
  

  // first, get the item channel FIFO by iterating in the downstream channel list
  id_item_fifo = downstream_item_command_fifo; // FIFO id to inject items,

  // let's check whether we get a correct item fifo value
  if (id_item_fifo < 0) {
    log_warn("item_collector_thread: there is not item fifo. Items will be silently removed");
   // hn_exit_program_terminate(1);
   // retval = 2;
   // return &retval;
    filter_out_item_fifo = 1;
  } else {
    filter_out_item_fifo = 0;
  }

  int id_dnstr_shm_fifo  = downstream_bursttransfer_command_fifo;
  int id_upstr_shm_fifo  = upstream_bursttransfer_command_fifo;
  int filter_out_burst_fifo;
  if (id_dnstr_shm_fifo < 0) {
    log_warn("item_collector_thread: there is not burt traffic command fifo. Burst transfers will be silently removed");
    filter_out_burst_fifo = 1;
  } else {
    filter_out_burst_fifo = 0;
  }



  // Resource manager init procedure
  pthread_cleanup_push(unlock_mutex, NULL);
  if (!filter_out_item_fifo) {
    
    pthread_mutex_lock(&mutex_access_variables);

    // let's start the rsc mgr initializtion procedure
    if (hn_rscmgt_get_status() != HN_RESOURCE_MANAGER_STATUS_RUNNING) {
      log_info("Reseting architecture..."); 
      item = hn_compose_item(0, COMMAND_RESET_SYSTEM, 0);
      hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item, 1);
      hn_rscmgt_reset();

      pthread_cond_signal(&cond_rscmgt_init);
      pthread_mutex_unlock(&mutex_access_variables);
      
      sleep(6);

      pthread_mutex_lock(&mutex_access_variables);

      // now, let's get the arch id
      log_info("Getting architecture ID..."); 
      hn_prepare_items_for_read_reg_operation(0, 0, (uint32_t *)&item_vector, &num_items);
      // we inject the items into the item channel
      hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
      hn_rscmgt_get_arch_id();

      pthread_cond_signal(&cond_rscmgt_init);
      pthread_mutex_unlock(&mutex_access_variables);

     
      // block the thread until the resouce manager gets the running status
      pthread_mutex_lock(&mutex_access_variables);
      while ((!hn_exit_program_is_terminated()) && (hn_rscmgt_get_status() != HN_RESOURCE_MANAGER_STATUS_RUNNING) && (hn_rscmgt_get_status() != HN_RESOURCE_MANAGER_STATUS_NOT_INITIALIZED)) {
        pthread_cond_wait(&cond_rscmgt_init, &mutex_access_variables);
      }
    }

    pthread_cond_broadcast(&cond_rscmgt_init);
    pthread_mutex_unlock(&mutex_access_variables);
  } 
  pthread_cleanup_pop(0);
  // Resource manager init procedure end
  

  if (hn_rscmgt_get_status() != HN_RESOURCE_MANAGER_STATUS_RUNNING) {
    hn_exit_program_terminate(0);
  } else {
    log_info("item_collector_thread: ready...");
  }


  // notify the hn_stats_monitor that the collector is already running
  log_verbose("item_collector_thread, notify stats_monitor_thread that collector is ready");
  pthread_cond_broadcast(&cond_collector_init);
  
  // get arch info
  rsc_info = hn_rscmgt_get_info();

  // We loop endless
  uint32_t sleep = 0;   // when set the collector sleeps for a while
  pthread_cleanup_push(clean_access_variable_mutex, NULL);
  while (!hn_exit_program_is_terminated()) 
  {
    // we may go to sleep for a while (if the previous scan of all client's fifo did not
    // get any data)
    if (sleep) usleep(COLLECTOR_SLEEP_MICROSECONDS);
    sleep = 1;  // for next iteration (if we find data then we cancel sleep; see below)

    // We query all the input FIFOs in search of data. We perform a round-robin arbitration
    // in the query process
    int iter = hn_list_iter_front_begin(client_list);
    while (!hn_list_iter_end(client_list, iter)) {
      // We query each input FIFO
      hn_client_info_t *client = (hn_client_info_t *)hn_list_get_data_at_position(client_list, iter);
      if (client != NULL) {
        unsigned long long client_id = client->cid;
        unsigned int client_fifo = client->output_fifo;
        int num_elements = hn_buffer_fifo_copy_elements_to_buf(client_fifo, &data, 1);
        if (num_elements == 1) {
          //
          sleep = 0; // next iteration do not sleep
          //
          // we process the read element
          switch (data.command)
          {
            case HN_COMMAND_RESET:   // The operation is to inject a reset item to the system
              log_debug("item_collector_thread: reset request received");
              item = hn_compose_item(data.tile, COMMAND_RESET_SYSTEM, 0);
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item, 1);
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_READ_REGISTER:      
              // The operation is to read a register. We check the lock in exclusive mode for register acccess
              // and then inject the corresponding items into the FIFO 
              // associated to the item channel
              log_debug("item_collector_thread: read register request received");
              pthread_mutex_lock( &mutex_access_variables );
              pthread_testcancel();
              if (!lock_tile_register_access[data.tile]) {
                // we lock the access to the tile for read register operations
                lock_tile_register_access[data.tile] = 1;
                // we annotate the receiving application id for the read values
                associated_read_register_client[data.tile] = client_id;
                // we generate the items for the read operation
                hn_prepare_items_for_read_reg_operation(data.tile, data.fpga_address, (uint32_t *)&item_vector, &num_items);
                // we inject the items into the item channel
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              }
              pthread_mutex_unlock( &mutex_access_variables );
              break;

            case HN_COMMAND_WRITE_REGISTER:
              // The operation is to write a register. We prepare the items and
              // store them into the FIFO associated to the item channel
              log_debug("item_collector_thread: write register request received");
              hn_prepare_items_for_write_reg_operation(data.tile, data.fpga_address, *((uint32_t *)data.data), (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we dealocate the data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_READ_MEMORY_BLOCK:   
              // The operation is to read a memory block. We check the lock in exclusive mode for memory access
              // and then inject the corresponding items into the FIFO
              // associated to the item channel
              log_debug("item_collector_thread: read memory request received");
              pthread_mutex_lock( &mutex_access_variables );
              pthread_testcancel();
              if (!lock_memory[data.tile]) {
                // we lock the access to the tile for the memory read operation
                lock_memory[data.tile] = 1;
                // we annotate the receiving application id for the read values
                associated_read_memory_client[data.tile] = client_id;
                pthread_mutex_unlock( &mutex_access_variables );
                
                // we generate the items for the read operation
                hn_prepare_items_for_read_memblock_operation(data.tile, data.fpga_address, (uint32_t *)&item_vector, &num_items);
                // we inject the items into the item channel
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              } else {
                pthread_mutex_unlock( &mutex_access_variables );
              }
              break;

            case HN_COMMAND_READ_MEMORY_WORD:   
              // The operation is to read a memory word. We check the lock in exclusive mode for memory access
              // and then inject the corresponding items into the FIFO
              // associated to the item channel
              log_debug("item_collector_thread: read memory word request received");
              pthread_mutex_lock( &mutex_access_variables );
              pthread_testcancel();
              if (!lock_memory[data.tile]) {
                // we lock the access to the tile for the memory read operation
                lock_memory[data.tile] = 1;
                // we annotate the receiving application id for the read values
                associated_read_memory_client[data.tile] = client_id;
                
                pthread_mutex_unlock( &mutex_access_variables );

                // we generate the items for the read operation
                hn_prepare_items_for_read_memword_operation(data.tile, data.fpga_address, (uint32_t *)&item_vector, &num_items);
                // we inject the items into the item channel
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              } else {
                pthread_mutex_unlock( &mutex_access_variables );
              }
              break;

            case HN_COMMAND_READ_MEMORY_HALF:   
              // The operation is to read a memory half word. We check the lock in exclusive mode for memory access
              // and then inject the corresponding items into the FIFO
              // associated to the item channel
              log_debug("item_collector_thread: read memory half request received");
              pthread_mutex_lock( &mutex_access_variables );
              pthread_testcancel();
              if (!lock_memory[data.tile]) {
                // we lock the access to the tile for the memory read operation
                lock_memory[data.tile] = 1;
                // we annotate the receiving application id for the read values
                associated_read_memory_client[data.tile] = client_id;
                pthread_mutex_unlock( &mutex_access_variables );

                // we generate the items for the read operation
                hn_prepare_items_for_read_memhalf_operation(data.tile, data.fpga_address, (uint32_t *)&item_vector, &num_items);
                // we inject the items into the item channel
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              } else {
                pthread_mutex_unlock( &mutex_access_variables );
              }
              break;

            case HN_COMMAND_READ_MEMORY_BYTE:   
              // The operation is to read a memory byte. We check the lock in exclusive mode for memory access
              // and then inject the corresponding items into the FIFO
              // associated to the item channel
              log_debug("item_collector_thread: read memory byte request received");
              pthread_mutex_lock( &mutex_access_variables );
              pthread_testcancel();
              if (!lock_memory[data.tile]) {
                // we lock the access to the tile for the memory read operation
                lock_memory[data.tile] = 1;
                // we annotate the receiving application id for the read values
                associated_read_memory_client[data.tile] = client_id;
                pthread_mutex_unlock( &mutex_access_variables );
                // we generate the items for the read operation
                hn_prepare_items_for_read_membyte_operation(data.tile, data.fpga_address, (uint32_t *)&item_vector, &num_items);
                // we inject the items into the item channel
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              } else {
                pthread_mutex_unlock( &mutex_access_variables );
              }
              break;

            case HN_COMMAND_WRITE_MEMORY_BLOCK:
              // The operation is to write a memory block. We generate and inject the 
              // corresponding items into the FIFO associated to the item channel
              log_debug("item_collector_thread: write memory block request received");
              hn_prepare_items_for_write_memblock_operation(data.tile, data.fpga_address, (uint8_t *)data.data, (uint32_t *)&item_vector, &num_items);
              // we inject the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we deallocate the data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_WRITE_MEMORY_WORD:
              // The operation is to write a memory word. We generate and inject the 
              // corresponding items into the FIFO associated to the item channel
              log_debug("item_collector_thread: write memory word request received");
              hn_prepare_items_for_write_memword_operation(data.tile, data.fpga_address, (uint32_t *)data.data, (uint32_t *)&item_vector, &num_items);
              // we inject the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we deallocate the data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_WRITE_MEMORY_HALF:
              // The operation is to write a memory half word. We generate and inject the 
              // corresponding items into the FIFO associated to the item channel
              log_debug("item_collector_thread: write memory half request received");
              hn_prepare_items_for_write_memhalf_operation(data.tile, data.fpga_address, (uint32_t *)data.data, (uint32_t *)&item_vector, &num_items);
              // we inject the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we deallocate the data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_WRITE_MEMORY_BYTE:
              // The operation is to write a memory byte. We generate and inject the 
              // corresponding items into the FIFO associated to the item channel
              log_debug("item_collector_thread: write memory byte request received");
              hn_prepare_items_for_write_membyte_operation(data.tile, data.fpga_address, (uint32_t *)data.data, (uint32_t *)&item_vector, &num_items);
              // we inject the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we deallocate the data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_READ_SHARED_MEMORY_BUFFER:   
            case HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER:   
              {
                // The operation is to trigger a shared memory access for read/write.
                uint32_t                           rv;
                hn_command_shm_buffer_transfer_t  *cmd;
                hn_shm_buffer_info_t              *shm_buffer_info = NULL;
                hn_shm_buffer_transfer_t           shm_transfer_info;

                // The operation is to create a shared memory link with the application client
                log_debug("item_collector_thread: shared memory buffer transfer %s request received", 
                    (data.command == HN_COMMAND_READ_SHARED_MEMORY_BUFFER)? "READ " : "WRITE"
                    );
                hn_command_shm_buffer_manage_status_t  shm_operation_status;

                shm_operation_status.info.command = data.command;
                shm_operation_status.status       = HN_SHM_OPERATION_ERROR;
                shm_operation_status.client_id    = client_id;

                // call function to search buffer, and program transfer
                cmd = (hn_command_shm_buffer_transfer_t *)(data.data);
                rv =  hn_shared_memory_transfer_buffer_operation(cmd, &shm_buffer_info);
                if(rv == 0)
                {
                  log_debug("  requested shm located, updating response");
                  shm_operation_status.status = HN_SHM_OPERATION_OK;
                  //shm has been assigned in previous function, in case of error rv would be different of 0
                  strcpy(shm_operation_status.name, shm_buffer_info->name);
                } else {
                  log_error("Requested shm can't be located");
                }

                // only avoid sendback status message if operation was succesfully programmed and is blocking, return in all other cases
                if ((cmd->blocking == 0) || (rv != 0))
                {
                  log_debug("  sending response");
                  hn_buffer_fifo_write_bytes_from_buf(shm_notification_fifo, &shm_operation_status, sizeof(hn_command_shm_buffer_manage_status_t));
                }

                // compose message to send to fifo for backend thread
                hn_prepare_data_for_shm_transfer_operation(cmd ,shm_buffer_info, &shm_transfer_info, client_id);

                shm_operation_status.status       = HN_SHM_OPERATION_ERROR;

                if (data.command == HN_COMMAND_READ_SHARED_MEMORY_BUFFER) {
                  if (filter_out_burst_fifo != 0) {
                    log_debug("@item_collector: no downstream burst transfer fifo, operation shm discarded, notifying client app");
                    shm_operation_status.status = HN_SHM_OPERATION_ERROR;
                    hn_buffer_fifo_write_bytes_from_buf(shm_notification_fifo, &shm_operation_status, sizeof(hn_command_shm_buffer_manage_status_t));
                  }
                  else
                  {
                    hn_buffer_fifo_write_elements_from_buf(id_upstr_shm_fifo, &shm_transfer_info, 1);
                  }
                }
                else if (data.command == HN_COMMAND_WRITE_SHARED_MEMORY_BUFFER)
                {
                  if (filter_out_burst_fifo != 0) {
                    log_debug("@item_collector: no downstream burst transfer fifo, operation shm discarded, notifying client app");
                    shm_operation_status.status = HN_SHM_OPERATION_ERROR;
                    hn_buffer_fifo_write_bytes_from_buf(shm_notification_fifo, &shm_operation_status, sizeof(hn_command_shm_buffer_manage_status_t));
                  }
                  else
                  {
                    // update shm_fifo for current channel
                    log_debug("send data to shm downstream command fifo %u....", id_dnstr_shm_fifo);

                    hn_buffer_fifo_write_elements_from_buf(id_dnstr_shm_fifo, &shm_transfer_info, 1);
                  }
                }
              }

              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_SET_FILTER:
              // The operation is to set the client's filter
              //
              log_debug("item_collector_thread: set filter request received");
              client->filter.target = ((hn_filter_t *)(data.data))->target;
              client->filter.mode   = ((hn_filter_t *)(data.data))->mode;
              client->filter.tile   = ((hn_filter_t *)(data.data))->tile;
              client->filter.core   = ((hn_filter_t *)(data.data))->core;
              // we deallocate the data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_CREATE_SHARED_MEMORY_BUFFER:
              {
                uint32_t   rv;
                char       name[SHM_POSIX_NAME_MAX_LEN];
                // The operation is to create a shared memory link with the application client
                log_debug("item_collector_thread: create shared memory buffer request received");
                hn_command_shm_buffer_manage_status_t  shm_operation_status;

                shm_operation_status.info.command = HN_COMMAND_CREATE_SHARED_MEMORY_BUFFER;
                shm_operation_status.status       = HN_SHM_OPERATION_ERROR;
                shm_operation_status.client_id    = client_id;

                rv =  hn_shared_memory_create_buffer_operation((hn_command_shm_buffer_create_t *)(data.data), name);
                if(rv == 0)
                {
                  log_debug("  buffer succesfully created, updating response");
                  shm_operation_status.status = HN_SHM_OPERATION_OK;
                  strcpy(shm_operation_status.name, name);
                }
                hn_buffer_fifo_write_bytes_from_buf(shm_notification_fifo, &shm_operation_status, sizeof(hn_command_shm_buffer_manage_status_t));
              }
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              
              break;

            case HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER:
              {
                // The operation is to destroy a shared memory link with the application client
                log_debug("item_collector_thread: destroy shared memory buffer request received");
                uint32_t   rv;
                hn_command_shm_buffer_manage_status_t  shm_operation_status;

                shm_operation_status.info.command = HN_COMMAND_DESTROY_SHARED_MEMORY_BUFFER;
                shm_operation_status.status       = HN_SHM_OPERATION_ERROR;
                shm_operation_status.client_id    = client_id;

                rv =  hn_shared_memory_destroy_buffer_operation((hn_command_shm_buffer_destroy_t *)(data.data));
                if(rv == 0)
                {
                  shm_operation_status.status = HN_SHM_OPERATION_OK;
                  strcpy(shm_operation_status.name, ((hn_command_shm_buffer_destroy_t *)(data.data))->name);
                }
                hn_buffer_fifo_write_bytes_from_buf(shm_notification_fifo, &shm_operation_status, sizeof(hn_command_shm_buffer_manage_status_t));
              }
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_ENABLE_CORE_DEBUG:
              // The operation is to send an enable item for core debug into the PEAK unit
              log_debug("item_collector_thread: enable core debug request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item((uint32_t)data.subtile, COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS, PEAK_ENABLE_CORE_DEBUG_VALUE);
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_DISABLE_CORE_DEBUG:
              // The operation is to send a disable item for core debug
              log_debug("item_collector_thread: disable core debug request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item((uint32_t)data.subtile, COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS, PEAK_DISABLE_CORE_DEBUG_VALUE);
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_ENABLE_CACHE_CONTROLLER_DEBUG:
              // The operation is to send an enable item for cache controller debug
              log_debug("item_collector_thread: enable cache controller debug request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item((uint32_t)data.subtile, COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS, PEAK_ENABLE_CACHE_CONTROLLER_DEBUG_VALUE);
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_DISBALE_CACHE_CONTROLLER_DEBUG:
              // The operation is to send a disable item for cache controller debug
              log_debug("item_collector_thread: disable cache controller debug request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item((uint32_t)data.subtile, COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS, PEAK_DISABLE_CACHE_CONTROLLER_DEBUG_VALUE);
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_ENABLE_MEMORY_DEBUG:
              // The operation is to send an enable item for memory debug
              log_debug("item_collector_thread: enable memory debug request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item((uint32_t)data.subtile, COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS, PEAK_ENABLE_MEMORY_DEBUG_VALUE);
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_DISABLE_MEMORY_DEBUG:
              // The operation is to send a disable item for memory debug
              log_debug("item_collector_thread: disable memory debug request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item((uint32_t)data.subtile, COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS, PEAK_DISABLE_MEMORY_DEBUG_VALUE);
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_WRITE_PROTOCOL:
              // The operation is to send the peak protocol to the FPGA
              log_debug("item_collector_thread: peak write protocol request received");
              // We prepare all the items and then send them to the FPGA, via the write to register operation
              for (i=0;i<data.size;i++) {
                item = hn_compose_peak_item(0, COMMAND_PEAK_WRITE_PROTOCOL, *((uint8_t *)data.data + i));
                hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              }
              // we free the data memory space
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_KEY_INTERRUPT:
              // The operation is to send a key item to peak
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item((uint32_t)data.subtile, COMMAND_PEAK_KEY_INTERRUPT, *(uint8_t *)data.data);
              log_verbose("item_collector_thread: peak_key_interrupt_item=%08X", item);
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              //usleep(1000000);
              usleep(     1000);
              // we free the data memory space
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_SET_PROCESSOR_ADDRESS:
              // The operation is to send the PC address to a PEAK core, we prepare items and send them via write to register operation
              log_debug("item_collector_thread: set processor address request received");
              // first item
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_SET_PROCESSOR_ADDRESS, *((uint8_t *)data.data));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // second item
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_SET_PROCESSOR_ADDRESS, *((uint8_t *)data.data + 1));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // third item
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_SET_PROCESSOR_ADDRESS, *((uint8_t *)data.data + 2));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // fourth item
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_SET_PROCESSOR_ADDRESS, *((uint8_t *)data.data + 3));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we free the data memory space
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_CONFIGURE_TILE:
              // The operation is to configure a peak tile
              log_debug("item_collector_thread: peak configure tile request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_CONFIGURE_TILE, *((uint8_t *)data.data));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we free the data memory space
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_RESET:
              log_error("COMMAND_PEAK_RESET seems not being implemented\n");
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS:
              // The operation is to configure a peak tile for debug and stats
              log_debug("item_collector_thread: peak configure debug and stats request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS, *((uint8_t *)data.data));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we free the data memory space
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_READ_REGISTER:
              // The operation is to configure a peak tile for debug and stats
              log_debug("item_collector_thread: peak read register request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_READ_REGISTER, *((uint8_t *)data.data));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);

              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_READ_REGISTER, *((uint8_t *)data.data+1));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              // we free the data memory space
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_PEAK_WRITE_REGISTER:
              // The operation is to configure a peak tile for debug and stats
              log_debug("item_collector_thread: peak write register request received");
              // We first prepare the item and then send it as a write to register operation
              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_WRITE_REGISTER, *((uint8_t *)data.data));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);

              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_WRITE_REGISTER, *((uint8_t *)data.data+1));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);

              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_WRITE_REGISTER, *((uint8_t *)data.data+2));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);

              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_WRITE_REGISTER, *((uint8_t *)data.data+3));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);

              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_WRITE_REGISTER, *((uint8_t *)data.data+4));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);

              item = hn_compose_peak_item(data.subtile, COMMAND_PEAK_WRITE_REGISTER, *((uint8_t *)data.data+5));
              hn_prepare_items_for_write_reg_operation(data.tile, HN_MANGO_TILEREG_TO_UNIT, item, (uint32_t *)&item_vector, &num_items);
              // we store the items into the fifo
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);

              // we free the data memory space
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;
            case HN_COMMAND_REGISTER_INTS:
              // The operation is to register an interrupt entry for a specific tile unit
              log_debug("item_collector_thread: register_ints command received: tile %d, vector %04x, client id %d", data.tile, *(uint16_t *)data.data, client_id);
              if ( hn_rscmgt_register_int(data.tile, *(uint16_t *)data.data, client_id, &id) ) {
                // we return to the client the id
                rscmgt_reply.result = HN_RSC_FOUND;
                rscmgt_reply.id     = id;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                log_error("No more registered interrupts entries available, maximum reached");
                // We send reply
                rscmgt_reply.result  = HN_RSC_OPERATION_FAILED;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we free the data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_TRIGGER_INTS:
              // The operation is to trigger an interrupt for an associated id
              if ( hn_rscmgt_get_interrupt_info(data.rsc_req.id, &tile, &vector) ) {
                log_debug("item_collector_thread: trigger_ints command received: id %d (tile %d, vector mask %04x), vector %04x", data.rsc_req.id, tile, vector, *(uint16_t *)data.data);
                hn_prepare_items_for_trigger_interrupt_command(tile, (vector & *(uint16_t *)data.data), (uint32_t *)item_vector, &num_items);
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
              } else {
                log_debug("item_collector_thread: trigger_ints command received: id %d (no associated info found)", data.rsc_req.id);
              }
              // we free data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_WAIT_INTS:
              // The operation is to signal a wait on an interrupt for an associated id
              if ( hn_rscmgt_get_interrupt_info(data.rsc_req.id, &tile, &vector) ) {
                log_debug("item_collector_thread: wait_ints command received: id %d (tile %d, vector mask %04x), vector %04x", data.rsc_req.id, tile, vector, *(uint16_t *)data.data);
                // if the interrupt was already received, then we signal the client
                if ( hn_rscmgt_received_interrupt(data.rsc_req.id, *(uint16_t *)data.data) ) {
                  // we cancel the interrupt
                  hn_rscmgt_cancel_interrupt(data.rsc_req.id, *(uint16_t *)data.data);
                  // we signal the client
                  rscmgt_reply.result = HN_RSC_INT_RECEIVED;
                  rscmgt_reply.client_id = client_id;
                  hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                } else {
                  // we annotate the wait
                  hn_rscmgt_annotate_wait_on_interrupt(data.rsc_req.id, *(uint16_t *)data.data);
                  // we free the rscmgt access since this request does not have a reply to the client
                }
              } else {
                log_debug("item_collector_thread: wait_ints command received: id %d (no associated info found)", data.rsc_req.id);
                // signal the error to the client
                rscmgt_reply.result = HN_RSC_INT_ERROR;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we free data
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_INTS:
              // The operation is to release an interrupt entry
              log_debug("item_collector_thread: release_ints command received: id %d", data.rsc_req.id);
              if ( hn_rscmgt_release_int(data.rsc_req.id) ) {
                // we return OK
                rscmgt_reply.result = HN_RSC_FOUND;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                log_error("Error, releasing interrupt entry");
                // We send ERROR
                rscmgt_reply.result  = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_REGISTER_DMA:
              // The operation is to register an dma entry
              log_debug("item_collector_thread: register_dma command received");
              if (hn_rscmgt_register_dma (&id, client_id) ) {
                // We send id back to the client
                rscmgt_reply.result = HN_RSC_FOUND;
                rscmgt_reply.id     = id;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                log_error("No more registered DMA entries available, maximum reached");
                // We send reply
                rscmgt_reply.result  = HN_RSC_OPERATION_FAILED;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_TRIGGER_DMA:
              // The operation is to trigger a dma operation associated to a specific dma id
              log_debug("item_collector_thread: trigger_dma command received. id %d, tile_src %d, addr_src %08x, size %llu, tile_dst %d, addr_dst %08x, to_unit %d, to_mem %d, to_ext %d, notify %d", data.dma_req.id, data.dma_req.tile_src, data.dma_req.addr_src, 
                                                        data.dma_req.size, data.dma_req.tile_dst,
                                                        data.dma_req.addr_dst, data.dma_req.to_unit, data.dma_req.to_mem, 
                                                        data.dma_req.to_ext, data.dma_req.notify);
              // we store the DMA operation info
              if (!hn_rscmgt_write_dma_operation ( data.dma_req.id, /*channel*/ 0, data.dma_req.tile_src, data.dma_req.addr_src, 
                                                   data.dma_req.size, data.dma_req.tile_dst,
                                                   data.dma_req.addr_dst, data.dma_req.to_unit, 
                                                   data.dma_req.to_mem, data.dma_req.to_ext, data.dma_req.notify )) {
                // Error, could not store data
                rscmgt_reply.result = HN_RSC_DMA_ERROR;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // Now we program the DMA
                hn_prepare_items_for_trigger_dma_command(data.dma_req.tile_src, /*channel*/ 0, data.dma_req.addr_src, data.dma_req.size,
                                                         data.dma_req.tile_dst, data.dma_req.addr_dst, data.dma_req.to_unit, 
                                                         data.dma_req.to_mem, data.dma_req.to_ext, (uint32_t *)item_vector, &num_items);
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
                // Now we send reply if notify is not set (if set the client will be notified upon completion)
                if (data.dma_req.notify == 0) {
                  rscmgt_reply.result = HN_RSC_OK;
                  rscmgt_reply.client_id = client_id;
                  hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                }
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_STATUS_DMA:
              // The operation is to get dma statusi associated to a specific dma id
              log_debug("item_collector_thread: get_status_dma command received. Not implemented yet!");
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_DMA:
              // The operation is to release an DMA entry
              log_debug("item_collector_thread: release_dma command received: id %d", data.dma_req.id);
              if ( hn_rscmgt_release_dma(data.dma_req.id) ) {
                // we return OK
                rscmgt_reply.result = HN_RSC_FOUND;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                log_error("Error, releasing DMA entry");
                // We send ERROR
                rscmgt_reply.result  = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_LOCK_RESOURCES_ACCESS_RSC:
              // The operation is to get the lock to operate with the resource manager
              log_debug("item_collector_thread: lock_resouces_access command received");
              if (!hn_rscmgt_get_lock()) {
                hn_rscmgt_set_lock();
                // we send the reply to the client
                rscmgt_reply.result = HN_RSC_OK;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              } 
              break;

            case HN_COMMAND_UNLOCK_RESOURCES_ACCESS_RSC:
              // The operation is to release the lock to operate with the resource manager
              log_debug("item_collector_thread: unlock_resouces_access command received");
              hn_rscmgt_reset_lock();
              // we send the reply to the client
              rscmgt_reply.result = HN_RSC_OK;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_FIND_MEMORY_RSC:
              // The operation is to find a memory segment, this function triggers resource manager functions
              // We call the find_memory function from the resource manager
              if ( hn_rscmgt_find_memory(data.tile, data.rsc_req.size, &tile_mem, &starting_addr) ) {
                // memory found, we send back the reply to the application
                // with the memory and starting address
                rscmgt_reply.result = HN_RSC_FOUND;
                rscmgt_reply.num_memories = 1;
                rscmgt_reply.tile_mem = tile_mem;
                rscmgt_reply.starting_addr = starting_addr;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // no memory found, we send notification no memory found
                rscmgt_reply.result = HN_RSC_NOTFOUND;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_FIND_MEMORIES_RSC:
              // The operation is to find memories, this function triggers resource manager functions
              // We call the find_memories function from the resource manager
              hn_rscmgt_find_memories(data.rsc_req.size, &tile_mems, &starting_addrs, &num_memories);
              if (num_memories > 0) {
                // We send the number of memories found and for each we send
                // the tile and the starting address
                rscmgt_reply.result = HN_RSC_FOUND;
                rscmgt_reply.num_memories = num_memories;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                for (i=0;i<num_memories;i++) {
                  rscmgt_reply.result = HN_RSC_DATA;
                  rscmgt_reply.tile_mem = tile_mems[i];
                  rscmgt_reply.starting_addr = starting_addrs[i];
                  hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                }
              } else {
                // no memory found, we send notification no memory found
                rscmgt_reply.result = HN_RSC_NOTFOUND;
                rscmgt_reply.num_memories = 0;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the allocated data by the find_memories function
              if (num_memories > 0) {
                free(tile_mems);
                free(starting_addrs);
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_ALLOCATE_MEMORY_RSC:
              // The operation is to allocate a memory segment, this triggers resource manager functions
              // We call the allocate_memory function from the resource manager
              if ( hn_rscmgt_allocate_memory(data.tile, data.fpga_address, data.rsc_req.size) ) {
                // We send ok to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send error to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_MEMORY_RSC:
              // The operation is to release a memory segment, this function triggers resource manager functions
              // We call the release_memory function from the resource manager
              //log_warn("JM10, @item_collector, hn_rscmgt_release_memory for tile %u,  address 0x08%x\n\n",data.tile, data.fpga_address);
              
              if ( hn_rscmgt_release_memory(data.tile, data.fpga_address, data.rsc_req.size) ) {
                // We send ok to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send error to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_SET_BACKEND_WEIGHTS:
              hn_thread_backend_get_data_from_fpga_set_weights(data.data, data.size);
              hn_thread_backend_send_data_to_fpga_set_weights(data.data, data.size);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_NETWORK_BANDWIDTH_RSC:
              // The operation is to get the available network bandwidth, this function triggers resource manager functions
              // We call the get_available_network_bandwidth function from the resource manager
              bw = hn_rscmgt_get_available_network_bandwidth(data.rsc_req.tile_src, data.rsc_req.tile_dst);
              // We send the bandwidth back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.bw     = bw;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;
 
            case HN_COMMAND_GET_READ_MEMORY_BANDWIDTH_RSC:
              // The operation is to get the available read memory bandwidth, this function triggers resource manager functions
              // We call the get_available_read_memory_bandwidth function from the resource manager
              bw = hn_rscmgt_get_available_read_memory_bandwidth(data.tile);
              // We send the bandwidth back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.bw     = bw;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_WRITE_MEMORY_BANDWIDTH_RSC:
              // The operation is to get the available write memory bandwidth, this function triggers resource manager functions
              // We call the get_available_write_memory_bandwidth function from the resource manager
              bw = hn_rscmgt_get_available_write_memory_bandwidth(data.tile);
              // We send the bandwidth back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.bw     = bw;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_READ_CLUSTER_BANDWIDTH_RSC:
              // The operation is to get the available read cluster bandwidth, this function triggers resource manager functions
              // We call the get_available_read_cluster_bandwidth function from the resource manager
              bw = hn_rscmgt_get_available_read_cluster_bandwidth();
              // We send the bandwidth back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.bw     = bw;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_WRITE_CLUSTER_BANDWIDTH_RSC:
              // The operation is to get the available write cluster bandwidth, this function triggers resource manager functions
              // We call the get_available_write_cluster_bandwidth function from the resource manager
              bw = hn_rscmgt_get_available_write_cluster_bandwidth();
              // We send the bandwidth back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.bw     = bw;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RESERVE_NETWORK_BANDWIDTH_RSC:
              // The operation is to reserve network bandwidth, this function triggers resource manager functions
              // We call the reserve_network_bandwidth function from the resource manager
              if ( hn_rscmgt_reserve_network_bandwidth(data.rsc_req.tile_src, data.rsc_req.tile_dst, data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RESERVE_READ_MEMORY_BANDWIDTH_RSC:
              // The operation is to reserve read memory bandwidth, this function triggers resource manager functions
              // We call the reserve_read_memory_bandwidth function from the resource manager
              if ( hn_rscmgt_reserve_read_memory_bandwidth(data.tile, data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RESERVE_WRITE_MEMORY_BANDWIDTH_RSC:
              // The operation is to reserve write memory bandwidth, this function triggers resource manager functions
              // We call the reserve_write_memory_bandwidth function from the resource manager
              if ( hn_rscmgt_reserve_write_memory_bandwidth(data.tile, data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RESERVE_READ_CLUSTER_BANDWIDTH_RSC:
              // The operation is to reserve read cluster bandwidth, this function triggers resource manager functions
              // We call the reserve_read_cluster_bandwidth function from the resource manager
              if ( hn_rscmgt_reserve_read_cluster_bandwidth(data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RESERVE_WRITE_CLUSTER_BANDWIDTH_RSC:
              // The operation is to reserve write cluster bandwidth, this function triggers resource manager functions
              // We call the reserve_write_cluster_bandwidth function from the resource manager
              if ( hn_rscmgt_reserve_write_cluster_bandwidth(data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_NETWORK_BANDWIDTH_RSC:
              // The operation is to release network bandwidth, this function triggers resource manager functions
              // We call the release_network_bandwidth function from the resource manager
              if ( hn_rscmgt_release_network_bandwidth(data.rsc_req.tile_src, data.rsc_req.tile_dst, data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_READ_MEMORY_BANDWIDTH_RSC:
              // The operation is to release read memory bandwidth, this function triggers resource manager functions
              // We call the release_read_memory_bandwidth function from the resource manager
              if ( hn_rscmgt_release_read_memory_bandwidth(data.tile, data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_WRITE_MEMORY_BANDWIDTH_RSC:
              // The operation is to release write memory bandwidth, this function triggers resource manager functions
              // We call the release_write_memory_bandwidth function from the resource manager
              if ( hn_rscmgt_release_write_memory_bandwidth(data.tile, data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_READ_CLUSTER_BANDWIDTH_RSC:
              // The operation is to release read cluster bandwidth, this function triggers resource manager functions
              // We call the release_read_cluster_bandwidth function from the resource manager
              if ( hn_rscmgt_release_read_cluster_bandwidth(data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_WRITE_CLUSTER_BANDWIDTH_RSC:
              // The operation is to release write cluster bandwidth, this function triggers resource manager functions
              // We call the release_write_cluster_bandwidth function from the resource manager
              if ( hn_rscmgt_release_write_cluster_bandwidth(data.rsc_req.bw) ) {
                // We send OK to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_FIND_UNITS_SET_RSC:
              log_debug("item_collector_thread: finding units set for client_id %u. num_tiles=%u", client_id, data.rsc_req.num_tiles);
              // The operation is to find a set of units, this function triggers resource manager functions
              // We call the find_unit_set function from the resource manager
              tiles_dst = malloc(sizeof(uint32_t)*data.rsc_req.num_tiles);
              types_dst = malloc(sizeof(uint32_t)*data.rsc_req.num_tiles);
              found = hn_rscmgt_find_units_set(data.tile, data.rsc_req.num_tiles, data.data, tiles_dst, types_dst, 1, 1);
              if (found) {
                // We send the set back to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                for (i=0;i<data.rsc_req.num_tiles;i++) {
                  rscmgt_reply.result = HN_RSC_DATA;
                  rscmgt_reply.tile = tiles_dst[i];
                  rscmgt_reply.type = types_dst[i];
                  hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                }
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              // we free dynamic memory generated
              free(tiles_dst); 
              free(types_dst);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              // we free the data field
              free(data.data);
              break;

            case HN_COMMAND_FIND_UNITS_SETS_RSC:
              // The operation is to find all disjoint sets of units, this function triggers resource manager functions
              // We call the find_unit_sets function from the resource manager
              found = hn_rscmgt_find_units_sets(data.tile, data.rsc_req.num_tiles, data.data, &tiles_dsts, &types_dsts, &num);
              if (num > 0) {
                // We send the sets back to the application
                rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
                rscmgt_reply.num_sets = num;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                for (j=0;j<num;j++) {
                  uint32_t *p1 = tiles_dsts[j];
                  uint32_t *p2 = types_dsts[j]; 
                  for (i=0;i<data.rsc_req.num_tiles;i++) {
                    rscmgt_reply.result = HN_RSC_DATA;
                    rscmgt_reply.tile = p1[i];
                    rscmgt_reply.type = p2[i];
                    hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
                  }
                }
              } else {
                // We send ERROR to the application
                rscmgt_reply.result = HN_RSC_OPERATION_FAILED;
                rscmgt_reply.client_id = client_id;
                hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              }
              if (num > 0) {
                // we free dynamic memory generated
                for (i=0;i<num;i++) {
                  free(tiles_dsts[i]); 
                  free(types_dsts[i]);
                }
                free(tiles_dsts);
                free(types_dsts);
              }
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              // we free the data field
              free(data.data);
              break;

            case HN_COMMAND_RESERVE_UNITS_SET_RSC:
              // The operation is to reserve a set of units, this function triggers resource manager functions
              // We call the reserve_unit_set function from the resource manager
              hn_rscmgt_reserve_units_set(data.rsc_req.num_tiles, data.data);
              // We send OK back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_RELEASE_UNITS_SET_RSC:
              // The operation is to release a set of units, this function triggers resource manager functions
              // We call the release_unit_set function from the resource manager
              hn_rscmgt_release_units_set(data.rsc_req.num_tiles, data.data);
              // We send OK back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              // we free the data field
              free(data.data);
              break;

            case HN_COMMAND_GET_NUM_TILES_RSC:
              log_debug("item_collector_thread: get_num_tiles_rsc command");
              // The command is a request to get the number of tiles
              // We call the get_num_tiles function from the resource manager
              hn_rscmgt_get_num_tiles(&num_tiles, &num_tiles_x, &num_tiles_y);
              // We send OK back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.num_tiles = num_tiles;
              rscmgt_reply.num_tiles_x = num_tiles_x;
              rscmgt_reply.num_tiles_y = num_tiles_y;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_TILE_INFO_RSC:
              // The command is a request to get the number of tiles
              log_debug("item_collector_thread: get_tile_info_rsc command. tile=%u", data.tile);
              uint32_t mem_size = 0;
              uint32_t tile_family = 0;
              uint32_t tile_conf   = 0;
              // We call the get_num_tiles function from the resource manager
              hn_rscmgt_get_tile_info(data.tile, &tile_family, &tile_conf, &mem_size);
              // We send OK back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.mem_size = mem_size;
              rscmgt_reply.type     = tile_family;
              rscmgt_reply.subtype  = tile_conf;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_MEMORY_SIZE_RSC:
              log_debug("item_collector_thread: get_memory_size_rsc command");
              // The command is a request to get the number of tiles
              // We call the get_num_tiles function from the resource manager
              hn_rscmgt_get_memory_size(data.tile, &mem_size);  
              // We send OK back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.mem_size = mem_size;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_NUMBER_OF_NETWORKS_RSC:
              log_debug("item_collector_thread: get_num_vns_rsc command");
              // The command is a request to get the number of tiles
              uint32_t num_vns = 0;
              // We call the get_num_tiles function from the resource manager
              hn_rscmgt_get_num_vns(&num_vns);
              // We send OK back to the application
              rscmgt_reply.result = HN_RSC_OPERATION_SUCCES;
              rscmgt_reply.num_vns = num_vns;
              rscmgt_reply.client_id = client_id;
              hn_buffer_fifo_write_bytes_from_buf(rscmgt_fifo, &rscmgt_reply, sizeof(rscmgt_reply));
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_TEMPERATURE_OF_FPGA:
              {
                hn_command_temperature_request_t *cmd;
                hn_command_temperature_response_t response;
                int32_t                           temperature;
                uint32_t                          rv;

                cmd = (hn_command_temperature_request_t *)(data.data);
                rv = hn_get_temperature_of_device_operation(cmd, &temperature);
               
                log_debug("item_collector_thread: get temperature of fpga for tile %d ", cmd->tile);

                // in case of error, the error code will be propgagated to the client
                response.info.command = cmd->info.command;
                response.tile         = cmd->tile;
                response.client_id    = client_id;
                response.status       = rv;
                response.temperature  = temperature;
                hn_buffer_fifo_write_bytes_from_buf(temperature_notification_fifo, &response, sizeof(hn_command_temperature_response_t));
              }
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_GET_TEMPERATURE_OF_MOTHERBOARD:
              {
                log_error("UNDER construction command (%d) in hn daemon (collector thread)\n", data.command);
              }
              free(data.data);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;

            case HN_COMMAND_CONFIGURE_DEBUG_AND_STATS:
              log_debug("item_collector_thread: configure_debug_and_stats command: tile %u config_function=%u",
                  data.tile, *((uint32_t *)data.data));
              item = hn_compose_item(data.tile, HN_MANGO_ITEM_COMMAND_CONFIGURE_DEBUG_AND_STATS, *((char *)data.data));
              if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item, 1);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              free(data.data);
              break;
            
            case HN_COMMAND_CLOCK: 
              {
                hn_clock_info_t *ci = (hn_clock_info_t *)data.data;
                log_debug("item_collector_thread: clock command: tile %u clock_function=%u num_cycles=%u",
                  data.tile, ci->clock_function, ci->num_cycles);

                char payload = (char)ci->clock_function;
                if (ci->clock_function == HN_MANGO_ITEM_COMMAND_FUNCTION_RUNFOR_CLOCK) {
                  payload |= ((char)ci->num_cycles & 0x7F);
                }
                item = hn_compose_item(data.tile, HN_MANGO_ITEM_COMMAND_CLOCK, payload);
                if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item, 1);
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
                free(data.data);
              }
              break;

            case HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_TILE:
              {
                hn_command_stats_monitor_t           *cmd;
                hn_command_stats_monitor_response_t   rsp; // response message to libhn

                cmd = (hn_command_stats_monitor_t *)(data.data);
                // move to log verbose or log debug
                log_info("item_collector_thread: enable stats monitor for tile: %u", cmd->tile);

                rsp.common.command = cmd->common.command;
                rsp.tile           = cmd->tile;
                rsp.client_id      = client_id;
                // check that tile ID is in valid range [0 : num_tiles-1];
                // if we send a cmd for a NON existing tile, that could BLOCK the network on the HN

                // get arch info
                // check that tile is not already added in list, and add it
                if(cmd->tile >= rsc_info->num_tiles)
                {
                  // error, requested tile out of range, return error
                  log_error("item_collector_thread: adding tile %d to stats monitor, out of range", cmd->tile);
                  rsp.status =  HN_STATS_MONITOR_OPERATION_ERROR_OUT_OF_RANGE;
                }
                else
                {
                  // tile is in range, add tile to list of tiles to poll stats periodically
                  if ( rsc_info->tile_info[cmd->tile].type == HN_TILE_FAMILY_PEAK)
                  {
                    rsp.status = hn_stats_monitor_add_tile(cmd->tile, client_id);

                    // this should be faster than checking if the list is empty because we do need to lock the mutex
                    if (rsp.status == 0) {
                      if(stats_monitor_active == 0)
                      {
                        // notify the stats monitor thread to resume 
                        // we do not need to update any var protecting its access with mutex since the tiles_list has its own mutex
                        pthread_mutex_lock(&mutex_stats_monitor_config_access);
                        stats_monitor_active = 1;
                        pthread_mutex_unlock(&mutex_stats_monitor_config_access);
                        pthread_cond_signal(&cond_stats_monitor_config_change);
                      }

                      
                    }
                  } else
                  {
                    log_error("NOT supported feature: Request set monitor statistics for this family type: %d", rsc_info->tile_info[cmd->tile].type);
                  }
                }

                // prepare response, all ok
                hn_buffer_fifo_write_bytes_from_buf(stats_monitor_notification_fifo, &rsp, sizeof(hn_command_stats_monitor_response_t));

                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
                free(data.data);
              }
              break;
            case HN_COMMAND_CONFIGURE_STATS_MONITOR_UNSET_TILE:
              {
                hn_command_stats_monitor_t           *cmd;
                hn_command_stats_monitor_response_t   rsp; // response message to libhn

                cmd = (hn_command_stats_monitor_t *)(data.data);
                // move to log verbose or log debug
                log_info("item_collector_thread: disable stats monitor for tile: %u", cmd->tile);

                rsp.common.command = cmd->common.command;
                rsp.tile           = cmd->tile;
                rsp.client_id      = client_id;
                // check that tile ID is in valid range [0 : num_tiles-1];
                // if we send a cmd for a NON existing tile, that could BLOCK the network on the HN

                // check that tile is not already added in list, and add it
                if(cmd->tile >= rsc_info->num_tiles)
                {
                  // error, requested tile out of range, return error
                  log_error("item_collector_thread: removing tile %d from stats monitor, out of range", cmd->tile);
                  rsp.status =  HN_STATS_MONITOR_OPERATION_ERROR_OUT_OF_RANGE;
                }
                else
                {
                  // tile is in range, add tile to list of tiles to poll stats periodically
                  rsp.status = hn_stats_monitor_remove_tile(cmd->tile, client_id);
                  pthread_mutex_lock(&mutex_stats_monitor_config_access);
                  if (hn_list_is_empty(stats_monitor_tiles_list)){
                    stats_monitor_active = 0;
                  }
                  pthread_mutex_unlock(&mutex_stats_monitor_config_access);

                }

                // prepare response, all ok
                hn_buffer_fifo_write_bytes_from_buf(stats_monitor_notification_fifo, &rsp, sizeof(hn_command_stats_monitor_response_t));

                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
                free(data.data);
              }
              break;
            case HN_COMMAND_CONFIGURE_STATS_MONITOR_SET_PERIOD:
              {
                hn_command_stats_monitor_t           *cmd;
                hn_command_stats_monitor_response_t   rsp; // response message to libhn

                cmd = (hn_command_stats_monitor_t *)(data.data);
                // move to log verbose or log debug
                log_notice("@item_collector_thread: update stats monitor poling interval to %u ms", cmd->polling_period);

                rsp.common.command = cmd->common.command;
                rsp.tile           = cmd->tile;
                rsp.polling_period = cmd->polling_period;
                rsp.client_id      = client_id;
                // check that tile ID is in valid range [0 : num_tiles-1];
                // if we send a cmd for a NON existing tile, that could BLOCK the network on the HN

                if (cmd->polling_period < HN_STATS_MONITOR_POLLING_PERIOD_MIN)
                {
                  // error, requested tile out of range, return error
                  log_error("@item_collector_thread: value %u is out of range, min pollin period value is %u ms", cmd->polling_period, HN_STATS_MONITOR_POLLING_PERIOD_MIN);
                  rsp.status =  HN_STATS_MONITOR_OPERATION_ERROR_OUT_OF_RANGE;
                }
                else
                {
                  // value is bigger that min, update
                  pthread_mutex_lock(&mutex_stats_monitor_config_access);
                  stats_monitor_polling_period_us = cmd->polling_period * 1000;
                  pthread_mutex_unlock(&mutex_stats_monitor_config_access);
                  rsp.status = HN_STATS_MONITOR_OPERATION_OK;
                }

                // prepare response, all ok
                hn_buffer_fifo_write_bytes_from_buf(stats_monitor_notification_fifo, &rsp, sizeof(hn_command_stats_monitor_response_t));

                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
                free(data.data);
              }
              break;
            case HN_COMMAND_READ_STATS_MONITOR:
              {
                hn_command_stats_monitor_t           *cmd;
                hn_command_stats_monitor_response_t   rsp; // response message to libhn
                uint32_t                              num_cores;

                cmd = (hn_command_stats_monitor_t *)(data.data);
                log_info("item_collector_thread: read stats monitor for tile: %u", cmd->tile);
                
                rsp.common.command = cmd->common.command;
                rsp.tile           = cmd->tile;
                rsp.client_id      = client_id;
               
                // check that tile ID is in valid range [0 : num_tiles-1];
                if(cmd->tile >= rsc_info->num_tiles)
                {
                  // error, requested tile out of range, return error
                  log_error("item_collector_thread: read stats for tile %d, tile id out of range", cmd->tile);
                  rsp.status =  HN_STATS_MONITOR_OPERATION_ERROR_OUT_OF_RANGE;
                  // prepare response, all ok
                  hn_buffer_fifo_write_bytes_from_buf(stats_monitor_notification_fifo, &rsp, sizeof(hn_command_stats_monitor_response_t));
                }
                else
                {
                  int tmp_pos;
                  num_cores = rsc_info->tile_info[cmd->tile].num_cores;
                   
                  if (!hn_stats_monitor_find_tile_in_list(cmd->tile, &tmp_pos))
                  {
                    log_error("tile %d in not enabled for stats monitoring");
                    rsp.status = HN_STATS_MONITOR_OPERATION_ERROR_EMPTY_TILE;
                    //rsp.status = HN_STATS_MONITOR_OPERATION_OK;
                    rsp.num_cores = 0;
                    hn_buffer_fifo_write_bytes_from_buf(stats_monitor_notification_fifo, &rsp, sizeof(hn_command_stats_monitor_response_t));
                  }
                  else 
                  {
                    uint32_t curr_core_index;

                    // let's send the response containing the number of cores to the hn_lib

                    rsp.status    = HN_STATS_MONITOR_OPERATION_OK;
                    rsp.num_cores = num_cores;
                    hn_buffer_fifo_write_bytes_from_buf(stats_monitor_notification_fifo, &rsp, sizeof(hn_command_stats_monitor_response_t));

                    pthread_mutex_lock(&mutex_stats_monitor_table);

                    for (curr_core_index = 0; curr_core_index < num_cores; curr_core_index++)
                    {

                      //rsp.core_stats.tics_kernel      = stats_monitor_table[cmd->tile][curr_core_index].tics_kernel;
                      //rsp.core_stats.tics_user        = stats_monitor_table[cmd->tile][curr_core_index].tics_user;
                      rsp.core_stats.tics_sleep       = stats_monitor_table[cmd->tile][curr_core_index].timestamp - stats_monitor_table[cmd->tile][curr_core_index].core_cycles;
                      //rsp.core_stats.num_threads      = stats_monitor_table[cmd->tile][curr_core_index].num_threads;
                      rsp.core_stats.l1i_hits         = stats_monitor_table[cmd->tile][curr_core_index].l1i_hits;
                      rsp.core_stats.l1i_misses       = stats_monitor_table[cmd->tile][curr_core_index].l1i_misses;
                      rsp.core_stats.l1d_hits         = stats_monitor_table[cmd->tile][curr_core_index].l1d_hits;
                      rsp.core_stats.l1d_misses       = stats_monitor_table[cmd->tile][curr_core_index].l1d_misses;
                      rsp.core_stats.l2_hits          = stats_monitor_table[cmd->tile][curr_core_index].l2_hits;
                      rsp.core_stats.l2_misses        = stats_monitor_table[cmd->tile][curr_core_index].l2_misses;
                      rsp.core_stats.btb_predict      = stats_monitor_table[cmd->tile][curr_core_index].btb_predict;
                      rsp.core_stats.btb_misspredict  = stats_monitor_table[cmd->tile][curr_core_index].btb_misspredict;
                      rsp.core_stats.core_cycles      = stats_monitor_table[cmd->tile][curr_core_index].core_cycles;
                      rsp.core_stats.core_instr       = stats_monitor_table[cmd->tile][curr_core_index].core_instr;
                      rsp.core_stats.core_cpi         = ((float)stats_monitor_table[cmd->tile][curr_core_index].core_instr) / ((float)stats_monitor_table[cmd->tile][curr_core_index].core_cycles);
                      rsp.core_stats.mc_accesses      = stats_monitor_table[cmd->tile][curr_core_index].mc_accesses;
                      rsp.core_stats.timestamp        = stats_monitor_table[cmd->tile][curr_core_index].timestamp;
                      memcpy((void *)&rsp.core_stats.nstats, (void *)&stats_monitor_table[cmd->tile][curr_core_index].nstats, HN_MAX_VNS * sizeof (hn_unit_net_stats_t) );

                      hn_buffer_fifo_write_bytes_from_buf(stats_monitor_notification_fifo, &rsp, sizeof(hn_command_stats_monitor_response_t));
                    }

                    pthread_mutex_unlock(&mutex_stats_monitor_table);
                  }
                }
                // we remove the request from the client's queue
                hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
                free(data.data);
              }
              break;
            default:                 
              log_error("Unrecognized command (%d) in hn daemon (collector thread)\n", data.command);
              // we remove the request from the client's queue
              hn_buffer_fifo_read_elements_to_buf(client_fifo, &data, 1);
              break;
          }
        } else if (num_elements == 0) {
          if (client->health_status == HN_CLIENT_HEALTH_STATUS_ZOMBIE) {
            pthread_mutex_lock(&client->health_mtx);
            
            // no more elements in the output buffer FIFO and the client has gone away (we have a zombie)
            // let's command the acceptance for removing it from the client list
            client->health_status = HN_CLIENT_HEALTH_STATUS_RIP;
            log_debug("item_collector_thread: client id %llu RIP", client->cid); 

            pthread_mutex_unlock(&client->health_mtx);
          }
        }
      }
      iter = hn_list_iter_next(client_list, iter);
    }
  }
  pthread_cleanup_pop(0);

  log_info("item_collector_thread: exiting...");
  return &retval;
}

/*
 * hn_compose_item function composes an item from a tile, a command, and a byte
 */
uint32_t hn_compose_item(uint32_t tile, uint32_t command, char byte)
{
  return ((tile & 0x00000FFF) << 14) | ((command & 0x0000003F) << 8) | (byte & 0x00000000FF);
}

/*
 * hn_compose_peak_item function composes a peak item from a tile, a command, and a byte
 */
uint32_t hn_compose_peak_item(uint32_t tile, uint32_t command, char byte)
{
  // the PEAK item has the same format as MANGO items
  return hn_compose_item(tile, command, byte);
}

/* 
 * hn_prepare_items_for_read_reg_operation. This function builds the items for a read register operation
 */
void hn_prepare_items_for_read_reg_operation(uint32_t tile, uint32_t reg, uint32_t *item_vector, uint32_t *num_items) {

  item_vector[0] = hn_compose_item(tile, COMMAND_READ_TILEREG, (char) (reg &  0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_READ_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  *num_items = 2;
}

/*
 * hn_prepare_items_for_write_reg_operation. This function builds the items for a write register operation
 */
void hn_prepare_items_for_write_reg_operation(uint32_t tile, uint32_t reg, uint32_t data, uint32_t *item_vector, uint32_t *num_items) {
  item_vector[0] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) (reg & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) (data & 0x000000FF));
  item_vector[3] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) ((data & 0x0000FF00) >> 8));
  item_vector[4] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) ((data & 0x00FF0000) >> 16));
  item_vector[5] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) ((data & 0xFF000000) >> 24));
  *num_items = 6;
}

/*
 * hn_prepare_items_for_read_memblock_operation. This function builds the items for a read memory block operation
 */
void hn_prepare_items_for_read_memblock_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items) {
  item_vector[0] = hn_compose_item(tile, COMMAND_READ_MEMORY_BLOCK, (char) (addr & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_READ_MEMORY_BLOCK, (char) ((addr & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_READ_MEMORY_BLOCK, (char) ((addr & 0x00FF0000) >> 16));
  item_vector[3] = hn_compose_item(tile, COMMAND_READ_MEMORY_BLOCK, (char) ((addr & 0xFF000000) >> 24));
  *num_items = 4;
}

/*
 * hn_prepare_items_for_read_memword_operation. This function builds the items for a read memory word operation
 */
void hn_prepare_items_for_read_memword_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items) {
  item_vector[0] = hn_compose_item(tile, COMMAND_READ_MEMORY_WORD, (char) (addr & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_READ_MEMORY_WORD, (char) ((addr & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_READ_MEMORY_WORD, (char) ((addr & 0x00FF0000) >> 16));
  item_vector[3] = hn_compose_item(tile, COMMAND_READ_MEMORY_WORD, (char) ((addr & 0xFF000000) >> 24));
  *num_items = 4;
}

/*
 * hn_prepare_items_for_read_memhalf_operation. This function builds the items for a read memory half word operation
 */
void hn_prepare_items_for_read_memhalf_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items) {
  log_error("function not ready\n");
  *num_items = 0;
}

/*
 * hn_prepare_items_for_read_membyte_operation. This function builds the items for a read memory byte operation
 */
void hn_prepare_items_for_read_membyte_operation(uint32_t tile, uint32_t addr, uint32_t *item_vector, uint32_t *num_items) {
  log_error("function not ready\n");
  *num_items = 0;
}

/*
 * hn_prepare_items_for_write_memblock_operation. This unction builds the items for a write memory block operation
 */
void hn_prepare_items_for_write_memblock_operation(uint32_t tile, uint32_t addr, uint8_t *data, uint32_t *item_vector, uint32_t *num_items) {
  int i;

  item_vector[0] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BLOCK, (char) (addr & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BLOCK, (char) ((addr & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BLOCK, (char) ((addr & 0x00FF0000) >> 16));
  item_vector[3] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BLOCK, (char) ((addr & 0xFF000000) >> 24));

//  for (i=0;i<64;i++) item_vector[i+4] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BLOCK, data[i]);

  for (i=0;i<64;i++) item_vector[4+i] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BLOCK, data[i]);
  *num_items = 68;
}

/*
 * hn_prepare_items_for_write_memword_operation. This unction builds the items for a write memory word operation
 */
void hn_prepare_items_for_write_memword_operation(uint32_t tile, uint32_t addr, uint32_t *data, uint32_t *item_vector, uint32_t *num_items) {
  item_vector[0] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) (addr & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) ((addr & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) ((addr & 0x00FF0000) >> 16));
  item_vector[3] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) ((addr & 0xFF000000) >> 24));

  item_vector[4] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) (*data & 0x000000FF));
  item_vector[5] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) ((*data & 0x0000FF00) >> 8));
  item_vector[6] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) ((*data & 0x00FF0000) >> 16));
  item_vector[7] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_WORD, (char) ((*data & 0xFF000000) >> 24));

  *num_items = 8;
}

/*
 * hn_prepare_items_for_write_memhalf_operation. This unction builds the items for a write memory half word operation
 */
void hn_prepare_items_for_write_memhalf_operation(uint32_t tile, uint32_t addr, uint32_t *data, uint32_t *item_vector, uint32_t *num_items) {
  item_vector[0] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, (char) (addr & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, (char) ((addr & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, (char) ((addr & 0x00FF0000) >> 16));
  item_vector[3] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, (char) ((addr & 0xFF000000) >> 24));

  item_vector[4] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, (char) (*data & 0x000000FF));
  item_vector[5] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, (char) ((*data & 0x0000FF00) >> 8));
  item_vector[6] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, 0);
  item_vector[7] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_HALF, 0);

  *num_items = 8;
}

/*
 * hn_prepare_items_for_write_membyte_operation. This unction builds the items for a write memory byte operation
 */
void hn_prepare_items_for_write_membyte_operation(uint32_t tile, uint32_t addr, uint32_t *data, uint32_t *item_vector, uint32_t *num_items) {
  item_vector[0] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, (char) (addr & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, (char) ((addr & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, (char) ((addr & 0x00FF0000) >> 16));
  item_vector[3] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, (char) ((addr & 0xFF000000) >> 24));

  item_vector[4] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, (char) (*data & 0x000000FF));
  item_vector[5] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, 0);
  item_vector[6] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, 0);
  item_vector[7] = hn_compose_item(tile, COMMAND_WRITE_MEMORY_BYTE, 0);

  *num_items = 8;
}

/*
 * hn_prepare_items_for_trigger_interrupt_command
 */
void hn_prepare_items_for_trigger_interrupt_command(uint32_t tile, uint16_t vector, uint32_t *item_vector, uint32_t *num_items) {
  // triggering interrupt means writting a register
  uint32_t reg = HN_MANGO_TILEREG_UNIT_INTERRUPTS_REG;
  item_vector[0] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char)  (reg & 0x000000FF));
  item_vector[1] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char)  (vector & 0x000000FF));
  item_vector[3] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) ((vector & 0x0000FF00) >> 8));
  item_vector[4] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) 0);
  item_vector[5] = hn_compose_item(tile, COMMAND_WRITE_TILEREG, (char) 0);
  *num_items = 6;
}

/*
 * hn_prepare_items_for_trigger_dma_command
 */
void hn_prepare_items_for_trigger_dma_command(uint32_t tile_src, uint32_t chan, uint32_t addr_src, unsigned long long size, uint32_t tile_dst, uint32_t addr_dst, uint32_t to_unit, uint32_t to_mem, uint32_t to_ext, uint32_t *item_vector, uint32_t *num_items) {
  // Programming a DMA means writting four words to a register
  uint32_t reg = HN_MANGO_TILEREG_DMA_REG;

  //log_warn("JM10, @hn_thread_item_collector_function setting trigger dma command tile_src %2u  chan %2u  addr_src 0x%08x  size %u  tile_dst %2u  addr_dst %u  to_unit %u  to_mem %u  to_ext %u",
  //    tile_src, chan, addr_src, size, tile_dst, addr_dst, to_unit, to_mem, to_ext);

  // first word: channel selection
  uint32_t data = chan;
  item_vector[0] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (reg & 0x000000FF));
  item_vector[1] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  item_vector[2] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (data & 0x000000FF));
  item_vector[3] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x0000FF00) >> 8));
  item_vector[4] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x00FF0000) >> 16));
  item_vector[5] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0xFF000000) >> 24));

  // second word: addr_src
  data = addr_src;
  item_vector[6] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (reg & 0x000000FF));
  item_vector[7] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  item_vector[8] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (data & 0x000000FF));
  item_vector[9] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x0000FF00) >> 8));
  item_vector[10] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x00FF0000) >> 16));
  item_vector[11] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0xFF000000) >> 24));

  // third word: addr_dst
  data = addr_dst;
  item_vector[12] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (reg & 0x000000FF));
  item_vector[13] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  item_vector[14] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (data & 0x000000FF));
  item_vector[15] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x0000FF00) >> 8));
  item_vector[16] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x00FF0000) >> 16));
  item_vector[17] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0xFF000000) >> 24));

  // fourth word: {node_dst, function} (function is 4 bits and node_dst is 12 bits)
  uint32_t node_dst_type = to_unit ? HN_MANGO_UNIT_ID : to_mem ? HN_MANGO_MEM_ID : to_ext ? HN_MANGO_EXT_ID : 0;
  uint32_t node_dst_tile = tile_dst;
  uint32_t function = 1;
  data = (node_dst_tile << 7) | (node_dst_type << 4) | function;
  item_vector[18] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (reg & 0x000000FF));
  item_vector[19] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  item_vector[20] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (data & 0x000000FF));
  item_vector[21] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x0000FF00) >> 8));
  item_vector[22] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x00FF0000) >> 16));
  item_vector[23] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0xFF000000) >> 24));

  // fifth word: num_blocks_to_transfer
  data = (uint32_t)(size + ((((size + 63)/64)*64) - size)) / 64;
  item_vector[24] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (reg & 0x000000FF));
  item_vector[25] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((reg & 0x0000FF00) >> 8));
  item_vector[26] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char)  (data & 0x000000FF));
  item_vector[27] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x0000FF00) >> 8));
  item_vector[28] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0x00FF0000) >> 16));
  item_vector[29] = hn_compose_item(tile_src, COMMAND_WRITE_TILEREG, (char) ((data & 0xFF000000) >> 24));

  *num_items = 30;
} 

/*
 *
 */
uint32_t hn_shared_memory_create_buffer_operation(hn_command_shm_buffer_create_t *shm_cmd, char *name)
{
  int                    ri;
  uint32_t               rv;
  hn_shm_buffer_info_t  *buffer_info;
  void                  *buff_addr_ptr;
  int                    buff_fd;
  char                   buff_name[SHM_POSIX_NAME_MAX_LEN];
  

  // allocate memory for local information, to add to list of shared memory buffers
  //  the first, to abort the operation in case the malloc fails
  buffer_info = (hn_shm_buffer_info_t *)malloc(sizeof(hn_shm_buffer_info_t));
  if (buffer_info == NULL)
  {
    // error, return error
    log_error("daemon, allocating memory for new shm_buffer information");
    return 1;
  }

  // we create buffer in shared memory region
  rv = hn_shared_memory_create_buffer(buff_name, shm_cmd->size , &buff_addr_ptr, &buff_fd);
  if ( rv != 0 ) 
  {
    log_error ("daemon, creating shared memory buffer");
    return 2;
  }

  // we add new buffer to shared_memory_buffer_list
  strcpy(buffer_info->name, buff_name);
  buffer_info->size = shm_cmd->size;
  buffer_info->addr = buff_addr_ptr;
  buffer_info->fd   = buff_fd;
  log_debug("shm buffer succesfully created with name: %s    local_fd: %d    local_addr: %x    size: %u", buffer_info->name, buffer_info->fd, buffer_info->addr, buffer_info->size );
  log_debug("           adding buffer_info entry to list..., delete-me");
  ri = hn_list_add_to_end(shared_memory_buffer_list, buffer_info);
  if (ri != 0)
  {
    // error adding info to list, destroy buffer and notify error to application
    log_error("adding shm_buffer information entry to list");
    hn_shared_memory_destroy_buffer(buff_name, shm_cmd->size , buff_addr_ptr, buff_fd);
    return 3;
  }

  // everyting ok, return 0
  strcpy(name, buff_name);
  return 0;
}


/*! 
 * \brief Search for buffer in shared memory area, close it and release resources on daemon side
 */
uint32_t hn_shared_memory_destroy_buffer_operation(hn_command_shm_buffer_destroy_t *shm_cmd)
{
  int                   iter;
  uint32_t              rdu;
  hn_shm_buffer_info_t *shm_buffer;
  int                   buffer_found;
  int                   buffer_iter;

  log_debug(" IN function hn_shared_memory_destroy_buffer_operation, delete-me");
  log_debug("    shm_cmd %d    shm_name%s", (shm_cmd->info).command, shm_cmd->name);
  
  buffer_found = 0;
  iter         = hn_list_iter_front_begin(shared_memory_buffer_list);
  while((!hn_list_iter_end(shared_memory_buffer_list, iter)))
  {
    shm_buffer = (hn_shm_buffer_info_t *)hn_list_get_data_at_position(shared_memory_buffer_list, iter);
    log_debug("    checking iter id %d,   name %s", iter, shm_buffer->name);
    
    if ((shm_buffer != NULL) && (strcmp(shm_buffer->name, shm_cmd->name) == 0))
    {
      // we found it
      // close buffer on this side
      rdu = hn_shared_memory_destroy_buffer(shm_buffer->name, shm_buffer->size, shm_buffer->addr, shm_buffer->fd);
      if (rdu != 0)
      {
        log_error("destroying shared memory buffer on hn_shm_free request");
        log_error("Continue releasing operations, will let buffer useless");
      }
      buffer_found = 1;
      buffer_iter  = iter;
      // set iterator to end of the list to relesase semaphores
      hn_list_iter_end(shared_memory_buffer_list, HN_LIST_ITER_END);

      break;
    }
    iter = hn_list_iter_next(shared_memory_buffer_list, iter) ;
  };

  // Check whether we finished reading the list.... otherwise notify the list "handler" we are done, to release the semaphore
  if(!hn_list_iter_end(shared_memory_buffer_list, iter))
  {
      hn_list_iter_end(shared_memory_buffer_list, HN_LIST_ITER_END);
  }

  if (buffer_found != 0)
  {
    log_debug("daemon: shm_buffer to destroy found: %s,  delete-me", shm_cmd->name);
      //remove entry from list
      hn_list_remove_element_at(shared_memory_buffer_list, buffer_iter);
      free (shm_buffer);
      return rdu;
  }
  else
  {
    log_error("daemon: hn_shm_free, no buffer info found for name: %s", shm_cmd->name);
    return 1;
  }

  
  // ERROR, if we reach this point, we did not found the requested buffer name
  log_error("destroying shared memory buffer. Unknown name: %s");
  return 1;
}

/*
 *
 */
void hn_prepare_data_for_shm_transfer_operation(hn_command_shm_buffer_transfer_t *shm_operation, hn_shm_buffer_info_t *shm_buffer_info, hn_shm_buffer_transfer_t *transfer_info, uint32_t client_id)
{
  char *local_addr;

  log_debug("hn_prepare_data_for_shm_transfer_operation");
  //log_debug("  shm_buffer_info->addr %x",  shm_buffer_info->addr);

  local_addr = ((char *)shm_buffer_info->addr) + (shm_operation->offset);
  
  //log_debug("local address %x", (void *)local_addr);
  
  log_debug("  client_id %d  operation %d   host_shm_addr %016x   dst_tile %u    MC dst_addr %08X    tr_size %ubytes    blocking %u",
      client_id, shm_operation->info.command, (void *)local_addr, shm_operation->tile, shm_operation->addr, shm_operation->size, shm_operation->blocking
      );
  
  transfer_info->client_id = client_id;
  transfer_info->channel   = NULL; // will be later annotated in backend function when the channel is assigned
  transfer_info->type      = shm_operation->info.command;
  transfer_info->shm_addr  = local_addr;
  transfer_info->tile      = shm_operation->tile;
  transfer_info->addr      = shm_operation->addr;
  transfer_info->size      = shm_operation->size;
  transfer_info->blocking  = shm_operation->blocking;
  log_debug("  data set");
}

/*
 *
 */
uint32_t hn_shared_memory_transfer_buffer_operation(hn_command_shm_buffer_transfer_t *shm_cmd, hn_shm_buffer_info_t **shm_buffer_info)
{
  int                   iter;
  hn_shm_buffer_info_t *shm_buffer;
  int                   buffer_found;
  char                 *shm_addr_base;
  char                 *shm_addr_end;
  char                 *shm_addr_req;

  log_debug(" IN function hn_shared_memory_transfer_buffer_operation, delete-me");
  log_debug("    shm_cmd %d    shm_name%s", (shm_cmd->info).command, shm_cmd->name);

  buffer_found = 0;
  iter         = hn_list_iter_front_begin(shared_memory_buffer_list);
  while((!hn_list_iter_end(shared_memory_buffer_list, iter)))
  {
    shm_buffer = (hn_shm_buffer_info_t *)hn_list_get_data_at_position(shared_memory_buffer_list, iter);
    log_debug("    checking iter id %d,   name %s", iter, shm_buffer->name);

    if ((shm_buffer != NULL) && (strcmp(shm_buffer->name, shm_cmd->name) == 0))
    {
      // we found it
      buffer_found = 1;
      // set iterator to end of the list to relesase semaphores
      hn_list_iter_end(shared_memory_buffer_list, HN_LIST_ITER_END);
      break;
    }
    iter = hn_list_iter_next(shared_memory_buffer_list, iter) ;
  };

  if (buffer_found == 0)
  {
    log_error("daemon: hn_shm_write, no buffer info found for name: %s", shm_buffer->name);
    return 1;
  }

  log_debug("daemon: shm_buffer found: %s,  delete-me", shm_cmd->name);

  //checking transfer parameters are in range of buffer limits
  // offset + transfer_size 
  shm_addr_base = (char *)(shm_buffer->addr);
  shm_addr_end  = (char *)(shm_addr_base + (shm_buffer->size));
  shm_addr_req  = (char *)(shm_addr_base + shm_cmd->offset);
  if ((shm_addr_base <= shm_addr_req) && (shm_addr_end >= shm_addr_req) )
  {
    log_debug("  shm buffer start addres for transfer is in memroy range");
    // we found it
    // check requested buffer transfer is inside buffer memory space
    if((shm_addr_req + shm_cmd->size) > shm_addr_end)
    {
      log_error("  request shm transfer operation out of buffer bounds");
      log_error("  request transfer base addr %p", shm_addr_req);
      log_error("          transfer size %u bytes", shm_cmd->size);
      log_error("  buffer is [%p, %p]", shm_addr_base, shm_addr_end);
      return 1;
    }
  }
  // requestred buffer transfer is in range of current buffer area, continue with transfer operations
  // JM PEPE RAFA
  // TO-TO decide the point of the code where the shm transfer has to be programmed and the type of information to store
  //   and how to manage it... 
  *shm_buffer_info = shm_buffer;

  return 0;
} 

/*
 * hn_shared_memory_launch_upstream_transfer
 */
void hn_shared_memory_launch_upstream_transfer(uint32_t tile_src, uint32_t chan, uint32_t addr_src, unsigned long long size, uint32_t tile_dst, uint32_t addr_dst) {
  uint32_t item_vector[100];
  uint32_t num_items;

  hn_prepare_items_for_trigger_dma_command(tile_src, chan, addr_src, size, tile_dst, addr_dst, 0, 0, 1, item_vector, &num_items);

  if (!filter_out_item_fifo) hn_buffer_fifo_write_elements_from_buf(id_item_fifo, &item_vector, num_items);
} 

uint32_t hn_get_temperature_of_device_operation(hn_command_temperature_request_t *cmd, int32_t *temperature)
{
  uint32_t   rv;
  char       mb_desc[6];
  char       fm_desc[6];
  char      *fm_desc_ptr;

  
  if(cmd->info.command != HN_COMMAND_GET_TEMPERATURE_OF_FPGA)
  {
    fm_desc_ptr = NULL;
  }
  else
  {
    fm_desc_ptr = fm_desc;
  }

  rv = hn_rscmgt_get_tile_location(cmd->tile, mb_desc, fm_desc);
  
  if(rv != 0)
  {
    log_error("getting physical location of tile %u", cmd->tile);
    return 1;
  }

  rv = hn_iface_mmi64_get_device_temperature(mb_desc, fm_desc_ptr, temperature);
  if(rv != 0)
  {
    log_error("getting temperature of fpga for tile %u", cmd->tile);
    return 2;
  }


  return 0;
}

//---------------------------------------------------------------------------------------------------------------------
// STATS MONITOR
//  hn_stats_monitor_add_tile
//  hn_stats_monitor_remove_tile

// search if a tile is in the list
uint32_t hn_stats_monitor_find_tile_in_list(uint32_t tile, int *position_in_list)
{
  uint32_t *tile_id_at_iter_ptr;
  uint32_t tile_found = 0;
  int      iter;
 
  //pthread_mutex_lock(&mutex_stats_monitor_config_access);
  if (hn_list_is_empty(stats_monitor_tiles_list))
  {
    // we firt check if the list is empty
  }
  else 
  {
    iter = hn_list_iter_front_begin(stats_monitor_tiles_list);
    while (!hn_list_iter_end(stats_monitor_tiles_list, iter))
    {
      tile_id_at_iter_ptr = (uint32_t *)hn_list_get_data_at_position(stats_monitor_tiles_list, iter);
      log_verbose("  iter: %d,  ptr %p,  id %u", iter, tile_id_at_iter_ptr, *tile_id_at_iter_ptr);

      if((*tile_id_at_iter_ptr) == tile)
      {
        tile_found = 1;
        *position_in_list = iter;
        hn_list_iter_end(stats_monitor_tiles_list, HN_LIST_ITER_END);
        break;
      }
      iter = hn_list_iter_next(stats_monitor_tiles_list, iter);
    };

    //// let's make sure the list is closed to enable later access...
    hn_list_iter_end(stats_monitor_tiles_list, HN_LIST_ITER_END);
  }
  //pthread_mutex_unlock(&mutex_stats_monitor_config_access);
  log_debug("  tile %d %sfound", tile, tile_found?"":"not ");
  return tile_found;
}

// --------------------------------------------------------------------------------------------------------------------
// add tile to list of statst monitor
//   checks that tile is not already added in the list
uint32_t hn_stats_monitor_add_tile(uint32_t tile, unsigned long long client_id)
{
  int       pos;
  int       radd;
  uint32_t *new_tile_id_ptr;
  
  log_debug("Adding tile %u to stats monitor list", tile);

  if(hn_stats_monitor_find_tile_in_list(tile, &pos) == 0)
  {
    new_tile_id_ptr = malloc(sizeof(uint32_t));
    if (new_tile_id_ptr == NULL)
    {
      log_error("hn_stats_monitor_add_tile, malloc error requesting memory for new entry in stats_monitor_tiles_list");
      return 1;
    }
    (*new_tile_id_ptr) = tile;
   
    //pthread_mutex_lock(&mutex_stats_monitor_config_access);
    radd = hn_list_add_to_end(stats_monitor_tiles_list, new_tile_id_ptr);
    //pthread_mutex_unlock(&mutex_stats_monitor_config_access);
    if (radd != 0)
    {
      // error adding info to list,return error
      log_error("hn_stats_monitor_add_tile, adding tile to list");
      free(new_tile_id_ptr);
      return 2;
    }
    hn_stats_monitor_add_client_for_tile_stats(tile, client_id); 
    log_debug("  done, tile %u added to stats monitor", tile);
  }
  else
  {
    // tile was already in list, we will not add it, will return success
    hn_stats_monitor_add_client_for_tile_stats(tile, client_id); 
    log_info("hn_stats_monitor_add_tile, tile already enlisted, stats already available");
  }

  // all done, tile added (if not already)
  return 0;
}

// --------------------------------------------------------------------------------------------------------------------
// remove tile from list of stats monitor
//   checks that tile was in the list
uint32_t hn_stats_monitor_remove_tile(uint32_t tile, unsigned long long client_id)
{
  int       pos;
  uint32_t *tile_id_ptr;
  uint32_t  rv = 0;
  uint32_t num_clients;

  log_debug("Removing tile %u from stats monitor list", tile);

  if(hn_stats_monitor_find_tile_in_list(tile, &pos) != 0)
  {
    // tile was in the list, at position "pos"
    //pthread_mutex_lock(&mutex_stats_monitor_config_access);
    rv = hn_stats_monitor_remove_client_for_tile_stats(tile, client_id);
    if (rv != 0) log_error("hn_stats_monitor error for tile: %u, number of clients getting stats mismatch", tile);

    // we now check how many clients are getting the stats of this tile, we will only remove the tile when the last client disconnects
    hn_stats_monitor_get_num_clients_for_tile_stats(tile, &num_clients);
    if(num_clients == 0)
    {
      // we only remove the client from the list when no client is reading the stats of this tile
      tile_id_ptr = (uint32_t *) hn_list_get_data_at_position(stats_monitor_tiles_list, pos);
      hn_list_remove_element_at(stats_monitor_tiles_list, pos);
      //pthread_mutex_unlock(&mutex_stats_monitor_config_access);
      free(tile_id_ptr);
      log_debug("  done, tile %u removed from stats monitor, no more clients requesting stats of this tile\n", tile);
    }
    else
    {
      log_debug("   not yet, tile %u NOT removed from stats list because there is(are) still %u client(s) reading stats from this tile", tile, num_clients);
    }
  }
  else
  {
    // tile was not found in list, we will not add it, will return success
    log_error("hn_stats_monitor_remove_tile, tile id %u not found", tile);
    rv = 1;
  }

  // all done
  return rv;
}

