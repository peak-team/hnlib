#ifndef __HN_ITEM_HANDLER_H__
#define __HN_ITEM_HANDLER_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 29, 2018
// File Name: hn_request_handler.h
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//   Handler for the items
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stddef.h>
#include <stdint.h>

#include "hn_item_commands.h"

typedef struct hn_mango_tilereg_0_st
{
  uint32_t arch_id:8;
  uint32_t padding:24;
}__attribute__((packed)) hn_mango_tilereg_0_t;

/*!
 * \brief MANGO item data type
 */
typedef struct hn_item_st
{
  uint32_t payload:8;   //<! item payload
  uint32_t command:6;   //<! item command
  uint32_t tile:9;      //<! item tile 
  uint32_t padding:9;   //<! item padding
}__attribute__((packed)) hn_item_t;

/*!
 * \brief upstream commands enumeration
 */
typedef enum
{
  HN_ITEM_UPSTREAM_COMMAND_READ_REGISTER_FIRST = COMMAND_TILEREG_FIRST_ITEM,
  HN_ITEM_UPSTREAM_COMMAND_READ_REGISTER_REMAINING = COMMAND_TILEREG_REMAINING_ITEMS,
  // ...
  HN_ITEM_UPSTREAM_COMMAND_END_LIST = 255
}hn_item_upstream_command_type_t;

/*!
 * \brief downstream commands enumeration
 */
typedef enum
{
  HN_ITEM_DOWNSTREAM_COMMAND_RESET         = COMMAND_RESET_SYSTEM,
  HN_ITEM_DOWNSTREAM_COMMAND_READ_REGISTER = COMMAND_READ_TILEREG,
  // ...
  HN_ITEM_DOWNSTREAM_COMMAND_END_LIST = 255
}hn_item_downstream_command_type_t;

/*!
 * \brief Item upstream handler prototype definition
 *
 * \param [in] item pointer to the item data type \see{#hn_item_t}
 * \param [out] buf pointer processed data for the incomming item
 * \returns the number of bytes actually processed
 */
typedef size_t (*hn_item_upstream_handler_t)(hn_item_t *item, void *buf);

/*!
 * \brief Item downstream handler prototype definition
 *
 * \param [out] item_vector pointer to the item data type \see{#hn_item_t}
 * \param [in] item_vector_size the size of item_vector
 * \param [in] tile the tile the item is for
 * \param [in] subtile the subtile inside the tile this item is for (for hierachy level tiles)
 * \param [in] buf data payload for the items to generate
 * \param [in] buf_size the size of buf
 * \returns the number of items generated actually
 */
typedef size_t (*hn_item_downstream_handler_t)(hn_item_t *item_vector, size_t item_vector_size, 
                      unsigned int tile, unsigned int subtile, void *buf, size_t buf_size);

/*!
 * \brief Registers an item upstream handler for the command type of the item 
 *
 *  An upstream handler gets an item and generates the value depending on the command
 *  It can be only a handler for command type. 
 *
 * \param [in] type the command type of the item
 * \param [in] handle the function handler to process the item
 * \retval  0 in case of success
 * \retval -1 in case of failure
 */
int hn_item_upstream_register_handler(hn_item_upstream_command_type_t type, hn_item_upstream_handler_t handle);

/*!
 * \brief Deregisters an upstream handler
 *
 * \param type the command type for the handler to be deregister
 * \retval  0 in case of success
 * \retval -1 in case of failure
 */
int hn_item_upstream_deregister_handler(hn_item_upstream_command_type_t type);

/*!
 * \brief Registers an item downstream handler for the command type of the item 
 *
 *  A downstream handler generates the corresponding items depending on the command
 *  It can be only a handler for command type. 
 *
 * \param [in] type the command type of the item
 * \param [in] handle the function handler to process the item
 * \retval  0 in case of success
 * \retval -1 in case of failure
 */
int hn_item_downstream_register_handler(hn_item_downstream_command_type_t type, hn_item_downstream_handler_t handle);

/*!
 * \brief Deregisters an item downstream handler
 *
 * \param type the command type for the handler to be deregister
 * \retval  0 in case of success
 * \retval -1 in case of failure
 */
int hn_item_dowstream_deregister_handler(hn_item_downstream_command_type_t type);

/*!
 * \brief Runs the upstream handler for the incomming items
 *
 * The handler transform incomming items into
 * the valuable data 
 *
 * \param [in] item_buf pointer to bufer with the items (\see{#hn_item_t}) to process 
 * \param [in] num_items number of items in item_buf
 * \param [in] buf_size the size of buf in bytes
 * \param [out] buf pointer the buffer where the result of the processing will be stored
 * \returns the bytes actually processed
 */
size_t hn_item_upstream_handle(hn_item_t *item_buf, size_t num_items, void *buf, size_t buf_size);

/*!
 * \brief Runs the downstream handler
 *
 * The handler transform incomming data into items depending on the command
 *
 * \param [out] item_buf pointer to bufer with the generated items (\see{#hn_item_t})
 * \param [in] item_buf_size size of item buf (in number of items)
 * \param [in] tile the tile the items are for
 * \param [in] subtile the subtile the items are for
 * \param [in] cmd the type of the items to generate
 * \param [in] buf pointer to the buffer with the data required by the items to be generated
 * \param [in] buf_size size of buf
 * \returns the number of items generated actually 
 */
size_t hn_item_downstream_handle(hn_item_t *item_buf, size_t item_buf_size, unsigned int tile, unsigned int subtile, 
        hn_item_downstream_command_type_t cmd, void *buf, size_t buf_size);

#endif
