#ifndef __HN_REQUEST_HANDLER_H__
#define __HN_REQUEST_HANDLER_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 29, 2018
// File Name: hn_request_handler.h
// Design Name:
// Module Name: HN Daemon
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//   Handler for the requests comming from the application clients
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stddef.h>

#include "hn_daemon.h"

/*!
 * \brief data type for moving request from the clients to the different threads in the daemon
 *
 * It is the green square in the ppt figure
 */

/*!
 * \brief clock managing request info structure
 */
typedef struct hn_clock_info_st
{
  uint32_t clock_function;  //!< clock function (pause, resume, run for)
  uint32_t num_cycles;      //!< number of cycles for the run for function
}hn_clock_info_t;

typedef struct hn_rsc_request_st
{
  uint32_t id;
  uint32_t tile_src;
  uint32_t tile_dst;
  uint32_t tile_mem;
  uint32_t num_tiles;
  unsigned long long size;
  unsigned long long bw;
}hn_rsc_request_t;

typedef struct hn_dma_request_st
{
  uint32_t id;
  uint32_t tile_src;
  uint32_t addr_src;
  unsigned long long size;
  uint32_t tile_dst;
  uint32_t addr_dst;
  uint32_t to_unit;
  uint32_t to_mem;
  uint32_t to_ext;
  uint32_t notify;
}hn_dma_request_t;

typedef struct hn_request_st
{
  uint8_t  command;        //!< Command 
  uint16_t tile;           //!< Tile
  uint16_t subtile;        //!< Tile inside of the Unit this command is for. For single-unit tiles set 0 
  uint32_t fpga_address;   //!< Target address in the FPGA for the command.
  uint32_t host_address;   //!< Target address in the host for the command
  hn_rsc_request_t rsc_req;
  hn_dma_request_t dma_req;
  uint32_t size;           //!< Size of the data command 
  void     *data;          //!< Pointer to the data required for the command
}hn_request_t;

/*!
 * \brief handler prototype definition
 *
 * \param [out] req pointer in which the created request will be stored
 * \param [in] buf pointer to the incomming data from the client to transform in an HN daemon request
 * \param [in] buf_size size of the buffer 'buf'
 * \returns the number of bytes consumed from the 'buf' in processing the request command
 */
typedef size_t (*hn_request_handler_t)(hn_request_t *req, void *buf, size_t buf_size);

/*!
 * \brief Registers a request handler for the command type request
 *
 *  It can be only a handler for command type. It should deregister a handler
 *  before adding a new one
 *
 * \param [in] type the command type request identificator
 * \param [in] handle the function handler to process the request
 * \retval  0 in case of success
 * \retval -1 in case of failure
 */
int hn_request_register_handler(hn_command_type_t type, hn_request_handler_t handle);

/*!
 * \brief Deregisters a handler
 *
 * \param type the command type for the handler to be deregister
 * \retval  0 in case of success
 * \retval -1 in case of failure
 */
int hn_request_deregister_handler(hn_command_type_t type);

/*!
 * \brief Runs the handler for the incomming data
 *
 * The Request handler transform incomming request from the clients into
 * the daemon request structure formate (\see{#hn_request_t})
 *
 * \param [out] req_vector pointer in which the created requests will be stored
 * \param [in]  max_requests the maximum number of request to handle in an iteration
 * \param [in] buf pointer incomming data from the client to transform in an HN daemon request
 * \param [in] buf_size size of the buffer 'buf'
 * \param [out] bytes_consumed pointer the number of bytes consumed from 'buf'
 * \returns the number of created requests
 * \retval 0 in case of no request can be created
 */
size_t hn_request_handle(hn_request_t *req_vector, size_t max_request, void *buf, size_t buf_size, size_t *bytes_consumed);

#endif
