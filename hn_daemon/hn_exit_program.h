#ifndef __HN_EXIT_PROGRAM_H__
#define __HN_EXIT_PROGRAM_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 23, 2018
// File Name: hn_exit_program.h
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Utility to know flag the termination of the program in an ordered way
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*!
 * \brief flags the termination of the program 
 *
 * \param [in] exit status
 */
void hn_exit_program_terminate(int status);

/*
 * \brief checks if the termination of the program has been flagged
 *
 * \retval 1 the termination of the program has been flagged
 * \retval 0 the termination of the program has not been flagged
 */
int hn_exit_program_is_terminated();


/*!
 * \brief Gets the exit status of the program
 *
 * \return the exit status set when terminating the program. \see{#hn_terminate_program()}
 */
int hn_exit_program_get_exit_status();

/*!
 * \brief Installs a termination handler for a signal
 *
 * \retval  0 if the handler has been installed correctly
 * \retval -1 in case of failure installing the handler
 */
int hn_exit_program_install_handler(int signum);

#endif
