///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: January 30, 2018
// Design Name: 
// Module Name: Interface connectivity test 
// Project Name: HN daemon
// Target Devices:
// Tool Versions:
// Description:
//  Open communication channel (handler) with profpga-mmi64 modules
//  Scan for MMI64 modules in HN system that belong to the daemon communications infrastructure
//  Close communications channel and release resources
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <time.h>

#define _HN_SHARED_STRUCTURES_DEFINITION
#include "hn_shared_structures.h"

#include "hn_logging.h"
#include "hn_iface_mmi64.h"

/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/
int main (int argc, char *argv[]) 
{
  uint32_t status;
  
  if (hn_log_initialize(HN_LOG_TO_STDOUT, NULL) < 0)
  {
    printf("ERROR initializing log system: %s\n", strerror(errno));
    return 2;
  }

  log_info("\n");
  log_info("---------------------------------------------------------");
  log_info(" This is a basic test of mmi64 MB interface connectivity ");
  log_info("      open communications interface                      ");
  log_info("      scan for mmi64 modules for HN communications       ");
  log_info("      close communications interface                     ");
  log_info("---------------------------------------------------------");

  log_info("\n");
  log_info("Initializing mmi64 communications interface through usb device");
  status = hn_iface_mmi64_init("mmi64");

  if(status != 0) {
    log_error("Error, something went unexpectedly wrong");
    log_error("mmi64 support in hn_daemon not initialized");
    log_error("No handler to proFPGA systems loaden nor buffer allocated");
    log_error("Test cancelled");
    return status;
  }

  log_info("handler to mmi64 infrastructure succesfully loaded");
  log_info("pointers to mmi64 hn modules succesfully assigned");
  log_info("upstream modules initialized");
  log_info("\n");

  log_info("... stopping test");
  log_info("\n");
  log_info("Release resources");
  hn_iface_mmi64_release();
  
  hn_log_close();

  printf("\n");
  printf("All done\n");
  printf("  Bye bye\n");


  return status;
}
//*********************************************************************************************************************
// end of file test_iface_connectivity.c
//*********************************************************************************************************************

