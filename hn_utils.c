///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomarm10@gap.upv.es)
//
// Create Date: february 21, 2017
// Design Name:
// Module Name: Heterogeneous node
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    Heterogeneous node background communication process
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include "hn_utils.h"
/**********************************************************************************************************************
 **
 ** macros
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Data structures
 **
 **********************************************************************************************************************
 **/

/**********************************************************************************************************************
 **
 ** Global variables
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/
/**********************************************************************************************************************
 **
 ** Public Functions implementation
 **
 **********************************************************************************************************************
 **/

/******************************************************************************
 ******************************************************************************
 **/
unsigned char hn_utils_hex_to_val(char digit)
{
  if ((digit >= '0') && (digit <= '9')) return digit - '0';
  if ((digit >= 'a') && (digit <= 'f')) return 10 + (digit - 'a');
  if ((digit >= 'A') && (digit <= 'F')) return 10 + (digit - 'A');

  return 0;
}
/******************************************************************************
 ******************************************************************************
 **/

unsigned long int hn_utils_hex_addr_to_ul( uint8_t hex_value[8]) {
  unsigned long int ul_value;
  ul_value =   ((unsigned long int)hn_utils_hex_to_val(hex_value[0]) * (unsigned long int)268435456 ) + ( (unsigned long int)hn_utils_hex_to_val(hex_value[1]) * (unsigned long int)16777216 )
             + ((unsigned long int)hn_utils_hex_to_val(hex_value[2]) *   (unsigned long int)1048576 ) + ( (unsigned long int)hn_utils_hex_to_val(hex_value[3]) *    (unsigned long int)65536 )
             + ((unsigned long int)hn_utils_hex_to_val(hex_value[4]) *     (unsigned long int) 4096 ) + ( (unsigned long int)hn_utils_hex_to_val(hex_value[5]) *      (unsigned long int)256 )
             + ((unsigned long int)hn_utils_hex_to_val(hex_value[6]) *        (unsigned long int)16 ) + ( (unsigned long int)hn_utils_hex_to_val(hex_value[7])            )  ;
  
  return ul_value;
}

float get_elapsed_time_seconds(struct timespec start, struct timespec end)
{
  struct timespec   temp;
  unsigned long int elapsed_nanoseconds;

  if ((end.tv_nsec - start.tv_nsec) < 0)
  {
    temp.tv_sec = end.tv_sec - start.tv_sec - 1;
    temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
  }
  else
  {
    temp.tv_sec = end.tv_sec - start.tv_sec;
    temp.tv_nsec = end.tv_nsec - start.tv_nsec;
  }

  elapsed_nanoseconds = temp.tv_sec * 1000000000 + temp.tv_nsec;

  return (float)(elapsed_nanoseconds) / 1e9;
}

//*********************************************************************************************************************
// end of file hn_utils.cpp
//*********************************************************************************************************************

