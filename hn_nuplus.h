#ifndef __HN_NUPLUS_H__
#define __HN_NUPLUS_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: February 12, 2018
// File Name: hn_nuplus.h
// Design Name:
// Module Name:
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    HN functions for NUPLUS unit header file
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created - R. Tornero (ratorga@gap.upv.es)
//   // TODO add here revisions and author
//
// Additional Comments: NONE
//
//   // TODO describe here additions
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//! \file

#ifdef __cplusplus
extern "C" {
#endif

//--------------------------------------------------------------------------------------------------
//BBBBBBBBBEEEEEEEEEEEEEEWWWWWWWWWWWWWWAAAAAAAAAAARRRRRRRRRRRRREEEEEEEEEEEEE!!!!!!!!!!!!!!!!!!!!!!!!
//
// NU+ MODELS GO FROM 50 TO 99 INCLUSIVE TO BE ALIGNED WITH BBQUE
//-------------------------------------------------------------------------------------------------- 
/*!
 * \brief Model id for the base nuplus model
 */
#define HN_NUPLUS_MODEL_BASE  50 

// NUPLUS Commands
#define	COMMAND_NUPLUS_SET_PC               0
#define	COMMAND_NUPLUS_SET_THREAD_MASK      2
#define	COMMAND_NUPLUS_READ_STATUS          8
#define	COMMAND_NUPLUS_WRITE_STATUS         9
#define	COMMAND_NUPLUS_LOG_REQUEST          10

// NUPLUS Messages
#define	NUPLUS_BOOT_ACK                     1
#define	NUPLUS_ENABLE_CORE_ACK              3
#define	NUPLUS_START_MEX                    7

// NUPLUS Control Registers
#define	NUPLUS_CORE_ID                      1
#define	NUPLUS_THREAD_ID                    2
#define	NUPLUS_GLOBAL_ID                    3
#define	NUPLUS_GCOUNTER_LOW                 4
#define	NUPLUS_GCOUNTER_HIGH                5
#define	NUPLUS_THREAD_EN                    6
#define	NUPLUS_MISS_DATA                    7
#define	NUPLUS_MISS_INSTR                   8
#define	NUPLUS_PC                           9
#define	NUPLUS_TRAP_REASON                  10
#define	NUPLUS_THREAD_STATUS                11
#define	NUPLUS_KERNEL_ARGC                  12
#define	NUPLUS_KERNEL_ARGV                  13
#define	THREAD_NUMB_ID                      14
#define	THREAD_MISS_CC_ID                   15
#define	KERNEL_WORK                         16

// NUPLUS Thread Status
#define NUPLUS_THREAD_IDLE                  0
#define	NUPLUS_THREAD_RUNNING               1
#define	NUPLUS_THREAD_END_MODE              2
#define	NUPLUS_THREAD_TRAPPED               3
#define	NUPLUS_THREAD_WAITING_BARRIER       4
#define	NUPLUS_THREAD_BOOTING               5

// NUPLUS Default Configurations
#define NUPLUS_DEFAULT_PC                   0x00000400
#define NUPLUS_DEFAULT_ARGS_THREAD_ID       5
#define NUPLUS_DEFAULT_ARGS_ADDRESS         0x00000000
#define NUPLUS_DEFAULT_CORE_NUMB            1
#define NUPLUS_MAX_THREAD_NUMB              8
#define NUPLUS_MAX_ARGS                     240

// NUPLUS Logger Commands
#define NUPLUS_LOGGER_SNOOP_CORE            0
#define NUPLUS_LOGGER_SNOOP_MEM             1
#define NUPLUS_LOGGER_GET_EVENT_CORE        2
#define NUPLUS_LOGGER_GET_EVENT_MEM         3
#define NUPLUS_LOGGER_GET_EVENT_TOT         4

typedef struct {
    uint32_t wr;
    uint32_t id;
    uint32_t data [16];
    uint32_t addr;
} nup_logger_entry_t;

/*!
 * \brief Returns an string representation for the unit model given
 *
 * \param [in] model the model the string representation will be for
 * \return an string representatiton of the model
 * \retval NULL the model does not match any of the supported models
 */
const char *hn_nuplus_to_str_unit_model(uint32_t model);

/*!
 * \brief Gets the utilization of a NU+ unit
 *
 * \param [in] tile the tile the NU+ unit utilization is going to be obtained for
 * \param [out] utilization the utilization of the given unit
 * \retval HN_SUCCEEDED in case of success or one of the error codes in hn.h otherwise
 */
uint32_t hn_nuplus_get_utilization(uint32_t tile, uint32_t *utilization);


// --------------------------------------------------------------------------------------------------------------------
// Functions specific to NUPLUS
// --------------------------------------------------------------------------------------------------------------------

// --------------------------------------------------------------------------------------------------
// hn_nuplus_set_pc
//   short: Sets a PC address to a given thread in the NUPLUS core
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_id: NUPLUS thread id
//   argument nuplus_pc: New PC address to be written
//
uint32_t hn_nuplus_set_pc(uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_id, uint32_t nuplus_pc);                                

// --------------------------------------------------------------------------------------------------
// hn_nuplus_set_thread_mask
//   short: Sets NUPLUS core active thread mask
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_mask: NUPLUS thread mask (which threads are active)
//
uint32_t hn_nuplus_set_thread_mask(uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_mask);      

// --------------------------------------------------------------------------------------------------
// hn_nuplus_get_control_register
//   short: Reads NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_id: NUPLUS thread id (some control registers have different instance for each thread)
//   argument nuplus_cr: NUPLUS control register id
//   argument item_rec: where to store the value from the selected control register
//
uint32_t hn_nuplus_get_control_register (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_id, uint32_t nuplus_cr, uint32_t * item_rec);

// --------------------------------------------------------------------------------------------------
// hn_nuplus_set_control_register
//   short: Write a NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_thread_id: NUPLUS thread id (some control registers have different instance for each thread)
//   argument nuplus_cr: NUPLUS control register id
//   argument nuplus_data: data to write into the selected NUPLUS control register
//
uint32_t hn_nuplus_set_control_register (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_thread_id, uint32_t nuplus_cr, uint32_t nuplus_data);

// --------------------------------------------------------------------------------------------------
// hn_nuplus_stop
//   short: Stops NUPLUS core
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_num: Total number of cores
//
uint32_t hn_nuplus_stop(uint32_t tile, uint32_t nuplus_core_num);     

// ---------------------------------------------------------------------------------------------
// hn_boot_nuplus
//   short: Boots NUPLUS core
//   argument tile: Tile where the unit is located
//   argument tile_memory: Tile where the memory image (if needed) is located)
//   argument addr: Starting address of the memory image (if needed)
//
uint32_t hn_nuplus_boot(uint32_t tile, uint32_t tile_memory, uint32_t addr);

// ---------------------------------------------------------------------------------------------
// hn_nuplus_thread_mask_convert
//   short: provate function which converts the number of active threads into
//      the right activation thread mask
//   argument thread_numb: number of active threads
//
uint32_t hn_nuplus_thread_mask_convert(uint32_t thread_numb);

// --------------------------------------------------------------------------------------------------
// hn_nuplus_run_kernel
//   uint32_t: HN_SUCCEEDED
//   argument tile: Tile where the NuPlus unit is located
//   argument addr: Virtual address where the image will be found
//   argument args: string including all arguments
//
uint32_t hn_nuplus_run_kernel(uint32_t tile, uint32_t addr, char *args);

// --------------------------------------------------------------------------------------------------
// hn_nuplus_log_event
//   short: Reads NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_log_cmd: NUPLUS Logger command
//   argument event_rec: where to store the event value from the logger
//
uint32_t hn_nuplus_log_event (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_log_cmd, uint32_t * event_rec);

// --------------------------------------------------------------------------------------------------
// hn_nuplus_log_data
//   short: Reads NUPLUS control register
//   argument tile: MANGO tile where the unit is located
//   argument nuplus_core_id: NUPLUS core id
//   argument nuplus_log_cmd: NUPLUS Logger command
//   argument nuplus_log_addr: NUPLUS logger address to retrieve
//   argument event_rec: where to store the values from the logger
//
uint32_t hn_nuplus_log_data (uint32_t tile, uint32_t nuplus_core_id, uint32_t nuplus_log_cmd, uint32_t nuplus_log_addr, nup_logger_entry_t * event_rec);

// ---------------------------------------------------------------------------------------------
// hn_nuplus_endian_row
//   short: private function which swaps endianess of a give memory row
//   argument data: memory row
//   argument size: row length
//
void hn_nuplus_endian_row (uint32_t * data, int size);

#ifdef __cplusplus
}
#endif

#endif
