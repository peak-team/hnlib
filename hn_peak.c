///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2017  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: february 12, 2018
// File Name: hn_peak.c
// Design Name:
// Module Name: Heterogeneous node library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    PEAK functions for the HN library. Implmentation file
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**********************************************************************************************************************
 **
 ** Included files
 **
 **********************************************************************************************************************
 **/
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#include "hn.h"
#include "hn_private_common.h"
#include "hn_peak.h"



/**********************************************************************************************************************
 **
 ** Local functions prototype declaration
 **
 **********************************************************************************************************************
 **/



/**********************************************************************************************************************
 **
 ** Functions implementation
 **
 **********************************************************************************************************************
 **/
const char *hn_peak_to_str_unit_model(uint32_t model) {
  switch(model) {
    case HN_PEAK_MANYCORE_0 : return "PMC2";
    case HN_PEAK_MANYCORE_1 : return "PMC4";
    case HN_PEAK_MANYCORE_2 : return "PMC8";
    case HN_PEAK_MANYCORE_3 : return "PMC8";
    case HN_PEAK_MANYCORE_4 : return "PMC8";
    case HN_PEAK_MANYCORE_5 : return "PMC8";
    case HN_PEAK_MANYCORE_6 : return "PMC8";
    default                  : return NULL;
  }
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_boot(uint32_t tile, uint32_t tile_memory, uint32_t addr, const char *protocol_img_path, const char *kernel_img_path)
{ 
  HN_INITIALIZE_CHECK;

  uint32_t  rv; //returned value from load image function
  log_debug("hn_peak_boot: tile %d, tile_memory %d, addr 0x%08x, protocol image file path: %s  kernel image file path: %s",
        tile, tile_memory, addr, protocol_img_path, kernel_img_path);

  // The process to boot PEAK is divided in the following steps:
  //   1) Write the PEAKos image into memory (provided by addr parameter)
  //   2) Set the TLB of the UNIT to correctly match virtual addresses with physical addresses
  //   3) Write the coherence protocol into the unit
  //   4) Set the PEAK unit (core 0) to point to the starting address
  //   5) Unfreeze core 0 in PEAK unit
  
  // Writting PEAKos image, first check that file exists and is readable
  // char peakosfilePath[1024];
  // sprintf(peakosfilePath, "%s/mango-apps/hn-library/peakos.img", getenv("HOME"));
  // rv = hn_write_image_into_memory(peakosfilePath, tile_memory, addr);
  rv = hn_write_image_into_memory(kernel_img_path, tile_memory, addr);
  if (rv != HN_SUCCEEDED)
  {
    log_error("peakos image file not found, errno value is: %d", errno);
    return rv;
  }

  // Set TLB. PEAKos runs on Segment 0 virtual address space (0x0XXXXXXX)
  hn_set_tlb(tile, 0, 0x00000000, 0x0FFFFFFF, addr, 1, 0, 0, tile_memory);

  // Writting the coherence protocol into the unit, first check that file exists and is readable
  // char peak_protocolfilePath[1024];
  // sprintf(peak_protocolfilePath, "%s/mango-apps/hn-library/peak_protocol.img", getenv("HOME"));
  // rv = hn_peak_write_protocol(peak_protocolfilePath, tile);
  rv = hn_peak_write_protocol(protocol_img_path, tile);
  if(rv != HN_SUCCEEDED)
  {
    log_error("memory coherence protocol image file not found, errno value is: %d", errno);
    return rv;
  }

  // Set PEAK unit (core 0) to point to the starting address (virtual address)
  hn_peak_set_new_pc(tile, 0, 0x00032000);

  // Unfreeze core 0 in PEAK
  hn_peak_unfreeze(tile, 0);

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_write_protocol(const char *file_name, uint32_t tile)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_write_protocol: file_name %s, tile %d", file_name, tile);

  // We open the protocol file
  FILE *fd = fopen(file_name, "r");
  if (fd == NULL) return HN_PROTOCOL_FILE_NOT_FOUND;

  hn_command_peak_write_protocol_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_WRITE_PROTOCOL;
  cmd.tile           = tile;
  cmd.subtile      = 0;

  // We read the file and write the data to send, the file is in ascii format with bytes coded in bits
  // there are linefeed characters that we must avoid
  char buffer[8];
  uint8_t byte;
  uint32_t num_bytes = 0;
  do {
    // we read 8 binary digits and build a byte
    fread(buffer, 1, 1, fd);
    if ((buffer[0]=='0') || (buffer[0]=='1')) {
      fread(&buffer[1], 1, 7, fd);
      byte =  (buffer[7] - '0')       + ((buffer[6] - '0') << 1) + ((buffer[5] - '0') << 2) + ((buffer[4] - '0') << 3) +
             ((buffer[3] - '0') << 4) + ((buffer[2] - '0') << 5) + ((buffer[1] - '0') << 6) + ((buffer[0] - '0') << 7);
      cmd.data[num_bytes] = byte;
      num_bytes++;
    }
  } while (!feof(fd));
  
  cmd.size = num_bytes;
  log_debug("num bytes injected = %d", num_bytes);
  
  return  hn_write_socket(&cmd, sizeof(cmd));
}

/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_run_kernel(uint32_t tile, uint32_t addr, char *args)
{
  HN_INITIALIZE_CHECK;

  uint32_t  i;
  char    peakos_command[1024];
  int     wc;

  log_debug("hn_peak_run_kernel: tile %d", tile);

  // We prepare the command to send to peak
  sprintf(peakos_command, "slaunch %1d %s\n", (addr >> 28) & 0x0000000F, args);

  log_debug("command line: %s", peakos_command);

  // We send the key strokes to PEAKos to launch the kernel
  uint32_t len = strlen(peakos_command);
  hn_command_peak_key_interrupt_t cmd;
  for (i = 0; i < len; i++) {
    cmd.common.command = HN_COMMAND_PEAK_KEY_INTERRUPT;
    cmd.tile           = tile;
    cmd.subtile      = 0;
    cmd.data         = peakos_command[i];
    wc = hn_write_socket(&cmd, sizeof(cmd));
    if (wc != HN_SUCCEEDED) {
      return wc;
    }
  }

  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_set_new_pc(uint32_t tile, uint32_t core, uint32_t addr)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_set_new_pc: tile %d, core %d, addr %x", tile, core, addr);

  hn_command_peak_set_processor_address_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_SET_PROCESSOR_ADDRESS;
  cmd.tile           = tile;
  cmd.subtile      = core;
  cmd.addr         = addr;

  return  hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_unfreeze(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_unfreeze: tile %d, core %d", tile, core);

  hn_command_peak_configure_tile_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_TILE;
  cmd.tile           = tile;
  cmd.subtile      = core;
  cmd.conf         = 0x02;

  return  hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_freeze(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_freeze: tile %d, core %d", tile, core);

  hn_command_peak_configure_tile_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_TILE;
  cmd.tile           = tile;
  cmd.subtile      = core;
  cmd.conf         = 0x01;

  return  hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_reset(uint32_t tile)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_reset: tile %d", tile);

  hn_command_peak_reset_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_RESET;
  cmd.tile           = tile;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_get_console_char(uint32_t *core, char *c)
{
  HN_INITIALIZE_CHECK;

  uint32_t rc = hn_read_socket(c, 1);
  log_debug("hn_peak_get_console_char : %c", c);

  return rc;
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_send_char(uint32_t tile, uint32_t core, char ch)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_send_char: tile %d, core %d, char %d\n", tile, core, ch);

  hn_command_peak_key_interrupt_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_KEY_INTERRUPT;
  cmd.tile           = tile;
  cmd.subtile      = core;
  cmd.data         = ch;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_echo_enable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_echo_enable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile      = core;
  cmd.conf         = HN_PEAK_COMMAND_ECHO_ENABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_echo_disable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_echo_disable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile      = core;
  cmd.conf         = HN_PEAK_COMMAND_ECHO_DISABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_debug_core_enable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_debug_core_enable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.conf           = HN_PEAK_COMMAND_CORE_DEBUG_ENABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_debug_core_disable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_debug_core_disable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.conf           = HN_PEAK_COMMAND_CORE_DEBUG_DISABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_debug_protocol_enable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_debug_protocol_enable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.conf           = HN_PEAK_COMMAND_CACHE_COHERENCE_PROTOCOL_DEBUG_ENABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_debug_protocol_disable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_debug_protocol_disable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.conf           = HN_PEAK_COMMAND_CACHE_COHERENCE_PROTOCOL_DEBUG_DISABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_debug_core_forense_enable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_debug_core_forense_enable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.conf           = HN_PEAK_COMMAND_CORE_FORENSE_DEBUG_ENABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_debug_core_forense_disable(uint32_t tile, uint32_t core)
{
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_debug_core_forense_disable: tile %2d, core %2d", tile, core);

  hn_command_peak_configure_debug_and_stats_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_CONFIGURE_DEBUG_AND_STATS;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.conf           = HN_PEAK_COMMAND_CORE_FORENSE_DEBUG_DISABLE & 0x000000FF;

  return hn_write_socket(&cmd, sizeof(cmd));
}
/**************************************************************************************************
***************************************************************************************************
**/

uint32_t hn_peak_get_utilization(uint32_t tile, uint32_t *utilization)
{
  HN_INITIALIZE_CHECK;

  // We access all TILEREGs inside PEAK to get each core's utilization
  // TODO
  *utilization = 20;
  return HN_SUCCEEDED;
}
/**************************************************************************************************
***************************************************************************************************
**/


uint32_t hn_peak_read_register(uint32_t tile, uint32_t core, uint32_t addr, uint32_t *data) {
  HN_INITIALIZE_CHECK;

  int rc;
  log_debug("hn_peak_read_register: tile %2d, core %2d, addr %u", tile, core, addr);

  hn_command_peak_read_register_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_READ_REGISTER;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.reg            = addr;
  
  rc = hn_write_socket(&cmd, sizeof(cmd));
  if ((rc == HN_SUCCEEDED) && (data != NULL)) {
    rc = hn_read_socket(data, sizeof(*data));
  }

  return rc;
}
/**************************************************************************************************
***************************************************************************************************
**/


uint32_t hn_peak_write_register(uint32_t tile, uint32_t core, uint32_t addr, uint32_t data) {
  HN_INITIALIZE_CHECK;

  log_debug("hn_peak_write_register: tile %2d, core %2d, addr %u, data %08x (%u)", tile, core, addr, data, data);

  hn_command_peak_write_register_t cmd;
  cmd.common.command = HN_COMMAND_PEAK_WRITE_REGISTER;
  cmd.tile           = tile;
  cmd.subtile        = core;
  cmd.reg            = addr;
  cmd.value          = data;
  
  return hn_write_socket(&cmd, sizeof(cmd));
}

