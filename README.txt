
hn_daemon
-----------------------------------------
1- Skip this step if using eth interface
   To make, install and load the pcie driver, cd to hn_daemon folder and run
   > make
   > ./load_pcie_driver.sh

2- To start the daemon, in the hn_daemon folder, type as root (or with sudo permissions):
   > ./hn_daemon <iface_name>
   iface_name is either:
     eth  device name: i.e. eth0 -> ./hn_daemon eth0
     pcie device path: ./hn_daemon /dev/pcie

3- To stop the daemon, in the hn_daemon folder, type as root (or with sudo permissions):
   > ./hn_daemon_stop.sh
