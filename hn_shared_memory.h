#ifdef __cplusplus
extern "C" {
#endif

#ifndef __HN_SHARED_MEMORY_H__
#define __HN_SHARED_MEMORY_H__
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  J. Martinez (jomam10@gap.upv.es)
//
// Create Date: February 12, 2018
// File Name: hn_shared_memory.h
// Design Name:
// Module Name: 
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
//    
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
 *
 */

uint32_t hn_shared_memory_open_buffer(char *bname, uint32_t size, void **ptr, int *fd);

uint32_t hn_shared_memory_create_buffer(char *name, uint32_t size, void **ptr, int *fd);

uint32_t hn_shared_memory_close_buffer(uint32_t size, void *shm_addr, int shm_fd);

uint32_t hn_shared_memory_destroy_buffer(char *name, uint32_t size, void *shm_addr, int shm_fd);

//uint32_t hn_shared_memory_write_to_ddr_memory(void *ptr, uint32_t size, uint32_t address_dst, uint32_t tile_dst, uint32_t blocking_transfer);

//uint32_t hn_shared_memory_read_from_ddr_memory(void *ptr, uint32_t size, uint32_t address_dst, uint32_t tile_dst, uint32_t blocking_transfer);

//*********************************************************************************************************************
// end of file hn_shared_memory.h
//*********************************************************************************************************************
#endif

#ifdef __cplusplus
}
#endif

