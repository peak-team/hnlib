///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// (c) Copyright 2012 - 2018  Parallel Architectures Group (GAP)
// Department of Computing Engineering (DISCA)
// Universitat Politecnica de Valencia (UPV)
// Valencia, Spain
// All rights reserved.
//
// All code contained herein is, and remains the property of
// Parallel Architectures Group. The intellectual and technical concepts
// contained herein are proprietary to Parallel Architectures Group and
// are protected by trade secret or copyright law.
// Dissemination of this code or reproduction of this material is
// strictly forbidden unless prior written permission is obtained
// from Parallel Architectures Group.
//
// THIS SOFTWARE IS MADE AVAILABLE "AS IS" AND IT IS NOT INTENDED FOR USE
// IN WHICH THE FAILURE OF THE SOFTWARE COULD LEAD TO DEATH, PERSONAL INJURY,
// OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE.
//
// contact: jflich@disca.upv.es
//---------------------------------------------------------------------------------------------------------------------
//
// Company:   GAP (UPV)
// Engineer:  R. Tornero (ratorga@gap.upv.es)
//
// Create Date: January 17, 2018
// File Name: hn_logging.c
// Design Name:
// Module Name: HN library
// Project Name: MANGO
// Target Devices:
// Tool Versions:
// Description:
//
//    HN logging system
//
//
// Dependencies: NONE
//
// Revision:
//   Revision 0.01 - File Created
//
// Additional Comments: NONE
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#include "hn_logging.h"

// Width of the WARN, ERROR... field in the fprintf
#define HN_LOG_MSG_PREFIX_WIDTH 10

#define HN_LOG_MSG_TIME_PREFIX_WIDTH 20

// checking if initialized
#define HN_LOG_CHECK_INIT  assert(hn_log_initialized == 1)


static void prefix_timestamp_level(const char *level);

// internal file descriptor used to write log messages to
static FILE *hn_log_fd;

// indicates whether the logging system is initalized
static int hn_log_initialized = 0;

// current log mode 
static hn_log_type_t hn_log_mode = HN_LOG_TO_STDOUT;

// mutex for access to the file descriptor in a pthread safe mode
#ifdef HN_LOGGING_PTHREAD_SAFE
static pthread_mutex_t fd_access_mutex;
#endif



/****************************************************************
 * Initialization & deinitialization functions
 ****************************************************************/

int hn_log_initialize(hn_log_type_t mode, char *flog_name)
{
  char fname[128];
  FILE *fd;

  if (mode == HN_LOG_TO_FILE)
  {
    if (flog_name != NULL) strncpy(fname, flog_name, 127);
    else strncpy(fname, HN_LOG_DEFAULT_FILE, 127);
      
    fname[127] = 0;
    if ((fd = fopen(fname, "w")) == NULL) return -1;
  } 
  else if (mode == HN_LOG_TO_STDOUT)
  {
    fd = stdout;
  } 
  else
  {
    return -1;
  }

#ifdef HN_LOGGING_PTHREAD_SAFE
  if (pthread_mutex_init(&fd_access_mutex, NULL) != 0)
  {
    return -1;
  }
#endif

  hn_log_fd = fd;
  hn_log_mode = mode;
  hn_log_initialized = 1;

  return 0;
}


int hn_log_close()
{
  if ((hn_log_mode == HN_LOG_TO_FILE) && (hn_log_initialized == 1))
  {
    fclose(hn_log_fd);
  }

#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_destroy(&fd_access_mutex);
#endif

  hn_log_mode = HN_LOG_TO_STDOUT;
  hn_log_initialized = 0;

  return 0;
}


void prefix_timestamp_level(const char *l) 
{
  struct timeval  tmnow;
  struct tm       tm;
  char            buf[37];
  char            usec_buf[7];

  gettimeofday(&tmnow, NULL);
  localtime_r(&tmnow.tv_sec, &tm);
  strftime(buf,30,"%H:%M:%S", &tm);
  strcat(buf,".");
  sprintf(usec_buf,"%06d",(int)tmnow.tv_usec);
  strcat(buf,usec_buf);

  fprintf(hn_log_fd, "%-*s%-*s", HN_LOG_MSG_TIME_PREFIX_WIDTH, buf, HN_LOG_MSG_PREFIX_WIDTH, l);
}

/***********************************
 * Printing message functions
 ***********************************/

void log_error(const char *fmt, ...)
{
  va_list         list;
  // logging system must be initialized first
  HN_LOG_CHECK_INIT;

#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_lock(&fd_access_mutex);
#endif
  fprintf(hn_log_fd, "\033[31m");
  prefix_timestamp_level("ERROR");
  va_start(list, fmt);
  vfprintf(hn_log_fd, fmt, list);
  va_end(list);
  fprintf(hn_log_fd, "\033[0m\n");
  fflush(hn_log_fd);
#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_unlock(&fd_access_mutex);
#endif
}

void log_warn(const char *fmt, ...)
{
  va_list         list;
  
  // logging system must be initialized first
  HN_LOG_CHECK_INIT;

#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_lock(&fd_access_mutex);
#endif
  fprintf(hn_log_fd, "\033[33m");
  prefix_timestamp_level("WARN");
  va_start(list, fmt);
  vfprintf(hn_log_fd, fmt, list);
  va_end(list);
  fprintf(hn_log_fd, "\033[0m\n");
  fflush(hn_log_fd);
#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_unlock(&fd_access_mutex);
#endif
}

void log_info(const char *fmt, ...)
{
  va_list         list;

  // logging system must be initialized first
  HN_LOG_CHECK_INIT;

#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_lock(&fd_access_mutex);
#endif
  fprintf(hn_log_fd, "\033[32m");
  prefix_timestamp_level("INFO");
  va_start(list, fmt);
  vfprintf(hn_log_fd, fmt, list);
  va_end(list);
  fprintf(hn_log_fd, "\033[0m\n");
  fflush(hn_log_fd);
#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_unlock(&fd_access_mutex);
#endif
}

void log_notice(const char *fmt, ...)
{
  va_list         list;

  // logging system must be initialized first
  HN_LOG_CHECK_INIT;

#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_lock(&fd_access_mutex);
#endif
  fprintf(hn_log_fd, "\033[36m");
  prefix_timestamp_level("NOTICE");
  va_start(list, fmt);
  vfprintf(hn_log_fd, fmt, list);
  va_end(list);
  fprintf(hn_log_fd, "\033[0m\n");
  fflush(hn_log_fd);
#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_unlock(&fd_access_mutex);
#endif
}

void _log_debug(const char *fmt, ...)
{
  va_list         list;

  // logging system must be initialized first
  HN_LOG_CHECK_INIT;

#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_lock(&fd_access_mutex);
#endif
  prefix_timestamp_level("DEBUG");
  va_start(list, fmt);
  vfprintf(hn_log_fd, fmt, list);
  va_end(list);
  fprintf(hn_log_fd, "\033[0m\n");
  fflush(hn_log_fd);
#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_unlock(&fd_access_mutex);
#endif
}

void _log_verbose(const char *fmt, ...)
{
  va_list         list;

  // logging system must be initialized first
  HN_LOG_CHECK_INIT;

#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_lock(&fd_access_mutex);
#endif
  prefix_timestamp_level("VERBOSE");
  va_start(list, fmt);
  vfprintf(hn_log_fd, fmt, list);
  va_end(list);
  fprintf(hn_log_fd, "\033[0m\n");
  fflush(hn_log_fd);
#ifdef HN_LOGGING_PTHREAD_SAFE
  pthread_mutex_unlock(&fd_access_mutex);
#endif
}

